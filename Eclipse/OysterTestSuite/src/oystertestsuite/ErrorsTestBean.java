/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Specifically written to carry the information on the most recent run of the
 * Test Suite.
 *
<<<<<<< HEAD
 * @author UALR
=======
 * @author UALR
>>>>>>> db795f026300c43faabe494172f53a2664fdc9f3
 */
public class ErrorsTestBean implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3855085090279203363L;
	private String JarUsedInRun;
    private String DateTime;
    private ArrayList<String> list = new ArrayList<>();
    private ArrayList<ErrorsTestBean> ErrorOutputBeanList = new ArrayList<>();
    private String TestBeanName;
    private String TestFailedName;
    private String TestsRunCount;
    private String TestFailed;
    private String TestPassed;
    private Map<String, List<String>> map = new HashMap<>();

    public void setMap(Map<String, List<String>> map) {
        this.map = map;
    }

    public Map<String, List<String>> getMap() {
        return map;
    }

    public void setTestsRunCount(String TestsRunCount) {
        this.TestsRunCount = TestsRunCount;
    }

    public void setTestFailed(String TestFailed) {
        this.TestFailed = TestFailed;
    }

    public void setTestPassed(String TestPassed) {
        this.TestPassed = TestPassed;
    }

    public String getTestsRunCount() {
        return TestsRunCount;
    }

    public String getTestFailed() {
        return TestFailed;
    }

    public String getTestPassed() {
        return TestPassed;
    }

    public void setJarUsedInRun(String JarUsedInRun) {
        this.JarUsedInRun = JarUsedInRun;
    }

    public void setDateTime(String DateTime) {
        this.DateTime = DateTime;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public void setErrorOutputBeanList(ArrayList<ErrorsTestBean> ErrorOutputBeanList) {
        this.ErrorOutputBeanList = ErrorOutputBeanList;
    }

    public void setTestBeanName(String TestBeanName) {
        this.TestBeanName = TestBeanName;
    }

    public void setTestFailedName(String TestFailedName) {
        this.TestFailedName = TestFailedName;
    }

    public String getJarUsedInRun() {
        return JarUsedInRun;
    }

    public String getDateTime() {
        return DateTime;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public ArrayList<ErrorsTestBean> getErrorOutputBeanList() {
        return ErrorOutputBeanList;
    }

    public String getTestBeanName() {
        return TestBeanName;
    }

    public String getTestFailedName() {
        return TestFailedName;
    }

}
