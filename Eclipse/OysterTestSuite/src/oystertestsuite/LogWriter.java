/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
<<<<<<< HEAD
 * @author UALR
=======
 * @author UALR
>>>>>>> db795f026300c43faabe494172f53a2664fdc9f3
 */
public class LogWriter {

    private String newline = System.getProperty(Constants.LineSeparator);
    OysterTestBean kBean = new OysterTestBean();
    FileWriter fileWriter = null;
    FileWriter ChgReptWriters = null;
    FileWriter ClusterRefReports = null;
    String fileName;
    String ChgReportName;
    String ClusterReport;

    public LogWriter(String LogFileAndPath) {

        try {
            fileName = LogFileAndPath;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public LogWriter(OysterTestBean myBean, String file, String testSuiteOysterRootDir) {

        if (file.equalsIgnoreCase(Constants.ChangeReport)) {
            try {
                ChgReportName = testSuiteOysterRootDir + "/" + myBean.getTestCase_Name() + Constants.OUTPUT + myBean.getOutputChangeReportFile();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (ChgReptWriters != null) {
                        ChgReptWriters.flush();
                        ChgReptWriters.close();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (file.equalsIgnoreCase(Constants.ClusterRef)) {
            try {
                ClusterReport = testSuiteOysterRootDir + "/" + myBean.getTestCase_Name() + Constants.OUTPUT + myBean.getOutPutRefClusterFile();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (ClusterRefReports != null) {
                        ClusterRefReports.flush();
                        ClusterRefReports.close();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void WriteToChangeReport(String Value) {
        try {
            ChgReptWriters = new FileWriter(ChgReportName, true);
            ChgReptWriters.write(Value + newline);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ChgReptWriters != null) {
                    ChgReptWriters.flush();
                    ChgReptWriters.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void WriteToClusterRefReport(String Value) {
        try {
            ClusterRefReports = new FileWriter(ClusterReport, true);
            ClusterRefReports.write(Value + newline);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ClusterRefReports != null) {
                    ClusterRefReports.flush();
                    ClusterRefReports.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void WriteToLog(String Value) {

        try {
            fileWriter = new FileWriter(fileName, true);
            fileWriter.write(Value + newline);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    // fileWriter.close();
                    fileWriter.flush();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void WriteToLog(String Value, String Value2) {

        try {
            fileWriter = new FileWriter(fileName, true);
            fileWriter.write(Value);
            fileWriter.write(Value2);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    // fileWriter.close();
                    fileWriter.flush();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void Flush() {
        try {
            if (fileWriter != null) {
                fileWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Close() {
        try {
            if (fileWriter != null) {
                fileWriter.flush();
                fileWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
