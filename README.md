# README #


Oyster Test Suite
=================

* OysterTestSuite is a GUI program used for Regression Testing of OYSTER java code changes.

* The Program runs against the Oyster Demo Directories to test changes to the Oyster jar.

* Originally designed to help UALR Oyster java developers.


### Setup ###

* Downloading the OysterTestSuiteDir.zip file in downloads is the best way to start.

* A copy of the OysterTestSuite.jar is included in the zip file.

* The file contains the Demo Directories and all necessary support files.

* Without this starting point, testing of any code changes can not occur.


#### Configuration ####

* Java projects and code for NetBeans and Eclipse are available.

* DotNet project code for Visual Studio 2017 is available.

* The Java version will run on Windows, Unix and Mac.

* The TestSuite.xml file will need the line endings "CRLF" changed to "LF" for the Mac.

* The DotNet version runs on Windows only.


#### Dependencies ####

* Java 1.8 and Python 3.7 must be installed.

* The OysterTestSuiteDir.zip must be installed for testing.

* NetBeans 8 or higher and  Eclipse Java Photon for java development.

* Visual Studio 2017 Community Edition for .NET






