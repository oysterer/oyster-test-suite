/*
 
 */
package oystertestsuite;

import javax.swing.text.StyledEditorKit;
import javax.swing.text.ViewFactory;

/**
 *
 * @author UALR
 */
public class OtsEditorKit extends StyledEditorKit {

    private ViewFactory newViewFactory;

    public OtsEditorKit() {
        newViewFactory = new OtsViewFactory();
    }

    @Override
    public ViewFactory getViewFactory() {
        return newViewFactory;
    }

    @Override
    public String getContentType() {
        return "text/xml";
    }

}
