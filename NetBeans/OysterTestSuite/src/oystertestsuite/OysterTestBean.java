/*
* Bean class to hold data and values for the OysterTestSuite process
 */
package oystertestsuite;

import java.util.ArrayList;
import java.util.Dictionary;

public class OysterTestBean implements java.io.Serializable {

   

    private String TestSuiteWorkspace;
    private String TestSuiteXMLFile;
    private String TestSuiteScriptsDir;
    private String TestSuiteOysterRoot;
    private String TestSuiteOysterToJarRun;
    private String LogFile;
    private String PythonExe;
    private String PythonFile;
    private String OutPutLogFile;
    private String OysterLinkFile;
    private ArrayList<String> list;// = new ArrayList<String>();
    private ArrayList<OysterTestBean> ExpectedOutputBeanList; // = new ArrayList<OysterTestBean>();
    private String FileLoadedToXMLTextBox = "";
    private String FileLoadedToXMLTextBox2 = "";
    private String FileLoadedToXMLTextBox3 = "";

  
    private String OutputChangeReportFile;
    private String OysterRunScript;
    private int RefExpectedCount;
    private int ClusterExpectedCount;
    private int LogRefExpectedCount;
    private int LogClusterExpectedCount;
    private int LinkRefExpectedCount;
    private int LinkClusterExpectedCount;
    
      public String getFileLoadedToXMLTextBox3() {
        return FileLoadedToXMLTextBox3;
    }

    public void setFileLoadedToXMLTextBox3(String FileLoadedToXMLTextBox3) {
        this.FileLoadedToXMLTextBox3 = FileLoadedToXMLTextBox3;
    }

    public void setLogRefExpectedCount(int LogRefExpectedCount) {
        this.LogRefExpectedCount = LogRefExpectedCount;
    }

    public void setLogClusterExpectedCount(int LogClusterExpectedCount) {
        this.LogClusterExpectedCount = LogClusterExpectedCount;
    }

    public void setLinkRefExpectedCount(int LinkRefExpectedCount) {
        this.LinkRefExpectedCount = LinkRefExpectedCount;
    }

    public void setLinkClusterExpectedCount(int LinkClusterExpectedCount) {
        this.LinkClusterExpectedCount = LinkClusterExpectedCount;
    }

    public int getLogRefExpectedCount() {
        return LogRefExpectedCount;
    }

    public int getLogClusterExpectedCount() {
        return LogClusterExpectedCount;
    }

    public int getLinkRefExpectedCount() {
        return LinkRefExpectedCount;
    }

    public int getLinkClusterExpectedCount() {
        return LinkClusterExpectedCount;
    }

    private String TestCase_Name;
    private Dictionary TestCases; // As Dictionary(Of String, String)
    private Dictionary ChangeReport;  //(Of String, Collection)
    private int LogReferenceCount;
    private int LogClusterCount;
    private int LinkReferenceCount;
    private int LinkClusterCount;
    private int ChangeReportOutputIdentities;
    private int ChangeReportInputIdentitiesUpdated;
    private int ChangeReporetInputIdentitiesMerged;
    private int ChangeReportNewInputIdentitiesCreated;
    private int ChangeReportInputIdentitiesNotUpdated;
    private int ChangeReportErrorIdentities;
    private int ChangeReportInputIdentities;
    private int ChangeReportExpectOutputIdentities;
    private int ChangeReportExpectInputIdentitiesUpdated;
    private int ChangeReportExpectInputIdentitiesMerged;
    private int ChangeReportExpectNewInputIdentitiesCreated;
    private int ChangeReportExpectInputIdentitiesNotUpdated;
    private int ChangeReportExpectErrorIdentities;
    private int ChangeReportExpectInputIdentities;
    private int SpecialLinkCounts;

    public String getTestSuiteWorkspace() {
        return TestSuiteWorkspace;
    }

    public void setTestSuiteWorkspace(String TestSuiteWorkspace) {
        this.TestSuiteWorkspace = TestSuiteWorkspace;
    }

    public String getTestSuiteScriptsDir() {
        return TestSuiteScriptsDir;
    }

    public void setTestSuiteScriptsDir(String TestSuiteScriptsDir) {
        this.TestSuiteScriptsDir = TestSuiteScriptsDir;
    }

    public ArrayList<OysterTestBean> getExpectedOutputBeanList() {
        return ExpectedOutputBeanList;
    }

    public void setExpectedOutputBeanList(ArrayList<OysterTestBean> ExpectedOutputBeanList) {
        this.ExpectedOutputBeanList = ExpectedOutputBeanList;
    }
//Dictionary dict as new HashTable();
    private Dictionary TestCaseBeanDictionary; //(Of String, HelperBean)

    public Dictionary getTestCaseBeanDictionary() {
        return TestCaseBeanDictionary;
    }

    public void setTestCaseBeanDictionary(Dictionary TestCaseBeanDictionary) {
        this.TestCaseBeanDictionary = TestCaseBeanDictionary;
    }

    public Dictionary getChangeReport() {
        return ChangeReport;
    }

    public void setChangeReport(Dictionary ChangeReport) {
        this.ChangeReport = ChangeReport;
    }

    private String OutPutRefClusterFile;

    public String getOutPutRefClusterFile() {
        return OutPutRefClusterFile;
    }

    public void setOutPutRefClusterFile(String OutPutRefClusterFile) {
        this.OutPutRefClusterFile = OutPutRefClusterFile;
    }

    public String getOutputChangeReportFile() {
        return OutputChangeReportFile;
    }

    public void setOutputChangeReportFile(String OutputChangeReportFile) {
        this.OutputChangeReportFile = OutputChangeReportFile;
    }

    public String getOysterRunScript() {
        return OysterRunScript;
    }

    public void setOysterRunScript(String OysterRunScript) {
        this.OysterRunScript = OysterRunScript;
    }

    public int getRefExpectedCount() {
        return RefExpectedCount;
    }

    public void setRefExpectedCount(int RefExpectedCount) {
        this.RefExpectedCount = RefExpectedCount;
    }

    public int getClusterExpectedCount() {
        return ClusterExpectedCount;
    }

    public void setClusterExpectedCount(int ClusterExpectedCount) {
        this.ClusterExpectedCount = ClusterExpectedCount;
    }

    public String getTestCase_Name() {
        return TestCase_Name;
    }

    public void setTestCase_Name(String TestCase_Name) {
        this.TestCase_Name = TestCase_Name;
    }

    public int getLogReferenceCount() {
        return LogReferenceCount;
    }

    public void setLogReferenceCount(int LogReferenceCount) {
        this.LogReferenceCount = LogReferenceCount;
    }

    public int getLogClusterCount() {
        return LogClusterCount;
    }

    public void setLogClusterCount(int LogClusterCount) {
        this.LogClusterCount = LogClusterCount;
    }

    public int getLinkReferenceCount() {
        return LinkReferenceCount;
    }

    public void setLinkReferenceCount(int LinkReferenceCount) {
        this.LinkReferenceCount = LinkReferenceCount;
    }

    public int getLinkClusterCount() {
        return LinkClusterCount;
    }

    public void setLinkClusterCount(int LinkClusterCount) {
        this.LinkClusterCount = LinkClusterCount;
    }

    public int getChangeReportOutputIdentities() {
        return ChangeReportOutputIdentities;
    }

    public void setChangeReportOutputIdentities(int ChangeReportOutputIdentities) {
        this.ChangeReportOutputIdentities = ChangeReportOutputIdentities;
    }

    public int getChangeReportInputIdentitiesUpdated() {
        return ChangeReportInputIdentitiesUpdated;
    }

    public void setChangeReportInputIdentitiesUpdated(int ChangeReportInputIdentitiesUpdated) {
        this.ChangeReportInputIdentitiesUpdated = ChangeReportInputIdentitiesUpdated;
    }

    public int getChangeReporetInputIdentitiesMerged() {
        return ChangeReporetInputIdentitiesMerged;
    }

    public void setChangeReporetInputIdentitiesMerged(int ChangeReporetInputIdentitiesMerged) {
        this.ChangeReporetInputIdentitiesMerged = ChangeReporetInputIdentitiesMerged;
    }

    public int getChangeReportNewInputIdentitiesCreated() {
        return ChangeReportNewInputIdentitiesCreated;
    }

    public void setChangeReportNewInputIdentitiesCreated(int ChangeReportNewInputIdentitiesCreated) {
        this.ChangeReportNewInputIdentitiesCreated = ChangeReportNewInputIdentitiesCreated;
    }

    public int getChangeReportInputIdentitiesNotUpdated() {
        return ChangeReportInputIdentitiesNotUpdated;
    }

    public void setChangeReportInputIdentitiesNotUpdated(int ChangeReportInputIdentitiesNotUpdated) {
        this.ChangeReportInputIdentitiesNotUpdated = ChangeReportInputIdentitiesNotUpdated;
    }

    public int getChangeReportErrorIdentities() {
        return ChangeReportErrorIdentities;
    }

    public void setChangeReportErrorIdentities(int ChangeReportErrorIdentities) {
        this.ChangeReportErrorIdentities = ChangeReportErrorIdentities;
    }

    public int getChangeReportInputIdentities() {
        return ChangeReportInputIdentities;
    }

    public void setChangeReportInputIdentities(int ChangeReportInputIdentities) {
        this.ChangeReportInputIdentities = ChangeReportInputIdentities;
    }

    public int getChangeReportExpectOutputIdentities() {
        return ChangeReportExpectOutputIdentities;
    }

    public void setChangeReportExpectOutputIdentities(int ChangeReportExpectOutputIdentities) {
        this.ChangeReportExpectOutputIdentities = ChangeReportExpectOutputIdentities;
    }

    public int getChangeReportExpectInputIdentitiesUpdated() {
        return ChangeReportExpectInputIdentitiesUpdated;
    }

    public void setChangeReportExpectInputIdentitiesUpdated(int ChangeReportExpectInputIdentitiesUpdated) {
        this.ChangeReportExpectInputIdentitiesUpdated = ChangeReportExpectInputIdentitiesUpdated;
    }

    public int getChangeReportExpectInputIdentitiesMerged() {
        return ChangeReportExpectInputIdentitiesMerged;
    }

    public void setChangeReportExpectInputIdentitiesMerged(int ChangeReportExpectInputIdentitiesMerged) {
        this.ChangeReportExpectInputIdentitiesMerged = ChangeReportExpectInputIdentitiesMerged;
    }

    public int getChangeReportExpectNewInputIdentitiesCreated() {
        return ChangeReportExpectNewInputIdentitiesCreated;
    }

    public void setChangeReportExpectNewInputIdentitiesCreated(int ChangeReportExpectNewInputIdentitiesCreated) {
        this.ChangeReportExpectNewInputIdentitiesCreated = ChangeReportExpectNewInputIdentitiesCreated;
    }

    public int getChangeReportExpectInputIdentitiesNotUpdated() {
        return ChangeReportExpectInputIdentitiesNotUpdated;
    }

    public void setChangeReportExpectInputIdentitiesNotUpdated(int ChangeReportExpectInputIdentitiesNotUpdated) {
        this.ChangeReportExpectInputIdentitiesNotUpdated = ChangeReportExpectInputIdentitiesNotUpdated;
    }

    public int getChangeReportExpectErrorIdentities() {
        return ChangeReportExpectErrorIdentities;
    }

    public void setChangeReportExpectErrorIdentities(int ChangeReportExpectErrorIdentities) {
        this.ChangeReportExpectErrorIdentities = ChangeReportExpectErrorIdentities;
    }

    public int getChangeReportExpectInputIdentities() {
        return ChangeReportExpectInputIdentities;
    }

    public void setChangeReportExpectInputIdentities(int ChangeReportExpectInputIdentities) {
        this.ChangeReportExpectInputIdentities = ChangeReportExpectInputIdentities;
    }

    public int getSpecialLinkCounts() {
        return SpecialLinkCounts;
    }

    public void setSpecialLinkCounts(int SpecialLinkCounts) {
        this.SpecialLinkCounts = SpecialLinkCounts;
    }

    public Dictionary getTestCases() {
        return TestCases;
    }

    public void setTestCases(Dictionary TestCases) {
        this.TestCases = TestCases;
    }

    public String getOysterLinkFile() {
        return OysterLinkFile;
    }

    public void setOysterLinkFile(String OysterLinkFile) {
        this.OysterLinkFile = OysterLinkFile;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public String getFileLoadedToXMLTextBox() {
        return FileLoadedToXMLTextBox;
    }

    public void setFileLoadedToXMLTextBox(String FileLoadedToXMLTextBox) {
        this.FileLoadedToXMLTextBox = FileLoadedToXMLTextBox;
    }

    public String getFileLoadedToXMLTextBox2() {
        return FileLoadedToXMLTextBox2;
    }

    public void setFileLoadedToXMLTextBox2(String FileLoadedToXMLTextBox2) {
        this.FileLoadedToXMLTextBox2 = FileLoadedToXMLTextBox2;
    }

    public OysterTestBean() {
    }

    public ArrayList<String> getArrayList() {
        return list;
    }

    public void setArrayList(ArrayList<String> myList) {
        list = myList;
    }

    public String getTestSuiteOysterRoot() {
        return TestSuiteOysterRoot;
    }

    public void setTestSuiteOysterRoot(String testSuiteOysterRoot) {
        TestSuiteOysterRoot = testSuiteOysterRoot;
    }

    public String getTestSuiteOysterToJarRun() {
        return TestSuiteOysterToJarRun;
    }

    public void setTestSuiteOysterToJarRun(String testSuiteOysterToJarRun) {
        TestSuiteOysterToJarRun = testSuiteOysterToJarRun;
    }

    public String getLogFile() {
        return LogFile;
    }

    public void setLogFile(String logFile) {
        LogFile = logFile;
    }

    public String getPythonExe() {
        return PythonExe;
    }

    public void setPythonExe(String pythonExe) {
        PythonExe = pythonExe;
    }

    public String getPythonFile() {
        return PythonFile;
    }

    public void setPythonFile(String pythonFile) {
        PythonFile = pythonFile;
    }

    public String getOutPutLogFile() {
        return OutPutLogFile;
    }

    public void setOutPutLogFile(String outPutLogFile) {
        OutPutLogFile = outPutLogFile;
    }
    
     /**
     * @return the TestSuiteXMLFile
     */
    public String getTestSuiteXMLFile() {
        return TestSuiteXMLFile;
    }

    /**
     * @param TestSuiteXMLFile the TestSuiteXMLFile to set
     */
    public void setTestSuiteXMLFile(String TestSuiteXMLFile) {
        this.TestSuiteXMLFile = TestSuiteXMLFile;
    }
}
