/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

/**
 *
 * @author UALR
 */
import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OtsContextMenuER extends JPopupMenu {

    private Clipboard clipboard;

    private UndoManager undoManager;

    private JMenuItem cut;
    private JMenuItem copy;
    private JMenuItem paste;

    private JMenuItem comment;
    private JMenuItem unComment;
    private JTextComponent textComponent;

    public OtsContextMenuER() {
        undoManager = new UndoManager();
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        addPopupMenuItems();
    }

    private void addPopupMenuItems() {
        comment = new JMenuItem("Comment Selected Text");
        comment.setEnabled(false);
        comment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        comment.addActionListener(event -> textComponent.replaceSelection("#" + textComponent.getSelectedText().trim()));
        add(comment);

        unComment = new JMenuItem("Uncomment Selected Text");
        unComment.setEnabled(false);
        unComment.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        unComment.addActionListener(event -> textComponent.replaceSelection(cleanText(textComponent.getSelectedText().trim())));
        add(unComment);

        add(new JSeparator());

        copy = new JMenuItem("Copy Selection");
        copy.setEnabled(false);
        copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        copy.addActionListener(event -> textComponent.copy());
        add(copy);

        paste = new JMenuItem("Paste Selection");
        paste.setEnabled(false);
        paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        paste.addActionListener(event -> textComponent.paste());
        add(paste);

        cut = new JMenuItem("Cut Selection");
        cut.setEnabled(false);
        cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        cut.addActionListener(event -> textComponent.cut());
        add(cut);

    }

    private String cleanText(String text) {

        String myText = text.replace("#", "");
       // myText = myText.replace("-->", "");

        return myText;
    }

    private void addTo(JTextComponent textComponent) {
        textComponent.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent pressedEvent) {
                if ((pressedEvent.getKeyCode() == KeyEvent.VK_Z)
                        && ((pressedEvent.getModifiersEx() & Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()) != 0)) {
                    if (undoManager.canUndo()) {
                        undoManager.undo();
                    }
                }

                if ((pressedEvent.getKeyCode() == KeyEvent.VK_Y)
                        && ((pressedEvent.getModifiersEx() & Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()) != 0)) {
                    if (undoManager.canRedo()) {
                        undoManager.redo();
                    }
                }
            }
        });

        textComponent.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent releasedEvent) {
                handleContextMenu(releasedEvent);
            }

            @Override
            public void mouseReleased(MouseEvent releasedEvent) {
                handleContextMenu(releasedEvent);
            }
        });

        textComponent.getDocument().addUndoableEditListener(event -> undoManager.addEdit(event.getEdit()));
    }

    private void handleContextMenu(MouseEvent releasedEvent) {
        if (releasedEvent.getButton() == MouseEvent.BUTTON3) {
            processClick(releasedEvent);
        }
    }

    private void processClick(MouseEvent event) {
        textComponent = (JTextComponent) event.getSource();
        textComponent.requestFocus();
        boolean enableCut = false;
        boolean enableCopy = false;
        boolean enablePaste = false;
        boolean enableComment = false;
        boolean enableunComment = false;

        String selectedText = textComponent.getSelectedText();

        if (selectedText != null) {
            if (selectedText.length() > 0) {
                enableunComment = true;
                enableComment = true;
                enableCut = true;
                enableCopy = true;

            }
        }

        if (clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor) && textComponent.isEnabled()) {
            enablePaste = true;
        }

        cut.setEnabled(enableCut);
        copy.setEnabled(enableCopy);
        paste.setEnabled(enablePaste);

        comment.setEnabled(enableComment);
        unComment.setEnabled(enableunComment);

        // Shows the popup menu
        show(textComponent, event.getX(), event.getY());
    }

    public static void addOtsContextMenu(JTextComponent component) {
        OtsContextMenuER otsContextMenu = new OtsContextMenuER();
        otsContextMenu.addTo(component);
    }
}
