/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package oystertestsuite;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class is used to parse the OysterRunScript XML file and return an
 * instantiated <code>OysterRunScript</code> object.
 *
 * @author Eric D. Nelson
 */
// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.6E0A2B39-1F9A-4652-9A94-A390DE893E6A]
// </editor-fold> 
public class SuiteXMLParser extends OysterXMLParser {

    private static String text = null;
    private static String value = null;
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.BB06FE95-AE94-BA7D-E237-96CF9FAC3139]
    // </editor-fold> 
    /**
     * The <code>OysterRunScript</code> to be populated
     */

    //stores values to return about the TestSuiteXML file
    private static OysterTestBean testSuitelXMLBean;//

    //Handles the run Scripts for OysterTestSuite
    private static OysterTestBean scriptsXMLBean;//    

    /**
     * Used to hold the array list test xml files to run
     */
    private ArrayList<String> runFileList = new ArrayList<String>(100);

    /**
     * Used to hold the array list test xml files to run
     */
    private ArrayList<OysterTestBean> runXMLValuesList = new ArrayList<OysterTestBean>(100);

    /**
     * Used to hold the current XML parent tag
     */
    private String parent = "";

    /**
     * The LCRD filter used during parsing
     */
    private ArrayList<String> filter = null;

    /**
     * The filterNumber used during parsing
     */
    private int filterNumber = 1;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.AAAAC5A5-AA73-F505-6B15-35C39FEA65D2]
    // </editor-fold> 
    /**
     * Creates a new instance of <code>RunScriptParser</code>.
     */
    public SuiteXMLParser() {
        super();
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.68381C37-A0DA-48BF-7455-2226B713C8C3]
    // </editor-fold> 
    /**
     * Returns the <code>OysterRunScript</code> for this
     * <code>RunScriptParser</code>.
     *
     * @return the <code>OysterRunScript</code>.
     */
    /*   public OysterRunScript getRunScript () {
        return runScript;
    }*/
    public OysterTestBean getTestSuitelXMLBean() {
        return testSuitelXMLBean;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.7BFAB102-7817-831E-CA70-E701F850FD55]
    // </editor-fold> 
    /**
     * Sets the <code>OysterRunScript</code> for this
     * <code>RunScriptParser</code>.
     *
     * @param rs the <code>OysterRunScript</code> to be set.
     */
    /*   public void setRunScript (OysterRunScript rs) {
        runScript = rs;
    }*/
    public void setTestSuitelXMLBean(OysterTestBean tb) {
        testSuitelXMLBean = tb;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.BDD384C3-44E8-A45F-88C7-153F39CBCD20]
    // </editor-fold> 
    /**
     * Called when the Parser starts parsing the Current XML File. Handle any
     * document specific initialization code here.
     *
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startDocument() throws org.xml.sax.SAXException {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.DB4AA7BC-6769-55CE-1027-6C6C3DE5F115]
    // </editor-fold> 
    /**
     * Called when the Parser Completes parsing the Current XML File. Handle any
     * document specific clean up code here.
     *
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endDocument() throws org.xml.sax.SAXException {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.84B81869-DD2A-A6E0-0FC7-F34BC0BDA3EB]
    // </editor-fold> 
    /**
     * Called when the starting of the Element is reached. For Example if we
     * have Tag called <Title> ... </Title>, then this method is called when
     * <Title>
     * tag is Encountered while parsing the Current XML File. The attrs
     * Parameter has the list of all Attributes declared for the Current Element
     * in the XML File.
     *
     * @param namespaceURI URI for this namespace
     * @param lName local XML name
     * @param qName qualified XML name
     * @param attrs list of all Attributes declared for the Current Element
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startElement(String namespaceURI, String lName, String qName, Attributes attrs) throws org.xml.sax.SAXException {
        String eName = lName; // element name

        if ("".equals(eName)) {
            eName = qName;
        }
        // namespaceAware = false emit("<"+eName);

        // clear the data
        data = "";

        if (eName.equalsIgnoreCase("TestSuite")) {
            parent = eName;
        } else if (eName.equalsIgnoreCase("RunInfo")) {
            parent = eName;
        } else if (eName.equalsIgnoreCase("TestSuiteInfo")) {
            parent = eName;
        } else if (eName.equalsIgnoreCase("TestCase")) {
            parent = eName;
        } else if (eName.equalsIgnoreCase("ReferenceClusterFile")) {
            parent = eName;
        } else if (eName.equalsIgnoreCase("ChangeReportsFile")) {
            parent = eName;
        }
        // get XML attributes
        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {

                String token = attrs.getValue(i).trim();
                //   System.out.println("XML token is "+ token);
                if (token.equalsIgnoreCase("TestSuiteRootDir")) {
                    text = token;
                } else if (token.equalsIgnoreCase("TestSuiteScriptsDir")) {
                    text = token;
                } else if (token.equalsIgnoreCase("OysterRootTestSuiteDir")) {
                    text = token;
                } else if (token.equalsIgnoreCase("OysterJarToRun")) {
                    text = token;
                } else if (token.equalsIgnoreCase("TestSuiteLogFile")) {
                    text = token;
                } else if (token.equalsIgnoreCase("PythonExeDir")) {
                    text = token;
                } else if (token.equalsIgnoreCase("PythonRunFile")) {
                    text = token;
                } else if (token.equalsIgnoreCase("TestSuiteXMLFile")) {
                    text = token;
                } else if (token.contains("LinkFile")) {

                    this.scriptsXMLBean.setOysterLinkFile(token);
                } else if (token.contains("RunScript")) {

                    this.scriptsXMLBean.setTestCase_Name(token.substring(0, token.indexOf("RunScript")));

                    text = token.substring(0, token.indexOf("RunScript"));
                } else if (token.contains("Script")) {

                    this.scriptsXMLBean.setTestCase_Name(token.substring(0, token.indexOf("RunScript")));

                    text = token.substring(0, token.indexOf("RunScript"));
                } else if (token.contains(".log")) {

                    this.scriptsXMLBean.setLogFile(token);
                } else if (token.contains("ReferenceCounts")) {
                    value = token;
                } else if (token.contains("ClusterCounts")) {
                    value = token;
                } else if (token.contains("LogReferenceCounts")) {
                    value = token;
                } else if (token.contains("LogClusterCounts")) {
                    value = token;
                } else if (token.contains("LinkReferenceCounts")) {
                    value = token;
                } else if (token.contains("LinkClusterCounts")) {
                    value = token;
                } else if (token.contains("ReferenceClusterFile")) {
                    value = token;
                } else if (token.contains("ChangeReportsFile")) {
                    value = token;
                } else if (token.contains("OutputIdentities")) {
                    value = token;
                } else if (token.contains("InputIdentities")) {
                    value = token;
                } else if (token.contains("InputIdentitiesUpdated")) {
                    value = token;
                } else if (token.contains("InputIdentitiesNotUpdated")) {
                    value = token;
                } else if (token.contains("InputIdentitiesMerged")) {
                    value = token;
                } else if (token.contains("NewInputIdentitiesCreated")) {
                    value = token;
                } else if (token.contains("ErrorIdentities")) {
                    value = token;
                }

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.BDA83A51-4C31-63F2-7785-FFD067240C3B]
    // </editor-fold> 
    /**
     * Called when the Ending of the current Element is reached. For example in
     * the above explanation, this method is called when </Title> tag is reached
     *
     * @param namespaceURI URI for this namespace
     * @param sName
     * @param qName qualified XML name
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endElement(String namespaceURI, String sName, String qName) throws org.xml.sax.SAXException {
        String eName = sName; // element name 
        if ("".equals(eName)) {
            eName = qName;
        }

        if (eName.equalsIgnoreCase("TestSuite")) {
            // System.out.println("list size " + list.size());
            testSuitelXMLBean.setArrayList(runFileList);
        } else if (eName.equalsIgnoreCase("RunInfo")) {;
            parent = "";

        } else if (eName.equalsIgnoreCase("TestSuiteInfo")) {

            parent = "";
            if (text.equalsIgnoreCase("TestSuiteRootDir")) {
                testSuitelXMLBean.setTestSuiteWorkspace(data.trim());
            } else if (text.equalsIgnoreCase("TestSuiteScriptsDir")) {
                testSuitelXMLBean.setTestSuiteScriptsDir(data.trim());
            } else if (text.equalsIgnoreCase("OysterRootTestSuiteDir")) {
                testSuitelXMLBean.setTestSuiteOysterRoot(data.trim());
            } else if (text.equalsIgnoreCase("OysterJarToRun")) {
                testSuitelXMLBean.setTestSuiteOysterToJarRun(data.trim());
            } else if (text.equalsIgnoreCase("TestSuiteLogFile")) {
                testSuitelXMLBean.setOutPutLogFile(data.trim());
            } else if (text.equalsIgnoreCase("PythonExeDir")) {
                testSuitelXMLBean.setPythonExe(data.trim());
            } else if (text.equalsIgnoreCase("PythonRunFile")) {
                testSuitelXMLBean.setPythonFile(data.trim());
            } else if (text.equalsIgnoreCase("TestSuiteXMLFile")) {
                testSuitelXMLBean.setTestSuiteXMLFile(data.trim());
            }
            //testBean.setTestSuiteCaseWorkspace(eName.getBytes().toString());
        } else if (eName.equalsIgnoreCase("TestCase")) {

            runFileList.add(data.trim());

        } else if (eName.equalsIgnoreCase("ReferenceClusterFile")) {
            if (text.equalsIgnoreCase(scriptsXMLBean.getTestCase_Name())) {
                scriptsXMLBean.setOutPutRefClusterFile(data.trim());
                //  System.out.println("ReferenceClusterFile " + data.trim());
            }

        } else if (eName.equalsIgnoreCase("ChangeReportsFile")) {
            if (text.equalsIgnoreCase(scriptsXMLBean.getTestCase_Name())) {
                scriptsXMLBean.setOutputChangeReportFile(data.trim());
                //  System.out.println("ReferenceClusterFile " + data.trim());
            }
        } else if (eName.equalsIgnoreCase("Compare")) {
            if (text.equalsIgnoreCase(scriptsXMLBean.getTestCase_Name())) {

                if (value.equalsIgnoreCase("ReferenceCounts")) {
                    // System.out.println("ReferenceCountsData " + data.trim());
                    scriptsXMLBean.setRefExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("ClusterCounts")) {
                    //    System.out.println("ClusterCounts " + data.trim());
                    scriptsXMLBean.setClusterExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("LogReferenceCounts")) {
                    // System.out.println("LinkReferenceCountsData " + data.trim());
                    scriptsXMLBean.setLogRefExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("LinkReferenceCounts")) {
                    //    System.out.println("ClusterCounts " + data.trim());
                    scriptsXMLBean.setLinkRefExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("LogClusterCounts")) {
                    //    System.out.println("ClusterCounts " + data.trim());
                    scriptsXMLBean.setLogClusterExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("LinkClusterCounts")) {
                    //    System.out.println("ClusterCounts " + data.trim());
                    scriptsXMLBean.setLinkClusterExpectedCount(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("ReferenceClusterFile")) {
                    //       System.out.println("ReferenceClusterFile " + data.trim());
                    scriptsXMLBean.setOutPutRefClusterFile(data.trim());
                    value = "";
                } else if (value.equalsIgnoreCase("ChangeReportsFile")) {
                    scriptsXMLBean.setOutputChangeReportFile(data.trim());
                    //   System.out.println("ChangeReportsFile " + data.trim());
                    value = "";
                } else if (value.equalsIgnoreCase("OutputIdentities")) {
                    //System.out.println("OutputIdentities " + data.trim());
                    scriptsXMLBean.setChangeReportExpectOutputIdentities(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("InputIdentities")) {
                    scriptsXMLBean.setChangeReportExpectInputIdentities(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("InputIdentitiesUpdated")) {
                    scriptsXMLBean.setChangeReportExpectInputIdentitiesUpdated(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("InputIdentitiesNotUpdated")) {
                    scriptsXMLBean.setChangeReportExpectInputIdentitiesNotUpdated(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("InputIdentitiesMerged")) {
                    scriptsXMLBean.setChangeReportExpectInputIdentitiesMerged(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("NewInputIdentitiesCreated")) {
                    scriptsXMLBean.setChangeReportExpectNewInputIdentitiesCreated(Integer.parseInt(data.trim()));
                    value = "";
                } else if (value.equalsIgnoreCase("ErrorIdentities")) {
                    scriptsXMLBean.setChangeReportExpectErrorIdentities(Integer.parseInt(data.trim()));
                    value = "";
                }

            }

        }
    }

    /**
     * This method is the main entry point for the SAX Parser.
     *
     * @param file the XML file to be parsed.
     * @return <code>OysterRunScript</code> containing data from the file.
     */
    public OysterTestBean parse(String file) {
        // Use an instance of ourselves as the SAX event handler 
        DefaultHandler handler = new SuiteXMLParser();
        // Use the default (non-validating) parser 
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);

        try {
            // Set up output stream 
            setOut(new OutputStreamWriter(System.out, "UTF8"));

            testSuitelXMLBean = new OysterTestBean();

            scriptsXMLBean = new OysterTestBean();

            // Parse the input 
            SAXParser saxParser = factory.newSAXParser();
        //    System.out.println("in parser " + file);

            saxParser.parse(new File(file), handler);

        } catch (IOException ex) {
            Logger.getLogger(SuiteXMLParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            System.err.println(file);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SuiteXMLParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            System.err.println(file);
        } catch (SAXException ex) {
            Logger.getLogger(SuiteXMLParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            System.err.println(file);
        }
        // if (file.contains("TestSuite.xml") || file.contains("TestSuiteLinux.xml") ) {
        if (file.contains("TestSuite.xml")) {
            return testSuitelXMLBean;
        } else {
            return scriptsXMLBean;

        }

    }

    // FIXME: Need to add Parser Level and XML Level validation
}
