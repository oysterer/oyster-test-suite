/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

/*
    Copyright 2019 @UALR 
    OysterTestSuite for OYSTER Regression Testing
 */
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author 2019 UALR Graduate Study public class OysterTestSuite
 * extends javax.swing.JFrame implements WindowListener { public class
 * OysterTestSuite extends javax.swing.JFrame {
 *
 * public class OysterTestSuite extends javax.swing.JFrame implements
 * WindowListener {
 */
public class OysterTestSuite extends javax.swing.JFrame implements WindowListener {

    //Used for the JTrees' models
    private DefaultTreeModel treeModel;
    private DefaultTreeModel treeModel1;
    private DefaultTreeModel treeModel2;
    private DefaultTreeModel treeModel3;
    //These are special xml textpanes used to have formatting in the jTextPane offshoot.`
    private OtsTextPane newTextPane = new oystertestsuite.OtsTextPane();
    private OtsTextPane newTextPane2 = new oystertestsuite.OtsTextPane();
    //  private OtsTextPane newTextPane3 = new oystertestsuite.OtsTextPane();
    //Carries all the information used in the program for running and comparing results.
    private OysterTestBean oBean = new OysterTestBean();
    private OysterTestBean filesToSaveBean = new OysterTestBean();
    public static String TestSuiteXMLFile;
    private String runScriptName = null;
    //reused over and over for output files
    private String newline = System.getProperty(Constants.LineSeparator);
    //used in the run output from the python/oyster runs
    //to say if it is an Error so record it differently
    private Boolean bool = false;
    //these are used to carry various strings as named for reporting
    private ArrayList<String> NoLinkFileList = new ArrayList<String>();
    private ArrayList<String> NoChangeReportList = new ArrayList<String>();
    private ArrayList<String> RunErrorList = new ArrayList<String>();
    //used to put dates in output files
    DateFormat dateFormat = new SimpleDateFormat(Constants.DateFormatString);
    Date date = new Date();
    //moved file writing to helper from this main form
    private OysterTestSuiteHelper oysterTestSuiteHelper = new OysterTestSuiteHelper();
    private String PythonRunTime;
    //error reporting bean to carry info
    private ErrorsTestBean eTestBean = new ErrorsTestBean();
    //moved here to be able to kill it when needed
    //and if the process hangs on anyones machine    
    private Process process;
    //for chosen single test to run
    public static String RunSingleTest = "";
    //drive keeper for install inititalized
    // public static String Drive = "";
    //used for the popup menu for jtrees
    private TreePopup treePopup4;
    private TreePopup treePopup2;
    private TreePopup treePopup;
    public static boolean isWindows = System.getProperty("os.name").toLowerCase().contains("windows");

    /**
     * Creates new form OysterTestSuite including the programmer added
     * information
     */
    public OysterTestSuite() {

        //must read the TestSuite.xml file first 
        //from the argument passed in to the jar
        ReadTestSuiteXML();

        //This is netbeans init()
        //The IDE creates code backing most of the code added by a programmer
        //and it checks for correctness
        initComponents();

        //use the testsuite.xml information to load the info to screen
        LoadLabels();

        //Losd the OysterTestSuiteBean with knowledge
        //about what is expected from a run for a particular Jar
        ReadAllExpectedValueFiles(this.oBean);

        //Save and reload information about the process
        //when needed so add events
        addWindowListener(this);

        //this checks logs to see if they need renaming 
        //to allow new logs to be created
        oysterTestSuiteHelper.CheckLogFileSizes(this.oBean);

        //This will load the TestSuite.xml file to the 
        //testarea on startup
        LoadEditor(TestSuiteXMLFile, Constants.NewTextPane);
        //after loading it save what it is for use
        filesToSaveBean.setFileLoadedToXMLTextBox(TestSuiteXMLFile);

        //load ERMetrics if textboxes have directories 
        LoadOnStartup();
        // LoadEditor(Constants.TestSuiteXMLFilePath, Constants.NewTextPane);
        // TestSuiteXMLFile
    }

    /*Read the TestSuite.xml file passed in
    *through the main function and when needed.
     */
    public void ReadTestSuiteXML() {

        SuiteXMLParser xmlParser = new SuiteXMLParser();

        if (isWindows) {

            oBean.setTestSuiteWorkspace(TestSuiteXMLFile.substring(0, TestSuiteXMLFile.lastIndexOf("/")));
            //TestSuiteXMLFile was assigned in Main and passed in as an argument
            xmlParser.parse(TestSuiteXMLFile);
            this.oBean = xmlParser.getTestSuitelXMLBean();

        } else {

            oBean.setTestSuiteWorkspace("./");
            //  oBean.setTestSuiteWorkspace(TestSuiteXMLFile.substring(0, TestSuiteXMLFile.lastIndexOf("/")));

            //TestSuiteXMLFile was assigned in Main and passed in as an argument
            xmlParser.parse(TestSuiteXMLFile);
            this.oBean = xmlParser.getTestSuitelXMLBean();
        }

    }

    /**
     * @param args the command line arguments - TestSuite.xml This sets
     * everything in motion. See the OysterTestSuite Constructor above for more
     * OysterTestSuite initialization.
     */
    public static void main(String args[]) {

        //read the argument in ss work before init
        //this argumnet is the testsuite.xml file
        TestSuiteXMLFile = args[0];

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OysterTestSuite().setVisible(true);
            }
        });
    }

    /*
    * Load the labels on the home page on left side
    * with testsuite.xml information
     */
    public void LoadLabels() {

        jLabel1.setText(oBean.getTestSuiteWorkspace().trim());
        jLabel2.setText(oBean.getTestSuiteOysterRoot().trim());
        jLabel3.setText(oBean.getTestSuiteScriptsDir().trim());
        jLabel4.setText(oBean.getTestSuiteOysterToJarRun().trim());
        jLabel9.setText(oBean.getOutPutLogFile().trim());
        jLabel11.setText(oBean.getPythonExe().trim());
        jLabel13.setText(oBean.getPythonFile().trim());
        jLabel34.setText(oBean.getTestSuiteXMLFile().trim());

    }

    /*Make sure the oBean has the information it needs to complete
    *the comparisons of output after run
     */
    public void ReadAllExpectedValueFiles(OysterTestBean oBean) {

        SuiteXMLParser xmlParser;
        ArrayList<String> runfiles;
        ArrayList<OysterTestBean> ExpectedOutputBeanList;

        try {

            ReadTestSuiteXML();
            xmlParser = new SuiteXMLParser();
            runfiles = oBean.getArrayList();
            Integer k = 0;
            ExpectedOutputBeanList = new ArrayList<OysterTestBean>();
            if (this.RunSingleTest.isEmpty() || this.RunSingleTest == null) {
                for (String str : runfiles) {
                    String pathAndFile = oBean.getTestSuiteScriptsDir().replace("\\", "/") + "/" + str;
                    ExpectedOutputBeanList.add(xmlParser.parse(pathAndFile));
                }
            } else {
                String pathAndFile = oBean.getTestSuiteScriptsDir().replace("\\", "/") + "/" + RunSingleTest;
                ExpectedOutputBeanList.add(xmlParser.parse(pathAndFile));
            }

            this.oBean.setExpectedOutputBeanList(ExpectedOutputBeanList);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            runfiles = null;
            xmlParser = null;

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor. Cool!
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTextArea7 = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        ERMRootTxt = new javax.swing.JTextField();
        ERMJarTxt = new javax.swing.JTextField();
        ERMOysterRootTxt = new javax.swing.JTextField();
        OysterDirLinkFileTxt = new javax.swing.JTextField();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTree4 = new javax.swing.JTree();
        jLabel36 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jButton14 = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTextArea6 = new javax.swing.JTextArea();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel28 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        jLabel39 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jLabel18 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTree2 = new javax.swing.JTree();
        jLabel52 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        HomeBtn = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Oyster Test Suite");
        setBackground(new java.awt.Color(10, 170, 107));
        setForeground(new java.awt.Color(228, 255, 242));
        setIconImage(new javax.swing.ImageIcon(getClass().getResource("/UALRICO.jpg")).getImage());
        setLocation(new java.awt.Point(0, 0));
        setLocationByPlatform(true);
        setResizable(false);
        setSize(new java.awt.Dimension(1553, 768));

        jTabbedPane1.setBackground(new java.awt.Color(204, 255, 204));
        jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTabbedPane1.setForeground(new java.awt.Color(153, 0, 51));
        jTabbedPane1.setToolTipText("");
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTabbedPane1.setMaximumSize(new java.awt.Dimension(1533, 641));
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(1533, 641));
        jTabbedPane1.setName("Setup"); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(1533, 641));

        jPanel1.setBackground(new java.awt.Color(228, 255, 242));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setMaximumSize(new java.awt.Dimension(1524, 656));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTree1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTree1.setSelectionModel(null);
        LoadjTree(1);
        jScrollPane3.setViewportView(jTree1);
        /*final TreePopup treePopup = new TreePopup(jTree1);
        MouseListener ml = new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                if(e.isPopupTrigger()) {
                    treePopup.show(e.getComponent(), e.getX(), e.getY());
                }
            };
            public void mousePressed(MouseEvent e) {

                int selRow = jTree1.getRowForLocation(e.getX(), e.getY());
                TreePath selPath = jTree1.getPathForLocation(e.getX(), e.getY());
                if(selRow != -1) {
                    if(e.getClickCount() == 1) {
                        if(e.isPopupTrigger()) {
                            treePopup.show(e.getComponent(), e.getX(), e.getY());
                        }
                    }
                    else if(e.getClickCount() == 2) {
                        if(!selPath.toString().contains(".")){
                            return;}
                        LoadEditor(ParseTreeSelectPath(selPath.toString()),"newTextPane");

                    }
                }
            }
        };
        jTree1.addMouseListener(ml);
        */

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(353, 151, 346, 438));

        jScrollPane6.setMaximumSize(null);
        jScrollPane6.setMinimumSize(null);
        jScrollPane6.setPreferredSize(null);

        oystertestsuite.OtsContextMenu.addOtsContextMenu(newTextPane);
        jScrollPane6.setViewportView(newTextPane);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(717, 151, 780, 438));

        jPanel6.setBackground(new java.awt.Color(55, 128, 74));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel6.setForeground(new java.awt.Color(156, 250, 199));
        jPanel6.setPreferredSize(new java.awt.Dimension(220, 369));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("TestSuiteRootDir");

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(156, 250, 199));
        jLabel5.setText("Test Suite Root Directory");

        jLabel2.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("OysterRootllbl2");

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(156, 250, 199));
        jLabel6.setText("Test Suite Oyster Root Directory");

        jLabel4.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("JarLbl4");

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(156, 250, 199));
        jLabel7.setText("Oyster Jar To Run");

        jLabel3.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("ScriptsLbl3");

        jLabel8.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(155, 250, 199));
        jLabel8.setText("Test Suite Scripts Directory");

        jLabel9.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("LogFileLbl9");
        jLabel9.setName("LogFile"); // NOI18N

        jLabel10.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(155, 250, 199));
        jLabel10.setText("Test Suite Log File");

        jLabel11.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("PythonRootLbl11");

        jLabel12.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(155, 250, 199));
        jLabel12.setText("Python Root Directory");

        jLabel13.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("PythonFileLbl13");

        jLabel14.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(155, 250, 199));
        jLabel14.setText("Python Run File");

        jLabel23.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Test Suite Run Settings");

        jLabel34.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("TestSuiteXMLFileLbl34");

        jLabel35.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(155, 250, 199));
        jLabel35.setText("Test Suite XML File");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11)
                            .addComponent(jLabel13)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel14))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel35))
                            .addComponent(jLabel34))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel23)
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addGap(1, 1, 1)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(4, 4, 4)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(4, 4, 4)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel35)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel9.getAccessibleContext().setAccessibleName("");
        jLabel14.getAccessibleContext().setAccessibleName("");
        jLabel23.getAccessibleContext().setAccessibleName("Test Suite File Information");

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 151, 308, 438));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel15.setText("Double Click a File to View or Edit");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 610, 310, 26));

        jLabel16.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel16.setText("File Loaded is:");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 120, -1, -1));

        jButton5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton5.setText("Refresh");
        jButton5.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButton5.setMaximumSize(new java.awt.Dimension(79, 30));
        jButton5.setMinimumSize(new java.awt.Dimension(70, 30));
        jButton5.setName("Refresh"); // NOI18N
        jButton5.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5SaveActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 610, -1, -1));

        jLabel22.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel22.setText("Working Directory Files");
        jPanel1.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 120, -1, -1));

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ualr2.jpg"))); // NOI18N
        jLabel24.setText("jLabel18");
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1424, 13, 73, -1));

        jLabel17.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 120, -1, -1));

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton6.setText("Save");
        jButton6.setName("Save"); // NOI18N
        jButton6.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6SaveActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 610, -1, -1));

        jButton7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton7.setText("Single");
        jButton7.setMaximumSize(new java.awt.Dimension(72, 23));
        jButton7.setMinimumSize(new java.awt.Dimension(72, 23));
        jButton7.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(719, 610, -1, -1));
        jButton7.setToolTipText("Choose a Single Test to Run.");
        jPanel1.add(jCheckBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 620, -1, -1));
        jCheckBox1.setToolTipText("Check To Run Mulitple Times");

        jScrollPane12.setBorder(null);

        jTextArea7.setBackground(new java.awt.Color(228, 255, 242));
        jTextArea7.setColumns(20);
        jTextArea7.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jTextArea7.setLineWrap(true);
        jTextArea7.setRows(5);
        jTextArea7.setText("The Directories and Files listed below come directly from the Test Suite XML file.  For changes made outside of the Oyster Test Suite program to the Test Suite XML file require the use of the Refresh Button to pick up the changes.");
        jTextArea7.setWrapStyleWord(true);
        jScrollPane12.setViewportView(jTextArea7);

        jPanel1.add(jScrollPane12, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 13, 310, 120));

        jButton2.setText("  Run  ");
        jButton2.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1370, 610, -1, -1));

        jTabbedPane1.addTab("Oyster Test Suite Setup", jPanel1);

        jPanel2.setBackground(new java.awt.Color(228, 255, 242));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setMaximumSize(new java.awt.Dimension(1524, 656));
        jPanel2.setMinimumSize(new java.awt.Dimension(1524, 656));
        jPanel2.setPreferredSize(new java.awt.Dimension(1524, 656));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setToolTipText("");
        jScrollPane1.setName("editArea"); // NOI18N
        jScrollPane1.setPreferredSize(new java.awt.Dimension(268, 104));
        jScrollPane1.setRequestFocusEnabled(false);

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextArea1.setName(""); // NOI18N
        jTextArea1.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(jTextArea1);
        jTextArea1.getAccessibleContext().setAccessibleName("");

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 134, 733, 454));

        jScrollPane2.setPreferredSize(new java.awt.Dimension(268, 104));

        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(5);
        jTextArea2.setWrapStyleWord(true);
        jTextArea2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jScrollPane2.setViewportView(jTextArea2);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(776, 134, 721, 454));

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ualr2.jpg"))); // NOI18N
        jLabel19.setText("jLabel19");
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(1424, 13, 73, -1));

        jLabel20.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel20.setText("Oyster Run Output");
        jPanel2.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(286, 99, -1, -1));

        jLabel21.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel21.setText("Run Issues Output");
        jPanel2.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(1017, 99, -1, -1));

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton3.setText("Stop Run");
        jButton3.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButton3.setMaximumSize(new java.awt.Dimension(80, 30));
        jButton3.setMinimumSize(new java.awt.Dimension(80, 30));
        jButton3.setPreferredSize(new java.awt.Dimension(80, 30));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(348, 606, 91, -1));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton4.setText("History");
        jButton4.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButton4.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1370, 610, -1, -1));

        jTabbedPane1.addTab("Run Output", jPanel2);

        jPanel7.setBackground(new java.awt.Color(239, 255, 253));
        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel7.setMaximumSize(new java.awt.Dimension(1524, 656));
        jPanel7.setMinimumSize(new java.awt.Dimension(1524, 656));
        jPanel7.setPreferredSize(new java.awt.Dimension(1524, 656));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ERMRootTxt.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        ERMRootTxt.setMaximumSize(new java.awt.Dimension(59, 25));
        ERMRootTxt.setMinimumSize(new java.awt.Dimension(59, 25));
        ERMRootTxt.setName("ERMRootTxt"); // NOI18N
        ERMRootTxt.setPreferredSize(new java.awt.Dimension(59, 25));
        ERMRootTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ERMRootTxtKeyTyped(evt);
            }
        });
        jPanel7.add(ERMRootTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 214, 347, -1));

        ERMJarTxt.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        ERMJarTxt.setMaximumSize(new java.awt.Dimension(59, 25));
        ERMJarTxt.setMinimumSize(new java.awt.Dimension(59, 25));
        ERMJarTxt.setName("ERMJarTxt"); // NOI18N
        ERMJarTxt.setPreferredSize(new java.awt.Dimension(59, 25));
        ERMJarTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ERMJarTxtKeyTyped(evt);
            }
        });
        jPanel7.add(ERMJarTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 304, 347, -1));

        ERMOysterRootTxt.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        ERMOysterRootTxt.setToolTipText("");
        ERMOysterRootTxt.setMaximumSize(new java.awt.Dimension(59, 25));
        ERMOysterRootTxt.setMinimumSize(new java.awt.Dimension(59, 25));
        ERMOysterRootTxt.setName("ERMOysterRootTxt"); // NOI18N
        ERMOysterRootTxt.setPreferredSize(new java.awt.Dimension(59, 25));
        ERMOysterRootTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ERMOysterRootTxtKeyTyped(evt);
            }
        });
        jPanel7.add(ERMOysterRootTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 394, 347, -1));

        OysterDirLinkFileTxt.setEditable(false);
        OysterDirLinkFileTxt.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        OysterDirLinkFileTxt.setMaximumSize(new java.awt.Dimension(59, 25));
        OysterDirLinkFileTxt.setMinimumSize(new java.awt.Dimension(59, 25));
        OysterDirLinkFileTxt.setName("OysterDirLinkFileTxt"); // NOI18N
        OysterDirLinkFileTxt.setPreferredSize(new java.awt.Dimension(59, 25));
        jPanel7.add(OysterDirLinkFileTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 484, 347, -1));

        jButton8.setText("Browse");
        jButton8.setActionCommand("");
        jButton8.setMargin(new java.awt.Insets(2, 10, 2, 10));
        jButton8.setMaximumSize(new java.awt.Dimension(79, 30));
        jButton8.setMinimumSize(new java.awt.Dimension(79, 30));
        jButton8.setName(""); // NOI18N
        jButton8.setPreferredSize(new java.awt.Dimension(79, 30));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(386, 212, -1, -1));

        jButton9.setText("Browse");
        jButton9.setMargin(new java.awt.Insets(2, 10, 2, 10));
        jButton9.setMaximumSize(new java.awt.Dimension(79, 30));
        jButton9.setMinimumSize(new java.awt.Dimension(79, 30));
        jButton9.setPreferredSize(new java.awt.Dimension(79, 30));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(386, 302, -1, -1));

        jButton10.setText("Browse");
        jButton10.setMargin(new java.awt.Insets(2, 10, 2, 10));
        jButton10.setMaximumSize(new java.awt.Dimension(79, 30));
        jButton10.setMinimumSize(new java.awt.Dimension(79, 30));
        jButton10.setPreferredSize(new java.awt.Dimension(79, 30));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(386, 392, -1, -1));

        jButton11.setText("Copy");
        jButton11.setName("Copy"); // NOI18N
        jButton11.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(386, 482, -1, -1));

        jButton12.setText("Run");
        jButton12.setName("Run"); // NOI18N
        jButton12.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1370, 610, -1, -1));

        jButton13.setText("Save");
        jButton13.setToolTipText("");
        jButton13.setName(""); // NOI18N
        jButton13.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton13, new org.netbeans.lib.awtextra.AbsoluteConstraints(386, 569, -1, -1));

        jTree4.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("ERMetrics");
        jTree4.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        /*MouseListener mn = new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                if(e.isPopupTrigger()) {
                    treePopup4.show(e.getComponent(), e.getX(), e.getY());
                }
            };
            public void mousePressed(MouseEvent e) {
                int selRow = jTree4.getRowForLocation(e.getX(), e.getY());
                TreePath selPath = jTree4.getPathForLocation(e.getX(), e.getY());
                if(selRow != -1) {
                    if(e.getClickCount() == 1) {

                    }
                    else if(e.getClickCount() == 2) {
                        if(!selPath.toString().contains(".")){
                            return;}
                        LoadEditor(ParseTreeSelectPath(selPath.toString()),"jTextArea5");

                    }
                }
            }
        };
        jTree4.addMouseListener(mn);*/
        jScrollPane8.setViewportView(jTree4);

        jPanel7.add(jScrollPane8, new org.netbeans.lib.awtextra.AbsoluteConstraints(473, 162, 330, 437));

        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ualr2.jpg"))); // NOI18N
        jLabel36.setText("jLabel18");
        jPanel7.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(1424, 13, 73, -1));

        jLabel40.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel40.setText("File Loaded is:");
        jPanel7.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 137, -1, -1));

        jLabel42.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel42.setText("Working Directory Files");
        jPanel7.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 137, -1, -1));

        jLabel43.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel43.setText("Double Click a File to View or Edit");
        jPanel7.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 610, 310, -1));

        jButton14.setText("Save");
        jButton14.setToolTipText("");
        jButton14.setMaximumSize(new java.awt.Dimension(70, 30));
        jButton14.setMinimumSize(new java.awt.Dimension(70, 30));
        jButton14.setName(""); // NOI18N
        jButton14.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 610, -1, -1));

        jLabel44.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel44.setText("ERMetrics Working Directory");
        jPanel7.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 187, -1, -1));

        jLabel45.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel45.setText("ERMetrics Jar File");
        jPanel7.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 277, -1, -1));

        jLabel46.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel46.setText("Oyster Working Directory");
        jPanel7.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 367, -1, -1));

        jLabel47.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel47.setText("Oyster Link File From Last Run");
        jPanel7.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 457, -1, -1));

        jScrollPane13.setBorder(null);

        jTextArea6.setBackground(new java.awt.Color(239, 255, 253));
        jTextArea6.setColumns(20);
        jTextArea6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jTextArea6.setRows(5);
        jTextArea6.setText("1. Browse to the ERMetrics Work Directory, ERMetrics Jar and Oyster Work Directory.  Save your choices. \n2. Copy the loaded last Run Link file to the ERMetrics Working Directory from the Oyster Working Directory.   \n3. Change the properties file of ERMetrics to run the new Link File and use a Truth or Reference file.\n4. Run ERMetrics.");
        jTextArea6.setBorder(null);
        jTextArea6.setMaximumSize(new java.awt.Dimension(707, 90));
        jScrollPane13.setViewportView(jTextArea6);

        jPanel7.add(jScrollPane13, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 28, 970, 98));

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(153, 0, 51));
        jLabel48.setText("Example: C:\\OysterTestSuite\\Oyster\\ER_Calculator");
        jPanel7.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 244, -1, -1));

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(153, 0, 51));
        jLabel49.setText("Example: C:\\OysterTestSuite\\Oyster\\ER_Calculator\\er-metrics.jar");
        jPanel7.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 334, -1, -1));

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(153, 0, 51));
        jLabel50.setText("Example: C:\\OysterTestSuite\\Oyster\\ListABC");
        jPanel7.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 424, -1, -1));

        jLabel51.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(153, 0, 51));
        jLabel51.setText("Link File Date:");
        jPanel7.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 518, -1, -1));

        jScrollPane14.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTextArea5.setColumns(20);
        jTextArea5.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jTextArea5.setRows(5);
        jTextArea5.setBorder(null);
        jTextArea5.setMaximumSize(new java.awt.Dimension(260, 95));
        oystertestsuite.OtsContextMenuER.addOtsContextMenu(jTextArea5);
        jScrollPane14.setViewportView(jTextArea5);

        jPanel7.add(jScrollPane14, new org.netbeans.lib.awtextra.AbsoluteConstraints(817, 162, 680, 437));

        jTabbedPane1.addTab("ERMetrics Setup", jPanel7);

        jPanel4.setBackground(new java.awt.Color(55, 128, 74));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel4.setMaximumSize(new java.awt.Dimension(1524, 656));
        jPanel4.setMinimumSize(new java.awt.Dimension(1524, 656));
        jPanel4.setPreferredSize(new java.awt.Dimension(1524, 656));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jScrollPane9.setForeground(new java.awt.Color(136, 5, 5));
        jScrollPane9.setViewportBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jScrollPane9.setMaximumSize(new java.awt.Dimension(462, 412));
        jScrollPane9.setMinimumSize(new java.awt.Dimension(462, 412));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setModel(new DefaultTableModel());
        jScrollPane9.setViewportView(jTable1);

        jPanel4.add(jScrollPane9, new org.netbeans.lib.awtextra.AbsoluteConstraints(607, 115, 890, 504));

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ualr2.jpg"))); // NOI18N
        jLabel28.setText("jLabel18");
        jPanel4.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(1424, 13, 73, -1));

        jPanel5.setBackground(new java.awt.Color(228, 255, 242));
        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel5.setMaximumSize(new java.awt.Dimension(542, 504));
        jPanel5.setMinimumSize(new java.awt.Dimension(542, 504));

        jLabel31.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(55, 128, 74));
        jLabel31.setText("Run Date Time:");

        jLabel32.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(55, 128, 74));
        jLabel32.setText("Number of Tests:");

        jLabel37.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(55, 128, 74));
        jLabel37.setText("Passed:");

        jLabel38.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(55, 128, 74));
        jLabel38.setText("Failed:");

        jLabel41.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(55, 128, 74));
        jLabel41.setText("Double Click a Failed Test");

        jList1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jList1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jList1MouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(jList1);

        jScrollPane10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTextArea4.setColumns(20);
        jTextArea4.setLineWrap(true);
        jTextArea4.setRows(5);
        jTextArea4.setWrapStyleWord(true);
        jScrollPane10.setViewportView(jTextArea4);

        jLabel39.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(55, 128, 74));
        jLabel39.setText("Jar:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel41))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel37)
                            .addComponent(jLabel38)
                            .addComponent(jLabel39)
                            .addComponent(jLabel32)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel31))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 532, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(jLabel32)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel37)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel38)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel39)
                        .addGap(41, 41, 41)))
                .addComponent(jLabel41)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 115, 560, -1));

        jLabel29.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("Oyster Test Suite Run History");
        jPanel4.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(887, 82, -1, -1));

        jLabel30.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("Last Run Information");
        jPanel4.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(237, 82, -1, -1));

        jTabbedPane1.addTab("Test Suite Run History", jPanel4);

        jPanel3.setBackground(new java.awt.Color(228, 255, 242));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel3.setMaximumSize(new java.awt.Dimension(1524, 656));
        jPanel3.setMinimumSize(new java.awt.Dimension(1524, 656));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ualr2.jpg"))); // NOI18N
        jLabel25.setText("jLabel18");
        jPanel3.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(1424, 13, 73, -1));

        jScrollPane7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jScrollPane7.setMaximumSize(new java.awt.Dimension(873, 425));
        jScrollPane7.setMinimumSize(new java.awt.Dimension(873, 425));
        jScrollPane7.setPreferredSize(new java.awt.Dimension(873, 425));
        jPanel3.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(513, 119, 984, 461));
        oystertestsuite.OtsContextMenu.addOtsContextMenu(newTextPane2);
        jScrollPane7.setViewportView(newTextPane2);

        jLabel18.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel18.setText("Loaded File is:");
        jPanel3.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(513, 89, -1, -1));

        jLabel26.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel26.setText(" ");
        jPanel3.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(631, 89, -1, -1));

        jLabel27.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel27.setText("Run Output Files");
        jPanel3.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 90, -1, -1));

        jScrollPane5.setMaximumSize(new java.awt.Dimension(373, 450));
        jScrollPane5.setMinimumSize(new java.awt.Dimension(374, 364));
        jScrollPane5.setPreferredSize(new java.awt.Dimension(373, 373));

        jTree2.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTree2.setSelectionModel(null);
        LoadjTree(2);
        jScrollPane5.setViewportView(jTree2);
        MouseListener ml2 = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow2 = jTree2.getRowForLocation(e.getX(), e.getY());
                TreePath selPath2 = jTree2.getPathForLocation(e.getX(), e.getY());
                if(selRow2 != -1) {
                    if(e.getClickCount() == 1) {
                        //wait for two clicks
                    }
                    else if(e.getClickCount() == 2) {
                        if(!selPath2.toString().contains(".")){
                            return;}
                        LoadEditor(ParseTreeSelectPath(selPath2.toString()),"newTextPane2");

                    }
                }
            }
        };
        jTree2.addMouseListener(ml2);

        jPanel3.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 119, 420, 461));

        jLabel52.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jLabel52.setText("Double Click a File to View or Edit");
        jPanel3.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 590, 330, 26));

        jTabbedPane1.addTab("Test Output Files", jPanel3);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setText("Exit");
        jButton1.setPreferredSize(new java.awt.Dimension(78, 30));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        HomeBtn.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        HomeBtn.setText("Home");
        HomeBtn.setMaximumSize(new java.awt.Dimension(70, 30));
        HomeBtn.setMinimumSize(new java.awt.Dimension(70, 30));
        HomeBtn.setPreferredSize(new java.awt.Dimension(78, 30));
        HomeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HomeBtnActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Arial Narrow", 3, 24)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(153, 0, 51));
        jLabel33.setText("OysterTestSuite");

        jMenuBar1.setBackground(new java.awt.Color(55, 128, 74));
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));

        jMenu1.setForeground(new java.awt.Color(255, 255, 255));
        jMenu1.setText("  File  ");
        jMenu1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                none(evt);
            }
        });

        jMenuItem1.setText("Exit");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Change OTS Directory");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setForeground(new java.awt.Color(255, 255, 255));
        jMenu2.setText("About");
        jMenu2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jMenu2.setName("Exit"); // NOI18N
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(HomeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(94, 94, 94)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(93, 93, 93))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 691, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(HomeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("");
        jButton1.getAccessibleContext().setAccessibleDescription("");

        setSize(new java.awt.Dimension(1569, 811));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    //Exit clicked on menu
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //we send this because we want the data table to be written out when
        //the program closes.
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));

    }//GEN-LAST:event_jButton1ActionPerformed

    private void none(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_none
        //we send this because we want the data table to be written out when
        //the program closes.

        //  System.exit(0);
    }//GEN-LAST:event_none

//About clicked
    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
//        //show ABOUT when clicked
        AboutDialog about = new AboutDialog(this, true);
        about.setAlwaysOnTop(true);
        about.setVisible(true);

    }//GEN-LAST:event_jMenu2MouseClicked

//Go to setup tab
    private void HomeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HomeBtnActionPerformed
        jTabbedPane1.setSelectedIndex(0);
    }//GEN-LAST:event_HomeBtnActionPerformed

    /*
    Click a failed test for the last run on the History Tab and load 
    information about the test when it occured. 
     */
    private void jList1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jList1MouseClicked

        JList list = (JList) evt.getSource();

        if (evt.getClickCount() == 2) {
            int index = list.locationToIndex(evt.getPoint());
            if (index >= 0) {
                Object o = list.getModel().getElementAt(index);

                if (o != null) {

                    for (ErrorsTestBean eBean : eTestBean.getErrorOutputBeanList()) {

                        if (eBean.getTestBeanName().equalsIgnoreCase(o.toString())) {
                            jLabel31.setText("Run Date Time: " + eBean.getDateTime());
                            jLabel32.setText("Tests Run: " + eBean.getTestsRunCount());
                            jLabel37.setText("Passed: " + (Integer.parseInt(eBean.getTestsRunCount()) - this.RunErrorList.size()));
                            jLabel38.setText("Failed: " + Integer.toString(this.RunErrorList.size()));
                            jLabel39.setText("Jar: " + eBean.getJarUsedInRun());

                            jTextArea4.setText(null);
                            jTextArea4.append(eBean.getTestBeanName() + newline);
                            jTextArea4.append(newline);
                            //it may be an ERROR from the run which will
                            //be in another bean within a hashmap.
                            boolean write = true;
                            for (String str : eBean.getList()) {
                                jTextArea4.append(str + newline);
                                //output is here so write is false
                                write = false;
                            }
                            if (write) {
                                //write true because the data is in the hashmap
                                Map<String, List<String>> map = this.eTestBean.getMap();
                                if (map.get(o.toString()) != null) {
                                    List<String> listA = map.get(o.toString());
                                    for (String str : listA) {
                                        jTextArea4.append(str.trim());
                                        jTextArea4.append(newline);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_jList1MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jTabbedPane1.setSelectedIndex(3);        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    //Stop the python and java process
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        //  if (process != null) {
        try {
            int input = 0;

            if (isWindows) {
                JFrame messageFrame = new JFrame(Constants.StopJava);
                input = JOptionPane.showConfirmDialog(messageFrame, Constants.KillJava);
            } else {
                JFrame messageFrame = new JFrame(Constants.StopJava);
                input = JOptionPane.showConfirmDialog(messageFrame, Constants.KillPython);
            }

            if (input == 0) {
                if (isWindows) {
                    //kill python because it is private but local
                    process.destroy();
                    // To kill a command prompt
                    String processName = "java.exe";

                    //checks for any windows pid but Oystertestsuire's pid
                    stopRunningProcess(processName);

                } else {

                    String cmd1 = "ps -C python3.7";
                    String cmd2 = "jps -lV";

                    ProcessBuilder builder1 = new ProcessBuilder();
                    ProcessBuilder builder2 = new ProcessBuilder();
                    ProcessBuilder builder3 = new ProcessBuilder();
                    ProcessBuilder builder4 = new ProcessBuilder();

                    builder1.command("sh", "-c", cmd1);
                    Process p = builder1.start();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;
                    String Pythonpid = "";
                    while ((line = reader.readLine()) != null) {
                        if (!line.contains("PID")) {

                            Pythonpid = line.substring(0, line.indexOf("tty")).trim();
                            break;
                        }
                    }
                    if (!Pythonpid.isEmpty()) {
                        String cmd = "kill -9 " + Pythonpid;
                        builder2.command("sh", "-c", cmd);
                        builder2.start();
                        Pythonpid = "";
                    } else {
                        System.out.println("Python pid not found");
                    }

                    builder3.command("sh", "-c", cmd2);
                    Process p1 = builder3.start();
                    BufferedReader reader1 = new BufferedReader(new InputStreamReader(p1.getInputStream()));

                    String line1 = null;
                    String jarPid = "";
                    while ((line1 = reader1.readLine()) != null) {

                        if (line1.contains(oBean.getTestSuiteOysterToJarRun())) {
                            jarPid = line1.substring(0, line1.indexOf("oyster")).trim();
                            break;
                        }
                    }

                    if (!jarPid.isEmpty()) {
                        String cmd = "kill -9 " + jarPid;
                        builder4.command("sh", "-c", cmd);
                        builder4.start();
                        jarPid = "";
                    } else {
                        System.out.println("Jar pid not found");
                    }

                }
            }

        } catch (Exception e) {
            System.out.println("Java or Python Process Kill Error! " + e.getMessage());
        }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        //show singlechoice dialog
        SingleDialog SChoice = new SingleDialog(this, true);
        SChoice.setVisible(true);

    }//GEN-LAST:event_jButton7ActionPerformed

    /*
        Saves the loaded file from the save button on OysterTestSuite Setup Tab
        and refreshes the view.
     */
    private void jButton6SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6SaveActionPerformed
        try {

            if (!filesToSaveBean.getFileLoadedToXMLTextBox().isEmpty() || filesToSaveBean.getFileLoadedToXMLTextBox() != null) {
                boolean worked = SaveFile(filesToSaveBean.getFileLoadedToXMLTextBox(), 1);
                if (worked) {
                    JOptionPane.showMessageDialog(null, Constants.LoadedFileSaved);
                } else {
                    JOptionPane.showMessageDialog(null, Constants.LoadedFileNotSaved);
                }
            } else {
                JOptionPane.showMessageDialog(null, "File Not Available to Save!");
            }

            ReadTestSuiteXML();
            LoadLabels();
            jPanel6.repaint();

        } catch (Exception e) {
            System.out.println("Save File Failed! " + e.getMessage());
        }

    }//GEN-LAST:event_jButton6SaveActionPerformed

    /* 
        Allows the Refresh button to re-read the TestSuite.xml
        after changing anything ?? Jar version
     */
    private void jButton5SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5SaveActionPerformed

        //refresh the Screen
        ReadTestSuiteXML();
        LoadLabels();
        jPanel6.repaint();

    }//GEN-LAST:event_jButton5SaveActionPerformed

    /*
        Saves the file loaded to the ERMetrics Textpane
     */
    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed

        try {
            if (filesToSaveBean.getFileLoadedToXMLTextBox3() != null && !filesToSaveBean.getFileLoadedToXMLTextBox3().isEmpty()) {
                if (jTextArea5.getLineCount() > 0 && jLabel40.getText().length() > 16) {
                    boolean worked = SaveFile(filesToSaveBean.getFileLoadedToXMLTextBox3(), 3);
                    if (worked) {
                        JOptionPane.showMessageDialog(null, Constants.LoadedFileSaved);
                    } else {
                        JOptionPane.showMessageDialog(null, Constants.LoadedFileNotSaved);
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "File Not Available to Save!");
                }

            }
        } catch (Exception e) {
            System.out.println("Saving File failed! " + e.getMessage());
        }
    }//GEN-LAST:event_jButton14ActionPerformed

    /*
    * If loading the ERMetrics test boxes is complete, this outputs the 
    * config.xml file to store the choices for reload on startup
     */
    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed

        Map<String, String> ERMetricsText = new HashMap<>();

        try {
            if (ERMRootTxt.getText().trim().isEmpty() || ERMJarTxt.getText().trim().isEmpty() || ERMOysterRootTxt.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null, Constants.ThreeBoxesFilled);
            } else {
                ProcessClass p = new ProcessClass();
                ERMetricsText.put(Constants.ERMetricsRootDir, ERMRootTxt.getText().trim());
                ERMetricsText.put(Constants.ERMetricsJar, ERMJarTxt.getText().trim());
                ERMetricsText.put(Constants.ERMetricsOysterDir, ERMOysterRootTxt.getText().trim());
                p.WriteToProp(ERMetricsText);

                p = null;
                LoadjTree(3);
                JOptionPane.showMessageDialog(null, "Your Input is Saved!");
            }
        } catch (Exception e) {
            System.out.println("Error Saving ERMetrics Paths " + e.getLocalizedMessage());
        }

    }//GEN-LAST:event_jButton13ActionPerformed

    /*
        Runs ERMetrics
     */
    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        jTextArea5.setText("");
        jLabel40.setText("Loaded Rile is: ERMetrics Run");
        RunTestsERMetrics();
    }//GEN-LAST:event_jButton12ActionPerformed

    /*
        After running the link file will be loaded in the ERMetrics Tab and 
        this copies it over to the ERMetrics directory for use.
        A real time saver.
     */
    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed

        String Csource = "";

        try {
            if (OysterDirLinkFileTxt.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Link File is not Loaded!");
            } else {

                Csource = OysterDirLinkFileTxt.getText().replace("\\", "/");

                String CDest = ERMRootTxt.getText() + OysterDirLinkFileTxt.getText().substring(OysterDirLinkFileTxt.getText().lastIndexOf("/"));
                CDest = CDest.replace("\\", "/");

                File dest = new File(CDest);

                File source = new File(Csource);
                if (source.length() < 3) {
                    JOptionPane.showMessageDialog(null, "Link File is Empty! Run Oyster Again!");
                } else {

                    Files.copy(source.toPath(), dest.toPath(), REPLACE_EXISTING);
                    JOptionPane.showMessageDialog(null, "Link File was Copied!");

                }

            }

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Copy link file error is " + e.getMessage());
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    /*
        Loads the ERMetricsOysterRoot text Box
     */
    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed

        Boolean SetPath = Browse(oBean.getTestSuiteOysterRoot(), Constants.Directory, ERMOysterRootTxt);
        if (SetPath == true) {
            if (ERMRootTxt.getText().trim().isEmpty() || ERMJarTxt.getText().trim().isEmpty() || ERMOysterRootTxt.getText().trim().isEmpty()) {

            } else {

                JOptionPane.showMessageDialog(null, "Save your Choices!");

            }
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    /*
        Loads the ERMJarTxt text Box
     */
    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed

        if (!ERMRootTxt.getText().isEmpty()) {
            Browse(ERMRootTxt.getText(), Constants.File, ERMJarTxt);
        } else {
            Browse(oBean.getTestSuiteOysterRoot(), Constants.File, ERMJarTxt);
        }

    }//GEN-LAST:event_jButton9ActionPerformed

    /*
        Loads the ERMRootTxt text Box
     */
    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed

        Browse(oBean.getTestSuiteOysterRoot(), Constants.Directory, ERMRootTxt);

    }//GEN-LAST:event_jButton8ActionPerformed

    /*
    * Used to make sure the user uses the Browse button
     */
    private void ERMRootTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ERMRootTxtKeyTyped

        JOptionPane.showMessageDialog(null, Constants.BrowseButton);
        ProcessClass p = new ProcessClass();
        ERMRootTxt.setText(p.ReadAProperty(Constants.ERMetricsRootDir));
        p = null;

    }//GEN-LAST:event_ERMRootTxtKeyTyped

    /*
    * Used to make sure the user uses the Browse button
     */
    private void ERMJarTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ERMJarTxtKeyTyped
        JOptionPane.showMessageDialog(null, Constants.BrowseButton);
        ProcessClass p = new ProcessClass();
        ERMJarTxt.setText(p.ReadAProperty(Constants.ERMetricsJar));
        p = null;
    }//GEN-LAST:event_ERMJarTxtKeyTyped

    /*
    * Used to make sure the user uses the Browse button
     */
    private void ERMOysterRootTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ERMOysterRootTxtKeyTyped
        JOptionPane.showMessageDialog(null, Constants.BrowseButton);
        ProcessClass p = new ProcessClass();
        ERMOysterRootTxt.setText(p.ReadAProperty(Constants.ERMetricsOysterDir));
        p = null;
    }//GEN-LAST:event_ERMOysterRootTxtKeyTyped

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        Browse("./", Constants.Directory, null);

        if (this.TestSuiteXMLFile != null) {

            ReadTestSuiteXML();
            LoadLabels();
            LoadEditor(TestSuiteXMLFile, Constants.NewTextPane);

        }

    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {

            //to pick singleTestCase need to run this now
            ReadAllExpectedValueFiles(this.oBean);

            //go to run tab and run the tests
            jTabbedPane1.setSelectedIndex(1);

            //time to run again with fresh numbers
            LogWriter TestSuiteLog = new LogWriter(oBean.getOutPutLogFile());
            oysterTestSuiteHelper.GetLogsReadyToGo(jTextArea1, jTextArea2, TestSuiteLog, oBean);

//        //reinitialize the RunnErrorList
            this.RunErrorList = new ArrayList<String>();

            this.eTestBean.getMap().clear();

            //reset the data list for the JList so old data is gone
            this.jList1.setListData(new String[0]);

            //clear jTestArea4
            jTextArea4.setText(null);

            //run the process
        } catch (Exception e) {
            System.out.println("Run button Error is " + e.getMessage());
        }

        RunTests();

    }//GEN-LAST:event_jButton2ActionPerformed

    /*
        This will stop the java and python process in case the user
        cannot get it to run.  It will leave the OysterTestSuite program 
        running.
     */
    public void stopRunningProcess(String serviceName) {

        try {
            Process pro = Runtime.getRuntime().exec(Constants.TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(pro.getInputStream()));
            String line;
            String name = ManagementFactory.getRuntimeMXBean().getName();
            int loc = name.indexOf("@");
            name = name.substring(0, loc);
            while ((line = reader.readLine()) != null) {

                if (line.startsWith(serviceName) || line.startsWith("Oyster Test Suite")) {

                    if (!line.contains(name)) {

                        int loc2 = line.indexOf("Console");
                        line = line.substring(loc2 - 6, loc2 - 1).trim();
                        Runtime.getRuntime().exec(Constants.KILL + line + "");

                    }

                }
            }

        } catch (IOException e) {
            System.out.println("Error killing Java Process!");
        }

    }

    /*
        This is where all the Output Checks Start and Finish.
        The bean representing a test directory/test xml file in the array list are sent 
        through three methods one at a time to check output to expected output..
        and the data is written to various files.
     */
    public void CheckRunOutputLogAndLink() {

        ArrayList<OysterTestBean> ExpectedOutputBeanList = new ArrayList<OysterTestBean>();
        LogWriter TestSuiteLog = new LogWriter(oBean.getOutPutLogFile());
        LogWriter ClusterRef;
        this.NoLinkFileList = new ArrayList<String>();
        this.NoChangeReportList = new ArrayList<String>();

        try {

            //clearing this 
            ExpectedOutputBeanList = oBean.getExpectedOutputBeanList();
            if (ExpectedOutputBeanList == null || ExpectedOutputBeanList.isEmpty()) {

                ReadAllExpectedValueFiles(this.oBean);
                ExpectedOutputBeanList = oBean.getExpectedOutputBeanList();
            }

            for (OysterTestBean myBean : ExpectedOutputBeanList) {

                Integer in = 0;
                ErrorsTestBean errorsTestBean = new ErrorsTestBean();
                errorsTestBean.setTestsRunCount(Integer.toString(oBean.getArrayList().size()));
                errorsTestBean.setTestBeanName(myBean.getTestCase_Name());
                errorsTestBean.setJarUsedInRun(oBean.getTestSuiteOysterToJarRun());
                errorsTestBean.setDateTime(dateFormat.format(new Date()));

                ClusterRef = new LogWriter(myBean, "ClusterRef", oBean.getTestSuiteOysterRoot());
                ClusterRef.WriteToClusterRefReport(newline + myBean.getTestCase_Name());
                ClusterRef.WriteToClusterRefReport("Run Date is " + dateFormat.format(new Date()));
                ClusterRef.WriteToClusterRefReport(oBean.getTestSuiteOysterToJarRun() + newline);
                String fileName = "";

                if (isWindows) {
                    fileName = oBean.getTestSuiteOysterRoot() + "/" + myBean.getTestCase_Name() + Constants.OUTPUT + myBean.getTestCase_Name() + "Log_0.log";
                } else {
                    fileName = oBean.getTestSuiteOysterRoot() + "/" + myBean.getTestCase_Name() + "/Output/" + myBean.getTestCase_Name() + "Log_0.log";
                }

                fileName.replace("\\", "/");

                File file = new File(fileName);

                if (file.exists()) {

                    Scanner input = new Scanner(file);

                    while (input.hasNextLine()) {
                        String line = input.nextLine().replace(",", "");
                        if (line.contains("Total Records Processed")) {
                            line = line.substring(line.indexOf(":") + 1).trim();

                            ClusterRef.WriteToClusterRefReport("Total Log References Expected    " + myBean.getLogRefExpectedCount());
                            ClusterRef.WriteToClusterRefReport("Total Log Clusters Created Expected    " + myBean.getLogClusterExpectedCount());
                            ClusterRef.WriteToClusterRefReport("Total Link References Expected    " + myBean.getLinkRefExpectedCount());
                            ClusterRef.WriteToClusterRefReport("Total Link Clusters Expected    " + myBean.getLinkClusterExpectedCount());

                            ClusterRef.WriteToClusterRefReport(newline + "From Log File " + fileName + newline);
                            ClusterRef.WriteToClusterRefReport("Total Records Processed    " + Integer.parseInt(line));

                            if (Integer.parseInt(line) != myBean.getLogRefExpectedCount()) {
                                errorsTestBean.getList().add("Log File Total References Processed " + line + "\nExpected Log References " + myBean.getLogRefExpectedCount());
                                jTextArea2.append(newline);
                                if (in == 0) {
                                    RunErrorList.add(myBean.getTestCase_Name());
                                    oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);
                                    in = 1;
                                }
                                oysterTestSuiteHelper.WriteFooter("Total Records Processed", oBean, myBean, TestSuiteLog, jTextArea2, line);
//                             
                            }

                        } else if (line.contains("Total Clusters")) {
                            line = line.substring(line.indexOf(":") + 1).trim();

                            ClusterRef.WriteToClusterRefReport("Total Clusters    " + line);
                            if (Integer.parseInt(line) != myBean.getLogClusterExpectedCount()) {
                                errorsTestBean.getList().add("Log File Total Clusters " + line + "\nExpected Log Clusters " + myBean.getLogClusterExpectedCount());
                                if (in == 0) {
                                    RunErrorList.add(myBean.getTestCase_Name());
                                    oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);
                                    in = 1;
                                }
                                oysterTestSuiteHelper.WriteFooter("Total Clusters", null, myBean, TestSuiteLog, jTextArea2, line);
//                              
                            }
                        }

                    }

                    input.close();
                    input = null;
                    ClusterRef.Flush();
                    ClusterRef.Close();
                    ClusterRef = null;

                    //the in variable count has to due with writing header or not
                    //0 write it, 1 already written header.  This works because 
                    //we are sending one bean through at a time to all checks after run
                    if (CheckLinkFileOutput(myBean, in, errorsTestBean) == 0) {
                        CheckChangeReports(myBean, 0, errorsTestBean);
                    } else {
                        CheckChangeReports(myBean, 1, errorsTestBean);
                    }

                }

            }

            jTextArea2.append(newline);
            TestSuiteLog.WriteToLog(newline);

            FillTable(RunErrorList.size(), NoLinkFileList.size(), NoChangeReportList.size());
            String[] listData = new String[oBean.getArrayList().size()];
            Integer listCount = 0;
            if (!RunErrorList.isEmpty()) {
                Iterator itr = RunErrorList.iterator();

                while (itr.hasNext()) {
                    String ToWrite = itr.next().toString().intern();
//                    if (!CheckOutput.equalsIgnoreCase(ToWrite)) {
                    //jTextArea4.append(ToWrite + newline);
                    listData[listCount] = ToWrite;
                    listCount += 1;
                }
                jList1.setListData(listData);

            }
            if (!NoLinkFileList.isEmpty()) {
                jTextArea2.append(newline);
                Iterator itr = NoLinkFileList.iterator();
                jTextArea2.append("Link File(s) Not Found for the TestCase(s)" + newline);
                TestSuiteLog.WriteToLog("Link File(s) Not Found for the TestCase(s)");

                while (itr.hasNext()) {
                    String ToWrite = itr.next().toString().intern();
                    jTextArea2.append(ToWrite + newline);
                    TestSuiteLog.WriteToLog(ToWrite);

                }

                NoLinkFileList.clear();
                NoLinkFileList = null;
                jTextArea2.append(newline);
                TestSuiteLog.WriteToLog(newline);

            }

            if (!NoChangeReportList.isEmpty()) {
                jTextArea2.append(newline);
                Iterator itrN = NoChangeReportList.iterator();
                jTextArea2.append("Change Report(s) Not Found for the TestCase(s)" + newline);
                TestSuiteLog.WriteToLog("Change Report(s) Not Found for the TestCase(s)");

                while (itrN.hasNext()) {
                    String ToWrite = itrN.next().toString().intern();
                    jTextArea2.append(ToWrite + newline);
                    TestSuiteLog.WriteToLog(ToWrite);

                }

                jTextArea2.append(newline);
                NoChangeReportList.clear();
                NoChangeReportList = null;
            }
//            //run is over so say so
            oysterTestSuiteHelper.WriteEndOfPythonRun(TestSuiteLog, jTextArea2);
            //close log writers
            oysterTestSuiteHelper.CloseWriters(TestSuiteLog, null);
            //save the history table after run
            WriteTableOut(this.jTable1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            //clear singletest if needed
            if (!jCheckBox1.isSelected()) {
                if (oBean.getArrayList().contains(RunSingleTest)) {
                    oBean.getArrayList().remove(RunSingleTest);
                }

                this.RunSingleTest = "";
            }
            System.gc();

        }

    }

    private boolean SaveFile(String FileName, Integer i) {

        if (FileName.isEmpty() || FileName == null) {
            JOptionPane jOptionPane = new JOptionPane();
            JFrame messageFrame = new JFrame("File to Save is Null!");
            jOptionPane.showMessageDialog(messageFrame, "Reload you file and try to Save Again!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));

        } else {
            FileName.replace("\\", "/");

            if (!isWindows) {

                if (FileName.startsWith("./OysterTestSuite") || FileName.startsWith("/OysterTestSuite")) {
                    FileName = "." + FileName.substring(FileName.lastIndexOf("/"));

                }
            }

            final String fileName = FileName;

//            AccessControlContext acc = AccessController.getContext();
//
//            AccessController.doPrivileged(new PrivilegedAction<Void>() {
//                public Void run() {
            try {
                File file = new File(fileName);
                try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file),
                        StandardCharsets.UTF_8)) {

                    if (i == 1) {
                        out.write(newTextPane.getText());
                        filesToSaveBean.setFileLoadedToXMLTextBox(fileName);
                    } else if (i == 2) {
                        out.write(newTextPane2.getText());
                        filesToSaveBean.setFileLoadedToXMLTextBox2(fileName);
                    } else if (i == 3) {
                        out.write(jTextArea5.getText());
                        filesToSaveBean.setFileLoadedToXMLTextBox3(fileName);
                    }
                    out.close();
                }
            } catch (Exception iOException) {

                iOException.printStackTrace();
                return false;
            }

        }
        return true;
//
//            }, acc);
//        }
    }

    //Now check the Link File for expexted vs output
    public Integer CheckLinkFileOutput(OysterTestBean tBean, Integer in, ErrorsTestBean errorsTestBean) {

        File file;
        Scanner input;
        LogWriter TestSuiteLog = new LogWriter(oBean.getOutPutLogFile());
        LogWriter ClusterRef = new LogWriter(tBean, "ClusterRef", oBean.getTestSuiteOysterRoot());

        try {

            Set<String> _values = new HashSet<>();

            String fileName = oBean.getTestSuiteOysterRoot() + "/" + tBean.getTestCase_Name() + Constants.OUTPUT + tBean.getOysterLinkFile();
            fileName = fileName.replace("\\", "/");
            file = new File(fileName);
            int counter = 0;
            if (file.exists()) {

                input = new Scanner(file);
                while (input.hasNextLine()) {

                    String line = input.nextLine();
                    line = line.substring(line.indexOf("\t"));
                    line = line.trim();
                    String s = line.substring(0, line.indexOf("\t"));
                    s = s.trim();

                    if (!s.equalsIgnoreCase("OysterID")) {
                        counter = counter + 1;
                        if (!_values.contains(s) && !s.contains("XXXXXXXXXXXXXXXX")) {

                            _values.add(s);
                        }
                    }
                }

                ClusterRef.WriteToClusterRefReport(newline + "From Link File " + fileName + newline);
                ClusterRef.WriteToClusterRefReport("Link File Reference Count is    " + counter);
                ClusterRef.WriteToClusterRefReport("Link File Cluster Count is    " + _values.size());

                tBean.setLinkReferenceCount(counter);
                tBean.setLinkClusterCount(_values.size());
                String counterstr = Integer.toString(counter);

                if (counter != tBean.getLinkRefExpectedCount()) {
                    errorsTestBean.getList().add("Link File Total References " + counter + "\nLink File Expected References " + tBean.getLinkRefExpectedCount());
                    jTextArea2.append(newline);
                    if (in == 0) {
                        RunErrorList.add(tBean.getTestCase_Name());
                        oysterTestSuiteHelper.WriteHeader(oBean, tBean, TestSuiteLog, jTextArea2);
                        in = 1;
                    }

                    oysterTestSuiteHelper.WriteFooter("Link File Reference Count", oBean, tBean, TestSuiteLog, jTextArea2, counterstr);

                }

                if (_values.size() != tBean.getLinkClusterExpectedCount()) {
                    errorsTestBean.getList().add("Link File Total Clusters " + _values.size() + "\nLink File Expected Clusters " + tBean.getLinkClusterExpectedCount());
                    jTextArea2.append(newline);
                    if (in == 0) {
                        RunErrorList.add(tBean.getTestCase_Name());
                        oysterTestSuiteHelper.WriteHeader(oBean, tBean, TestSuiteLog, jTextArea2);//    
                        in = 1;
                    }
                    counterstr = Integer.toString(_values.size());

                    oysterTestSuiteHelper.WriteFooter("Link File Cluster Count", oBean, tBean, TestSuiteLog, jTextArea2, counterstr);
                }

                if (input != null) {
                    input.close();
                    file = null;
                }
            } else {
                NoLinkFileList.add(tBean.getTestCase_Name());
            }
            ClusterRef.WriteToClusterRefReport(newline + "End of  " + tBean.getTestCase_Name());
            // ClusterRef.WriteToClusterRefReport("End of Run " + "-----------------------------");
            ClusterRef.WriteToClusterRefReport("-----------------------------" + newline);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            oysterTestSuiteHelper.CloseWriters(TestSuiteLog, ClusterRef);
        }
        return in;
    }

//    private static String replaceWhitespace(String str) {
//        if (str.contains("/ ")) {
//            str = str.replace("/ ", "%20");
//        }
//        return str;
//    }

    /*
    Now check the Change Reports for expected vs output
     */
    public Integer CheckChangeReports(OysterTestBean myBean, Integer in, ErrorsTestBean errorsTestBean) {
        LogWriter TestSuiteLog = new LogWriter(oBean.getOutPutLogFile());
        LogWriter ChangeRptLog = new LogWriter(myBean, "ChangeReport", oBean.getTestSuiteOysterRoot());

        try {
            String fileName = "";

            if (isWindows) {
                fileName = oBean.getTestSuiteOysterRoot() + "/" + myBean.getTestCase_Name() + Constants.OUTPUT + Constants.IdentityChangeReport;
            } else {
                fileName = oBean.getTestSuiteOysterRoot() + "/" + myBean.getTestCase_Name() + Constants.OUTPUT + "\\" + Constants.IdentityChangeReportLinux + "\\";

            }

            fileName = fileName.replace("\\", "/");

            File file = new File(fileName);

            if (file.exists()) {

                ChangeRptLog.WriteToChangeReport(newline + myBean.getTestCase_Name());
                ChangeRptLog.WriteToChangeReport("Run Date is " + dateFormat.format(new Date()));
                ChangeRptLog.WriteToChangeReport(oBean.getTestSuiteOysterToJarRun() + newline);
                ChangeRptLog.WriteToChangeReport("-------------------Change Report Check Start------------------------");

                Scanner input = new Scanner(file);

                while (input.hasNextLine()) {

                    String line = input.nextLine();
                    if (line.contains("Count of Output Identities")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Count of Output Identities    " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Output Identities Counts    " + myBean.getChangeReportExpectOutputIdentities());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectOutputIdentities()) {
                            errorsTestBean.getList().add("Change Report Output identities " + line + "\nChange Report Expected Output Identites " + myBean.getChangeReportExpectOutputIdentities());

                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Count of Output Identities", oBean, myBean, TestSuiteLog, jTextArea2, line);
                        }

                    } else if (line.contains("Identities Updated and Written to Output")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Identities Updated and Written to Output Count : " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Identities Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesUpdated());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectInputIdentitiesUpdated()) {
                            errorsTestBean.getList().add("Change Report Identities Updated and Written " + line + "\nChange Report Expected Identities Updated and Written " + myBean.getChangeReportExpectInputIdentitiesUpdated());
                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Identities Updated and Written to Output", oBean, myBean, TestSuiteLog, jTextArea2, line);
                        }
                    } else if (line.contains("Input Identities Not Updated and Written to Output")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Input Identities Not Updated and Written to Output Count : " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Input Identities Not Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesNotUpdated());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectInputIdentitiesNotUpdated()) {
                            if (in == 0) {
                                errorsTestBean.getList().add("Change Report Identities Not Updated and Written " + line + "\nChange Report Expected Identities Not Updated and Written " + myBean.getChangeReportExpectInputIdentitiesNotUpdated());
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Input Identities Not Updated and Written to Output", oBean, myBean, TestSuiteLog, jTextArea2, line);
                        }

                    } else if (line.contains("Count of Input Identities Merged")) {

                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Input Identities Merged Count : " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Input Identities Merged Count : " + myBean.getChangeReportExpectInputIdentitiesMerged());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectInputIdentitiesMerged()) {
                            errorsTestBean.getList().add("Change Report Input Identities Merged " + line + "\nChange Report Expected Input Identities Merged " + myBean.getChangeReportExpectInputIdentitiesMerged());
                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Count of Input Identities Merged", oBean, myBean, TestSuiteLog, jTextArea2, line);
                        }

                    } else if (line.contains("Count of New Identities Created")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report New Identities Created Count : " + line);
                        ChangeRptLog.WriteToChangeReport("Expected New Identities Created Count : " + myBean.getChangeReportExpectNewInputIdentitiesCreated());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectNewInputIdentitiesCreated()) {
                            errorsTestBean.getList().add("Change Report New Identities Created " + line + "\nChange Report Expected New Identities Created " + myBean.getChangeReportExpectNewInputIdentitiesCreated());
                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Count of New Identities Created", oBean, myBean, TestSuiteLog, jTextArea2, line);

                        }

                    } else if (line.contains("Count of Error Identities")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Error Identities Count: " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Error Identities Count : " + myBean.getChangeReportExpectErrorIdentities());
                        ChangeRptLog.WriteToChangeReport("---------------------------------------------------------------------");

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectErrorIdentities()) {
                            errorsTestBean.getList().add("Change Report Error Identities " + line + "\nChange Report Expected Error Identities " + myBean.getChangeReportExpectErrorIdentities());
                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    
                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Count of Error Identities", oBean, myBean, TestSuiteLog, jTextArea2, line);

                        }
                    } else if (line.contains("Count of Input Identities:")) {
                        line = line.substring(line.indexOf(":") + 1).trim();

                        ChangeRptLog.WriteToChangeReport("Change Report Input Identities Count : " + line);
                        ChangeRptLog.WriteToChangeReport("Expected Input Identities Count : " + myBean.getChangeReportExpectInputIdentities());

                        if (Integer.parseInt(line) != myBean.getChangeReportExpectInputIdentities()) {
                            errorsTestBean.getList().add("Change Report Input Identities " + line + "\nChange Report Expected Input Identities " + myBean.getChangeReportExpectInputIdentities());
                            if (in == 0) {
                                RunErrorList.add(myBean.getTestCase_Name());
                                oysterTestSuiteHelper.WriteHeader(oBean, myBean, TestSuiteLog, jTextArea2);//    

                                in = 1;
                            }
                            oysterTestSuiteHelper.WriteFooter("Count of Input Identities:", oBean, myBean, TestSuiteLog, jTextArea2, line);
                        }
                    }

                }
                if (in == 1) {

                    oysterTestSuiteHelper.WriteEndOfOysterRun(myBean, TestSuiteLog, jTextArea2);
                }

            } else {
                NoChangeReportList.add(myBean.getTestCase_Name());
                if (in == 1) {

                    oysterTestSuiteHelper.WriteEndOfOysterRun(myBean, TestSuiteLog, jTextArea2);

                }

            }
            file = null;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            eTestBean.getErrorOutputBeanList().add(errorsTestBean);
            oysterTestSuiteHelper.CloseWriters(TestSuiteLog, ChangeRptLog);
        }
        return in;
    }

    public void RunTestsERMetrics() {

        try {

            String cmd = "java -jar er-metrics.jar ERMetrics.properties";
            ProcessClass p = new ProcessClass();
            String dir = p.ReadAProperty(Constants.ERMetricsRootDir).replace("\\", "/");

            p = null;

            ProcessBuilder builder = new ProcessBuilder();
            if (isWindows) {
                builder = new ProcessBuilder("cmd.exe", "/c", cmd).directory(new File(dir));
            } else {

                builder = new ProcessBuilder("sh", "-c", cmd);
                builder.directory(new File(dir));
            }

            process = builder.start();

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            saveOuputToScreen(stdInput, stdError, true);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

        }

    }

    //Major method to run the python script and oyster tests as desired
    public void RunTests() {

        //  Process process;
        try {
            boolean run = true;
            ArrayList<String> list = oBean.getArrayList();
            Integer k = 0;
            String inputString = "";
            if (RunSingleTest.isEmpty()) {
                if (isWindows) {
                    for (String str : list) {
                        //     TestCount = +1;
                        if (k < list.size() - 1) {
                            str = str.substring(0, str.indexOf("."));
                            str = str + "RunScript.xml";
                            str = oBean.getTestSuiteOysterRoot() + "/" + str;
                            inputString = inputString + str + ",";

                            k = k + 1;
                        } else {
                            str = str.substring(0, str.indexOf("."));
                            str = str + "RunScript.xml";
                            str = oBean.getTestSuiteOysterRoot() + "/" + str;
                            inputString = inputString + str;

                        }

                    }
                } else {

                    for (String str : list) {
                        //     TestCount = +1;
                        if (k < list.size() - 1) {
                            str = str.substring(0, str.indexOf("."));
                            str = str + "RunScript.xml";
                            inputString = inputString + str + ",";
                            k = k + 1;
                        } else {
                            str = str.substring(0, str.indexOf("."));
                            str = str + "RunScript.xml";
                            //  str = oBean.getTestSuiteOysterRoot() + "/" + str;
                            inputString = inputString + str;

                        }
                    }

                }
            } else {
                if (isWindows) {
                    ArrayList<String> newList = new ArrayList<>();
                    newList.add(RunSingleTest);
                    this.oBean.getArrayList().clear();

                    this.oBean.setArrayList(newList);
                    String TestToRun = RunSingleTest;

                    TestToRun = TestToRun.substring(0, TestToRun.indexOf("."));
                    TestToRun = TestToRun + "RunScript.xml";
                    TestToRun = oBean.getTestSuiteOysterRoot() + "/" + TestToRun;
                    inputString = TestToRun;

                } else {
                    ArrayList<String> newList = new ArrayList<>();
                    newList.add(RunSingleTest);
                    this.oBean.getArrayList().clear();
                    this.oBean.setArrayList(newList);
                    String TestToRun = RunSingleTest;

                    TestToRun = TestToRun.substring(0, TestToRun.indexOf("."));
                    TestToRun = TestToRun + "RunScript.xml";
                    inputString = TestToRun;
                }
            }
            if (run) {
                String pythonExe = oBean.getPythonExe().replace("\\", "/");
                String runfile = oBean.getPythonFile().replace("\\", "/");
                String runLogDir = oBean.getOutPutLogFile().substring(0, oBean.getOutPutLogFile().lastIndexOf("/"));
                String runLog = runLogDir + "/PythonRunfile.log";
                runLog = runLog.replace("\\", "/");

                String oysterDir = oBean.getTestSuiteOysterRoot();//.replace("\\", "\\\\");

                if (isWindows) {
                    process = Runtime.getRuntime().exec(pythonExe + "/python.exe " + '"' + runfile + '"' + " " + inputString + " " + oBean.getTestSuiteOysterToJarRun() + " " + runLog + " " + '"' + oysterDir + '"');
//                    String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
//                    PID = processName.split("@")[0];

                } else {
                    ProcessBuilder builder = new ProcessBuilder();
                    // String cmd1 = "python3.7 ./Python/TestSuite.py " + inputString + " " + oBean.getTestSuiteOysterToJarRun() + " " + runLog + " " +  oysterDir;
                    String cmd1 = "python3.7 " + '"' + runfile + '"' + " " + inputString + " " + oBean.getTestSuiteOysterToJarRun() + " " + runLog;
                    builder.command("sh", "-c", cmd1);
                    builder.directory(new File("./"));
                    process = builder.start();
//                    String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
//                    this.PID = processName.split("@")[0];
                }

                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                saveOuputToScreen(stdInput, stdError, false);
            } else {
                JOptionPane jOptionPane = new JOptionPane();
                JFrame messageFrame = new JFrame("No Tests To Run Chosen!");
                jOptionPane.showMessageDialog(messageFrame, "No Tests To Run Chosen!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));

            }
        } catch (Exception e) {
            System.out.println("RunTests " + e.getMessage());
        } finally {

        }

    }

    //runs on its own thread to record to the screen during run
    private void saveOuputToScreen(BufferedReader stdInput, BufferedReader stdError, Boolean ERM) {
        //this is to get any ERRORS throuwn by the Oyster.Jar recorded to the Issue Page
        Map<String, List<String>> map = eTestBean.getMap();

        final SwingWorker<Void, String> textWorker = new SwingWorker<Void, String>() {
            @Override
            protected Void doInBackground() throws Exception {

                String s = null;

                while ((s = stdInput.readLine()) != null) {
                    publish(s);
                }

                while ((s = stdError.readLine()) != null) {
                    bool = true;
                    publish(s);
                }
                return null;
            }

            @Override
            protected void process(List<String> chunks) {

                LogWriter TestSuiteLog = new LogWriter(oBean.getOutPutLogFile());
                // Strings sent to the EDT by the publish method
                // This called on the Swing event thread.
                for (String chunk : chunks) {

                    if (chunk.toUpperCase().contains("Opening".toUpperCase())) {
                        if (isWindows) {
                            //comes in in windows from run as backslash so....
                            String TestCaseName = chunk.substring(chunk.lastIndexOf("\\") + 1);
                            runScriptName = TestCaseName.substring(0, TestCaseName.indexOf("RunScript"));
                        } else {

                            String TestCaseName = chunk.substring(chunk.lastIndexOf("/") + 1);
                            runScriptName = TestCaseName.substring(0, TestCaseName.indexOf("RunScript"));
                        }
                    }
                    if (bool) {
                        boolean contains = false;
                        if (ERM == true) {

                            jTextArea5.append(newline);
                            jTextArea5.append(chunk);
                            jTextArea5.append(newline);
                            jTextArea5.append(newline);
                            bool = false;
                        } else {

                            if (!RunErrorList.isEmpty()) {
                                contains = RunErrorList.contains(runScriptName);
                                if (!contains) {
                                    RunErrorList.add(runScriptName);
                                }
                            } else {
                                RunErrorList.add(runScriptName);
                            }

                            if (map.get(runScriptName) == null) {
                                List<String> list = new ArrayList<>();
                                list.add(chunk);
                                map.put(runScriptName, list);
                            } else {
                                List<String> tempList = map.get(runScriptName);
                                tempList.add(chunk);
                                map.put(runScriptName, tempList);
                            }

                            jTextArea2.append("Error in " + runScriptName + newline);
                            jTextArea2.append(newline);
                            jTextArea2.append(chunk);
                            jTextArea2.append(newline);
                            jTextArea2.append(newline);
                            TestSuiteLog.WriteToLog(newline + "Error in " + runScriptName);
                            TestSuiteLog.WriteToLog(chunk + newline);
                            bool = false;
                        }
                    } else if (chunk.toUpperCase().contains("ERROR".toUpperCase()) || (chunk.toUpperCase().contains("SEVERE".toUpperCase()))) {
                        boolean contains = false;
                        if (ERM == true) {

                            jTextArea5.append(newline);
                            jTextArea5.append(chunk);
                            jTextArea5.append(newline);
                            jTextArea5.append(newline);
                            bool = false;
                        } else {
                            if (!RunErrorList.isEmpty()) {
                                contains = RunErrorList.contains(runScriptName);
                                if (!contains) {
                                    RunErrorList.add(runScriptName);
                                }
                            } else {
                                RunErrorList.add(runScriptName);
                            }

                            if (map.get(runScriptName) == null) {
                                List<String> list = new ArrayList<>();
                                list.add(chunk);
                                map.put(runScriptName, list);
                            } else {
                                List<String> tempList = map.get(runScriptName);
                                tempList.add(chunk);
                                map.put(runScriptName, tempList);
                            }
                            jTextArea2.append(newline);
                            jTextArea2.append("Error in  " + runScriptName + newline);
                            jTextArea2.append(chunk);
                            jTextArea2.append(newline);
                            jTextArea2.append(newline);
                            TestSuiteLog.WriteToLog(newline + "Error in " + runScriptName);
                            TestSuiteLog.WriteToLog(chunk + newline);
                            bool = false;
                        }
                    } else {
                        if (ERM == true) {

                            jTextArea5.append(chunk);
                            jTextArea5.append(newline);
                            bool = false;
                        } else {
                            jTextArea1.append(chunk);
                            jTextArea1.append(newline);
                            bool = false;
                            if (chunk.contains("Ran in")) {
                                PythonRunTime = chunk.substring(7, 14) + " secs";
                            }
                        }
                    }

                    if (chunk.contains("End of Python Run")) {
                        TestSuiteLog.Flush();
                        TestSuiteLog.Close();
                        //start the comparisons from run vs. expected
                        CheckRunOutputLogAndLink();

                    }

                }
            }

        };

        textWorker.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
                    try {
                        // need to call this on the event thread
                        // to catch any exceptions that have occurred in the Swing Worker
                        textWorker.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        // TODO catch exceptions buried in this guy
                        e.printStackTrace();
                    }
                }
            }
        });

        // run our SwingWorker
        textWorker.execute();

    }

    //Load the double clicked file chosen on the Setup Tab
    public void LoadEditor(String fileAndPath, String TextPane) {
        String filePath = fileAndPath;

        try {

            //do not load jars,zip or exe to the textpane - ignore it
            if (!fileAndPath.toUpperCase().endsWith(".JAR")
                    && !fileAndPath.toUpperCase().endsWith(".EXE")
                    && !fileAndPath.toUpperCase().endsWith(".ZIP")) {

                FileInputStream fr = new FileInputStream(filePath);
                InputStreamReader isr = new InputStreamReader(fr, "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                StringBuffer buffer = new StringBuffer();

                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    buffer.append(newline);
                }

                if (TextPane.equalsIgnoreCase("newTextPane")) {
                    newTextPane.setFont(new Font("Arial", Font.PLAIN, 16));
                    jScrollPane6.setViewportView(newTextPane);
                    if (buffer.toString().isEmpty()) {
                        newTextPane.setText("File is Empty! Rerun it in Oyster!");
                    } else {
                        newTextPane.setText(buffer.toString());
                    }
                    newTextPane.setCaretPosition(0);
                    filesToSaveBean.setFileLoadedToXMLTextBox(filePath);
                    String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
                    jLabel17.setText(fileName);
                } else if (TextPane.equalsIgnoreCase("newTextPane2")) {
                    newTextPane2.setFont(new Font("Arial", Font.PLAIN, 16));
                    if (buffer.toString().isEmpty()) {
                        newTextPane2.setText("File is Empty! Rerun it in Oyster!");
                    } else {
                        newTextPane2.setText(buffer.toString());
                    }
                    // newTextPane2.setCaretPosition(0);

                    filesToSaveBean.setFileLoadedToXMLTextBox2(filePath);
                    String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
                    jLabel26.setText(fileName);

                } else if (TextPane.equalsIgnoreCase("jTextArea5")) {
                    jTextArea5.setFont(new Font("Arial", Font.PLAIN, 16));

                    if (buffer.toString().isEmpty()) {
                        jTextArea5.setText("File is Empty! Rerun it in Oyster!");
                    } else {
                        jTextArea5.setText(buffer.toString());
                    }

                    // jTextArea5.setText(buffer.toString());
                    jTextArea5.setCaretPosition(0);
                    filesToSaveBean.setFileLoadedToXMLTextBox3(filePath);
                    String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
                    jLabel40.setText("Loaded File is: " + fileName);
                }

                reader.close();
                reader = null;
                fr.close();
                fr = null;
                isr.close();
                isr = null;
                buffer = null;
            }//end of check for exe,jar or zip file
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public class CreateChildNodes implements Runnable {

        private DefaultMutableTreeNode root;

        private File fileRoot;

        public CreateChildNodes(File fileRoot,
                DefaultMutableTreeNode root) {
            this.fileRoot = fileRoot;
            this.root = root;
        }

        @Override
        public void run() {
            createChildren(fileRoot, root);
        }

        private void createChildren(File fileRoot,
                DefaultMutableTreeNode node) {
            try {

                File[] files = fileRoot.listFiles();
                if (files == null) {
                    return;
                }

                for (File file : files) {
                    DefaultMutableTreeNode childNode
                            = new DefaultMutableTreeNode(new FileNode(file));
                    node.add(childNode);
                    if (file.isDirectory()) {
                        createChildren(file, childNode);
                    }
                }
            } catch (Exception e) {
                System.out.println("tree error 2 " + e.getMessage());
            }

        }

    }

    public class FileNode {

        private File file;

        public FileNode(File file) {
            this.file = file;
        }

        @Override
        public String toString() {

            String name = "";
            try {
                name = file.getName();
                if (name.equals("")) {
                    return file.getAbsolutePath();
                } else {
                    return name;
                }
            } catch (Exception e) {
                System.out.println("tree error 1 " + e.getMessage());
            }
            return name;
        }
    }

//    //used to only load specific items to jTree
    private void createChildren(File fileRoot,
            DefaultMutableTreeNode node) {

        File[] files = fileRoot.listFiles();
        if (files == null) {
            return;

        } else {
            for (File file : files) {

                DefaultMutableTreeNode childNode
                        = new DefaultMutableTreeNode(new FileNode(file));
                if (file.isFile()) {

                    if (!file.getName().contains(".zip") && !file.getName().contains(".docx") && !file.getName().contains(".jar")) {
                        //  if (file.getName().contains(".xml")) {
                        node.add(childNode);

                    }
                }
            }

        }
    }

    private String GetFullPath() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        String line = "";
        StringBuilder output = new StringBuilder();
        processBuilder.command("bash", "-c", "pwd");
        try {

            Process process = processBuilder.start();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            while ((line = reader.readLine()) != null) {

                output.append(line);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    private void createChildren1(File fileRoot,
            DefaultMutableTreeNode node) {

        Path path = Paths.get(GetFullPath());

        List<File> fileList = new ArrayList<>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
            Iterator<Path> iterator = ds.iterator();

            while (iterator.hasNext()) {
                Path p = iterator.next();
                fileList.add(p.toFile());
            }
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
        }

        File[] files = fileList.toArray(new File[fileList.size()]);

        if (files == null) {
            return;

        } else {
            for (File file : files) {

                DefaultMutableTreeNode childNode
                        = new DefaultMutableTreeNode(new FileNode(file));
                if (file.isFile()) {

                    if (!file.getName().contains(".zip") && !file.getName().contains(".docx") && !file.getName().contains(".jar")) {

                        node.add(childNode);

                    }
                }

            }
//u
        }
//
    }

    //used to only load specific items to jTree
    private void createChildren2(File fileRoot,
            DefaultMutableTreeNode node) {
        File[] files = fileRoot.listFiles();
        if (files == null) {
            return;
        } else {
            for (File file : files) {
                DefaultMutableTreeNode childNode
                        = new DefaultMutableTreeNode(new FileNode(file));

                if (file.isFile()) {

                    if (!file.getName().contains(".zip") && !file.getName().contains(".docx") && !file.getName().contains(".jar")) {
                        if (file.getName().contains("*.xml")) {
                            node.add(childNode);
                        }

                    }
                }

            }

        }

    }

    /*
    * used to load textpanes to parse the comma delimited
    * path file to make a usable path for loading and 
    * saving textpane loaded files
     */
    public String ParseTreeSelectPath(String filePath) {

        filePath = filePath.substring(1, filePath.length() - 1);
        String[] strArray = filePath.split(",");

        String pathString = "/";
        String returnString = "";
        int i = 0;
        for (String str : strArray) {

            if (i == 0) {
                returnString = pathString + str.trim();
                i = 1;
            } else {
                returnString = returnString + pathString + str.trim();
            }

        }

        if (isWindows) {

        } else {

            if (returnString.toUpperCase().contains("ER_CALCULATOR")) {

                String basePath = "./Oyster/ER_Calculator/";
                String file = returnString.substring(returnString.toString().lastIndexOf("/") + 1);
                returnString = basePath + file;

            } else if (returnString.toUpperCase().contains("/OYSTERTESTSUITE/SCRIPTS")) {

                returnString = returnString.substring(returnString.lastIndexOf("/"));
                returnString = "./Scripts" + returnString;

            } else if (returnString.toUpperCase().contains("LINK") || returnString.toUpperCase().contains("OUTPUT")
                    || returnString.toUpperCase().contains("INPUT") || returnString.toUpperCase().contains("SCRIPTS")) {

                String path = returnString.substring(returnString.indexOf("/Oyster/"));
                returnString = "." + path;

            } else if (returnString.toUpperCase().contains("RUNLOGS")) {

                String path = returnString.substring(returnString.indexOf("/RunLogs/"));
                returnString = "." + path;

            } else if (returnString.toUpperCase().contains("/OYSTERTESTSUITE/OYSTER")) {

                returnString = returnString.substring(returnString.lastIndexOf("/"));
                returnString = "./Oyster" + returnString;

            } else if (returnString.toUpperCase().contains("/OYSTERTESTSUITE")) {

                returnString = "." + returnString.substring(returnString.lastIndexOf("/"));
            }
        }

        return returnString;
    }

//    //used when loading the jTrees
//    public class FileNode {
//
//        private File file;
//
//        public FileNode(File file) {
//            this.file = file;
//        }
//
//        @Override
//        public String toString() {
//            String name = file.getName();
//            if (name.equals("")) {
//                return file.getAbsolutePath();
//            } else {
//                return name;
//            }
//        }
//
//    }

    /*
    * Use this to ceate a backup of the data file everytime it gets written out
    * There are 4 back up files rotated in the datafiles dirextory on disk
     */
    private void BackUpOfDataFile(File source, File dest) throws IOException {

//        //ncessary to have permissions
//        AccessControlContext acc = AccessController.getContext();
//
//        //ncessary to have permissions
//        AccessController.doPrivileged(new PrivilegedAction<Void>() {
//            public Void run() {
        try {
            InputStream is = new FileInputStream(source);
            OutputStream os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            is.close();
            os.close();
            //  return null;
        } catch (IOException ie) {
            System.out.println("BackUpOfDataFile method error is " + ie.getMessage());
        }
        //   return null;
//            }
//        }, acc);
    }

    //this loads the jTable on the History tab
    public void LoadTable(JTable jTable1, String FileName) {

        //read file back into jTable and then center values
        DefaultTableModel model = new DefaultTableModel();
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

        try {
            BufferedReader br = new BufferedReader(new FileReader(FileName));

            String line = br.readLine();
            String[] colHeaders = line.split(",");
            model.setColumnIdentifiers(colHeaders);

            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                model.addRow(data);
            }

            jTable1.setModel(model);
            Enumeration e = jTable1.getColumnModel().getColumns();
            for (int i = 0; e.hasMoreElements(); i++) {
                ((TableColumn) e.nextElement()).setCellRenderer(centerRenderer);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /*
    * Write what was in the jTable to file for use when program
    * starts again.  Keep the main file and the laas 4 backups 
    * shifting thrught the backups
     */
    public void WriteTableOut(JTable jTable1) {

        File source = null;
        File dest = null;
        File dest1 = null;
        File dest2 = null;
        File dest3 = null;
        File dest4 = null;
        int k = 0;

        try {
            BufferedWriter bfw = null;
            if (isWindows) {
                bfw = new BufferedWriter(new FileWriter(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "JTable1Data.data"));
            } else {
                // String runlog = oBean.
                bfw = new BufferedWriter(new FileWriter(Constants.RUNLOGSLINUX + "/JTable1Data.data"));
            }

            for (int i = 0; i < jTable1.getColumnCount(); i++) {
                bfw.write(jTable1.getColumnName(i));
                if (i != jTable1.getColumnCount() - 1) {
                    bfw.write(",");
                }
            }

            for (int i = 0; i < jTable1.getRowCount(); i++) {
                bfw.newLine();
                for (int j = 0; j < jTable1.getColumnCount(); j++) {
                    bfw.write((String) (jTable1.getValueAt(i, j).toString()));
                    if (j != jTable1.getColumnCount() - 1) {
                        bfw.write(",");
                    }

                }
            }
            bfw.close();
            bfw = null;
            //make a backup of the file created

            if (isWindows) {
                source = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "JTable1Data.data");
                dest = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "DataFiles/JTable1Data.data");
                dest1 = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "DataFiles/JTable1Data.data.1");
                dest2 = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "DataFiles/JTable1Data.data.2");
                dest3 = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "DataFiles/JTable1Data.data.3");
                dest4 = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "DataFiles/JTable1Data.data.4");
            } else {
                source = new File(Constants.RUNLOGSLINUX + "/JTable1Data.data");
                dest = new File(Constants.RUNLOGSLINUX + "/DataFiles/JTable1Data.data");
                dest1 = new File(Constants.RUNLOGSLINUX + "/DataFiles/JTable1Data.data.1");
                dest2 = new File(Constants.RUNLOGSLINUX + "/DataFiles/JTable1Data.data.2");
                dest3 = new File(Constants.RUNLOGSLINUX + "/DataFiles/JTable1Data.data.3");
                dest4 = new File(Constants.RUNLOGSLINUX + "/DataFiles/JTable1Data.data.4");
            }

            if (dest3.exists()) {
                BackUpOfDataFile(dest3, dest4);
            }
            if (dest2.exists()) {
                BackUpOfDataFile(dest2, dest3);
            }
            if (dest1.exists()) {

                BackUpOfDataFile(dest1, dest2);
            }
            if (dest.exists()) {
                BackUpOfDataFile(dest, dest1);
            }

            BackUpOfDataFile(source, dest);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } finally {

        }

    }

    //Used to load the table when it is totally empty and no file to import
    //and also adds rows to the table as runs ends
    public void FillTable(int RunErrorList, int NoLinkFileList, int NoChangeReportList) {

        DefaultTableModel dm;
        // Vector<Object> data = new Vector<Object>();
        try {
            // Vector<Object> data = new Vector<Object>();
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
            // jTable1.setDefaultRenderer(String.class, centerRenderer);

            if (jTable1.getModel().getRowCount() == 0) {

                //first time you must set headers AND load the data
                dm = new DefaultTableModel(0, 0);
                String header[] = new String[]{"Date", "Run Jar", "# Tests", "Failed", "Passed", "NoLinkFile", "NoChangeReport", "PythonRunTime"};
                dm.setColumnIdentifiers(header);
                jTable1.setModel(dm);

                Enumeration e = jTable1.getColumnModel().getColumns();
                for (int i = 0; e.hasMoreElements(); i++) {
                    ((TableColumn) e.nextElement()).setCellRenderer(centerRenderer);
                }

                Vector<Object> data = new Vector<Object>();
                data.add(dateFormat.format(new Date()));
                data.add(oBean.getTestSuiteOysterToJarRun());
                data.add(Integer.toString(oBean.getArrayList().size()));
                data.add(Integer.toString(RunErrorList));
                data.add(Integer.toString(oBean.getArrayList().size() - RunErrorList));
                data.add(Integer.toString(NoLinkFileList));
                data.add(Integer.toString(NoChangeReportList));
                data.add(PythonRunTime);

                dm.addRow(data);
                dm.fireTableDataChanged();

                //now load the labels on the run History tab for Latest Run info              
                jLabel31.setText("Run Date Time: " + dateFormat.format(new Date()));
                jLabel32.setText("Tests Run: " + oBean.getArrayList().size());
                jLabel37.setText("Passed: " + (Integer.toString(oBean.getArrayList().size() - RunErrorList)));
                jLabel38.setText("Failed: " + Integer.toString(this.RunErrorList.size()));
                jLabel39.setText("Jar: " + oBean.getTestSuiteOysterToJarRun());

            } else {

                //all runs after first run. Always use the model not the table
                dm = (DefaultTableModel) (jTable1.getModel());

                Enumeration e = jTable1.getColumnModel().getColumns();
                for (int i = 0; e.hasMoreElements(); i++) {
                    ((TableColumn) e.nextElement()).setCellRenderer(centerRenderer);
                }

                Vector<Object> data = new Vector<Object>();
                data.add(dateFormat.format(new Date()));
                data.add(oBean.getTestSuiteOysterToJarRun());
                data.add(Integer.toString(oBean.getArrayList().size()));
                data.add(Integer.toString(RunErrorList));
                data.add(Integer.toString(oBean.getArrayList().size() - RunErrorList));
                data.add(Integer.toString(NoLinkFileList));
                data.add(Integer.toString(NoChangeReportList));
                data.add(PythonRunTime);
                //insert the row at the top of table
                dm.insertRow(0, data);
                //let every listener know the table changed
                dm.fireTableDataChanged();

                //now load the labels on the run History tab for Latest Run info              
                jLabel31.setText("Run Date Time: " + dateFormat.format(new Date()));
                jLabel32.setText("Tests Run: " + oBean.getArrayList().size());
                jLabel37.setText("Passed: " + (Integer.toString(oBean.getArrayList().size() - RunErrorList)));
                jLabel38.setText("Failed: " + Integer.toString(this.RunErrorList.size()));
                jLabel39.setText("Jar: " + oBean.getTestSuiteOysterToJarRun());

            }

        } catch (Exception e) {
            System.out.println("Error in Filltable is " + e.getMessage());
            e.printStackTrace();
        } finally {

        }

    }

    /*
    Load the three Jtrees. Called in the JTree edit code
    from the Design mode.
     */
    public void LoadjTree(Integer TreeNum) {

        File fileRoot = null;
        File fileRoot2 = null;
        File fileRoot3;
        File fileRoot4;
        File fileRoot5;
        File fileRoot6;

        if (oBean.getTestSuiteWorkspace() != null) {
            if (TreeNum == 2) {
                if (isWindows) {
                    fileRoot2 = new File(oBean.getTestSuiteWorkspace());
                } else {

                    fileRoot2 = new File(oBean.getTestSuiteWorkspace());
                }

            } else if (TreeNum == 1) {
                if (isWindows) {
                    fileRoot = new File(oBean.getTestSuiteWorkspace());
                } else {

                    fileRoot = new File(oBean.getTestSuiteWorkspace());
                }

            }
        } else {
            if (TreeNum == 2) {
                fileRoot2 = new File("/OysterTestSuite");
            } else if (TreeNum == 1) {
                fileRoot = new File("/OysterTestSuite");
            }
        }

        if (TreeNum == 2) {

            DefaultMutableTreeNode root2 = new DefaultMutableTreeNode(new FileNode(fileRoot2));
            treeModel2 = new DefaultTreeModel(root2);
            jTree2 = new javax.swing.JTree(treeModel2);
            jTree2.setShowsRootHandles(true);

            fileRoot3 = new File(oBean.getTestSuiteOysterRoot());
            DefaultMutableTreeNode Oyster = new DefaultMutableTreeNode(new FileNode(fileRoot3));
            root2.add(Oyster);
            CreateChildNodes ccn2 = new CreateChildNodes(fileRoot3, Oyster);
            new Thread(ccn2).start();

            String logDirectory = "";
            if (isWindows) {
                logDirectory = oBean.getTestSuiteWorkspace() + "/RunLogs";
            } else {
                logDirectory = Constants.RUNLOGSLINUX;
            }

            fileRoot4 = new File(logDirectory);
            DefaultMutableTreeNode LogDir = new DefaultMutableTreeNode(new FileNode(fileRoot4));
            root2.add(LogDir);
            CreateChildNodes ccn20 = new CreateChildNodes(fileRoot4, LogDir);
            new Thread(ccn20).start();

            jTree2.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            Font currentFont = jTree2.getFont();
            Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 4);
            jTree2.setFont(bigFont);
            treePopup2 = new TreePopup(jTree2);
            jTree2.addMouseListener(mlma);
            jScrollPane5.setViewportView(jTree2);
            jTree2.expandPath(new TreePath(root2));
            //  jTree2.repaint();

        } else if (TreeNum == 1) {

            if (isWindows) {
                jTree1.setModel(null);

                DefaultMutableTreeNode root = new DefaultMutableTreeNode(new FileNode(fileRoot));
                treeModel = new DefaultTreeModel(root);
                jTree1 = new javax.swing.JTree(treeModel);

                createChildren(fileRoot, root);
                fileRoot5 = new File(oBean.getTestSuiteOysterRoot());
                DefaultMutableTreeNode Oyster = new DefaultMutableTreeNode(new FileNode(fileRoot5));
                root.add(Oyster);
                CreateChildNodes ccn = new CreateChildNodes(fileRoot5, Oyster);
                new Thread(ccn).start();

                String logDirectory = oBean.getTestSuiteWorkspace() + Constants.SCRIPTS;
                fileRoot6 = new File(logDirectory);
                DefaultMutableTreeNode LogDir = new DefaultMutableTreeNode(new FileNode(fileRoot6));
                root.add(LogDir);

                CreateChildNodes ccn4 = new CreateChildNodes(fileRoot6, LogDir);
                new Thread(ccn4).start();

                Font currentFont = jTree1.getFont();
                Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 4);
                jTree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                jTree1.setFont(bigFont);
                treePopup = new TreePopup(jTree1);
                jTree1.addMouseListener(ml);
                jScrollPane3.setViewportView(jTree1);
                jTree1.setShowsRootHandles(true);
                jTree1.expandPath(new TreePath(root));

            } else {

                jTree1.setModel(null);
                fileRoot = new File(oBean.getTestSuiteWorkspace());
                DefaultMutableTreeNode root = new DefaultMutableTreeNode(new FileNode(fileRoot));
                treeModel = new DefaultTreeModel(root);

                jTree1 = new JTree(treeModel);
//                CreateChildNodes ccn11 = new CreateChildNodes(fileRoot, root);
//                new Thread(ccn11).start();
                createChildren1(fileRoot, root);

                fileRoot5 = new File(oBean.getTestSuiteOysterRoot());
                DefaultMutableTreeNode Oyster = new DefaultMutableTreeNode(new FileNode(fileRoot5));
                root.add(Oyster);
                CreateChildNodes ccn1 = new CreateChildNodes(fileRoot5, Oyster);
                new Thread(ccn1).start();

                String logDirectory = oBean.getTestSuiteScriptsDir();

                //  String logDirectory = oBean.getTestSuiteWorkspace() + Constants.SCRIPTS;
                fileRoot6 = new File(logDirectory);
                DefaultMutableTreeNode LogDir = new DefaultMutableTreeNode(new FileNode(fileRoot6));
                root.add(LogDir);

                CreateChildNodes ccn4 = new CreateChildNodes(fileRoot6, LogDir);
                new Thread(ccn4).start();

                jTree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                Font currentFont = jTree1.getFont();
                Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 4);
                jTree1.setFont(bigFont);
                treePopup = new TreePopup(jTree1);
                jTree1.addMouseListener(ml);
                jScrollPane3.setViewportView(jTree1);
                jTree1.setShowsRootHandles(true);
                jTree1.expandPath(new TreePath(root));
            }
            //  jTree1.repaint();
        } else if (TreeNum == 3) {

            if (ERMRootTxt.getText().isEmpty() || ERMJarTxt.getText().isEmpty() || ERMOysterRootTxt.getText().isEmpty()) {
                //do nothing
            } else if (isWindows) {
                ProcessClass p = new ProcessClass();
                String ERMetricsRoot = p.ReadAProperty(Constants.ERMetricsRootDir);
                String OysterRoot = ERMetricsRoot.substring(0, ERMetricsRoot.lastIndexOf("/"));
                String OysterLinkDir = p.ReadAProperty(Constants.ERMetricsOysterDir);
                String OysterDirOutputDir = OysterLinkDir + "/Output";
                String OysterTestSuiteDir = ERMetricsRoot.substring(0, ERMetricsRoot.lastIndexOf("Oyster") - 1);

                File fileRoot7 = new File(OysterTestSuiteDir.replace("\\", "/"));
                File fileRoot22 = new File(ERMetricsRoot.replace("\\", "/"));
                File fileRoot33 = new File(OysterRoot.replace("\\", "/"));
                File fileRoot44 = new File(OysterLinkDir.replace("\\", "/"));
                File fileRoot55 = new File(OysterDirOutputDir.replace("\\", "/"));
                jTree4.setModel(null);
                DefaultMutableTreeNode root7 = new DefaultMutableTreeNode(new FileNode(fileRoot7));
                treeModel3 = new DefaultTreeModel(root7);
                jTree4 = new javax.swing.JTree(treeModel3);

                DefaultMutableTreeNode root33 = new DefaultMutableTreeNode(new FileNode(fileRoot33));
                root7.add(root33);

                DefaultMutableTreeNode root22 = new DefaultMutableTreeNode(new FileNode(fileRoot22));
                root33.add(root22);
                //  createChildren2(fileRoot22, root22);
                CreateChildNodes ccn1 = new CreateChildNodes(fileRoot22, root22);
                new Thread(ccn1).start();
                DefaultMutableTreeNode root44 = new DefaultMutableTreeNode(new FileNode(fileRoot44));
                root33.add(root44);

                DefaultMutableTreeNode root55 = new DefaultMutableTreeNode(new FileNode(fileRoot55));
                root44.add(root55);
                createChildren3(fileRoot55, root55);

                TreePath path = new TreePath(root55.getPath());

                treePopup4 = new TreePopup(jTree4);
                jTree4.addMouseListener(this.mlm);
                jTree4.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                Font currentFont = jTree4.getFont();
                Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 4);
                jTree4.setFont(bigFont);
                jScrollPane8.setViewportView(jTree4);
                this.jTree4.setShowsRootHandles(true);
                jTree4.expandPath(new TreePath(root7));
                jTree4.expandPath(path);

            } else {

                ProcessClass p = new ProcessClass();
                String ERMetricsRoot = p.ReadAProperty(Constants.ERMetricsRootDir);

                String OysterRoot = ERMetricsRoot.substring(0, ERMetricsRoot.lastIndexOf("/"));
                String OysterLinkDir = p.ReadAProperty(Constants.ERMetricsOysterDir);
                String OysterDirOutputDir = OysterLinkDir + "/Output";
                String OysterTestSuiteDir = ERMetricsRoot.substring(0, ERMetricsRoot.lastIndexOf("Oyster") - 1);

                File fileRoot7 = new File(OysterTestSuiteDir.replace("\\", "/"));
                File fileRoot22 = new File(ERMetricsRoot.replace("\\", "/"));
                File fileRoot33 = new File(OysterRoot.replace("\\", "/"));
                File fileRoot44 = new File(OysterLinkDir.replace("\\", "/"));
                File fileRoot55 = new File(OysterDirOutputDir.replace("\\", "/"));
                jTree4.setModel(null);
                DefaultMutableTreeNode root7 = new DefaultMutableTreeNode(new FileNode(fileRoot7));
                treeModel3 = new DefaultTreeModel(root7);
                jTree4 = new javax.swing.JTree(treeModel3);

                DefaultMutableTreeNode root33 = new DefaultMutableTreeNode(new FileNode(fileRoot33));
                root7.add(root33);

                DefaultMutableTreeNode root22 = new DefaultMutableTreeNode(new FileNode(fileRoot22));
                root33.add(root22);
                createChildren2(fileRoot22, root22);
                CreateChildNodes ccn1 = new CreateChildNodes(fileRoot22, root22);
                new Thread(ccn1).start();

                DefaultMutableTreeNode root44 = new DefaultMutableTreeNode(new FileNode(fileRoot44));
                root33.add(root44);

                DefaultMutableTreeNode root55 = new DefaultMutableTreeNode(new FileNode(fileRoot55));
                root44.add(root55);
                createChildren3(fileRoot55, root55);

                TreePath path = new TreePath(root55.getPath());

                treePopup4 = new TreePopup(jTree4);
                jTree4.addMouseListener(this.mlm);
                jTree4.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                Font currentFont = jTree4.getFont();
                Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 4);
                jTree4.setFont(bigFont);
                jScrollPane8.setViewportView(jTree4);
                jTree4.setShowsRootHandles(true);
                jTree4.expandPath(new TreePath(root7));
                jTree4.expandPath(path);

            }

        }
    }

    //used to only load specific items to jTree
    private void createChildren3(File fileRoot,
            DefaultMutableTreeNode node) {
        File[] files = fileRoot.listFiles();
        if (files == null) {
            return;
        } else {
            for (File file : files) {
                DefaultMutableTreeNode childNode
                        = new DefaultMutableTreeNode(new FileNode(file));
                if (file.isFile()) {
                    if (!file.getName().contains(".zip") && !file.getName().contains(".docx")) {
                        if (file.getName().contains(".link")) {
                            String newPath = file.getPath().replace("\\", "/");
                            OysterDirLinkFileTxt.setText(newPath);
                            node.add(childNode);
                            //  add the date under the textbox
                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                            jLabel51.setText("Link File Date: " + sdf.format(file.lastModified()));
                        }

                    }
                }

            }

        }

    }

    /*
    * If ERMetrics external config.xml was saved we load it
    * on startup
     */
    private void LoadOnStartup() {

        ProcessClass p = new ProcessClass();
        ERMRootTxt.setText(p.ReadAProperty(Constants.ERMetricsRootDir));
        ERMJarTxt.setText(p.ReadAProperty(Constants.ERMetricsJar));
        ERMOysterRootTxt.setText(p.ReadAProperty(Constants.ERMetricsOysterDir));
        LoadjTree(3);

    }
//used for the jTrees for pupup right click functinality
    MouseListener ml = new MouseAdapter() {
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                treePopup.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        ;
     public void mousePressed(MouseEvent e) {
            int selRow = jTree1.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = jTree1.getPathForLocation(e.getX(), e.getY());
            if (selRow != -1) {
                if (e.getClickCount() == 1) {
                    if (e.isPopupTrigger()) {
                        treePopup.show(e.getComponent(), e.getX(), e.getY());
                    }
                } else if (e.getClickCount() == 2) {

                    if (!selPath.toString().contains(".")) {
                        return;
                    }

                    LoadEditor(ParseTreeSelectPath(selPath.toString()), "newTextPane");

                }
            }
        }
    };

    MouseListener mlm = new MouseAdapter() {

        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                treePopup4.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        ;
        
        public void mousePressed(MouseEvent e) {

            int selRow = jTree4.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = jTree4.getPathForLocation(e.getX(), e.getY());

            if (selRow != -1) {
                if (e.getClickCount() == 1) {
                    if (e.isPopupTrigger()) {
                        treePopup4.show(e.getComponent(), e.getX(), e.getY());
                    }
                } else if (e.getClickCount() == 2) {
                    if (!selPath.toString().contains(".")) {
                        return;
                    }
                    LoadEditor(ParseTreeSelectPath(selPath.toString()), "jTextArea5");

                }
            }

        }

    };

    MouseListener mlma = new MouseAdapter() {

        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                treePopup2.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        ;
        
        public void mousePressed(MouseEvent e) {

            int selRow = jTree2.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = jTree2.getPathForLocation(e.getX(), e.getY());

            if (selRow != -1) {
                if (e.getClickCount() == 1) {
                    if (e.isPopupTrigger()) {
                        treePopup2.show(e.getComponent(), e.getX(), e.getY());
                    }
                } else if (e.getClickCount() == 2) {
                    if (!selPath.toString().contains(".")) {
                        return;
                    }
                    LoadEditor(ParseTreeSelectPath(selPath.toString()), "newTextPane2");
                }
            }

        }

    };

//    
//private void Directory(){
    private Boolean Browse(String path, String FileOrDirectory, JTextField field) {
        Boolean SetPath = false;
        JFrame frame = new JFrame("");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        try {
            JFileChooser fileChooser = new JFileChooser(path);
            if (FileOrDirectory.equalsIgnoreCase(Constants.Directory)) {
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            } else {
                fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            }

            int option = fileChooser.showOpenDialog(frame);
            if (option == JFileChooser.APPROVE_OPTION) {
                Boolean found = false;
                File file = fileChooser.getSelectedFile();
                String name = "er-metrics.jar";
                if (field != null) {
                    if (field.getName().equalsIgnoreCase("ERMRootTxt")) {

                        File[] list = file.listFiles();
                        if (list != null) {
                            for (File fil : list) {
                                if (name.equalsIgnoreCase(fil.getName())) {

                                    String newPath = file.getPath().replace("\\", "/");
                                    if (newPath.contains(":")) {
                                        newPath = newPath.substring(newPath.indexOf(":") + 1);
                                    }

                                    field.setText(newPath);
                                    found = true;
                                    SetPath = true;

                                }
                            }
                        }

                        if (found == false) {
                            JOptionPane jOptionPane = new JOptionPane();
                            JFrame messageFrame = new JFrame("Directory Not Correct!");
                            jOptionPane.showMessageDialog(messageFrame, "Directory Must Contain the ERMetrics Jar File!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));
                            return false;
                        }
                    } else if (field.getName().equalsIgnoreCase("ERMJarTxt")) {

                        if (file.getPath().toUpperCase().contains(name.toUpperCase())) {
                            String newPath = file.getPath().replace("\\", "/");
                            if (newPath.contains(":")) {
                                newPath = newPath.substring(newPath.indexOf(":") + 1);
                            }

                            field.setText(newPath);
                            SetPath = true;
                        } else {
                            JOptionPane jOptionPane = new JOptionPane();
                            JFrame messageFrame = new JFrame("Jar File Not Correct!");
                            jOptionPane.showMessageDialog(messageFrame, "The er-metrics.jar File Must Be Chosen !", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));
                            return false;
                        }
                    } else {

                        String aPath = file.getCanonicalPath().toString().replace("\\", "/");

                        if (aPath.toLowerCase().contains("oystertestsuite") && aPath.toLowerCase().contains("oyster")) {
                            //   String newPath = file.getPath().replace("\\", "/");
                            if (isWindows) {

                                if (aPath.contains(":")) {
                                    aPath = aPath.substring(aPath.indexOf(":") + 1);
                                }
                                field.setText(aPath);
                            } else {

                                field.setText(aPath);

                            }
                            SetPath = true;
                        } else {
                            JOptionPane jOptionPane = new JOptionPane();
                            JFrame messageFrame = new JFrame("Incorrect Directory!");
                            jOptionPane.showMessageDialog(messageFrame, "The Directory choice must be under OysterTestSuite/Oyster!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));
                            return false;
                        }
                    }

                } else {
                    this.TestSuiteXMLFile = file.getPath().replace("\\", "/") + "/TestSuite.xml";
                }

            } else {
                //label.setText("Open command canceled");
                return SetPath;
            }

            //  SetPath = true;
        } catch (Exception et) {
            et.printStackTrace();
        }
        return SetPath;
    }
    // Only two of these used but all must be implemented
    // to get the windowOpened and windowClosing Events needed
    //Used to save the jTable values for run history 

    public void windowClosing(WindowEvent e) {

        try {
            //write out the run history
            //moved this to after run complete
            // WriteTableOut(this.jTable1);
        } catch (Exception et) {
            et.printStackTrace();
        }

        //shut down
        System.exit(0);

    }

    //Used to load the file back in on startup of the program
    public void windowOpened(WindowEvent e) {
//        jTree1.expandRow(0);
//        jTree2.expandRow(0);
//        jTree2.expandRow(2);
        try {
            if (isWindows) {
                File path = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "JTable1Data.data");

                if (path.exists() && path.length() > 0) {
                    LoadTable(this.jTable1, path.toString());
                } else {
                    path = new File(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS + "Datafiles/JTable1Data.data");
                    LoadTable(this.jTable1, path.toString());
                }

            } else {
                File path = new File("./RunLogs/JTable1Data.data");

                if (path.exists() && path.length() > 0) {
                    LoadTable(this.jTable1, path.toString());
                } else {
                    path = new File("./RunLogs/Datafiles/JTable1Data.data");
                    LoadTable(this.jTable1, path.toString());
                }
            }
        } catch (Exception ex) {
            System.out.println("Error Loading RunHistory Table on Startup is " + ex.getMessage());
        }

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }

    public void windowClosed(WindowEvent e) {

    }

    class TreePopup extends JPopupMenu {

        public TreePopup(JTree tree) {

            JMenuItem delete = new JMenuItem("Delete Selected File");
            JMenuItem copy = new JMenuItem("Make Copy of Selected File");
            delete.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {

                    if (tree.getLeadSelectionPath() != null) {
                        File aCopy = new File(ParseTreeSelectPath(tree.getLeadSelectionPath().toString()));
                        if (aCopy.isFile()) {
                            aCopy.delete();
                            //cannot tell which jtree so...

                            LoadjTree(1);
                            LoadjTree(3);
                            LoadjTree(2);
                            tree.repaint();;
                        } else {
                            JOptionPane jOptionPane = new JOptionPane();
                            JFrame messageFrame = new JFrame("Directory Not File");
                            jOptionPane.showMessageDialog(messageFrame, "Directories Cannot be Deleted Here!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));
                        }
                    }
                }
            });
            copy.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    if (tree.getLeadSelectionPath() != null) {
                        File original = new File(ParseTreeSelectPath(tree.getLeadSelectionPath().toString()));
                        File theCopy = new File(ParseTreeSelectPath(tree.getLeadSelectionPath().toString()) + "copy");
                        if (original.isFile()) {
                            try {
                                if (theCopy.exists()) {
                                    return;
                                }

                                Files.copy(original.toPath(), theCopy.toPath());
                                tree.repaint();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            LoadjTree(1);
                            LoadjTree(3);
                            LoadjTree(2);
                            tree.repaint();
                        } else {
                            JOptionPane jOptionPane = new JOptionPane();
                            JFrame messageFrame = new JFrame("Directory Not File");
                            jOptionPane.showMessageDialog(messageFrame, "Directories Cannot be Copied Here!", Constants.UALR, 1, new ImageIcon(OysterTestSuite.class.getResource(Constants.UalrJpg)));
                        }
                    }
                }
            });
            add(delete);
            add(new JSeparator());
            add(copy);

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ERMJarTxt;
    private javax.swing.JTextField ERMOysterRootTxt;
    private javax.swing.JTextField ERMRootTxt;
    private javax.swing.JButton HomeBtn;
    private javax.swing.JTextField OysterDirLinkFileTxt;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea5;
    private javax.swing.JTextArea jTextArea6;
    private javax.swing.JTextArea jTextArea7;
    private javax.swing.JTree jTree1;
    private javax.swing.JTree jTree2;
    private javax.swing.JTree jTree4;
    // End of variables declaration//GEN-END:variables
}
