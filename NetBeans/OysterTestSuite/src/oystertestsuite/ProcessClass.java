/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author UALR
 */
public class ProcessClass {

    

    String sys = System.getProperty("os.name");

    private File getPropFile(){
        File configFile;
         
           if (sys.toUpperCase().contains("WINDOWS")) {
            configFile = new File("/OysterTestSuite/config.xml");
            
        } else {
            configFile = new File("./configLinux.xml");
        }
           return configFile;
    }
    public String ReadAProperty(String Prop) {

     
        String value = "";
        try {
            // FileReader reader = new FileReader(configFile);
            // InputStream inputStream = ProcessClass.class.getResourceAsStream("C:\\OysterTestSuite\\config.xml");
            InputStream inputStream = new FileInputStream(getPropFile());
            Properties props = new Properties();
            props.loadFromXML(inputStream);

            value = props.getProperty(Prop);

            //        System.out.print("Value is: " + value);
            inputStream.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
             System.out.println(ex.getMessage());
        }
        return value;
    }

    public void WriteToProp(String prop, String value) {

        try {
         
            // FileReader reader = new FileReader(configFile);
            //        InputStream inputStream = ProcessClass.class.getResourceAsStream("C:/OysterTestSuite/config.xml");
            //     InputStream inputStream = new FileInputStream(configFile);
            Properties props = new Properties();
            //     props.loadFromXML(inputStream);
            File configFile = getPropFile();
       //     File configFile = new File("/OysterTestSuite/config.xml");
            props.setProperty(prop, value);
            FileOutputStream outputStream = new FileOutputStream(configFile);
            props.storeToXML(outputStream, "OysterTestSuite Settings");

            //   System.out.print("Host name is: " + host);
            outputStream.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            // file does not exist
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public void WriteToProp(Map<String, String> map) {

        try {
            // FileReader reader = new FileReader(configFile);
            //        InputStream inputStream = ProcessClass.class.getResourceAsStream("C:/OysterTestSuite/config.xml");
            //     InputStream inputStream = new FileInputStream(configFile);
            Properties props = new Properties();
            //     props.loadFromXML(inputStream);

            File configFile = getPropFile();
            //   props.setProperty(prop, Value);
            FileOutputStream outputStream = new FileOutputStream(configFile);
            Set<Map.Entry<String, String>> entries = map.entrySet();
            //get the jText
            for (Map.Entry<String, String> entry : entries) {

                String name = entry.getKey();
                String value = entry.getValue();
                props.setProperty(name, value);

            }

            props.storeToXML(outputStream, "ERMetrics settings");
            outputStream.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            // file does not exist
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

}
