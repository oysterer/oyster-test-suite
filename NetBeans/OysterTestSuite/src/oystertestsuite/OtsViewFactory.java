/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

import javax.swing.text.Element;
import javax.swing.text.PlainView;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

/**
 *
 * @author UALR
 */
public class OtsViewFactory extends Object implements ViewFactory {

    /**
     *
     */
    public View create(Element element) {

        return new PlainView(element);

    }

}
