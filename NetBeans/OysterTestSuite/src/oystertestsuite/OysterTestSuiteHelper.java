/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JTextArea;

/**
 * Move some duplicated code out to one place
 * for easy reuse.
 *
 * @author UALR
 */
public class OysterTestSuiteHelper {

    private String newline = System.getProperty(Constants.LineSeparator);
    DateFormat dateFormat = new SimpleDateFormat(Constants.DateFormatString);
    Date date = new Date();

    /*
    * Write the header that is each recoreded Error.  Header is Always the same
     so this could be refactored here to write to the text area and the TestSuite.log
     */
    public void WriteHeader(OysterTestBean oBean, OysterTestBean myBean, LogWriter TestSuiteLog, JTextArea jTextArea2) {
        jTextArea2.append("--------------" + newline);
        jTextArea2.append(myBean.getTestCase_Name() + newline);
        jTextArea2.append(oBean.getTestSuiteOysterToJarRun().trim() + newline);
        jTextArea2.append(newline);
        TestSuiteLog.WriteToLog("--------------");
        TestSuiteLog.WriteToLog(myBean.getTestCase_Name());
        TestSuiteLog.WriteToLog(oBean.getTestSuiteOysterToJarRun().trim() + newline);
    }

    /*
    * Write the end of oyster run
     */
    public void WriteEndOfOysterRun(OysterTestBean myBean, LogWriter TestSuiteLog, JTextArea jTextArea2) {
        jTextArea2.append(newline + "End of " + myBean.getTestCase_Name() + newline);
        jTextArea2.append("--------------" + newline);
        TestSuiteLog.WriteToLog(newline + "End of " + myBean.getTestCase_Name());
        TestSuiteLog.WriteToLog("--------------");

    }

    /*
    * Write the end of the python run
     */
    public void WriteEndOfPythonRun(LogWriter TestSuiteLog, JTextArea jTextArea2) {
        jTextArea2.append(newline + "End of OysterTestSuite Run at " + dateFormat.format(new Date()) + newline + newline);
        //TestSuiteLog.WriteToLog("_________________________________________________");
        TestSuiteLog.WriteToLog(newline + newline + "End of OysterTestSuite Run at " + dateFormat.format(new Date()) + newline + newline);
       // TestSuiteLog.WriteToLog("________________________________________________");

    }

    /*
      Close file wirters
     */
    public void CloseWriters(LogWriter TestSuiteLog, LogWriter ChangeRptLog) {
        try {
            if (TestSuiteLog != null) {
                TestSuiteLog.Flush();
                TestSuiteLog.Close();
                TestSuiteLog = null;
            }
            if (ChangeRptLog != null) {
                ChangeRptLog.Flush();
                ChangeRptLog.Close();
                ChangeRptLog = null;
            }
        } catch (Exception e) {
            //who cares
        }

    }

    /*
        Write the error to the respective textarea and TestSuite Log depending on what is passed as the Switch value
     */
    public void WriteFooter(String Switch, OysterTestBean oBean, OysterTestBean myBean, LogWriter TestSuiteLog, JTextArea jTextArea2, String line) {

        switch (Switch) {

            case "Total Records Processed":

                jTextArea2.append("Error in Expected Log References Count vs. Log File References Count" + newline);
                jTextArea2.append("Expected Log References Count : " + myBean.getLogRefExpectedCount() + newline);
                jTextArea2.append("Log File References Count : " + Integer.parseInt(line) + newline);

                TestSuiteLog.WriteToLog("Error in Log Expected References Count vs. Log File References Count" + newline, "Expected Log References Count : " + myBean.getLogRefExpectedCount() + newline);
                TestSuiteLog.WriteToLog("Log File References Count : " + Integer.parseInt(line));
                break;
            case "Total Clusters":

                jTextArea2.append("Error in Expected Log Cluster Count vs. Log File Cluster Count" + newline);
                jTextArea2.append("Expected Log Cluster Count : " + myBean.getLogClusterExpectedCount() + newline);
                jTextArea2.append("Log File Cluster Count : " + Integer.parseInt(line) + newline);

                TestSuiteLog.WriteToLog("Error in Log Expected Cluster Count vs. Log File Cluster Count");
                TestSuiteLog.WriteToLog("Expected Cluster Count : " + myBean.getLogClusterExpectedCount());
                TestSuiteLog.WriteToLog("Log File Cluster Count : " + Integer.parseInt(line));

                break;
            case "Link File Reference Count":
                //counter change to line, same string counting variable
                jTextArea2.append("Error in Expected Link Refernces Count vs. Link File References Count" + newline);
                jTextArea2.append("Expected Link References Count : " + myBean.getRefExpectedCount() + newline);
                jTextArea2.append("Link File References Count : " + line + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Link Expected Refernces Count vs. Link File Reference Count" + newline, "Expected Link References Count : " + myBean.getRefExpectedCount() + newline);
                TestSuiteLog.WriteToLog("Link File References Count : " + line, newline);

                break;
            case "Link File Cluster Count":

                jTextArea2.append("Error in Expected Link Cluster Count vs. Link File Cluster Count" + newline);
                jTextArea2.append("Expected Link Cluster Count : " + myBean.getLinkClusterExpectedCount() + newline);
                jTextArea2.append("Link File Cluster Count : " + line + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Expected Link Cluster Count vs. Link File Cluster Count" + newline, "Expected Link Cluster Count : " + myBean.getLinkClusterExpectedCount() + newline);
                TestSuiteLog.WriteToLog("Link File Cluster Count : " + line, newline);

                break;
            case "Count of Output Identities":

                jTextArea2.append("Error in Expected Output Identities Count vs. Change Report Output Identities Count" + newline);
                jTextArea2.append("Expected Output Identities Count : " + myBean.getChangeReportExpectOutputIdentities() + newline);
                jTextArea2.append("Change Report Output Identities Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Expected Output Identities Count vs. Change Report Output Identities Count" + newline, "Expected Output Identities Count : " + myBean.getChangeReportOutputIdentities() + newline);
                TestSuiteLog.WriteToLog("Change Report Output Identities Count : " + myBean.getChangeReportExpectOutputIdentities());
                TestSuiteLog.WriteToLog("Change Report Output Identities Count : " + Integer.parseInt(line), newline);

                break;
            case "Identities Updated and Written to Output":

                jTextArea2.append("Error in Identities Updated and Written to Output Count vs. Change Report Identities Updated and Written to Output Count" + newline);
                jTextArea2.append("Expected Identities Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesUpdated() + newline);
                jTextArea2.append("Change Report Identities Updated and Written to Output Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Expected Output Identities Count vs. Change Report Output Identities Count" + newline, "Expected Identities Updated and Written to Output Count :" + myBean.getChangeReportExpectInputIdentitiesUpdated() + newline);
                TestSuiteLog.WriteToLog("Change Report Identities Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesUpdated(), newline);
                TestSuiteLog.WriteToLog("Change Report Identities Updated and Written to Output Count : " + Integer.parseInt(line), newline);

                break;
            case "Input Identities Not Updated and Written to Output":

                jTextArea2.append("Error in Input Identities Not Updated and Written to Output Count vs. Change Report Input Identities Not Updated and Written to Output Count" + newline);
                jTextArea2.append("Expected Input Identities Not Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesNotUpdated() + newline);
                jTextArea2.append("Change Report Input Identities Not Updated and Written to Output Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Input Identities Not Updated and Written to Output Count vs. Change Report Input Identities Not Updated and Written to Output Count" + newline, "Expected Input Identities Not Updated and Written to Output Count : " + myBean.getChangeReportExpectInputIdentitiesNotUpdated() + newline);
                TestSuiteLog.WriteToLog("Change Report Input Identities Not Updated and Written to Output Count : " + Integer.parseInt(line), newline);

                break;
            case "Count of Input Identities Merged":

                jTextArea2.append("Error in Input Identities Merged Count vs. Change Report Input Identities Merged Count" + newline);
                jTextArea2.append("Expected Input Identities Merged Count : " + myBean.getChangeReportExpectInputIdentitiesMerged() + newline);
                jTextArea2.append("Change Report Input Identities Merged Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Input Identities Merged Count vs. Change Report Input Identities Merged Count" + newline, "Expected Input Identities Merged Count : " + myBean.getChangeReportExpectInputIdentitiesMerged() + newline);
                TestSuiteLog.WriteToLog("Change Report Input Identities Merged Count : " + Integer.parseInt(line), newline);

                break;
            case "Count of New Identities Created":

                jTextArea2.append("Error in New Identities Created Count vs. Change Report New Identities Created Count" + newline);
                jTextArea2.append("Expected New Identities Created Count : " + myBean.getChangeReportExpectNewInputIdentitiesCreated() + newline);
                jTextArea2.append("Change Report New Identities Created Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in New Identities Created Count vs. Change Report New Identities Created Count" + newline, "Expected New Identities Created Count : " + myBean.getChangeReportExpectNewInputIdentitiesCreated() + newline);
                TestSuiteLog.WriteToLog("Change Report New Identities Created Count : " + Integer.parseInt(line), newline);

                break;
            case "Count of Error Identities":

                jTextArea2.append("Error in Error Identities Count vs. Change Report Error Identities Count" + newline);
                jTextArea2.append("Expected Error Identities Count : " + myBean.getChangeReportExpectErrorIdentities() + newline);
                jTextArea2.append("Change Report Error Identities Count: " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error in Error Identities Count vs. Change Report Error Identities Count : " + newline, "Expected Error Identities Count : " + myBean.getChangeReportExpectErrorIdentities() + newline);
                TestSuiteLog.WriteToLog("Change Report Error Identities Count: " + Integer.parseInt(line), newline);

                break;
            case "Count of Input Identities:":

                jTextArea2.append("Error Input Identities Count vs. Change Report Input Identities Count" + newline);
                jTextArea2.append("Expected Input Identities Count : " + myBean.getChangeReportExpectInputIdentities() + newline);
                jTextArea2.append("Change Report Input Identities Count : " + Integer.parseInt(line) + newline);
                jTextArea2.append(newline);

                TestSuiteLog.WriteToLog("Error Input Identities Count vs. Change Report Input Identities Count" + newline, "Expected Input Identities Count : " + myBean.getChangeReportExpectInputIdentities() + newline);
                TestSuiteLog.WriteToLog("Expected Input Identities Count : " + myBean.getChangeReportExpectInputIdentities());
                TestSuiteLog.WriteToLog("Change Report Input Identities Count : " + Integer.parseInt(line), newline);

                break;
            default:

                break;
        }

    }

    /*
    Clear Text Areas, Write info to the TestSuiteLog and RunIssues text area
     */
    public void GetLogsReadyToGo(JTextArea jTextArea1, JTextArea jTextArea2, LogWriter TestSuiteLog, OysterTestBean oBean) {

        jTextArea1.selectAll();
        jTextArea1.setText(null);
        jTextArea2.selectAll();
        jTextArea2.setText(null);

        jTextArea2.append(Constants.RunStart + dateFormat.format(new Date()) + newline);
        jTextArea2.append(oBean.getTestSuiteOysterToJarRun().trim() + newline);

        TestSuiteLog.WriteToLog("-----------------------------------------------------");
        TestSuiteLog.WriteToLog(Constants.RunStart  + dateFormat.format(new Date()));
        TestSuiteLog.WriteToLog(oBean.getTestSuiteOysterToJarRun());
        TestSuiteLog.WriteToLog("-----------------------------------------------------");

        if (TestSuiteLog != null) {
            TestSuiteLog.Flush();
            TestSuiteLog.Close();
            TestSuiteLog = null;
        }
    }

    /*
    Checks and renames log files that have gotten to large > 300K
     */
    public void CheckLogFileSizes(OysterTestBean oBean) {
         String sys = System.getProperty("os.name");
        if(sys.toUpperCase().contains("WINDOWS")){
        try {
            int random = (int)(Math.random() * 36550 + 1);
            String fileExtension = String.valueOf(random) + String.valueOf(Calendar.MILLISECOND) + Long.toString(date.getTime());
            ArrayList<Path> FilesToCheck = new ArrayList<Path>();
            Path p = Paths.get(oBean.getTestSuiteOysterRoot() + "/");

            Files.find(p, Integer.MAX_VALUE, (filePath, fileAttr) -> fileAttr.isRegularFile() && filePath.toString().endsWith("txt")
                    && (filePath.toString().contains("ReferenceAnd") || filePath.toString().contains("IdentitiesCount"))
                    && new File(filePath.toString()).length() > 300000).forEach(FilesToCheck::add);

            for (Path pi : FilesToCheck) {
                new File(pi.toString()).renameTo(new File(pi.toString() + fileExtension));
            }

            ArrayList<Path> LogsToCheck = new ArrayList<Path>();
            Path l = Paths.get(oBean.getTestSuiteWorkspace() + Constants.RUNLOGS);
            Files.find(l, Integer.MAX_VALUE, (filePath, fileAttr) -> fileAttr.isRegularFile() && filePath.toString().endsWith("log")
                    && (filePath.toString().contains("TestSuite") || filePath.toString().contains("PythonRunfile"))
                    && new File(filePath.toString()).length() > 300000).forEach(LogsToCheck::add);

            for (Path li : LogsToCheck) {
              //  System.out.println(li.toString());
                if(li.toString().contains("TestSuite.log") || li.toString().contains("PythonRunfile.log")){
                new File(li.toString()).renameTo(new File(li.toString() + fileExtension));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    }
}
