/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oystertestsuite;

/**
 *
 * @author UALR
 */
public class Constants {

    public static final String newline = System.getProperty(Constants.LineSeparator);
    public static final String IdentityChangeReport = "Identity Change Report.txt";
    public static final String IdentityChangeReportLinux = "Identity Change Report.txt";
    public static final String OUTPUT = "/Output/";
    public static final String RUNLOGS = "/RunLogs/";
    public static final String RUNLOGSLINUX = "./RunLogs";
    public static final String SCRIPTS = "/Scripts/";
    public static final String LineSeparator = "line.separator";
    public static final String DateFormatString = "MM/dd/yyyy HH:mm";
    public static final String AboutBoxString = "Oyster Test Suite " + newline + "Regression Testing";
    public static final String UALR = "Universtiy of Arkansas at Little Rock";
    public static final String RunStart = "New OysterTestSuite Run at ";
    public static final String UalrJpg = "/ualr2.jpg";
    public static final String TestSuiteXML = "TestSuite.xml";
    public static final String ABOUT = "ABOUT";
    public static final String SavedFile = "Saved File";
    public static final String ChangeReport = "ChangeReport";
    public static final String ClusterRef = "ClusterRef";
    public static final String NewTextPane = "newTextPane";
    public static final String Directory = "Directory";
    public static final String File = "File";
    public static final String TASKLIST = "tasklist";
    public static final String KILL = "TASKKIll /F /PID ";
    public static final String BrowseButton = "Please use the Browse Button!";
    public static final String ERMetricsRootDir = "ERMetricsRootDir";
    public static final String ERMetricsJar = "ERMetricsJar";
    public static final String ERMetricsOysterDir = "ERMetricsOysterDir";
    public static final String KillJava = "Are you SURE you want to Kill the RUN? This only works for Windows!";
    public static final String KillPython = "Are you SURE you want to Kill the Run? Linux Script will kill Python3.7 and Oyster Java processes!";
    public static final String LoadedFileSaved = "The Loaded File is Saved!";
    public static final String LoadedFileNotSaved = "The Loaded File was Not Saved!  Load again and try Changes";
    public static final String ThreeBoxesFilled = "All Three Text Boxes must be filled to Save!";
    public static final String StopJava = "Stop Java?";
}
