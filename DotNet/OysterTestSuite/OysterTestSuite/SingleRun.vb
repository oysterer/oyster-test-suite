﻿Imports System.IO
Imports System.Windows.Forms

Public Class SingleRun
    Protected Overrides Sub OnLoad(e As EventArgs)
        'If File.Exists(My.Settings.TestSuiteRootDir & "\SingleRunFileNames.txt") Then
        If File.Exists(My.Settings.TestSuiteRootDir & "\TestSuite.xml") Then

            Dim StartParsing As Boolean = False

            Using objReader As New StreamReader(My.Settings.TestSuiteRootDir & "\TestSuite.xml")
                Do While objReader.Peek() <> -1
                    Dim line As String = objReader.ReadLine()
                    If Not String.IsNullOrWhiteSpace(line) And Not line.ToUpper.Contains("TESTS") And StartParsing And line.ToUpper.Contains("TESTCASE") Then
                        line = line.Substring(line.IndexOf(">") + 1)
                        line = line.Substring(0, line.IndexOf("<"))


                        CheckedListBox1.Items.Add(line)
                    Else
                        If line.ToUpper.Contains("TESTS") Then
                            StartParsing = True
                        End If
                    End If


                    '   CheckedListBox1.Items.Add(line)
                Loop

            End Using
        Else
            MessageBox.Show(My.Settings.TestSuiteRootDir & "\TestSuite.xml" & vbLf & " Does Not Exist!", "Permissions Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        End If




    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        Dim itemChecked As Object



        For Each itemChecked In CheckedListBox1.CheckedItems

            OTSHelper.OneRunXMLFile = itemChecked.ToString()
            '  MsgBox(OTSHelper.OneRunXMLFile)
        Next
        If OTSHelper.OneRunXMLFile = Nothing Then
            MessageBox.Show("You must Choose a Test to Run or Cancel!", "Opps Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else
            '  Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub CheckedListBox1_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        If e.NewValue = CheckState.Checked Then
            For i As Integer = 0 To Me.CheckedListBox1.Items.Count - 1 Step 1
                If i <> e.Index Then
                    Me.CheckedListBox1.SetItemChecked(i, False)
                End If
            Next i
        End If
    End Sub
End Class
