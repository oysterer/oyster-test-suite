﻿Imports System.IO
Imports System.Security.Permissions
Imports System.Text

''''<summary>
''''     Oyster Helper and ERMetrics Helper called Oyster Helper
'''' '    
''''' Classes
''''     Constants - Shared String Constants as Protected Shared Const for use in the program mainly for Messaging and constants representing objects such as Tabs,RichTextBoxes,etc.
''''     HelperBean - Bean with Getters and Setters for working storage and working with file saving in Treeviews, etc.
''''     HelperTabControl - On Tab Change Class - Makes sure the tab chosen is displayed correctly for existing and saved input data.
''''     HelpForm - Help. 
''''     OysterForm - This file. Main Windows Application Form with button clicks etc, Methods with IO, etc., moved to OysterHelper Class as good programming standard procedure.
''''     OysterHelper - Guts of the Work Methods, See file for list of methods contained in the class
''''     ProcessClass - Run Method Only to start Window Process CMD.EXE for Running Oyster and ERMetrics in Java and Sending Output Back to OysterHelper
''''     FormResizer - In Case you Cannot See, Makes OysterHelper Large and Then Small Again when desired.
''''     ReadFileForm - shows the testing file created
''''     TesterRows - Holds the Output Rows for testing
''''     TestHelper - Holds the majority of The Testing methods
''''     TestOutPutForm - Holds the testing output in a datagridview
''''     TestSignInForm - used to sign into the Testing Tab
''''     
''''     A Second Project OysterGUI creates the install for Oyster Helper
''''
''''
''''
'''' </summary>
'''''

Imports System.Threading

Public Class Oysterform

    'Used by the Oyster & ERMetrics process when running to write to correspond.. to the screen in this case
    Private Delegate Sub ConsoleOutputDelegate(ByVal outputString As String)
    Private Delegate Sub ConsoleErrorDelegate(ByVal errorString As String)

    'used to resize the program if chosen
    Private RS As New FormResizer

    'HashTable used to hold helperbeans with RichTextBox loaded file save Information
    Protected Friend saveInfoCollection As New Hashtable()

    'OysterHelper used to have form I.O. work and storage
    Private oysterHelper As New OysterHelper

    'we have to keep up with what is happening
    Private helperBean As New HelperBean



    'used to save helperbeans for easy access
    Protected Friend list As New List(Of HelperBean)()

    'keep the tab choices close And under control And re-loaded On tabclick
    Protected Friend helperTabControl As New HelperTabControl

    'used to get elapsed time in oysterRun
    Protected Friend stopWatch As New Stopwatch
    'used to load and unload TestTab

    'used for tab hiding and showing
    Protected Friend ListA As New List(Of TabPage)()

    'used by assertion tab 
    Private selectedrb As RadioButton
    Private dta As DataTable = Nothing


    '-----------------------------------------------------------------
    'Startup and load the screens if saved info available
    '-----------------------------------------------------------------

    Protected Overrides Sub OnLoad(e As EventArgs)

        Me.DoubleBuffered = True
        'for resizingu
        RS.FindAllControls(Me)
        'want to save any user input when exiting
        My.Application.SaveMySettingsOnExit = True

        Dim f As New FileIOPermission(PermissionState.None)
        f.AllLocalFiles = FileIOPermissionAccess.Append
        Try
            f.Demand()
        Catch s As Exception
            MessageBox.Show(s.Message, "Permissions Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        ToolStripStatusLabel1.Text = Constants.OHelper
        ToolStripStatusLabel1.Visible = True
        ToolStripProgressBar1.Visible = False
        RunScriptNamelbl.Text = Nothing
        InputFileLbl.Text = Nothing
        LinkFilelbl.Text = Nothing
        Timelbl.Text = Nothing
        LFDateLbl.Text = Nothing
        RunTimeValuelbl.Text = Nothing

        'To use User changeable settings you must load them
        'on startup to then load the TextBoxes in OysterHelper 
        'once saved in the program.
        My.Settings.Reload()


        'link label for Entity Resolution Scoring Rule Weights Generator
        LinkLabel1.Links.Add(0, 48, "https://yxye1.shinyapps.io/shinyapp/")
        'oysterHelper.RunProcesTabOnLoad(Constants.Welcome)

        'For Each page As TabPage In OysterTabs.TabPages
        '    oysterHelper.RunProcesTabOnLoad(page.Name)
        'Next


        'if user settings saved..load the textboxes with them
        OysterRootDirTxBx.Text = My.Settings.OysterRootDir
        OysterWorkDirTxBx.Text = My.Settings.OysterWorkingDir
        ERMetricsTxBx.Text = My.Settings.ERMetricsWorkDir
        OysterJarTxBx.Text = My.Settings.OysterJar
        ERMetricsJarTxBx.Text = My.Settings.ERMetricsJar
        'Oyster error label when an Error is returned from the Running Oyster Instance
        ErrorInRunLbl.Visible = False
        WelLoadedFileLbl.Text = Constants.FileLoadedIs

        'Oyster Working directory for ERMetrics comes from the Oyster Tab
        'since there is no way to load it from the ERMetrics Screen
        If Not String.IsNullOrEmpty(My.Settings.EROysterWorkDir) And My.Settings.EROysterWorkDir.Equals(My.Settings.OysterWorkingDir) _
            And Not String.IsNullOrEmpty(My.Settings.OysterRootDir) Then

            EROysterWkDirTxBx.Text = My.Settings.EROysterWorkDir

            LFDateLbl.Text = Nothing
            HSLinkDateLbl.Text = Nothing

            If String.IsNullOrEmpty(ERMetLinkFileTxbx.Text) And File.Exists(ERMetLinkFileTxbx.Text) Then

                ERMetLinkFileTxbx.Text = OysterHelper.GetLinkFileName(My.Settings.OysterRootDir, My.Settings.EROysterWorkDir)
                LFDateLbl.Text = IO.File.GetLastWriteTime(ERMetLinkFileTxbx.Text)
                HSLinkDateLbl.Text = LFDateLbl.Text
                HSCsvTxBx.Text = OysterHelper.GetInputCSVFileName(EROysterWkDirTxBx.Text)
                HSLinkFileTxBx.Text = ERMetLinkFileTxbx.Text
                My.Settings.ERMetricsLinkFile = ERMetLinkFileTxbx.Text
            Else
                If File.Exists(My.Settings.ERMetricsLinkFile) Then
                    ERMetLinkFileTxbx.Text = My.Settings.ERMetricsLinkFile
                    HSLinkFileTxBx.Text = My.Settings.ERMetricsLinkFile
                End If

            End If

            EROysterWkDirTxBx.ReadOnly = True
            ERMetLinkFileTxbx.ReadOnly = True
            HSLinkFileTxBx.ReadOnly = True
            HSCsvTxBx.ReadOnly = True

        Else
            EROysterWkDirTxBx.Text = My.Settings.OysterWorkingDir
            EROysterWkDirTxBx.ReadOnly = True
            HSLinkFileTxBx.ReadOnly = True
            My.Settings.EROysterWorkDir = My.Settings.OysterWorkingDir
            HSCsvTxBx.ReadOnly = True
        End If

        My.Settings.Save()

        'load the RunScripts if the Directories are already set
        If Not String.IsNullOrEmpty(OysterRootDirTxBx.Text) And OysterRootDirTxBx.Text.ToUpper.Contains(Constants.OYSTER) _
            And Not String.IsNullOrEmpty(OysterWorkDirTxBx.Text) And Not String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            OysterXmlListBx.DataSource = IO.Directory.GetFiles(OysterRootDirTxBx.Text, "*.xml") _
            .Select(Function(file) IO.Path.GetFileName(file)) _
            .ToList
            If Not My.Settings.LastRunFile = Nothing Then
                OysterXmlListBx.SelectedItem = My.Settings.LastRunFile
            End If
            'allow Oyster Button to be available to Run Oyster
            RunOyster.Enabled = True
            'If all is well, load the RichTextbox on the Oyster Setup and Run Tab
            ' Dim oysterHelper As New OysterHelper
            oysterHelper.RunProcessTab(Constants.Welcome)
            ' oysterHelper = Nothing
        Else
            RunOyster.Enabled = False
        End If

        If Not My.Settings.Testing Then

            ' Hide the testing tab
            Dim tab As TabPage = OysterTabs.TabPages.Item("TestTab")
            EnablePage(tab, False)

        End If

    End Sub

    '-----------------------------------------------------------------  
    'Save the Welcome/Setup and Run Oyster Tab
    '-----------------------------------------------------------------

    Private Sub SaveAll_Click(sender As Object, e As EventArgs) Handles SaveAll.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) _
            Or String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            MessageBox.Show(Constants.SaveMessage, "Requirement", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            Me.oysterHelper.Save_All_WelcomeTab()
        End If


    End Sub

    '-----------------------------------------------------------------
    'go to the first tab
    '-----------------------------------------------------------------
    Private Sub HomeBtn_Click(sender As Object, e As EventArgs) Handles HomeBtn.Click
        OysterTabs.SelectTab(0)
    End Sub


    'Close the program from the Exit button in the toolstrip menu
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Dispose()
    End Sub


    '-----------------------------------------------------------------
    ' The 5 Main Text Boxes are checked for accuracy and input from  
    ' the keyboard.  The Browse buttons should be used to           
    ' fill any textboxes, empty or already filled. Andy change      
    ' to add New textboxes will need to made in these 2 methods 
    ' to prevent random keyboard entry that is just asking for errors
    '-------------------------------------------------------------- --

    'check for textchanged by space or new entry in OyterRootDirTxtBx
    Private Sub RootDirTxtBx_TextChanged(sender As Object, e As EventArgs) Handles OysterRootDirTxBx.TextChanged
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterRootDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterRootDirTxBx)
        Else
            oysterHelper.TextChanged(OysterRootDirTxBx)
        End If

    End Sub

    'check for textchanged by space or new entry in  OysterWorkDirTxBx
    Private Sub OysterWorkDirTxBx_TextChanged(sender As Object, e As EventArgs) Handles OysterWorkDirTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterWorkDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterWorkDirTxBx)
        Else
            oysterHelper.TextChanged(OysterWorkDirTxBx)
        End If

    End Sub

    'check for textchanged by space or new entry in ERMetricsTxBx
    Private Sub ERMetricsTxBx_TextChanged(sender As Object, e As EventArgs) Handles ERMetricsTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(ERMetricsTxBx.Text) Then
            oysterHelper.TextChangedSpace(ERMetricsTxBx)
        Else
            oysterHelper.TextChanged(ERMetricsTxBx)
        End If


    End Sub

    'check for textchanged by space or new entry in OysterJarTxBx
    Private Sub OysterJarTxBx_TextChanged(sender As Object, e As EventArgs) Handles OysterJarTxBx.TextChanged
        '
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterJarTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterJarTxBx)
        Else
            oysterHelper.TextChanged(OysterJarTxBx)
        End If

    End Sub

    'check for textchanged by space or new entry in ERMetricsJarTxtBx
    Private Sub ERMetricsJarTxtBx_TextChanged(sender As Object, e As EventArgs) Handles ERMetricsJarTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(ERMetricsJarTxBx.Text) Then
            oysterHelper.TextChangedSpace(ERMetricsJarTxBx)
        Else
            oysterHelper.TextChanged(ERMetricsJarTxBx)
        End If

    End Sub

    '-------------------------------------------------------
    'end textchanged
    '-------------------------------------------------------

    '------------------------------------------------------------
    ' Browse buttons used to fill textboxres.  Any new textboxes 
    ' will need to be added to the generic FillTextBox method in
    ' Oysterhelper.
    '------------------------------------------------------------
    Private Sub OysterWkDirBtn_Click(sender As Object, e As EventArgs) Handles OysterWkDirBtn.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Then
            MessageBox.Show(Constants.OysterEmpty, "Oyster Root Directory Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            If String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Then
                FolderBrowserDialog1.SelectedPath = OysterRootDirTxBx.Text

            Else
                FolderBrowserDialog1.SelectedPath = OysterWorkDirTxBx.Text
            End If

            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(OysterWorkDirTxBx, FolderBrowserDialog1.SelectedPath)
            End If
        End If

    End Sub


    Private Sub OysterRootDirBtn_Click(sender As Object, e As EventArgs) Handles OysterRootDirBtn.Click

        FolderBrowserDialog1.SelectedPath = Constants.CDrive
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            oysterHelper.FillTextBox(OysterRootDirTxBx, FolderBrowserDialog1.SelectedPath)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub OysterJarBtn_Click(sender As Object, e As EventArgs) Handles OysterJarBtn.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Then
            MessageBox.Show(Constants.OysterRootWkDirRequired, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else

            OpenFileDialog1.InitialDirectory = OysterRootDirTxBx.Text

            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(OysterJarTxBx, OpenFileDialog1.FileName)


            Else
                Exit Sub
            End If
        End If

    End Sub

    Private Sub ERMetricsWkDirBtn_Click(sender As Object, e As EventArgs) Handles ERMetricsWkDirBtn.Click

        FolderBrowserDialog1.SelectedPath = OysterRootDirTxBx.Text
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            oysterHelper.FillTextBox(ERMetricsTxBx, FolderBrowserDialog1.SelectedPath)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub ERMetricJarBtn_Click(sender As Object, e As EventArgs) Handles ERMetricJarBtn.Click

        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Then
            MessageBox.Show(Constants.ERWorkingDirectory, "ERMetrics Working Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            OpenFileDialog1.InitialDirectory = ERMetricsTxBx.Text
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(ERMetricsJarTxBx, OpenFileDialog1.FileName)
            End If
        End If
    End Sub


    '-----------------------------------------------------------------
    '---------------End browse buttons so far ----------------
    '-----------------------------------------------------------------

    '-------------------------------------------------------
    'Run  and stop buttoncalls for Oyster and ERMetrics
    '-----------------------------------------------------------------

    'run oyster
    Private Sub RunOyster_Click(sender As Object, e As EventArgs) Handles RunOyster.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Or String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            MessageBox.Show(Constants.OysterRootWkDirJarRequired, "Oyster", MessageBoxButtons.OK, MessageBoxIcon.Information)
            RunOyster.Enabled = False
            Exit Sub
        End If

        'if run file not right stop it now
        If Not Path.GetFileName(OysterWorkDirTxBx.Text).Equals(OysterXmlListBx.SelectedValue.Substring(0, OysterXmlListBx.SelectedValue.ToUpper.IndexOf("RUN"))) Then
            MessageBox.Show(Constants.OysterWorkDirError, "Oyster", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            helperBean.RichTexBoxNameBool = True
            Me.oysterHelper.RunOyster(helperBean)
        End If

    End Sub

    'run ERMetrics
    Private Sub RunERMetricsBtn_Click(sender As Object, e As EventArgs) Handles RunERMetricsBtn.Click

        Try
            If String.IsNullOrEmpty(ERMetricsTxBx.Text) Or String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then
                MessageBox.Show(Constants.ERMetricsDirChoiceError, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            If Not My.Computer.FileSystem.DirectoryExists(ERMetricsTxBx.Text) Then
                ERMetricsTxBx.Text = Nothing
                MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            ElseIf String.IsNullOrEmpty(My.Settings.ERMetricsWorkDir) Or String.IsNullOrEmpty(My.Settings.ERMetricsJar) Then
                MessageBox.Show(Constants.ERMetricsSaveFile, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else
                helperBean.RichTexBoxNameBool = False
                oysterHelper.RunERMetrics(helperBean)

            End If
        Catch ex As Exception
            ERMetricsTxBx.Text = Nothing
            MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try

    End Sub

    'Stop the Oyster Run and Java Processs
    Private Sub StopRun_Click(sender As Object, e As EventArgs) Handles StopRun.Click

        Me.oysterHelper.Kill_Run()
        ToolStripStatusLabel1.Text = Constants.OHelper
        ToolStripProgressBar1.Visible = False
    End Sub

    '-----------------------------------------------------------------
    'End Run, Stop Oyster and ERMetrics
    '---------------------------------------------------------------

    '---------------------------------------------------------------
    ' Edit allowed and Saves for Files Loaded to RichTextBoxes using the generic
    ' `method SaveRichTextBoxFile.  New RichTextBoxes will cause the need to change
    ' the method to include them including constants values used. See method
    ' and constants file.
    '-----------------------------------------------------------------

    'set Edit for RichTextBox2 
    Private Sub EditOystRunBtn_Click(sender As Object, e As EventArgs) Handles EditOystRunBtn.Click
        RichTextBox2.ReadOnly = False
    End Sub

    'save richtextbox2 loaded file
    Private Sub SaveFile_RichTextBox2_Click(sender As Object, e As EventArgs) Handles SaveFile_RichTextBox2.Click

        If Not String.IsNullOrEmpty(RichTextBox2.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox2, RichTextBox2, saveInfoCollection.Item(Constants.RichTextBox2).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    ' Make loaded file in RichTextBox9 editable
    Private Sub EditFileBtn_Click(sender As Object, e As EventArgs) Handles EditFileBtn.Click
        RichTextBox9.ReadOnly = False
    End Sub

    ' Save the file loaded in RichTextBox9 through generic method
    ' SaveRichTextBoxFile.

    Private Sub SaveFileBtn_RichTextBox9Click(sender As Object, e As EventArgs) Handles SaveFileBtn.Click

        If Not String.IsNullOrEmpty(RichTextBox9.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox9, Me.RichTextBox9, Me.saveInfoCollection.Item(Constants.RichTextBox9).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SaveFileERMetricsBtnRichTextBox3_Click(sender As Object, e As EventArgs) Handles SaveFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox3, Me.RichTextBox3, Me.saveInfoCollection.Item(Constants.RichTextBox3).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub EditFileERMetricsBtnRichTextBox3_Click(sender As Object, e As EventArgs) Handles EditFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            RichTextBox3.ReadOnly = False
        Else
            MessageBox.Show(Constants.MakeFileReadOnlyError, "Read Only Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    '-----------------------------------------------------------------------------
    'End of Edits and Saves to RichTextboxes
    '-----------------------------------------------------------------------------


    '----------------------------------------------------------------------------
    'TreeView content can be double clicked to load the file to a RichTextBox
    'for editing through the generic metod LoadFileFromTreeview. New RichTextBoxes
    'will need to be added to constants and to the method.
    '----------------------------------------------------------------------------
    Sub KBMTreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles KBMTreeView.NodeMouseDoubleClick

        If KBMTreeView.SelectedNode IsNot Nothing Then

            Dim fileExists As Boolean = My.Computer.FileSystem.FileExists(KBMTreeView.SelectedNode.Tag)

            If fileExists Then

                Dim FileToLoad As String = KBMTreeView.SelectedNode.Tag.ToString

                Dim info As New FileInfo(FileToLoad)
                If info.Length = 0 Then
                    MessageBox.Show(Constants.FileIsEmpty, "File Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                Dim helperBean As New HelperBean
                helperBean.RichTextBoxName = "DataGridView1"
                helperBean.IKBFileToLoad = FileToLoad
                Dim i As Integer = Constants.DataGridView1
                'use the integer in constants for each textbox to store
                'in the hashtable
                If Not saveInfoCollection.Item(i) Is Nothing Then
                    saveInfoCollection.Remove(i)
                    saveInfoCollection.Add(i, helperBean)
                Else
                    saveInfoCollection.Add(i, helperBean)
                End If
                Cursor = Cursors.WaitCursor
                BackgroundWorker2.RunWorkerAsync(FileToLoad)
            Else
                MessageBox.Show("File does not Exist!", "File Exists", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If


        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Delegate Sub SafeCallDelegate(text As String)

    Public Shared FileToLoadToRTB As String = Nothing

    Sub KBSearchTreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles KBSearchTreeView.NodeMouseDoubleClick

        If KBSearchTreeView.SelectedNode IsNot Nothing Then
            Cursor = Cursors.WaitCursor
            FileToLoadToRTB = KBSearchTreeView.SelectedNode.Tag
            Dim Thread2 As Thread = New Thread(New ThreadStart(AddressOf SetText))
            Thread2.Start()
            Thread.Sleep(1000)

        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub WriteTextSafe(text As String)

        If RichTextBox11.InvokeRequired Then
            Dim d As New SafeCallDelegate(AddressOf SetText)
            Invoke(d, New Object() {text})
        Else

            RichTextBox11.LoadFile(text, RichTextBoxStreamType.PlainText)
            KBLoadFilelbl.Text = "Loaded file is: " & Path.GetFileName(FileToLoadToRTB)
            KBFileLineCountLbl.Text = "Line Count is: " & RichTextBox11.Lines.Count.ToString("N0")
            Dim i As Integer = 11
            Dim helpBean As New HelperBean
            helpBean.FileLoaded = FileToLoadToRTB
            If Not saveInfoCollection.Item(i) Is Nothing Then
                saveInfoCollection.Remove(i)
                saveInfoCollection.Add(i, helpBean)
            Else
                saveInfoCollection.Add(i, helpBean)
            End If
            Cursor = Cursors.Default
        End If
    End Sub

    Public Sub SetText()
        WriteTextSafe(FileToLoadToRTB)
    End Sub

    Sub TreeView3_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView3.NodeMouseDoubleClick

        If TreeView3.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView3, RichTextBox9)
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Sub TreeView1_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseDoubleClick

        If TreeView1.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView1, RichTextBox2)
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub


    Sub TreeView4_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView4.NodeMouseDoubleClick

        If TreeView4.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView4, RichTextBox3)


        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    '-----------------------------------------------------------------
    'End loadfilefromtreeview double Clicks -----------------------------
    '--------------------------------------------------------------------


    ' save ERMetrics textboxes
    Private Sub SaveERMetricsBtn_Click(sender As Object, e As EventArgs) Handles SaveERMetricsBtn.Click
        oysterHelper.SaveERmetricsTxBoxes()
    End Sub

    ' move the link file created from Oyster Run to ERMetrics folder
    Private Sub MoveFileBtn_Click(sender As Object, e As EventArgs) Handles MoveFileBtn.Click
        oysterHelper.MoveLinkFile()
    End Sub

    'help dialogs that need attention
    Private Sub OysterToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim oysterHelp As New OysterHelpForm
        oysterHelp.ShowDialog()
    End Sub
    'help dialogs that need attention
    Private Sub OysterHelperToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OysterHelperToolStripMenuItem.Click
        Dim oysterHelp As New OysterHelpForm
        oysterHelp.ShowDialog()
    End Sub
    ' Method called to resize the program.
    Private Sub Oysterform_Resize(sender As Object, e As EventArgs) Handles Me.Resize

        RS.ResizeAllControls(Me, Me.Width, Me.Height)
    End Sub

    ''' Close the application which will also dispose resources.
    Private Sub Exitbtn_Click(sender As Object, e As EventArgs) Handles Exitbtn.Click
        Try
            Me.Dispose()
        Catch ex As Exception
        End Try

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' Right click methods Below''''''''''''''''''''''''''''''''''

    Private Sub CommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.SelectedText = "<!-- " & richTextBox.SelectedText.Trim & " --> "
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub UnCommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnCommentSelectedTextToolStripMenuItem.Click

        Try
            Dim array1() As Char = {"<", "!", "-"}
            Dim array2() As Char = {"-", ">"}
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            Dim str As String = st.TrimEnd(array2)
            richTextBox.SelectedText = str.Trim & vbCr
            'uncomment to save 
            '  Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                If Not String.IsNullOrEmpty(treeView.SelectedNode.Tag) Then
                    Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                    'MsgBox(selectedFileAndPath)

                    My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                    selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                    oysterHelper.RunProcessTab(Constants.Welcome)

                Else
                    MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)

                End If


            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub DeleteSelectedFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectedFileToolStripMenuItem.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            'Dim tab = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                oysterHelper.RunProcessTab(Constants.Welcome)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ShowFilePropertiesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowFilePropertiesToolStripMenuItem.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub


    Private Sub CopySelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopySelectionToolStripMenuItem.Click

        'Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

        'If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
        Try
            Dim richTextBox As RichTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)

            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

        '   End If

    End Sub

    Private Sub CutSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CutSelectionToolStripMenuItem.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Cut()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub PasteSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PasteSelectionToolStripMenuItem.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub CommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem1.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.SelectedText = "#" & richTextBox.SelectedText
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub UncommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles UncommentSelectedTextToolStripMenuItem1.Click
        Try
            Dim array1() As Char = {"#"}

            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            richTextBox.SelectedText = st.Trim
            'next two uncomment to save the file
            ' Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItemCon42_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                'MsgBox(selectedFileAndPath)
                My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                oysterHelper.RunProcessTab(Constants.ERMetricsSetup)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItemCon43_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                oysterHelper.RunProcessTab(Constants.ERMetricsSetup)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    Private Sub ToolStripMenuItemcon44_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem4.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try
    End Sub
    Private Sub ToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem5.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                'MsgBox(selectedFileAndPath)
                My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                Dim name As String = treeView.Name
                If treeView.Name.Equals("KBSearchTreeView") Then
                    oysterHelper.RunProcessTab(Constants.KnowledgeBaseView)
                Else
                    oysterHelper.RunProcessTab(Constants.WorkWithOysterFiles)
                End If

            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub ToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem6.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                If treeView.Name.Equals("KBSearchTreeView") Then
                    oysterHelper.RunProcessTab(Constants.KnowledgeBaseView)
                Else
                    oysterHelper.RunProcessTab(Constants.WorkWithOysterFiles)
                End If
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub


    Private Sub ToolStripMenuItem7_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem7.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    'copy to clipboard 
    Private Sub CopyToClipboardToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyToClipboardToolStripMenuItem.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    'specifically copys cell from datagridview and pastes it to the search box
    Private Sub ToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem8.Click
        Try
            Dim DataGridView = DirectCast((sender).Owner.SourceControl, DataGridView)
            DataGridView.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText
            DataGridView.Select()
            If DataGridView.GetCellCount(DataGridViewElementStates.Selected) > 0 Then
                ' Add the selection to the clipboard.
                Clipboard.SetDataObject(
                    DataGridView.GetClipboardContent())
                KBMTextBox.Text = Clipboard.GetText
            Else
                MessageBox.Show("You must Select a Cell First", "Select a Cell", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            MessageBox.Show("Error is " & ex.Message, "Error Selecting and Pasting", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try


    End Sub


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'End Right click methods

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ''''''''''''''''''''''''''''''''''''''''''''''''''
    'Helpful Stuff Methods to create the CSV and Reference file
    '''''''''''''''''''''''''''''..........................

    'this does its part to create a refence file
    Private Sub CreateRefFilebtn_Click(sender As Object, e As EventArgs) Handles CreateRefFilebtn.Click

        If Not String.IsNullOrEmpty(HSCsvTxBx.Text) Then

            If Not File.Exists(HSCsvTxBx.Text) Then
                MessageBox.Show("CVS File does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                HSCsvTxBx.Text = Nothing
                Exit Sub
            ElseIf Not File.Exists(HSLinkFileTxBx.Text) Then
                MessageBox.Show("LinkFile does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                HSLinkFileTxBx.Text = Nothing
                Exit Sub
            ElseIf File.ReadAllText(HSCsvTxBx.Text).Length = 0 Then
                MessageBox.Show("CVS File is empty, Rerun Oyster", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            End If

            'don't let user click other button that calls backgroundworker until it is done with this run
            CreateCSVBtn.Enabled = False

            'false sent to getHelperBean then make reference file
            ' BackgroundWorker1.RunWorkerAsync(oysterHelper.GetHelperBean(False))
            oysterHelper.CreateHelpfulStufFiles(oysterHelper.GetHelperBean(False))

        Else
            MessageBox.Show(Constants.RunOysterFirst, "Empty Parameters", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If

    End Sub

    'Helper Stuff create create csv file from last oyster run
    Private Sub CreateCSVBtn_Click(sender As Object, e As EventArgs) Handles CreateCSVBtn.Click


        Dim helperBean As New HelperBean
        Dim fileCount As Integer = 0
        Dim subString As String = Nothing

        Try

            If Not String.IsNullOrEmpty(HSCsvTxBx.Text) And Not String.IsNullOrEmpty(HSLinkFileTxBx.Text) Then

                If Not File.Exists(HSCsvTxBx.Text) Then
                    MessageBox.Show("CVS File does not Exist, Check if" & vbCrLf & " it has been Deleted!", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSCsvTxBx.Text = Nothing
                    Exit Sub
                ElseIf Not File.Exists(HSLinkFileTxBx.Text) Then
                    MessageBox.Show("LinkFile does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSLinkFileTxBx.Text = Nothing
                    ERMetLinkFileTxbx.Text = Nothing
                    Exit Sub

                ElseIf File.ReadAllText(HSCsvTxBx.Text).Length = 0 Then
                    MessageBox.Show("CVS File is empty, Rerun Oyster", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub

                End If

                'don't let user click other button that calls backgroundworker until it is done with this run
                CreateRefFilebtn.Enabled = False

                'if sending true to gethelperbean then make csv file
                ' BackgroundWorker1.RunWorkerAsync(oysterHelper.GetHelperBean(True))
                oysterHelper.CreateHelpfulStufFiles(oysterHelper.GetHelperBean(True))
            Else
                MessageBox.Show(Constants.RunOysterFirst, "Empty Parameters", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

        Catch ex As Exception

        End Try

    End Sub



    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'end create CVS and Ref files
    ''''''''''''''''''''''''''''''''''''''''''''''''''''


    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Each click on a tab checks and loads each 
    'tab as needed
    '''''''''''''''''''''''''''''''''''''''''''''''''''' 
    Private Sub MyTabControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OysterTabs.SelectedIndexChanged

        oysterHelper.RunProcessTab(OysterTabs.SelectedTab.Name)


    End Sub


    'Timer used along with the stopwatch to keep time on Oyster runs
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timelbl.Text = String.Format("{0}:{1}:{2}", stopWatch.Elapsed.Hours.ToString("00"), stopWatch.Elapsed.Minutes.ToString("00"), stopWatch.Elapsed.Seconds.ToString("00"))
    End Sub

    Private Sub KBSearchBtn_Click(sender As Object, e As EventArgs) Handles KBSearchBtn.Click

        Dim searchTerm As String = SearchKBTxtBx.Text

        If String.IsNullOrEmpty(searchTerm) Or String.IsNullOrWhiteSpace(searchTerm) Then
            MessageBox.Show("Search Term is Empty!", "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf String.IsNullOrEmpty(KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)) Or String.IsNullOrWhiteSpace(KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)) Then
            MessageBox.Show("You must First Load a File to Search", "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            oysterHelper.SearchKBLoadedFile(searchTerm, helperBean)
        End If


    End Sub



    Private Sub ColumnCheckedLB_MouseClick(sender As Object, e As MouseEventArgs) Handles ColumnCheckedLB.MouseClick
        Dim idx, sidx As Integer
        sidx = ColumnCheckedLB.SelectedIndex
        For idx = 0 To ColumnCheckedLB.Items.Count - 1
            If idx <> sidx Then
                ColumnCheckedLB.SetItemChecked(idx, False)
            Else
                ColumnCheckedLB.SetItemChecked(sidx, True)
            End If
        Next
    End Sub


    Private Sub SearchIdtyBtn_Click(sender As Object, e As EventArgs) Handles SearchIdtyBtn.Click

        If String.IsNullOrEmpty(KBMTextBox.Text) Then
            MessageBox.Show("Search Term Required!", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            oysterHelper.SearchDataGrid()
        End If



    End Sub

    Private Sub ResetFilterBtn_Click(sender As Object, e As EventArgs) Handles ResetFilterBtn.Click

        Dim bsource As BindingSource = DataGridView1.DataSource
        If bsource Is Nothing Then
            MessageBox.Show("Nothing to Reset!", "Null Reference", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Exit Sub
        End If

        bsource.RemoveFilter()
        KBMFilterCountLbl.Text = "Filtered Count is: "
        KBMTextBox.Text = Nothing

        For Each i As Integer In ColumnCheckedLB.CheckedIndices
            ColumnCheckedLB.SetItemChecked(i, False)
        Next



    End Sub

    'used to speed the load of the datagridview in the IKB Searching Tab
    Private Sub BackgroundWorker2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork

        Try

            Dim FiletoLoad As String = e.Argument

            Dim table As New DataTable()
            Dim bsource As New BindingSource
            Dim DictList As New Dictionary(Of String, String)

            Dim xmlAll As New XDocument


            xmlAll = XDocument.Load(FiletoLoad)
            Dim Attributes As IEnumerable(Of XElement) = xmlAll.Root.Elements("Metadata").Elements("Attributes").Elements("Attribute")
            Dim References As IEnumerable(Of XElement) = xmlAll.Root.Elements("Identities").Elements("Identity")
            Dim oysteridValue As String = Nothing
            xmlAll = Nothing
            Dim column As DataColumn = Nothing
            Dim order As Integer = 0
            For Each xel In Attributes
                column = New DataColumn
                column.DataType = System.Type.GetType("System.String")
                column.ColumnName = xel.Attribute("Name").Value
                column.ReadOnly = True
                column.Unique = False
                table.Columns.Add(column)
                table.Columns(xel.Attribute("Name").Value).SetOrdinal(order)
                DictList.Add(xel.Attribute("Name").Value, xel.Attribute("Tag").Value)
                order = order + 1
            Next
            column = New DataColumn
            column.DataType = System.Type.GetType("System.String")
            column.ColumnName = "OysterId"
            column.ReadOnly = True
            column.Unique = False

            table.Columns.Add(column)
            table.Columns("OysterId").SetOrdinal(order)
            '    Console.WriteLine(order)
            order = Nothing
            ' DictList.Add("OysterId", oysteridValue)
            column = Nothing
            For Each iden In References

                For Each el In iden.Elements("References").Elements("Reference")
                    Dim outputString As String = Nothing
                    Dim str As String = el.Element("Value").Value

                    For Each key As String In DictList.Keys
                        Dim keyPlus As String = DictList.Item(key) & "^"
                        Dim loc As Integer = str.IndexOf(keyPlus)
                        If Not loc = -1 Then
                            Dim i As Integer = str.IndexOf("|")
                            If Not i = -1 Then
                                outputString = outputString + str.Substring(loc + 2, i - 2) & vbTab
                                str = str.Substring(i + 1)
                            Else
                                outputString = outputString + str.Substring(loc + 2) & vbTab
                            End If
                        Else
                            outputString = outputString & vbTab
                        End If

                    Next
                    outputString = outputString & iden.Attribute("Identifier").Value.ToString
                    Dim row1() As String = Split(outputString, vbTab)
                    table.Rows.Add(row1)
                Next
            Next
            ' BackgroundWorker2.ReportProgress(20)
            Attributes = Nothing
            References = Nothing
            Dim Helpbean As New HelperBean
            Helpbean.DataGridViewTable = table
            Helpbean.IKBMDictionary = DictList
            Helpbean.FileLoaded = FiletoLoad
            e.Result = Helpbean

        Catch ex As Exception
            Console.WriteLine("Error in Backgroundworker2.dowork is " & ex.Message)
        Finally

            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub



    Private Sub BackgroundWorker2_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted

        Dim hbean As HelperBean = Nothing
        Dim table As DataTable = Nothing
        Dim bsource As New BindingSource
        Dim DictList As New Dictionary(Of String, String)
        ToolStripProgressBar1.Value = 0
        ToolStripProgressBar1.PerformStep()


        Try

            hbean = CType(e.Result, HelperBean)
            table = CType(hbean.DataGridViewTable, DataTable)

            DictList = CType(hbean.IKBMDictionary, Dictionary(Of String, String))
            ColumnCheckedLB.Items.Clear()

            For Each key As String In DictList.Keys
                If Not key.ToUpper.Contains("OysterId".ToUpper) Then
                    ColumnCheckedLB.Items.Add(key)
                End If
            Next
            ColumnCheckedLB.Items.Add("OysterId")
            ToolStripProgressBar1.PerformStep()

            bsource.DataSource = table
            DataGridView1.DataSource = bsource

            Dim aInteger As Integer = 0
            With DataGridView1
                For Each key As String In DictList.Keys
                    .Columns(key).DisplayIndex = aInteger
                    aInteger = aInteger + 1
                Next
                .Columns("OysterId").DisplayIndex = aInteger
            End With

            DictList.Clear()
            Me.DataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells)
            LoadCountLbl.Text = "Reference Line Count is: " & table.Rows.Count & " Rows"
            KBMFileLoadedLbl.Text = "File Loaded is: " & hbean.FileLoaded.Substring(hbean.FileLoaded.LastIndexOf("\") + 1)
            KBMFilterCountLbl.Text = "Filtered Count is:"
        Catch ex As Exception
            MessageBox.Show("Error in Backgroundworker2 Completed is " & ex.Message, "Worker Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            ToolStripStatusLabel1.Text = "Oyster Helper"
            ToolStripProgressBar1.Value = 0
            ToolStripProgressBar1.Visible = False
            hbean = Nothing
            bsource = Nothing
            table = Nothing
            DictList = Nothing
            Cursor = Cursors.Default
            GC.Collect()
            GC.WaitForPendingFinalizers()

        End Try
    End Sub

    Private Sub SelectEntireRowToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SelectEntireRowToolStripMenuItem.Click
        oysterHelper.ToolSelectEntireRow()
    End Sub


    Private Sub StartTest_Click(sender As Object, e As EventArgs) Handles StartTests.Click
        oysterHelper.RunTests()

    End Sub



    'Used to hide and show the testing tab
    Public Sub EnablePage(page As TabPage, enable As Boolean)

        If (enable) Then
            If page IsNot Nothing Then
                OysterTabs.TabPages.Add(page)
                ListA.Remove(page)
            End If

        Else
            If page IsNot Nothing Then
                OysterTabs.TabPages.Remove(page)
                ListA.Add(page)

                'in case the program is maximized resize the tab when it is visible
                RS.ResizeAllControls(Me, Me.Width, Me.Height)
                '---------------------------------------------------s
            End If

        End If
    End Sub


    '---------------Assertion Tab Work------------------------------------


    '  Private Sub CreateAssertFileBtn_Click(sender As Object, e As EventArgs) Handles CreateAssertFileBtn.Click

    Private Sub CreateAssertFile()


        Dim rButton As RadioButton = GroupBox1.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked = True)

        If rButton Is Nothing Then
            MessageBox.Show("You must choose an Asserton Type!")
            Exit Sub
        End If
        dta = New DataTable


        oysterHelper.CreateTemplate(rButton, dta)


    End Sub

    Private Sub StrToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrToStrAssertion.CheckedChanged

        If StrToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFile()

            ' MessageBox.Show("Use the Create Assertion Button to Create a Template!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub RefToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToStrAssertion.CheckedChanged
        If RefToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            MessageBox.Show("If you have a Reference File" & vbCrLf & " Load it to the DataGridView for use!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            oysterHelper.LoadAssertionTemplate()
            AssertLabel.Text = "Template Loaded is for: " & oysterHelper.GetAssertType.Substring(1)
            'TrimTemplate(dta)
        End If


    End Sub

    Private Sub StrSplitAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrSplitAssertion.CheckedChanged
        If StrSplitAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFile()
            'CreateAssertFileBtn.Enabled = True
            ' MessageBox.Show("Use the Create Assertion Button to Create a Template!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If

    End Sub

    Private Sub RefToRefAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToRefAssertion.CheckedChanged

        If RefToRefAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb

            MessageBox.Show("If you have a Reference File" & vbCrLf & " Load it to the DataGridView for use!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)

            oysterHelper.LoadAssertionTemplate()
            AssertLabel.Text = "Template Loaded is for: " & oysterHelper.GetAssertType.Substring(1)
            '  TrimTemplate(dta)

        End If



    End Sub

    Private Sub SaveAssertFileBtn_Click(sender As Object, e As EventArgs) Handles SaveAssertFileBtn.Click

        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.Filter = "CSV File|*.csv|TextFile|*.txt"
        saveFileDialog1.Title = "Save an Assertion File"
        saveFileDialog1.ShowDialog()


        ' If the file name is not an empty string open it for saving.  
        If saveFileDialog1.FileName <> "" Then
            SaveGridDataInFile(saveFileDialog1.FileName)
        End If



    End Sub

    Private Sub SaveGridDataInFile(ByRef fName As String)
        oysterHelper.saveDataGridAssertionFile(fName)
    End Sub



    Public Sub TrimTemplate(ByRef dta As DataTable)
        oysterHelper.TemplateTrim(dta)



    End Sub

    '--------------------------------------------------------------------------
    '------End Assertion Tab Work
    '-----------------------------------------------------------------------------



    '--------------------------------------------------------------

    'Very Important ConsoleOutput Methods used to write back
    'to The RichtextBoxes during Oyster and ERmetrics Runs.
    'Console ouput writers for the ProcessClass
    '-------------------------------------------------------------
    Private Sub ConsoleOutput(ByVal outputString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleOutputDelegate(AddressOf ConsoleOutput)
            Dim args As Object() = {outputString}
            Me.Invoke(del, args)

        Else
            If helperBean.RichTexBoxNameBool Then
                RunRichTextBox.ScrollToCaret()
                RunRichTextBox.AppendText(String.Concat(outputString, Environment.NewLine))
                ToolStripProgressBar1.PerformStep()
            Else
                ERMetricsRunRichTxtBx.ScrollToCaret()
                ERMetricsRunRichTxtBx.AppendText(String.Concat(outputString, Environment.NewLine))
                ToolStripProgressBar1.PerformStep()
            End If

            If ToolStripProgressBar1.Value = 100 Then
                ToolStripProgressBar1.Value = 0
                ToolStripProgressBar1.PerformStep()
            End If

            If outputString.Contains("Total elapsed time ") Or outputString.Contains("error") Then

                StopRun.Enabled = False
                Me.ToolStripProgressBar1.Visible = False
                ' Me.ToolStripStatusLabel1.Visible = False
                ToolStripStatusLabel1.Text = Constants.OHelper

                Timer1.Enabled = False
                'stop the stopwatch and report elapsed time
                stopWatch.Stop()
                Dim ts As TimeSpan = stopWatch.Elapsed
                ' RunTimeValuelbl.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10)
                ' RunTimeValuelbl.Text = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds)
                ' oysterHelper.Kill_Oyster()
                RunERMetricsBtn.Enabled = True

                If TestHelper.Testing Then
                    TestHelper.SecondTest()
                End If


            ElseIf outputString.Equals("Complete") Then
                ToolStripProgressBar1.Visible = False
                ToolStripStatusLabel1.Text = Constants.OHelper
                StopRun.Enabled = False

                If TestHelper.Testing Then
                    TestHelper.FinishReport()
                End If

            End If
        End If
    End Sub

    Sub ConsoleOutputHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleOutput(outLine.Data)
        End If
    End Sub

    Private err As Integer = 0

    Private Sub ConsoleError(ByVal errorString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleErrorDelegate(AddressOf ConsoleError)
            Dim args As Object() = {errorString}
            Me.Invoke(del, args)
        Else
            If helperBean.RichTexBoxNameBool Then
                RunRichTextBox.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
                StopRun.Enabled = False
                ErrorInRunLbl.Visible = False
                ToolStripStatusLabel1.Text = Constants.OHelper
                ToolStripProgressBar1.Visible = False
                If stopWatch.IsRunning Then
                    stopWatch.Stop()
                End If
                RunERMetricsBtn.Enabled = True

                err = err + 1


                'If err = 2 Then
                '    oysterHelper.Kill_Run()
                '    MessageBox.Show("Error in the Oyster Runner is " & errorString, "Check Your Requirements", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    Exit Sub
                'End If
            Else
                ERMetricsRunRichTxtBx.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
                StopRun.Enabled = False
                ErrorInRunLbl.Visible = False
                RunERMetricsBtn.Enabled = True
                MessageBox.Show("Error in the Oyster Run", "Check Requirements", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sendingProcess"></param>
    ''' <param name="errLine"></param>
    Sub ConsoleErrorHandler(ByVal sendingProcess As Object, ByVal errLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(errLine.Data) Then
            ConsoleError(errLine.Data)
        End If
    End Sub
    'End ConsoleOutput Methods ----------------------------------------------
    '-------------------------------------------------------------------------



    'Start of Test Methods ----------------------------------------------
    '-------------------------------------------------------------------------
    Private Sub ReadTestFileBtn_Click(sender As Object, e As EventArgs) Handles ReadTestFileBtn.Click

        Try
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If

            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                MessageBox.Show("Oyster Helper cannot get your User Name. " & vbCrLf & "Please close Oyster Helper and then Run as Administrator!", "Empty User Name!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If


            If IO.File.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") Then
                oysterHelper.ReadTestFile()
            Else
                MessageBox.Show("Test File not Found!", "Test File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            TestHelper.Testing = False
            TestHelper.isFinished = False
            MessageBox.Show("Error reading test file is " & ex.Message, "Test File Read Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub

    'Create the test file if the textboxes are all filled
    Private Sub CreateFileBtn_Click(sender As Object, e As EventArgs) Handles CreateFileBtn.Click

        oysterHelper.CreateTestFile()

    End Sub

    'signin to the reveal the testing tab that is hidden
    Private Sub ToolStripDropDownButton1_DoubleClick(sender As Object, e As EventArgs) Handles ToolStripDropDownButton1.DoubleClick
        oysterHelper.LoginForTesting()

    End Sub

    Private Sub DeleteValuesBtn_Click(sender As Object, e As EventArgs) Handles DeleteValuesBtn.Click

        oysterHelper.DeleteOysterDirectory()
        My.Settings.Testing = True
        My.Settings.Save()

    End Sub

    Private Sub RestartBtn_Click(sender As Object, e As EventArgs) Handles RestartBtn.Click
        Application.Restart()
    End Sub

    Private Sub SearchKBTxtBx_TextChanged(sender As Object, e As EventArgs) Handles SearchKBTxtBx.TextChanged

        If RichTextBox11.Lines.Count > 0 Then
            RichTextBox11.SelectionStart = 0
            RichTextBox11.ScrollToCaret()
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        OH_AboutBox.ShowDialog()
    End Sub


    'Hide the Testing tab if desired fro m the Testing Tab
    Private Sub HideTab_Click_1(sender As Object, e As EventArgs) Handles HideTab.Click

        Dim tab As TabPage = OysterTabs.TabPages.Item("TestTab")
        EnablePage(tab, False)
        My.Settings.Testing = False
        My.Settings.Save()

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        System.Diagnostics.Process.Start(e.Link.LinkData.ToString())
    End Sub

End Class
