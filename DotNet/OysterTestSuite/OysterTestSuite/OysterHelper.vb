﻿Imports System.IO
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms.DataVisualization.Charting

''' 
''' Accept design requires work to move from the OysterForm to another class.  This is that class.
''' 
''' Important Nethods in OysterHelper
''' 
''' TextChanged - checks choices for textboxes for correctness, user typing, space bar, etc. Only Browse button can be used to cut down on user input errors.
''' 
''' TextChangedSpace - checks to see if user used spaces in textbox change and not the browse button.
''' 
''' LoadFileFromTreeView - This is used to keep up what file is loaded in which Richtextbox so we correctly save the right file everytime.
'''                        Any new Treeviews and RichTextbox pairs added especially on new Tabs would need constants and be added here. It then loads the file
''' 
''' GetLinkFileName - reads the Oyster Run Script used in Oyster Run for the link file name and returns it for use and loading textboxes,etc.
''' 
''' GetInputCSVFileName - Reads Oyster SourceScript XML file to return the input source file name for use and loading textboxes,etc
''' 
''' MoveLinkFile - moves the latest Linkfile to the ERMetrics Directory for use on button click.
''' 
''' CheckPath - Method used in any change to TextBoxes 
''' 
''' checkTextboxes - checks textboxes for being empty
''' 
''' RunOyster - Runs Oyster by checking all basic requirements and starts a ProcessClass to Run Oyster as a Java process in a Command Window.
''' The run is not seen but the output is sent to the Run Output Tab.
''' 
''' RunERMetrics - Runs ERMetrics in the same ProcessClass but with different parameters.
'''  
''' GetHelperBean-method to get the information as to which file is being created in Helpful Stuff
''' 
''' RunProcessTab - does some checks before calling processing tabs
''' 
''' InfoCall - File Info and Directory Info used in filling textboxes can throw errors, this handles them safely and sends a Boolean back
''' Errors where occuring if a user tried to load from a restricted directory or a restricted file in a directory
''' 
''' KillRun - Stops the Oyster or ERMetrics run by killing all java process on the machine making the call
''' 
''' SaveERMetrics - Saves the ERMetric Tab information entered for textboxes and refreshs the Tab to load other
''' 
''' FillTextBox - Generic Method to FillTextBoxes, uses ByRef parameters to blindly do work
'''
''' SaveRichTextBoxFile - Saves the File loaded and Edited in a RichTextBox, Generic
'''
''' CreateHelpfulStufFiles - Creates the output files for the Helpful Stuff Tab
'''
''' FindMyText - searches the loaded XML file in the IKB XML View Tab
''' 
''' Save_All_WelcomeTab - saves the information on the First Tab - Oyster Setup and Run
'''
''' CheckOysterDirectories - helps check textbox entries
'''
''' Kill_Run - stops the java processes for oyster and ermetrics
''' 
''' SaveERmetricsTxBoxes - save the ERMetrics textboxes
'''
''' SearchDataGrid - searches the datagridview when loaded in the IKB Searching Tab
'''
''' ToolSelectEntireRow - selects a row in the datagridview in the IKB Searching Tab and loads 
''' the search box on the IKB XML View tab with the OysterID and loads the XML file if needed
'''
''' ReadTestFile - reads the test file for use in testing
'''
''' ReadTestFile - creates the test file from the 5 main textboxes in the first 2 tabs for testing purposes
'''
''' LoginForTesting - the login for testing method
'''
''' DeleteOysterDirectory - deletes the saved user information so testing can occur.  MUST delete first to run Testing.
'''
''' RunTests - runs the testing of Oyster Helper
'''
''' CreateTemplate - creates two of the four assertion templates in the Assertion Helper Tab
'''
''' LoadAssertionTemplate - creates two different templates from input files in the Assertion Helper Tab
'''
''' SaveDataGridAssertionFile - saves any template in the datagridview of the Assertion Helper Tab
'''
''' LoadColumns - helps Create/Load the columns into the datagridview the assertion files of the Assertion Helper Tab
'''
''' GetAssertType - returns the assertion type chosen in the Assertion  Helper Tab
'''
''' TemplateTrim - Removes rows from input files loaded to the datagridview in the Assertion Helper Tab
'''
'''
'''


Public Class OysterHelper




    'this is used to keep up what file is loaded in which Richtextbox so we correctly save the right file everytime
    'Any new Treeviews and RichTextbox pairs added especially on new Tabs would need constants be added here
    Public Shared Sub LoadFileFromTreeView(ByRef TreeView As TreeView, ByRef RichTextBox As RichTextBox)
        Try


            Dim fileExists As Boolean = My.Computer.FileSystem.FileExists(TreeView.SelectedNode.Tag)

            If fileExists Then
                '    Console.WriteLine("in oysterhelper method " & Now.ToLongTimeString)
                Dim fileAndPath As String = TreeView.SelectedNode.Tag.ToString
                Dim i As Integer = 0
                Dim RichTextBoxName As String = Nothing
                Dim fileLoaded As String = Path.GetFileName(fileAndPath)
                Select Case RichTextBox.Name

                    Case Constants.RichTextBox21String
                        i = Constants.RichTextBox21
                        RichTextBoxName = Constants.RichTextBox21String
                        Oysterform.PythonFileLbl.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox9String
                        i = Constants.RichTextBox9
                        RichTextBoxName = Constants.RichTextBox9String
                        Oysterform.WelLoadedFileLbl.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox2String
                        i = Constants.RichTextBox2
                        RichTextBoxName = Constants.RichTextBox2String
                        Oysterform.WorkWithOysterFile.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox3String
                        i = Constants.RichTextBox3
                        RichTextBoxName = Constants.RichTextBox3String
                        Oysterform.ERMetricsLoadedFile.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox11String
                        i = Constants.RichTextBox11
                        RichTextBoxName = Constants.RichTextBox11String
                        Oysterform.KBLoadFilelbl.Text = Constants.FileLoadedIs & fileLoaded

                    Case Else
                        MessageBox.Show(Constants.AddRichTextBoxName, "RichTextBox Name Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                End Select


                RichTextBox.LoadFile(fileAndPath, RichTextBoxStreamType.PlainText)

                'save the file name that is loaded to make sure we save the correct file
                Dim helperBean As New HelperBean
                helperBean.RichTextBoxName = RichTextBoxName
                helperBean.FileLoaded = fileAndPath
                'If RichTextBoxName.Equals("RichTextBox21") Then
                '    Oysterform.PythonFileLbl.Text = "Loaded File is: " & Path.GetFileName(fileAndPath)
                'End If

                'use the integer in constants for each textbox to store
                'in the hashtable
                If Not Oysterform.saveInfoCollection.Item(i) Is Nothing Then
                    Oysterform.saveInfoCollection.Remove(i)
                    Oysterform.saveInfoCollection.Add(i, helperBean)
                Else
                    Oysterform.saveInfoCollection.Add(i, helperBean)
                End If


                If RichTextBoxName.Equals(Constants.RichTextBox11String) Then
                    Oysterform.KBFileLineCountLbl.Text = "Line Count Is " & RichTextBox.Lines.Count.ToString()
                End If

                ''        Console.WriteLine(Now.ToLongTimeString)
                If RichTextBox.Text = Nothing Then
                    RichTextBox.Text = Constants.FileIsEmpty
                End If

            Else

                ' Console.WriteLine("File To Load Doesnot Exist!")
            End If

        Catch ex As Exception
            MessageBox.Show("Loading RichTextBox Error Is " & ex.Message, "Error Loading RichTextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally

            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try

    End Sub

    Public Sub CreateHelpfulStufFiles(ByVal hBean As HelperBean)


        ' this creates the new csv file from a linkfile from last oysterrun
        ' The file created has all entries that did not fire a rule
        ' It will help with improving rules
        Dim fWriter As StreamWriter = Nothing
        Dim fileWriter As StreamWriter = Nothing
        Dim fileWriter1 As StreamWriter = Nothing
        '    Dim OutputDelimiter = inputDelimiter

        Dim AList As New ArrayList
        Dim BList As New ArrayList
        Dim FList As New ArrayList
        Dim CList As New Dictionary(Of String, String)
        Dim DList As New Dictionary(Of String, String)
        Dim EList As New Dictionary(Of String, String)
        Dim JList As New Dictionary(Of String, String)

        Try



            'used to make sure the header is added
            Dim firstLine As Integer = 1
            Dim Term As String = "[@]"
            'used to keep up with
            Dim keeper As Integer = 1
            Dim inputCSVFile As String = hBean.InputCSVFileAndPath
            Dim delimiter As String = Nothing

            'input linkfile and path sent over through e.Argument as HelperBean
            'this is the linkfile we working with
            For Each Line As String In File.ReadLines(hBean.LinkFileFileAndPath)
                'make sure we have no empty lines in the file process because it can cause issues
                If Not String.IsNullOrEmpty(Line) And Not String.IsNullOrWhiteSpace(Line) Then

                    'lets make the first dictionary that will be used to write out the Reference file
                    'we know this is a tab delimited file so no need to get delimiter 
                    If keeper > 1 Then

                        'we want every line in the linkfile in dlist to create new referece file except the old input header so
                        'skip the first line with a integer counter keeper. creating dictionary to make file creation faster.
                        'we parse out the id and the linkfile into the dictionary to get ready to write the new reference file out.

                        'NOTE: This parsing is for the at this time established output of Oyster and the LinkFile
                        DList.Add(Line.Substring(0, Line.IndexOf(vbTab)).Substring(Line.IndexOf(".") + 1), Line.Substring(Line.IndexOf(vbTab) + 1, 16))
                        'we also want this information, recid and oysterId for CBSBootstarpfile
                        EList.Add(Line.Substring(0, Line.IndexOf(vbTab)).Substring(Line.IndexOf(".") + 1), Line.Substring(Line.IndexOf(vbTab) + 1, 16))
                        'this arraylist used for the CBSBootstrapFile
                        FList.Add(Line.Substring(0, Line.IndexOf(vbTab)).Substring(Line.IndexOf(".") + 1))
                    Else
                        keeper = 2
                    End If



                    '  If Line Then contains "[*]" only add it because we want non matching and False negatives In a New input file
                    If Line.Contains(Term) Then

                        'add the recid to used later to pull the records from the input file
                        AList.Add(Line.Substring(0, Line.IndexOf(vbTab)).Substring(Line.IndexOf(".") + 1))

                    Else

                        'add the guid OysterId for lines that show they are linked by rules 
                        'If Not Line.Contains(Term) Then
                        BList.Add(Line.Substring(0, Line.IndexOf(vbTab)).Substring(Line.IndexOf(".") + 1))


                    End If

                End If
            Next

            'this can be either a CVS file or a Ref File or CBS File-only one at a time
            'number 2 means it is the Reference file which is always comma delimited
            If hBean.CSVorReforCBSFileCreation = 2 Then

                fWriter = New StreamWriter(hBean.OutPutRefFilePathOutPut, False)
                '      Console.WriteLine("DList sizeids : : " & DList.Count.ToString)

                fWriter.WriteLine("recid,idtruth")
                Dim Assorted = From pair In DList
                               Order By pair.Value
                Dim AssortedDictionary = Assorted.ToDictionary(Function(p) p.Key, Function(p) p.Value)

                For Each kv As KeyValuePair(Of String, String) In AssortedDictionary
                    fWriter.WriteLine(kv.Key & "," & kv.Value)
                Next

                Assorted = Nothing
                AssortedDictionary = Nothing
                fWriter.Close()
                'creating new input file when number 1
            ElseIf hBean.CSVorReforCBSFileCreation = 1 Then
                'its a new input CSV file done this way with dictionary to make it faster
                fileWriter = New StreamWriter(hBean.OutPutCSVFileAndPath, False)
                For Each theLine As String In File.ReadLines(inputCSVFile)

                    'check for whitespace and extra lines with nothing in them, they cause errors
                    If Not String.IsNullOrEmpty(theLine) And Not String.IsNullOrWhiteSpace(theLine) Then

                        'want to write out the header so we add it first with "ABC" key
                        If firstLine = 1 Then
                            'set firstline to 2 for use when writing out the file
                            firstLine = 2
                            'we need to know what the delimeter is
                            'so we can parse theLine and get the 
                            'recid to use as key in dictionary


                            If theLine.Contains(vbTab) Then
                                delimiter = vbTab
                            ElseIf theLine.Contains("|") Then
                                delimiter = "|"
                            ElseIf theLine.Contains(";") Then
                                delimiter = ";"
                            ElseIf theLine.Contains(",") Then
                                delimiter = ","
                            End If

                            CList.Add("ABC", theLine)
                            CList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine)
                        Else
                            'add recid as Key and entire line as Value in the dictionary to write out the file
                            'quickly later.
                            CList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine)
                            '    EList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine.Substring(theLine.IndexOf(delimiter) + 1))


                        End If

                    End If

                Next


                'done this way to make it faster writing large 
                '   If hBean.CSVorReforCBSFileCreation = 1 Then
                For Each aLine As String In AList
                    If firstLine = 2 Then
                        firstLine = 3
                        'the header
                        fileWriter.WriteLine(CList.Item("ABC"))
                        fileWriter.WriteLine(CList.Item(aLine))
                    Else
                        fileWriter.WriteLine(CList.Item(aLine))
                    End If

                Next
                fileWriter.Close()
                '     End If


            ElseIf hBean.CSVorReforCBSFileCreation = 3 Then
                firstLine = 1
                For Each theLine As String In File.ReadLines(inputCSVFile)

                    'check for whitespace and extra lines with nothing in them, they cause errors
                    If Not String.IsNullOrEmpty(theLine) And Not String.IsNullOrWhiteSpace(theLine) Then
                        '  theLine = theLine.Replace(",", vbTab)
                        'want to write out the header so we add it first with "ABC" key
                        If firstLine = 1 Then
                            'set firstline to 2 for use when writing out the file
                            firstLine = 2
                            'we need to know what the delimeter is
                            'so we can parse theLine and get the 
                            'recid to use as key in dictionary
                            '   CList.Add("ABC", theLine)

                            If theLine.Contains(vbTab) Then
                                delimiter = vbTab
                            ElseIf theLine.Contains("|") Then
                                delimiter = "|"
                            ElseIf theLine.Contains(";") Then
                                delimiter = ";"
                            ElseIf theLine.Contains(",") Then
                                delimiter = ","
                            End If
                            '           Console.WriteLine(theLine.Substring(theLine.IndexOf(delimiter) + 1))
                            '  JList.Add("ABC", theLine.Substring(theLine.IndexOf(delimiter) + 1))
                            'Dim aline As String = Nothing
                            'If Not delimiter.Equals(OutputDelimiter) Then
                            '    theLine = theLine.Replace(delimiter, OutputDelimiter)
                            'End If
                            '     theLine = theLine.Replace(delimiter, vbTab)
                            ' Console.WriteLine(theLine)
                            JList.Add("ABC", theLine)


                        Else
                            '            Console.WriteLine(theLine)
                            '           Console.WriteLine(delimiter)
                            'add recid as Key and entire line as Value in the dictionary to write out the file
                            'quickly later.
                            ' CList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine)
                            '            Console.WriteLine(theLine.Substring(0, theLine.IndexOf(delimiter)))
                            '            Console.WriteLine(theLine.Substring(theLine.IndexOf(delimiter) + 1))

                            '  JList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine.Substring(theLine.IndexOf(delimiter) + 1))
                            'If Not delimiter.Equals(OutputDelimiter) Then
                            '    theLine = theLine.Replace(delimiter, OutputDelimiter)
                        End If
                        '  theLine = theLine.Replace(delimiter, vbTab)
                        JList.Add(theLine.Substring(0, theLine.IndexOf(delimiter)), theLine)


                    End If



                Next

                fileWriter1 = New StreamWriter(hBean.CBStrapFilePathOutPut, False)
                Dim firstline1 As Integer = 1
                ' Dim Assorted = From pair In EList Order By pair.Value
                'Dim AssortedDictionary = Assorted.ToDictionary(Function(p) p.Key, Function(p) p.Value)
                For Each aLine As String In FList
                    '   aLine = aLine.Replace(delimiter, OutputDelimiter)

                    If firstline1 = 1 Then
                        firstline1 = 2
                        'the header
                        fileWriter1.WriteLine(JList.Item("ABC") & delimiter & "OID")

                        fileWriter1.WriteLine(JList.Item(aLine) & delimiter & EList.Item(aLine))
                    Else
                        fileWriter1.WriteLine(JList.Item(aLine) & delimiter & EList.Item(aLine))
                    End If

                Next

            End If




            MessageBox.Show(Constants.FileCreationComplete, "File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            AList.Clear()
            BList.Clear()
            CList.Clear()
            DList.Clear()
            JList.Clear()
            EList.Clear()
            FList.Clear()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            MessageBox.Show("Error is " & ex.Message, "Background Worker Error", MessageBoxButtons.OK, MessageBoxIcon.Information)


        Finally
            Oysterform.CreateRefFilebtn.Enabled = True
            Oysterform.CreateCSVBtn.Enabled = True
            Oysterform.CBStrapBtn.Enabled = True

            If fileWriter1 IsNot Nothing Then
                fileWriter1.Close()

            End If
            fileWriter = Nothing
            fWriter = Nothing
            fileWriter1 = Nothing
            AList = Nothing
            BList = Nothing
            CList = Nothing
            DList = Nothing
            EList = Nothing
            FList = Nothing
            JList = Nothing

            GC.Collect()
            GC.WaitForPendingFinalizers()

        End Try

    End Sub

    Public Sub SearchKBLoadedFile(ByVal theSearchTerm As String, ByRef hBean As HelperBean)

        Dim helperBean As HelperBean = hBean

        Dim searchTerm As String = theSearchTerm
        Dim iFoundPos As Integer 'Position of first character of match
        Dim newifoundPos As Integer = 0
        Dim CharcsInRichTextBox As Integer = Oysterform.RichTextBox11.Text.Length
        Dim KBSearchLoadedFile As String = Nothing
        Dim WholeWord As Boolean = False

        Try
            If Not Oysterform.KBLoadFilelbl.Text.Substring(Oysterform.KBLoadFilelbl.Text.IndexOf(":") + 2).Equals(helperBean.KBSearchLoadedFileName) Then
                newifoundPos = 0

                helperBean.KBSearchLoadedFileName = Oysterform.KBLoadFilelbl.Text.Substring(Oysterform.KBLoadFilelbl.Text.IndexOf(":") + 2)
            Else
                newifoundPos = helperBean.SearchPositionNow

                'KBSearchLoadedFile = KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)
            End If

            WholeWord = Oysterform.KBCheckBox.Checked

            '  Console.WriteLine(Now.ToLongTimeString)

            ' iFoundPos = RichTextBox11.Find(searchTerm, 0, RichTextBoxFinds.None)
            If Not newifoundPos = 0 Then
                newifoundPos = newifoundPos + 1
            End If

            iFoundPos = FindMyText(searchTerm, newifoundPos, CharcsInRichTextBox, WholeWord)

            If Not iFoundPos = -1 Then

                helperBean.SearchPositionNow = iFoundPos + 1
                ' helperBean.KBSearchLoadedFileName = KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)

                Dim LineNumber As Integer = Oysterform.RichTextBox11.GetLineFromCharIndex(iFoundPos)
                Oysterform.RichTextBox11.SelectionStart = Oysterform.RichTextBox11.GetFirstCharIndexFromLine(LineNumber - 3)
                Oysterform.RichTextBox11.ScrollToCaret()

                Oysterform.RichTextBox11.SelectionStart = iFoundPos
                Oysterform.RichTextBox11.SelectionLength = Oysterform.SearchKBTxtBx.Text.Length
                'RichTextBox11.ScrollToCaret()
                Oysterform.RichTextBox11.Select()

                '   Console.WriteLine(Now.ToLongTimeString)

            Else
                helperBean.SearchPositionNow = 0
                MessageBox.Show("Search Term Not Found!", "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("Error Searching for Term is " & ex.Message, "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            'for very large files linke 3 million lines
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try



    End Sub

    'searching XML file
    Public Function FindMyText(ByVal searchText As String, ByVal searchStart As Integer, ByVal searchEnd As Integer, ByVal WholeWord As Boolean) As Integer


        ' Initialize the return value to false by default.
        Dim returnValue As Integer = -1

        ' Ensure that a search string and a valid starting point are specified.
        If searchText.Length > 0 And searchStart >= 0 Then
            ' Ensure that a valid ending value is provided.
            If searchEnd > searchStart Or searchEnd = -1 Then
                ' Obtain the location of the search string in richTextBox1.
                Dim indexToText As Integer = 0
                If WholeWord Then
                    ' indexToText = Oysterform.RichTextBox11.Find(searchText, searchStart, searchEnd, RichTextBoxFinds.WholeWord)
                    indexToText = Oysterform.RichTextBox11.Find(searchText, searchStart, RichTextBoxFinds.WholeWord)
                Else
                    ' indexToText = Oysterform.RichTextBox11.Find(searchText, searchStart, searchEnd, RichTextBoxFinds.None)
                    indexToText = Oysterform.RichTextBox11.Find(searchText, searchStart, RichTextBoxFinds.None)
                End If
                '  Dim indexToText As Integer = RichTextBox11.Find(searchText, searchStart, searchEnd, RichTextBoxFinds.None)
                ' Determine whether the text was found in richTextBox1.
                If indexToText >= 0 Then
                    ' Return the index to the specified search text.
                    returnValue = indexToText
                End If
            End If
        End If

        Return returnValue
    End Function




    'reads the Oyster Run Script used in Oyster Run for the link file name and returns it for use and loading textboxes,etc.
    Protected Friend Shared Function GetLinkFileName(ByVal oysterRootDirectory As String, ByVal oysterWorkingDirectory As String) As String

        Dim linkFileName As String = Nothing
        Dim fullPathAndName As String = Nothing
        Dim objReader As System.IO.StreamReader = Nothing

        Try
            fullPathAndName = oysterRootDirectory & "\" & Path.GetFileName(oysterWorkingDirectory) & "RunScript.xml"



            If System.IO.File.Exists(fullPathAndName) Then

                Dim xelement As XElement = XElement.Load(fullPathAndName)

                linkFileName = xelement.Descendants("LinkOutput").Value
                If Not linkFileName.Contains("Oyster") Then
                    linkFileName = oysterRootDirectory + linkFileName.Substring(1)
                End If

                ' Console.WriteLine("NEW " & linkFileName)
                ' Dim objReader As System.IO.StreamReader = Nothing
                'objReader = New System.IO.StreamReader(fullPathAndName)
                'Do While objReader.Peek() <> -1
                '    Dim st As String = objReader.ReadLine()
                '    If st.Contains("<LinkOutput Type=") And Not st.Contains("#") Then

                '        Dim str As String = Nothing
                '        'some files still have the hard coded path
                '        If st.ToUpper.Contains(Oysterform.OysterRootDirTxBx.Text.ToUpper) Then
                '            str = st.Substring(st.IndexOf(">") + 1)
                '            linkFileName = str.Substring(0, str.LastIndexOf("<"))
                '        Else
                '            str = st.Substring(st.IndexOf(">") + 2)
                '            linkFileName = oysterRootDirectory + str.Substring(0, str.LastIndexOf("<"))
                '        End If

                '        Console.WriteLine("ME " & linkFileName)

                '        Exit Do
                '    End If
                'Loop
            Else
                    MessageBox.Show(Constants.RunScriptNotFound, "Link File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("Error Thrown Getting Link File Name is " & ex.Message, "Oyster Run File", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            'If objReader IsNot Nothing Then
            '    objReader.Close()
            '    objReader = Nothing

            'End If
        End Try
        Return linkFileName
    End Function

    'parses the SourceDescriptor xml to get the CSV file output name
    Protected Friend Shared Function GetInputCSVFileName(ByVal oysterWorkingDirectory As String) As String

        Dim CSVFileName As String = Nothing
        Dim fullPathAndName As String = Nothing
        Dim objReader As System.IO.StreamReader = Nothing
        Try
            fullPathAndName = oysterWorkingDirectory + "\scripts\" + Path.GetFileName(oysterWorkingDirectory) + "SourceDescriptor.xml"

            If System.IO.File.Exists(fullPathAndName) Then

                Dim xelement As XElement = XElement.Load(fullPathAndName)

                CSVFileName = xelement.Descendants("Source").Value

                If Not CSVFileName.Contains("Oyster") Then
                    CSVFileName = Oysterform.OysterRootDirTxBx.Text + CSVFileName.Substring(1)
                End If
                '   Console.WriteLine("NEW " & CSVFileName)

                'objReader = New System.IO.StreamReader(fullPathAndName)
                'Do While objReader.Peek() <> -1
                '    Dim st As String = objReader.ReadLine()
                '    If st.Contains("<Source ") And Not st.Contains("#") And Not st.Contains("<!") Then

                '        Dim str As String = Nothing
                '        'some files still have the hard coded path
                '        If st.ToUpper.Contains(Oysterform.OysterRootDirTxBx.Text.ToUpper) Then
                '            str = st.Substring(st.IndexOf(">") + 1)
                '            CSVFileName = str.Substring(0, str.LastIndexOf("<"))
                '        Else
                '            str = st.Substring(st.IndexOf(">") + 2)
                '            CSVFileName = oysterWorkingDirectory.Substring(0, oysterWorkingDirectory.LastIndexOf("\")) + str.Substring(0, str.LastIndexOf("<"))
                '            Console.WriteLine("Me " & CSVFileName)
                '        End If

                '        'Exit Do
                '    End If
                'Loop

            Else
                MessageBox.Show(Constants.RunScriptNotFound, "CSV File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("Error Thrown Getting CSVFile Name is " & ex.Message, "Oyster Run File", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            'If objReader IsNot Nothing Then
            '    objReader.Close()
            '    objReader = Nothing
            'End If

        End Try

        Return CSVFileName

    End Function

    'move the link file to the ERMetrics directory for use by ERMetrics process
    Public Sub MoveLinkFile()
        Try
            If Not String.IsNullOrEmpty(Oysterform.ERMetLinkFileTxbx.Text) Then
                If Not File.Exists(Oysterform.ERMetLinkFileTxbx.Text) Then
                    MessageBox.Show("File Does Not Exist", "Check  File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Oysterform.ERMetLinkFileTxbx.Text = Nothing
                    Oysterform.HSLinkFileTxBx.Text = Nothing
                    My.Settings.ERMetricsLinkFile = Nothing
                    My.Settings.Save()
                    Exit Sub
                End If

                If Not String.IsNullOrEmpty(Oysterform.ERMetricsTxBx.Text) Then

                    If File.ReadAllText(Oysterform.ERMetLinkFileTxbx.Text).Length = 0 Then
                        MessageBox.Show(Constants.LinkFileEmpty, "File Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                    Dim fileToCopy As String = Oysterform.ERMetLinkFileTxbx.Text
                    Oysterform.HSLinkFileTxBx.Text = Oysterform.ERMetLinkFileTxbx.Text
                    Dim fileItSelf As String = Path.GetFileName(Oysterform.ERMetLinkFileTxbx.Text)
                    Dim checkFile As String = Oysterform.ERMetricsTxBx.Text + "\" + fileItSelf
                    '' Move the file.
                    File.Copy(fileToCopy, checkFile, True)

                    RunProcessTab(Constants.ERMetricsSetup)
                    MessageBox.Show(Constants.CheckERPropertiesFile, "Check Properties File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show(Constants.EmptyERMetricsDirectory, "Empty Direcytory", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show(Constants.EmptyLinkFileTextBox, "Link File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show(Constants.ErrorCopyingFile & " " & ex.Message, "Copy Link File Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub
    'save information on the Welcome tab
    Public Sub Save_All_RobotTab()
        Try


            My.Settings.PythonExeDir = Oysterform.PythonExeTxBx.Text
            My.Settings.RobotWrkDir = Oysterform.RobotWkDirTxBx.Text
            '   My.Settings.OysterJar = Oysterform.OysterJarTxBx.Text
            '    Oysterform.TreeView1.Show()
            ''make ER Oyster Directory the same as Oyster Working Directory
            'Oysterform.EROysterWkDirTxBx.Text = Oysterform.OysterWorkDirTxBx.Text
            'My.Settings.EROysterWorkDir = Oysterform.OysterWorkDirTxBx.Text
            My.Settings.Save()

            MessageBox.Show(Constants.SettingsSaved, "Choices Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)


            'If Not Oysterform.PythonExeTxBx.Text = Nothing Then
            '    Oysterform.OysterXmlListBx.DataSource = Nothing
            '    Oysterform.OysterXmlListBx.Items.Clear()
            '    Oysterform.OysterXmlListBx.DataSource = IO.Directory.GetFiles(Oysterform.OysterRootDirTxBx.Text, "*.xml") _
            '    .Select(Function(file) IO.Path.GetFileName(file)) _
            '    .ToList
            'End If

            Oysterform.RunPythonBtn.Enabled = True
            RunProcessTab(Constants.RobotTab)

            'If Not My.Settings.LastRunFile = Nothing Then
            '    Oysterform.OysterXmlListBx.SelectedItem = My.Settings.LastRunFile
            'End If

        Catch ex As Exception
            MessageBox.Show("Error Saving Tab is " & ex.Message, "Saving Choices", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    'save information on the Welcome tab
    Public Sub Save_All_WelcomeTab()
        Try


            My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
            My.Settings.OysterWorkingDir = Oysterform.OysterWorkDirTxBx.Text
            My.Settings.OysterJar = Oysterform.OysterJarTxBx.Text
            Oysterform.TreeView1.Show()
            'make ER Oyster Directory the same as Oyster Working Directory
            Oysterform.EROysterWkDirTxBx.Text = Oysterform.OysterWorkDirTxBx.Text
            My.Settings.EROysterWorkDir = Oysterform.OysterWorkDirTxBx.Text
            My.Settings.Save()

            MessageBox.Show(Constants.SettingsSaved, "Choices Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)


            If Not Oysterform.OysterRootDirTxBx.Text = Nothing Then
                Oysterform.OysterXmlListBx.DataSource = Nothing
                Oysterform.OysterXmlListBx.Items.Clear()
                Oysterform.OysterXmlListBx.DataSource = IO.Directory.GetFiles(Oysterform.OysterRootDirTxBx.Text, "*.xml") _
                .Select(Function(file) IO.Path.GetFileName(file)) _
                .ToList
            End If

            Oysterform.RunOyster.Enabled = True
            RunProcessTab(Constants.Welcome)

            If Not My.Settings.LastRunFile = Nothing Then
                Oysterform.OysterXmlListBx.SelectedItem = My.Settings.LastRunFile
            End If

        Catch ex As Exception
            MessageBox.Show("Error Saving Tab is " & ex.Message, "Saving Choices", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    'checks the textboxes passed to see if they are empty to cut down on 
    'calls to process tabs.
    Public Function CheckTextboxes(ByRef TBox As TextBox) As Boolean

        Dim TextBoxName As String = TBox.Name
        If String.IsNullOrEmpty(TBox.Text) Or String.IsNullOrWhiteSpace(TBox.Text) Then
            Return False
        End If

        Return True
    End Function

    'process tabs
    Public Sub RunProcessTab(ByVal Name As String)

        Try


            'this is done to cut down on calls and 
            'make sure the right information is filled in
            'if not get out of here
            If Name.Equals(Constants.Welcome) Then
                If Not CheckTextboxes(Oysterform.OysterRootDirTxBx) Then
                    Exit Sub
                ElseIf Not CheckTextboxes(Oysterform.OysterWorkDirTxBx) Then
                    Exit Sub
                ElseIf Not CheckTextboxes(Oysterform.OysterJarTxBx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.ERMetricsSetup) Then
                If Not CheckTextboxes(Oysterform.ERMetricsTxBx) Then
                    Exit Sub
                    'ElseIf Not checkTextboxes(Oysterform.ERMetricsJarTxtBx) Then
                    '    Exit Sub
                ElseIf Not CheckTextboxes(Oysterform.EROysterWkDirTxBx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.HelpfulStuff) Then
                If Not CheckTextboxes(Oysterform.OysterWorkDirTxBx) Then
                    Exit Sub
                ElseIf Not CheckTextboxes(Oysterform.ERMetLinkFileTxbx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.WorkWithOysterFiles) Then

                If Not CheckTextboxes(Oysterform.OysterWorkDirTxBx) Then
                    Exit Sub
                ElseIf Not CheckTextboxes(Oysterform.ERMetricsTxBx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.KnowledgeBaseMaint) Then
                If Not CheckTextboxes(Oysterform.OysterRootDirTxBx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.KnowledgeBaseView) Then
                If Not CheckTextboxes(Oysterform.OysterRootDirTxBx) Then
                    Exit Sub
                End If
            ElseIf Name.Equals(Constants.RobotTab) Then
                If Not CheckTextboxes(Oysterform.PythonExeTxBx) Then
                    Exit Sub
                End If
                If Not CheckTextboxes(Oysterform.RobotWkDirTxBx) Then
                    Exit Sub
                End If
            End If



            'everything must be a go so select the tab from OysterTabs by
            'its name that was sent over and get all the controls.
            Oysterform.OysterTabs.SelectTab(Name)
            Dim CtrlCollection As Control.ControlCollection = Oysterform.OysterTabs.SelectedTab.Controls
            'needed to access HelperTabControl
            Dim helperTabControl As New HelperTabControl
            'if there is no treeview on the tab then we need to know that for how we call the 
            'proceesTab method in HelperTabControl
            Dim noTreeView As Boolean = True

            For Each ctl As Control In CtrlCollection
                If TypeOf ctl Is TreeView Then
                    helperTabControl.ProcessTab(Oysterform.OysterTabs.SelectedTab.Name, ctl)
                    noTreeView = False
                    Exit For
                End If
            Next
            'if there was no treeview in controls then call with the ctl set to nothing
            If noTreeView Then
                helperTabControl.ProcessTab(Oysterform.OysterTabs.SelectedTab.Name, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show("Error Processing Tab is " & ex.Message, "Processing Tab", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub


    'run Oyster
    Public Sub RunOyster(ByVal helperBean As HelperBean)
        Dim hBean As New HelperBean
        hBean.RichTexBoxNameBool = helperBean.RichTexBoxNameBool
        Dim pc As ProcessClass
        Try
            ' Console.WriteLine("in nexe runoyster method")
            'save run info for analysis page to come - not used at this time
            ' Dim ahelperBean As New HelperBean
            'ahelperBean.OysterRunScriptName = Path.GetFileName(Oysterform.OysterWorkDirTxBx.Text)
            'ahelperBean.OysterRunTimeValue = Now.ToString
            Oysterform.RunTimeValuelbl.Text = Nothing
            'Oysterform.list.Add(ahelperBean)

            Oysterform.LFDateLbl.Text = Nothing
            Oysterform.HSLinkDateLbl.Text = Nothing
            'if everything is ready load ermetrics link file for this run
            '  If Not String.IsNullOrEmpty(Oysterform.OysterRootDirTxtBx.Text) And Not String.IsNullOrEmpty(Oysterform.OysterWorkDirTxBx.Text) Then
            Dim linkFile As String = GetLinkFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
            Dim csvFile As String = GetInputCSVFileName(Oysterform.OysterWorkDirTxBx.Text)

            'if link file exists and is not null coming back from call lets use it
            If Not String.IsNullOrEmpty(linkFile) And Not String.IsNullOrWhiteSpace(linkFile) Then

                Oysterform.ERMetLinkFileTxbx.Text = linkFile
                My.Settings.ERMetricsLinkFile = linkFile
                My.Settings.Save()

                Oysterform.HSLinkFileTxBx.Text = linkFile
                Oysterform.LFDateLbl.Text = IO.File.GetLastWriteTime(linkFile)
                Oysterform.HSLinkDateLbl.Text = Oysterform.LFDateLbl.Text
                '   Oysterform.HSRefCSvTxBx.Text = Oysterform.HSCsvTxBx.Text
                '  Oysterform.HSRefLinkTxBx.Text = Oysterform.ERMetLinkFileTxbx.Text

            End If


            If Not String.IsNullOrEmpty(csvFile) And IO.File.Exists(csvFile) Then

                Oysterform.HSCsvTxBx.Text = csvFile
                Oysterform.InputFileLbl.Text = IO.Path.GetFileName(csvFile)

            End If

            Oysterform.ToolStripStatusLabel1.Text = "Oyster Running"
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = True
            Oysterform.ToolStripProgressBar1.PerformStep()
            Oysterform.ErrorInRunLbl.Visible = False

            'save settings before run incase user has not
            My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
            My.Settings.OysterWorkingDir = Oysterform.OysterWorkDirTxBx.Text
            My.Settings.OysterJar = Oysterform.OysterJarTxBx.Text
            My.Settings.LastRunFile = Oysterform.OysterXmlListBx.SelectedValue
            My.Settings.Save()
            ''''''''''''''''''''''''''''
            Oysterform.Timelbl.Text = "00:00:00"
            Dim runScript As String = Oysterform.OysterXmlListBx.SelectedValue
            Oysterform.RunScriptNamelbl.Text = runScript
            Oysterform.LinkFilelbl.Text = IO.Path.GetFileName(My.Settings.ERMetricsLinkFile)

            Dim stopwatch As New Stopwatch

            ' Oysterform.RunTimeValuelbl.Text = Now.ToLongTimeString
            Dim jarFile As String = IO.Path.GetFileName(Oysterform.OysterJarTxBx.Text)
            pc = New ProcessClass
            pc.Run(Oysterform.OysterRootDirTxBx.Text, runScript, True, jarFile, hBean)
            Oysterform.StopRun.Enabled = True

        Catch ex As ArgumentException
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False
            Oysterform.StopRun.Enabled = False
            MessageBox.Show(ex.Message, "Oyster Run Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)

        Finally
            pc = Nothing

        End Try

    End Sub

    'sub to help with textbox entry
    Public Sub CheckOysterDirectories()

        Try

            Dim filesExist As Boolean = False
            If Not String.IsNullOrEmpty(Oysterform.OysterRootDirTxBx.Text) Then
                Oysterform.FolderBrowserDialog1.SelectedPath = Oysterform.OysterRootDirTxBx.Text
            Else
                MessageBox.Show(Constants.OysterEmpty, "Empty Oyster TextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Oysterform.OysterWorkDirTxBx.Text = Nothing
                Exit Sub
            End If

            If Oysterform.FolderBrowserDialog1.ShowDialog = DialogResult.OK And Not Oysterform.FolderBrowserDialog1.SelectedPath.ToUpper.Equals("C:\") _
                And Not Oysterform.FolderBrowserDialog1.SelectedPath.ToUpper.Equals("D:\") Then

                Dim path As String = Oysterform.FolderBrowserDialog1.SelectedPath

                If Not path.Contains(Oysterform.OysterRootDirTxBx.Text) Then
                    MessageBox.Show(Constants.CorrectWorkingDirectory, "Empty Oyster Work Directory TextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else

                    Dim picList As String() = Directory.GetFiles(path, "*.xml", searchOption:=SearchOption.AllDirectories)

                    If picList.Count > 0 Then
                        For Each item As String In picList
                            'looking for the Atributes file to make sure have the a good directory
                            If item.ToUpper.Contains(Constants.ATTRIBUTES) Then
                                filesExist = True
                                Oysterform.OysterWorkDirTxBx.Text = path
                                If Not path.ToUpper.Equals(My.Settings.OysterWorkingDir.ToUpper) Then
                                    My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
                                    My.Settings.OysterWorkingDir = Oysterform.OysterWorkDirTxBx.Text
                                    Oysterform.EROysterWkDirTxBx.Text = Oysterform.OysterWorkDirTxBx.Text
                                    My.Settings.Save()
                                    Dim oysterHelper As New OysterHelper
                                    RunProcessTab(Constants.Welcome)
                                    oysterHelper = Nothing
                                End If
                                Exit For
                            End If
                        Next

                    Else
                        MessageBox.Show(Constants.WorkDirUnderRoot, "Work Directory TextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            Else
                If String.IsNullOrEmpty(Oysterform.OysterWorkDirTxBx.Text) Then
                    MessageBox.Show(Constants.WorkDirUnderRoot, "Work Directory TextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If

        Catch ex As Exception
            MessageBox.Show("Error in Checking Oyster Directories is " & ex.Message, "Checking Oyster Directories", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    'Kill the running process, Oyster or ERMetrics
    Public Sub Kill_Run()

        ' Dim response As Integer = 0

        If TestHelper.Testing = True And My.Settings.Testing = True Then

            Dim response As Integer = MessageBox.Show(Constants.StopJava, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)

            If response = DialogResult.OK Then
                Dim _proceses As Process()
                Try
                    _proceses = Process.GetProcessesByName("java")
                    For Each proces As Process In _proceses
                        proces.Kill()
                    Next
                    Oysterform.ToolStripProgressBar1.Visible = False
                    Oysterform.StopRun.Enabled = False
                Catch ex As ArgumentException
                    'MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Finally
                    _proceses = Nothing
                    Oysterform.ToolStripProgressBar1.Visible = False
                    Oysterform.StopRun.Enabled = False
                    'stop the stopwatch in it is running
                    If Oysterform.stopWatch.IsRunning Then
                        Oysterform.stopWatch.Stop()
                    End If
                    Oysterform.RunERMetricsBtn.Enabled = True
                End Try

            End If

        Else
            Dim response As Integer = MessageBox.Show(Constants.StopJava, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Information)

            If response = DialogResult.Yes Then
                Dim _proceses As Process()
                Try
                    _proceses = Process.GetProcessesByName("java")
                    For Each proces As Process In _proceses
                        proces.Kill()
                    Next
                    Oysterform.ToolStripProgressBar1.Visible = False
                    Oysterform.StopRun.Enabled = False
                Catch ex As ArgumentException
                    'MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Finally
                    _proceses = Nothing
                    Oysterform.ToolStripProgressBar1.Visible = False
                    Oysterform.StopRun.Enabled = False
                    'stop the stopwatch in it is running
                    If Oysterform.stopWatch.IsRunning Then
                        Oysterform.stopWatch.Stop()
                    End If
                    Oysterform.RunERMetricsBtn.Enabled = True
                End Try

            End If

        End If
        ''  response = MessageBox.Show(Constants.StopJava, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Console.WriteLine("OK Response first " & response)
        '' Dim response As DialogResult = MessageBox.Show(Constants.StopJava, "Warning", MsgBoxStyle.YesNo)

        'If response = DialogResult.OK Then
        '    Dim _proceses As Process()
        '    Try
        '        _proceses = Process.GetProcessesByName("java")
        '        For Each proces As Process In _proceses
        '            proces.Kill()
        '        Next
        '        Oysterform.ToolStripProgressBar1.Visible = False
        '        Oysterform.StopRun.Enabled = False
        '    Catch ex As ArgumentException
        '        'MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Finally
        '        _proceses = Nothing
        '        Oysterform.ToolStripProgressBar1.Visible = False
        '        Oysterform.StopRun.Enabled = False
        '        'stop the stopwatch in it is running
        '        If Oysterform.stopWatch.IsRunning Then
        '            Oysterform.stopWatch.Stop()
        '        End If
        '        Oysterform.RunERMetricsBtn.Enabled = True
        '    End Try

        'End If


    End Sub

    ''Kill the running process, Oyster or ERMetrics
    'Public Sub Kill_Oyster()

    '    Dim _proceses As Process()
    '    Try
    '        _proceses = Process.GetProcessesByName("java")
    '        For Each proces As Process In _proceses
    '            proces.Kill()
    '        Next
    '        Oysterform.ToolStripProgressBar1.Visible = False
    '        Oysterform.StopRun.Enabled = False
    '    Catch ex As ArgumentException

    '    Finally
    '        _proceses = Nothing
    '        Oysterform.ToolStripProgressBar1.Visible = False
    '        Oysterform.StopRun.Enabled = False
    '        'stop the stopwatch in it is running

    '    End Try


    'End Sub

    'check the path and files chosen to make sure it can be used
    'paths and jar files are very important and must be rigid checks
    Public Function CheckPath(ByVal Path As String, ByVal DirType As String) As Boolean

        Dim list As FileInfo() = Nothing
        Dim yesNo As Boolean = False

        Try


            Select Case DirType
                Case Constants.Root
                    'this InfoCall is for errors thrown using FileInfo and Directory Info 
                    'on restricted directories and files. 
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim directoryInfo As New DirectoryInfo(Path)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.jar")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.Contains(Constants.OYSTER) Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.WorkDirectory
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim sPath As String = Path + "\" + "scripts"
                        Dim directoryInfo As New DirectoryInfo(sPath.ToUpper)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.xml")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.EndsWith(".XML") Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.Python
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim directoryInfo As New DirectoryInfo(Path)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.exe")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.Contains(Constants.Python) Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.Robot
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim directoryInfo As New DirectoryInfo(Path)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.py")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.Contains(Constants.Robot) Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.ERMetrics
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim directoryInfo As New DirectoryInfo(Path)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.jar")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.Contains(Constants.ERMETRICSJAR) Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.OysterJar
                    Dim fileOk As Boolean = InfoCall(Path, Constants.File)
                    If fileOk Then
                        Dim fileInfo As New FileInfo(Path)
                        If fileInfo.Name.ToUpper.Contains(Constants.OYSTER) _
                    And fileInfo.Name.ToUpper.Contains(".JAR") Then
                            If fileInfo.Exists Then
                                yesNo = True
                            Else
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    End If
                Case Constants.ERMETRICSJAR
                    Dim fileOk As Boolean = InfoCall(Path, Constants.File)
                    If fileOk Then
                        Dim fileInfo As New FileInfo(Path)
                        If fileInfo.Name.ToUpper.Contains(Constants.ERMETRICSJAR) _
                              And fileInfo.Name.Contains(".jar") Then
                            If fileInfo.Exists Then
                                yesNo = True
                            Else
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    End If

                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Checking Paths", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return yesNo
    End Function

    'gerneric method to handel all loadable textboxes
    'check if the user inserted a space
    Public Sub TextChangedSpace(ByRef TextBox As TextBox)

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")

        If regSpace.IsMatch(TextBox.Text) Then

            Dim TextBoxName As String = TextBox.Name
            Dim TxtBoxValueTrimmed = TextBox.Text.Trim
            Dim SettingsValue As String

            Select Case TextBoxName
                Case "OysterJarTxBx"
                    SettingsValue = My.Settings.OysterJar
                Case "ERMetricsJarTxBx"
                    SettingsValue = My.Settings.ERMetricsJar
                Case "OysterRootDirTxBx"
                    SettingsValue = My.Settings.OysterRootDir
                Case "OysterWorkDirTxBx"
                    SettingsValue = My.Settings.OysterWorkingDir
                Case "ERMetricsTxBx"
                    SettingsValue = My.Settings.ERMetricsWorkDir
                Case "PythonExeTxBx"
                    SettingsValue = My.Settings.PythonExeDir
                Case "RobotWkDirTxBx"
                    SettingsValue = My.Settings.RobotWrkDir
                Case "NewJarTxBx"
                    SettingsValue = My.Settings.NewTestJar
                Case Else
                    'if your not identified above 
                    'we don't care about you (readonly?)   RobotWkDirTxBx
                    Exit Sub
            End Select

            If Not String.IsNullOrEmpty(SettingsValue) Then
                TextBox.Text = SettingsValue
                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                TextBox.Text = TxtBoxValueTrimmed
                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

    'This InfoCall is for errors thrown using FileInfo and Directory Info
    'returns false if there is a problem with the chosen 
    Protected Shared Function InfoCall(TextBoxText As String, InfoType As String) As Boolean
        Try
            'if it throws and error then not good
            If InfoType.Equals(Constants.Directory) Then
                Dim directoryInfo As New DirectoryInfo(TextBoxText)
                If directoryInfo.Exists Then
                    Return True
                Else
                    Return False
                End If
            ElseIf InfoType.Equals(Constants.File) Then
                Dim fileInfo As New FileInfo(TextBoxText)
                If fileInfo.Exists Then
                    Return True
                Else
                    Return False
                End If

            End If

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    'Text changed so make sure it is ok
    Public Sub TextChanged(ByRef TextBox As TextBox)

        Try


            If Not String.IsNullOrEmpty(TextBox.Text) Then

                Dim DirType As String = Nothing
                Dim SettingValue As String = Nothing
                Dim Message As String = Nothing
                Dim ValueNow As String = TextBox.Text
                Dim TextboxName As String = TextBox.Name

                If TextboxName.Contains("Jar") Then

                    Dim fileOK As Boolean = InfoCall(TextBox.Text, Constants.File)

                    Select Case TextboxName
                        Case "OysterJarTxBx"
                            DirType = Constants.OysterJar
                            SettingValue = My.Settings.OysterJar
                            Message = "Oyster Jar Directory"
                        Case "ERMetricsJarTxBx"
                            DirType = Constants.ERMETRICSJAR
                            SettingValue = My.Settings.ERMetricsJar
                            Message = "ERMetrics Jar Directory"
                        Case "NewJarTxBx"
                            DirType = Constants.OysterJar
                            SettingValue = My.Settings.NewTestJar
                            Message = "Oyster Test Jar Directory"
                        Case Else
                            DirType = "Nothing"
                            Exit Sub
                    End Select

                    If fileOK Then

                        If CheckPath(ValueNow, DirType) Then
                            TextBox.Text = ValueNow
                        Else
                            If Not String.IsNullOrEmpty(SettingValue) Then
                                TextBox.Text = SettingValue
                                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Else
                                TextBox.Text = Nothing
                                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                        'Else
                    Else
                        If Not String.IsNullOrEmpty(SettingValue) Then
                            TextBox.Text = SettingValue
                            MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            TextBox.Text = Nothing
                            MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If


                Else
                    Dim directoryOk As Boolean = InfoCall(TextBox.Text, Constants.Directory)

                    Select Case TextboxName
                        Case "OysterRootDirTxBx"
                            DirType = Constants.Root
                            SettingValue = My.Settings.OysterRootDir
                            Message = "Oyster Root Directory"
                        Case "OysterWorkDirTxBx"
                            DirType = Constants.WorkDirectory
                            SettingValue = My.Settings.OysterWorkingDir
                            Message = "Not The Right Directory"
                        Case "ERMetricsTxBx"
                            DirType = Constants.ERMetrics
                            SettingValue = My.Settings.ERMetricsWorkDir
                            Message = "ERMetrics Directory"
                        Case "PythonExeTxBx"
                            DirType = Constants.Python
                            SettingValue = My.Settings.PythonExeDir
                            Message = "Not The Right Directory"
                        Case "RobotWkDirTxBx"
                            DirType = Constants.Robot
                            SettingValue = My.Settings.RobotWrkDir
                            Message = "Not The Right Directory"
                        Case Else
                            Exit Sub
                    End Select

                    If directoryOk Then

                        If CheckPath(ValueNow, DirType) Then
                            TextBox.Text = ValueNow
                        Else
                            If Not String.IsNullOrEmpty(SettingValue) Then
                                TextBox.Text = SettingValue
                                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Else
                                TextBox.Text = Nothing
                                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                    Else
                        If Not String.IsNullOrEmpty(SettingValue) Then

                            MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            TextBox.Text = SettingValue
                        Else

                            MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            TextBox.Text = Nothing
                        End If
                    End If
                End If
            ElseIf String.IsNullOrEmpty(TextBox.Text) Then
                'Dim TextBox_Name = TextBox.Name
                'Dim SettingsValue As String = Nothing

                'Select Case TextBox_Name
                '    Case "OysterJarTxBx"
                '        SettingsValue = My.Settings.OysterJar
                '    Case "ERMetricsJarTxBx"
                '        SettingsValue = My.Settings.ERMetricsJar
                '    Case "OysterRootDirTxBx"
                '        SettingsValue = My.Settings.OysterRootDir
                '    Case "OysterWorkDirTxBx"
                '        SettingsValue = My.Settings.OysterWorkingDir
                '    Case "ERMetricsTxBx"
                '        SettingsValue = My.Settings.ERMetricsWorkDir
                '    Case Else

                'End Select
                'If Not String.IsNullOrEmpty(SettingsValue) Then


                '    MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    TextBox.Text = SettingsValue
                'Else

                '    ' MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    '  TextBox.Text = Nothing
                'End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error in TextChanged is " & ex.Message, "Text Changed Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub


    'Save the ERMetrics TextBoxes 
    Public Sub SaveERmetricsTxBoxes()
        'make sure no one is null or empty
        Try


            If Not String.IsNullOrEmpty(Oysterform.ERMetricsTxBx.Text) _
            And Not String.IsNullOrEmpty(Oysterform.EROysterWkDirTxBx.Text) _
            And Not String.IsNullOrEmpty(Oysterform.ERMetricsJarTxBx.Text) Then
                'save them when you can
                My.Settings.ERMetricsWorkDir = Oysterform.ERMetricsTxBx.Text
                My.Settings.ERMetricsJar = Oysterform.ERMetricsJarTxBx.Text
                My.Settings.EROysterWorkDir = Oysterform.EROysterWkDirTxBx.Text
                My.Settings.Save()

                MessageBox.Show(Constants.SettingsSaved, "Save Settings", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Dim myDir As String = Oysterform.ERMetricsTxBx.Text
                '  Dim topNode As String = Nothing

                If Oysterform.TreeView4.GetNodeCount(False) > 0 Then
                    Oysterform.TreeView4.Nodes.Clear()
                End If

                Oysterform.TreeView4.Nodes.Add(myDir, myDir)

                For Each file As String In My.Computer.FileSystem.GetFiles(myDir)

                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".properties", comparisonType:=1) Then
                        Oysterform.TreeView4.Nodes(0).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    ElseIf st.EndsWith("s.log", comparisonType:=1) Then
                        Oysterform.TreeView4.Nodes(0).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    ElseIf st.EndsWith(".out", comparisonType:=1) Then
                        Oysterform.TreeView4.Nodes(0).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    End If

                Next
                Dim dir As String = Oysterform.EROysterWkDirTxBx.Text + "\" + "Output"
                Oysterform.TreeView4.Nodes.Add(dir, dir)

                For Each file As String In My.Computer.FileSystem.GetFiles(dir)
                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".link", comparisonType:=1) Then
                        Oysterform.TreeView4.Nodes(1).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    End If

                Next

                Oysterform.TreeView4.Nodes(0).Expand()
                Oysterform.TreeView4.Nodes(1).Expand()
                Oysterform.TreeView4.Nodes(1).EnsureVisible()
                Oysterform.TreeView4.Show()

            Else
                MessageBox.Show(Constants.ERTextBoxesMustBeFilled, "ERMetrics Text Boxes", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Error Saving ERMetrics TextBoxes", "ERMetrics Text Boxes", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    'Run ERMetrics
    Public Sub RunERMetrics(ByVal helperBean As HelperBean)
        Dim hBean As New HelperBean
        hBean.RichTexBoxNameBool = helperBean.RichTexBoxNameBool
        Dim pc As New ProcessClass
        Try

            Dim jarExists As Boolean = My.Computer.FileSystem.FileExists(Oysterform.ERMetricsJarTxBx.Text)
            If jarExists Then
                Dim ahelperBean As New HelperBean
                ahelperBean.OysterRunScriptName = Constants.ERMetrics
                ahelperBean.OysterRunTimeValue = Now.ToString
                Oysterform.list.Add(ahelperBean)
                Dim jarFile As String = Path.GetFileName(Oysterform.ERMetricsJarTxBx.Text)
                pc.RunERMetrics(Oysterform.ERMetricsTxBx.Text, jarFile)
                Oysterform.StopRun.Enabled = True
                Oysterform.ToolStripStatusLabel1.Text = "ERMetrics Running"
                Oysterform.ToolStripStatusLabel1.Visible = True
                Oysterform.ToolStripProgressBar1.Visible = True
                Oysterform.ToolStripProgressBar1.PerformStep()
            Else
                MessageBox.Show(Constants.OysterWorkDirError, "Wrong Run File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As ArgumentException
            MessageBox.Show(ex.Message, "Error Running ERMetrics", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            pc = Nothing
        End Try

    End Sub


    'Generic method to fill text boxes with correct information
    Public Sub FillTextBox(ByRef Textbox As TextBox, ByVal path As String)

        Dim thePath As String = path
        Dim DirType As String = Nothing
        Dim SettingValue As String = Nothing
        Dim Message As String = Nothing
        Dim ValueNow As String = Textbox.Text
        Dim TextboxName As String = Textbox.Name.Trim
        Dim fileOK As Boolean = False
        Dim directoryOk As Boolean = False

        Try


            If Not String.IsNullOrEmpty(thePath) Then

                Select Case TextboxName

                    Case "OysterJarTxBx"
                        DirType = Constants.OysterJar
                        SettingValue = My.Settings.OysterJar
                        Message = Constants.OysterFlavorRequired
                        'make sure an error is not thrown
                        fileOK = InfoCall(thePath, Constants.File)
                    Case "ERMetricsJarTxBx"
                        DirType = Constants.ERMETRICSJAR
                        SettingValue = My.Settings.ERMetricsJar
                        Message = Constants.ERMetricsJarNotFound
                        fileOK = InfoCall(thePath, Constants.File)
                    Case "OysterRootDirTxBx"
                        DirType = Constants.Root
                        SettingValue = My.Settings.OysterRootDir
                        Message = Constants.RootMessage
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case "OysterWorkDirTxBx"
                        DirType = Constants.WorkDirectory
                        SettingValue = My.Settings.OysterWorkingDir
                        Message = Constants.WorkDirUnderRoot
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case "ERMetricsTxBx"
                        DirType = Constants.ERMetrics
                        SettingValue = My.Settings.ERMetricsWorkDir
                        Message = Constants.ERWorkingDirectory
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case "PythonExeTxBx"
                        DirType = Constants.Python
                        SettingValue = My.Settings.PythonExeDir
                        Message = Constants.PythonEXEDir
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case "RobotWkDirTxBx"
                        DirType = Constants.Robot
                        SettingValue = My.Settings.RobotWrkDir
                        Message = Constants.RobotWkDir
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case Else

                        Exit Sub
                End Select

            Else
                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If fileOK Then

                If CheckPath(thePath, DirType) Then
                    ' Textbox.Text = thePath
                    'If DirType.Equals(Constants.ERMetrics) Then
                    '    Textbox.Text = thePath
                    '    'My.Settings.ERMetricsWorkDir = thePath
                    '    'My.Settings.Save()
                    '    'Textbox.Text = My.Settings.ERMetricsWorkDir
                    If DirType.Equals(Constants.ERMETRICSJAR) Then
                        Textbox.Text = thePath
                        'My.Settings.ERMetricsJar = thePath
                        'My.Settings.Save()
                        'Textbox.Text = My.Settings.ERMetricsJar
                    ElseIf DirType.Equals(Constants.OysterJar) Then
                        Textbox.Text = thePath

                    End If
                    'Textbox.Text = thePath
                    MessageBox.Show("Please Save Your Choices!", "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    If Not String.IsNullOrEmpty(SettingValue) Then
                        Textbox.Text = SettingValue
                        MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        Textbox.Text = Nothing
                        MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

            ElseIf directoryOk Then

                If CheckPath(thePath, DirType) Then
                    'it passes so put it in
                    Textbox.Text = thePath

                    'this first one is to put the oyster working directory
                    'into the EROysterWorking directory becasue the oyster
                    'workdirectory was set above in TextBox.Text = thepath
                    If DirType.Equals(Constants.WorkDirectory) Then
                        'we know the root is set to get here
                        ' My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
                        '  My.Settings.OysterWorkingDir = Textbox.Text
                        'ERMetrics works with the same working directory
                        Oysterform.EROysterWkDirTxBx.Text = Textbox.Text
                        '  My.Settings.Save()
                        RunProcessTab(Constants.Welcome)
                    ElseIf DirType.Equals(Constants.Root) Then
                        'My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
                        'My.Settings.Save()
                        'refresh the tab where oyster root is
                        RunProcessTab(Constants.Welcome)
                    ElseIf DirType.Equals(Constants.ERMetrics) Then
                        'My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
                        'My.Settings.Save()
                        'refresh ermetrics tab
                        RunProcessTab(Constants.ERMetricsSetup)
                    ElseIf DirType.Equals(Constants.Robot) Then
                        'My.Settings.OysterRootDir = Oysterform.OysterRootDirTxBx.Text
                        'My.Settings.Save()
                        'refresh ermetrics tab
                        Oysterform.RobotWkDirTxBx.Text = Textbox.Text
                        ' RunProcessTab(Constants.RobotTab)

                        MessageBox.Show("Please Save Your Choices!", "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    If Not String.IsNullOrEmpty(SettingValue) Then
                        Textbox.Text = SettingValue
                        MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        Textbox.Text = Nothing
                        MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            ElseIf Not fileOK Then

                MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ElseIf Not directoryOk Then
                MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)


            End If

        Catch ex As Exception
            MessageBox.Show("Error TextBox is " & ex.Message, "TextBox Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    'method to get the information as to which file is being created in Helpful Stuff
    Function GetHelperBean(ByVal CreateCVSorReforCBS As Integer) As HelperBean

        Dim aHelperBean As New HelperBean
        Try


            'create CVS file if 1
            If CreateCVSorReforCBS = 1 Then


                My.Settings.CSVFileCount = My.Settings.CSVFileCount + 1
                My.Settings.Save()

                'making cvs file to run if True or truth file base if false
                aHelperBean.CSVorReforCBSFileCreation = 1
                Dim i As Integer = Oysterform.HSCsvTxBx.Text.LastIndexOf("\")

                aHelperBean.OutPutCSVFileAndPath = Oysterform.HSCsvTxBx.Text.Substring(0, i + 1) & "NewInputFile" & My.Settings.CSVFileCount.ToString & ".csv"

                'fill the HSOutCsvTxBx textbox with output csv
                Oysterform.HSOutCsvTxBx.Text = aHelperBean.OutPutCSVFileAndPath

                'fill helperbean to be sent to backgroundworker
                aHelperBean.InputCSVFileAndPath = Oysterform.HSCsvTxBx.Text
                aHelperBean.LinkFileFileAndPath = Oysterform.HSLinkFileTxBx.Text

                'create reference file if 2
            ElseIf CreateCVSorReforCBS = 2 Then


                My.Settings.RefFileCount = My.Settings.RefFileCount + 1
                My.Settings.Save()
                'making cvs file to run if True or truth/Reference file if false
                aHelperBean.CSVorReforCBSFileCreation = 2

                'write beginning reference file

                aHelperBean.OutPutRefFilePathOutPut = Oysterform.ERMetricsTxBx.Text & "\Reference" & My.Settings.RefFileCount.ToString & ".txt"

                Oysterform.HSRefOutputTxBx.Text = aHelperBean.OutPutRefFilePathOutPut

                'fill helperbean to be send to backgroundworker
                aHelperBean.InputCSVFileAndPath = Oysterform.HSCsvTxBx.Text
                aHelperBean.LinkFileFileAndPath = Oysterform.HSLinkFileTxBx.Text

                'create bootstrap file if 3
            ElseIf CreateCVSorReforCBS = 3 Then

                My.Settings.CBSFileCount = My.Settings.CBSFileCount + 1
                My.Settings.Save()
                'making cvs file to run if True or truth/Reference file if false
                aHelperBean.CSVorReforCBSFileCreation = 3

                Dim i As Integer = Oysterform.HSCsvTxBx.Text.LastIndexOf("\")

                'write beginning reference file
                aHelperBean.CBStrapFilePathOutPut = Oysterform.HSCsvTxBx.Text.Substring(0, i + 1) & "WeightFile" & My.Settings.CBSFileCount.ToString & ".csv"

                Oysterform.WgtFileTxBx.Text = aHelperBean.CBStrapFilePathOutPut

                'fill helperbean to be send to backgroundworker
                aHelperBean.InputCSVFileAndPath = Oysterform.HSCsvTxBx.Text
                aHelperBean.LinkFileFileAndPath = Oysterform.HSLinkFileTxBx.Text

            End If
        Catch ex As Exception
            MessageBox.Show("Error creating File is " & ex.Message, "File Creation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return aHelperBean


    End Function

    'Generic method to save richtextboxes
    Public Sub SaveRichTextBoxFile(ByVal RichTextBoxKey As Integer, ByVal myRichTextBox As RichTextBox, ByVal FileToSave As String)
        Try
            Dim helperBean As New HelperBean
            helperBean = Oysterform.saveInfoCollection.Item(RichTextBoxKey)
            myRichTextBox.SaveFile(FileToSave, RichTextBoxStreamType.PlainText)
            myRichTextBox.ReadOnly = True

            MessageBox.Show("File " & FileToSave & " Saved!", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)
            If FileToSave.ToUpper.Contains("RunScript") Then

            End If

        Catch ex As Exception
            MessageBox.Show("Error in Saving the File is " & ex.Message, "File Error On Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    'search the loaded file in the datagrid
    '--------------------------------------------
    Public Sub SearchDataGrid()

        Dim str As String = Nothing
        Dim bsource As BindingSource = Oysterform.DataGridView1.DataSource
        Try

            Dim HasCheck As Boolean = False
            For i As Integer = 0 To Oysterform.ColumnCheckedLB.Items.Count - 1

                Dim chkstate As CheckState

                chkstate = Oysterform.ColumnCheckedLB.GetItemCheckState(i)
                ' Toggle the item state.

                If (chkstate = CheckState.Checked) Then
                    HasCheck = True
                    Exit For

                End If
            Next


            If HasCheck Then
                If Oysterform.KBMWholeWordChB.Checked Then
                    bsource.Filter = String.Format(Oysterform.ColumnCheckedLB.GetItemText(Oysterform.ColumnCheckedLB.Items(Oysterform.ColumnCheckedLB.SelectedIndex)) & " = '{0}' ", Oysterform.KBMTextBox.Text)
                    Oysterform.KBMFilterCountLbl.Text = "Filtered Count is: " & bsource.Count.ToString
                Else
                    bsource.Filter = String.Format(Oysterform.ColumnCheckedLB.GetItemText(Oysterform.ColumnCheckedLB.Items(Oysterform.ColumnCheckedLB.SelectedIndex)) & " Like '%{0}%'", Oysterform.KBMTextBox.Text)
                    Oysterform.KBMFilterCountLbl.Text = "Filtered Count is: " & bsource.Count.ToString
                End If
            Else

                Dim columnCollection As DataGridViewColumnCollection = Oysterform.DataGridView1.Columns

                If Oysterform.KBMWholeWordChB.Checked Then

                    For Each col As DataGridViewColumn In columnCollection
                        If Not col.Index = -1 Then
                            If Not col.Name.Contains("OysterId") Then
                                str = str & col.Name & " =" & " '{0}' or "
                            Else
                                str = str & col.Name & " =" & " '{0}'"
                            End If
                        End If
                    Next

                    bsource.Filter = String.Format(str, Oysterform.KBMTextBox.Text)
                    Oysterform.KBMFilterCountLbl.Text = "Filtered Count is: " & bsource.Count.ToString

                Else


                    For Each col As DataGridViewColumn In columnCollection
                        If Not col.Index = -1 Then
                            If Not col.Name.Contains("OysterId") Then
                                str = str & col.Name & " Like" & " '%{0}%' or "
                            Else
                                str = str & col.Name & " Like" & " '%{0}%'"
                            End If
                        End If
                    Next

                    bsource.Filter = String.Format(str, Oysterform.KBMTextBox.Text)
                    Oysterform.KBMFilterCountLbl.Text = "Filtered Count is: " & bsource.Count.ToString

                End If

            End If
        Catch ex As Exception
            MessageBox.Show("Error Searching is " & ex.Message, "Search Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


    Public Sub ToolSelectEntireRow()
        '     Console.WriteLine("1 " & Now.ToLongTimeString)

        Try

            Oysterform.ToolStripStatusLabel1.Text = "Working"

            Dim dataGridCell As DataGridViewCell
            If Not Oysterform.DataGridView1.CurrentCell Is Nothing Then
                dataGridCell = Oysterform.DataGridView1.Item(Oysterform.DataGridView1.CurrentCell.ColumnIndex, Oysterform.DataGridView1.CurrentCell.RowIndex)

            Else
                MessageBox.Show("Click on a Cell Before using Right Click!", "Error Selecting", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
                Exit Sub
            End If

            Dim iRowIndex As Integer = dataGridCell.RowIndex
            Dim datagridRow As DataGridViewRow = Oysterform.DataGridView1.Rows.Item(iRowIndex)
            Dim g As Integer = Oysterform.DataGridView1.Columns.GetColumnCount(DataGridViewElementStates.Visible)
            Dim SearchWord As String = datagridRow.Cells(g - 1).Value
            Oysterform.SearchKBTxtBx.Text = SearchWord

            Dim hepBean As HelperBean = Oysterform.saveInfoCollection.Item(Constants.DataGridView1)
            Dim FileToLoad As String = hepBean.IKBFileToLoad
            Dim helpBean As New HelperBean
            helpBean.RichTextBoxName = "RichTextBox11"
            helpBean.FileLoaded = FileToLoad
            '     Console.WriteLine("2 " & Now.ToLongTimeString)
            If Oysterform.saveInfoCollection.Item(11) Is Nothing Then
                '  Console.WriteLine("3 " & Now.ToLongTimeString)
                Dim i As Integer = 11
                'use the integer in constants for each textbox to store
                'in the hashtable
                If Not Oysterform.saveInfoCollection.Item(i) Is Nothing Then
                    Oysterform.saveInfoCollection.Remove(i)
                    Oysterform.saveInfoCollection.Add(i, helpBean)
                Else
                    Oysterform.saveInfoCollection.Add(i, helpBean)
                End If

                Oysterform.FileToLoadToRTB = FileToLoad
                Dim Thread2 As Thread = New Thread(New ThreadStart(AddressOf Oysterform.SetText))
                Thread2.Start()
                '  Thread.Sleep(1000)
                'Oysterform.KBLoadFilelbl.Text = "Loaded File is: " & Path.GetFileName(FileToLoad)
                'Oysterform.KBFileLineCountLbl.Text = "Line Count is: " & Oysterform.RichTextBox11.Lines.Count.ToString("N0")

                'Oysterform.OysterTabs.SelectTab("KnowledgeBaseView")
                '             Console.WriteLine("4 " & Now.ToLongTimeString)
            ElseIf Not Oysterform.saveInfoCollection.Item(11) Is Nothing Then
                '  Console.WriteLine("5 " & Now.ToLongTimeString)
                Dim hBean As New HelperBean
                hBean = CType(Oysterform.saveInfoCollection.Item(11), HelperBean)

                If Not String.IsNullOrEmpty(hBean.FileLoaded) And hBean.FileLoaded.Trim.Equals(FileToLoad.Trim) Then
                    Oysterform.OysterTabs.SelectTab("KnowledgeBaseView")
                Else
                    Dim i As Integer = 11
                    'use the integer in constants for each textbox to store
                    'in the hashtable
                    If Not Oysterform.saveInfoCollection.Item(i) Is Nothing Then
                        Oysterform.saveInfoCollection.Remove(i)
                        Oysterform.saveInfoCollection.Add(i, helpBean)
                    Else
                        Oysterform.saveInfoCollection.Add(i, helpBean)
                    End If

                    Oysterform.RichTextBox11.LoadFile(FileToLoad, RichTextBoxStreamType.PlainText)
                    Oysterform.SearchKBTxtBx.Text = SearchWord
                    Oysterform.FileToLoadToRTB = FileToLoad
                    Dim Thread2 As Thread = New Thread(New ThreadStart(AddressOf Oysterform.SetText))
                    Thread2.Start()
                    '   Thread.Sleep(1000)
                    'Oysterform.KBLoadFilelbl.Text = "Loaded File is: " & Path.GetFileName(FileToLoad)
                    'Oysterform.KBFileLineCountLbl.Text = "Line Count is: " & Oysterform.RichTextBox11.Lines.Count.ToString("N0")
                    'Oysterform.OysterTabs.SelectTab("KnowledgeBaseView")
                End If

            End If
            '  Console.WriteLine("5 " & Now.ToLongTimeString)
        Catch ex As Exception
            MessageBox.Show("Error in load row is " & ex.Message, "Error Loading Row", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally

            Oysterform.ToolStripStatusLabel1.Text = "Oyster Helper"
            GC.WaitForPendingFinalizers()
            GC.Collect()

        End Try
    End Sub




    'Test Methods  Section-------------------------------------------------------
    '------------------------------------------------------------------
    Public Sub ReadTestFile()

        Dim readFileForm As New ReadFileForm


        Try
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If

            If IO.File.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") Then
                Using sr As New IO.StreamReader("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv")
                    Dim s As String = ""
                    Dim i As Integer = 1
                    While Not sr.EndOfStream
                        s += sr.ReadLine + vbNewLine

                    End While
                    readFileForm.ReadFileRichTxBx.Text = s
                End Using
                readFileForm.ShowDialog()
            Else
                TestHelper.Testing = False
                TestHelper.isFinished = False
                MessageBox.Show("Test File not Found!", "Test File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            TestHelper.Testing = False
            TestHelper.isFinished = False
            MessageBox.Show("ReadTestFile Error is " & ex.Message, "Error Loading Row", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub

    'create the test file
    Public Sub CreateTestFile()

        Try

            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(0).Controls
                If ctrl.Name.Contains("TxBx") Then
                    If String.IsNullOrEmpty(ctrl.Text) Or String.IsNullOrWhiteSpace(ctrl.Text) Then
                        MessageBox.Show("The 3 Run Oyster TextBoxes must not be empty!", "Empty TextBox Found!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                End If
            Next
            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(1).Controls
                If ctrl.Name.Contains("TxBx") And ctrl.Name.Contains("ERMetrics") Then
                    If String.IsNullOrEmpty(ctrl.Text) Or String.IsNullOrWhiteSpace(ctrl.Text) Then
                        MessageBox.Show("The 2 Run ERMetric TextBoxes must not be empty!", "Empty TextBox Found!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                End If
            Next
            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(8).Controls
                If ctrl.Name.Contains("TxBx") And ctrl.Name.Contains("Robot") Then
                    If String.IsNullOrEmpty(ctrl.Text) Or String.IsNullOrWhiteSpace(ctrl.Text) Then
                        MessageBox.Show("The 2 Run Robot TextBoxes must not be empty!", "Empty TextBox Found!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                End If
            Next

            Dim file As System.IO.StreamWriter
            'MsgBox("1: " & System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString & vbCrLf &
            '   "2: " & Environment.UserDomainName & vbCrLf &
            '   "3: " & WindowsIdentity.GetCurrent().Name & vbCrLf &
            '    "4: " & Thread.CurrentPrincipal.Identity.Name & vbCrLf &
            '   "5: " & Environment.UserName & vbCrLf &
            '   "6: " & My.User.Name & vbCrLf &
            '    "7: " & My.Computer.Name)
            ''      Dim username = System.di.DirectoryServices.AccountManagement.UserPrincipal..Current.DisplayName
            ''   Console.WriteLine(username)
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If

            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                MessageBox.Show("Oyster Helper cannot get your User Name. " & vbCrLf & "Please close Oyster Helper and then Run as Administrator!", "Empty User Name!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If Not Directory.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest") Then
                My.Computer.FileSystem.CreateDirectory("c:\Users\" & username & "\AppData\Local\OysterHelperTest")

            End If

            file = My.Computer.FileSystem.OpenTextFileWriter("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv", False)
            'create a text comma delimeted file with 

            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(0).Controls
                If ctrl.Name.Contains("TxBx") Then
                    file.WriteLine(ctrl.Name & "," & ctrl.Text)

                End If

            Next
            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(1).Controls
                If ctrl.Name.Contains("TxBx") And ctrl.Name.Contains("ERMetrics") Then
                    file.WriteLine(ctrl.Name & "," & ctrl.Text)

                End If

            Next
            For Each ctrl As Control In Oysterform.OysterTabs.GetControl(8).Controls
                If ctrl.Name.Contains("TxBx") Then  ' And ctrl.Name.Contains("Robot")
                    file.WriteLine(ctrl.Name & "," & ctrl.Text)

                End If

            Next
            file.Close()

            If Not IO.File.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") Then
                MessageBox.Show("Oyster Helper cannot get your User Name. " & vbCrLf & "Please close Oyster Helper and then Run as Administrator!", "Empty User Name!", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                MessageBox.Show("Your Testing File was created", "Test File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            TestHelper.Testing = False
            TestHelper.isFinished = False
            MessageBox.Show("Error making test file is " & ex.Message, "Test File Creation Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    'Must login to Testing Tab
    Public Sub LoginForTesting()

        Dim SignIn As Boolean = False
        Dim login As Form = New TestSignInForm
        Dim result As DialogResult = login.ShowDialog()

        If result = DialogResult.OK Then
            SignIn = TestSignInForm.SignIn
            login.Dispose()
        Else
            login.Dispose()
            Exit Sub
        End If
        '  MsgBox(SignIn)
        If SignIn Then
            Dim tab As TabPage = Nothing
            For Each t As TabPage In Oysterform.ListA
                tab = t
            Next

            If tab IsNot Nothing Then
                Oysterform.EnablePage(tab, True)
                Oysterform.OysterTabs.SelectTab("TestTab")
                My.Settings.Testing = True
                My.Settings.Save()

            End If
        Else
            MessageBox.Show("You Must be an Oyster Admin to use the Testing Function!", "Sign In Failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    'Delete saved user info for testing to begin
    Public Sub DeleteOysterDirectory()



        Dim response As Integer = MessageBox.Show("Delete Oyster Users Directory for Testing?", "Warning", MessageBoxButtons.YesNo)
        ' Dim response As DialogResult = MessageBox.Show(Constants.StopJava, "Warning", MsgBoxStyle.YesNo)

        If response = DialogResult.Yes Then
            Try

                Dim username As String = Environment.UserName
                If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                    username = SystemInformation.UserName
                End If


                If IO.Directory.Exists("C:\Users\" & username & "\AppData\Local\OysterHelper") Then
                    IO.Directory.Delete("C:\Users\" & username & "\AppData\Local\OysterHelper", True)
                    MessageBox.Show("Oyster Users Directory Deleted!", " Oyster Directory Deleted!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Users Saved Settings Directory not Found!", " Oyster Directory Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show("Error Deleteing Oyster Helper Directory is " & ex.Message, "Directory Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If

    End Sub

    Public Sub RunTests()
        Try
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If


            '---------------------------------------------------------------------------------------
            ' make sure we have a test file and the textboxes are empty before running any tests
            '---------------------------------------------------------------------------------------
            If Not IO.File.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") Then
                MessageBox.Show("You must create a Test File to Run Tests!", "Test File Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            ElseIf Not String.IsNullOrEmpty(Oysterform.OysterRootDirTxBx.Text) Or Not String.IsNullOrEmpty(Oysterform.OysterWorkDirTxBx.Text) Then

                MessageBox.Show("You must Delete Saved User Choices and Restart Oyster Helper to Run Tests!", "Testing Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else
                TestHelper.Testing = True
                TestHelper.isFinished = True
                TestHelper.j = 0
            End If

            ' startThreadTests
            Dim tHelper As New TestHelper
            'This method starts the method in a new Thread 
            'to close the msgboxes during the test
            tHelper.StartThreadTests()
            'start the tests themselves
            tHelper.StartTests()
        Catch ex As Exception
            MessageBox.Show("Error in Starting Testing is " & ex.Message, "Testing Start Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TestHelper.Testing = False
            TestHelper.isFinished = False
        End Try
    End Sub



    'end testing methods
    '-----------------------------------------


    '-------Assertion Tab--------------------------------

    Public Sub CreateTemplate(ByRef rButton As RadioButton, ByRef dta As DataTable)



        Select Case rButton.Name

            Case Oysterform.StrToStrAssertion.Name


                dta.Columns.Add("REFID")
                dta.Columns.Add("@OID")
                dta.Columns.Add(GetAssertType)
                Dim TextLine As String
                Dim SplitLine() As String
                TextLine = "1,0RGA18FJ2HE0NWZM,1"
                SplitLine = Split(TextLine, ",")
                dta.Rows.Add(SplitLine)
                TextLine = "2,0RGHPQFVKLL4KD75,1"
                SplitLine = Split(TextLine, ",")
                dta.Rows.Add(SplitLine)

                Oysterform.AssertDataGridView.DataSource = Nothing
                Oysterform.BindingSource1.DataSource = dta
                Oysterform.AssertDataGridView.DataSource = Oysterform.BindingSource1
                Oysterform.AssertDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

                Oysterform.AssertLabel.Text = "Template Loaded is for: " & GetAssertType.Substring(1)
                MessageBox.Show("This is an example file only! Change " & vbCrLf & "as needed for your Assertion Run!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)


            Case Oysterform.StrSplitAssertion.Name
                dta = New DataTable

                dta.Columns.Add("REFID")
                dta.Columns.Add("@RID")
                dta.Columns.Add("@OID")
                dta.Columns.Add(GetAssertType)

                Dim TextLine As String
                Dim SplitLine() As String
                TextLine = "1,School1.B946975,0RGA18FJ2HE0NWZM,1"
                SplitLine = Split(TextLine, ",")
                dta.Rows.Add(SplitLine)

                TextLine = "2,School1.B946974,0RGA18FJ2HE0NWZM,2"
                SplitLine = Split(TextLine, ",")
                ' Console.WriteLine(SplitLine.Count)
                dta.Rows.Add(SplitLine)

                Oysterform.AssertDataGridView.DataSource = Nothing
                Oysterform.BindingSource1.DataSource = dta
                Oysterform.AssertDataGridView.DataSource = Oysterform.BindingSource1
                Oysterform.AssertLabel.Text = "Template Loaded is for: " & GetAssertType.Substring(1)

                Oysterform.AssertDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

                MessageBox.Show("This is an example file only! Change " & vbCrLf & "as needed for your Assertion Run!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)

                'Case ReferenceTransferAsserton.Name
                '    Console.WriteLine(rButton.Name)

                'Case TruePositiveAssertion.Name
                '    Console.WriteLine(rButton.Name)


                'Case TrueNegativelAssertion.Name
                '    Console.WriteLine(rButton.Name)
            Case Oysterform.RefToRefAssertion.Name

                LoadAssertionTemplate()

            Case Oysterform.RefToStrAssertion.Name
                LoadAssertionTemplate()

            Case Else
                MessageBox.Show("Choose and Assertion Type!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)


                Exit Sub
        End Select



    End Sub


    Public Sub LoadAssertionTemplate()

        Try
            'holders for value comparisions
            Dim str1 As String = Nothing
            Dim str2 As String = Nothing
            Dim str3 As String = Nothing
            Dim str4 As String = Nothing
            Dim objReader As StreamReader = Nothing

            Dim dta As New DataTable
            Dim fName As String = ""
            Oysterform.OpenFileDialog2.InitialDirectory = "C:\Oyster\ListA\Input"
            Oysterform.OpenFileDialog2.Filter = "CSV files(*.csv)|*.csv|TXT|*.txt" ' "BMP|*.bmp|GIF|*.gif|
            Oysterform.OpenFileDialog1.RestoreDirectory = True
            If (Oysterform.OpenFileDialog2.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                fName = Oysterform.OpenFileDialog2.FileName
            End If

            Dim delimiter As String = Nothing
            Dim TextLine As String = ""
            Dim SplitLine() As String = Nothing
            Oysterform.AssertDataGridView.DataSource = Nothing
            Oysterform.AssertDataGridView.Columns.Clear()
            Oysterform.AssertDataGridView.Rows.Clear()

            dta.Columns.Clear()
            dta.Rows.Clear()


            If System.IO.File.Exists(fName) = True Then
                objReader = New System.IO.StreamReader(fName, Encoding.ASCII)
                Dim index As Integer = 0
                Do While objReader.Peek() <> -1
                    TextLine = objReader.ReadLine()

                    If index > 0 Then



                        SplitLine = Split(TextLine, delimiter)

                        If index = 1 Then
                            dta.Rows.Add(SplitLine)
                            str1 = SplitLine(1).Trim
                            str2 = SplitLine(2).Trim
                            str3 = SplitLine(3).Trim
                            str4 = SplitLine(4).Trim

                        ElseIf (SplitLine(1).Trim.Equals(str1) And SplitLine(2).Trim.Equals(str2)) Or (SplitLine(3).Trim.Equals(str3) And SplitLine(4).Trim.Equals(str4)) _
                               Or (SplitLine(2).Trim.Equals(str2) And SplitLine(4).Trim.Equals(str4)) Then 'Or (SplitLine(1).Trim.Equals(str1) And SplitLine(3).Trim.Equals(str3)
                            dta.Rows.Add(SplitLine)

                        End If

                    ElseIf index = 0 Then

                        If TextLine.Contains(";") Then
                            delimiter = ";"

                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab

                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"

                        ElseIf TextLine.Contains(",") Then
                            delimiter = ","

                        Else
                            MessageBox.Show("Oyster Helper allows comma, tab, semicolon and pipe delimeted files only!", "File Delimeters", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        End If
                        LoadColumns(TextLine, dta)
                    Else


                    End If
                    index = index + 1
                Loop
                ' AssertDataGridView.DataSource = Nothing

                Oysterform.TrimTemplate(dta)
            Else
                MessageBox.Show("File Does Not Exist!", "Load File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
            If Not objReader Is Nothing Then
                objReader.Close()
                objReader = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show("Error loading File is " & ex.Message, "File Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub SaveDataGridAssertionFile(ByVal fName As String)
        Dim I As Integer = 0
        Dim j As Integer = 0
        Dim cellvalue As String
        Dim rowLine As String = ""

        Try
            If IO.File.Exists(fName) Then
                System.IO.File.Delete(fName)
            End If

            Dim objWriter As New System.IO.StreamWriter(fName, False)
            Dim colString As String = Nothing
            Dim k As Integer = (Oysterform.AssertDataGridView.Columns.Count - 1)
            Dim l As Integer = 0
            For l = 0 To (Oysterform.AssertDataGridView.Columns.Count - 1)
                If k = l Then
                    colString += Oysterform.AssertDataGridView.Columns.Item(l).Name
                Else
                    colString += Oysterform.AssertDataGridView.Columns.Item(l).Name & ","
                End If
            Next

            objWriter.WriteLine(colString)

            For j = 0 To (Oysterform.AssertDataGridView.Rows.Count - 2)

                For I = 0 To (Oysterform.AssertDataGridView.Columns.Count - 1)
                    If Not TypeOf Oysterform.AssertDataGridView.CurrentRow.Cells.Item(I).Value Is DBNull Then
                        cellvalue = Oysterform.AssertDataGridView.Item(I, j).Value
                    Else
                        cellvalue = ""
                    End If
                    If I = (Oysterform.AssertDataGridView.Columns.Count - 1) Then
                        rowLine = rowLine + cellvalue
                    Else
                        rowLine = rowLine + cellvalue + ","
                    End If
                Next

                objWriter.WriteLine(rowLine)
                rowLine = ""

            Next

            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If

            MessageBox.Show("Assertion File Written!", "File Wring Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show("Error writing to file is " & ex.Message, "File Write Error", MessageBoxButtons.OK, MessageBoxIcon.Information)


        Finally

            FileClose(1)

        End Try


    End Sub

    Private Sub LoadColumns(ByVal headerRow As String, ByRef dta As DataTable)

        Dim newline() As String = Nothing
        Dim Row1 As String() = Nothing

        If headerRow.Contains(",") Then
            newline = Split(headerRow, ",")
            Row1 = Split(headerRow, ",")
        ElseIf headerRow.Contains(vbTab) Then
            newline = Split(headerRow, vbTab)
            Row1 = Split(headerRow, vbTab)
        ElseIf headerRow.Contains("|") Then
            newline = Split(headerRow, "|")
            Row1 = Split(headerRow, "|")
        Else
            MessageBox.Show("Oyster Helper allows comma, tab and pipe delimeted files!", "File Delimeters", MessageBoxButtons.OK, MessageBoxIcon.Information)


        End If

        If Oysterform.AssertDataGridView.Columns.Count = 0 AndAlso Row1.Count > 0 Then
            Dim i As Integer

            For i = 0 To Row1.Count - 1
                dta.Columns.Add(newline(i))

            Next

        End If

    End Sub

    Public Function GetAssertType() As String
        Dim rButton As RadioButton = Oysterform.GroupBox1.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked = True)
        Dim AssertName As String = Nothing

        If rButton Is Nothing Then
            MessageBox.Show("You must choose an Asserton Type!", "Assertion Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return AssertName
        End If

        Select Case rButton.Name

            Case Oysterform.StrToStrAssertion.Name
                AssertName = “@AssertStrToStr”

            Case Oysterform.StrSplitAssertion.Name
                AssertName = “@AssertSplitStr”

            Case Oysterform.RefToRefAssertion.Name
                AssertName = "@AssertRefToRef"

            Case Oysterform.RefToStrAssertion.Name
                AssertName = “@AssertRefToStr”

                'Case ReferenceTransferAsserton.Name
                '    Console.WriteLine(rButton.Name)

                'Case TruePositiveAssertion.Name
                '    Console.WriteLine(rButton.Name)


                'Case TrueNegativelAssertion.Name
                '    Console.WriteLine(rButton.Name)
            Case Else
                MessageBox.Show("Assertion Type Not Found", "Assertion Type Incorrect", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Return AssertName
        End Select

        Return AssertName

    End Function

    Public Sub TemplateTrim(ByRef dta As DataTable)

        Dim assertType As String = Nothing
        assertType = GetAssertType()


        If Not String.IsNullOrEmpty(assertType) Then
            dta.Columns.Add(assertType)
        Else
            Exit Sub
        End If


        'If dta IsNot Nothing Then
        '    For i As Integer = dta.Rows.Count - 3 To 0 Step -1
        '        dta.Rows.Remove(dta.Rows(i))
        '    Next
        'Else
        '    MessageBox.Show("Data Table is empty!")
        'End If

        If assertType.Equals("@AssertRefToRef") Then
            If dta IsNot Nothing Then
                'For i As Integer = dta.Rows.Count - 3 To 0 Step -1
                '    dta.Rows.Remove(dta.Rows(i))
                'Next
            Else
                MessageBox.Show("Data Table is empty!")
            End If

            For i = 0 To dta.Rows.Count - 1
                Dim datarow As DataRow = dta.Rows(i)
                If i = 0 Then
                    datarow.Item("@AssertRefToRef") = 1
                Else
                    datarow.Item("@AssertRefToRef") = 2
                End If
            Next
        Else
            If dta IsNot Nothing Then
                For i As Integer = dta.Rows.Count - 2 To 0 Step -1
                    dta.Rows.Remove(dta.Rows(i))
                Next
            Else
                MessageBox.Show("Data Table is empty!")
            End If
            For i = 0 To dta.Rows.Count - 1
                Dim datarow As DataRow = dta.Rows(i)
                If i = 0 Then
                    datarow.Item("@AssertRefToStr") = "FWEK1MWFCQN184ZR"
                Else
                    datarow.Item("@AssertRefToStr") = "U54RJQVGLVRHWJ2X"
                End If
            Next
        End If

        Oysterform.BindingSource1.DataSource = dta
        Oysterform.AssertDataGridView.DataSource = Oysterform.BindingSource1
        Oysterform.AssertLabel.Text = "Template Loaded is for: " & GetAssertType.Substring(1)
        Oysterform.AssertDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

    End Sub

    '--------End Assertion Tab Work--------------------------
    '--------------------------------------------------------

    '----Begin Analyze Methods from AnalyzeForm---------------------

    Public Sub LoadDataGridView(ByVal FileNamePath As String)
        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim dt As New DataTable
        Dim Bindingsource1 As New BindingSource
        Dim index As Integer = 0
        Dim delimiter As String = Nothing
        Dim countsClass As New CountsClass
        Dim CountBadRows As Integer = 0
        Try

            If System.IO.File.Exists(FileNamePath) = True Then

                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()
                    If Not String.IsNullOrEmpty(TextLine) Then

                        If index > 0 Then

                            Try
                                SplitLine = Split(TextLine, delimiter)
                                If Not SplitLine.Count > HeaderRowCount Then
                                    dt.Rows.Add(SplitLine)
                                Else
                                    CountBadRows += 1

                                End If

                            Catch ex As Exception
                                Console.WriteLine(ex.Message)
                                CountBadRows += 1
                            End Try

                        Else

                            If TextLine.Contains(",") Then
                                delimiter = ","
                            ElseIf TextLine.Contains(vbTab) Then
                                delimiter = vbTab
                            ElseIf TextLine.Contains("|") Then
                                delimiter = "|"
                            ElseIf TextLine.Contains(";") Then
                                delimiter = ";"
                            End If

                            LoadColumns(TextLine, dt, False)

                            '               Console.WriteLine("Column count " & dt.Columns.Count)
                            index = 1
                            HeaderRow = Split(TextLine, delimiter)
                            HeaderRowCount = HeaderRow.Length


                            'For i = 0 To HeaderRowCount - 1
                            '    countsClass.SetPropertyValue(HeaderRow(i), 0)
                            'Next
                        End If
                    End If
                Loop

                AnalyzeForm.FileDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                AnalyzeForm.FileDataGridView.DataSource = Nothing
                Bindingsource1.DataSource = dt
                AnalyzeForm.FileDataGridView.DataSource = Bindingsource1



                If objReader IsNot Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If

                If CountBadRows > 0 Then
                    MessageBox.Show(CountBadRows.ToString & " Bad Rows were skipped Loading", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Else
                MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("LoadDataGridView Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally

        End Try
    End Sub

    Public Sub LoadColumns(ByVal headerRow As String, ByRef dt As DataTable, ByVal analyzeData As Boolean)
        Dim delimiter As String = Nothing
        Dim count As Integer = 0

        Try
            If headerRow.Contains(",") Then
                delimiter = ","
            ElseIf headerRow.Contains(vbTab) Then
                delimiter = vbTab
            ElseIf headerRow.Contains("|") Then
                delimiter = "|"
            ElseIf headerRow.Contains(";") Then
                delimiter = ";"
            End If
            Dim newline() As String = Split(headerRow, delimiter)

            Dim Row1 As String() = Split(headerRow, delimiter)


            If analyzeData Then
                AnalyzeForm.AnalyzeDataGridView.Columns.Clear()

                If AnalyzeForm.AnalyzeDataGridView.Columns.Count = 0 Then

                    For i = 0 To Row1.Count - 1
                        If analyzeData And count = 0 Then

                            Dim chk As New DataGridViewCheckBoxCell

                            dt.Columns.Add(newline(i).ToUpper)
                            count = 1
                        Else
                            dt.Columns.Add(newline(i).ToUpper)
                        End If
                    Next

                End If
            ElseIf Not analyzeData Then

                AnalyzeForm.FileDataGridView.Columns.Clear()

                For i = 0 To Row1.Count - 1
                    If Not analyzeData And count = 0 Then

                        Dim chk As New DataGridViewCheckBoxCell

                        dt.Columns.Add(newline(i).ToUpper)
                        count = 1
                    Else
                        dt.Columns.Add(newline(i).ToUpper)
                    End If
                Next

            End If

        Catch ex As Exception

            MessageBox.Show("LoadColumns error is " & ex.Message, "Analyzedata Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    'Public Sub DataLoad(ByVal fName As String)


    '    Dim dt As New DataTable
    '    Dim bindingSource1 As New BindingSource()
    '    Dim countsClass As New CountsClass
    '    Dim dta As New DataTable

    '    'Chart1.Series(0).Points.Clear()
    '    'ErrorsRichTextBox.Clear()

    '    'AnalyzeDataGridView.DataSource = Nothing

    '    AnalyzeForm.LoadedFileLbl.Text = "Loaded File is: " & fName
    '    AnalyzeForm.LFileLbl.Text = "Loaded File is: " & fName
    '    AnalyzeForm.loadedFile2lbl.Text = "Loaded File is: " & fName
    '    AnalyzeForm.ErrorsLbl.Text = "Loaded File is: " & fName
    '    'clear everything on load of new file
    '    AnalyzeForm.ECountLbl.Text = "Possible Errors Line Count is: "
    '    AnalyzeForm.FindsLBL.Text = "Hits: "
    '    ' AnalyzeForm.ErrorsRichTextBox.Clear()




    '    '   LoadDataGridView(fName)
    '    dta = countsClass.NewAnalyzeData(dta, fName)
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.IsLabelAutoFit = False
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.LabelStyle.Angle = 45
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep45
    '    'Chart2.Series(0).SetCustomProperty("PointWidth", "1")
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.LabelStyle.Enabled = True
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.MinorTickMark.Enabled = True
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisX.Interval = 1
    '    AnalyzeForm.Chart2.AlignDataPointsByAxisLabel()
    '    AnalyzeForm.Chart2.ChartAreas(0).AxisY.Title = "Non-Null Counts"
    '    AnalyzeForm.Chart2.Legends(0).Enabled = False
    '    AnalyzeForm.Chart2.Series(0).Points.DataBindXY(dta.Rows, "Column", dta.Rows, "Value")


    '    dt = countsClass.AnalyzeData(dt, fName)
    '    Oysterform.ToolStripProgressBar1.PerformStep()
    '    dt = countsClass.CalculatePercentage(dt)
    '    Oysterform.ToolStripProgressBar1.PerformStep()
    '    dt = countsClass.AnalyzeDistinctData(dt, fName)



    '    AnalyzeForm.AnalyzeDataGridView.DataSource = Nothing
    '    bindingSource1.DataSource = dt
    '    AnalyzeForm.AnalyzeDataGridView.DataSource = bindingSource1

    '    AnalyzeForm.AnalyzeDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
    '    AnalyzeForm.AnalyzeDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
    '    For Each column As DataGridViewColumn In AnalyzeForm.AnalyzeDataGridView.Columns
    '        column.SortMode = DataGridViewColumnSortMode.NotSortable
    '    Next

    '   End Sub
End Class
