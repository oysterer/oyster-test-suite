﻿Imports System.IO
Imports System.Security.Permissions


''' <summary>
''' Main Form Class for Oyster Test Suite
''' Most Methods that do work have been moved
''' to the OTSHelper class per Microsoft Good Prgramming Habits
''' </summary>
''' 
Public Class OysterTestSuiteForm

    'used to resize the program if chosen
    Private RS As New FormResizer
    'for the output and error from the Python Runs
    Private Delegate Sub ConsoleOutputPythonDelegate(ByVal outputString As String)
    Private Delegate Sub ConsoleErrorPythonDelegate(ByVal errorString As String)
    'ermetrics
    Private Delegate Sub ConsoleOutputDelegate(ByVal outputString As String)
    Private Delegate Sub ConsoleErrorDelegate(ByVal errorString As String)
    'used to get save the script name for use
    Private ScriptName As String
    'HashTable used to hold helperbeans with RichTextBox loaded file save Information
    Public saveInfoCollection As New Hashtable()
    'Used to write to the TextSuite.log file
    Public LogWriter As New OTSLogWriter
    'This used to access all the methods moved out of the Base Form
    Private OTSHelper1 As New OTSHelper
    'stores testcase with no identity change report
    '  Private NoChangeReportFile As New Hashtable

    'used to save helperbeans for easy access
    Protected Friend list As New List(Of HelperBean)()

    'what drive are we on used after parsing xml file
    Protected Friend Drive As String = "C"



    Protected Overrides Sub OnLoad(e As EventArgs)

        'permissions
        Dim f As New FileIOPermission(PermissionState.None)
        f.AllLocalFiles = FileIOPermissionAccess.Append
        Try
            f.Demand()
        Catch s As Exception
            MessageBox.Show(s.Message, "Permissions Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        'for resizing
        RS.FindAllControls(Me)

        'want to save any user input when exiting
        My.Application.SaveMySettingsOnExit = True
        My.Settings.Reload()
        Try
            Reader()

            If Not String.IsNullOrEmpty(My.Settings.TestSuiteRootDir) And Not String.IsNullOrEmpty(My.Settings.OysterRootTestSuiteDir) And Not String.IsNullOrEmpty(My.Settings.OysterJarToRun) Then
                'OysterRootTestSuiteDirLbl.Text = My.Settings.OysterRootTestSuiteDir
                TestSuiteRootDirLbl.Text = My.Settings.TestSuiteRootDir
                Drive = My.Settings.TestSuiteRootDir.Substring(0, 1)

                LoadSuiteXMLFile()
                LoadTestSuiteTreeView()
                LoadTreeview()
                CheckLogFileSizes()


            Else
                'first time up you need to tell it where OysterTestSuite Root Directory is located
                Dim diag As New LoadSuiteXMLFileDialog
                diag.ShowDialog()
                If Not String.IsNullOrEmpty(TestSuiteRootDirLbl.Text) Then
                    Drive = My.Settings.TestSuiteRootDir.Substring(0, 1)
                    LoadSuiteXMLFile()
                    LoadTestSuiteTreeView()
                    LoadTreeview()
                    CheckLogFileSizes()
                Else
                    MessageBox.Show("You Must Fill the Suite Root Directory Text Box!", "Load Text Box", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If

            'load the TestSuite.xml file on startup
            LoadTestSuiteXMLFile()

        Catch ex As Exception
            MessageBox.Show("Error Loading OysterTestSuite is " & ex.Message, "Loading OysterTestSuite", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub


    Private Sub LoadTestSuiteXMLFile()
        Try

            'load the TestSuite.xml file on startup
            For Each n As TreeNode In Me.TreeView3.Nodes(0).Nodes
                If n.Text = "TestSuite.xml" Then
                    TreeView3.SelectedNode = n
                    LoadFileFromTreeView(TreeView3, RichTextBox9)
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub


    ''' <summary>
    ''' Loads the TestSuite.xml file to the program
    ''' </summary>
    Private Sub LoadSuiteXMLFile()
        OTSHelper1.LoadSuiteXMLFile()
    End Sub

    ''' <summary>
    ''' Generic method to bb any file to a RichTextBox
    ''' </summary>
    ''' <param name="TreeView"></param>
    ''' <param name="RichTextBox"></param>
    Public Sub LoadFileFromTreeView(ByRef TreeView As TreeView, ByRef RichTextBox As RichTextBox)
        OTSHelper1.LoadFileFromTreeView(TreeView, RichTextBox)
    End Sub


    ''' <summary>
    ''' Loads TreeView3 with the user chosen file for editing or viewing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Sub TreeView3_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView3.NodeMouseDoubleClick

        If TreeView3.SelectedNode IsNot Nothing Then
            LoadFileFromTreeView(TreeView3, RichTextBox9)
            'RichTextBox1.SelectionStart = Len(RichTextBox9.Text)
            '' Scrolls to the caret
            'RichTextBox9.ScrollToCaret()
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    ''' <summary>
    ''' Closes the program
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ExitBtn_Click(sender As Object, e As EventArgs) Handles ExitBtn.Click
        Me.Close()
    End Sub


    ''' <summary>
    ''' This resizes the form and changes all the sizes
    ''' of controls on the form.  FormResiser is the class
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub OysterTestSuite_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        RS.ResizeAllControls(Me, Me.Width, Me.Height)
    End Sub


    ''' <summary>
    ''' Loads the TreeView on the SetupTAb
    ''' </summary>
    Private Sub LoadTestSuiteTreeView()
        OTSHelper1.LoadTestSuiteTreeView()
    End Sub


    ''' <summary>
    ''' Exit the program when clicking Exit on the Toolstrip
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub


    ''' <summary>
    ''' Run Button to start Python with Oyster Runs
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub RunBtn_Click(sender As Object, e As EventArgs) Handles RunBtn.Click
        RunCheckRTB.Clear()
        RunCheckRTB.Text += Constants.OysterTestSuiteRun & Now
        RunCheckRTB.Text += vbCrLf
        RunCheckRTB.Text += My.Settings.OysterJarToRun
        RunCheckRTB.Text += vbCrLf
        RunCheckRTB.Text += vbCrLf
        LogWriter.WriteToLog(Constants.Divider)
        LogWriter.WriteToLog(Constants.OysterTestSuiteRun & Now, My.Settings.OysterJarToRun + Environment.NewLine)
        LogWriter.WriteToLog(Constants.Divider)
        LogWriter.WriteToLog(Environment.NewLine)


        RunFiles()

    End Sub



    ''' <summary>
    ''' This runs the Python Oyster process
    ''' </summary>
    Private Sub RunFiles()
        OTSHelper.errorsTestBean = New ErrorsTestBean
        OTSHelper1.RunFiles()
    End Sub


    Public Function CompareResults(ByRef Collectn As Collection) As Collection
        Dim Collect As New Collection
        Collect = OTSHelper1.CompareResults(Collectn)

        Return Collect
        '   Return helprBean
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Collect"></param>
    Private Sub CreateOutputCheck(ByVal Collect As Collection)

        OTSHelper1.CreateOutputCheck(Collect, Me.RunCheckRTB, Me.LogWriter)

    End Sub



    ''' <summary>
    ''' Calls the method to check Change Reports for a specific Test
    ''' </summary>
    ''' <param name="helprBean"></param>
    Public Sub CheckTheIndentityReports(ByRef helprBean As HelperBean)

        OTSHelper1.CheckTheIndentityChangeReports(helprBean)


    End Sub

    ''' <summary>
    ''' 'Parse the individual ZML files for each Oyster Directory we are 
    ''' running and store in a HelperBean and return to the caller. All the 
    ''' expected outputs are retrieved here.
    ''' </summary>
    ''' <param name="TestToRun"></param>
    ''' <returns></returns>
    Private Function GetRunParameters(ByVal TestToRun As String) As HelperBean
        Dim helpBean As New HelperBean
        helpBean = OTSHelper1.GetRunParameters(TestToRun, helpBean)
        Return helpBean
    End Function

    '-----------------------------------------------------------
    ' All the consoleoutput from the python process

    ''' <summary>
    ''' This is very important function.  It writes the outputstring from the Python process to the RunRichTextBox.  It also waits 
    ''' for a specific last line from the Python process to pass through and then calls the BackgroundWorker to check all the outputs and 
    ''' ''' </summary>
    ''' <param name="outputString"></param>
    Private Sub ConsoleOutputPython(ByVal outputString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleOutputPythonDelegate(AddressOf ConsoleOutputPython)
            Dim args As Object() = {outputString}
            Me.Invoke(del, args)
        Else


            RunRichTextBox.ScrollToCaret()
            RunRichTextBox.AppendText(String.Concat(outputString, Environment.NewLine))

            ToolStripProgressBar1.PerformStep()

            If ToolStripProgressBar1.Value = 100 Then
                ToolStripProgressBar1.Value = 0
                ToolStripProgressBar1.PerformStep()
            End If
            If outputString.Contains("Opening") Then
                ''Get the running Script Name
                ' errorsBean = New ErrorsTestBean
                Me.ScriptName = outputString.Substring(outputString.IndexOf(" ") + 1)
                '  
            End If
            If outputString.ToUpper.Contains("ERROR") Then
                '   StartThreadTests()
                RunCheckRTB.SelectionStart = RunCheckRTB.Text.Length
                RunCheckRTB.ScrollToCaret()
                RunCheckRTB.Text += vbCrLf
                Dim sName = Path.GetFileName(Me.ScriptName).Substring(0, Path.GetFileName(Me.ScriptName).IndexOf("RunScript"))
                RunCheckRTB.Text += sName
                RunCheckRTB.Text += vbCrLf
                '  RunCheckRTB.Text += vbCrLf
                RunCheckRTB.Text += outputString
                RunCheckRTB.Text += vbCrLf
                Dim errorsBean As New ErrorsTestBean

                errorsBean.TestBeanNme = sName
                errorsBean.Lst.Add(outputString)

                If OTSHelper.errorsTestBean.ErrorOutputBeanLst.Count = 0 Then
                    OTSHelper.errorsTestBean.ErrorOutputBeanLst.Add(errorsBean)
                Else
                    For Each tBean As ErrorsTestBean In OTSHelper.errorsTestBean.ErrorOutputBeanLst
                        If tBean.TestBeanNme.Equals(errorsBean.TestBeanNme) Then
                            tBean.Lst.Add(outputString)
                        End If
                    Next

                End If


                LogWriter.WriteToLog("Error in  " & sName, "Error is  " & outputString)
                LogWriter.WriteToLog(Environment.NewLine)



            End If
            If outputString.ToUpper.Contains("Ran In".ToUpper) Then
                OTSHelper.errorsTestBean.PythonRunTime1 = outputString.Substring(7, 7) & " secs"
            End If

            If outputString.ToUpper.Contains("End of Python Run".ToUpper) Then

                Dim helperBean As New HelperBean
                helperBean.OysterJarToRun = My.Settings.OysterJarToRun

                BackgroundWorker1.RunWorkerAsync(helperBean)

                Me.ToolStripProgressBar1.Visible = False
                ToolStripStatusLabel1.Text = "Oyster Test Suite"

            End If
        End If
    End Sub






    ''' <summary>
    ''' This processes the error if ConsoleErrorPython if InvokeRequired
    ''' </summary>
    ''' <param name="errorString"></param>
    Private Sub PythonConsoleError(ByVal errorString As String)
        RunCheckRTB.AppendText(vbCrLf)
        RunCheckRTB.AppendText(String.Concat(Me.ScriptName, Environment.NewLine))
        RunCheckRTB.AppendText(vbCrLf)

        RunCheckRTB.AppendText(String.Concat("Python Error Output: ", errorString, Environment.NewLine))
        ToolStripProgressBar1.Visible = False
        ToolStripStatusLabel1.Text = "Oyster Test Suite"
        LogWriter.WriteToLog("Error in  " & Me.ScriptName, "Error is  " & errorString)
        LogWriter.WriteToLog(Environment.NewLine)



        '  StopRun.Enabled = False
    End Sub

    ''' <summary>
    ''' Processes Errors for the Pyhon Run Process
    ''' </summary>
    ''' <param name="errorString"></param>
    Private Sub ConsoleErrorPython(ByVal errorString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleErrorPythonDelegate(AddressOf PythonConsoleError)
            Dim args As Object() = {errorString}
            Me.Invoke(del, args)
        Else

            RunCheckRTB.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
            ToolStripProgressBar1.Visible = False
            ToolStripStatusLabel1.Text = "Oyster Test Suite"
            'StopRun.Enabled = False
            LogWriter.WriteToLog(Environment.NewLine)
            LogWriter.WriteToLog("Error in  " & Me.ScriptName, Environment.NewLine)
            LogWriter.WriteToLog(Environment.NewLine)
            LogWriter.WriteToLog("Error is  " & errorString, Environment.NewLine)
            LogWriter.WriteToLog(Environment.NewLine)

        End If

    End Sub

    Sub ConsoleErrorPythonHandler(ByVal sendingProcess As Object, ByVal errLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(errLine.Data) Then
            ConsoleErrorPython(errLine.Data)
        End If
    End Sub

    Sub ConsoleOutputPythonHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleOutputPython(outLine.Data)
        End If
    End Sub


    ' END All the consoleoutput from the python process

    '-----------------------------------------------------------

    ''' <summary>
    ''' Backgroundworker Thread called after the Python Process is complete to 
    ''' call all the methods to create the comparison output
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ' Do some time-consuming work on this thread.
        Dim collect As New Collection

        Dim hepBean As HelperBean = Nothing
        Try
            Dim dic As Dictionary(Of String, HelperBean)
            Dim CollectionOfHelperBeans As New Collection
            Dim helpBean As New HelperBean
            helpBean = saveInfoCollection.Item(99)
            dic = helpBean.AllTestCasesDictionary
            For Each key As String In dic.Keys
                Dim helperBean As New HelperBean
                helperBean = dic.Item(key)
                CollectionOfHelperBeans.Add(helperBean)
            Next

            collect = CompareResults(CollectionOfHelperBeans)

        Catch ex As Exception

            Console.WriteLine("CheckRunOutputs Error is " & ex.Message)
        Finally


        End Try

        e.Result = collect
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As System.Object,
                                                     ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
                                                     Handles BackgroundWorker1.RunWorkerCompleted
        ' Called when the BackgroundWorker is completed.
        Dim collect As New Collection

        collect = CType(e.Result, Collection)
        CreateOutputCheck(collect)
        SaveDataGridView()


    End Sub

    ''' <summary>
    ''' Makesthe file loaded to RichTextBox9 on the First Tab Editable
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditFileBtn_Click(sender As Object, e As EventArgs) Handles EditFileBtn.Click
        RichTextBox9.ReadOnly = False
    End Sub

    ''' <summary>
    ''' Saves the file loaded to RichTextBox9 on the First Tab - SetupTab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SaveFileBtn_Click(sender As Object, e As EventArgs) Handles SaveFileBtn.Click

        If Not String.IsNullOrEmpty(RichTextBox9.Text) Then
            SaveRichTextBoxFile(Constants.RichTextBox9, RichTextBox9, saveInfoCollection.Item(Constants.RichTextBox9).FileLoaded)
            LoadSuiteXMLFile()

        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub


    ''' <summary>
    ''' Generic method to save any loaded file in any RichTextBox
    ''' </summary>
    ''' <param name="RichTextBoxKey"></param>
    ''' <param name="myRichTextBox"></param>
    ''' <param name="FileToSave"></param>
    Public Sub SaveRichTextBoxFile(ByVal RichTextBoxKey As Integer, ByVal myRichTextBox As RichTextBox, ByVal FileToSave As String)
        Try
            Dim helperBean As New HelperBean
            helperBean = saveInfoCollection.Item(RichTextBoxKey)
            myRichTextBox.SaveFile(FileToSave, RichTextBoxStreamType.PlainText)
            myRichTextBox.ReadOnly = True

            MessageBox.Show("File " & FileToSave & " Saved!", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show("Error in Saving the File is " & ex.Message, "File Error On Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


    ''' <summary>
    ''' Button used to refresh if needed the Setup Tab Pane to the left
    ''' of the screen with the XML Run Information from the TestSuite.xml File
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub RefreshBtn_Click(sender As Object, e As EventArgs) Handles RefreshBtn.Click
        LoadSuiteXMLFile()
        LoadTestSuiteTreeView()
    End Sub

    ''' <summary>
    ''' Refreshs OutputFiles TreeView
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub RefreshOutput_Btn_Click(sender As Object, e As EventArgs) Handles RefreshOutput_Btn.Click
        LoadTreeview()
    End Sub


    '------------------------------------------------------------------------------
    'these are the right click commands in the context menu 1 start

    Private Sub CommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem.Click
        Try
            '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            '  If richTextBox.CanPaste(MyFormat) Then
            richTextBox.SelectedText = "<!-- " & richTextBox.SelectedText.Trim & " --> "
            '  End If
        Catch ex As Exception
            '  Me.LogWriter.WriteToLog("Contextmenu error is " & ex.Message, Environment.NewLine)
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub UnCommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnCommentSelectedTextToolStripMenuItem.Click

        Try
            Dim array1() As Char = {"<", "!", "-"}
            Dim array2() As Char = {"-", ">"}
            '      Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            Dim str As String = st.TrimEnd(array2)
            richTextBox.SelectedText = str.Trim & vbCr
            'uncomment to save 
            '  Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            '  Me.LogWriter.WriteToLog("Contextmenu error is " & ex.Message, Environment.NewLine)
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub CopySelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopySelectionToolStripMenuItem.Click

        'Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

        'If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
        Try
            Dim richTextBox As RichTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)

            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If

        Catch ex As Exception
            '  Me.LogWriter.WriteToLog("Contextmenu error is " & ex.Message, Environment.NewLine)
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

        '   End If

    End Sub

    Private Sub CutSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CutSelectionToolStripMenuItem.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Cut()
            End If
        Catch ex As Exception
            '  Me.LogWriter.WriteToLog("Contextmenu error is " & ex.Message, Environment.NewLine)
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub PasteSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PasteSelectionToolStripMenuItem.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            '  Me.LogWriter.WriteToLog("Contextmenu error is " & ex.Message, Environment.NewLine)
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    ' ---------context menu 1 exit-------------------

    '------------context menu 2 Start---------------------


    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            If treeView.SelectedNode IsNot Nothing Then
                If Not String.IsNullOrEmpty(treeView.SelectedNode.Tag) Then
                    Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                    'MsgBox(selectedFileAndPath)

                    My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                    selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                    LoadTestSuiteTreeView()
                    LoadTreeview()

                Else
                    MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)

                End If


            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            ' treeView.Nodes(0).Nodes(j).Expand()
            treeView.Refresh()
            treeView.Nodes(0).ExpandAll()
        Catch ex As Exception
            '   Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub DeleteSelectedFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectedFileToolStripMenuItem.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            'Dim tab = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                '  oysterHelper.RunProcessTab(Constants.Welcome)
                LoadTestSuiteTreeView()
                LoadTreeview()
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            '  treeView.Nodes(0).Nodes(j).Expand()
            treeView.Refresh()
            treeView.Nodes(0).ExpandAll()
        Catch ex As Exception
            ' Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ShowFilePropertiesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowFilePropertiesToolStripMenuItem.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub
    Private Sub RenameFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenameFileToolStripMenuItem.Click

        Try
            Dim renameFileName As String = Nothing
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim textToShow As String = treeView.SelectedNode.Text
                Dim j As Integer = treeView.SelectedNode.Parent.Index
                Dim i As Integer = treeView.SelectedNode.Index


                Dim rDialog As New RenameDiaglog
                rDialog.Label3.Text = textToShow
                rDialog.ReNameFileTxBx.Text = textToShow
                If rDialog.ShowDialog = DialogResult.OK Then

                    Dim text As String = rDialog.ReNameFileTxBx.Text

                    My.Computer.FileSystem.RenameFile(selectedFileAndPath, text)
                    LoadTestSuiteTreeView()
                    'oysterHelper.RunProcessTab(Constants.Welcome)
                End If

                Me.TreeView3.Nodes(0).Nodes(j).Expand()
            Else

            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    '-----------End Context Menu right click methods---------------------------------



    ''' <summary>
    ''' Take the user to the First Tab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub HistoryBtn_Click(sender As Object, e As EventArgs) Handles HomeBtn.Click
        OysterTestSuiteTabs.SelectTab(3)
    End Sub


    ''' <summary>
    ''' Loads the TreeView on the Test Output Files Tab
    ''' </summary>
    Private Sub LoadTreeview()
        OTSHelper1.LoadTreeview(Me.LogFileTreeView)
    End Sub


    Private Sub GoHomeBtn_Click(sender As Object, e As EventArgs) Handles GoHomeBtn.Click
        OysterTestSuiteTabs.SelectTab(0)
    End Sub


    ''' <summary>
    ''' Load the doubleclicked file to the richtextbox in the Test Output Files Tab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Sub LogFileTreeViewDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles LogFileTreeView.NodeMouseDoubleClick

        If LogFileTreeView.SelectedNode IsNot Nothing Then
            LoadFileFromTreeView(LogFileTreeView, RichTextBox1)

            RichTextBox1.SelectionStart = Len(RichTextBox1.Text)
            ' Scrolls to the caret
            RichTextBox1.ScrollToCaret()


        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    ''' <summary>
    ''' Makes the richtextbox loaded file in the Test Output Files Tab Editable
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Edit_OutputFileBtn_Click(sender As Object, e As EventArgs) Handles Edit_OutputFileBtn.Click
        RichTextBox1.ReadOnly = False
    End Sub



    ''' <summary>
    ''' Save the richtextbox loaded file in the Test Output files Tab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SaveFile_btn_Click(sender As Object, e As EventArgs) Handles SaveFile_btn.Click
        If Not String.IsNullOrEmpty(RichTextBox1.Text) Then
            SaveRichTextBoxFile(Constants.RichTextBox1, RichTextBox1, saveInfoCollection.Item(Constants.RichTextBox1).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub



    ''' <summary>
    ''' This checks log files for the entire process for size and 
    ''' if they meet exceed the threeshold, it will rename the files 
    ''' and a new one will be created on the next run
    ''' </summary>
    Private Sub CheckLogFileSizes()
        OTSHelper.CheckLogFileSizes()
    End Sub

    ''' <summary>
    ''' Save the datagridview info for reloading on startup
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub FrmProgramma_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '  SaveDataGridView()

    End Sub

    Public Sub SaveDataGridView()
        Dim RunLogsDirectory As String = My.Settings.TestSuiteLogFile.Substring(0, My.Settings.TestSuiteLogFile.LastIndexOf("/"))

        Dim filename As String = RunLogsDirectory & "/JTable1Data.data"
        'Create locations for log data files to make sure we hace a copy
        'go back 5 files
        Dim dest As String = RunLogsDirectory + "/JTable1Data.data"
        Dim dest1 As String = RunLogsDirectory + "/DataFiles/JTable1Data.data.1"
        Dim dest2 As String = RunLogsDirectory + "/DataFiles/JTable1Data.data.2"
        Dim dest3 As String = RunLogsDirectory + "/DataFiles/JTable1Data.data.3"
        Dim dest4 As String = RunLogsDirectory + "/DataFiles/JTable1Data.data.4"

        Dim destbak As String = RunLogsDirectory + "/DataFiles/bak/JTable1Data.data"
        Dim dest1bak As String = RunLogsDirectory + "/DataFiles/bak/JTable1Data.data.1"
        Dim dest2bak As String = RunLogsDirectory + "/DataFiles/bak/JTable1Data.data.2"
        Dim dest3bak As String = RunLogsDirectory + "/DataFiles/bak/JTable1Data.data.3"
        Dim dest4bak As String = RunLogsDirectory + "/DataFiles/bak/JTable1Data.data.4"

        If Not File.Exists(dest) Then
            MsgBox("File gone")
            Exit Sub
        End If

        Try
            Using sw As New IO.StreamWriter(filename)
                Dim cHeader As String = String.Empty
                For Each c As DataGridViewColumn In DataGridView1.Columns

                    cHeader &= c.Name & ","
                Next
                sw.WriteLine(cHeader.TrimEnd(","))
                Dim rLine As String = String.Empty
                For Each r As DataGridViewRow In DataGridView1.Rows
                    If Not r.IsNewRow Then
                        rLine = String.Empty
                        For Each cl As DataGridViewCell In r.Cells
                            rLine &= cl.Value & ","
                        Next
                        sw.WriteLine(rLine.TrimEnd(","))
                    End If
                Next


            End Using

            Do Until File.Exists(RunLogsDirectory & "/JTable1Data.data")
                Threading.Thread.Sleep(600)
            Loop




            ''move them back.  Only
            If (File.Exists(destbak)) Then
                My.Computer.FileSystem.MoveFile(dest3bak, dest4, True)
                My.Computer.FileSystem.MoveFile(dest2bak, dest3, True)
                My.Computer.FileSystem.MoveFile(dest1bak, dest2, True)
                My.Computer.FileSystem.MoveFile(destbak, dest1, True)
                My.Computer.FileSystem.CopyFile(dest, destbak)

            Else
                My.Computer.FileSystem.CopyFile(dest, destbak)
                My.Computer.FileSystem.MoveFile(dest1, dest1bak, True)
                My.Computer.FileSystem.MoveFile(dest2, dest2bak, True)
                My.Computer.FileSystem.MoveFile(dest3, dest3bak, True)
                My.Computer.FileSystem.MoveFile(dest4, dest4bak, True)

            End If
            'My.Computer.FileSystem.MoveFile(dest, destbak)
            '    My.Computer.FileSystem.MoveFile(dest1, dest1bak, True)
            '    My.Computer.FileSystem.MoveFile(dest2, dest2bak, True)
            '    My.Computer.FileSystem.MoveFile(dest3, dest3bak, True)
            '    My.Computer.FileSystem.MoveFile(dest4, dest4bak, True)

            'move them back to original location
            My.Computer.FileSystem.MoveFile(dest3bak, dest4, True)
            My.Computer.FileSystem.MoveFile(dest2bak, dest3, True)
            My.Computer.FileSystem.MoveFile(dest1bak, dest2, True)
            My.Computer.FileSystem.MoveFile(destbak, dest1, True)

        Catch ex As Exception
            Console.WriteLine("SaveDataGridView error is " & ex.Message)
        End Try
    End Sub



    Private Sub Reader()
        OTSHelper1.Reader()
    End Sub

    Private Sub StopRunBtn_Click(sender As Object, e As EventArgs) Handles StopRunBtn.Click
        OTSHelper1.StopRun()

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick

        For Each eBean As ErrorsTestBean In OTSHelper.errorsTestBean.ErrorOutputBeanLst


            If eBean.TestBeanNme.ToUpper.Equals(ListBox1.GetItemText(ListBox1.SelectedItem).ToUpper) Then
                RichTextBox2.Clear()


                For Each str As String In eBean.Lst
                    '    Console.WriteLine("OK2")
                    RichTextBox2.AppendText(str)
                    RichTextBox2.AppendText(vbLf)
                Next


            End If
        Next


    End Sub

    'adds a row of run data to the data table
    Public Sub FillTableRow()

        OTSHelper1.FillTableRow()
    End Sub

    Private Sub OysterHelperToolStripMenuItem_Click(sender As Object, e As EventArgs) 

    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Dim ohelpAbout As New AboutBox1
        ohelpAbout.ShowDialog()
    End Sub

    Private Sub SingleMode_Click(sender As Object, e As EventArgs) Handles SingleMode.Click
        Dim CheckListBox As New SingleRun
        CheckListBox.ShowDialog()

    End Sub


    'Very Important ConsoleOutput Methods used to write back
    'to The RichtextBoxes during Oyster and ERmetrics Runs.
    'Console ouput writers for the ProcessClass
    '-------------------------------------------------------------
    Private Sub ConsoleOutput(ByVal outputString As String)
        Try

            If Me.InvokeRequired Then
                Dim del As New ConsoleOutputDelegate(AddressOf ConsoleOutput)
                Dim args As Object() = {outputString}
                Me.Invoke(del, args)

            Else

                RichTextBox3.ScrollToCaret()

                RichTextBox3.AppendText(String.Concat(outputString, Environment.NewLine))



            End If


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

        End Try

    End Sub

    Sub ConsoleOutputHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleOutput(outLine.Data)
        End If
    End Sub
    Sub ConsoleErrorHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleError(outLine.Data)
        End If
    End Sub

    Private err As Integer = 0

    Private Sub ConsoleError(ByVal errorString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleErrorDelegate(AddressOf ConsoleError)
            Dim args As Object() = {errorString}
            Me.Invoke(del, args)
        Else

            RichTextBox3.ScrollToCaret()
            RichTextBox3.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))


        End If

    End Sub

    Private Sub ERMetricsWkDirBtn_Click(sender As Object, e As EventArgs) Handles ERMetricsWkDirBtn.Click

        FolderBrowserDialog1.SelectedPath = My.Settings.OysterRootTestSuiteDir
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then


            OTSHelper1.FillTextBox(ERMetricsTxBx, FolderBrowserDialog1.SelectedPath)



        Else
            Exit Sub
        End If
    End Sub

    Private Sub ERMetricJarBtn_Click(sender As Object, e As EventArgs) Handles ERMetricJarBtn.Click

        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Then
            MessageBox.Show(Constants.ERWorkingDirectory, "ERMetrics Working Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            OpenFileDialog1.InitialDirectory = ERMetricsTxBx.Text
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                OTSHelper1.FillTextBox(ERMetricsJarTxBx, OpenFileDialog1.FileName)
            End If
        End If


    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Each click on a tab checks and loads each 
    'tab as needed
    '''''''''''''''''''''''''''''''''''''''''''''''''''' 
    Private Sub MyTabControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OysterTestSuiteTabs.SelectedIndexChanged

        If OysterTestSuiteTabs.SelectedTab.Name.Equals("ERMetricsTab") Then

            EROysterWkDirTxBx.Text = My.Settings.EROysterWorkDir
            ERMetricsTxBx.Text = My.Settings.ERMetricsWorkDir
            ERMetricsJarTxBx.Text = My.Settings.ERMetricsJar

            If Not String.IsNullOrEmpty(ERMetricsTxBx.Text) _
            And Not String.IsNullOrEmpty(EROysterWkDirTxBx.Text) _
            And Not String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then

                OTSHelper1.SaveERmetricsTxBoxes(False)
            End If



        End If



    End Sub

    Private Sub SaveERMetricsBtn_Click(sender As Object, e As EventArgs) Handles SaveERMetricsBtn.Click
        OTSHelper1.SaveERmetricsTxBoxes(True)
    End Sub

    Private Sub OysterWkDirBtn_Click(sender As Object, e As EventArgs) Handles OysterWkDirBtn.Click

        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Then
            MessageBox.Show(Constants.ERWorkingDirectory, "ERMetrics Working Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub

        ElseIf String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then
            MessageBox.Show(Constants.ERMetricsJarEmpty, "ERMetrics Jar Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            FolderBrowserDialog1.SelectedPath = My.Settings.OysterRootTestSuiteDir
            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                OTSHelper1.FillTextBox(EROysterWkDirTxBx, FolderBrowserDialog1.SelectedPath)
            End If

        End If

    End Sub

    Private Sub MoveFileBtn_Click(sender As Object, e As EventArgs) Handles MoveFileBtn.Click
        OTSHelper1.MoveLinkFile()
        OTSHelper1.SaveERmetricsTxBoxes(False)
    End Sub

    Sub TreeView4_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView4.NodeMouseDoubleClick

        If TreeView4.SelectedNode IsNot Nothing Then
            OTSHelper1.LoadFileFromTreeView(TreeView4, RichTextBox3)

        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub RunERMetricsBtn_Click(sender As Object, e As EventArgs) Handles RunERMetricsBtn.Click

        Try
            If String.IsNullOrEmpty(ERMetricsTxBx.Text) Or String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then
                MessageBox.Show(Constants.ERMetricsDirChoiceError, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            If Not My.Computer.FileSystem.DirectoryExists(ERMetricsTxBx.Text) Then
                ERMetricsTxBx.Text = Nothing
                MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            ElseIf String.IsNullOrEmpty(My.Settings.ERMetricsWorkDir) Or String.IsNullOrEmpty(My.Settings.ERMetricsJar) Then
                MessageBox.Show(Constants.ERMetricsSaveFile, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else
                SaveFileERMetricsBtn.Enabled = False
                Dim hBean As New HelperBean
                hBean.RichTexBoxNameBool = False
                OTSHelper1.RunERMetrics(hBean)

            End If
        Catch ex As Exception
            ERMetricsTxBx.Text = Nothing
            MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try

    End Sub

    Private Sub CommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem1.Click
        Try
            '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            ' If richTextBox.CanPaste(MyFormat) Then
            richTextBox.SelectedText = "#" & richTextBox.SelectedText
            '   End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub UncommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles UncommentSelectedTextToolStripMenuItem1.Click
        Try
            Dim array1() As Char = {"#"}

            ' Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            richTextBox.SelectedText = st.Trim
            'next two uncomment to save the file
            ' Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub SaveFileERMetricsBtn_Click(sender As Object, e As EventArgs) Handles SaveFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            OTSHelper1.SaveRichTextBoxFile(Constants.RichTextBox3, Me.RichTextBox3, Me.saveInfoCollection.Item(Constants.RichTextBox3).FileLoaded)

        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub EditFileERMetricsBtn_Click(sender As Object, e As EventArgs) Handles EditFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            RichTextBox3.ReadOnly = False
        Else
            MessageBox.Show(Constants.MakeFileReadOnlyError, "Read Only Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub ChangeOTSRootToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChangeOTSRootToolStripMenuItem.Click
        Try
            Dim diag As New LoadSuiteXMLFileDialog
            diag.ShowDialog()
            If Not String.IsNullOrEmpty(TestSuiteRootDirLbl.Text) Then
                Drive = My.Settings.TestSuiteRootDir.Substring(0, 1)
                LoadSuiteXMLFile()
                LoadTestSuiteTreeView()
                LoadTreeview()
                CheckLogFileSizes()
                LoadTestSuiteXMLFile()

            Else
                MessageBox.Show("You Must Fill the Suite Root Directory Text Box!", "Load Text Box", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception

        End Try

    End Sub
End Class
