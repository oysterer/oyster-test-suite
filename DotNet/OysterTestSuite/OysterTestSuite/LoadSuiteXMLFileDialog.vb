﻿Imports System.Windows.Forms

Public Class LoadSuiteXMLFileDialog
    Private Path As String = Nothing

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        OysterTestSuiteForm.TestSuiteRootDirLbl.Text = Path

        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub LoadSuiteXMLBtn_Click(sender As Object, e As EventArgs) Handles LoadSuiteXMLBtn.Click


        ' OysterTestSuiteForm.FolderBrowserDialog1.SelectedPath = OysterTestSuiteForm.Drive & ":\"
        OysterTestSuiteForm.FolderBrowserDialog1.SelectedPath = "/"
        If OysterTestSuiteForm.FolderBrowserDialog1.ShowDialog = DialogResult.OK Then

            ' OysterTestSuiteForm.TestSuiteRootDirTxBx.Text
            Path = OysterTestSuiteForm.FolderBrowserDialog1.SelectedPath
            DialogTxBx.Text = Path
            My.Settings.TestSuiteRootDir = Path
            '   oysterHelper.FillTextBox(OysterRootDirTxBx, FolderBrowserDialog1.SelectedPath)
        Else
            Me.Close()
        End If
    End Sub
End Class
