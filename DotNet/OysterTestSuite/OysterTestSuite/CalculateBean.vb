﻿
Imports System.Collections.Generic
''' <summary>
''' 
''' A basic javabean and useful to store and get information
''' modify as needed
''' 
''' </summary>
''' 

Public Class CalculateBean



    Private TestSuiteCaseWorkspace As String
    Private TestSuiteCaseScriptsDir As String
    Private TestSuiteOysterRoot As String
    Private TestSuiteOysterToJarRun As String
    Private LogFile As String
    Private PythonExe As String
    Private PythonFile As String
    Private OutPutLogFile As String

    Private OysterLinkFile As String

    Private TestCaseBeanDictionary As Dictionary(Of String, HelperBean)

    Private OutPutRefClusterFile As String

    Private OutputChangeReportFile As String

    Private OysterRunScript As String



    Private RefExpectedCount As Integer

    Private ClusterExpectedCount As Integer

    Private LogReferenceCount As Integer

    Private LogClusterCount As Integer
    Private LinkReferenceCount As Integer

    Private LinkClusterCount As Integer

    Private TestCase_Name As String

    Private TestCases As Dictionary(Of String, String)

    Private ChangeReport As Dictionary(Of String, Collection)


    Public Property LinkFileRefCount() As Integer
        Get
            Return LinkReferenceCount
        End Get
        Set(ByVal value As Integer)
            LinkReferenceCount = value
        End Set
    End Property

    Public Property LinkFileClusterCount() As Integer
        Get
            Return LinkClusterCount
        End Get
        Set(ByVal value As Integer)
            LinkClusterCount = value
        End Set
    End Property

    Public Property LogFileRefCount() As Integer
        Get
            Return LogReferenceCount
        End Get
        Set(ByVal value As Integer)
            LogReferenceCount = value
        End Set
    End Property

    Public Property LogFileClusterCount() As Integer
        Get
            Return LogClusterCount
        End Get
        Set(ByVal value As Integer)
            LogClusterCount = value
        End Set
    End Property



    ''used to hold richboxname and file loaded for saving


    Private RichTextName As String
    ''used to keep track of information

    'Private OysterRunTime As String
    ''Helpfull Stuff textboxes
    'Private InputCSVFilePath As String


    'Private CBStrapFilePath As String

    ''Next three attributes for Truthfile Creation

    'Private OysterIdTotalCount As Integer
    'Private OysterID As String

    ''true means create new run file false means
    ''create possible truth/Reference file in helpful stuff


    Private FileLoadedName As String


    'Private ChartLowesttValue As Integer

    '' Searching IKB position
    'Private iFoundPos As Integer = 0
    'Private IKBSearchFileName As String = ""
    ''IKB Management
    'Private DataGridTable As DataTable
    'Private Dictionary As Dictionary(Of String, String)
    'Private Bsource As BindingSource

    ''these refernces and methods not used at this time
    ''Private Analyze As Boolean = False
    ''others not use d at this time
    ''Private OysterRunCount As Integer

    'Private FileToLoad As String

    'Private ERMetricsJarTxtBx As String
    ''Private OysterLinkFile As String
    ''Private OysterInputFile As String
    'Private OysterJarFile As String
    'Private ERMetricsJarFile As String


    Public Property PythonExeDir() As String
        Get
            Return PythonExe
        End Get
        Set(ByVal value As String)
            PythonExe = value
        End Set
    End Property


    Public Property RichTextBoxName() As String
        Get
            Return RichTextName
        End Get
        Set(ByVal value As String)
            RichTextName = value
        End Set
    End Property

    Public Property FileLoaded() As String
        Get
            Return FileLoadedName
        End Get
        Set(ByVal value As String)
            FileLoadedName = value
        End Set
    End Property

    Public Property PythonRunFile() As String
        Get
            Return PythonFile
        End Get
        Set(ByVal value As String)
            PythonFile = value
        End Set
    End Property

    Public Property TestSuiteRootDir() As String
        Get
            Return TestSuiteCaseWorkspace
        End Get
        Set(ByVal value As String)
            TestSuiteCaseWorkspace = value
        End Set
    End Property

    Public Property TestSuiteScriptsDir() As String
        Get
            Return TestSuiteCaseScriptsDir
        End Get
        Set(ByVal value As String)
            TestSuiteCaseScriptsDir = value
        End Set
    End Property

    Public Property OysterRootTestSuiteDir() As String
        Get
            Return TestSuiteOysterRoot
        End Get
        Set(ByVal value As String)
            TestSuiteOysterRoot = value
        End Set
    End Property

    Public Property OysterJarToRun() As String
        Get
            Return TestSuiteOysterToJarRun
        End Get
        Set(ByVal value As String)
            TestSuiteOysterToJarRun = value
        End Set
    End Property

    Public Property AllTestCasesDictionary() As Dictionary(Of String, HelperBean)
        Get
            Return TestCaseBeanDictionary
        End Get
        Set(ByVal value As Dictionary(Of String, HelperBean))
            TestCaseBeanDictionary = value
        End Set
    End Property


    Public Property TestCasesDictionary() As Dictionary(Of String, String)
        Get
            Return TestCases
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            TestCases = value
        End Set
    End Property

    Public Property RefClusterOutPutFile() As String
        Get
            Return OutPutRefClusterFile
        End Get
        Set(ByVal value As String)
            OutPutRefClusterFile = value
        End Set
    End Property

    'Public Property KBSearchLoadedFileName() As String
    '    Get
    '        Return IKBSearchFileName
    '    End Get
    '    Set(ByVal value As String)
    '        IKBSearchFileName = value
    '    End Set
    'End Property


    Public Property ExpectedRefCount() As Integer
        Get
            Return RefExpectedCount
        End Get
        Set(ByVal value As Integer)
            RefExpectedCount = value
        End Set
    End Property


    'Public Property ChartLowValue() As Integer
    '    Get
    '        Return ChartLowesttValue
    '    End Get
    '    Set(ByVal value As Integer)
    '        ChartLowesttValue = value
    '    End Set
    'End Property

    'Public Property SearchPositionNow() As Integer
    '    Get
    '        Return iFoundPos
    '    End Get
    '    Set(ByVal value As Integer)
    '        iFoundPos = value
    '    End Set
    'End Property

    'Public Property BindingSource() As BindingSource
    '    Get
    '        Return Bsource
    '    End Get
    '    Set(ByVal value As BindingSource)
    '        Bsource = value
    '    End Set
    'End Property

    Public Property ChangeReports() As Dictionary(Of String, Collection)
        Get
            Return ChangeReport
        End Get
        Set(ByVal value As Dictionary(Of String, Collection))
            ChangeReport = value
        End Set
    End Property

    'Public Property OysterIdCount() As Integer
    '    Get
    '        Return OysterIdTotalCount
    '    End Get
    '    Set(ByVal value As Integer)
    '        OysterIdTotalCount = value
    '    End Set
    'End Property

    'Public Property OysterIDValue() As String
    '    Get
    '        Return OysterID
    '    End Get
    '    Set(ByVal value As String)
    '        OysterID = value
    '    End Set
    'End Property

    'Public Property RichTexBoxNameBool() As Boolean
    '    Get
    '        Return RichTextName
    '    End Get
    '    Set(ByVal value As Boolean)
    '        RichTextName = value
    '    End Set
    'End Property

    Public Property ExpectedClusterCount() As Integer
        Get
            Return ClusterExpectedCount
        End Get
        Set(ByVal value As Integer)
            ClusterExpectedCount = value
        End Set
    End Property

    'Public Property InputCSVFileAndPath() As String
    '    Get
    '        Return InputCSVFilePath
    '    End Get
    '    Set(ByVal value As String)
    '        InputCSVFilePath = value
    '    End Set
    'End Property

    Public Property TestCaseName() As String
        Get
            Return TestCase_Name
        End Get
        Set(ByVal value As String)
            TestCase_Name = value
        End Set
    End Property

    Public Property OysterLinkFileWithPath() As String
        Get
            Return OysterLinkFile
        End Get
        Set(ByVal value As String)
            OysterLinkFile = value
        End Set
    End Property

    'Public Property OysterRunTimeValue() As String
    '    Get
    '        Return OysterRunTime
    '    End Get
    '    Set(ByVal value As String)
    '        OysterRunTime = value
    '    End Set
    'End Property


    Public Property OysterRunScriptName() As String
        Get
            Return OysterRunScript
        End Get
        Set(ByVal value As String)
            OysterRunScript = value
        End Set
    End Property

    Public Property TestSuiteLogFile() As String
        Get
            Return LogFile
        End Get
        Set(ByVal value As String)
            LogFile = value
        End Set
    End Property

    Public Property RunOutputLogFile() As String
        Get
            Return OutPutLogFile
        End Get
        Set(ByVal value As String)
            OutPutLogFile = value
        End Set
    End Property

    Public Property ChangeReportOutputFile() As String
        Get
            Return OutputChangeReportFile
        End Get
        Set(ByVal value As String)
            OutputChangeReportFile = value
        End Set
    End Property


    'these not used at this time

    'Public Property ERMetricsJarTxtBxValue() As String
    '    Get
    '        Return ERMetricsJarTxtBx
    '    End Get
    '    Set(ByVal value As String)
    '        ERMetricsJarTxtBx = value
    '    End Set
    'End Property

    'Public Property DataGridViewTable() As DataTable
    '    Get
    '        Return DataGridTable
    '    End Get
    '    Set(ByVal value As DataTable)
    '        DataGridTable = value
    '    End Set
    'End Property



    'Public Property OysterRunCountValue() As Integer
    '    Get
    '        Return OysterRunCount
    '    End Get
    '    Set(ByVal value As Integer)
    '        OysterRunCount = value
    '    End Set
    'End Property

    'Public Property ERMetricsJarName() As String
    '    Get
    '        Return ERMetricsJarFile
    '    End Get
    '    Set(ByVal value As String)
    '        ERMetricsJarFile = value
    '    End Set
    'End Property

    'Public Property OysterJarFileFileName() As String
    '    Get
    '        Return OysterJarFile
    '    End Get
    '    Set(ByVal value As String)
    '        OysterJarFile = value
    '    End Set
    'End Property

    'Public Property IKBFileToLoad As String
    '    Get
    '        Return FileToLoad
    '    End Get
    '    Set(ByVal value As String)
    '        FileToLoad = value
    '    End Set
    'End Property

    'Public Property OysterInputFileName() As String
    '    Get
    '        Return OysterInputFile
    '    End Get
    '    Set(ByVal value As String)
    '        OysterInputFile = value
    '    End Set
    'End Property

    'Public Property IKBMDictionary() As Dictionary(Of String, String)
    '    Get
    '        Return Dictionary
    '    End Get
    '    Set(ByVal value As Dictionary(Of String, String))
    '        Dictionary = value
    '    End Set
    'End Property

    'Public Property AnalyzeRun() As Boolean
    '    Get
    '        Return Analyze
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Analyze = value
    '    End Set
    'End Property


    'Public Property OysterLinkFileName() As String
    '    Get
    '        Return OysterLinkFile
    '    End Get
    '    Set(ByVal value As String)
    '        OysterLinkFile = value
    '    End Set
    'End Property


End Class
