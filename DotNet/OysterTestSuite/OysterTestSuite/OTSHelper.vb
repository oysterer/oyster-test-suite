﻿Imports System.IO
Imports System.Xml

Public Class OTSHelper

    'Good coding practices say to get all the work you can out of the 
    'Windows form Class.  Because of this, I need to make all methods 
    'called here public.  The in form calls are all private.

    '--------------------------------------------------------------
    'holders of nolink and nochangereport demo tests
    'share them between classes - oh well, the cost 
    'of moving work to other classes
    Public Shared NoLinkFile As Hashtable
    Public Shared noChangeReportfile As Hashtable
    'errors holder
    Private RunErrorsList As List(Of String)
    'to hold the errors
    Public Shared errorsTestBean As New ErrorsTestBean
    'single run
    Public Shared OneRunXMLFile As String = Nothing




    ''' <summary>
    ''' Change report vs. expected values validation
    ''' </summary>
    ''' <param name="helprBean"></param>
    Public Sub CheckTheIndentityChangeReports(ByRef helprBean As HelperBean)

        Dim aFile As System.IO.StreamWriter = Nothing

        Try
            Dim ChangeReportFile As String = helprBean.OysterRootTestSuiteDir & "/" & helprBean.TestCaseName & "/Output/Identity Change Report.txt"

            If File.Exists(ChangeReportFile) Then
                'set the marker for has identity change report
                helprBean.HasChangeReportFile = True

                Dim str() As String = File.ReadAllLines(ChangeReportFile)
                Dim changeReportOut As String = My.Settings.OysterRootTestSuiteDir & "/" & helprBean.TestCaseName & "/Output/" & helprBean.ChangeReportOutputFile

                aFile = New StreamWriter(changeReportOut, True, System.Text.Encoding.UTF8, 65536)
                aFile.WriteLine("---------------------------------------------------------------------")
                aFile.WriteLine(vbLf)
                aFile.WriteLine(helprBean.TestCaseName)
                aFile.WriteLine("Run Date is " & Now.ToShortDateString & " at " & Now.ToShortTimeString)
                aFile.WriteLine(My.Settings.OysterJarToRun)
                aFile.WriteLine("-------------------Change Report Check Start------------------------")
                For Each st As String In str

                    If st.Contains("Count of Output Identities") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportOutputIdentitiesCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "OutputIdentities")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    ElseIf st.Contains("Count of Input Identities Updated") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportInputIdentitiesUpdatedCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "InputIdentitiesUpdated")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    ElseIf st.Contains("Count of Input Identities Not Updated") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportInputIdentitiesNotUpdatedCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "InputIdentitiesNotUpdated")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    ElseIf st.Contains("Count of Input Identities Merged") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportInputIdentitiesMergedCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "InputIdentitiesMerged")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    ElseIf st.Contains("Count of New Identities") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportNewInputIdentitiesCreatedCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "NewInputIdentitiesCreated")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    ElseIf st.Contains("Count of Error Identities:") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportErrorIdentitiesCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "ErrorIdentities")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                            Exit For
                        End If

                    ElseIf st.Contains("Count of Input Identities") Then
                        aFile.WriteLine(st)
                        Dim colen As Integer = st.IndexOf(":")
                        st = st.Substring(colen + 1).Trim
                        helprBean.ChangeReportInputIdentitiesCount = CInt(st)

                        Dim ExpectedValue As String = ChangeReportValues(helprBean, "InputIdentities")
                        If Not ExpectedValue.Equals("") Then
                            aFile.WriteLine(ExpectedValue)
                        End If

                    End If

                Next
                str = Nothing
            Else
                helprBean.HasChangeReportFile = False
            End If
            'aFile.WriteLine(vbCrLf)
            'aFile.WriteLine("-------------------End of Change Report Check ----------------------")
            'aFile.WriteLine(vbCrLf)

        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("NOTE: Error chcecking change reports is " & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)

        Finally
            If aFile IsNot Nothing Then
                aFile.Close()
                aFile = Nothing
            End If
        End Try

    End Sub


    ''' <summary>
    ''' Take the helperbean by Reference and returnstringwanted meaning the key name value needed
    ''' and return Expected values as counts. Worker function to keep code lines down during reporting.
    ''' </summary>
    ''' <param name="helprBean"></param>
    ''' <param name="ReturnStringWanted"></param>
    ''' <returns></returns>
    Private Function ChangeReportValues(ByRef helprBean As HelperBean, ByVal ReturnStringWanted As String) As String

        Dim dic As New Dictionary(Of String, Collection)
        dic = helprBean.ChangeReports
        Dim ReturnString As String = Nothing
        If Not dic Is Nothing Then

            For Each key In dic.Keys

                Dim col As New Collection
                col = dic.Item(key)
                Dim diction As New Dictionary(Of String, String)
                diction = CType(col.Item(1), Dictionary(Of String, String))

                For Each myKey In diction.Keys

                    If myKey.Equals("OutputIdentities") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectOutputIdentitiesCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("InputIdentitiesUpdated") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectInputIdentitiesUpdatedCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("InputIdentitiesMerged") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectInputIdentitiesMergedCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("NewInputIdentitiesCreated") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectNewInputIdentitiesCreatedCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("InputIdentitiesNotUpdated") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectInputIdentitiesNotUpdatedCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("ErrorIdentities") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectErrorIdentitiesCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For
                    ElseIf myKey.Equals("InputIdentities") And myKey.Equals(ReturnStringWanted) Then
                        helprBean.ChangeReportExpectInputIdentitiesCount = CInt(diction.Item(myKey))
                        ReturnString = Constants.Expected & myKey & Constants.Counts & diction.Item(myKey)
                        Exit For

                    End If
                Next
            Next

        Else
            Return String.Empty
        End If
        Return ReturnString
    End Function

    ''' <summary>
    ''' Get the link file name from individual XML files for the
    ''' only used here so can be private
    ''' </summary>
    ''' <param name="RunScriptName"></param>
    ''' <returns></returns>
    Private Function GetLinkFileName(ByVal RunScriptName As String) As String

        Dim linkFileName As String = Nothing

        Try

            If System.IO.File.Exists(RunScriptName) Then

                Dim xelement As XElement = XElement.Load(RunScriptName)

                linkFileName = xelement.Descendants("LinkOutput").Value

                If Not String.IsNullOrEmpty(linkFileName) Then

                    If linkFileName.StartsWith(".") Then

                        linkFileName = My.Settings.OysterRootTestSuiteDir & linkFileName.Substring(linkFileName.IndexOf(".") + 1)
                        linkFileName = linkFileName.Replace("/", "/")

                    Else

                        linkFileName = My.Settings.OysterRootTestSuiteDir & "/" & linkFileName.Substring(linkFileName.IndexOf(":" + 8))
                    End If

                End If
            Else
                MessageBox.Show(Constants.RunScriptNotFound, "Link File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("NOTE: Error Thrown Getting Link File Name is " & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            '  MessageBox.Show("Error Thrown Getting Link File Name is " & ex.Message, "Oyster Run File", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally

        End Try
        Return linkFileName
    End Function

    ''' <summary>
    '''  Run the OysterTestSuite process
    ''' </summary>
    Public Sub RunFiles()


        Dim DictOfTestCasesToRun As New Dictionary(Of String, String)
        Dim AllTestCaseDictionary As New Dictionary(Of String, HelperBean)
        Dim helpBean As New HelperBean

        Try

            OysterTestSuiteForm.OysterTestSuiteTabs.SelectTab(1)
            Dim list As String = Nothing
            Dim doc As New XmlDocument()
            '-------------------------------------
            'Note have to figure out what to do here
            Dim xmlFile As String = OysterTestSuiteForm.TestSuiteRootDirLbl.Text & "/TestSuite.xml"

            helpBean.TestSuiteRootDir = My.Settings.TestSuiteRootDir
            helpBean.OysterRootTestSuiteDir = My.Settings.OysterRootTestSuiteDir
            helpBean.TestSuiteScriptsDir = My.Settings.TestSuiteScriptsDir
            helpBean.OysterJarToRun = My.Settings.OysterJarToRun
            helpBean.PythonExeDir = My.Settings.PythonExeDirctory
            helpBean.PythonRunFile = My.Settings.PythonRunFile
            helpBean.TestSuiteLogFile = My.Settings.TestSuiteLogFile


            Dim xelement As XElement = XElement.Load(xmlFile)
            OysterTestSuiteForm.RunRichTextBox.Clear()

            If Not OneRunXMLFile = Nothing Then
                Dim x As String = OneRunXMLFile.Replace("\", "/")
                DictOfTestCasesToRun.Add(x, x)
                OysterTestSuiteForm.RunRichTextBox.AppendText(x)
                OysterTestSuiteForm.RunRichTextBox.AppendText(vbCrLf)
            Else


                Dim rules As IEnumerable(Of XElement) = xelement.Descendants("TestCase")

                For Each x As XElement In rules

                    DictOfTestCasesToRun.Add(x, x)
                    OysterTestSuiteForm.RunRichTextBox.AppendText(x)
                    OysterTestSuiteForm.RunRichTextBox.AppendText(vbCrLf)

                Next
            End If

            helpBean.TestCasesDictionary = DictOfTestCasesToRun
            DictOfTestCasesToRun = helpBean.TestCasesDictionary
            Dim TestCaseHelperBean As HelperBean
            errorsTestBean.TestsRunCnt = helpBean.TestCasesDictionary.Count

            For Each key As String In DictOfTestCasesToRun.Keys
                TestCaseHelperBean = New HelperBean
                Dim TestToRun As String = helpBean.TestSuiteScriptsDir & "/" & DictOfTestCasesToRun.Item(key)
                TestCaseHelperBean = GetRunParameters(TestToRun, TestCaseHelperBean)
                AllTestCaseDictionary.Add(DictOfTestCasesToRun.Item(key), TestCaseHelperBean)
            Next

            helpBean.AllTestCasesDictionary = AllTestCaseDictionary


            Dim i As Integer = 1
            Dim howManyTestCases As Integer = AllTestCaseDictionary.Count
            Dim OysterRoot As String = helpBean.OysterRootTestSuiteDir
            OysterRoot = OysterRoot.Replace("\", "/")



            For Each key In AllTestCaseDictionary.Keys
                Dim helprBean As HelperBean = AllTestCaseDictionary(key)
                helprBean.OysterLinkFileWithPath = GetLinkFileName(helprBean.OysterRunScriptName)

                If howManyTestCases = 1 Then
                    Dim runscript = helprBean.OysterRunScriptName.Replace("\", "\")
                    ' list = OysterRoot & "//" & helprBean.OysterRunScriptName
                    list = runscript
                    Exit For
                ElseIf howManyTestCases > 1 Then

                    If i < howManyTestCases Then
                        Dim runscript = helprBean.OysterRunScriptName.Replace("\", "/")
                        list = list & runscript & ","
                        i += 1
                    Else
                        Dim runscript = helprBean.OysterRunScriptName.Replace("\", "/")
                        '  list = OysterRoot & "//" & helprBean.OysterRunScriptName
                        ' list = list & OysterRoot & "//" & helprBean.OysterRunScriptName
                        list = list & runscript
                    End If

                End If
            Next


            Dim process As New ProcessClass
            process.RunPython(list, helpBean)



            'this collection add is very important to the program storage of data
            If Not OysterTestSuiteForm.saveInfoCollection.Item(99) Is Nothing Then
                OysterTestSuiteForm.saveInfoCollection.Remove(99)
                OysterTestSuiteForm.saveInfoCollection.Add(99, helpBean)
            Else
                OysterTestSuiteForm.saveInfoCollection.Add(99, helpBean)
            End If

        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("NOTE: ERROR in RunFiles is " & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show("ERROR in RunFiles is " & ex.Message, "Run Files Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            'make single run null so it doesn't get called when
            'you are done with it.
            If Not OysterTestSuiteForm.KeepChkBox1.Checked Then
                OneRunXMLFile = Nothing
            End If

        End Try


    End Sub


    ''' <summary>
    ''' 'Parse the individual ZML files for each Oyster Directory we are 
    ''' running and store in a HelperBean and return to the caller. All the 
    ''' expected outputs are retrieved here.
    ''' </summary>
    ''' <param name="TestToRun"></param>
    ''' <returns></returns>
    Public Function GetRunParameters(ByVal TestToRun As String, ByVal helpBeaner As HelperBean) As HelperBean

        Dim helpBean As HelperBean
        helpBean = helpBeaner
        Dim ReportOutputFiles As New Dictionary(Of String, String)

        Try
            Dim runChangeReports As Boolean = True

            helpBean.TestSuiteLogFile = My.Settings.TestSuiteLogFile
            helpBean.OysterRootTestSuiteDir = My.Settings.OysterRootTestSuiteDir

            Dim xelement As XElement = XElement.Load(TestToRun)

            Dim attList As IEnumerable(Of XAttribute) = xelement.Attributes("RunScript")
            Dim RunScript As String = Nothing
            For Each attr In attList
                helpBean.OysterRunScriptName = My.Settings.OysterRootTestSuiteDir & "/" & attr.Value
            Next

            Dim attL As IEnumerable(Of XAttribute) = xelement.Attributes("LogFile")
            For Each att In attL
                helpBean.RunOutputLogFile = att.Value
            Next

            Dim NameList As IEnumerable(Of XAttribute) = xelement.Attributes(Constants.Name)
            Dim Name As String = Nothing
            For Each NList In NameList
                helpBean.TestCaseName = NList.Value
            Next

            Dim rules As IEnumerable(Of XElement) = xelement.Descendants("Output")
            Dim expected As IEnumerable(Of XElement) = xelement.Descendants("Expected")
            '   Dim References As IEnumerable(Of XElement) = xelement.Descendants("References")
            '  Dim CLusters As IEnumerable(Of XElement) = xelement.Descendants("Clusters")


            'cut out repetition as much
            'as possible of loop in expected 
            'with counters
            Dim refCount = 0
            Dim clusCount = 0
            'get the reference and cluster counts for comparison later
            For Each eList In expected
                Dim parentExpect As String = eList.Parent.Parent.Name.ToString

                If parentExpect.Equals(Constants.References) And refCount = 0 Then

                    For Each expect In expected
                        Dim at As IEnumerable(Of XAttribute) = expect.Attributes()
                        For Each V In at
                            If V.Value.ToString.Equals("LogReferenceCounts") Then
                                helpBean.LogRefExpectCount = Integer.Parse(expect.Value)
                                Exit For
                            ElseIf V.Value.Equals("LinkReferenceCounts") Then
                                helpBean.LinkRefExpectCount = Integer.Parse(expect.Value)
                                refCount = 1
                                Exit For
                            End If

                        Next
                        If refCount = 1 Then
                            Exit For
                        End If

                    Next

                ElseIf parentExpect.Equals("Clusters") And clusCount = 0 Then
                    For Each expect In expected
                        Dim at As IEnumerable(Of XAttribute) = expect.Attributes()
                        For Each V In at
                            If V.Value.ToString.Equals("LogClusterCounts") Then
                                helpBean.LogClusterExpectCount = Integer.Parse(expect.Value)
                                Exit For
                            ElseIf V.Value.Equals("LinkClusterCounts") Then
                                helpBean.LinkClusterExpectCount = Integer.Parse(expect.Value)
                                clusCount = 1
                                Exit For
                            End If

                        Next
                        If clusCount = 1 Then
                            Exit For
                        End If
                    Next
                End If
            Next

            For Each ref In rules
                ' ReportOutputFiles.Clear()
                Dim parent As String = ref.Parent.Parent.Name.ToString

                If parent.Equals("OutputFiles") Then
                    If ref.Value.Contains("ReferenceAndCluster") Then
                        helpBean.RefClusterOutPutFile = ref.Value

                    ElseIf ref.Value.Contains(Constants.ChangeReport) Then
                        helpBean.ChangeReportOutputFile = ref.Value

                    End If

                ElseIf parent.Equals(Constants.ChangeReports) Then
                    If runChangeReports Then

                        For Each expect In expected
                            Dim parentExpected As String = expect.Parent.Parent.Name.ToString
                            Dim parentExpect As String = expect.Parent.Name.ToString

                            If parentExpect.Equals(Constants.Compare) And parentExpected.Equals(Constants.ChangeReports) Then
                                Try
                                    If Not ReportOutputFiles.ContainsKey(expect.Attribute(Constants.Name).Value) Then
                                        ReportOutputFiles.Add(expect.Attribute(Constants.Name).Value, expect.Value)
                                    Else

                                    End If
                                Catch ex As Exception


                                    Console.WriteLine("Error trying to add " & expect.Attribute(Constants.Name).Value & " to Dictionary ReportOutPutFiles")
                                End Try


                            End If

                        Next
                    End If


                    If ReportOutputFiles.Count > 0 Then

                        Dim collection As New Collection From {
                            ReportOutputFiles
                        }
                        Dim dic As New Dictionary(Of String, Collection)

                        If helpBean.ChangeReports Is Nothing Then

                            dic.Add(helpBean.TestCaseName, collection)
                            helpBean.ChangeReports = dic
                        Else
                            dic = helpBean.ChangeReports
                            If Not dic.ContainsKey(helpBean.TestCaseName) Then
                                dic.Add(helpBean.TestCaseName, collection)
                                helpBean.ChangeReports = dic
                            End If

                        End If
                        runChangeReports = False
                    End If
                End If
            Next

        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("OTSHelper GetRunParameters error is " & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show("OTSHelper GetRunParameters error is " & ex.Message, "GetRunParameters Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally

        End Try
        Return helpBean

    End Function

    ''' <summary>
    ''' This method loads from the OysterTestSuite directory
    ''' and all directories under it for the Log Output File Tab
    ''' into the TreeView on that Tab
    ''' </summary>
    ''' <param name="LogFileTreeView"></param>
    Public Sub LoadTreeview(ByRef LogFileTreeView As TreeView)

        Dim myDir As String = My.Settings.TestSuiteRootDir
        Dim DNode As TreeNode
        Dim CNode As New TreeNode
        Try


            LogFileTreeView.Nodes.Clear()

            DNode = New TreeNode(myDir)
            LogFileTreeView.Nodes.Add(DNode)

            For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(myDir, FileIO.SearchOption.SearchTopLevelOnly)

                If Not Path.GetFileName(foundDirectory2).ToUpper.Equals("DOCUMENTATION") And
                    Not Path.GetFileName(foundDirectory2).ToUpper.Equals("SCRIPTS") And
                    Not Path.GetFileName(foundDirectory2).ToUpper.Equals("PYTHON") Then

                    CNode = New TreeNode(Path.GetFileName(foundDirectory2))
                    LogFileTreeView.Nodes(0).Nodes.Add(CNode)

                End If


                'If Path.GetFileName(foundDirectory2).ToUpper.Equals("DOCUMENTATION") Then
                '    'do nothing
                'End If

                'If Not Path.GetFileName(foundDirectory2).ToUpper.Equals("SCRIPTS") Then
                '    CNode = New TreeNode(Path.GetFileName(foundDirectory2))

                '    LogFileTreeView.Nodes(0).Nodes.Add(CNode)
                'End If

                If Path.GetFileName(foundDirectory2).ToUpper.Equals("Oyster".ToUpper) Then
                    For Each foundDirectory3 As String In My.Computer.FileSystem.GetDirectories(foundDirectory2, FileIO.SearchOption.SearchTopLevelOnly)
                        If Not foundDirectory3.ToUpper.Contains("data".ToUpper) Then

                            Dim KNode As New TreeNode(Path.GetFileName(foundDirectory3))
                            LogFileTreeView.Nodes(0).LastNode.Nodes.Add(Path.GetFileName(foundDirectory3), Path.GetFileName(foundDirectory3))
                            LogFileTreeView.Update()

                            For Each Fdirectory As String In My.Computer.FileSystem.GetDirectories(foundDirectory3)

                                If Fdirectory.ToUpper.Contains(Constants.OUTPUT) Then

                                    Dim jNode = New TreeNode(Path.GetFileName(foundDirectory3))
                                    LogFileTreeView.Nodes.Find(Path.GetFileName(foundDirectory3), True)(0).Nodes.Add(Fdirectory, Path.GetFileName(Fdirectory))

                                    For Each file As String In My.Computer.FileSystem.GetFiles(Fdirectory)

                                        If file.Contains("ReferenceAndClusterCounts") Or file.Contains("ChangeReportIdentitiesCounts") Or file.Contains("Log_0.log") Then
                                            LogFileTreeView.Nodes.Find(Fdirectory, True)(0).Nodes.Add(Path.GetFileName(file)).Tag = file
                                        End If
                                    Next
                                    Exit For
                                End If

                            Next

                        End If
                    Next

                End If

                If Path.GetFileName(foundDirectory2).ToUpper.Equals("RUNLOGS") Then

                    Dim Anode As TreeNode
                    Anode = LogFileTreeView.Nodes.Item(Path.GetFileName(foundDirectory2))

                    For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)


                        If (file.Contains(".log")) Then

                            CNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                        End If
                    Next

                End If

                'If Path.GetFileName(foundDirectory2).ToUpper.Equals("SCRIPTS") Then

                '    Dim Anode As TreeNode
                '    Anode = LogFileTreeView.Nodes.Item(Path.GetFileName(foundDirectory2))
                '    For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)


                '        If (file.Contains(".xml")) Then
                '            CNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                '        End If
                '    Next

                'End If
            Next

        Catch ex As Exception

            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("Error Loading TreeView is " & ex.Message & Now, Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show("Error Loading Tree View is " & ex.Message, "LoadTreeview Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

        LogFileTreeView.Nodes(0).Expand()
        LogFileTreeView.Nodes(0).Nodes(1).Expand()

        LogFileTreeView.Show()

    End Sub


    ''' <summary>
    ''' This method does all the heavy lifting for the comparison of expected results
    ''' and the output from the run. 
    ''' </summary>
    ''' <param name="Collect"></param>
    ''' <param name="RunCheckRTB"></param>
    ''' <param name="LogWriter"></param>
    Public Sub CreateOutputCheck(ByVal Collect As Collection, ByRef RunCheckRTB As RichTextBox, ByRef LogWriter As OTSLogWriter)

        Dim OutputCollection As New Collection
        OutputCollection = Collect
        If RunCheckRTB.Text.Length > 5 Then
            RunCheckRTB.Text += vbCrLf
        End If
        Try
            NoLinkFile = New Hashtable
            noChangeReportfile = New Hashtable
            RunErrorsList = New List(Of String)
            Dim errorsBean As ErrorsTestBean
            Dim dtime As String = errorsTestBean.PythonRunTime1
            'the global holder of errors for reporting
            'get a new one for every run
            '  errorsTestBean = New ErrorsTestBean()
            errorsTestBean.PythonRunTime1 = dtime

            For Each helperBean As HelperBean In OutputCollection
                'individual error bean for each helperbean in collection
                errorsBean = New ErrorsTestBean()
                errorsBean.TestsRunCnt = OutputCollection.Count
                errorsBean.TestBeanNme = helperBean.TestCaseName
                errorsBean.JarUsedInRn = My.Settings.OysterJarToRun
                errorsBean.DateTme = Now.ToShortDateString

                errorsTestBean.TestsRunCnt = OutputCollection.Count
                errorsTestBean.JarUsedInRn = My.Settings.OysterJarToRun
                Dim dt As Date = Date.Today

                errorsTestBean.DateTme = dt.ToString("MM'/'dd'/'yyyy") + " " + Format(Now, "HH:mm")

                RunCheckRTB.SelectionStart = RunCheckRTB.Text.Length
                RunCheckRTB.ScrollToCaret()

                Dim PrintInteger As Integer = 0

                If Not helperBean.LogRefExpectCount = helperBean.LogFileRefCount Then

                    If PrintInteger = 0 Then
                        LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                    End If

                    'this keeps it from printing header above for each next of the same testcase
                    PrintInteger += 1
                    RunCheckRTB.Text += Constants.ErrorExpectRefCountLogFileRefCount
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += Constants.ExpectRefCnt & helperBean.LogRefExpectCount
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += "Log File Reference Count   " & helperBean.LogFileRefCount
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += vbCrLf

                    errorsBean.Lst.Add(Constants.ExpectRefCnt & helperBean.LogRefExpectCount)
                    errorsBean.Lst.Add("Log File Reference Count   " & helperBean.LogFileRefCount)

                    'write to log file
                    LogWriter.WriteToLog(Constants.ErrorExpectRefCountLogFileRefCount)
                    LogWriter.WriteToLog(Constants.ExpectRefCnt & helperBean.LogRefExpectCount, "Log File Reference Count    " & helperBean.LogFileRefCount)
                    LogWriter.WriteToLog(Environment.NewLine)

                End If

                If Not helperBean.LogClusterExpectCount = helperBean.LogFileClusterCount Then
                    If PrintInteger = 0 Then
                        LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                    End If
                    PrintInteger += 1

                    RunCheckRTB.Text += Constants.ErrorExpectClusterCntLogFileClusterCnt
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += Constants.ExpectClusterCnt & helperBean.LogClusterExpectCount
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += "Log File Cluster Count    " & helperBean.LogFileClusterCount
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += vbCrLf

                    errorsBean.Lst.Add(Constants.ExpectClusterCnt & helperBean.LogClusterExpectCount)
                    errorsBean.Lst.Add("Log File Cluster Count    " & helperBean.LogFileClusterCount)

                    LogWriter.WriteToLog(Constants.ErrorExpectClusterCntLogFileClusterCnt)
                    LogWriter.WriteToLog(Constants.ExpectClusterCnt & helperBean.LogClusterExpectCount, "Log File Cluster Count    " & helperBean.LogFileClusterCount)
                    LogWriter.WriteToLog(Environment.NewLine)


                End If

                If My.Computer.FileSystem.FileExists(helperBean.OysterLinkFileWithPath) Then


                    If Not helperBean.LinkRefExpectCount = helperBean.LinkFileRefCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1

                        RunCheckRTB.Text += Constants.ErrorExpectRefCntLinkFileRefCnt
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Link File Reference Count " & helperBean.LinkRefExpectCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Link File Reference Count   " & helperBean.LinkFileRefCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf

                        errorsBean.Lst.Add("Expected Link File Reference Count " & helperBean.LinkRefExpectCount)
                        errorsBean.Lst.Add("Link File Reference Count   " & helperBean.LinkFileRefCount)


                        LogWriter.WriteToLog(Constants.ErrorExpectRefCntLinkFileRefCnt)
                        LogWriter.WriteToLog("Expected Link File Reference Count " & helperBean.LinkRefExpectCount, "Link File Reference Count    " & helperBean.LinkFileRefCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If


                    If Not helperBean.LinkClusterExpectCount = helperBean.LinkFileClusterCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1
                        RunCheckRTB.Text += Constants.ErrorExpectClusterCntLinkFileClusterCnt
                        ' RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Link File Cluster Count " & helperBean.LinkClusterExpectCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Link File Cluster Count  " & helperBean.LinkFileClusterCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf

                        errorsBean.Lst.Add("Expected Link File Cluster Count " & helperBean.LinkClusterExpectCount)
                        errorsBean.Lst.Add("Link File Cluster Count  " & helperBean.LinkFileClusterCount)

                        LogWriter.WriteToLog(Constants.ErrorExpectClusterCntLinkFileClusterCnt)
                        LogWriter.WriteToLog("Expected Link File Cluster Count " & helperBean.LinkClusterExpectCount, "Link File Cluster Count   " & helperBean.LinkFileClusterCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If
                Else

                    helperBean.HasLinkFile = False
                    If Not NoLinkFile.ContainsKey(helperBean.TestCaseName) Then
                        NoLinkFile.Add(helperBean.TestCaseName, helperBean.TestCaseName)
                    End If

                End If

                If helperBean.HasChangeReportFile = True Then

                    If Not helperBean.ChangeReportExpectOutputIdentitiesCount = helperBean.ChangeReportOutputIdentitiesCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1


                        RunCheckRTB.Text += Constants.ErrorExpectOutIdentChgeReportOutIdent
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Output Indentities Count     " & helperBean.ChangeReportExpectOutputIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Output Identities Count  " & helperBean.ChangeReportOutputIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Output Indentities Count     " & helperBean.ChangeReportExpectOutputIdentitiesCount)
                        errorsBean.Lst.Add("Change Report Output Identities Count  " & helperBean.ChangeReportOutputIdentitiesCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectOutIdentChgeReportOutIdent)
                        LogWriter.WriteToLog("Expected Output Indentities Count     " & helperBean.ChangeReportExpectOutputIdentitiesCount, "Change Report Output Identities Count    " & helperBean.ChangeReportOutputIdentitiesCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If

                    If Not helperBean.ChangeReportExpectInputIdentitiesCount = helperBean.ChangeReportInputIdentitiesCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1

                        RunCheckRTB.Text += Constants.ErrorExpectInputIdentChgeReportInputIdent
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Input Indentities Count      " & helperBean.ChangeReportExpectInputIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Input Identities Count  " & helperBean.ChangeReportInputIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Input Indentities Count      " & helperBean.ChangeReportExpectInputIdentitiesCount)
                        errorsBean.Lst.Add("Change Report Input Identities Count  " & helperBean.ChangeReportInputIdentitiesCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectInputIdentChgeReportInputIdent)
                        LogWriter.WriteToLog("Expected Input Indentities Count      " & helperBean.ChangeReportExpectInputIdentitiesCount, "Change Report Input Identities Count " & helperBean.ChangeReportInputIdentitiesCount)
                        LogWriter.WriteToLog(Environment.NewLine)
                        ' LogWriter.WriteToLog("-----------------")

                    End If

                    If Not helperBean.ChangeReportExpectInputIdentitiesUpdatedCount = helperBean.ChangeReportInputIdentitiesUpdatedCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1


                        RunCheckRTB.Text += Constants.ErrorExpectInputIdentUpdateChgeReportInputIdentUpdate
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Input Indentities Updated Count      " & helperBean.ChangeReportExpectInputIdentitiesUpdatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Input Identities Updated Count  " & helperBean.ChangeReportInputIdentitiesUpdatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Input Indentities Updated Count      " & helperBean.ChangeReportExpectInputIdentitiesUpdatedCount)
                        errorsBean.Lst.Add("Change Report Input Identities Updated Count  " & helperBean.ChangeReportInputIdentitiesUpdatedCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectInputIdentUpdateChgeReportInputIdentUpdate)
                        LogWriter.WriteToLog("Expected Input Indentities Update Count      " & helperBean.ChangeReportExpectInputIdentitiesUpdatedCount, "Change Report Input Identities Updated Count  " & helperBean.ChangeReportInputIdentitiesUpdatedCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If


                    If Not helperBean.ChangeReportExpectInputIdentitiesNotUpdatedCount = helperBean.ChangeReportInputIdentitiesNotUpdatedCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1


                        RunCheckRTB.Text += Constants.ErrorExpectNotUpdatedIdentCntChgReptNotUpdateIdentCnt
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Expected Not Updated Indentities Count      " & helperBean.ChangeReportExpectInputIdentitiesNotUpdatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Not Updated Identities Count  " & helperBean.ChangeReportInputIdentitiesNotUpdatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Not Updated Indentities Count      " & helperBean.ChangeReportExpectInputIdentitiesNotUpdatedCount)
                        errorsBean.Lst.Add("Change Report Not Updated Identities Count  " & helperBean.ChangeReportInputIdentitiesNotUpdatedCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectNotUpdatedIdentCntChgReptNotUpdateIdentCnt)
                        LogWriter.WriteToLog("Expected Not Updated Indentities       " & helperBean.ChangeReportExpectInputIdentitiesNotUpdatedCount, "Change Report Not Updated Identities   " & helperBean.ChangeReportInputIdentitiesNotUpdatedCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If


                    If Not helperBean.ChangeReportExpectInputIdentitiesMergedCount = helperBean.ChangeReportInputIdentitiesMergedCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1

                        RunCheckRTB.Text += Constants.ErrorExpectInputIdentMergCntChgRptInputIdentMergCnt
                        RunCheckRTB.Text += "Expected Input Indentities Nerged Count      " & helperBean.ChangeReportExpectInputIdentitiesMergedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Input Identities Merged Count  " & helperBean.ChangeReportInputIdentitiesMergedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Input Indentities Nerged Count      " & helperBean.ChangeReportExpectInputIdentitiesMergedCount)
                        errorsBean.Lst.Add("Change Report Input Identities Merged Count  " & helperBean.ChangeReportInputIdentitiesMergedCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectInputIdentMergCntChgRptInputIdentMergCnt)
                        LogWriter.WriteToLog("Expected Input Indentities Nerged       " & helperBean.ChangeReportExpectInputIdentitiesMergedCount, "Change Report Input Identities Merged   " & helperBean.ChangeReportInputIdentitiesMergedCount)
                        LogWriter.WriteToLog(Environment.NewLine)
                    End If

                    If Not helperBean.ChangeReportExpectNewInputIdentitiesCreatedCount = helperBean.ChangeReportNewInputIdentitiesCreatedCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1

                        RunCheckRTB.Text += Constants.ErrorExpectNewInputIdentCreatCntChgRptNewInputIdentCreatCnt
                        RunCheckRTB.Text += vbLf & "Expected New Input Indentities Created      " & helperBean.ChangeReportExpectNewInputIdentitiesCreatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report New Input Identities Created  " & helperBean.ChangeReportNewInputIdentitiesCreatedCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected New Input Indentities Created      " & helperBean.ChangeReportExpectNewInputIdentitiesCreatedCount)
                        errorsBean.Lst.Add("Change Report New Input Identities Created  " & helperBean.ChangeReportNewInputIdentitiesCreatedCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectNewInputIdentCreatCntChgRptNewInputIdentCreatCnt)
                        LogWriter.WriteToLog("Expected New Input Indentities      " & helperBean.ChangeReportExpectNewInputIdentitiesCreatedCount, "Change Report New Input Identities  " & helperBean.ChangeReportNewInputIdentitiesCreatedCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If

                    If Not helperBean.ChangeReportExpectErrorIdentitiesCount = helperBean.ChangeReportErrorIdentitiesCount Then
                        If PrintInteger = 0 Then
                            LogWriter.WriteHeader(RunCheckRTB, LogWriter, helperBean)

                        End If
                        PrintInteger += 1

                        RunCheckRTB.Text += Constants.ErrorExpectErrorIdentCntChgRptErrorIdentCnt
                        RunCheckRTB.Text += "Expected Error Indentities Count     " & helperBean.ChangeReportExpectErrorIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += "Change Report Error Identities Count   " & helperBean.ChangeReportErrorIdentitiesCount
                        RunCheckRTB.Text += vbCrLf
                        RunCheckRTB.Text += vbCrLf
                        errorsBean.Lst.Add("Expected Error Indentities Count     " & helperBean.ChangeReportExpectErrorIdentitiesCount)
                        errorsBean.Lst.Add("Change Report Error Identities Count   " & helperBean.ChangeReportErrorIdentitiesCount)
                        LogWriter.WriteToLog(Constants.ErrorExpectErrorIdentCntChgRptErrorIdentCnt)
                        LogWriter.WriteToLog("Expected Error Indentities Count     " & helperBean.ChangeReportExpectErrorIdentitiesCount, "Change Report Error Identities Count   " & helperBean.ChangeReportErrorIdentitiesCount)
                        LogWriter.WriteToLog(Environment.NewLine)

                    End If
                Else
                    helperBean.HasChangeReportFile = False
                    If Not noChangeReportfile.ContainsKey(helperBean.TestCaseName) Then
                        noChangeReportfile.Add(helperBean.TestCaseName, helperBean.TestCaseName)
                    End If

                End If

                If PrintInteger > 0 Then
                    '  RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += "End of " & helperBean.TestCaseName
                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += "-----------------"
                    RunCheckRTB.Text += vbCrLf
                    LogWriter.WriteToLog("End of " & helperBean.TestCaseName)
                    LogWriter.WriteToLog("-----------------")

                End If


                RunCheckRTB.SelectionStart = RunCheckRTB.Text.Length
                RunCheckRTB.ScrollToCaret()
                If errorsBean.Lst.Count > 0 Then
                    errorsTestBean.ErrorOutputBeanLst.Add(errorsBean)
                End If


            Next

            If NoLinkFile.Count > 0 Then


                RunCheckRTB.Text += vbCrLf
                RunCheckRTB.Text += vbCrLf
                RunCheckRTB.Text += Constants.LinkFilestNotFndTestCases
                LogWriter.WriteToLog(Constants.LinkFilestNotFndTestCases)
                'Writes out all test directories run that did not have 
                'a link file created.
                For Each key As String In NoLinkFile.Keys

                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += NoLinkFile(key).ToString
                    LogWriter.WriteToLog(NoLinkFile(key).ToString)

                Next
                LogWriter.WriteToLog(Environment.NewLine)
                RunCheckRTB.Text += vbCrLf
                RunCheckRTB.Text += vbCrLf
            End If
            If noChangeReportfile.Count > 0 Then

                RunCheckRTB.Text += Constants.ChgReptNotFndTestCases
                LogWriter.WriteToLog(Constants.ChgReptNotFndTestCases)

                For Each key As String In noChangeReportfile.Keys

                    RunCheckRTB.Text += vbCrLf
                    RunCheckRTB.Text += noChangeReportfile(key).ToString
                    LogWriter.WriteToLog(noChangeReportfile(key).ToString)

                Next
                LogWriter.WriteToLog(Environment.NewLine)
                RunCheckRTB.Text += vbCrLf
            End If

            RunCheckRTB.Text += vbCrLf
            RunCheckRTB.Text += vbCrLf
            RunCheckRTB.Text += Constants.EndOysterTestSuiteRun & Now
            RunCheckRTB.Text += vbCrLf
            ' LogWriter.WriteToLog(Environment.NewLine)
            LogWriter.WriteToLog(Constants.EndOysterTestSuiteRun & Now, Environment.NewLine)
            '  LogWriter.WriteToLog(Environment.NewLine)

            LoadErrors()
            OysterTestSuiteForm.FillTableRow()


        Catch ex As Exception
            RunCheckRTB.Text += vbCrLf
            RunCheckRTB.Text += Constants.ErrorCreateOutputCheck & ex.Message
            RunCheckRTB.Text += vbCrLf
            LogWriter.WriteToLog(Environment.NewLine)
            LogWriter.WriteToLog(Constants.ErrorCreateOutputCheck & ex.Message & Now, Environment.NewLine)
            LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show(Constants.ErrorCreateOutputCheck & ex.Message, "CreateOutput Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub


    Private Sub LoadErrors()
        Try
            'clear the listbox before reloading
            OysterTestSuiteForm.ListBox1.Items.Clear()

            For Each eBean As ErrorsTestBean In errorsTestBean.ErrorOutputBeanLst
                If OysterTestSuiteForm.ListBox1.FindStringExact(eBean.TestBeanNme) Then

                    OysterTestSuiteForm.ListBox1.Items.Add(eBean.TestBeanNme)
                End If


            Next

            OysterTestSuiteForm.Label15.Text = "Run Date Time: " & OTSHelper.errorsTestBean.DateTme
            OysterTestSuiteForm.Label20.Text = "Number of Tests: " & OTSHelper.errorsTestBean.TestsRunCnt
            OysterTestSuiteForm.Label18.Text = "Failed: " & OTSHelper.errorsTestBean.ErrorOutputBeanLst.Count.ToString()
            OysterTestSuiteForm.Label19.Text = "Passed:: " & (OTSHelper.errorsTestBean.TestsRunCnt - OTSHelper.errorsTestBean.ErrorOutputBeanLst.Count).ToString
            OysterTestSuiteForm.Label22.Text = "Jar: " & OTSHelper.errorsTestBean.JarUsedInRn
        Catch ex As Exception
            Console.WriteLine("LoadErrors Error is " & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' This method writes out the two files listed in the Test.xml file for each directory
    ''' for expected and output clusters for the log file and the link file.  The files reside
    ''' in the output directory under each test directory
    ''' </summary>
    ''' <param name="Collect"></param>
    ''' <returns></returns>
    Public Function CompareResults(ByRef Collect As Collection) As Collection


        '   Dim helprBean As New HelperBean
        '          helprBean = helpBean
        Dim aFile As System.IO.StreamWriter = Nothing
        Dim _values As New HashSet(Of String)
        Dim CalcValues As New Dictionary(Of String, Integer)
        For Each helprBean As HelperBean In Collect

            Try

                Dim outputFile As String = helprBean.OysterRootTestSuiteDir & "/" & helprBean.TestCaseName & "/Output/" & helprBean.RefClusterOutPutFile
                aFile = My.Computer.FileSystem.OpenTextFileWriter(outputFile, True)
                ' aFile = New StreamWriter(outputFile, True, System.Text.Encoding.UTF8, 65536)
                aFile.WriteLine("-------------------------------------------------")
                aFile.WriteLine(vbLf)
                aFile.WriteLine(helprBean.TestCaseName)
                aFile.WriteLine("Run Date is " & Now.ToShortDateString & " at " & Now.ToShortTimeString & " ---------------------------")
                aFile.WriteLine(My.Settings.OysterJarToRun)
                aFile.WriteLine("")
                aFile.WriteLine("Total Log References Expected " & helprBean.LogRefExpectCount)
                aFile.WriteLine("Total Log Clusters Created Expected " & helprBean.LogClusterExpectCount)
                aFile.WriteLine("Total Link References Expectedd " & helprBean.LinkRefExpectCount)
                aFile.WriteLine("Total Link Clusters Expected " & helprBean.LinkClusterExpectCount)
                aFile.WriteLine("")
                aFile.WriteLine("From Log File " & helprBean.RunOutputLogFile)
                aFile.WriteLine("")


                Dim logFile As String = helprBean.OysterRootTestSuiteDir & "/" & helprBean.TestCaseName & "/Output/" & helprBean.RunOutputLogFile


                If System.IO.File.Exists(logFile) Then
                    Dim str() As String = File.ReadAllLines(logFile)

                    For Each st As String In str
                        If st.StartsWith("Total Records Processed") Then

                            aFile.WriteLine(st)

                            Dim colen As Integer = st.IndexOf(":")
                            st = st.Substring(colen + 1).Trim

                            helprBean.LogFileRefCount = CInt(st)
                            Dim len As Integer = st.Length


                        ElseIf st.StartsWith("Total Clusters") Then

                            aFile.WriteLine(st)

                            Dim colen As Integer = st.IndexOf(":")
                            st = st.Substring(colen + 1).Trim
                            helprBean.LogFileClusterCount = CInt(st)

                        End If
                    Next
                    str = Nothing

                End If


                aFile.WriteLine("")
                Dim linkFile As String = helprBean.OysterLinkFileWithPath
                If Not String.IsNullOrEmpty(linkFile) Then
                    aFile.WriteLine("From the link file " & Path.GetFileName(linkFile))
                    aFile.WriteLine("")
                Else
                    aFile.WriteLine("No Link File Available!")
                    aFile.WriteLine("")
                End If

                Dim counter As Integer = 0

                If System.IO.File.Exists(linkFile) Then
                    Dim str() = File.ReadAllLines(linkFile)

                    For Each st As String In str

                        st = st.Substring(st.IndexOf(vbTab))
                        st = st.Trim
                        Dim s = st.Substring(0, st.IndexOf(vbTab))
                        s = s.Trim

                        If Not s.Contains("OysterID") Then
                            counter += 1

                            If Not _values.Contains(s) And Not s.Contains("XXXXXXXXXXXXXXXX") Then

                                _values.Add(s)
                                '  StrList.Add(s)
                            End If
                        End If

                    Next

                End If


                aFile.WriteLine(Constants.LinkFileReferenceCount & counter)
                helprBean.LinkFileRefCount = counter
                aFile.WriteLine("Link File Cluster Count is " & _values.Count)
                helprBean.LinkFileClusterCount = _values.Count

                aFile.WriteLine("")
                aFile.WriteLine("End " & helprBean.TestCaseName)
                aFile.WriteLine("End of Run -----------------------------------------")

                counter = 0
                _values.Clear()

                OysterTestSuiteForm.CheckTheIndentityReports(helprBean)

            Catch ex As Exception
                OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                OysterTestSuiteForm.LogWriter.WriteToLog(Constants.ErrorCompareResultsFuncton & ex.Message)
                OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)

                aFile.WriteLine(vbCrLf)
                aFile.WriteLine(Constants.ErrorCompareResultsFuncton & ex.Message)
                aFile.WriteLine(vbCrLf)

                MessageBox.Show(Constants.ErrorCompareResultsFuncton & ex.Message, "Compare Results Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Finally

                If aFile IsNot Nothing Then
                    aFile.Flush()
                    aFile.Close()
                    aFile = Nothing
                End If



            End Try
        Next

        Return Collect

    End Function



    ''' <summary>
    ''' This method loads Treeview3 on the main setup screen
    ''' </summary>
    Public Sub LoadTestSuiteTreeView()

        Try

            Dim myDir As String = OysterTestSuiteForm.TestSuiteRootDirLbl.Text
            OysterTestSuiteForm.TreeView3.Nodes.Clear()
            OysterTestSuiteForm.TreeView3.Nodes.Add(myDir)

            Dim s As String = OysterTestSuiteForm.TestSuiteRootDirLbl.Text
            Dim str As String = Path.GetFileName(myDir)
            Dim l As List(Of String) = IO.Directory.GetFiles(s, "*.xml").Select(Function(file) IO.Path.GetFileName(file)).ToList

            For Each item As String In l
                '  If item.ToUpper.Contains(str.ToUpper & Constants.RUNSCRIPTS) And item.StartsWith(str) Then
                Dim sa As String = s + "/" + item
                OysterTestSuiteForm.TreeView3.TopNode.Nodes.Add(item).Tag = sa
                ' End If
            Next
            Dim DNode As TreeNode
            Dim DNode1 As TreeNode
            Dim DNode2 As TreeNode
            Dim Dnode3 As TreeNode
            Dim Dnode4 As TreeNode
            Dim Dnode5 As TreeNode
            Dim cnt As Integer = 0
            For Each foundDirectory As String In My.Computer.FileSystem.GetDirectories(
                 myDir, FileIO.SearchOption.SearchAllSubDirectories)
                If Path.GetFileName(foundDirectory).ToUpper.Equals("OYSTER") Then
                    DNode = OysterTestSuiteForm.TreeView3.TopNode.Nodes.Add(Path.GetFileName(foundDirectory))



                    For Each foundDirectory6 As String In My.Computer.FileSystem.GetDirectories(
                        foundDirectory, FileIO.SearchOption.SearchAllSubDirectories)

                        '''''''''''''''''''''''''''''''''''''''''''''''''''
                        ' specifically for ER_Metrics root directory
                        ' because it has no input,output,scripts directories 
                        ' Like other directories
                        '''''''''''''''''''''''''''''''''''''''''''''''''''
                        If foundDirectory6.ToUpper.Contains("ER_Calculator".ToUpper) Then
                            Dnode3 = DNode.Nodes.Add(Path.GetFileName(foundDirectory6))
                            For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory6)
                                If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".jar", comparisonType:=1) And
                                    Not file.EndsWith(".ert", comparisonType:=1) Then
                                    Dnode3.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        End If
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'End of special ERMetrics Directory processing
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If foundDirectory6.ToUpper.Contains("data".ToUpper) Then
                            Dnode4 = DNode.Nodes.Add(Path.GetFileName(foundDirectory6))
                            For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory6)
                                If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".jar", comparisonType:=1) And
                                    Not file.EndsWith(".ert", comparisonType:=1) Then
                                    Dnode4.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        End If


                        If Not foundDirectory6.ToUpper.Contains("INPUT") And Not foundDirectory6.ToUpper.Contains("OUTPUT") And Not foundDirectory6.ToUpper.Contains("SCRIPTS") _
                            And Not foundDirectory6.ToUpper.Contains("ER_Calculator".ToUpper) And Not foundDirectory6.ToUpper.Contains("data".ToUpper) Then
                            DNode1 = DNode.Nodes.Add(Path.GetFileName(foundDirectory6))
                            For Each fDirectory As String In My.Computer.FileSystem.GetDirectories(
                            foundDirectory6, FileIO.SearchOption.SearchAllSubDirectories)
                                DNode2 = DNode1.Nodes.Add(Path.GetFileName(fDirectory))
                                For Each file As String In My.Computer.FileSystem.GetFiles(fDirectory)
                                    If Not file.EndsWith(".zip") Then
                                        DNode2.Nodes.Add(Path.GetFileName(file)).Tag = file
                                    End If
                                Next
                            Next

                        End If



                    Next

                    For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory)
                        If Not file.EndsWith(".zip") Then
                            DNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                        End If
                    Next


                ElseIf Path.GetFileName(foundDirectory).ToUpper.Equals("SCRIPTS") And cnt = 0 Then
                    Dnode5 = OysterTestSuiteForm.TreeView3.TopNode.Nodes.Add(Path.GetFileName(foundDirectory))
                    For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory)
                        If Not file.EndsWith(".zip") Then
                            Dnode5.Nodes.Add(Path.GetFileName(file)).Tag = file
                        End If
                    Next
                    cnt = 1
                End If

            Next

            OysterTestSuiteForm.TreeView3.Nodes(0).Expand()
            OysterTestSuiteForm.TreeView3.Nodes(0).EnsureVisible()
            OysterTestSuiteForm.TreeView3.Show()
        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog(Constants.LoadingTreeViewError & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show(Constants.LoadingTreeViewError & ex.Message, "Error Loading TreeView", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    'adds a row of run data to the data table in hustory tab
    Public Sub FillTableRow()

        Dim row As New DataGridViewRow

        Dim sArray(OysterTestSuiteForm.DataGridView1.Columns.Count - 1) As String

        'only 8 couumns so far
        For c As Integer = 0 To OysterTestSuiteForm.DataGridView1.Columns.Count - 1

            Select Case c

                Case 0
                    sArray(c) = OTSHelper.errorsTestBean.DateTme
                Case 1
                    sArray(c) = My.Settings.OysterJarToRun
                Case 2
                    sArray(c) = OTSHelper.errorsTestBean.TestsRunCnt.ToString
                Case 3
                    sArray(c) = OTSHelper.errorsTestBean.ErrorOutputBeanLst.Count.ToString
                Case 4
                    sArray(c) = (OTSHelper.errorsTestBean.TestsRunCnt - OTSHelper.errorsTestBean.ErrorOutputBeanLst.Count).ToString
                Case 5
                    sArray(c) = OTSHelper.NoLinkFile.Count.ToString
                Case 6
                    sArray(c) = OTSHelper.noChangeReportfile.Count.ToString
                Case 7
                    sArray(c) = OTSHelper.errorsTestBean.PythonRunTime1
                Case Else
                    '
            End Select
        Next c
        OysterTestSuiteForm.DataGridView1.Rows.Insert(0, sArray)
        OysterTestSuiteForm.SaveDataGridView()

    End Sub

    ''' <summary>
    ''' This method reads the jTable1Data.data file and loads it
    ''' to the gridview on the run history tab
    ''' </summary>
    Public Sub Reader()

        Dim dt As DataTable
        '   Dim bsource As BindingSource

        Try
            dt = New DataTable
            Dim makeCols As Integer = 0
            Dim row As DataGridViewRow
            Dim RunLogsDir As String = My.Settings.TestSuiteLogFile.Substring(0, My.Settings.TestSuiteLogFile.LastIndexOf("/"))
            Using objReader As New StreamReader(RunLogsDir & "/JTable1Data.data")
                Do While objReader.Peek() <> -1
                    Dim line As String = objReader.ReadLine()

                    Dim splitLine() As String
                    splitLine = Split(line, ",")
                    If makeCols = 0 Then
                        For Each str As String In splitLine
                            If Not String.IsNullOrEmpty(str) Then
                                Dim col As New DataGridViewColumn()
                                col.Name = str
                                OysterTestSuiteForm.DataGridView1.Columns.Add(str, str)
                                '      dt.Columns.Add(str)
                            End If

                            makeCols = 1
                        Next
                    Else
                        Dim c As Integer = 0
                        Dim sArray(splitLine.Count - 1) As String


                        For Each st As String In splitLine
                            If Not String.IsNullOrEmpty(st) Then
                                sArray(c) = st
                                c = c + 1

                            End If

                        Next
                        row = New DataGridViewRow
                        row.SetValues(sArray)
                        OysterTestSuiteForm.DataGridView1.Rows.Add(sArray)

                    End If


                Loop

                'to insert rows at the beginning cannot use datasource
                'bsource = New BindingSource
                'bsource.DataSource = dt
                'DataGridView1.DataSource = bsource

                OysterTestSuiteForm.DataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells)
                OysterTestSuiteForm.DataGridView1.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells)
                OysterTestSuiteForm.DataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            End Using
        Catch ex As Exception
            Console.WriteLine("Error in reader " & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' obvious
    ''' </summary>
    ''' <param name="TreeView"></param>
    ''' <param name="RichTextBox"></param>
    Public Sub LoadFileFromTreeView(ByRef TreeView As TreeView, ByRef RichTextBox As RichTextBox)

        Try
            Dim fileExists As Boolean = My.Computer.FileSystem.FileExists(TreeView.SelectedNode.Tag)

            If fileExists Then

                Dim fileAndPath As String = TreeView.SelectedNode.Tag.ToString
                RichTextBox.LoadFile(fileAndPath, RichTextBoxStreamType.PlainText)
                Dim i As Integer = 0
                Dim RichTextBoxName As String = Nothing
                Dim fileLoaded As String = Path.GetFileName(fileAndPath)

                Select Case RichTextBox.Name

                    Case Constants.RichTextBox1String
                        i = Constants.RichTextBox1
                        RichTextBoxName = Constants.RichTextBox1String
                        OysterTestSuiteForm.Label10.Text = Constants.FileLoadedIs & fileLoaded

                    Case Constants.RichTextBox9String
                        i = Constants.RichTextBox9
                        RichTextBoxName = Constants.RichTextBox9String
                        OysterTestSuiteForm.LoadedFileLbl.Text = Constants.LoadedFileIs & fileLoaded

                       ' Oysterform.WelLoadedFileLbl.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox2String
                        i = Constants.RichTextBox2
                        RichTextBoxName = Constants.RichTextBox2String
                     '   Oysterform.WorkWithOysterFile.Text = Constants.FileLoadedIs & fileLoaded
                    Case Constants.RichTextBox3String
                        i = Constants.RichTextBox3
                        RichTextBoxName = Constants.RichTextBox3String
                        OysterTestSuiteForm.ERMetricsLoadedFile.Text = Constants.FileLoadedIs & fileLoaded
                        OysterTestSuiteForm.SaveFileERMetricsBtn.Enabled = True
                    Case Else
                        MessageBox.Show(Constants.AddRichTextBoxName, "RichTextBox Name Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                End Select


                RichTextBox.LoadFile(fileAndPath, RichTextBoxStreamType.PlainText)

                'save the file name that is loaded to make sure we save the correct file
                Dim helperBean As New HelperBean
                helperBean.RichTextBoxName = RichTextBoxName
                helperBean.FileLoaded = fileAndPath

                'use the integer in constants for each textbox to store
                'in the hashtable
                If Not OysterTestSuiteForm.saveInfoCollection.Item(i) Is Nothing Then
                    OysterTestSuiteForm.saveInfoCollection.Remove(i)
                    OysterTestSuiteForm.saveInfoCollection.Add(i, helperBean)
                Else
                    OysterTestSuiteForm.saveInfoCollection.Add(i, helperBean)
                End If


                If RichTextBox.Text = Nothing Then
                    RichTextBox.Text = Constants.FileIsEmpty
                End If

            Else
                MessageBox.Show("File Selected does not Exist", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog(Constants.LoadingRichTextBoxError & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show(Constants.LoadingRichTextBoxError & ex.Message, "Error Loading RichTextBox", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally

        End Try

    End Sub

    'obvious
    Public Sub LoadSuiteXMLFile()

        Try

            Dim doc As New XmlDocument()
            '-------------------------------------
            Dim xmlFile As String = OysterTestSuiteForm.TestSuiteRootDirLbl.Text & "/TestSuite.xml"

            '-----------------------------
            doc.Load(xmlFile)
            Dim nodelist As XmlNodeList = doc.GetElementsByTagName("TestSuiteInfo")

            For Each node As XmlElement In nodelist
                If node.GetAttribute(Constants.Name).Equals("TestSuiteRootDir") Then
                    My.Settings.TestSuiteRootDir = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("TestSuiteScriptsDir") Then
                    OysterTestSuiteForm.TestSuiteScriptsDirLbl.Text = node.InnerText
                    My.Settings.TestSuiteScriptsDir = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("OysterRootTestSuiteDir") Then
                    OysterTestSuiteForm.OysterRootTestSuiteDirLbl.Text = node.InnerText
                    My.Settings.OysterRootTestSuiteDir = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("OysterJarToRun") Then
                    OysterTestSuiteForm.OysterJarToRunLbl.Text = node.InnerText
                    errorsTestBean.JarUsedInRn = node.InnerText
                    My.Settings.OysterJarToRun = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("TestSuiteLogFile") Then
                    OysterTestSuiteForm.TestSuiteLogFileLbl.Text = node.InnerText
                    My.Settings.TestSuiteLogFile = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("PythonExeDir") Then
                    OysterTestSuiteForm.PythonExeDirLbl.Text = node.InnerText
                    My.Settings.PythonExeDirctory = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("PythonRunFile") Then
                    OysterTestSuiteForm.PythonRunFileLbl.Text = node.InnerText
                    My.Settings.PythonRunFile = node.InnerText
                ElseIf node.GetAttribute(Constants.Name).Equals("TestSuiteXMLFile") Then
                    OysterTestSuiteForm.TestSuiteXMLbl.Text = node.InnerText
                    My.Settings.TestSuiteXMLFile = node.InnerText
                End If
            Next
            My.Settings.Save()
            My.Settings.Reload()



        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog(Constants.ErrorReadTestSuiteXML & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show(Constants.ErrorReadTestSuiteXML & ex.Message, "Load XML Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally

        End Try
    End Sub


    ''' <summary>
    ''' Checks output files for being too large and roll them
    ''' </summary>
    Public Shared Sub CheckLogFileSizes()

        Try

            Dim myDir As String = My.Settings.TestSuiteRootDir

            For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(myDir, FileIO.SearchOption.SearchTopLevelOnly)

                If Path.GetFileName(foundDirectory2).ToUpper.Equals("RunLogs".ToUpper) Then

                    For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)

                        If Not String.IsNullOrEmpty(file) And file.EndsWith("TestSuite.log") Then
                            Dim filePath As String = file

                            If IO.File.Exists(filePath) Then

                                Dim fi As New IO.FileInfo(filePath)

                                If fi.Length > 300000 Then
                                    'process everthing and write a specific error to Testsuite.log but move on
                                    Try
                                        Dim name As String = "TestSuite" & Now.Month & Now.Day & Now.Year & Now.Second & Now.Millisecond & ".log"
                                        My.Computer.FileSystem.RenameFile(filePath, name)
                                    Catch ex As Exception
                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                        OysterTestSuiteForm.LogWriter.WriteToLog("Error Checking Testsuite.log File Size is " & ex.Message)
                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                    End Try


                                End If
                            End If


                        ElseIf Not String.IsNullOrEmpty(file) And file.EndsWith("PythonRunfile.log") Then
                            Dim filePath As String = file

                            If IO.File.Exists(filePath) Then

                                Dim fi As New IO.FileInfo(filePath)

                                If fi.Length > 300000 Then
                                    'process everthing and write a specific error to Testsuite.log but move on
                                    Try
                                        Dim name As String = "PythonRunfile" & Now.Month & Now.Day & Now.Year & Now.Second & Now.Millisecond & ".log"
                                        My.Computer.FileSystem.RenameFile(filePath, name)
                                    Catch ex As Exception
                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                        OysterTestSuiteForm.LogWriter.WriteToLog("Error Checking PythonRunfile.log File Size is " & ex.Message)
                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                    End Try


                                End If
                            End If

                        End If
                    Next


                ElseIf Path.GetFileName(foundDirectory2).ToUpper.Equals("Oyster".ToUpper) Then

                    For Each foundDirectory3 As String In My.Computer.FileSystem.GetDirectories(foundDirectory2, FileIO.SearchOption.SearchTopLevelOnly)

                        For Each foundDirectory4 As String In My.Computer.FileSystem.GetDirectories(foundDirectory3, FileIO.SearchOption.SearchTopLevelOnly)

                            If foundDirectory4.ToUpper.Contains(Constants.OUTPUT) Then
                                For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory4)

                                    If Not String.IsNullOrEmpty(file) And file.Contains(Constants.RefAndClusterCnts) Or file.Contains(Constants.ChgRptIdentCnts) Then

                                        Dim filePath As String = file

                                        If IO.File.Exists(filePath) Then
                                            Dim fi As New IO.FileInfo(filePath)

                                            If fi.Length > 100000 Then
                                                'process everthing and write a specific error to Testsuite.log but move on
                                                Try
                                                    If file.Contains(Constants.RefAndClusterCnts) Then
                                                        Dim name As String = Constants.RefAndClusterCnts & Now.Month & Now.Day & Now.Year & Now.Second & Now.Millisecond & ".log"
                                                        My.Computer.FileSystem.RenameFile(filePath, name)

                                                    ElseIf file.Contains(Constants.ChgRptIdentCnts) Then
                                                        Dim name As String = Constants.ChgRptIdentCnts & Now.Month & Now.Day & Now.Year & Now.Second & Now.Millisecond & ".log"
                                                        My.Computer.FileSystem.RenameFile(filePath, name)

                                                    End If

                                                Catch ex As Exception
                                                    If file.Contains(Constants.RefAndClusterCnts) Then

                                                        OysterTestSuiteForm.LogWriter.WriteToLog(Now.ToShortDateString, Now.ToShortTimeString)
                                                        OysterTestSuiteForm.LogWriter.WriteToLog("Error Checking ReferenceAndClusterCounts.txt File Size is " & ex.Message)
                                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                                    Else
                                                        OysterTestSuiteForm.LogWriter.WriteToLog(Now.ToShortDateString, Now.ToShortTimeString)
                                                        OysterTestSuiteForm.LogWriter.WriteToLog("Error Checking ChangeReportIdentitiesCounts.txt File Size is " & ex.Message)
                                                        OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
                                                    End If

                                                End Try


                                            End If
                                        End If
                                    End If
                                Next

                            End If

                        Next


                    Next

                Else

                End If
            Next

        Catch ex As Exception
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            OysterTestSuiteForm.LogWriter.WriteToLog("Error Checking Log File Sizes is " & ex.Message)
            OysterTestSuiteForm.LogWriter.WriteToLog(Environment.NewLine)
            MessageBox.Show("Error Checking Log File Sizes is " & ex.Message, "Check File Size Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    ''' <summary>
    ''' This method allows for the java run process and python process to be stopped
    ''' </summary>
    Public Sub StopRun()
        Try
            Dim response As Integer = MessageBox.Show(Constants.StopJava, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Information)

            If response = DialogResult.Yes Then
                Dim _proceses As Process()
                Dim p As Process()

                Try
                    p = Process.GetProcessesByName("python")
                    For Each proces As Process In p
                        proces.Kill()
                    Next
                    _proceses = Process.GetProcessesByName("java")
                    For Each proces As Process In _proceses
                        proces.Kill()
                    Next

                Catch ex As ArgumentException
                    MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Try

            End If
        Catch ex As Exception

        End Try
    End Sub


    'Generic method to fill text boxes with correct information
    Public Sub FillTextBox(ByRef Textbox As TextBox, ByVal path As String)

        Dim thePath As String = path
        Dim DirType As String = Nothing
        Dim SettingValue As String = Nothing
        Dim Message As String = Nothing
        Dim ValueNow As String = Textbox.Text
        Dim TextboxName As String = Textbox.Name.Trim
        Dim fileOK As Boolean = False
        Dim directoryOk As Boolean = False

        Try



            If Not String.IsNullOrEmpty(thePath) Then

                Select Case TextboxName

                    Case "ERMetricsJarTxBx"
                        DirType = Constants.ERMETRICSJARName
                        SettingValue = My.Settings.ERMetricsJar
                        Message = Constants.ERMetricsJarNotFound
                        fileOK = InfoCall(thePath, Constants.File)
                    Case "EROysterWkDirTxBx"
                        DirType = Constants.WorkDirectory
                        SettingValue = My.Settings.EROysterWorkDir
                        Message = Constants.WorkDirUnderRoot
                        directoryOk = InfoCall(thePath, Constants.Directory)
                    Case "ERMetricsTxBx"
                        DirType = Constants.ERMetrics
                        SettingValue = My.Settings.ERMetricsWorkDir
                        Message = Constants.ERWorkingDirectory
                        directoryOk = InfoCall(thePath, Constants.Directory)

                End Select

            Else
                '     MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If fileOK Then

                If CheckPath(thePath, DirType) Then
                    thePath = thePath.Substring(thePath.IndexOf(":") + 1).Replace("\", "/")
                    Textbox.Text = thePath

                Else
                    If Not String.IsNullOrEmpty(SettingValue) Then
                        Textbox.Text = SettingValue
                        MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        Textbox.Text = Nothing
                        MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

            ElseIf directoryOk Then

                If CheckPath(thePath, DirType) Then
                    'it passes so put it in
                    thePath = thePath.Substring(thePath.IndexOf(":") + 1).Replace("\", "/")

                    Textbox.Text = thePath

                    If Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetricsTxBx.Text) And Not String.IsNullOrWhiteSpace((OysterTestSuiteForm.ERMetricsTxBx.Text)) Then
                        If Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetricsJarTxBx.Text) And Not String.IsNullOrWhiteSpace((OysterTestSuiteForm.ERMetricsJarTxBx.Text)) Then
                            If Not String.IsNullOrEmpty(OysterTestSuiteForm.EROysterWkDirTxBx.Text) And Not String.IsNullOrWhiteSpace((OysterTestSuiteForm.EROysterWkDirTxBx.Text)) Then
                                MessageBox.Show("Please Save Your Choices!", "Textbox Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                    End If

                Else
                    If Not String.IsNullOrEmpty(SettingValue) Then
                        Textbox.Text = SettingValue
                        MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        Textbox.Text = Nothing
                        MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            ElseIf Not fileOK Then

                MessageBox.Show(Message, "Jar Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ElseIf Not directoryOk Then
                MessageBox.Show(Message, "Directory Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)


            End If

        Catch ex As Exception
            MessageBox.Show("Error TextBox is " & ex.Message, "TextBox Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub
    'This InfoCall is for errors thrown using FileInfo and Directory Info
    'returns false if there is a problem with the chosen 
    Protected Shared Function InfoCall(TextBoxText As String, InfoType As String) As Boolean
        Try
            'if it throws and error then not good
            If InfoType.Equals(Constants.Directory) Then
                Dim directoryInfo As New DirectoryInfo(TextBoxText)
                If directoryInfo.Exists Then
                    Return True
                Else
                    Return False
                End If
            ElseIf InfoType.Equals(Constants.File) Then
                Dim fileInfo As New FileInfo(TextBoxText)
                If fileInfo.Exists Then
                    Return True
                Else
                    Return False
                End If

            End If

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' 
    '''check the path and files chosen to make sure it can be used
    '''paths and jar files are very important and must be rigid checks
    ''' </summary>
    ''' <param name="Path"></param>
    ''' <param name="DirType"></param>
    ''' <returns></returns>
    Public Function CheckPath(ByVal Path As String, ByVal DirType As String) As Boolean

        Dim list As FileInfo() = Nothing
        Dim yesNo As Boolean = False

        Try

            Select Case DirType

                Case Constants.WorkDirectory
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim sPath As String = Path + "/" + "scripts"
                        Dim directoryInfo As New DirectoryInfo(sPath.ToUpper)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.xml")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.EndsWith(".XML") Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If

                Case Constants.ERMetrics
                    Dim fileOk As Boolean = InfoCall(Path, Constants.Directory)
                    If fileOk Then
                        Dim directoryInfo As New DirectoryInfo(Path)
                        If directoryInfo.Exists Then
                            Try
                                list = directoryInfo.GetFiles("*.jar")
                                For Each str As FileInfo In list
                                    If str.Name.ToUpper.Contains(Constants.ERMetricsJar.ToUpper) Then
                                        yesNo = True
                                        Exit For
                                    End If
                                Next
                            Catch ex As Exception
                                Return False
                            End Try
                        Else
                            Return False
                        End If
                    End If
                Case Constants.ERMETRICSJARName
                    Dim fileOk As Boolean = InfoCall(Path, Constants.File)
                    If fileOk Then
                        Dim fileInfo As New FileInfo(Path)
                        If fileInfo.Name.ToUpper.Contains(Constants.ERMetricsJar.ToUpper) _
                              And fileInfo.Name.Contains(".jar") Then
                            If fileInfo.Exists Then
                                yesNo = True
                            Else
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    End If

                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Checking Paths", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return yesNo
    End Function

    'gerneric method to handel all loadable textboxes
    'check if the user inserted a space
    Public Sub TextChangedSpace(ByRef TextBox As TextBox)

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")

        If regSpace.IsMatch(TextBox.Text) Then

            Dim TextBoxName As String = TextBox.Name
            Dim TxtBoxValueTrimmed = TextBox.Text.Trim
            Dim SettingsValue As String

            Select Case TextBoxName

                Case "ERMetricsJarTxBx"
                    SettingsValue = My.Settings.ERMetricsJar

                Case "ERMetricsTxBx"
                    SettingsValue = My.Settings.ERMetricsWorkDir
                Case "EROysterWkDirTxBx"
                    SettingsValue = My.Settings.EROysterWorkDir
                Case Else
                    'if your not identified above 
                    'we don't care about you (readonly?)   RobotWkDirTxBx
                    Exit Sub
            End Select

            If Not String.IsNullOrEmpty(SettingsValue) Then
                TextBox.Text = SettingsValue
                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                TextBox.Text = TxtBoxValueTrimmed
                MessageBox.Show(Constants.UseBrowseButton, Constants.BrowseButton, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

    'Save the ERMetrics TextBoxes 
    Public Sub SaveERmetricsTxBoxes(ByVal ShowMessage As Boolean)
        'make sure no one is null or empty
        Try

            If Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetricsTxBx.Text) _
            And Not String.IsNullOrEmpty(OysterTestSuiteForm.EROysterWkDirTxBx.Text) _
            And Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetricsJarTxBx.Text) Then
                'save them when you can
                My.Settings.ERMetricsWorkDir = OysterTestSuiteForm.ERMetricsTxBx.Text
                My.Settings.ERMetricsJar = OysterTestSuiteForm.ERMetricsJarTxBx.Text
                My.Settings.EROysterWorkDir = OysterTestSuiteForm.EROysterWkDirTxBx.Text
                My.Settings.Save()
                If ShowMessage Then
                    MessageBox.Show(Constants.SettingsSaved, "Save Settings", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If


                Dim myDir As String = OysterTestSuiteForm.ERMetricsTxBx.Text
                '  Dim topNode As String = Nothing

                If OysterTestSuiteForm.TreeView4.GetNodeCount(False) > 0 Then
                    OysterTestSuiteForm.TreeView4.Nodes.Clear()
                End If

                OysterTestSuiteForm.TreeView4.Nodes.Add(myDir, myDir)

                For Each file As String In My.Computer.FileSystem.GetFiles(myDir)

                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".properties", comparisonType:=1) Then
                        OysterTestSuiteForm.TreeView4.Nodes(0).Nodes.Add(st).Tag = file
                    ElseIf st.EndsWith("s.log", comparisonType:=1) Then
                        OysterTestSuiteForm.TreeView4.Nodes(0).Nodes.Add(st).Tag = file
                    ElseIf st.EndsWith(".link", comparisonType:=1) Then
                        OysterTestSuiteForm.TreeView4.Nodes(0).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    End If

                Next
                Dim dir As String = OysterTestSuiteForm.EROysterWkDirTxBx.Text + "/" + "Output"
                OysterTestSuiteForm.TreeView4.Nodes.Add(dir, dir)

                For Each file As String In My.Computer.FileSystem.GetFiles(dir)
                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".link", comparisonType:=1) Then
                        OysterTestSuiteForm.TreeView4.Nodes(1).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                        Dim myFile = file.Substring(file.IndexOf(";") + 1).Replace("\", "/")
                        myFile = myFile.Substring(myFile.IndexOf("/"))

                        OysterTestSuiteForm.ERMetLinkFileTxbx.Text = myFile
                        OysterTestSuiteForm.LFDateLbl.Text = IO.File.GetLastWriteTime(file)
                    End If

                Next

                OysterTestSuiteForm.TreeView4.Nodes(0).Expand()
                OysterTestSuiteForm.TreeView4.Nodes(1).Expand()
                OysterTestSuiteForm.TreeView4.Nodes(1).EnsureVisible()
                OysterTestSuiteForm.TreeView4.Show()

            Else
                If ShowMessage Then
                    MessageBox.Show(Constants.ERTextBoxesMustBeFilled, "ERMetrics Text Boxes", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If

        Catch ex As Exception
            If ShowMessage Then
                MessageBox.Show("Error Saving ERMetrics TextBoxes", "ERMetrics Text Boxes", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End Try

    End Sub


    'Run ERMetrics
    Public Sub RunERMetrics(ByVal helperBean As HelperBean)
        Dim hBean As New HelperBean
        hBean.RichTexBoxNameBool = helperBean.RichTexBoxNameBool
        Dim pc As New ProcessClass
        Try

            Dim jarExists As Boolean = My.Computer.FileSystem.FileExists(OysterTestSuiteForm.ERMetricsJarTxBx.Text)
            If jarExists Then
                Dim ahelperBean As New HelperBean
                ahelperBean.OysterRunScriptName = Constants.ERMetrics
                ahelperBean.OysterRunTimeValue = Now.ToString
                OysterTestSuiteForm.list.Add(ahelperBean)
                Dim jarFile As String = Path.GetFileName(OysterTestSuiteForm.ERMetricsJarTxBx.Text)
                OysterTestSuiteForm.ERMetricsLoadedFile.Text = Constants.FileLoadedIs & "ERMetrics Run"
                pc.RunERMetrics(OysterTestSuiteForm.ERMetricsTxBx.Text, jarFile)

            Else
                MessageBox.Show(Constants.OysterWorkDirError, "Wrong Run File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As ArgumentException
            MessageBox.Show(ex.Message, "Error Running ERMetrics", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            pc = Nothing
        End Try

    End Sub

    'move the link file to the ERMetrics directory for use by ERMetrics process
    Public Sub MoveLinkFile()
        Try
            If Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetLinkFileTxbx.Text) Then
                If Not File.Exists(OysterTestSuiteForm.ERMetLinkFileTxbx.Text) Then
                    MessageBox.Show("File Does Not Exist", "Check  File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If Not String.IsNullOrEmpty(OysterTestSuiteForm.ERMetricsTxBx.Text) Then

                    If File.ReadAllText(OysterTestSuiteForm.ERMetLinkFileTxbx.Text).Length = 0 Then
                        MessageBox.Show(Constants.LinkFileEmpty, "File Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                    Dim fileToCopy As String = OysterTestSuiteForm.ERMetLinkFileTxbx.Text
                    Dim fileItSelf As String = Path.GetFileName(OysterTestSuiteForm.ERMetLinkFileTxbx.Text)
                    Dim checkFile As String = OysterTestSuiteForm.ERMetricsTxBx.Text + "/" + fileItSelf
                    '' Move the file.
                    File.Copy(fileToCopy, checkFile, True)

                    '    RunProcessTab(Constants.ERMetricsSetup)
                    MessageBox.Show(Constants.CheckERPropertiesFile, "Check Properties File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show(Constants.EmptyERMetricsDirectory, "Empty Direcytory", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show(Constants.EmptyLinkFileTextBox, "Link File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show(Constants.ErrorCopyingFile & " " & ex.Message, "Copy Link File Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub


    Public Sub SaveRichTextBoxFile(ByVal RichTextBoxKey As Integer, ByVal myRichTextBox As RichTextBox, ByVal FileToSave As String)
        Try
            Dim helperBean As New HelperBean
            helperBean = OysterTestSuiteForm.saveInfoCollection.Item(RichTextBoxKey)
            myRichTextBox.SaveFile(FileToSave, RichTextBoxStreamType.PlainText)
            myRichTextBox.ReadOnly = True

            MessageBox.Show("File " & FileToSave & " Saved!", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show("Error in Saving the File is " & ex.Message, "File Error On Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

End Class
