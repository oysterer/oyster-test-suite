﻿

Imports System.IO
''' <summary>
''' 
''' ProcessClass.vb
''' 
''' This class starts Python in a command window, passes the python script and the jar to 
''' run python and records the output to the run output tab.
''' OysterTestSuite writes the same information to an output file in the RunLogs directory
''' </summary>
''' 




Public Class ProcessClass

    Public Sub RunPython(ByVal list As String, ByVal helpBean As HelperBean)
        '    Console.WriteLine("start " & Now.ToShortTimeString)
        Dim p As Process = Nothing

        Try

            p = New Process
            OysterTestSuiteForm.RunRichTextBox.Clear()
            Dim info As New ProcessStartInfo With {
            .UseShellExecute = False,
            .CreateNoWindow = True,
            .WindowStyle = ProcessWindowStyle.Normal,
            .RedirectStandardError = True,
            .RedirectStandardInput = True,
            .RedirectStandardOutput = True,
            .WorkingDirectory = helpBean.OysterRootTestSuiteDir,
             .FileName = Constants.CmdWindow
          }
            '
            p = Process.Start(info)
            AddHandler p.OutputDataReceived, AddressOf OysterTestSuiteForm.ConsoleOutputPythonHandler
            AddHandler p.ErrorDataReceived, AddressOf OysterTestSuiteForm.ConsoleErrorPythonHandler
            p.BeginErrorReadLine()
            p.BeginOutputReadLine()

            p.StandardInput.WriteLine("cd " & helpBean.PythonExeDir)
            Dim runFile As String = helpBean.PythonRunFile ' "C:\OysterScoringRobot\OYSTER Robot.py"

            Dim sp As String = """" + runFile + """"
            '   p.StandardInput.WriteLine("Python " & sp & " " & list & " " & helpBean.OysterJarToRun)
            '   Dim pyRun As String = helpBean.PythonExeDir & "\Python"
            Dim pyRun As String = helpBean.PythonExeDir & "/Python"
            'pythonExe + "/python.exe " + '"' + runfile + '"' + " " + inputString + " " + oBean.getTestSuiteOysterToJarRun() + " " + runLog + " " + '"' + oysterDir + '"');
            'Console.WriteLine(helpBean.RunOutputLogFile)
            'Console.WriteLine(helpBean.OysterRootTestSuiteDir)

            '  p.StandardInput.WriteLine(pyRun & " " & sp & " " & list & " " & helpBean.OysterJarToRun & " " & "C:\OysterTestSuite\RunLogs\PythonRunfile.log" & " " & helpBean.OysterRootTestSuiteDir)
            p.StandardInput.WriteLine(pyRun & " " & sp & " " & list & " " & helpBean.OysterJarToRun & " " & "/OysterTestSuite/RunLogs/PythonRunfile.log" & " " & helpBean.OysterRootTestSuiteDir)
        Catch ex As Exception

            MessageBox.Show("Error in running Python is " & ex.Message, "Run Python Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            Try
                'If p IsNot Nothing Then
                '    p.Close()
                'End If
                'p = Nothing

                '       Console.WriteLine("end " & Now.ToShortTimeString)
            Catch ex As Exception
                Console.WriteLine(ex.StackTrace)
                MessageBox.Show("Error in Closing Process is " & ex.Message, "Run Python Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try

        End Try
    End Sub

    Public Sub RunERMetrics(ByVal path As String, ByVal jarToUse As String)

        Dim myPath As String = path
        ' Dim fileToRun As String = theFileToRun
        Dim jarForUse As String = jarToUse
        Dim oysterBoolean As Boolean = False

        Dim p As New Process

        Try
            'Oysterform.OysterTabs.SelectedTab = Oysterform.Run

            OysterTestSuiteForm.RichTextBox3.Clear()


            Dim info As New ProcessStartInfo With {
                .UseShellExecute = False,
                .CreateNoWindow = True,
                .WindowStyle = ProcessWindowStyle.Normal,
                .RedirectStandardError = True,
                .RedirectStandardInput = True,
                .RedirectStandardOutput = True,
                .WorkingDirectory = path,
                .FileName = Constants.CmdWindow
              }

            p = Process.Start(info)
            AddHandler p.OutputDataReceived, AddressOf OysterTestSuiteForm.ConsoleOutputHandler
            AddHandler p.ErrorDataReceived, AddressOf OysterTestSuiteForm.ConsoleErrorHandler
            p.BeginErrorReadLine()
            p.BeginOutputReadLine()
            p.StandardInput.WriteLine("cd " & myPath & " " & vbCrLf)



            p.StandardInput.WriteLine("java -jar " & jarForUse & " " & vbCrLf)

        Catch ex As Exception
            'Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            'Oysterform.ToolStripStatusLabel1.Visible = True
            'Oysterform.ToolStripProgressBar1.Visible = False
            'Oysterform.StopRun.Enabled = False


            MessageBox.Show("Error in running ERMetrics is " & ex.Message, "Run ERMetrics Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Try
                If p IsNot Nothing Then
                    p.Close()
                    p = Nothing
                End If
            Catch ex As Exception
                MessageBox.Show("Error in Closing Process is " & ex.Message, "Run ERMetrics Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            'Oysterform.ToolStripStatusLabel1.Visible = False
            'Oysterform.ToolStripProgressBar1.Visible = False
            'Oysterform.StopRun.Enabled = False

        End Try
    End Sub

End Class
