﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class OysterTestSuiteForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OysterTestSuiteForm))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangeOTSRootToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OysterTestSuiteTabs = New System.Windows.Forms.TabControl()
        Me.SetupTab = New System.Windows.Forms.TabPage()
        Me.KeepChkBox1 = New System.Windows.Forms.CheckBox()
        Me.SingleMode = New System.Windows.Forms.Button()
        Me.RefreshBtn = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TestSuiteXMLbl = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.OysterJarToRunLbl = New System.Windows.Forms.Label()
        Me.PythonRunFileLbl = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.PythonExeDirLbl = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TestSuiteLogFileLbl = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TestSuiteScriptsDirLbl = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.OysterRootTestSuiteDirLbl = New System.Windows.Forms.Label()
        Me.ScriptsLbl = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TestSuiteRootDirLbl = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.RunBtn = New System.Windows.Forms.Button()
        Me.LoadedFileLbl = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.EditFileBtn = New System.Windows.Forms.Button()
        Me.SaveFileBtn = New System.Windows.Forms.Button()
        Me.RichTextBox9 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CommentSelectedTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnCommentSelectedTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopySelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CutSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeView3 = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowFilePropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunOutput = New System.Windows.Forms.TabPage()
        Me.HomeBtn = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.StopRunBtn = New System.Windows.Forms.Button()
        Me.RunCheckRTB = New System.Windows.Forms.RichTextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.RunRichTextBox = New System.Windows.Forms.RichTextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.ERMetricsTab = New System.Windows.Forms.TabPage()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.OysterWkDirBtn = New System.Windows.Forms.Button()
        Me.LFDateLbl = New System.Windows.Forms.Label()
        Me.LinkFileDateERLbl = New System.Windows.Forms.Label()
        Me.ERMetricsLoadedFile = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.ERMetLinkFileTxbx = New System.Windows.Forms.TextBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.MoveFileBtn = New System.Windows.Forms.Button()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.ERMetricJarBtn = New System.Windows.Forms.Button()
        Me.ERMetricsJarTxBx = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.EROysterWkDirTxBx = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.EditFileERMetricsBtn = New System.Windows.Forms.Button()
        Me.SaveFileERMetricsBtn = New System.Windows.Forms.Button()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip3 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CommentSelectedTextToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UncommentSelectedTextToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeView4 = New System.Windows.Forms.TreeView()
        Me.InstructionERMetricsRTxbx = New System.Windows.Forms.RichTextBox()
        Me.SaveERMetricsBtn = New System.Windows.Forms.Button()
        Me.RunERMetricsBtn = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.ERMetricsWkDirBtn = New System.Windows.Forms.Button()
        Me.ERMetricsTxBx = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.RunHistory = New System.Windows.Forms.TabPage()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.FileGazer = New System.Windows.Forms.TabPage()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.RefreshOutput_Btn = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.SaveFile_btn = New System.Windows.Forms.Button()
        Me.Edit_OutputFileBtn = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.LogFileTreeView = New System.Windows.Forms.TreeView()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ExitBtn = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.GoHomeBtn = New System.Windows.Forms.Button()
        Me.Process1 = New System.Diagnostics.Process()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Process2 = New System.Diagnostics.Process()
        Me.Process3 = New System.Diagnostics.Process()
        Me.Process4 = New System.Diagnostics.Process()
        Me.Process5 = New System.Diagnostics.Process()
        Me.Process6 = New System.Diagnostics.Process()
        Me.Process7 = New System.Diagnostics.Process()
        Me.Process8 = New System.Diagnostics.Process()
        Me.Process9 = New System.Diagnostics.Process()
        Me.Process10 = New System.Diagnostics.Process()
        Me.Process11 = New System.Diagnostics.Process()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip.SuspendLayout()
        Me.OysterTestSuiteTabs.SuspendLayout()
        Me.SetupTab.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.RunOutput.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ERMetricsTab.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip3.SuspendLayout()
        Me.RunHistory.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FileGazer.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = Global.OysterTestSuite.My.Resources.Resources.ualr2
        Me.PictureBox1.Location = New System.Drawing.Point(1331, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(74, 80)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'MenuStrip
        '
        Me.MenuStrip.AutoSize = False
        Me.MenuStrip.BackColor = System.Drawing.Color.ForestGreen
        Me.MenuStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip.Size = New System.Drawing.Size(1450, 28)
        Me.MenuStrip.Stretch = False
        Me.MenuStrip.TabIndex = 3
        Me.MenuStrip.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen
        Me.FileToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem, Me.ChangeOTSRootToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.FileToolStripMenuItem.Text = "File  "
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen
        Me.ExitToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ExitToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(199, 24)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ChangeOTSRootToolStripMenuItem
        '
        Me.ChangeOTSRootToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen
        Me.ChangeOTSRootToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ChangeOTSRootToolStripMenuItem.Name = "ChangeOTSRootToolStripMenuItem"
        Me.ChangeOTSRootToolStripMenuItem.Size = New System.Drawing.Size(199, 24)
        Me.ChangeOTSRootToolStripMenuItem.Text = "Change OTS Root"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold)
        Me.HelpToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen
        Me.AboutToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(122, 24)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'OysterTestSuiteTabs
        '
        Me.OysterTestSuiteTabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OysterTestSuiteTabs.Controls.Add(Me.SetupTab)
        Me.OysterTestSuiteTabs.Controls.Add(Me.RunOutput)
        Me.OysterTestSuiteTabs.Controls.Add(Me.ERMetricsTab)
        Me.OysterTestSuiteTabs.Controls.Add(Me.RunHistory)
        Me.OysterTestSuiteTabs.Controls.Add(Me.FileGazer)
        Me.OysterTestSuiteTabs.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterTestSuiteTabs.Location = New System.Drawing.Point(0, 31)
        Me.OysterTestSuiteTabs.Name = "OysterTestSuiteTabs"
        Me.OysterTestSuiteTabs.SelectedIndex = 0
        Me.OysterTestSuiteTabs.Size = New System.Drawing.Size(1457, 609)
        Me.OysterTestSuiteTabs.TabIndex = 4
        '
        'SetupTab
        '
        Me.SetupTab.BackColor = System.Drawing.Color.MintCream
        Me.SetupTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SetupTab.Controls.Add(Me.KeepChkBox1)
        Me.SetupTab.Controls.Add(Me.SingleMode)
        Me.SetupTab.Controls.Add(Me.RefreshBtn)
        Me.SetupTab.Controls.Add(Me.Panel1)
        Me.SetupTab.Controls.Add(Me.Label7)
        Me.SetupTab.Controls.Add(Me.RunBtn)
        Me.SetupTab.Controls.Add(Me.LoadedFileLbl)
        Me.SetupTab.Controls.Add(Me.Label9)
        Me.SetupTab.Controls.Add(Me.Label11)
        Me.SetupTab.Controls.Add(Me.EditFileBtn)
        Me.SetupTab.Controls.Add(Me.SaveFileBtn)
        Me.SetupTab.Controls.Add(Me.RichTextBox9)
        Me.SetupTab.Controls.Add(Me.TreeView3)
        Me.SetupTab.Controls.Add(Me.PictureBox1)
        Me.SetupTab.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SetupTab.Location = New System.Drawing.Point(4, 29)
        Me.SetupTab.Name = "SetupTab"
        Me.SetupTab.Padding = New System.Windows.Forms.Padding(3)
        Me.SetupTab.Size = New System.Drawing.Size(1449, 576)
        Me.SetupTab.TabIndex = 0
        Me.SetupTab.Text = "Oyster Test Suite Setup"
        '
        'KeepChkBox1
        '
        Me.KeepChkBox1.AutoSize = True
        Me.KeepChkBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeepChkBox1.Location = New System.Drawing.Point(824, 540)
        Me.KeepChkBox1.Name = "KeepChkBox1"
        Me.KeepChkBox1.Size = New System.Drawing.Size(15, 14)
        Me.KeepChkBox1.TabIndex = 98
        Me.ToolTip1.SetToolTip(Me.KeepChkBox1, "Check to keep choice for more than one" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Run. Uncheck and Run again to Clear.")
        Me.KeepChkBox1.UseVisualStyleBackColor = True
        '
        'SingleMode
        '
        Me.SingleMode.Location = New System.Drawing.Point(745, 532)
        Me.SingleMode.Name = "SingleMode"
        Me.SingleMode.Size = New System.Drawing.Size(70, 30)
        Me.SingleMode.TabIndex = 97
        Me.SingleMode.Text = "Single"
        Me.ToolTip1.SetToolTip(Me.SingleMode, "Choose a Single Test to Run")
        Me.SingleMode.UseVisualStyleBackColor = True
        '
        'RefreshBtn
        '
        Me.RefreshBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RefreshBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshBtn.Location = New System.Drawing.Point(156, 531)
        Me.RefreshBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RefreshBtn.Name = "RefreshBtn"
        Me.RefreshBtn.Size = New System.Drawing.Size(76, 30)
        Me.RefreshBtn.TabIndex = 96
        Me.RefreshBtn.Text = "Refresh"
        Me.RefreshBtn.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.ForestGreen
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.TestSuiteXMLbl)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.OysterJarToRunLbl)
        Me.Panel1.Controls.Add(Me.PythonRunFileLbl)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.PythonExeDirLbl)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TestSuiteLogFileLbl)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.TestSuiteScriptsDirLbl)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.OysterRootTestSuiteDirLbl)
        Me.Panel1.Controls.Add(Me.ScriptsLbl)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TestSuiteRootDirLbl)
        Me.Panel1.Location = New System.Drawing.Point(16, 117)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(357, 400)
        Me.Panel1.TabIndex = 95
        '
        'TestSuiteXMLbl
        '
        Me.TestSuiteXMLbl.AutoSize = True
        Me.TestSuiteXMLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TestSuiteXMLbl.ForeColor = System.Drawing.Color.White
        Me.TestSuiteXMLbl.Location = New System.Drawing.Point(15, 356)
        Me.TestSuiteXMLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TestSuiteXMLbl.Name = "TestSuiteXMLbl"
        Me.TestSuiteXMLbl.Size = New System.Drawing.Size(55, 18)
        Me.TestSuiteXMLbl.TabIndex = 97
        Me.TestSuiteXMLbl.Text = "Empty"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label25.Location = New System.Drawing.Point(37, 377)
        Me.Label25.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(122, 16)
        Me.Label25.TabIndex = 96
        Me.Label25.Text = "Test Suite XML File"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(76, 4)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(201, 20)
        Me.Label12.TabIndex = 95
        Me.Label12.Text = "Test Suite Run Settings"
        '
        'OysterJarToRunLbl
        '
        Me.OysterJarToRunLbl.AutoSize = True
        Me.OysterJarToRunLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterJarToRunLbl.ForeColor = System.Drawing.Color.White
        Me.OysterJarToRunLbl.Location = New System.Drawing.Point(15, 125)
        Me.OysterJarToRunLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.OysterJarToRunLbl.Name = "OysterJarToRunLbl"
        Me.OysterJarToRunLbl.Size = New System.Drawing.Size(55, 18)
        Me.OysterJarToRunLbl.TabIndex = 90
        Me.OysterJarToRunLbl.Text = "Empty"
        '
        'PythonRunFileLbl
        '
        Me.PythonRunFileLbl.AutoSize = True
        Me.PythonRunFileLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PythonRunFileLbl.ForeColor = System.Drawing.Color.White
        Me.PythonRunFileLbl.Location = New System.Drawing.Point(15, 309)
        Me.PythonRunFileLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.PythonRunFileLbl.Name = "PythonRunFileLbl"
        Me.PythonRunFileLbl.Size = New System.Drawing.Size(55, 18)
        Me.PythonRunFileLbl.TabIndex = 94
        Me.PythonRunFileLbl.Text = "Empty"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label17.Location = New System.Drawing.Point(37, 54)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(157, 16)
        Me.Label17.TabIndex = 59
        Me.Label17.Text = "Test Suite Root Directory"
        '
        'PythonExeDirLbl
        '
        Me.PythonExeDirLbl.AutoSize = True
        Me.PythonExeDirLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PythonExeDirLbl.ForeColor = System.Drawing.Color.White
        Me.PythonExeDirLbl.Location = New System.Drawing.Point(15, 263)
        Me.PythonExeDirLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.PythonExeDirLbl.Name = "PythonExeDirLbl"
        Me.PythonExeDirLbl.Size = New System.Drawing.Size(55, 18)
        Me.PythonExeDirLbl.TabIndex = 93
        Me.PythonExeDirLbl.Text = "Empty"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label1.Location = New System.Drawing.Point(37, 100)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(199, 16)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Test Suite Oyster Root Directory"
        '
        'TestSuiteLogFileLbl
        '
        Me.TestSuiteLogFileLbl.AutoSize = True
        Me.TestSuiteLogFileLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TestSuiteLogFileLbl.ForeColor = System.Drawing.Color.White
        Me.TestSuiteLogFileLbl.Location = New System.Drawing.Point(15, 217)
        Me.TestSuiteLogFileLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TestSuiteLogFileLbl.Name = "TestSuiteLogFileLbl"
        Me.TestSuiteLogFileLbl.Size = New System.Drawing.Size(55, 18)
        Me.TestSuiteLogFileLbl.TabIndex = 92
        Me.TestSuiteLogFileLbl.Text = "Empty"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label2.Location = New System.Drawing.Point(37, 146)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 16)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "Oyster Jar To Run"
        '
        'TestSuiteScriptsDirLbl
        '
        Me.TestSuiteScriptsDirLbl.AutoSize = True
        Me.TestSuiteScriptsDirLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TestSuiteScriptsDirLbl.ForeColor = System.Drawing.Color.White
        Me.TestSuiteScriptsDirLbl.Location = New System.Drawing.Point(15, 171)
        Me.TestSuiteScriptsDirLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TestSuiteScriptsDirLbl.Name = "TestSuiteScriptsDirLbl"
        Me.TestSuiteScriptsDirLbl.Size = New System.Drawing.Size(55, 18)
        Me.TestSuiteScriptsDirLbl.TabIndex = 91
        Me.TestSuiteScriptsDirLbl.Text = "Empty"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label4.Location = New System.Drawing.Point(37, 284)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 16)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "Python Root Directory"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label3.Location = New System.Drawing.Point(37, 330)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 16)
        Me.Label3.TabIndex = 82
        Me.Label3.Text = "Python Run File"
        '
        'OysterRootTestSuiteDirLbl
        '
        Me.OysterRootTestSuiteDirLbl.AutoSize = True
        Me.OysterRootTestSuiteDirLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterRootTestSuiteDirLbl.ForeColor = System.Drawing.Color.White
        Me.OysterRootTestSuiteDirLbl.Location = New System.Drawing.Point(15, 79)
        Me.OysterRootTestSuiteDirLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.OysterRootTestSuiteDirLbl.Name = "OysterRootTestSuiteDirLbl"
        Me.OysterRootTestSuiteDirLbl.Size = New System.Drawing.Size(55, 18)
        Me.OysterRootTestSuiteDirLbl.TabIndex = 89
        Me.OysterRootTestSuiteDirLbl.Text = "Empty"
        '
        'ScriptsLbl
        '
        Me.ScriptsLbl.AutoSize = True
        Me.ScriptsLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ScriptsLbl.ForeColor = System.Drawing.Color.SpringGreen
        Me.ScriptsLbl.Location = New System.Drawing.Point(37, 192)
        Me.ScriptsLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ScriptsLbl.Name = "ScriptsLbl"
        Me.ScriptsLbl.Size = New System.Drawing.Size(169, 16)
        Me.ScriptsLbl.TabIndex = 84
        Me.ScriptsLbl.Text = "Test Suite Scripts Directory"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.SpringGreen
        Me.Label6.Location = New System.Drawing.Point(38, 238)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 16)
        Me.Label6.TabIndex = 86
        Me.Label6.Text = "Test Suite Log File"
        '
        'TestSuiteRootDirLbl
        '
        Me.TestSuiteRootDirLbl.AutoSize = True
        Me.TestSuiteRootDirLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TestSuiteRootDirLbl.ForeColor = System.Drawing.Color.White
        Me.TestSuiteRootDirLbl.Location = New System.Drawing.Point(15, 33)
        Me.TestSuiteRootDirLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TestSuiteRootDirLbl.Name = "TestSuiteRootDirLbl"
        Me.TestSuiteRootDirLbl.Size = New System.Drawing.Size(55, 18)
        Me.TestSuiteRootDirLbl.TabIndex = 87
        Me.TestSuiteRootDirLbl.Text = "Empty"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label7.Location = New System.Drawing.Point(13, 14)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(378, 90)
        Me.Label7.TabIndex = 88
        Me.Label7.Text = resources.GetString("Label7.Text")
        '
        'RunBtn
        '
        Me.RunBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RunBtn.Location = New System.Drawing.Point(1307, 532)
        Me.RunBtn.Name = "RunBtn"
        Me.RunBtn.Size = New System.Drawing.Size(70, 30)
        Me.RunBtn.TabIndex = 73
        Me.RunBtn.Text = "Run"
        Me.RunBtn.UseVisualStyleBackColor = True
        '
        'LoadedFileLbl
        '
        Me.LoadedFileLbl.AutoSize = True
        Me.LoadedFileLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoadedFileLbl.ForeColor = System.Drawing.Color.Maroon
        Me.LoadedFileLbl.Location = New System.Drawing.Point(716, 86)
        Me.LoadedFileLbl.Name = "LoadedFileLbl"
        Me.LoadedFileLbl.Size = New System.Drawing.Size(118, 18)
        Me.LoadedFileLbl.TabIndex = 68
        Me.LoadedFileLbl.Text = "Loaded file is: "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Maroon
        Me.Label9.Location = New System.Drawing.Point(409, 529)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(235, 18)
        Me.Label9.TabIndex = 67
        Me.Label9.Text = "Double Click a File To View or Edit"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Maroon
        Me.Label11.Location = New System.Drawing.Point(434, 89)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(186, 18)
        Me.Label11.TabIndex = 66
        Me.Label11.Text = "Working Directory Files"
        '
        'EditFileBtn
        '
        Me.EditFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditFileBtn.Location = New System.Drawing.Point(1004, 532)
        Me.EditFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.EditFileBtn.Name = "EditFileBtn"
        Me.EditFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.EditFileBtn.TabIndex = 64
        Me.EditFileBtn.Text = "Edit"
        Me.EditFileBtn.UseVisualStyleBackColor = False
        '
        'SaveFileBtn
        '
        Me.SaveFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveFileBtn.Location = New System.Drawing.Point(1151, 532)
        Me.SaveFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveFileBtn.Name = "SaveFileBtn"
        Me.SaveFileBtn.Size = New System.Drawing.Size(70, 30)
        Me.SaveFileBtn.TabIndex = 65
        Me.SaveFileBtn.Text = "Save"
        Me.SaveFileBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox9
        '
        Me.RichTextBox9.AcceptsTab = True
        Me.RichTextBox9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RichTextBox9.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox9.ContextMenuStrip = Me.ContextMenuStrip1
        Me.RichTextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox9.Location = New System.Drawing.Point(719, 117)
        Me.RichTextBox9.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox9.Name = "RichTextBox9"
        Me.RichTextBox9.Size = New System.Drawing.Size(686, 400)
        Me.RichTextBox9.TabIndex = 62
        Me.RichTextBox9.Text = ""
        Me.RichTextBox9.WordWrap = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentSelectedTextToolStripMenuItem, Me.UnCommentSelectedTextToolStripMenuItem, Me.CopySelectionToolStripMenuItem, Me.PasteSelectionToolStripMenuItem, Me.CutSelectionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(213, 114)
        '
        'CommentSelectedTextToolStripMenuItem
        '
        Me.CommentSelectedTextToolStripMenuItem.Name = "CommentSelectedTextToolStripMenuItem"
        Me.CommentSelectedTextToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CommentSelectedTextToolStripMenuItem.Text = "Comment Selected Text"
        '
        'UnCommentSelectedTextToolStripMenuItem
        '
        Me.UnCommentSelectedTextToolStripMenuItem.Name = "UnCommentSelectedTextToolStripMenuItem"
        Me.UnCommentSelectedTextToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.UnCommentSelectedTextToolStripMenuItem.Text = "Uncomment Selected Text"
        '
        'CopySelectionToolStripMenuItem
        '
        Me.CopySelectionToolStripMenuItem.Name = "CopySelectionToolStripMenuItem"
        Me.CopySelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CopySelectionToolStripMenuItem.Text = "Copy Selection"
        '
        'PasteSelectionToolStripMenuItem
        '
        Me.PasteSelectionToolStripMenuItem.Name = "PasteSelectionToolStripMenuItem"
        Me.PasteSelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.PasteSelectionToolStripMenuItem.Text = "Paste Selection"
        '
        'CutSelectionToolStripMenuItem
        '
        Me.CutSelectionToolStripMenuItem.Name = "CutSelectionToolStripMenuItem"
        Me.CutSelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CutSelectionToolStripMenuItem.Text = "Cut Selection"
        '
        'TreeView3
        '
        Me.TreeView3.ContextMenuStrip = Me.ContextMenuStrip2
        Me.TreeView3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView3.Location = New System.Drawing.Point(412, 117)
        Me.TreeView3.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView3.Name = "TreeView3"
        Me.TreeView3.Size = New System.Drawing.Size(263, 400)
        Me.TreeView3.TabIndex = 63
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.DeleteSelectedFileToolStripMenuItem, Me.ShowFilePropertiesToolStripMenuItem, Me.RenameFileToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(217, 92)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem1.Text = "Make Copy of Selected File"
        '
        'DeleteSelectedFileToolStripMenuItem
        '
        Me.DeleteSelectedFileToolStripMenuItem.Name = "DeleteSelectedFileToolStripMenuItem"
        Me.DeleteSelectedFileToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.DeleteSelectedFileToolStripMenuItem.Text = "Delete Selected File"
        '
        'ShowFilePropertiesToolStripMenuItem
        '
        Me.ShowFilePropertiesToolStripMenuItem.Name = "ShowFilePropertiesToolStripMenuItem"
        Me.ShowFilePropertiesToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ShowFilePropertiesToolStripMenuItem.Text = "Show File Size"
        '
        'RenameFileToolStripMenuItem
        '
        Me.RenameFileToolStripMenuItem.Name = "RenameFileToolStripMenuItem"
        Me.RenameFileToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.RenameFileToolStripMenuItem.Text = "Rename File"
        '
        'RunOutput
        '
        Me.RunOutput.BackColor = System.Drawing.Color.MintCream
        Me.RunOutput.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RunOutput.Controls.Add(Me.HomeBtn)
        Me.RunOutput.Controls.Add(Me.Label5)
        Me.RunOutput.Controls.Add(Me.StopRunBtn)
        Me.RunOutput.Controls.Add(Me.RunCheckRTB)
        Me.RunOutput.Controls.Add(Me.Label21)
        Me.RunOutput.Controls.Add(Me.RunRichTextBox)
        Me.RunOutput.Controls.Add(Me.PictureBox2)
        Me.RunOutput.Location = New System.Drawing.Point(4, 29)
        Me.RunOutput.Name = "RunOutput"
        Me.RunOutput.Padding = New System.Windows.Forms.Padding(3)
        Me.RunOutput.Size = New System.Drawing.Size(1449, 576)
        Me.RunOutput.TabIndex = 1
        Me.RunOutput.Text = "Run Output"
        '
        'HomeBtn
        '
        Me.HomeBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HomeBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HomeBtn.Location = New System.Drawing.Point(1307, 532)
        Me.HomeBtn.Name = "HomeBtn"
        Me.HomeBtn.Size = New System.Drawing.Size(70, 30)
        Me.HomeBtn.TabIndex = 87
        Me.HomeBtn.Text = "History"
        Me.HomeBtn.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Maroon
        Me.Label5.Location = New System.Drawing.Point(988, 74)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(190, 25)
        Me.Label5.TabIndex = 86
        Me.Label5.Text = "Run Issues Output"
        '
        'StopRunBtn
        '
        Me.StopRunBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StopRunBtn.Location = New System.Drawing.Point(322, 535)
        Me.StopRunBtn.Name = "StopRunBtn"
        Me.StopRunBtn.Size = New System.Drawing.Size(80, 30)
        Me.StopRunBtn.TabIndex = 8
        Me.StopRunBtn.Text = "Stop Run"
        Me.StopRunBtn.UseVisualStyleBackColor = True
        '
        'RunCheckRTB
        '
        Me.RunCheckRTB.Location = New System.Drawing.Point(735, 117)
        Me.RunCheckRTB.Margin = New System.Windows.Forms.Padding(2)
        Me.RunCheckRTB.Name = "RunCheckRTB"
        Me.RunCheckRTB.Size = New System.Drawing.Size(670, 402)
        Me.RunCheckRTB.TabIndex = 85
        Me.RunCheckRTB.Text = ""
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Maroon
        Me.Label21.Location = New System.Drawing.Point(267, 74)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(191, 25)
        Me.Label21.TabIndex = 84
        Me.Label21.Text = "Oyster Run Output"
        '
        'RunRichTextBox
        '
        Me.RunRichTextBox.Location = New System.Drawing.Point(14, 117)
        Me.RunRichTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.RunRichTextBox.Name = "RunRichTextBox"
        Me.RunRichTextBox.Size = New System.Drawing.Size(697, 402)
        Me.RunRichTextBox.TabIndex = 83
        Me.RunRichTextBox.Text = ""
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.OysterTestSuite.My.Resources.Resources.ualr2
        Me.PictureBox2.Location = New System.Drawing.Point(1331, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(74, 80)
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'ERMetricsTab
        '
        Me.ERMetricsTab.BackColor = System.Drawing.Color.AliceBlue
        Me.ERMetricsTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ERMetricsTab.Controls.Add(Me.Label34)
        Me.ERMetricsTab.Controls.Add(Me.OysterWkDirBtn)
        Me.ERMetricsTab.Controls.Add(Me.LFDateLbl)
        Me.ERMetricsTab.Controls.Add(Me.LinkFileDateERLbl)
        Me.ERMetricsTab.Controls.Add(Me.ERMetricsLoadedFile)
        Me.ERMetricsTab.Controls.Add(Me.Label24)
        Me.ERMetricsTab.Controls.Add(Me.Label26)
        Me.ERMetricsTab.Controls.Add(Me.Label27)
        Me.ERMetricsTab.Controls.Add(Me.Label28)
        Me.ERMetricsTab.Controls.Add(Me.ERMetLinkFileTxbx)
        Me.ERMetricsTab.Controls.Add(Me.PictureBox5)
        Me.ERMetricsTab.Controls.Add(Me.MoveFileBtn)
        Me.ERMetricsTab.Controls.Add(Me.Label29)
        Me.ERMetricsTab.Controls.Add(Me.ERMetricJarBtn)
        Me.ERMetricsTab.Controls.Add(Me.ERMetricsJarTxBx)
        Me.ERMetricsTab.Controls.Add(Me.Label30)
        Me.ERMetricsTab.Controls.Add(Me.EROysterWkDirTxBx)
        Me.ERMetricsTab.Controls.Add(Me.Label31)
        Me.ERMetricsTab.Controls.Add(Me.EditFileERMetricsBtn)
        Me.ERMetricsTab.Controls.Add(Me.SaveFileERMetricsBtn)
        Me.ERMetricsTab.Controls.Add(Me.RichTextBox3)
        Me.ERMetricsTab.Controls.Add(Me.TreeView4)
        Me.ERMetricsTab.Controls.Add(Me.InstructionERMetricsRTxbx)
        Me.ERMetricsTab.Controls.Add(Me.SaveERMetricsBtn)
        Me.ERMetricsTab.Controls.Add(Me.RunERMetricsBtn)
        Me.ERMetricsTab.Controls.Add(Me.Label32)
        Me.ERMetricsTab.Controls.Add(Me.ERMetricsWkDirBtn)
        Me.ERMetricsTab.Controls.Add(Me.ERMetricsTxBx)
        Me.ERMetricsTab.Controls.Add(Me.Label33)
        Me.ERMetricsTab.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ERMetricsTab.Location = New System.Drawing.Point(4, 29)
        Me.ERMetricsTab.Name = "ERMetricsTab"
        Me.ERMetricsTab.Size = New System.Drawing.Size(1449, 576)
        Me.ERMetricsTab.TabIndex = 4
        Me.ERMetricsTab.Text = "ER-Metrics Setup & Run"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.Maroon
        Me.Label34.Location = New System.Drawing.Point(41, 342)
        Me.Label34.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(308, 15)
        Me.Label34.TabIndex = 115
        Me.Label34.Text = "Example: C:\OysterTestSuite\Oyster\BlockingGoodIndex"
        '
        'OysterWkDirBtn
        '
        Me.OysterWkDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.OysterWkDirBtn.Location = New System.Drawing.Point(399, 312)
        Me.OysterWkDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterWkDirBtn.Name = "OysterWkDirBtn"
        Me.OysterWkDirBtn.Size = New System.Drawing.Size(70, 30)
        Me.OysterWkDirBtn.TabIndex = 114
        Me.OysterWkDirBtn.Text = "Browse"
        Me.OysterWkDirBtn.UseVisualStyleBackColor = False
        '
        'LFDateLbl
        '
        Me.LFDateLbl.AutoSize = True
        Me.LFDateLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LFDateLbl.ForeColor = System.Drawing.Color.Maroon
        Me.LFDateLbl.Location = New System.Drawing.Point(139, 409)
        Me.LFDateLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LFDateLbl.Name = "LFDateLbl"
        Me.LFDateLbl.Size = New System.Drawing.Size(33, 15)
        Me.LFDateLbl.TabIndex = 113
        Me.LFDateLbl.Text = "Date"
        '
        'LinkFileDateERLbl
        '
        Me.LinkFileDateERLbl.AutoSize = True
        Me.LinkFileDateERLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkFileDateERLbl.ForeColor = System.Drawing.Color.Maroon
        Me.LinkFileDateERLbl.Location = New System.Drawing.Point(50, 409)
        Me.LinkFileDateERLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LinkFileDateERLbl.Name = "LinkFileDateERLbl"
        Me.LinkFileDateERLbl.Size = New System.Drawing.Size(85, 15)
        Me.LinkFileDateERLbl.TabIndex = 112
        Me.LinkFileDateERLbl.Text = "Link File Date:"
        '
        'ERMetricsLoadedFile
        '
        Me.ERMetricsLoadedFile.AutoSize = True
        Me.ERMetricsLoadedFile.ForeColor = System.Drawing.Color.Maroon
        Me.ERMetricsLoadedFile.Location = New System.Drawing.Point(775, 125)
        Me.ERMetricsLoadedFile.Name = "ERMetricsLoadedFile"
        Me.ERMetricsLoadedFile.Size = New System.Drawing.Size(94, 16)
        Me.ERMetricsLoadedFile.TabIndex = 111
        Me.ERMetricsLoadedFile.Text = "Loaded file is: "
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Maroon
        Me.Label24.Location = New System.Drawing.Point(908, 66)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(156, 31)
        Me.Label24.TabIndex = 110
        Me.Label24.Text = "ER-Metrics "
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Maroon
        Me.Label26.Location = New System.Drawing.Point(513, 513)
        Me.Label26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(223, 17)
        Me.Label26.TabIndex = 109
        Me.Label26.Text = "Double Click a File To View or Edit"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Maroon
        Me.Label27.Location = New System.Drawing.Point(547, 124)
        Me.Label27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(154, 17)
        Me.Label27.TabIndex = 108
        Me.Label27.Text = "Working Directory Files"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(37, 363)
        Me.Label28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(203, 17)
        Me.Label28.TabIndex = 107
        Me.Label28.Text = "Oyster Link File From Last Run"
        '
        'ERMetLinkFileTxbx
        '
        Me.ERMetLinkFileTxbx.Location = New System.Drawing.Point(39, 382)
        Me.ERMetLinkFileTxbx.Name = "ERMetLinkFileTxbx"
        Me.ERMetLinkFileTxbx.ReadOnly = True
        Me.ERMetLinkFileTxbx.Size = New System.Drawing.Size(340, 22)
        Me.ERMetLinkFileTxbx.TabIndex = 106
        '
        'PictureBox5
        '
        Me.PictureBox5.ErrorImage = CType(resources.GetObject("PictureBox5.ErrorImage"), System.Drawing.Image)
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(1331, 14)
        Me.PictureBox5.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox5.TabIndex = 105
        Me.PictureBox5.TabStop = False
        '
        'MoveFileBtn
        '
        Me.MoveFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.MoveFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoveFileBtn.Location = New System.Drawing.Point(399, 378)
        Me.MoveFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.MoveFileBtn.Name = "MoveFileBtn"
        Me.MoveFileBtn.Size = New System.Drawing.Size(70, 30)
        Me.MoveFileBtn.TabIndex = 104
        Me.MoveFileBtn.Text = "Copy"
        Me.ToolTip1.SetToolTip(Me.MoveFileBtn, "Copy Link File " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "To ERMetrics" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Directory")
        Me.MoveFileBtn.UseVisualStyleBackColor = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Maroon
        Me.Label29.Location = New System.Drawing.Point(42, 275)
        Me.Label29.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(311, 15)
        Me.Label29.TabIndex = 103
        Me.Label29.Text = "Example: C:\OysterTestSuite\ERCalculator\er-metrics.jar"
        '
        'ERMetricJarBtn
        '
        Me.ERMetricJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ERMetricJarBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ERMetricJarBtn.Location = New System.Drawing.Point(399, 245)
        Me.ERMetricJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricJarBtn.Name = "ERMetricJarBtn"
        Me.ERMetricJarBtn.Size = New System.Drawing.Size(70, 30)
        Me.ERMetricJarBtn.TabIndex = 102
        Me.ERMetricJarBtn.Text = "Browse"
        Me.ERMetricJarBtn.UseVisualStyleBackColor = False
        '
        'ERMetricsJarTxBx
        '
        Me.ERMetricsJarTxBx.Location = New System.Drawing.Point(39, 249)
        Me.ERMetricsJarTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsJarTxBx.Name = "ERMetricsJarTxBx"
        Me.ERMetricsJarTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.ERMetricsJarTxBx.Size = New System.Drawing.Size(340, 22)
        Me.ERMetricsJarTxBx.TabIndex = 100
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(37, 231)
        Me.Label30.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(126, 17)
        Me.Label30.TabIndex = 101
        Me.Label30.Text = "ERMetrics Jar File:"
        '
        'EROysterWkDirTxBx
        '
        Me.EROysterWkDirTxBx.Location = New System.Drawing.Point(39, 316)
        Me.EROysterWkDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.EROysterWkDirTxBx.Name = "EROysterWkDirTxBx"
        Me.EROysterWkDirTxBx.Size = New System.Drawing.Size(340, 22)
        Me.EROysterWkDirTxBx.TabIndex = 98
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(37, 295)
        Me.Label31.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(171, 17)
        Me.Label31.TabIndex = 99
        Me.Label31.Text = "Oyster Working Directory:"
        '
        'EditFileERMetricsBtn
        '
        Me.EditFileERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditFileERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditFileERMetricsBtn.Location = New System.Drawing.Point(1151, 532)
        Me.EditFileERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.EditFileERMetricsBtn.Name = "EditFileERMetricsBtn"
        Me.EditFileERMetricsBtn.Size = New System.Drawing.Size(70, 30)
        Me.EditFileERMetricsBtn.TabIndex = 97
        Me.EditFileERMetricsBtn.Text = "Edit"
        Me.ToolTip1.SetToolTip(Me.EditFileERMetricsBtn, "Edit Loaded File")
        Me.EditFileERMetricsBtn.UseVisualStyleBackColor = False
        '
        'SaveFileERMetricsBtn
        '
        Me.SaveFileERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveFileERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveFileERMetricsBtn.Location = New System.Drawing.Point(1307, 532)
        Me.SaveFileERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveFileERMetricsBtn.Name = "SaveFileERMetricsBtn"
        Me.SaveFileERMetricsBtn.Size = New System.Drawing.Size(70, 30)
        Me.SaveFileERMetricsBtn.TabIndex = 96
        Me.SaveFileERMetricsBtn.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveFileERMetricsBtn, "Save the Loaded File")
        Me.SaveFileERMetricsBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox3.ContextMenuStrip = Me.ContextMenuStrip3
        Me.RichTextBox3.Location = New System.Drawing.Point(778, 146)
        Me.RichTextBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.Size = New System.Drawing.Size(627, 361)
        Me.RichTextBox3.TabIndex = 95
        Me.RichTextBox3.Text = ""
        Me.ToolTip1.SetToolTip(Me.RichTextBox3, "Right Click for Menus")
        Me.RichTextBox3.WordWrap = False
        '
        'ContextMenuStrip3
        '
        Me.ContextMenuStrip3.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentSelectedTextToolStripMenuItem1, Me.UncommentSelectedTextToolStripMenuItem1})
        Me.ContextMenuStrip3.Name = "ContextMenuStrip3"
        Me.ContextMenuStrip3.Size = New System.Drawing.Size(213, 48)
        '
        'CommentSelectedTextToolStripMenuItem1
        '
        Me.CommentSelectedTextToolStripMenuItem1.Name = "CommentSelectedTextToolStripMenuItem1"
        Me.CommentSelectedTextToolStripMenuItem1.Size = New System.Drawing.Size(212, 22)
        Me.CommentSelectedTextToolStripMenuItem1.Text = "Comment Selected Text"
        '
        'UncommentSelectedTextToolStripMenuItem1
        '
        Me.UncommentSelectedTextToolStripMenuItem1.Name = "UncommentSelectedTextToolStripMenuItem1"
        Me.UncommentSelectedTextToolStripMenuItem1.Size = New System.Drawing.Size(212, 22)
        Me.UncommentSelectedTextToolStripMenuItem1.Text = "Uncomment Selected Text"
        '
        'TreeView4
        '
        Me.TreeView4.Location = New System.Drawing.Point(484, 146)
        Me.TreeView4.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView4.Name = "TreeView4"
        Me.TreeView4.Size = New System.Drawing.Size(281, 361)
        Me.TreeView4.TabIndex = 94
        Me.ToolTip1.SetToolTip(Me.TreeView4, "Right Click for Menus")
        '
        'InstructionERMetricsRTxbx
        '
        Me.InstructionERMetricsRTxbx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionERMetricsRTxbx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionERMetricsRTxbx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstructionERMetricsRTxbx.Location = New System.Drawing.Point(45, 28)
        Me.InstructionERMetricsRTxbx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionERMetricsRTxbx.Name = "InstructionERMetricsRTxbx"
        Me.InstructionERMetricsRTxbx.ReadOnly = True
        Me.InstructionERMetricsRTxbx.Size = New System.Drawing.Size(792, 94)
        Me.InstructionERMetricsRTxbx.TabIndex = 93
        Me.InstructionERMetricsRTxbx.Text = resources.GetString("InstructionERMetricsRTxbx.Text")
        '
        'SaveERMetricsBtn
        '
        Me.SaveERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveERMetricsBtn.Location = New System.Drawing.Point(309, 440)
        Me.SaveERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveERMetricsBtn.Name = "SaveERMetricsBtn"
        Me.SaveERMetricsBtn.Size = New System.Drawing.Size(70, 30)
        Me.SaveERMetricsBtn.TabIndex = 92
        Me.SaveERMetricsBtn.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveERMetricsBtn, "Save Your" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Program Choices")
        Me.SaveERMetricsBtn.UseVisualStyleBackColor = False
        '
        'RunERMetricsBtn
        '
        Me.RunERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RunERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RunERMetricsBtn.ForeColor = System.Drawing.Color.Maroon
        Me.RunERMetricsBtn.Location = New System.Drawing.Point(399, 440)
        Me.RunERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RunERMetricsBtn.Name = "RunERMetricsBtn"
        Me.RunERMetricsBtn.Size = New System.Drawing.Size(70, 30)
        Me.RunERMetricsBtn.TabIndex = 91
        Me.RunERMetricsBtn.Text = "Run"
        Me.ToolTip1.SetToolTip(Me.RunERMetricsBtn, "Run ERMetrics")
        Me.RunERMetricsBtn.UseVisualStyleBackColor = False
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Maroon
        Me.Label32.Location = New System.Drawing.Point(41, 214)
        Me.Label32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(243, 15)
        Me.Label32.TabIndex = 90
        Me.Label32.Text = "Example: C:\OysterTestSuite\ER_Calculator"
        '
        'ERMetricsWkDirBtn
        '
        Me.ERMetricsWkDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ERMetricsWkDirBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ERMetricsWkDirBtn.Location = New System.Drawing.Point(399, 183)
        Me.ERMetricsWkDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsWkDirBtn.Name = "ERMetricsWkDirBtn"
        Me.ERMetricsWkDirBtn.Size = New System.Drawing.Size(70, 30)
        Me.ERMetricsWkDirBtn.TabIndex = 89
        Me.ERMetricsWkDirBtn.Text = "Browse"
        Me.ERMetricsWkDirBtn.UseVisualStyleBackColor = False
        '
        'ERMetricsTxBx
        '
        Me.ERMetricsTxBx.Location = New System.Drawing.Point(39, 188)
        Me.ERMetricsTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsTxBx.Name = "ERMetricsTxBx"
        Me.ERMetricsTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.ERMetricsTxBx.Size = New System.Drawing.Size(340, 22)
        Me.ERMetricsTxBx.TabIndex = 88
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(37, 169)
        Me.Label33.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(189, 17)
        Me.Label33.TabIndex = 87
        Me.Label33.Text = "ERMetrics Working Directory"
        '
        'RunHistory
        '
        Me.RunHistory.BackColor = System.Drawing.Color.ForestGreen
        Me.RunHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RunHistory.Controls.Add(Me.Label14)
        Me.RunHistory.Controls.Add(Me.Label13)
        Me.RunHistory.Controls.Add(Me.Panel2)
        Me.RunHistory.Controls.Add(Me.PictureBox4)
        Me.RunHistory.Controls.Add(Me.DataGridView1)
        Me.RunHistory.Location = New System.Drawing.Point(4, 29)
        Me.RunHistory.Name = "RunHistory"
        Me.RunHistory.Size = New System.Drawing.Size(1449, 576)
        Me.RunHistory.TabIndex = 3
        Me.RunHistory.Text = "Test Suite Run History"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(841, 68)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(283, 24)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "Oyster Test Suite Run History"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(165, 68)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(200, 24)
        Me.Label13.TabIndex = 5
        Me.Label13.Text = "Last Run Information"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Honeydew
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.RichTextBox2)
        Me.Panel2.Controls.Add(Me.ListBox1)
        Me.Panel2.Location = New System.Drawing.Point(21, 111)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(489, 446)
        Me.Panel2.TabIndex = 4
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.Maroon
        Me.Label22.Location = New System.Drawing.Point(267, 171)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(35, 20)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Jar:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.Maroon
        Me.Label20.Location = New System.Drawing.Point(267, 47)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(130, 20)
        Me.Label20.TabIndex = 6
        Me.Label20.Text = "Number of Tests:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.Maroon
        Me.Label19.Location = New System.Drawing.Point(267, 90)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(66, 20)
        Me.Label19.TabIndex = 5
        Me.Label19.Text = "Passed:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Maroon
        Me.Label18.Location = New System.Drawing.Point(267, 132)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 20)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Failed:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(42, 179)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(192, 20)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Double Click a Failed Test"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Maroon
        Me.Label15.Location = New System.Drawing.Point(15, 17)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 20)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Run Date Time:"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Location = New System.Drawing.Point(15, 235)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(457, 197)
        Me.RichTextBox2.TabIndex = 1
        Me.RichTextBox2.Text = ""
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 20
        Me.ListBox1.Location = New System.Drawing.Point(15, 47)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(246, 124)
        Me.ListBox1.TabIndex = 0
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.OysterTestSuite.My.Resources.Resources.ualr2
        Me.PictureBox4.Location = New System.Drawing.Point(1331, 14)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(74, 80)
        Me.PictureBox4.TabIndex = 3
        Me.PictureBox4.TabStop = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(532, 111)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(873, 446)
        Me.DataGridView1.TabIndex = 0
        '
        'FileGazer
        '
        Me.FileGazer.BackColor = System.Drawing.Color.MintCream
        Me.FileGazer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FileGazer.Controls.Add(Me.Label23)
        Me.FileGazer.Controls.Add(Me.RefreshOutput_Btn)
        Me.FileGazer.Controls.Add(Me.Label8)
        Me.FileGazer.Controls.Add(Me.Label10)
        Me.FileGazer.Controls.Add(Me.SaveFile_btn)
        Me.FileGazer.Controls.Add(Me.Edit_OutputFileBtn)
        Me.FileGazer.Controls.Add(Me.PictureBox3)
        Me.FileGazer.Controls.Add(Me.LogFileTreeView)
        Me.FileGazer.Controls.Add(Me.RichTextBox1)
        Me.FileGazer.Location = New System.Drawing.Point(4, 29)
        Me.FileGazer.Name = "FileGazer"
        Me.FileGazer.Size = New System.Drawing.Size(1449, 576)
        Me.FileGazer.TabIndex = 2
        Me.FileGazer.Text = "Test Output Files"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Maroon
        Me.Label23.Location = New System.Drawing.Point(167, 88)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(145, 20)
        Me.Label23.TabIndex = 98
        Me.Label23.Text = "Run Output Files"
        '
        'RefreshOutput_Btn
        '
        Me.RefreshOutput_Btn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RefreshOutput_Btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshOutput_Btn.Location = New System.Drawing.Point(317, 517)
        Me.RefreshOutput_Btn.Margin = New System.Windows.Forms.Padding(2)
        Me.RefreshOutput_Btn.Name = "RefreshOutput_Btn"
        Me.RefreshOutput_Btn.Size = New System.Drawing.Size(76, 30)
        Me.RefreshOutput_Btn.TabIndex = 97
        Me.RefreshOutput_Btn.Text = "Refresh"
        Me.RefreshOutput_Btn.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Maroon
        Me.Label8.Location = New System.Drawing.Point(69, 525)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(223, 17)
        Me.Label8.TabIndex = 70
        Me.Label8.Text = "Double Click a File To View or Edit"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Maroon
        Me.Label10.Location = New System.Drawing.Point(477, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(126, 20)
        Me.Label10.TabIndex = 69
        Me.Label10.Text = "Loaded file is: "
        '
        'SaveFile_btn
        '
        Me.SaveFile_btn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SaveFile_btn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveFile_btn.Location = New System.Drawing.Point(1307, 532)
        Me.SaveFile_btn.Name = "SaveFile_btn"
        Me.SaveFile_btn.Size = New System.Drawing.Size(70, 30)
        Me.SaveFile_btn.TabIndex = 9
        Me.SaveFile_btn.Text = "Save"
        Me.SaveFile_btn.UseVisualStyleBackColor = True
        '
        'Edit_OutputFileBtn
        '
        Me.Edit_OutputFileBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Edit_OutputFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Edit_OutputFileBtn.Location = New System.Drawing.Point(1151, 532)
        Me.Edit_OutputFileBtn.Name = "Edit_OutputFileBtn"
        Me.Edit_OutputFileBtn.Size = New System.Drawing.Size(70, 30)
        Me.Edit_OutputFileBtn.TabIndex = 8
        Me.Edit_OutputFileBtn.Text = "Edit"
        Me.Edit_OutputFileBtn.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.OysterTestSuite.My.Resources.Resources.ualr2
        Me.PictureBox3.Location = New System.Drawing.Point(1331, 14)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(74, 80)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'LogFileTreeView
        '
        Me.LogFileTreeView.ContextMenuStrip = Me.ContextMenuStrip2
        Me.LogFileTreeView.Location = New System.Drawing.Point(18, 117)
        Me.LogFileTreeView.Name = "LogFileTreeView"
        Me.LogFileTreeView.Size = New System.Drawing.Size(443, 386)
        Me.LogFileTreeView.TabIndex = 1
        '
        'RichTextBox1
        '
        Me.RichTextBox1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.RichTextBox1.Location = New System.Drawing.Point(481, 117)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(924, 386)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.MintCream
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButton1, Me.ToolStripStatusLabel1, Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 696)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 10, 0)
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.Size = New System.Drawing.Size(1450, 25)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripDropDownButton1.DoubleClickEnabled = True
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.ShowDropDownArrow = False
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(4, 23)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Window
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.Maroon
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(122, 20)
        Me.ToolStripStatusLabel1.Text = "Oyster Test Suite"
        Me.ToolStripStatusLabel1.ToolTipText = "Oyster is Running"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.BackColor = System.Drawing.SystemColors.Window
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(75, 19)
        Me.ToolStripProgressBar1.Step = 20
        Me.ToolStripProgressBar1.Visible = False
        '
        'ExitBtn
        '
        Me.ExitBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ExitBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitBtn.Location = New System.Drawing.Point(1313, 656)
        Me.ExitBtn.Name = "ExitBtn"
        Me.ExitBtn.Size = New System.Drawing.Size(70, 30)
        Me.ExitBtn.TabIndex = 6
        Me.ExitBtn.Text = "Exit"
        Me.ExitBtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'GoHomeBtn
        '
        Me.GoHomeBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GoHomeBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GoHomeBtn.Location = New System.Drawing.Point(1157, 656)
        Me.GoHomeBtn.Name = "GoHomeBtn"
        Me.GoHomeBtn.Size = New System.Drawing.Size(70, 30)
        Me.GoHomeBtn.TabIndex = 7
        Me.GoHomeBtn.Text = "Home"
        Me.GoHomeBtn.UseVisualStyleBackColor = True
        '
        'Process1
        '
        Me.Process1.StartInfo.Domain = ""
        Me.Process1.StartInfo.LoadUserProfile = False
        Me.Process1.StartInfo.Password = Nothing
        Me.Process1.StartInfo.StandardErrorEncoding = Nothing
        Me.Process1.StartInfo.StandardOutputEncoding = Nothing
        Me.Process1.StartInfo.UserName = ""
        Me.Process1.SynchronizingObject = Me
        '
        'Process2
        '
        Me.Process2.StartInfo.Domain = ""
        Me.Process2.StartInfo.LoadUserProfile = False
        Me.Process2.StartInfo.Password = Nothing
        Me.Process2.StartInfo.StandardErrorEncoding = Nothing
        Me.Process2.StartInfo.StandardOutputEncoding = Nothing
        Me.Process2.StartInfo.UserName = ""
        Me.Process2.SynchronizingObject = Me
        '
        'Process3
        '
        Me.Process3.StartInfo.Domain = ""
        Me.Process3.StartInfo.LoadUserProfile = False
        Me.Process3.StartInfo.Password = Nothing
        Me.Process3.StartInfo.StandardErrorEncoding = Nothing
        Me.Process3.StartInfo.StandardOutputEncoding = Nothing
        Me.Process3.StartInfo.UserName = ""
        Me.Process3.SynchronizingObject = Me
        '
        'Process4
        '
        Me.Process4.StartInfo.Domain = ""
        Me.Process4.StartInfo.LoadUserProfile = False
        Me.Process4.StartInfo.Password = Nothing
        Me.Process4.StartInfo.StandardErrorEncoding = Nothing
        Me.Process4.StartInfo.StandardOutputEncoding = Nothing
        Me.Process4.StartInfo.UserName = ""
        Me.Process4.SynchronizingObject = Me
        '
        'Process5
        '
        Me.Process5.StartInfo.Domain = ""
        Me.Process5.StartInfo.LoadUserProfile = False
        Me.Process5.StartInfo.Password = Nothing
        Me.Process5.StartInfo.StandardErrorEncoding = Nothing
        Me.Process5.StartInfo.StandardOutputEncoding = Nothing
        Me.Process5.StartInfo.UserName = ""
        Me.Process5.SynchronizingObject = Me
        '
        'Process6
        '
        Me.Process6.StartInfo.Domain = ""
        Me.Process6.StartInfo.LoadUserProfile = False
        Me.Process6.StartInfo.Password = Nothing
        Me.Process6.StartInfo.StandardErrorEncoding = Nothing
        Me.Process6.StartInfo.StandardOutputEncoding = Nothing
        Me.Process6.StartInfo.UserName = ""
        Me.Process6.SynchronizingObject = Me
        '
        'Process7
        '
        Me.Process7.StartInfo.Domain = ""
        Me.Process7.StartInfo.LoadUserProfile = False
        Me.Process7.StartInfo.Password = Nothing
        Me.Process7.StartInfo.StandardErrorEncoding = Nothing
        Me.Process7.StartInfo.StandardOutputEncoding = Nothing
        Me.Process7.StartInfo.UserName = ""
        Me.Process7.SynchronizingObject = Me
        '
        'Process8
        '
        Me.Process8.StartInfo.Domain = ""
        Me.Process8.StartInfo.LoadUserProfile = False
        Me.Process8.StartInfo.Password = Nothing
        Me.Process8.StartInfo.StandardErrorEncoding = Nothing
        Me.Process8.StartInfo.StandardOutputEncoding = Nothing
        Me.Process8.StartInfo.UserName = ""
        Me.Process8.SynchronizingObject = Me
        '
        'Process9
        '
        Me.Process9.StartInfo.Domain = ""
        Me.Process9.StartInfo.LoadUserProfile = False
        Me.Process9.StartInfo.Password = Nothing
        Me.Process9.StartInfo.StandardErrorEncoding = Nothing
        Me.Process9.StartInfo.StandardOutputEncoding = Nothing
        Me.Process9.StartInfo.UserName = ""
        Me.Process9.SynchronizingObject = Me
        '
        'Process10
        '
        Me.Process10.StartInfo.Domain = ""
        Me.Process10.StartInfo.LoadUserProfile = False
        Me.Process10.StartInfo.Password = Nothing
        Me.Process10.StartInfo.StandardErrorEncoding = Nothing
        Me.Process10.StartInfo.StandardOutputEncoding = Nothing
        Me.Process10.StartInfo.UserName = ""
        Me.Process10.SynchronizingObject = Me
        '
        'Process11
        '
        Me.Process11.StartInfo.Domain = ""
        Me.Process11.StartInfo.LoadUserProfile = False
        Me.Process11.StartInfo.Password = Nothing
        Me.Process11.StartInfo.StandardErrorEncoding = Nothing
        Me.Process11.StartInfo.StandardOutputEncoding = Nothing
        Me.Process11.StartInfo.UserName = ""
        Me.Process11.SynchronizingObject = Me
        '
        'OysterTestSuiteForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.ForestGreen
        Me.ClientSize = New System.Drawing.Size(1450, 721)
        Me.Controls.Add(Me.GoHomeBtn)
        Me.Controls.Add(Me.ExitBtn)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.OysterTestSuiteTabs)
        Me.Controls.Add(Me.MenuStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "OysterTestSuiteForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Oyster Test Suite"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.OysterTestSuiteTabs.ResumeLayout(False)
        Me.SetupTab.ResumeLayout(False)
        Me.SetupTab.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.RunOutput.ResumeLayout(False)
        Me.RunOutput.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ERMetricsTab.ResumeLayout(False)
        Me.ERMetricsTab.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip3.ResumeLayout(False)
        Me.RunHistory.ResumeLayout(False)
        Me.RunHistory.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FileGazer.ResumeLayout(False)
        Me.FileGazer.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MenuStrip As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OysterTestSuiteTabs As TabControl
    Friend WithEvents SetupTab As TabPage
    Friend WithEvents RunOutput As TabPage
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripDropDownButton1 As ToolStripDropDownButton
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripProgressBar1 As ToolStripProgressBar
    Friend WithEvents ExitBtn As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents LoadedFileLbl As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents EditFileBtn As Button
    Friend WithEvents SaveFileBtn As Button
    Friend WithEvents RichTextBox9 As RichTextBox
    Friend WithEvents TreeView3 As TreeView
    Friend WithEvents Label1 As Label
    Friend WithEvents RunBtn As Button
    Friend WithEvents Label21 As Label
    Friend WithEvents RunRichTextBox As RichTextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents ScriptsLbl As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TestSuiteRootDirLbl As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents OysterRootTestSuiteDirLbl As Label
    Friend WithEvents OysterJarToRunLbl As Label
    Friend WithEvents TestSuiteScriptsDirLbl As Label
    Friend WithEvents TestSuiteLogFileLbl As Label
    Friend WithEvents PythonExeDirLbl As Label
    Friend WithEvents PythonRunFileLbl As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents RefreshBtn As Button
    Friend WithEvents RunCheckRTB As RichTextBox
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents CommentSelectedTextToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UnCommentSelectedTextToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CopySelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PasteSelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CutSelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HomeBtn As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents FileGazer As TabPage
    Friend WithEvents LogFileTreeView As TreeView
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents SaveFile_btn As Button
    Friend WithEvents Edit_OutputFileBtn As Button
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents GoHomeBtn As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Process1 As Process
    Friend WithEvents RefreshOutput_Btn As Button
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DeleteSelectedFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowFilePropertiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RenameFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label12 As Label
    Friend WithEvents RunHistory As TabPage
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Panel2 As Panel
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents StopRunBtn As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents SingleMode As Button
    Friend WithEvents KeepChkBox1 As CheckBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents TestSuiteXMLbl As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents ERMetricsTab As TabPage
    Friend WithEvents LFDateLbl As Label
    Friend WithEvents LinkFileDateERLbl As Label
    Friend WithEvents ERMetricsLoadedFile As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents ERMetLinkFileTxbx As TextBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents MoveFileBtn As Button
    Friend WithEvents Label29 As Label
    Friend WithEvents ERMetricJarBtn As Button
    Friend WithEvents ERMetricsJarTxBx As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents EROysterWkDirTxBx As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents EditFileERMetricsBtn As Button
    Friend WithEvents SaveFileERMetricsBtn As Button
    Friend WithEvents RichTextBox3 As RichTextBox
    Friend WithEvents TreeView4 As TreeView
    Friend WithEvents SaveERMetricsBtn As Button
    Friend WithEvents RunERMetricsBtn As Button
    Friend WithEvents Label32 As Label
    Friend WithEvents ERMetricsWkDirBtn As Button
    Friend WithEvents ERMetricsTxBx As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents InstructionERMetricsRTxbx As RichTextBox
    Friend WithEvents OysterWkDirBtn As Button
    Friend WithEvents Label34 As Label
    Friend WithEvents ContextMenuStrip3 As ContextMenuStrip
    Friend WithEvents CommentSelectedTextToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents UncommentSelectedTextToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Process2 As Process
    Friend WithEvents Process3 As Process
    Friend WithEvents Process4 As Process
    Friend WithEvents Process5 As Process
    Friend WithEvents ChangeOTSRootToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Process6 As Process
    Friend WithEvents Process7 As Process
    Friend WithEvents Process8 As Process
    Friend WithEvents Process9 As Process
    Friend WithEvents Process10 As Process
    Friend WithEvents Process11 As Process
End Class
