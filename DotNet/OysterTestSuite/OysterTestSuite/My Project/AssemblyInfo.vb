﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OysterTestSuite")>
<Assembly: AssemblyDescription("Oyster Regression Testing Program to be used by Programmers that work on the Oyster Jar file to make sure changes produce the same output as the baseline established with this program.")>
<Assembly: AssemblyCompany("UALR")>
<Assembly: AssemblyProduct("OysterTestSuite")>
<Assembly: AssemblyCopyright("Copyright ©  UALR 2020")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("05d9f8ec-c92b-4b81-b6ee-3c466cc64b43")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.5")>
<Assembly: AssemblyFileVersion("1.0.0.5")>
