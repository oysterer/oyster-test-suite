﻿

''' <summary>
''' Simple logger class
''' </summary>
Public Class OTSLogWriter


    ''' <summary>
    ''' Single Method to log to the TestSuite.log file
    ''' </summary>
    ''' <param name="Value"></param>
    ''' <param name="Value2"></param>
    Public Sub WriteToLog(ByVal Value As String, Optional ByVal Value2 As String = "")
        My.Computer.FileSystem.WriteAllText(My.Settings.TestSuiteLogFile, Value, True)
        My.Computer.FileSystem.WriteAllText(My.Settings.TestSuiteLogFile, Environment.NewLine, True)
        My.Computer.FileSystem.WriteAllText(My.Settings.TestSuiteLogFile, Value2, True)
    End Sub

    Public Sub WriteHeader(ByRef RunCheckRTB As RichTextBox, ByRef LogWriter As OTSLogWriter, ByVal helperBean As HelperBean)
        RunCheckRTB.Text += Constants.Divider
        RunCheckRTB.Text += vbCrLf
        RunCheckRTB.Text += helperBean.TestCaseName
        RunCheckRTB.Text += vbCrLf
        RunCheckRTB.Text += My.Settings.OysterJarToRun
        RunCheckRTB.Text += vbCrLf
        RunCheckRTB.Text += vbCrLf
        LogWriter.WriteToLog(Constants.Divider)
        LogWriter.WriteToLog(helperBean.TestCaseName, My.Settings.OysterJarToRun)
        LogWriter.WriteToLog(Environment.NewLine)
    End Sub

End Class
