﻿
'Constants Class

'Static String Class in vb.net.  No Class instance needed to access the String values.
'Constants.CDrive is a good example and all you need to reference any defined member. 
'Great For use in a MsgBox and any String that should be changed only in one place but 
'possibly used in several places. The values should and could change and many more Strings 
'added to help define the program.

'Very OK to change and modify as needed.



Imports System.IO

Public Class Constants

    Protected Friend Const CmdWindow As String = "C:\windows\system32\cmd.exe"
    Protected Friend Const ChangeReport = "ChangeReport"
    Protected Friend Const ChangeReports = "ChangeReports"
    Protected Friend Const Compare = "Compare"
    Protected Friend Const References = "References"
    Protected Friend Const ErrorExpectOutIdentChgeReportOutIdent = "Error in Expected Output Identities Count vs. Change Report Output Identities Count"
    Protected Friend Const ErrorExpectInputIdentChgeReportInputIdent = "Error in Expected Input Identities Count  vs. the Change Report Input Identities Count"
    Protected Friend Const ErrorExpectInputIdentUpdateChgeReportInputIdentUpdate = "Error in Expected Input Identities Updated Count vs. the Change Report Input Identites Updated Count"
    Protected Friend Const ErrorExpectRefCountLogFileRefCount = "Error in Expected Log References Count vs. the Log File References Count"
    Protected Friend Const ErrorExpectClusterCntLogFileClusterCnt = "Error in Expected Log Cluster Count vs. Log File Cluster Count"
    Protected Friend Const ErrorExpectClusterCntLinkFileClusterCnt = "Error in Expected Link Cluster Count vs. Link File Cluster Count"
    Protected Friend Const ErrorExpectNotUpdatedIdentCntChgReptNotUpdateIdentCnt = "Error in Expected Not Updated Identities Count vs. Change Report Not Updated Identities File Count"
    Protected Friend Const OysterTestSuiteRun = "New OysterTestSuite Run at "
    Protected Friend Const ChgReptNotFndTestCases = "Change Report(s) Not Found for the TestCase(s)"
    Protected Friend Const LinkFilestNotFndTestCases = "Link File(s) Not Found for the TestCase(s)"
    Protected Friend Const Divider = "---------------------------------------------------------------"
    Protected Friend Const EndOysterTestSuiteRun = "End of OysterTestSuite Run at "
    Protected Friend Const ErrorCreateOutputCheck = "Error Creating OutputCheck is "
    Protected Friend Const ErrorExpectRefCntLinkFileRefCnt = "Error in Expected Link References Count vs. Link File References Count"
    Protected Friend Const ExpectClusterCnt = "Expected Log Cluster Count    "
    Protected Friend Const ExpectRefCnt = "Expected Log Reference Count   "
    Protected Friend Const ErrorExpectInputIdentMergCntChgRptInputIdentMergCnt = "Error in Expected Input Identities Merged Count vs. Change Report Input Identities Merged Count"
    Protected Friend Const ErrorExpectNewInputIdentCreatCntChgRptNewInputIdentCreatCnt = "Error in Expected New Input Identities Created Count vs. the Change Report New Input Identities Created Count"
    Protected Friend Const ErrorExpectErrorIdentCntChgRptErrorIdentCnt = "Error in Expected Error Identities Count vs. Change Report Error Identities Count"
    Protected Friend Const ErrorReadTestSuiteXML = "Error Reading Test Suite XML File is "


    Protected Friend Const Name = "Name"
    Protected Friend Const RefAndClusterCnts = "ReferenceAndClusterCounts"
    Protected Friend Const ChgRptIdentCnts = "ChangeReportIdentitiesCounts"
    Protected Friend Const Expected = "Expected "
    Protected Friend Const Counts = " Counts: "


    '''Used for saving richtextbox loaded file information in a Hashtable
    '''for saving the file

    Protected Friend Const RichTextBox1String = "RichTextBox1"
    Protected Friend Const RichTextBox1 = 1
    Protected Friend Const RichTextBox9 = 9
    Protected Friend Const RichTextBox9String = "RichTextBox9"
    Protected Friend Const RichTextBox2 = 2
    Protected Friend Const RichTextBox2String = "RichTextBox2"
    Protected Friend Const RichTextBox3 = 3
    Protected Friend Const RichTextBox3String = "RichTextBox3"

    Protected Friend Const DataGridView1 = 1

    ''''Directories
    Protected Friend Const INPUT = "INPUT"
    Protected Friend Const OUTPUT = "OUTPUT"
    Protected Friend Const SCRIPTS = "SCRIPTS"

    ''''Messages only
    ''Protected Friend Const RobotTxBxMustBeFilled = "Python Directory and Robot Working Directory must not be Empty!"
    Protected Friend Const LinkFileReferenceCount = "Link File Reference Count is "
    Protected Friend Const ErrorCompareResultsFuncton = "Error in CompareResults Function is "
    Protected Friend Const LoadingTreeViewError = "Loading TreeView Error Is "
    Protected Friend Const LoadedFileIs = "Loaded File Is: "
    Protected Friend Const LoadingRichTextBoxError = "Loading RichTextBox Error Is "

    Protected Friend Const SettingsSaved = "Settings are now Saved for" & vbCrLf & " the next time you start the program."
    ''Protected Friend Const UseBrowseButton = "Please use the Browse Button " & vbCrLf & "to fill the TextBox!"
    Protected Friend Const FileIsEmpty = "This file is Empty at this time!"
    ''Protected Friend Const ERTextBoxesMustBeFilled = "ERMetrics Working Directory, " & vbCrLf & "ERMetrics Jar and Oyster Working " & vbCrLf & "Directory must not be Empty!"
    Protected Friend Const EmptyRichTextBox = "The RichTextBox is empty!"

    Protected Friend Const RunScriptNotFound = "OysterTestSuite Could Not " & vbCrLf & "Find the Chosen RunScript xml file!"

    Protected Friend Const AddRichTextBoxName = "RichTextBox Name needs to be " & vbCrLf & "added in the Constants file!"

    Protected Friend Const RichTxtBxEmpty = "RichTextBox Empty"
    Protected Friend Const SelectAFile = "Select a File"

    Protected Friend Const SelectFile = "You must select a File from the listed Files!"
    Protected Friend Const StopJava = "This will stop all Java Processes on this box!"

    Protected Friend Const ERMETRICSJARName As String = "ER-METRICS"
    Protected Friend Const ERMetricsJarNotFound = "Your chosen ERMetrics path " & vbCrLf & "must contain ER-Metrics.jar to Run!"
    Protected Friend Const ERMetrics As String = "ERMetrics"
    Protected Friend Const ERWorkingDirectory = "ErMetrics Working Directory " & vbCrLf & "must be Entered and Correct!"
    Protected Friend Const WorkDirectory As String = "WorkDirectory"
    Protected Friend Const ERMetricsJar = "er-metrics.jar"
    Protected Friend Const ERMetricsWorkDir = "/OysterTestSuite/Oyster/ER_Calculator"
    Protected Friend Const Directory = "Directory"
    Protected Friend Const File = "File"
    Protected Friend Const BrowseButton = "Use the Browse Button"
    Protected Friend Const UseBrowseButton = "Please use the Browse Button " & vbCrLf & "to fill the TextBox!"
    Protected Friend Const ERTextBoxesMustBeFilled = "ERMetrics Working Directory, " & vbCrLf & "ERMetrics Jar and Oyster Working " & vbCrLf & "Directory must not be Empty!"
    Protected Friend Const OysterWorkDirError = "Your Oyster Working Directory is " & vbCrLf & "Wrong for this Run Script!"
    Protected Friend Const ERMetricsJarEmpty = "ErMetrics Jar must be Entered and Correct!"
    Protected Friend Const WorkDirUnderRoot = "You must Choose a Work Directory under the Oyster Root!"
    Protected Friend Const LinkFileEmpty = "The Link File is Empty.  Run Oyster again and let it finish!"
    Protected Friend Const CheckERPropertiesFile = "Link File Copied to ERMetrics " & vbCrLf & "working directory.  Check your " & vbCrLf & "properties file if needed."
    Protected Friend Const EmptyLinkFileTextBox = "LinkFile TextBox Cannot be Empty!"
    Protected Friend Const DirectoryChoice = "Directory Choice"
    Protected Friend Const ErrorCopyingFile = "Exception Copying File is "
    Protected Friend Const EmptyERMetricsDirectory = "ERMetrics Working Directory " & vbCrLf & "Must Not Be Empty!"
    Protected Friend Const ERMetricsDirChoiceError = "ERMetrics Directory and ERMetrics " & vbCrLf & " Jar must be entered!"
    Protected Friend Const ERMetricsSaveFile = "ERMetrics Directory and ERMetrics " & vbCrLf & " Jar must be Saved before Running ERMetrics!"
    Protected Friend Const FileLoadedIs = "File Loaded is: "
    Protected Friend Const MakeFileReadOnlyError = "Could not make your file read only!"
End Class
