﻿


Imports System.Collections.Generic
''' <summary>
''' 
''' A basic javabean and useful to store and get information
''' modify as needed
''' 
''' </summary>
''' 

Public Class HelperBean

    Private OysterRunTime As String
    Private TestCaseHasNoLinkFile As Boolean = True
    Private TestCaseHaNosChangeReportFile As Boolean = True
    Private TestSuiteCaseWorkspace As String
    Private TestSuiteCaseScriptsDir As String
    Private TestSuiteOysterRoot As String
    Private TestSuiteOysterToJarRun As String
    Private LogFile As String
    Private PythonExe As String
    Private PythonFile As String
    Private OutPutLogFile As String
    Private OysterLinkFile As String
    Private TestCaseBeanDictionary As Dictionary(Of String, HelperBean)
    Private OutPutRefClusterFile As String
    Private OutputChangeReportFile As String
    Private OysterRunScript As String
    Private RefExpectedCount As Integer
    Private ClusterExpectedCount As Integer
    Private TestCase_Name As String
    Private TestCases As Dictionary(Of String, String)
    Private ChangeReport As Dictionary(Of String, Collection)
    Private LogReferenceCount As Integer

    Private LogRefExpectedCount As Integer
    Private LogClusterExpectedCount As Integer
    Private LinkRefExpectedCount As Integer
    Private LinkClusterExpectedCount As Integer

    Private LogClusterCount As Integer
    Private LinkReferenceCount As Integer
    Private LinkClusterCount As Integer
    Private ChangeReportOutputIdentities As Integer
    Private ChangeReportInputIdentitiesUpdated As Integer
    Private ChangeReporetInputIdentitiesMerged As Integer
    Private ChangeReportNewInputIdentitiesCreated As Integer
    Private ChangeReportInputIdentitiesNotUpdated As Integer
    Private ChangeReportErrorIdentities As Integer
    Private ChangeReportInputIdentities As Integer
    Private ChangeReportExpectOutputIdentities As Integer
    Private ChangeReportExpectInputIdentitiesUpdated As Integer
    Private ChangeReportExpectInputIdentitiesMerged As Integer
    Private ChangeReportExpectNewInputIdentitiesCreated As Integer
    Private ChangeReportExpectInputIdentitiesNotUpdated As Integer
    Private ChangeReportExpectErrorIdentities As Integer
    Private ChangeReportExpectInputIdentities As Integer
    Private SpecialLinkCounts As Integer
    Private RichTxBoxName As Boolean

    ''used to hold richboxname and file loaded for saving
    Private RichTextName As String
    Private FileLoadedName As String

    Public Property RichTexBoxNameBool() As Boolean
        Get
            Return RichTxBoxName
        End Get
        Set(ByVal value As Boolean)
            RichTxBoxName = value
        End Set
    End Property
    Property OysterRunTimeValue() As String
        Get
            Return OysterRunTime
        End Get
        Set(ByVal value As String)
            OysterRunTime = value
        End Set
    End Property

    Public Property LogRefExpectCount() As Integer
        Get
            Return LogRefExpectedCount
        End Get
        Set(ByVal value As Integer)
            LogRefExpectedCount = value
        End Set
    End Property

    Public Property LogClusterExpectCount() As Integer
        Get
            Return LogClusterExpectedCount
        End Get
        Set(ByVal value As Integer)
            LogClusterExpectedCount = value
        End Set
    End Property

    Public Property LinkRefExpectCount() As Integer
        Get
            Return LinkRefExpectedCount
        End Get
        Set(ByVal value As Integer)
            LinkRefExpectedCount = value
        End Set
    End Property
    Public Property LinkClusterExpectCount() As Integer
        Get
            Return LinkClusterExpectedCount
        End Get
        Set(ByVal value As Integer)
            LinkClusterExpectedCount = value
        End Set
    End Property

    Public Property HasLinkFile() As Boolean
        Get
            Return TestCaseHasNoLinkFile
        End Get
        Set(ByVal value As Boolean)
            TestCaseHasNoLinkFile = value
        End Set
    End Property

    Public Property HasChangeReportFile() As Boolean
        Get
            Return TestCaseHaNosChangeReportFile
        End Get
        Set(ByVal value As Boolean)
            TestCaseHaNosChangeReportFile = value
        End Set
    End Property

    Public Property SpecialLinkCount() As Integer
        Get
            Return SpecialLinkCounts
        End Get
        Set(ByVal value As Integer)
            SpecialLinkCounts = value
        End Set
    End Property

    Public Property ChangeReportExpectOutputIdentitiesCount() As Integer
        Get
            Return ChangeReportExpectOutputIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectOutputIdentities = value
        End Set
    End Property

    Public Property ChangeReportExpectInputIdentitiesUpdatedCount() As Integer
        Get
            Return ChangeReportExpectInputIdentitiesUpdated
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectInputIdentitiesUpdated = value
        End Set
    End Property

    Public Property ChangeReportExpectInputIdentitiesMergedCount() As Integer
        Get
            Return ChangeReportExpectInputIdentitiesMerged
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectInputIdentitiesMerged = value
        End Set
    End Property

    Public Property ChangeReportExpectNewInputIdentitiesCreatedCount() As Integer
        Get
            Return ChangeReportExpectNewInputIdentitiesCreated
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectNewInputIdentitiesCreated = value
        End Set
    End Property

    Public Property ChangeReportExpectInputIdentitiesNotUpdatedCount() As Integer
        Get
            Return ChangeReportExpectInputIdentitiesNotUpdated
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectInputIdentitiesNotUpdated = value
        End Set
    End Property

    Public Property ChangeReportExpectErrorIdentitiesCount() As Integer
        Get
            Return ChangeReportExpectErrorIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectErrorIdentities = value
        End Set
    End Property

    Public Property ChangeReportExpectInputIdentitiesCount() As Integer
        Get
            Return ChangeReportExpectInputIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportExpectInputIdentities = value
        End Set
    End Property



    Public Property ChangeReportOutputIdentitiesCount() As Integer
        Get
            Return ChangeReportOutputIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportOutputIdentities = value
        End Set
    End Property

    Public Property ChangeReportInputIdentitiesUpdatedCount() As Integer
        Get
            Return ChangeReportInputIdentitiesUpdated
        End Get
        Set(ByVal value As Integer)
            ChangeReportInputIdentitiesUpdated = value
        End Set
    End Property

    Public Property ChangeReportInputIdentitiesMergedCount() As Integer
        Get
            Return ChangeReporetInputIdentitiesMerged
        End Get
        Set(ByVal value As Integer)
            ChangeReporetInputIdentitiesMerged = value
        End Set
    End Property

    Public Property ChangeReportNewInputIdentitiesCreatedCount() As Integer
        Get
            Return ChangeReportNewInputIdentitiesCreated
        End Get
        Set(ByVal value As Integer)
            ChangeReportNewInputIdentitiesCreated = value
        End Set
    End Property

    Public Property ChangeReportInputIdentitiesNotUpdatedCount() As Integer
        Get
            Return ChangeReportInputIdentitiesNotUpdated
        End Get
        Set(ByVal value As Integer)
            ChangeReportInputIdentitiesNotUpdated = value
        End Set
    End Property

    Public Property ChangeReportErrorIdentitiesCount() As Integer
        Get
            Return ChangeReportErrorIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportErrorIdentities = value
        End Set
    End Property

    Public Property ChangeReportInputIdentitiesCount() As Integer
        Get
            Return ChangeReportInputIdentities
        End Get
        Set(ByVal value As Integer)
            ChangeReportInputIdentities = value
        End Set
    End Property




    Public Property LinkFileRefCount() As Integer
        Get
            Return LinkReferenceCount
        End Get
        Set(ByVal value As Integer)
            LinkReferenceCount = value
        End Set
    End Property

    Public Property LinkFileClusterCount() As Integer
        Get
            Return LinkClusterCount
        End Get
        Set(ByVal value As Integer)
            LinkClusterCount = value
        End Set
    End Property

    Public Property LogFileRefCount() As Integer
        Get
            Return LogReferenceCount
        End Get
        Set(ByVal value As Integer)
            LogReferenceCount = value
        End Set
    End Property

    Public Property LogFileClusterCount() As Integer
        Get
            Return LogClusterCount
        End Get
        Set(ByVal value As Integer)
            LogClusterCount = value
        End Set
    End Property

    Public Property PythonExeDir() As String
        Get
            Return PythonExe
        End Get
        Set(ByVal value As String)
            PythonExe = value
        End Set
    End Property


    Public Property RichTextBoxName() As String
        Get
            Return RichTextName
        End Get
        Set(ByVal value As String)
            RichTextName = value
        End Set
    End Property

    Public Property FileLoaded() As String
        Get
            Return FileLoadedName
        End Get
        Set(ByVal value As String)
            FileLoadedName = value
        End Set
    End Property

    Public Property PythonRunFile() As String
        Get
            Return PythonFile
        End Get
        Set(ByVal value As String)
            PythonFile = value
        End Set
    End Property

    Public Property TestSuiteRootDir() As String
        Get
            Return TestSuiteCaseWorkspace
        End Get
        Set(ByVal value As String)
            TestSuiteCaseWorkspace = value
        End Set
    End Property

    Public Property TestSuiteScriptsDir() As String
        Get
            Return TestSuiteCaseScriptsDir
        End Get
        Set(ByVal value As String)
            TestSuiteCaseScriptsDir = value
        End Set
    End Property

    Public Property OysterRootTestSuiteDir() As String
        Get
            Return TestSuiteOysterRoot
        End Get
        Set(ByVal value As String)
            TestSuiteOysterRoot = value
        End Set
    End Property

    Public Property OysterJarToRun() As String
        Get
            Return TestSuiteOysterToJarRun
        End Get
        Set(ByVal value As String)
            TestSuiteOysterToJarRun = value
        End Set
    End Property

    Public Property AllTestCasesDictionary() As Dictionary(Of String, HelperBean)
        Get
            Return TestCaseBeanDictionary
        End Get
        Set(ByVal value As Dictionary(Of String, HelperBean))
            TestCaseBeanDictionary = value
        End Set
    End Property


    Public Property TestCasesDictionary() As Dictionary(Of String, String)
        Get
            Return TestCases
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            TestCases = value
        End Set
    End Property

    Public Property RefClusterOutPutFile() As String
        Get
            Return OutPutRefClusterFile
        End Get
        Set(ByVal value As String)
            OutPutRefClusterFile = value
        End Set
    End Property

    Public Property ExpectedRefCount() As Integer
        Get
            Return RefExpectedCount
        End Get
        Set(ByVal value As Integer)
            RefExpectedCount = value
        End Set
    End Property


    Public Property ChangeReports() As Dictionary(Of String, Collection)
        Get
            Return ChangeReport
        End Get
        Set(ByVal value As Dictionary(Of String, Collection))
            ChangeReport = value
        End Set
    End Property


    Public Property ExpectedClusterCount() As Integer
        Get
            Return ClusterExpectedCount
        End Get
        Set(ByVal value As Integer)
            ClusterExpectedCount = value
        End Set
    End Property


    Public Property TestCaseName() As String
        Get
            Return TestCase_Name
        End Get
        Set(ByVal value As String)
            TestCase_Name = value
        End Set
    End Property

    Public Property OysterLinkFileWithPath() As String
        Get
            Return OysterLinkFile
        End Get
        Set(ByVal value As String)
            OysterLinkFile = value
        End Set
    End Property

    Public Property OysterRunScriptName() As String
        Get
            Return OysterRunScript
        End Get
        Set(ByVal value As String)
            OysterRunScript = value
        End Set
    End Property

    Public Property TestSuiteLogFile() As String
        Get
            Return LogFile
        End Get
        Set(ByVal value As String)
            LogFile = value
        End Set
    End Property

    Public Property RunOutputLogFile() As String
        Get
            Return OutPutLogFile
        End Get
        Set(ByVal value As String)
            OutPutLogFile = value
        End Set
    End Property

    Public Property ChangeReportOutputFile() As String
        Get
            Return OutputChangeReportFile
        End Get
        Set(ByVal value As String)
            OutputChangeReportFile = value
        End Set
    End Property





End Class
