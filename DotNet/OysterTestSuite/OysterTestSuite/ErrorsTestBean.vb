﻿Imports OysterTestSuite

Public Class ErrorsTestBean

    Private JarUsedInRun As String
    Private DateTime As String
    Private list As New ArrayList()
    Private ErrorOutputBeanList As New List(Of ErrorsTestBean)
    Private TestBeanName As String
    Private TestFailedName As String
    Private TestsRunCount As Integer
    Private TestFailed As Integer
    Private TestPassed As Integer
    Private PythonRunTime As String

    Private map As New Dictionary(Of String, List(Of String))()



    Public Property JarUsedInRn As String
        Get
            Return JarUsedInRun
        End Get
        Set(value As String)
            JarUsedInRun = value
        End Set
    End Property

    Public Property TestBeanNme As String
        Get
            Return TestBeanName
        End Get
        Set(value As String)
            TestBeanName = value
        End Set
    End Property


    Public Property TestFailedNme As String
        Get
            Return TestFailedName
        End Get
        Set(value As String)
            TestFailedName = value
        End Set
    End Property


    Public Property TestsRunCnt As Integer
        Get
            Return TestsRunCount
        End Get
        Set(value As Integer)
            TestsRunCount = value
        End Set
    End Property

    Public Property TestFailedCnt As Integer
        Get
            Return TestFailed
        End Get
        Set(value As Integer)
            TestFailed = value
        End Set
    End Property

    Public Property TestPassd As Integer
        Get
            Return TestPassed
        End Get
        Set(value As Integer)
            TestPassed = value
        End Set
    End Property

    Public Property DateTme As String
        Get
            Return DateTime
        End Get
        Set(value As String)
            DateTime = value
        End Set
    End Property

    Public Property Mapp As Dictionary(Of String, List(Of String))
        Get
            Return map
        End Get
        Set(value As Dictionary(Of String, List(Of String)))
            map = value
        End Set
    End Property

    Public Property ErrorOutputBeanLst As List(Of ErrorsTestBean)
        Get
            Return ErrorOutputBeanList
        End Get
        Set(value As List(Of ErrorsTestBean))
            ErrorOutputBeanList = value
        End Set
    End Property

    Public Property Lst As ArrayList
        Get
            Return list
        End Get
        Set(value As ArrayList)
            list = value
        End Set
    End Property

    Public Property PythonRunTime1 As String
        Get
            Return PythonRunTime
        End Get
        Set(value As String)
            PythonRunTime = value
        End Set
    End Property
End Class
