﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AssertDataGridView = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.LoadFile = New System.Windows.Forms.Button()
        Me.CreateAssertFileBtn = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RefToStrAssertion = New System.Windows.Forms.RadioButton()
        Me.RefToRefAssertion = New System.Windows.Forms.RadioButton()
        Me.TrueNegativelAssertion = New System.Windows.Forms.RadioButton()
        Me.TruePositiveAssertion = New System.Windows.Forms.RadioButton()
        Me.ReferenceTransferAsserton = New System.Windows.Forms.RadioButton()
        Me.StrToStrAssertion = New System.Windows.Forms.RadioButton()
        Me.StrSplitAssertion = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CloseBtn = New System.Windows.Forms.Button()
        Me.SaveAssertFileBtn = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        CType(Me.AssertDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'AssertDataGridView
        '
        Me.AssertDataGridView.BackgroundColor = System.Drawing.Color.AliceBlue
        Me.AssertDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AssertDataGridView.Location = New System.Drawing.Point(326, 27)
        Me.AssertDataGridView.Name = "AssertDataGridView"
        Me.AssertDataGridView.Size = New System.Drawing.Size(1005, 371)
        Me.AssertDataGridView.TabIndex = 0
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'LoadFile
        '
        Me.LoadFile.Location = New System.Drawing.Point(891, 443)
        Me.LoadFile.Name = "LoadFile"
        Me.LoadFile.Size = New System.Drawing.Size(75, 23)
        Me.LoadFile.TabIndex = 1
        Me.LoadFile.Text = "Load File"
        Me.LoadFile.UseVisualStyleBackColor = True
        '
        'CreateAssertFileBtn
        '
        Me.CreateAssertFileBtn.Enabled = False
        Me.CreateAssertFileBtn.Location = New System.Drawing.Point(80, 342)
        Me.CreateAssertFileBtn.Name = "CreateAssertFileBtn"
        Me.CreateAssertFileBtn.Size = New System.Drawing.Size(132, 23)
        Me.CreateAssertFileBtn.TabIndex = 5
        Me.CreateAssertFileBtn.Text = "Create an Assertion File"
        Me.CreateAssertFileBtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RefToStrAssertion)
        Me.GroupBox1.Controls.Add(Me.RefToRefAssertion)
        Me.GroupBox1.Controls.Add(Me.TrueNegativelAssertion)
        Me.GroupBox1.Controls.Add(Me.TruePositiveAssertion)
        Me.GroupBox1.Controls.Add(Me.ReferenceTransferAsserton)
        Me.GroupBox1.Controls.Add(Me.StrToStrAssertion)
        Me.GroupBox1.Controls.Add(Me.StrSplitAssertion)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(253, 229)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Assertion Choice"
        '
        'RefToStrAssertion
        '
        Me.RefToStrAssertion.AutoSize = True
        Me.RefToStrAssertion.Location = New System.Drawing.Point(42, 77)
        Me.RefToStrAssertion.Name = "RefToStrAssertion"
        Me.RefToStrAssertion.Size = New System.Drawing.Size(111, 17)
        Me.RefToStrAssertion.TabIndex = 14
        Me.RefToStrAssertion.TabStop = True
        Me.RefToStrAssertion.Text = "RefToStrAssertion"
        Me.RefToStrAssertion.UseVisualStyleBackColor = True
        '
        'RefToRefAssertion
        '
        Me.RefToRefAssertion.AutoSize = True
        Me.RefToRefAssertion.Location = New System.Drawing.Point(42, 40)
        Me.RefToRefAssertion.Name = "RefToRefAssertion"
        Me.RefToRefAssertion.Size = New System.Drawing.Size(115, 17)
        Me.RefToRefAssertion.TabIndex = 13
        Me.RefToRefAssertion.TabStop = True
        Me.RefToRefAssertion.Text = "RefToRefAssertion"
        Me.RefToRefAssertion.UseVisualStyleBackColor = True
        '
        'TrueNegativelAssertion
        '
        Me.TrueNegativelAssertion.AutoSize = True
        Me.TrueNegativelAssertion.Location = New System.Drawing.Point(42, 284)
        Me.TrueNegativelAssertion.Name = "TrueNegativelAssertion"
        Me.TrueNegativelAssertion.Size = New System.Drawing.Size(133, 17)
        Me.TrueNegativelAssertion.TabIndex = 12
        Me.TrueNegativelAssertion.TabStop = True
        Me.TrueNegativelAssertion.Text = "TrueNegativeAssertion"
        Me.TrueNegativelAssertion.UseVisualStyleBackColor = True
        '
        'TruePositiveAssertion
        '
        Me.TruePositiveAssertion.AutoSize = True
        Me.TruePositiveAssertion.Location = New System.Drawing.Point(42, 247)
        Me.TruePositiveAssertion.Name = "TruePositiveAssertion"
        Me.TruePositiveAssertion.Size = New System.Drawing.Size(127, 17)
        Me.TruePositiveAssertion.TabIndex = 11
        Me.TruePositiveAssertion.TabStop = True
        Me.TruePositiveAssertion.Text = "TruePositiveAssertion"
        Me.TruePositiveAssertion.UseVisualStyleBackColor = True
        '
        'ReferenceTransferAsserton
        '
        Me.ReferenceTransferAsserton.AutoSize = True
        Me.ReferenceTransferAsserton.Location = New System.Drawing.Point(42, 327)
        Me.ReferenceTransferAsserton.Name = "ReferenceTransferAsserton"
        Me.ReferenceTransferAsserton.Size = New System.Drawing.Size(157, 17)
        Me.ReferenceTransferAsserton.TabIndex = 10
        Me.ReferenceTransferAsserton.TabStop = True
        Me.ReferenceTransferAsserton.Text = "ReferenceTransferAssertion"
        Me.ReferenceTransferAsserton.UseVisualStyleBackColor = True
        '
        'StrToStrAssertion
        '
        Me.StrToStrAssertion.AutoSize = True
        Me.StrToStrAssertion.Location = New System.Drawing.Point(42, 114)
        Me.StrToStrAssertion.Name = "StrToStrAssertion"
        Me.StrToStrAssertion.Size = New System.Drawing.Size(107, 17)
        Me.StrToStrAssertion.TabIndex = 8
        Me.StrToStrAssertion.TabStop = True
        Me.StrToStrAssertion.Text = "StrToStrAssertion"
        Me.StrToStrAssertion.UseVisualStyleBackColor = True
        '
        'StrSplitAssertion
        '
        Me.StrSplitAssertion.AutoSize = True
        Me.StrSplitAssertion.Location = New System.Drawing.Point(42, 151)
        Me.StrSplitAssertion.Name = "StrSplitAssertion"
        Me.StrSplitAssertion.Size = New System.Drawing.Size(101, 17)
        Me.StrSplitAssertion.TabIndex = 9
        Me.StrSplitAssertion.TabStop = True
        Me.StrSplitAssertion.Text = "StrSplitAssertion"
        Me.StrSplitAssertion.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1054, 424)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 42)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Use as Template"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CloseBtn
        '
        Me.CloseBtn.Location = New System.Drawing.Point(1317, 620)
        Me.CloseBtn.Name = "CloseBtn"
        Me.CloseBtn.Size = New System.Drawing.Size(75, 23)
        Me.CloseBtn.TabIndex = 8
        Me.CloseBtn.Text = "Close"
        Me.CloseBtn.UseVisualStyleBackColor = True
        '
        'SaveAssertFileBtn
        '
        Me.SaveAssertFileBtn.Location = New System.Drawing.Point(1158, 434)
        Me.SaveAssertFileBtn.Name = "SaveAssertFileBtn"
        Me.SaveAssertFileBtn.Size = New System.Drawing.Size(75, 23)
        Me.SaveAssertFileBtn.TabIndex = 9
        Me.SaveAssertFileBtn.Text = "Save File"
        Me.SaveAssertFileBtn.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(22, 27)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1374, 573)
        Me.TabControl1.TabIndex = 10
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.SaveAssertFileBtn)
        Me.TabPage1.Controls.Add(Me.AssertDataGridView)
        Me.TabPage1.Controls.Add(Me.CreateAssertFileBtn)
        Me.TabPage1.Controls.Add(Me.LoadFile)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1366, 547)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TabControl2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1366, 547)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Location = New System.Drawing.Point(3, 24)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1321, 487)
        Me.TabControl2.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1313, 461)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1313, 461)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "TabPage4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1408, 655)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.CloseBtn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.AssertDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents AssertDataGridView As DataGridView
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents LoadFile As Button
    Friend WithEvents CreateAssertFileBtn As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TrueNegativelAssertion As RadioButton
    Friend WithEvents TruePositiveAssertion As RadioButton
    Friend WithEvents ReferenceTransferAsserton As RadioButton
    Friend WithEvents StrToStrAssertion As RadioButton
    Friend WithEvents StrSplitAssertion As RadioButton
    Friend WithEvents RefToStrAssertion As RadioButton
    Friend WithEvents RefToRefAssertion As RadioButton
    Friend WithEvents Button1 As Button
    Friend WithEvents CloseBtn As Button
    Friend WithEvents SaveAssertFileBtn As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
End Class
