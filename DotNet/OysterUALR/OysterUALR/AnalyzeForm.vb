﻿Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms.DataVisualization.Charting
Imports Microsoft.VisualBasic.Strings

Public Class AnalyzeForm

    Public thePath As String = Nothing
    Private helpBeaner As HelperBean
    Private reload As Boolean = False
    Private Header As String = Nothing
    'used to resize the program if chosen
    Private RS As New FormResizer


    Protected Overrides Sub OnLoad(e As EventArgs)

        Dim dt As New DataTable
        Dim countsClass As New CountsClass
        Dim dta As New DataTable
        Dim binding1 As New BindingSource
        Dim dta1 As New DataTable

        Try
            Cursor = Cursors.WaitCursor
            ' Oysterform.ToolStripStatusLabel1.Text = "Loading..."
            ''        Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False
            'Oysterform.ToolStripProgressBar1.PerformStep()
            'for resizingu
            RS.FindAllControls(Me)
            ' Console.WriteLine("doing onload")
            If Not String.IsNullOrEmpty(thePath) Then

                AnalyzeDataGridView.DataSource = Nothing

                If System.IO.File.Exists(thePath) = True Then
                    ' Oysterform.ToolStripStatusLabel1.Text = "Loading..."
                    LoadedFileLbl.Text = "Loaded File is: " & thePath
                    loadedFile2lbl.Text = "Loaded File is: " & thePath
                    LFileLbl.Text = "Loaded File is: " & thePath
                    ErrorsLbl.Text = "Loaded File is: " & thePath
                    dta = countsClass.NewAnalyzeData(dta, thePath)
                    Chart2.ChartAreas(0).AxisX.IsLabelAutoFit = False
                    Chart2.ChartAreas(0).AxisX.LabelStyle.Angle = 45
                    Chart2.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep45
                    Chart2.ChartAreas(0).AxisX.LabelStyle.Enabled = True
                    Chart2.ChartAreas(0).AxisX.MinorTickMark.Enabled = True
                    Chart2.ChartAreas(0).AxisX.Interval = 1
                    '    Chart1.ChartAreas(0).AxisX.IsLabelAutoFit = True
                    Chart2.AlignDataPointsByAxisLabel()
                    '    Chart2.ChartAreas(0).AxisX.Title = "Data Attribute"
                    Chart2.ChartAreas(0).AxisY.Title = "Non-Null Counts"
                    Chart2.Legends(0).Enabled = False
                    Chart2.Series(0).Points.DataBindXY(dta.Rows, "Column", dta.Rows, "Value")


                    dt = countsClass.AnalyzeData(dt, thePath)
                    'Header = CountsClass.Header
                    '  Oysterform.ToolStripProgressBar1.PerformStep()
                    dt = countsClass.CalculatePercentage(dt)
                    '  Oysterform.ToolStripProgressBar1.PerformStep()
                    dt = countsClass.AnalyzeDistinctData(dt, thePath)

                    AnalyzeDataGridView.DataSource = Nothing
                    AnalyzeDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
                    AnalyzeDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                    binding1.DataSource = dt
                    AnalyzeDataGridView.DataSource = binding1
                    For Each column As DataGridViewColumn In AnalyzeDataGridView.Columns
                        column.SortMode = DataGridViewColumnSortMode.NotSortable
                    Next

                Else

                    MessageBox.Show("File Does Not Exist", "File To Load", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            Else
                DataLoadBtn.PerformClick()

            End If

        Catch ex As Exception
            MessageBox.Show("Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Cursor = Cursors.Default
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripProgressBar1.Visible = False

            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    'Private Sub AnalyzeForm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
    '    'If My.Settings.Testing Then
    '    '    MessageBox.Show("Testing", "Testing", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    Me.BringToFront()

    '    'End If
    '    Me.Close()
    'End Sub

    Private Sub LoadDataGridView(ByVal FileNamePath As String)

        Dim OHelper As New OysterHelper
        OHelper.LoadDataGridView(FileNamePath)


        'Dim TextLine As String = Nothing
        'Dim SplitLine() As String
        'Dim HeaderRow() As String = Nothing
        'Dim HeaderRowCount As Integer = 0
        'Dim dt As New DataTable
        'Dim Bindingsource1 As New BindingSource
        'Dim index As Integer = 0
        'Dim delimiter As String = Nothing
        'Dim countsClass As New CountsClass
        'Dim CountBadRows As Integer = 0
        'Try

        '    If System.IO.File.Exists(FileNamePath) = True Then

        '        Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)

        '        Do While objReader.Peek() <> -1

        '            TextLine = objReader.ReadLine()
        '            If Not String.IsNullOrEmpty(TextLine) Then

        '                If index > 0 Then

        '                    Try
        '                        SplitLine = Split(TextLine, delimiter)
        '                        If Not SplitLine.Count > HeaderRowCount Then
        '                            dt.Rows.Add(SplitLine)
        '                        Else
        '                            CountBadRows += 1

        '                        End If

        '                    Catch ex As Exception
        '                        Console.WriteLine(ex.Message)
        '                        CountBadRows += 1
        '                    End Try

        '                Else

        '                    If TextLine.Contains(",") Then
        '                        delimiter = ","
        '                    ElseIf TextLine.Contains(vbTab) Then
        '                        delimiter = vbTab
        '                    ElseIf TextLine.Contains("|") Then
        '                        delimiter = "|"
        '                    ElseIf TextLine.Contains(";") Then
        '                        delimiter = ";"
        '                    End If

        '                    LoadColumns(TextLine, dt, False)

        '                    Console.WriteLine("Column count " & dt.Columns.Count)
        '                    index = 1
        '                    HeaderRow = Split(TextLine, delimiter)
        '                    HeaderRowCount = HeaderRow.Length


        '                    'For i = 0 To HeaderRowCount - 1
        '                    '    countsClass.SetPropertyValue(HeaderRow(i), 0)
        '                    'Next
        '                End If
        '            End If
        '        Loop

        '        FileDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        '        FileDataGridView.DataSource = Nothing
        '        Bindingsource1.DataSource = dt
        '        FileDataGridView.DataSource = Bindingsource1



        '        If objReader IsNot Nothing Then
        '            objReader.Close()
        '            objReader = Nothing
        '        End If

        '        If CountBadRows > 0 Then
        '            MessageBox.Show(CountBadRows.ToString & " Bad Rows were skipped Loading", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        End If

        '    Else
        '        MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

        '    End If
        'Catch ex As Exception
        '    MessageBox.Show("LoadDataGridView Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Finally

        'End Try

    End Sub
    Private Sub DataLoadBtn_Click(sender As Object, e As EventArgs) Handles DataLoadBtn.Click

        Dim dt As New DataTable
        Dim bindingSource1 As New BindingSource()
        Dim countsClass As New CountsClass
        Dim dta As New DataTable

        Try
            Oysterform.ToolStripStatusLabel1.Text = "Loading..."
            ''     Oysterform.ToolStripStatusLabel1.Visible = True
            'Oysterform.ToolStripProgressBar1.Visible = True
            'Oysterform.ToolStripProgressBar1.PerformStep()

            Dim fName As String = ""
            If Not String.IsNullOrEmpty(Oysterform.OysterWorkDirTxBx.Text) Then

                OpenFileDialog1.InitialDirectory = Oysterform.OysterWorkDirTxBx.Text

            ElseIf Not String.IsNullOrEmpty(Oysterform.OysterRootDirTxBx.Text) Then

                OpenFileDialog1.InitialDirectory = Oysterform.OysterRootDirTxBx.Text

            Else
                OpenFileDialog1.InitialDirectory = "C:\"
            End If

            OpenFileDialog1.RestoreDirectory = True

            If (OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                fName = OpenFileDialog1.FileName
                AnalyzeDataTxBx.Text = fName
            Else
                ' MessageBox.Show("File Does Not Exist or Load Canceled!", "File To Load", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Dispose()
                Exit Sub
            End If

            Chart1.Series(0).Points.Clear()
            ErrorsRichTextBox.Clear()

            ' AnalyzeDataGridView.DataSource = Nothing


            If System.IO.File.Exists(fName) = True Then
                Cursor = Cursors.WaitCursor


                LoadedFileLbl.Text = "Loaded File is: " & fName
                LFileLbl.Text = "Loaded File is: " & fName
                loadedFile2lbl.Text = "Loaded File is: " & fName
                ErrorsLbl.Text = "Loaded File is: " & fName
                'clear everything on load of new file
                ECountLbl.Text = "Possible Errors Line Count is: "
                FindsLBL.Text = "Hits: "
                ErrorsRichTextBox.Clear()

                FileDataGridView.DataSource = Nothing


                '   LoadDataGridView(fName)
                dta = countsClass.NewAnalyzeData(dta, fName)
                Chart2.ChartAreas(0).AxisX.IsLabelAutoFit = False
                Chart2.ChartAreas(0).AxisX.LabelStyle.Angle = 45
                Chart2.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep45
                'Chart2.Series(0).SetCustomProperty("PointWidth", "1")
                Chart2.ChartAreas(0).AxisX.LabelStyle.Enabled = True
                Chart2.ChartAreas(0).AxisX.MinorTickMark.Enabled = True
                Chart2.ChartAreas(0).AxisX.Interval = 1
                Chart2.AlignDataPointsByAxisLabel()
                Chart2.ChartAreas(0).AxisY.Title = "Non-Null Counts"
                Chart2.Legends(0).Enabled = False
                Chart2.Series(0).Points.DataBindXY(dta.Rows, "Column", dta.Rows, "Value")


                dt = countsClass.AnalyzeData(dt, fName)
                '    Oysterform.ToolStripProgressBar1.PerformStep()
                dt = countsClass.CalculatePercentage(dt)
                '    Oysterform.ToolStripProgressBar1.PerformStep()
                dt = countsClass.AnalyzeDistinctData(dt, fName)



                AnalyzeDataGridView.DataSource = Nothing
                bindingSource1.DataSource = dt
                AnalyzeDataGridView.DataSource = bindingSource1

                AnalyzeDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                AnalyzeDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                For Each column As DataGridViewColumn In AnalyzeDataGridView.Columns
                    column.SortMode = DataGridViewColumnSortMode.NotSortable
                Next



            Else

                MessageBox.Show("File Does Not Exist", "File To Load", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
            '     End If
        Catch ex As Exception
            MessageBox.Show("DataLoad Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False

            GC.WaitForPendingFinalizers()
            GC.Collect()
            Cursor = Cursors.Default

        End Try
    End Sub
    Private Function ExportDataGridView(filePath As String, ByRef dgv As DataGridView) As Boolean

        Try

            Using sw As New StreamWriter(filePath)
                Dim i As Integer = dgv.Rows.Count
                Dim j As Integer = 0
                For Each dr As DataGridViewRow In dgv.Rows
                    If j < i - 1 Then
                        For Each dc As DataGridViewCell In dr.Cells
                            sw.WriteLine(dc.Value.ToString)

                            '    Console.WriteLine("I = " & i & "  j = " & j)
                        Next

                    End If
                    j += 1
                Next

            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

        Return True

    End Function

    Private Sub Exit_Btn2_Click_1(sender As Object, e As EventArgs) Handles Exit_Btn2.Click
        Me.Dispose()
    End Sub

    ' Method called to resize the program.
    Private Sub AnalyzeForm_Resize(sender As Object, e As EventArgs) Handles Me.Resize

        RS.ResizeAllControls(Me, Me.Width, Me.Height)
    End Sub

    Private Sub AnalyzeDataTxBx_TextChanged(sender As Object, e As EventArgs) Handles AnalyzeDataTxBx.TextChanged

        Dim FileNamePath As String = Nothing
        Dim dta1 As New DataTable

        Try



            FileNamePath = AnalyzeDataTxBx.Text
            If File.Exists(FileNamePath) Then

                dta1 = AnalyzeData(dta1, FileNamePath)
                If HeaderCheckedListBox.Items.Count > 0 Then
                    HeaderCheckedListBox.Items.Clear()
                End If
                If LoadCheckedListBox.Items.Count > 0 Then
                    LoadCheckedListBox.Items.Clear()
                End If

                For Each column As DataColumn In dta1.Columns
                    '    Console.WriteLine(column.ColumnName)
                    If String.IsNullOrEmpty(column.ColumnName) Or column.ColumnName.ToUpper.Equals("RECID") Or column.ColumnName.ToUpper.Equals("STATISTIC") Then
                    Else
                        HeaderCheckedListBox.Items.Add(column.ColumnName)
                        LoadCheckedListBox.Items.Add(column.ColumnName)
                    End If

                Next
                If AttribueDataGridView.Rows.Count > 0 Then
                    AttribueDataGridView.DataSource = Nothing
                End If
            Else

                MessageBox.Show("You Must Pick a File!", "AnalyzeData TextChange", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception

            MessageBox.Show("Analyze Data Text Changed file is " & ex.Message, "AnalyzeData TextChange", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try


    End Sub
    Public Function Distribution(ByRef dt As DataTable, ByVal FileNamePath As String, ByVal columnChoice As String, ByRef hightValue As HelperBean) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim choice As String = columnChoice
        Dim i As Integer = 0
        Dim delimiter As String = Nothing
        Dim index As Integer = 0
        Dim j As Integer = 0
        Dim countsClass As New CountsClass
        Dim lowestValue As Integer = 0
        Dim highestValue As Integer = 0
        Dim countValue As Integer = 0
        Dim rangevalue As Integer = 0
        Dim classCount As Integer = 0
        Dim widthValue As Integer = 0
        Dim limit As Integer = 0

        Try

            If System.IO.File.Exists(FileNamePath) = True Then

                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If Not String.IsNullOrEmpty(TextLine) Then

                        SplitLine = Split(TextLine, delimiter)

                        If index > 0 Then

                            If Not String.IsNullOrEmpty(SplitLine(i)) Then
                                j = countsClass.GetPropertyValue(SplitLine(i).ToUpper)
                                j = j + 1
                                countsClass.SetPropertyValue(SplitLine(i).ToUpper, j)
                            End If
                            '  Next
                        Else

                            If TextLine.Contains(",") Then
                                delimiter = ","
                            ElseIf TextLine.Contains(vbTab) Then
                                delimiter = vbTab
                            ElseIf TextLine.Contains("|") Then
                                delimiter = "|"
                            ElseIf TextLine.Contains(";") Then
                                delimiter = ";"
                            End If

                            index = 1
                            HeaderRow = Split(TextLine, delimiter)
                            HeaderRowCount = HeaderRow.Length

                            For k = 0 To HeaderRowCount - 1
                                If HeaderRow(k).ToUpper.Equals(choice) Then
                                    i = k
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                Loop

                Dim tLine() As String = Nothing

                Dim Row As String = ""

                dt.Columns.Add(choice.ToUpper)
                dt.Columns.Add("COUNTS")
                Dim sorted = (From pair In countsClass._values Order By pair.Value Ascending)

                For Each item In sorted
                    If countValue = 0 Then
                        lowestValue = countsClass.GetPropertyValue(item.Key)
                        countValue = 1
                    Else
                        If countsClass.GetPropertyValue(item.Key) > highestValue Then
                            highestValue = countsClass.GetPropertyValue(item.Key)
                        End If

                    End If

                Next

                helpBeaner.ChartHighValue = highestValue
                helpBeaner.ChartLowValue = lowestValue

                rangevalue = highestValue - lowestValue

                If rangevalue = 0 And Not highestValue = 0 Then
                    widthValue = 1
                    classCount = 1
                ElseIf rangevalue = 1 And classCount = 1 Then
                    widthValue = 1
                ElseIf rangevalue = 2 Then
                    classCount = 3
                    widthValue = 1
                ElseIf rangevalue = 3 Then
                    classCount = 4
                    widthValue = 1
                ElseIf rangevalue = 4 Then
                    classCount = 5
                    widthValue = 1
                ElseIf rangevalue = 5 Then
                    classCount = 6
                    widthValue = 1
                ElseIf rangevalue > 5 And rangevalue <= 10 Then
                    classCount = 10
                ElseIf rangevalue > 10 Then
                    classCount = 20
                End If

                If widthValue = 1 Then
                    widthValue = 1
                Else
                    If Not highestValue = 0 AndAlso Not classCount = 0 Then
                        widthValue = highestValue / classCount
                    Else
                        MessageBox.Show("No " & choice.ToUpper & " Counts Found!", "Distribution Method", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Try
                    End If
                End If


                Dim a As Double()() = New Double(classCount)() {}
                    a(0) = New Double() {0, 0}

                    For m As Integer = 1 To classCount
                        If m = 1 Then
                            If lowestValue = 1 And widthValue = 1 Then
                                a(m) = New Double() {lowestValue - 0.5, lowestValue + 0.5}
                                limit = lowestValue

                            Else
                                a(m) = New Double() {lowestValue - 0.5, lowestValue + widthValue + 0.5}
                                limit = lowestValue + widthValue
                            End If

                        Else
                            If m = classCount Then
                                a(m) = New Double() {limit + 0.5, highestValue + 0.5}
                                '        limit = limit + widthValue
                            Else
                                a(m) = New Double() {limit + 0.5, limit + widthValue + 0.5}
                                limit = limit + widthValue
                            End If

                        End If

                    Next

                    Dim sortedDictionary = (sorted.ToDictionary(Function(p) p.Key, Function(p) p.Value))

                    Dim countsClass1 As New CountsClass

                    For Each item In sortedDictionary
                        Dim h As Integer = 0
                        Dim s As Integer = 0
                        Dim propertyName As String = Nothing

                        For s = 1 To classCount

                            Dim stString As String = (a(s)(0)).ToString
                            Dim endString As String = (a(s)(1)).ToString

                            propertyName = stString & " to " & endString

                            h = countsClass1.GetPropertyValue(propertyName)
                            If h = 0 Then
                                countsClass1.SetPropertyValue(propertyName, 0)
                            End If

                            If item.Value >= a(s)(0) And item.Value <= a(s)(1) Then

                                h = countsClass1.GetPropertyValue(propertyName)
                                h = h + 1
                                countsClass1.SetPropertyValue(propertyName, h)
                                Exit For
                            End If
                        Next s



                    Next

                    For Each item In countsClass1._values

                        Row = item.Key & delimiter & countsClass1.GetPropertyValue(item.Key)
                        tLine = Split(Row, delimiter)
                        dt.Rows.Add(tLine)
                    Next
                    If objReader IsNot Nothing Then
                        objReader.Close()
                        objReader = Nothing
                    End If

                Else
                    MessageBox.Show("File Does Not Exist", "Distribution Method", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("Distribution Error is " & ex.Message, "Analyzedata Call", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()

        End Try

        Return dt

    End Function



    Public Sub LoadColumns(ByVal headerRow As String, ByRef dt As DataTable, ByVal analyzeData As Boolean)

        Dim OHelper As New OysterHelper

        OHelper.LoadColumns(headerRow, dt, analyzeData)
        'Dim delimiter As String = Nothing
        'Dim count As Integer = 0

        'Try
        '    If headerRow.Contains(",") Then
        '        delimiter = ","
        '    ElseIf headerRow.Contains(vbTab) Then
        '        delimiter = vbTab
        '    ElseIf headerRow.Contains("|") Then
        '        delimiter = "|"
        '    ElseIf headerRow.Contains(";") Then
        '        delimiter = ";"
        '    End If
        '    Dim newline() As String = Split(headerRow, delimiter)

        '    Dim Row1 As String() = Split(headerRow, delimiter)


        '    If analyzeData Then
        '        AnalyzeDataGridView.Columns.Clear()

        '        If AnalyzeDataGridView.Columns.Count = 0 Then

        '            For i = 0 To Row1.Count - 1
        '                If analyzeData And count = 0 Then

        '                    Dim chk As New DataGridViewCheckBoxCell

        '                    dt.Columns.Add(newline(i).ToUpper)
        '                    count = 1
        '                Else
        '                    dt.Columns.Add(newline(i).ToUpper)
        '                End If
        '            Next

        '        End If
        '    ElseIf Not analyzeData Then

        '        FileDataGridView.Columns.Clear()

        '        For i = 0 To Row1.Count - 1
        '            If Not analyzeData And count = 0 Then

        '                Dim chk As New DataGridViewCheckBoxCell

        '                dt.Columns.Add(newline(i).ToUpper)
        '                count = 1
        '            Else
        '                dt.Columns.Add(newline(i).ToUpper)
        '            End If
        '        Next

        '    End If

        'Catch ex As Exception

        '    MessageBox.Show("LoadColumns error is " & ex.Message, "Analyzedata Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End Try

    End Sub


    Public Function AnalyzeData(ByRef dt As DataTable, ByVal FileNamePath As String) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim delimiter As String = Nothing
        Dim index As Integer = 0
        Dim countsClass As New CountsClass
        Dim Row As String = Nothing

        Try

            If System.IO.File.Exists(FileNamePath) = True Then

                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)


                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If index > 0 Then
                        SplitLine = Split(TextLine, delimiter)
                        For i = 0 To HeaderRowCount - 1
                            If Not String.IsNullOrEmpty(SplitLine(i)) Then
                                '    Console.WriteLine(HeaderRow(i) & " value " & SplitLine(i))
                                Dim j As Integer = countsClass.GetPropertyValue(HeaderRow(i))
                                j = j + 1
                                countsClass.SetPropertyValue(HeaderRow(i), j)
                            End If
                        Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        countsClass.LoadColumns(TextLine, dt, True)
                        index = 1
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length

                        For i = 0 To HeaderRowCount - 1

                            countsClass.SetPropertyValue(HeaderRow(i), 0)
                        Next

                    End If

                Loop


                For i = 0 To HeaderRowCount - 1
                    If i < HeaderRowCount - 1 Then
                        Row = Row & countsClass.GetPropertyValue(HeaderRow(i)).ToString & delimiter
                    Else
                        Row = Row & countsClass.GetPropertyValue(HeaderRow(i)).ToString
                    End If
                Next

                Dim tLine() As String = Split(Row, delimiter)
                dt.Rows.Add(tLine)
                If objReader IsNot Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If

            Else
                MessageBox.Show("File Does Not Exist", "Analyzedata Call", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception

            MessageBox.Show("AnalyzeData Error is " & ex.Message, "Analyze Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        Return dt

    End Function

    Public Function NewDistribution(ByRef dt As DataTable, ByVal FileNamePath As String, ByVal columnChoice As String, ByVal Top As Integer) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim Header As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim choice As String = columnChoice
        Dim i As Integer = 0
        Dim aTop As Integer = Top
        Dim aCount As Integer = 0
        Dim delimiter As String = Nothing
        Dim index As Integer = 0
        Dim j As Integer = 0
        Dim countsClass As New CountsClass
        Dim lowestValue As Integer = 0
        Dim highestValue As Integer = 0
        Dim sb As New StringBuilder

        Try
            If LoadToErrorRB.Checked Then
                ErrorsRichTextBox.Clear()
                sb.AppendLine(vbCrLf & "Attribute " & choice)
                If RawRB.Checked Then
                    sb.AppendLine(vbCrLf & "Raw Checked" & vbCrLf)
                ElseIf LettersOnlyUpperCaseRB.Checked Then
                    sb.AppendLine(vbCrLf & "Letters Only UpperCase Checked" & vbCrLf)
                ElseIf SoundexRB.Checked Then
                    sb.AppendLine(vbCrLf & "Soundex Checked" & vbCrLf)
                ElseIf NumbersOnlyRB.Checked Then
                    sb.AppendLine(vbCrLf & "Numbers OnLy Checked" & vbCrLf)
                End If
            ElseIf AppendToCurrentRB.Checked Then
                ' sb = New StringBuilder(ErrorsRichTextBox.Text)
                sb.Append(ErrorsRichTextBox.Text)
                ErrorsRichTextBox.Clear()
                sb.AppendLine(vbCrLf & "Attribute " & choice)
                If RawRB.Checked Then
                    sb.AppendLine(vbCrLf & "Raw Checked" & vbCrLf)
                ElseIf LettersOnlyUpperCaseRB.Checked Then
                    sb.AppendLine(vbCrLf & "Letters Only UpperCase Checked" & vbCrLf)
                ElseIf SoundexRB.Checked Then
                    sb.AppendLine(vbCrLf & "Soundex Checked" & vbCrLf)
                ElseIf NumbersOnlyRB.Checked Then
                    sb.AppendLine(vbCrLf & "Numbers OnLy Checked" & vbCrLf)
                End If
            End If

            If System.IO.File.Exists(FileNamePath) = True Then

                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)

                Do While objReader.Peek() <> -1
                    TextLine = objReader.ReadLine()
                    If index > 0 Then

                        SplitLine = Split(TextLine, delimiter)

                        If Not String.IsNullOrEmpty(SplitLine(i)) Then

                            If RawRB.Checked Then
                                j = countsClass.GetPropertyValue(SplitLine(i))
                                j = j + 1
                                countsClass.SetPropertyValue(SplitLine(i), j)
                                aCount = aCount + 1
                                If LoadToErrorRB.Checked Or AppendToCurrentRB.Checked Then
                                    sb.AppendLine(TextLine)
                                End If
                            ElseIf LettersOnlyUpperCaseRB.Checked Then
                                Dim temp As String = Regex.Replace(SplitLine(i), "\d", String.Empty)
                                temp = Regex.Replace(temp, "[ ](?=[ ])|[-_/,]+", String.Empty)
                                temp = Regex.Replace(temp, "[^a-zA-Z ]", String.Empty)

                                If Not temp = String.Empty And Not String.IsNullOrEmpty(temp) And Not String.IsNullOrWhiteSpace(temp) Then
                                    j = countsClass.GetPropertyValue(temp.ToUpper)
                                    j = j + 1
                                    countsClass.SetPropertyValue(temp.ToUpper, j)
                                    aCount = aCount + 1
                                    If LoadToErrorRB.Checked Or AppendToCurrentRB.Checked Then
                                        sb.AppendLine(TextLine)
                                    End If
                                End If
                            ElseIf SoundexRB.Checked Then
                                '   [ ](?=[ ])|[^-_,A-Za-z0-9 ]+
                                ' [^\w\s]
                                Dim temp As String = Regex.Replace(SplitLine(i), "[^A-Za-z0-9\-/]", String.Empty)
                                temp = Regex.Replace(temp, "\d", String.Empty)
                                temp = Regex.Replace(temp, "[ ](?=[ ])|[-_/,]+", String.Empty)
                                If Not temp = String.Empty And Not String.IsNullOrEmpty(temp) And Not String.IsNullOrWhiteSpace(temp) Then
                                    j = countsClass.GetPropertyValue(Compute(temp, temp.Length))
                                    j = j + 1
                                    countsClass.SetPropertyValue(Compute(temp, temp.Length), j)
                                    aCount = aCount + 1
                                    If LoadToErrorRB.Checked Or AppendToCurrentRB.Checked Then
                                        sb.AppendLine(TextLine)
                                    End If
                                End If
                            ElseIf NumbersOnlyRB.Checked Then

                                Dim temp As String = Regex.Replace(SplitLine(i), "[^\d]", String.Empty)
                                If Not temp = String.Empty And Not String.IsNullOrEmpty(temp) And Not String.IsNullOrWhiteSpace(temp) Then
                                    j = countsClass.GetPropertyValue(temp.ToString)
                                    j = j + 1
                                    countsClass.SetPropertyValue(temp.ToString, j)
                                    aCount = aCount + 1
                                    If LoadToErrorRB.Checked Or AppendToCurrentRB.Checked Then
                                        sb.AppendLine(TextLine)
                                    End If
                                End If
                            Else

                                MessageBox.Show("Choose a Radio Button!", "Radio Button Choice", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            End If

                        End If
                        '  Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        index = 1
                        sb.AppendLine(TextLine)
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length
                        'get what we are looking for
                        For k = 0 To HeaderRowCount - 1
                            If HeaderRow(k).ToUpper.Equals(choice.ToUpper) Then
                                i = k
                                Exit For
                            End If
                        Next
                    End If

                Loop

                If LoadToErrorRB.Checked Then
                    LoadToErrorRB.Checked = False

                    ErrorsRichTextBox.Text = sb.ToString
                ElseIf AppendToCurrentRB.Checked Then
                    AppendToCurrentRB.Checked = False
                    ErrorsRichTextBox.AppendText(sb.ToString)
                End If
                dt.Clear()
                Dim tLine() As String = Nothing
                Dim Row As String = Nothing

                dt.Columns.Add(choice.ToUpper)
                dt.Columns.Add("COUNTS")

                Dim totalCounts As Integer = 0
                Dim aSorted = (From pair In countsClass._values Order By pair.Value Descending)
                Dim counter As Integer = 0
                For Each item In aSorted
                    totalCounts = totalCounts + 1
                Next
                Dim data() As Double = {}
                ReDim data(totalCounts - 1)
                Dim theCount As Double = aCount / totalCounts
                Dim sorted = Nothing
                If UseTopRB.Checked Then
                    sorted = (From pair In countsClass._values Order By pair.Value Descending).Take(aTop)
                Else
                    sorted = (From pair In countsClass._values Order By pair.Value Descending)
                End If
                '  Dim sorted = (From pair In countsClass._values Order By pair.Value Descending).Take(aTop)

                For Each item In sorted
                    If counter = 0 Then
                        highestValue = countsClass.GetPropertyValue(item.Key)
                    End If
                    data.SetValue(countsClass.GetPropertyValue(item.Key), counter)
                    counter = counter + 1

                    If RawRB.Checked Then
                        Row = item.Key & delimiter & countsClass.GetPropertyValue(item.Key)
                    ElseIf LettersOnlyUpperCaseRB.Checked Then
                        Row = item.Key & delimiter & countsClass.GetPropertyValue(item.Key)
                    ElseIf SoundexRB.Checked Then
                        Row = item.Key & delimiter & countsClass.GetPropertyValue(item.Key)
                    ElseIf NumbersOnlyRB.Checked Then
                        Row = item.Key & delimiter & countsClass.GetPropertyValue(item.Key)
                    End If
                    tLine = Split(Row, delimiter)
                    dt.Rows.Add(tLine)
                Next

                Dim dBle As Double = CountsClass.StandardDeviation(data)
                HighestLBL.Text = "Max Freq: " & highestValue.ToString
                If theCount = 0 Or theCount.ToString = "NaN" Then
                    AvgLbl.Text = "Avg Freq: " & 0
                Else
                    AvgLbl.Text = "Avg Freq: " & Math.Round(theCount, 3, MidpointRounding.AwayFromZero).ToString
                End If

                If dBle = 0 Then
                    StdDevLbl.Text = "Std Dev: " & 0
                Else
                    StdDevLbl.Text = "Std Dev: " & Math.Round(dBle, 3, MidpointRounding.AwayFromZero).ToString
                End If

                If StdDevLbl.Text.Equals("NaN") Then
                    StdDevLbl.Text = "Std Dev: " & 0
                Else
                    StdDevLbl.Text = "Std Dev: " & Math.Round(dBle, 3, MidpointRounding.AwayFromZero).ToString
                End If



                If objReader IsNot Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If


            End If

        Catch ex As Exception
            MessageBox.Show("CountsClass Distribution Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally

            If UseTopRB.Checked Then
                UseTopRB.Checked = False
            End If
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try

        Return dt

    End Function

    Private Sub LoadCheckedListBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LoadCheckedListBox.SelectedIndexChanged
        Dim idx, sidx As Integer
        sidx = LoadCheckedListBox.SelectedIndex
        For idx = 0 To LoadCheckedListBox.Items.Count - 1
            If idx <> sidx Then
                LoadCheckedListBox.SetItemChecked(idx, False)
            Else
                LoadCheckedListBox.SetItemChecked(sidx, True)
            End If
        Next

    End Sub

    Private Sub HeaderCheckedListBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles HeaderCheckedListBox.SelectedIndexChanged
        Dim idx, sidx As Integer
        sidx = HeaderCheckedListBox.SelectedIndex
        For idx = 0 To HeaderCheckedListBox.Items.Count - 1
            If idx <> sidx Then
                HeaderCheckedListBox.SetItemChecked(idx, False)
            Else
                HeaderCheckedListBox.SetItemChecked(sidx, True)
            End If
        Next

    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Dispose()
    End Sub

    Private Sub CreateDisBtn_Click_1(sender As Object, e As EventArgs) Handles CreateDisBtn.Click
        Dim dt As New DataTable

        Dim FileNamePath As String = Nothing
        Dim choice As String = Nothing

        Try
            If Not String.IsNullOrEmpty(AnalyzeDataTxBx.Text) Then
                FileNamePath = AnalyzeDataTxBx.Text
                choice = HeaderCheckedListBox.GetItemText(item:=HeaderCheckedListBox.SelectedItem)
            Else
                MessageBox.Show("You must Load a File to Analyze!", "Analyze Call", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Exit Sub
            End If

            If String.IsNullOrEmpty(choice) Then
                MessageBox.Show("You must choice an Attribute for the Distribution Graphing!", "Analyze Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            Dim YaxisValue As Integer = 0

            Chart1.ChartAreas(0).AxisY.IsStartedFromZero = False

            helpBeaner = New HelperBean

            dt = Distribution(dt, FileNamePath, choice, helpBeaner)

            Chart1.ChartAreas(0).AxisX.LabelAutoFitMaxFontSize = 8
            Chart1.ChartAreas(0).AxisX.IsLabelAutoFit = True


            Chart1.Series(0).SetCustomProperty("PointWidth", "1")
            Chart1.ChartAreas(0).AxisX.LabelStyle.Enabled = True
            Chart1.ChartAreas(0).AxisX.LabelStyle.Angle = 45
            Chart1.ChartAreas(0).AxisX.MinorTickMark.Enabled = True
            Chart1.ChartAreas(0).AxisX.Interval = 1

            Chart1.AlignDataPointsByAxisLabel()
            Chart1.ChartAreas(0).AxisX.Title = choice & " Class Ranges"
            Chart1.ChartAreas(0).AxisY.Title = "Counts for Class Range"
            Chart1.Legends(0).Enabled = False
            Chart1.Series(0).Points.DataBindXY(dt.Rows, choice, dt.Rows, "counts")
            '  ChartAreas[0].AxisX.LabelStyle.Angle = 45
        Catch ex As Exception
            MessageBox.Show("Create Distribution Button call error is " & ex.Message, "Analyze Button Call", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub

    Private Sub LookAtBtn_Click(sender As Object, e As EventArgs) Handles LookAtBtn.Click

        Dim choice As String = Nothing
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Cursor = Cursors.WaitCursor
            choice = LoadCheckedListBox.SelectedItem.ToString

            If String.IsNullOrEmpty(choice) Or choice.Equals(Nothing) Then
                MessageBox.Show("You must choose an Attribute to View Stats!", "Stats Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If RawRB.Checked Then
            ElseIf LettersOnlyUpperCaseRB.Checked Then
            ElseIf SoundexRB.Checked Then
            ElseIf NumbersOnlyRB.Checked Then
            Else
                MessageBox.Show("You must choice a Radio Button!", "Pick One", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            Dim top As Integer = CType(TopTxBx.Text, Integer)
            dt = NewDistribution(dt, AnalyzeDataTxBx.Text, choice, top)
            bSource.DataSource = dt
            AttribueDataGridView.DataSource = bSource
            AttribueDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells


        Catch ex As Exception
            MessageBox.Show("Create View Stats error is " & ex.Message, "View Stats Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Public Shared Function Compute(ByVal Word As String) As String
        Return Compute(Word, 4)
    End Function
    Public Shared Function GetCode(ByVal Achar As Char) As String

        Dim mChar As String = Nothing
        Select Case Achar

            Case "B", "F", "P", "V"
                mChar = "1"
            Case "C", "G", "J", "K", "Q", "S", "X", "Z"
                mChar = "2"
            Case "D", "T"
                mChar = "3"
            Case "L"
                mChar = "4"
            Case "M", "N"
                mChar = "5"
            Case "R"
                mChar = "6"
            Case "H", "W"
                mChar = "#"
            Case Else
                mChar = ""
        End Select
        Return mChar
    End Function



    Public Shared Function Compute(ByVal Word As String, ByVal Length As Integer) As String

        ' Value to return
        Dim Value As String = Nothing
        ' Size of the word to process
        Dim Size As Integer = Word.Length


        If (Size > 0) Then
            '      If (Size > 1) Then
            ' Convert the word to all uppercase
            Word = Word.ToUpper()
            ' Conver to the word to a character array for faster processing
            Dim Chars() As Char = Word.ToCharArray()
            ' Buffer to build up with character codes
            Dim Buffer As New System.Text.StringBuilder
            Buffer.Length = 0
            ' The current and previous character codes
            Dim PrevCode As Integer = 0
            Dim CurrCode As Integer = 0
            Dim SecndCode As String = Nothing
            ' Append the first character to the buffer
            Buffer.Append(Chars(0))
            ' Prepare variables for loop
            Dim i As Integer
            Dim LoopLimit As Integer = Size - 1
            If LoopLimit = 0 And Buffer.Length = 1 Then
                Buffer.Append("0", (3))
                ' Set the return value
                Value = Buffer.ToString()
                Return Value
            End If
            Dim previous As String = Nothing
            Dim counter As Integer = 0

            previous = GetCode(Chars(0))

            For i = 1 To LoopLimit

                Dim current As String = GetCode(Chars(i))
                If Not current.Equals("#") Then

                    If current.Length > 0 AndAlso Not current.Equals(previous) Then
                        Buffer.Append(current)
                        previous = current
                    Else
                        If Not current.Equals("") Then
                            previous = current
                        ElseIf i >= 1 And current.Equals("") Then
                            previous = current

                        End If

                    End If
                Else

                End If

            Next

            If (Buffer.Length < 4) Then
                Buffer.Append("0", (4 - Buffer.Length))
            End If

            ' Set the return value
            Value = Buffer.ToString()

            Value = Value.Substring(0, 4)
        End If
        ' Return the computed soundex
        Return Value
    End Function



    Private Sub FindErrorsBtn_Click(sender As Object, e As EventArgs) Handles FindErrorsBtn.Click

        Cursor = Cursors.WaitCursor
        Dim TextLine As String = Nothing
        Dim SplitLine() As String = Nothing
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim sb2 As String = "Hits: "
        Dim ErrorCounter As Integer = 0
        Try

            Dim FileNamePath As String = LFileLbl.Text.Substring(ErrorsLbl.Text.IndexOf(":") + 2).Trim

            If System.IO.File.Exists(FileNamePath) = True Then

                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)
                Dim index As Integer = 0

                Dim delimiter As String = Nothing

                Dim countsClass As New CountsClass
                '    Dim iCount As Integer = 0
                Dim str As String = Nothing

                '     Dim characters() As Char = {"<"c, ">"c, "="c, "!"c, "$"c, "*"c, "_"c, "+"c, "?"c}
                Dim astring() As String = {"NA", "VALUE", "NAME", "''", "NANA", "NANANA", "UNKNOWN", "#NAME?", "NULL", "U", "_", "# U", "<", ">", "!", "$", "=", "^", "%", "?", "+", ":", "AAA", "BBB", "CCC", "DDD",
                    "EEE", "FFF", "GGG", "HHH", "JJJ", "KKK", "LLL", "MMM", "NNN", "OOO", "PPP", "QQQ", "RRR", "SSS", "TTT", "UUU", "VVV", "WWW", "XXX", "YYY", "ZZZ", """"}

                Dim FList As New ArrayList
                Dim sb As New StringBuilder
                ' Console.WriteLine(Now.ToLongTimeString)
                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If Not String.IsNullOrEmpty(TextLine) AndAlso Not String.IsNullOrWhiteSpace(TextLine) Then
                        If index = 0 Then
                            sb.AppendLine(TextLine)
                            index = 1

                        End If

                        If delimiter = Nothing Then
                            If TextLine.Contains(vbTab) Then
                                delimiter = vbTab
                            ElseIf TextLine.Contains("|") Then
                                delimiter = "|"
                            ElseIf TextLine.Contains(";") Then
                                delimiter = ";"
                            ElseIf TextLine.Contains(",") Then
                                delimiter = ","

                            End If

                        End If
                        If index = 1 Then
                            str = delimiter & "NA" & delimiter
                            astring.SetValue(str, 0)
                            str = delimiter & "VALUE" & delimiter
                            astring.SetValue(str, 1)
                            str = delimiter & "NAME" & delimiter
                            astring.SetValue(str, 2)
                            str = delimiter & "NULL" & delimiter
                            astring.SetValue(str, 7)
                            str = delimiter & "U" & delimiter
                            astring.SetValue(str, 8)
                            str = delimiter & "_" & delimiter
                            astring.SetValue(str, 9)

                            index = 2


                        End If


                        Dim ECounter As Integer = 0

                        For theIndex = 0 To astring.Count - 1
                            If Not TextLine.ToUpper.IndexOf(astring(theIndex).ToUpper) = -1 Then
                                sb.AppendLine(TextLine)
                                ErrorCounter += 1

                                If Not sb2.Contains(astring(theIndex).ToUpper) Then
                                    If theIndex = astring.Count - 1 Then
                                        sb2 = sb2 & astring(theIndex).ToUpper
                                    Else
                                        sb2 = sb2 & astring(theIndex).ToUpper & ","

                                    End If


                                    If sb2.Length > 200 And ECounter = 0 Then
                                        sb2 = sb2 & vbCrLf
                                        ECounter = 1
                                    ElseIf sb2.Length > 400 And ErrorCounter = 1 Then
                                        sb2 = sb2 & vbCrLf
                                        ECounter = 2

                                    End If
                                End If
                            End If
                        Next
                    End If


                Loop

                ErrorsRichTextBox.Text = sb.ToString
                FindsLBL.Text = sb2.Substring(0, sb2.Length - 1)
                ECountLbl.Text = "Possible Erorrs Line Count is: " & ErrorCounter.ToString
                If objReader IsNot Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If

            Else
                MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            MessageBox.Show("AnalyzeData Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Cursor = Cursors.Default
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try



    End Sub

    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Return value.Count(Function(c As Char) c = ch)
    End Function

    Private Sub LoadFileGrid_Click(sender As Object, e As EventArgs) Handles LoadFileGrid.Click

        Cursor = Cursors.WaitCursor

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim delimiter As String = Nothing
        Dim dt As New DataTable
        Dim binding As New BindingSource
        Dim index As Integer = 0
        Dim sb As New StringBuilder
        Dim countsClass As New CountsClass
        Dim CountBadRows As Integer = 0


        Try
            Dim Phile As String = LFileLbl.Text.Substring(LFileLbl.Text.IndexOf(":") + 2).Trim

            If System.IO.File.Exists(Phile) = True Then

                Dim objReader As New System.IO.StreamReader(Phile, Encoding.ASCII)

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If index > 0 Then

                        SplitLine = Split(TextLine, delimiter)
                        'some data has too many columns so ignore
                        Try
                            SplitLine = Split(TextLine, delimiter)
                            If Not SplitLine.Count > HeaderRowCount Then
                                dt.Rows.Add(SplitLine)
                            Else
                                CountBadRows += 1
                                sb.AppendLine(TextLine)

                            End If

                        Catch ex As Exception
                            Console.WriteLine(ex.Message)
                        End Try

                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        countsClass.LoadColumns(TextLine, dt, False)
                        sb.AppendLine(TextLine)
                        index = 1
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length

                    End If
                Loop
                binding.DataSource = dt
                FileDataGridView.DataSource = binding

                If objReader IsNot Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If

                If CountBadRows > 0 Then
                    MessageBox.Show(CountBadRows.ToString & " Bad Rows were skipped Loading! " & vbCrLf & "Those lines had too many columns!" & vbCrLf & "These lines were added to the Errors Tab!", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ErrorsRichTextBox.Text = sb.ToString
                    ECountLbl.Text = "Possible Errors Line Count is: " & CountBadRows.ToString
                    FindsLBL.Text = "Hits: "
                    FreqDistTabs.SelectedTab = FindErrors

                End If

            Else
                MessageBox.Show("File does Not Exist!", "Exception Thrown Loading File", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            MsgBox("Error Loading View File is " & ex.Message)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub Sellect_AllToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Sellect_AllToolStripMenuItem.Click
        ErrorsRichTextBox.SelectAll()
    End Sub

    Private Sub CopyToClipboardToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyToClipboardToolStripMenuItem.Click

        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If ErrorsRichTextBox.SelectionLength > 0 Then
                ErrorsRichTextBox.Copy()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub PasteFromClipboardToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PasteFromClipboardToolStripMenuItem1.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            '     Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If ErrorsRichTextBox.CanPaste(MyFormat) Then
                ErrorsRichTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try
    End Sub

    Private Sub SaveErrorsBtn_Click(sender As Object, e As EventArgs) Handles SaveErrorsBtn.Click

        Dim Strng As String = Nothing
        Dim SaveFileDialog1 As New SaveFileDialog()
        Dim strErr() As String
        Dim newStr As String = Nothing

        Try

            SaveFileDialog1.Filter = "Text|*.txt|Text|*.csv"
            SaveFileDialog1.Title = "Save as a File"
            SaveFileDialog1.ShowDialog()
            If FindsLBL.Text.Trim.Length > 5 Then

                strErr = Split(FindsLBL.Text, ",")

                For Each s As String In strErr

                    If Not String.IsNullOrEmpty(s) Then

                        If s.Contains(ControlChars.Tab) Then
                            If s.Contains("NA") Then
                                s = "NA"
                            ElseIf s.Contains("U") AndAlso Not s.Contains("#") Then
                                s = "U"
                            End If
                            newStr = newStr & s.Trim & " , "
                        Else
                            ' If Not newStr = Nothing Then
                            newStr = newStr & s.Trim & " , "
                            '  End If

                        End If

                    End If

                Next

            End If

            Dim cnt As Integer = ErrorsRichTextBox.Lines.Count
            If Not ECountLbl.Text.Contains(cnt.ToString) Then
                ECountLbl.Text = "Possible Error Line Count is: " & ErrorsRichTextBox.Lines.Count
            End If

            If Not newStr = Nothing Then
                newStr = "Hits: " & newStr.Substring(0, newStr.Length - 1)
            Else
                newStr = ""
            End If

            If SaveFileDialog1.FileName <> "" Then
                Dim header As String = vbCrLf & "------------------------------------------------------------------------------" & vbCrLf & Now.ToLongDateString & vbCrLf & vbCrLf & ErrorsLbl.Text & vbCrLf & vbCrLf & newStr & vbCrLf & vbCrLf & ECountLbl.Text & vbCrLf & vbCrLf & "------------------------------------------------------------------------------" & vbCrLf & vbCrLf
                ErrorsRichTextBox.Text = header & ErrorsRichTextBox.Text
                ErrorsRichTextBox.SaveFile(SaveFileDialog1.FileName, RichTextBoxStreamType.PlainText)

            Else


            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


End Class

