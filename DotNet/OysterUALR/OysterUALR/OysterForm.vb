﻿'''' <summary>
''''     Oyster Helper and ERMetrics Helper called Oyster Helper
'''' '    
''''' Classes
''''     Constants - Shared String Constants as Protected Shared Const for use in the program mainly for Messaging and constants representing objects such as Tabs,RichTextBoxes,etc.
''''     HelperBean - Bean with Getters and Setters for working storage and working with file saving in Treeviews, etc.
''''     HelperTabControl - On Tab Change Class - Makes sure the tab chosen is displayed correctly for existing and saved input data.
''''     HelpForm - Help. 
''''     OysterForm - This file. Main Windows Application Form with button clicks etc, Methods with IO, etc., moved to OysterHelper Class as good programming standard procedure.
''''     OysterHelper - Guts of the Work Methods, See file for list of methods contained in the class
''''     ProcessClass - Run Method Only to start Window Process CMD.EXE for Running Oyster and ERMetrics in Java and Sending Output Back to OysterHelper
''''     FormResizer - In Case you Cannot See, Makes OysterHelper Large and Then Small Again when desired.
''''     ReadFileForm - shows the testing file created
''''     TesterRows - Holds the Output Rows for testing
''''     TestHelper - Holds the majority of The Testing methods
''''     TestOutPutForm - Holds the testing output in a datagridview
''''     TestSignInForm - used to sign into the Testing Tab
''''     AnalyzeForm - used to analyze input files
''''     CountsClass - used to help analyze input files
''''
''''     A Second Project OysterGUI creates the install for Oyster Helper
''''
''''
''''
'''' </summary>
''''
Imports System.IO
Imports System.Security.Permissions
Imports System.Text
Imports System.Threading
Imports Newtonsoft.Json

Public Class Oysterform

    'Used by the Oyster & ERMetrics process when running to write to correspond.. to the screen in this case
    Private Delegate Sub ConsoleOutputDelegate(ByVal outputString As String)
    Private Delegate Sub ConsoleErrorDelegate(ByVal errorString As String)
    Private Delegate Sub ConsoleOutputPythonDelegate(ByVal outputString As String)
    Private Delegate Sub ConsoleErrorPythonDelegate(ByVal errorString As String)

    'used to resize the program if chosen
    Private RS As New FormResizer

    'HashTable used to hold helperbeans with RichTextBox loaded file save Information
    Protected Friend saveInfoCollection As New Hashtable()

    'OysterHelper used to have form I.O. work and storage
    Private oysterHelper As New OysterHelper

    'we have to keep up with what is happening
    Private helperBean As New HelperBean

    'required shared value saying if java is running
    'even if it is a crash or a normal close. Is Oyster
    'Running is the question
    Protected Friend OysterRunning As Boolean = False

    'used to save helperbeans for easy access
    Protected Friend list As New List(Of HelperBean)()

    'keep the tab choices close And under control And re-loaded On tabclick
    Protected Friend helperTabControl As New HelperTabControl

    'used to get elapsed time in oysterRun
    Protected Friend stopWatch As New Stopwatch
    'used to load and unload TestTab

    'used for tab hiding and showing
    Protected Friend ListA As New List(Of TabPage)()

    'used by assertion tab 
    Private selectedrb As RadioButton
    Private dta As DataTable = Nothing

    'Next three used in consoleoutput methods
    'used to pipe output from Oyster,ERmetrics,Python
    Dim myCounter As Integer = 1
    Dim runAgain As Boolean = True
    Public Shared RunCount As Integer = 0


    '-----------------------------------------------------------------
    'Startup and load the screens if saved info available
    '-----------------------------------------------------------------

    Protected Overrides Sub OnLoad(e As EventArgs)


        'for resizingu
        RS.FindAllControls(Me)
        'want to save any user input when exiting
        My.Application.SaveMySettingsOnExit = True

        Dim f As New FileIOPermission(PermissionState.None)
        f.AllLocalFiles = FileIOPermissionAccess.Append
        Try
            f.Demand()
        Catch s As Exception
            MessageBox.Show(s.Message, "Permissions Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        ToolStripStatusLabel1.Text = Constants.OHelper
        ToolStripStatusLabel1.Visible = True
        ToolStripProgressBar1.Visible = False
        RunScriptNamelbl.Text = Nothing
        InputFileLbl.Text = Nothing
        LinkFilelbl.Text = Nothing
        Timelbl.Text = Nothing
        LFDateLbl.Text = Nothing
        RunTimeValuelbl.Text = Nothing

        'To use User changeable settings you must load them
        'on startup to then load the TextBoxes in OysterHelper 
        'once saved in the program.
        My.Settings.Reload()


        'link label for Entity Resolution Scoring Rule Weights Generator
        LinkLabel1.Links.Add(0, 48, "https://yxye1.shinyapps.io/shinyapp/")
        'oysterHelper.RunProcesTabOnLoad(Constants.Welcome)

        'For Each page As TabPage In OysterTabs.TabPages
        '    oysterHelper.RunProcesTabOnLoad(page.Name)
        'Next


        'if user settings saved..load the textboxes with them
        PythonExeTxBx.Text = My.Settings.PythonExeDir
        RobotWkDirTxBx.Text = My.Settings.RobotWrkDir
        OysterRootDirTxBx.Text = My.Settings.OysterRootDir
        OysterWorkDirTxBx.Text = My.Settings.OysterWorkingDir
        ERMetricsTxBx.Text = My.Settings.ERMetricsWorkDir
        OysterJarTxBx.Text = My.Settings.OysterJar
        EROysterWkDirTxBx.Text = My.Settings.EROysterWorkDir
        ERMetricsJarTxBx.Text = My.Settings.ERMetricsJar
        'Oyster error label when an Error is returned from the Running Oyster Instance
        ErrorInRunLbl.Visible = False
        WelLoadedFileLbl.Text = Constants.FileLoadedIs

        'Oyster Working directory for ERMetrics comes from the Oyster Tab
        'since there is no way to load it from the ERMetrics Screen.
        '-changed NOW 5/6/2020
        'It now can be changed as needed.  If empty on startup but OysterWorkDir is not 
        If String.IsNullOrEmpty(My.Settings.EROysterWorkDir) And Not String.IsNullOrEmpty(My.Settings.OysterWorkingDir) Then                               '   And My.Settings.EROysterWorkDir.Equals(My.Settings.OysterWorkingDir) _


            EROysterWkDirTxBx.Text = My.Settings.OysterWorkingDir

            LFDateLbl.Text = Nothing
            HSLinkDateLbl.Text = Nothing

            If String.IsNullOrEmpty(ERMetLinkFileTxbx.Text) And File.Exists(ERMetLinkFileTxbx.Text) Then

                ERMetLinkFileTxbx.Text = OysterHelper.GetLinkFileName(My.Settings.OysterRootDir, My.Settings.EROysterWorkDir)
                LFDateLbl.Text = IO.File.GetLastWriteTime(ERMetLinkFileTxbx.Text)
                HSLinkDateLbl.Text = LFDateLbl.Text
                HSCsvTxBx.Text = OysterHelper.GetInputCSVFileName(EROysterWkDirTxBx.Text)
                HSLinkFileTxBx.Text = ERMetLinkFileTxbx.Text
                My.Settings.ERMetricsLinkFile = ERMetLinkFileTxbx.Text
            Else
                If File.Exists(My.Settings.ERMetricsLinkFile) Then
                    ERMetLinkFileTxbx.Text = My.Settings.ERMetricsLinkFile
                    HSLinkFileTxBx.Text = My.Settings.ERMetricsLinkFile
                End If

            End If

            EROysterWkDirTxBx.ReadOnly = False ' changed 
            ERMetLinkFileTxbx.ReadOnly = True
            '  HSLinkFileTxBx.ReadOnly = True
            '  HSCsvTxBx.ReadOnly = True

        Else

            EROysterWkDirTxBx.Text = My.Settings.EROysterWorkDir
            'EROysterWkDirTxBx.Text = My.Settings.OysterWorkingDir
            EROysterWkDirTxBx.ReadOnly = False  'changed
            ' HSLinkFileTxBx.ReadOnly = True
            ' My.Settings.EROysterWorkDir = My.Settings.OysterWorkingDir
            '   HSCsvTxBx.ReadOnly = True
        End If

        My.Settings.Save()

        'load the RunScripts if the Directories are already set
        If Not String.IsNullOrEmpty(OysterRootDirTxBx.Text) And OysterRootDirTxBx.Text.ToUpper.Contains(Constants.OYSTER) _
            And Not String.IsNullOrEmpty(OysterWorkDirTxBx.Text) And Not String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            OysterXmlListBx.DataSource = IO.Directory.GetFiles(OysterRootDirTxBx.Text, "*.xml") _
            .Select(Function(file) IO.Path.GetFileName(file)) _
            .ToList
            If Not My.Settings.LastRunFile = Nothing Then
                OysterXmlListBx.SelectedItem = My.Settings.LastRunFile
            End If
            'allow Oyster Button to be available to Run Oyster
            RunOyster.Enabled = True
            'If all is well, load the RichTextbox on the Oyster Setup and Run Tab
            ' Dim oysterHelper As New OysterHelper
            oysterHelper.RunProcessTab(Constants.Welcome)
            ' oysterHelper = Nothing
        Else
            RunOyster.Enabled = False
        End If

        If Not My.Settings.Testing Then

            ' Hide the testing tab
            Dim tab As TabPage = OysterTabs.TabPages.Item("TestTab")
            EnablePage(tab, False)

        End If

    End Sub

    '-----------------------------------------------------------------  
    'Save the Welcome/Setup and Run Oyster Tab
    '-----------------------------------------------------------------

    Private Sub SaveAll_Click(sender As Object, e As EventArgs) Handles SaveAll.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) _
            Or String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            MessageBox.Show(Constants.SaveMessage, "Requirement", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            Me.oysterHelper.Save_All_WelcomeTab()
            'If TestHelper.NewJarTesting = True Then
            '    Console.WriteLine("Here")
            '    TestJarForm.SaveJarBtn.PerformClick()
            'Else
            '    Console.WriteLine("Missed")
            'End If
        End If


    End Sub

    '-----------------------------------------------------------------
    'go to the first tab
    '-----------------------------------------------------------------
    Private Sub HomeBtn_Click(sender As Object, e As EventArgs) Handles HomeBtn.Click
        OysterTabs.SelectTab(0)
    End Sub


    'Close the program from the Exit button in the toolstrip menu
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click


        Me.Close()
    End Sub


    '-----------------------------------------------------------------
    ' The 5 Main Text Boxes are checked for accuracy and input from  
    ' the keyboard.  The Browse buttons should be used to           
    ' fill any textboxes, empty or already filled. Andy change      
    ' to add New textboxes will need to made in these 2 methods 
    ' to prevent random keyboard entry that is just asking for errors
    '-------------------------------------------------------------- --

    'check for textchanged by space or new entry in OyterRootDirTxtBx
    Private Sub RootDirTxtBx_TextChanged(sender As Object, e As EventArgs) Handles OysterRootDirTxBx.TextChanged
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterRootDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterRootDirTxBx)
        Else
            oysterHelper.TextChanged(OysterRootDirTxBx)
        End If

    End Sub




    'check for textchanged by space or new entry in  OysterWorkDirTxBx
    Private Sub OysterWorkDirTxBx_TextChanged(sender As Object, e As EventArgs) Handles OysterWorkDirTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterWorkDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterWorkDirTxBx)
        Else
            oysterHelper.TextChanged(OysterWorkDirTxBx)
        End If

    End Sub

    'check for textchanged by space or new entry in ERMetricsTxBx
    Private Sub ERMetricsTxBx_TextChanged(sender As Object, e As EventArgs) Handles ERMetricsTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(ERMetricsTxBx.Text) Then
            oysterHelper.TextChangedSpace(ERMetricsTxBx)
        Else
            oysterHelper.TextChanged(ERMetricsTxBx)
        End If


    End Sub

    'check for textchanged by space or new entry in OysterJarTxBx
    Private Sub OysterJarTxBx_TextChanged(sender As Object, e As EventArgs) Handles OysterJarTxBx.TextChanged
        '
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(OysterJarTxBx.Text) Then
            oysterHelper.TextChangedSpace(OysterJarTxBx)
        Else
            oysterHelper.TextChanged(OysterJarTxBx)
        End If

    End Sub

    'check for textchanged by space or new entry in ERMetricsJarTxtBx
    Private Sub EEROysterWkDirTxBx_TextChanged(sender As Object, e As EventArgs) Handles EROysterWkDirTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(EROysterWkDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(EROysterWkDirTxBx)
        Else
            oysterHelper.TextChanged(EROysterWkDirTxBx)
        End If

    End Sub
    'check for textchanged by space or new entry in ERMetricsJarTxtBx
    Private Sub ERMetricsJarTxtBx_TextChanged(sender As Object, e As EventArgs) Handles ERMetricsJarTxBx.TextChanged

        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(ERMetricsJarTxBx.Text) Then
            oysterHelper.TextChangedSpace(ERMetricsJarTxBx)
        Else
            oysterHelper.TextChanged(ERMetricsJarTxBx)
        End If

    End Sub

    Private Sub PythonExeTxBx_TextChanged(sender As Object, e As EventArgs) Handles PythonExeTxBx.TextChanged
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(PythonExeTxBx.Text) Then
            oysterHelper.TextChangedSpace(PythonExeTxBx)
        Else
            oysterHelper.TextChanged(PythonExeTxBx)
        End If

    End Sub

    Private Sub RobotWkDirTxBx_TextChanged(sender As Object, e As EventArgs) Handles RobotWkDirTxBx.TextChanged
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(RobotWkDirTxBx.Text) Then
            oysterHelper.TextChangedSpace(RobotWkDirTxBx)
        Else
            oysterHelper.TextChanged(RobotWkDirTxBx)
        End If

    End Sub

    '-------------------------------------------------------
    'end textchanged
    '-------------------------------------------------------

    '------------------------------------------------------------
    ' Browse buttons used to fill textboxres.  Any new textboxes 
    ' will need to be added to the generic FillTextBox method in
    ' Oysterhelper.
    '------------------------------------------------------------
    Private Sub OysterWkDirBtn_Click(sender As Object, e As EventArgs) Handles OysterWkDirBtn.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Then
            MessageBox.Show(Constants.OysterEmpty, "Oyster Root Directory Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            If String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Then
                FolderBrowserDialog1.SelectedPath = OysterRootDirTxBx.Text

            Else
                FolderBrowserDialog1.SelectedPath = OysterWorkDirTxBx.Text
            End If

            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                Dim DirChosen As String = FolderBrowserDialog1.SelectedPath
                oysterHelper.FillTextBox(OysterWorkDirTxBx, FolderBrowserDialog1.SelectedPath)
                If (DirChosen.Equals(OysterWorkDirTxBx.Text) And Not String.IsNullOrEmpty(OysterJarTxBx.Text)) Then
                    MessageBox.Show("Please Save Your Choices!", "Save Choices", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If

    End Sub


    Private Sub OysterRootDirBtn_Click(sender As Object, e As EventArgs) Handles OysterRootDirBtn.Click

        FolderBrowserDialog1.SelectedPath = Constants.CDrive
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            oysterHelper.FillTextBox(OysterRootDirTxBx, FolderBrowserDialog1.SelectedPath)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub OysterJarBtn_Click(sender As Object, e As EventArgs) Handles OysterJarBtn.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Then
            MessageBox.Show(Constants.OysterRootWkDirRequired, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else

            OpenFileDialog1.InitialDirectory = OysterRootDirTxBx.Text

            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(OysterJarTxBx, OpenFileDialog1.FileName)


            Else
                Exit Sub
            End If
        End If

    End Sub

    Private Sub ERMetricsWkDirBtn_Click(sender As Object, e As EventArgs) Handles ERMetricsWkDirBtn.Click

        FolderBrowserDialog1.SelectedPath = OysterRootDirTxBx.Text
        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            oysterHelper.FillTextBox(ERMetricsTxBx, FolderBrowserDialog1.SelectedPath)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub ERMetricJarBtn_Click(sender As Object, e As EventArgs) Handles ERMetricJarBtn.Click

        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Then
            MessageBox.Show(Constants.ERWorkingDirectory, "ERMetrics Working Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            OpenFileDialog1.InitialDirectory = ERMetricsTxBx.Text
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(ERMetricsJarTxBx, OpenFileDialog1.FileName)
            End If
        End If
    End Sub

    Private Sub ERM_Wk_DirBtn_Click(sender As Object, e As EventArgs) Handles ERM_Wk_DirBtn.Click
        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Then
            MessageBox.Show(Constants.ERWorkingDirectory, "ERMetrics Working Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            FolderBrowserDialog1.SelectedPath = ERMetricsTxBx.Text
            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                Dim DirChosen As String = FolderBrowserDialog1.SelectedPath
                oysterHelper.FillTextBox(EROysterWkDirTxBx, DirChosen)
                If (DirChosen.Equals(EROysterWkDirTxBx.Text)) Then
                    MessageBox.Show("Please Save Your Choices to Update the LinkFile to Copy!", "Save Choices", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Else

            End If

        End If

    End Sub






    '-----------------------------------------------------------------
    '---------------End browse buttons so far ----------------
    '-----------------------------------------------------------------

    '-------------------------------------------------------
    'Run  and stop buttoncalls for Oyster and ERMetrics
    '-----------------------------------------------------------------

    'run oyster
    Private Sub RunOyster_Click(sender As Object, e As EventArgs) Handles RunOyster.Click

        If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Or String.IsNullOrEmpty(OysterJarTxBx.Text) Then
            TestHelper.isFinished = False
            TestHelper.NewJarTesting = False
            MessageBox.Show(Constants.OysterRootWkDirJarRequired, "Oyster", MessageBoxButtons.OK, MessageBoxIcon.Information)
            RunOyster.Enabled = False
            Exit Sub
        End If

        'if run file not right stop it now
        If Not Path.GetFileName(OysterWorkDirTxBx.Text).Equals(OysterXmlListBx.SelectedValue.Substring(0, OysterXmlListBx.SelectedValue.ToUpper.IndexOf("RUN"))) Then
            'TestHelper.isFinished = False
            'TestHelper.NewJarTesting = False
            MessageBox.Show(Constants.OysterWorkDirError, "Oyster", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            helperBean.RichTexBoxNameBool = True
            Me.oysterHelper.RunOyster(helperBean)
        End If

    End Sub

    'run ERMetrics
    Private Sub RunERMetricsBtn_Click(sender As Object, e As EventArgs) Handles RunERMetricsBtn.Click

        Try
            If String.IsNullOrEmpty(ERMetricsTxBx.Text) Or String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then
                MessageBox.Show(Constants.ERMetricsDirChoiceError, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            If Not My.Computer.FileSystem.DirectoryExists(ERMetricsTxBx.Text) Then
                ERMetricsTxBx.Text = Nothing
                MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            ElseIf String.IsNullOrEmpty(My.Settings.ERMetricsWorkDir) Or String.IsNullOrEmpty(My.Settings.ERMetricsJar) Then
                MessageBox.Show(Constants.ERMetricsSaveFile, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else
                helperBean.RichTexBoxNameBool = False
                oysterHelper.RunERMetrics(helperBean)

            End If
        Catch ex As Exception
            ERMetricsTxBx.Text = Nothing
            MessageBox.Show(Constants.ERWorkingDirectory, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try

    End Sub
    Dim oneRun As Boolean = False
    'Stop the Oyster Run and Java Processs
    Private Sub StopRun_Click(sender As Object, e As EventArgs) Handles StopRun.Click

        Me.oysterHelper.Kill_Run()
        ToolStripStatusLabel1.Text = Constants.OHelper
        ToolStripProgressBar1.Visible = False

        'we have know when to stop the testing
        'closewindow method because it can 
        'reek havic if left on when not testing
        'Testing will run this method to stop Oyster
        'one time only so we do nothing to befin with
        If TestHelper.Testing Then
            If oneRun = False Then
                oneRun = True
            Else
                TestHelper.Testing = False
                TestHelper.isFinished = False
            End If
        End If

        If TestHelper.NewJarTesting Then
            TestHelper.isFinished = False
            TestHelper.NewJarTesting = False
        End If


    End Sub

    '-----------------------------------------------------------------
    'End Run, Stop Oyster and ERMetrics
    '---------------------------------------------------------------

    '---------------------------------------------------------------
    ' Edit allowed and Saves for Files Loaded to RichTextBoxes using the generic
    ' `method SaveRichTextBoxFile.  New RichTextBoxes will cause the need to change
    ' the method to include them including constants values used. See method
    ' and constants file.
    '-----------------------------------------------------------------

    'set Edit for RichTextBox2 
    Private Sub EditOystRunBtn_Click(sender As Object, e As EventArgs) Handles EditOystRunBtn.Click
        RichTextBox2.ReadOnly = False
    End Sub

    'save richtextbox2 loaded file
    Private Sub SaveFile_RichTextBox2_Click(sender As Object, e As EventArgs) Handles SaveFile_RichTextBox2.Click

        If Not String.IsNullOrEmpty(RichTextBox2.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox2, RichTextBox2, saveInfoCollection.Item(Constants.RichTextBox2).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    ' Make loaded file in RichTextBox9 editable
    Private Sub EditFileBtn_Click(sender As Object, e As EventArgs) Handles EditFileBtn.Click
        RichTextBox9.ReadOnly = False
    End Sub

    ' Save the file loaded in RichTextBox9 through generic method
    ' SaveRichTextBoxFile.

    Private Sub SaveFileBtn_RichTextBox9Click(sender As Object, e As EventArgs) Handles SaveFileBtn.Click

        If Not String.IsNullOrEmpty(RichTextBox9.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox9, Me.RichTextBox9, Me.saveInfoCollection.Item(Constants.RichTextBox9).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SaveFileERMetricsBtnRichTextBox3_Click(sender As Object, e As EventArgs) Handles SaveFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox3, Me.RichTextBox3, Me.saveInfoCollection.Item(Constants.RichTextBox3).FileLoaded)
            If TestHelper.NewJarTesting Then
                OysterTabs.SelectTab(9)
                JarTestBtn.PerformClick()
                '    Console.WriteLine("Jar Testing true")
            End If
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub EditFileERMetricsBtnRichTextBox3_Click(sender As Object, e As EventArgs) Handles EditFileERMetricsBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox3.Text) Then
            RichTextBox3.ReadOnly = False
        Else
            MessageBox.Show(Constants.MakeFileReadOnlyError, "Read Only Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    '-----------------------------------------------------------------------------
    'End of Edits and Saves to RichTextboxes
    '-----------------------------------------------------------------------------


    '----------------------------------------------------------------------------
    'TreeView content can be double clicked to load the file to a RichTextBox
    'for editing through the generic metod LoadFileFromTreeview. New RichTextBoxes
    'will need to be added to constants and to the method.
    '----------------------------------------------------------------------------
    Sub KBMTreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles KBMTreeView.NodeMouseDoubleClick

        If KBMTreeView.SelectedNode IsNot Nothing Then

            Dim fileExists As Boolean = My.Computer.FileSystem.FileExists(KBMTreeView.SelectedNode.Tag)

            If fileExists Then

                Dim FileToLoad As String = KBMTreeView.SelectedNode.Tag.ToString

                Dim info As New FileInfo(FileToLoad)
                If info.Length = 0 Then
                    MessageBox.Show(Constants.FileIsEmpty, "File Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                Dim helperBean As New HelperBean
                helperBean.RichTextBoxName = "DataGridView1"
                helperBean.IKBFileToLoad = FileToLoad
                Dim i As Integer = Constants.DataGridView1
                'use the integer in constants for each textbox to store
                'in the hashtable
                If Not saveInfoCollection.Item(i) Is Nothing Then
                    saveInfoCollection.Remove(i)
                    saveInfoCollection.Add(i, helperBean)
                Else
                    saveInfoCollection.Add(i, helperBean)
                End If
                Cursor = Cursors.WaitCursor
                BackgroundWorker2.RunWorkerAsync(FileToLoad)
            Else
                MessageBox.Show("File does not Exist!", "File Exists", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If


        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Delegate Sub SafeCallDelegate(text As String)

    Public Shared FileToLoadToRTB As String = Nothing

    Sub KBSearchTreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles KBSearchTreeView.NodeMouseDoubleClick

        If KBSearchTreeView.SelectedNode IsNot Nothing Then
            Cursor = Cursors.WaitCursor
            FileToLoadToRTB = KBSearchTreeView.SelectedNode.Tag
            Dim Thread2 As Thread = New Thread(New ThreadStart(AddressOf SetText))
            Thread2.Start()
            Thread.Sleep(1000)

        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub WriteTextSafe(text As String)
        Dim sb As New StringBuilder
        Try


            If RichTextBox11.InvokeRequired Then
                Dim d As New SafeCallDelegate(AddressOf SetText)
                Invoke(d, New Object() {text})
            Else
                ' Console.WriteLine(Now.ToLongTimeString)
                RichTextBox11.LoadFile(text, RichTextBoxStreamType.PlainText)

                'Dim objReader As New System.IO.StreamReader(text, Encoding.ASCII)
                'Dim TextLine As String = Nothing

                'Do While objReader.Peek() <> -1
                '    sb.AppendLine(objReader.ReadLine())
                'Loop

                'RichTextBox11.Text = sb.ToString

                KBLoadFilelbl.Text = "Loaded file is: " & Path.GetFileName(FileToLoadToRTB)
                KBFileLineCountLbl.Text = "Line Count is: " & RichTextBox11.Lines.Count.ToString("N0")
                OysterTabs.SelectTab("KnowledgeBaseView")
                '  Console.WriteLine(Now.ToLongTimeString)
                Dim i As Integer = 11
                Dim helpBean As New HelperBean
                helpBean.FileLoaded = FileToLoadToRTB
                If Not saveInfoCollection.Item(i) Is Nothing Then
                    saveInfoCollection.Remove(i)
                    saveInfoCollection.Add(i, helpBean)
                Else
                    saveInfoCollection.Add(i, helpBean)
                End If
                Cursor = Cursors.Default
            End If
        Catch ex As Exception
            MessageBox.Show("Error Loading is " & ex.Message, "Thread Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            sb = Nothing
            GC.WaitForPendingFinalizers()
            GC.Collect()
        End Try
    End Sub

    Public Sub SetText()
        WriteTextSafe(FileToLoadToRTB)
    End Sub

    Sub RobotTreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles RobotTreeView.NodeMouseDoubleClick

        If RobotTreeView.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(RobotTreeView, RichTextBox21)
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub


    Sub TreeView3_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView3.NodeMouseDoubleClick

        If TreeView3.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView3, RichTextBox9)
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Sub TreeView1_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseDoubleClick

        If TreeView1.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView1, RichTextBox2)
        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub


    Sub TreeView4_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView4.NodeMouseDoubleClick

        If TreeView4.SelectedNode IsNot Nothing Then
            OysterHelper.LoadFileFromTreeView(TreeView4, RichTextBox3)


        Else
            MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    '-----------------------------------------------------------------
    'End loadfilefromtreeview double Clicks -----------------------------
    '--------------------------------------------------------------------
    'Oysterform close closing Events
    'closed event handling
    Private Sub Oysterform_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

        If OysterRunning Then
            'If checkLog() Then
            '    CheckIfRunning()
            'End If
            Kill(True)
            'If checkLog() Then

            'End If

        End If

    End Sub

    'closing event for any errors that are so bad the Windows Event
    'log has it documented so that if java is still working after an
    'Error/Crash we need to stop Oyster if it is still running as a 
    'Java Process.  It does not differentiate in what Java Process.
    'Also if you exit while testing, the close dialog method continues
    'to run so need to stop it also.

    Private Sub Oysterform_FormClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.FormClosing

        Try
            Cursor = Cursors.WaitCursor

            If CheckIfRunning() Then
                OysterRunning = True
                ' Console.WriteLine("Yes")
            Else
                OysterRunning = False
            End If

            If TestHelper.isFinished = True Then
                TestHelper.isFinished = False
                TestHelper.Testing = False
            End If

        Catch ex As Exception

        Finally
            Cursor = Cursors.Default
        End Try

    End Sub 'Form1_Closing


    Private Function CheckLog() As Boolean
        '   Console.WriteLine(Now.ToLongTimeString)
        Dim RBoolean As Boolean = False
        Try
            Dim x = New EventLog("Application")

            Dim entries = x.Entries()
            Dim lst = New List(Of EventLogEntry)

            For i = 0 To entries.Count - 1
                lst.Add(entries(i))
            Next
            '    Console.WriteLine(Now.ToLongTimeString)
            If lst.Count >= 0 Then
                Dim q = From log In lst
                        Where "Windows Error Reporting ; Application Error".Contains(log.Source)
                        Where log.Message.ToLower.Contains("oysterhelper")
                        Order By log.TimeWritten Descending
                        Select
                        EntryType = log.EntryType.ToString,
                        log.Source,
                        TimeWritten = log.TimeWritten.ToString()

                For Each item In q
                    '   Console.WriteLine("Got It " & item.TimeWritten & vbCrLf & vbCrLf)
                    If item.EntryType.ToLower.Contains("error") Then
                        If item.TimeWritten.ToString.Contains(Today.ToShortDateString) Then
                            Return True
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        Finally
            '       Console.WriteLine(Now.ToLongTimeString)
        End Try

        Return RBoolean
    End Function
    Private Function CheckIfRunning() As Boolean
        Dim running As Boolean = False
        Dim _processes As Process()

        Try
            _processes = Process.GetProcessesByName("java")
            If _processes.Length > 0 Then
                running = True
                Return running
            Else
                running = False
                Return running

            End If
        Catch ex As ArgumentException
            Console.WriteLine(ex.Message)
            'MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            _processes = Nothing
            ToolStripProgressBar1.Visible = False
            StopRun.Enabled = False
            'stop the stopwatch in it is running
            If stopWatch.IsRunning Then
                stopWatch.Stop()
            End If
            RunERMetricsBtn.Enabled = True
        End Try
        Return running
    End Function

    Private Sub Kill(ByVal Kill As Boolean)

        Dim _processes As Process()

        Try
            _processes = Process.GetProcessesByName("java")
            If _processes.Length > 0 Then
                For Each proces As Process In _processes
                    proces.Kill()
                Next
            End If
        Catch ex As ArgumentException
            ' Console.WriteLine(ex.Message)
            'MessageBox.Show(ex.Message, "Error Stopping Java", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            _processes = Nothing
            ToolStripProgressBar1.Visible = False
            StopRun.Enabled = False
            'stop the stopwatch in it is running
            If stopWatch.IsRunning Then
                stopWatch.Stop()
            End If
            RunERMetricsBtn.Enabled = True
        End Try
    End Sub

    ' save ERMetrics textboxes
    Private Sub SaveERMetricsBtn_Click(sender As Object, e As EventArgs) Handles SaveERMetricsBtn.Click
        oysterHelper.SaveERmetricsTxBoxes()
    End Sub

    ' move the link file created from Oyster Run to ERMetrics folder
    Private Sub MoveFileBtn_Click(sender As Object, e As EventArgs) Handles MoveFileBtn.Click
        oysterHelper.MoveLinkFile()
    End Sub

    'help dialogs that need attention
    Private Sub OysterToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim oysterHelp As New OysterHelpForm
        oysterHelp.ShowDialog()
    End Sub
    'help dialogs that need attention
    Private Sub OysterHelperToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OysterHelperToolStripMenuItem.Click
        Dim oysterHelp As New OysterHelpForm
        oysterHelp.ShowDialog()
    End Sub
    ' Method called to resize the program.
    Private Sub Oysterform_Resize(sender As Object, e As EventArgs) Handles Me.Resize

        RS.ResizeAllControls(Me, Me.Width, Me.Height)
    End Sub

    Public Sub ChangeLabel()
        Me.ToolStripStatusLabel1.Text = "Please wait...closing Oyster Helper!"
        StatusStrip1.Refresh()
    End Sub
    ''' Close the application which will also dispose resources.
    Private Sub Exitbtn_Click(sender As Object, e As EventArgs) Handles Exitbtn.Click
        Try

            Me.Close()
        Catch ex As Exception
        End Try

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' Right click methods Below''''''''''''''''''''''''''''''''''
    ''' 

    Private Sub CommentWorkingWithOysterFiles_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem9.Click
        If WorkWithOysterFile.Text.Contains("xml") Then
            Try
                '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
                Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
                ' After verifying that the data can be pasted, paste it.
                '  If richTextBox.CanPaste(MyFormat) Then
                richTextBox.SelectedText = "<!-- " & richTextBox.SelectedText.Trim & " --> "
                '  End If
            Catch ex As Exception
                Console.WriteLine("Contextmenu error is " & ex.Message)
            End Try

        ElseIf WorkWithOysterFile.Text.Contains("properties") Then
            Try
                '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
                Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
                ' After verifying that the data can be pasted, paste it.
                ' If richTextBox.CanPaste(MyFormat) Then
                richTextBox.SelectedText = "#" & richTextBox.SelectedText
                '   End If

            Catch ex As Exception
                Console.WriteLine("Contextmenu error is " & ex.Message)
            End Try

        End If
    End Sub

    Private Sub UnCommentWorkingWithOysterFiles_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem10.Click

        If WorkWithOysterFile.Text.Contains("xml") Then
            Try
                Dim array1() As Char = {"<", "!", "-"}
                Dim array2() As Char = {"-", ">"}
                '      Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
                Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
                Dim s As String = richTextBox.SelectedText.Trim
                Dim st As String = s.TrimStart(array1)
                Dim str As String = st.TrimEnd(array2)
                richTextBox.SelectedText = str.Trim & vbCr
                'uncomment to save 
                '  Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
                ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
            Catch ex As Exception
                Console.WriteLine("Contextmenu error is " & ex.Message)
            End Try


        ElseIf WorkWithOysterFile.Text.Contains("properties") Then
            Try
                Dim array1() As Char = {"#"}

                ' Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
                Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
                Dim s As String = richTextBox.SelectedText.Trim
                Dim st As String = s.TrimStart(array1)
                richTextBox.SelectedText = st.Trim
                'next two uncomment to save the file
                ' Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
                ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
            Catch ex As Exception
                Console.WriteLine("Contextmenu error is " & ex.Message)
            End Try

        End If


    End Sub

    Private Sub CopyWorkingWithOysterFiles_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem11.Click
        Try
            Dim richTextBox As RichTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)

            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try
    End Sub

    Private Sub PasteWorkingWithOysterFiles_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem12.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub CutWorkingWithOysterFiles_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem13.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Cut()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub CommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem.Click
        Try
            '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            '  If richTextBox.CanPaste(MyFormat) Then
            richTextBox.SelectedText = "<!-- " & richTextBox.SelectedText.Trim & " --> "
            '  End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub UnCommentSelectedTextToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnCommentSelectedTextToolStripMenuItem.Click

        Try
            Dim array1() As Char = {"<", "!", "-"}
            Dim array2() As Char = {"-", ">"}
            '      Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            Dim str As String = st.TrimEnd(array2)
            richTextBox.SelectedText = str.Trim & vbCr
            'uncomment to save 
            '  Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            If treeView.SelectedNode IsNot Nothing Then
                If Not String.IsNullOrEmpty(treeView.SelectedNode.Tag) Then
                    Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                    'MsgBox(selectedFileAndPath)

                    My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                    selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                    oysterHelper.RunProcessTab(Constants.Welcome)

                Else
                    MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)

                End If


            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            treeView.Nodes(0).Nodes(j).Expand()
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub DeleteSelectedFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectedFileToolStripMenuItem.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            'Dim tab = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                oysterHelper.RunProcessTab(Constants.Welcome)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            treeView.Nodes(0).Nodes(j).Expand()
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ShowFilePropertiesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowFilePropertiesToolStripMenuItem.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub


    Private Sub CopySelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopySelectionToolStripMenuItem.Click

        'Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

        'If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
        Try
            Dim richTextBox As RichTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)

            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

        '   End If

    End Sub

    Private Sub CutSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CutSelectionToolStripMenuItem.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Cut()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub PasteSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PasteSelectionToolStripMenuItem.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub CommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CommentSelectedTextToolStripMenuItem1.Click
        Try
            '  Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            ' If richTextBox.CanPaste(MyFormat) Then
            richTextBox.SelectedText = "#" & richTextBox.SelectedText
            '   End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub UncommentSelectedTextToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles UncommentSelectedTextToolStripMenuItem1.Click
        Try
            Dim array1() As Char = {"#"}

            ' Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            Dim s As String = richTextBox.SelectedText.Trim
            Dim st As String = s.TrimStart(array1)
            richTextBox.SelectedText = st.Trim
            'next two uncomment to save the file
            ' Dim i As Integer = CType(richTextBox.Name.Substring(richTextBox.Name.Length - 1), Integer)
            ' oysterHelper.SaveRichTextBoxFile(i, richTextBox, Me.saveInfoCollection.Item(i).FileLoaded)
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItemCon42_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                'MsgBox(selectedFileAndPath)
                My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                oysterHelper.RunProcessTab(Constants.ERMetricsSetup)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            treeView.Nodes(0).Nodes(j).Expand()
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripMenuItemCon43_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click

        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                oysterHelper.RunProcessTab(Constants.ERMetricsSetup)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            treeView.Nodes(0).Nodes(j).Expand()
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    Private Sub ToolStripMenuItemcon44_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem4.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try
    End Sub
    Private Sub ToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem5.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim j As Integer = treeView.SelectedNode.Parent.Index
                Dim i As Integer = treeView.SelectedNode.Index
                'MsgBox(selectedFileAndPath)
                My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                Dim name As String = treeView.Name
                If treeView.Name.Equals("KBSearchTreeView") Then
                    oysterHelper.RunProcessTab(Constants.KnowledgeBaseView)
                    treeView.Nodes(0).Nodes(j).Expand()
                Else
                    oysterHelper.RunProcessTab(Constants.WorkWithOysterFiles)
                    treeView.Nodes(0).Nodes(j).Expand()
                End If

            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub

    Private Sub ToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem6.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index


            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                If treeView.Name.Equals("KBSearchTreeView") Then
                    oysterHelper.RunProcessTab(Constants.KnowledgeBaseView)
                    KBSearchTreeView.Nodes(0).Nodes(j).Expand()
                Else
                    oysterHelper.RunProcessTab(Constants.WorkWithOysterFiles)
                    treeView.Nodes(0).Nodes(j).Expand()
                End If
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub


    Private Sub ToolStripMenuItem7_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem7.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    'copy to clipboard 
    Private Sub CopyToClipboardToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyToClipboardToolStripMenuItem.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    'specifically copys cell from datagridview and pastes it to the search box
    Private Sub ToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem8.Click
        Try
            Dim DataGridView = DirectCast((sender).Owner.SourceControl, DataGridView)
            DataGridView.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText
            DataGridView.Select()
            If DataGridView.GetCellCount(DataGridViewElementStates.Selected) > 0 Then
                ' Add the selection to the clipboard.
                Clipboard.SetDataObject(
                    DataGridView.GetClipboardContent())
                KBMTextBox.Text = Clipboard.GetText
            Else
                MessageBox.Show("You must Select a Cell First", "Select a Cell", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            MessageBox.Show("Error is " & ex.Message, "Error Selecting and Pasting", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try


    End Sub

    Private Sub ToolStripRoboCopyt18_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem18.Click
        Try
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            If richTextBox.SelectionLength > 0 Then
                richTextBox.Copy()
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub

    Private Sub ToolStripMenuPasteRobot18_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem19.Click
        Try
            '     Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            '  If richTextBox.CanPaste(MyFormat) Then
            richTextBox.Paste()
            '   End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub PasteFromClipboardToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PasteFromClipboardToolStripMenuItem.Click
        Try
            Dim MyFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Text)
            Dim richTextBox = DirectCast((sender).Owner.SourceControl, RichTextBox)
            ' After verifying that the data can be pasted, paste it.
            If richTextBox.CanPaste(MyFormat) Then
                richTextBox.Paste(MyFormat)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    Private Sub Show4FileDateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowFileDateToolStripMenuItem.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag


                MessageBox.Show("File is Dated: " & IO.File.GetLastWriteTime(selectedFileAndPath), "File Date Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub
    Private Sub Rename4FileToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles RenameFileToolStripMenuItem2.Click
        Try
            Dim renameFileName As String = Nothing
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            '   Console.WriteLine(j & " " & i)
            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim textToShow As String = treeView.SelectedNode.Text

                Dim rDialog As New RenameDiaglog
                rDialog.Label3.Text = textToShow
                rDialog.ReNameFileTxBx.Text = textToShow
                If rDialog.ShowDialog = DialogResult.OK Then

                    Dim text As String = rDialog.ReNameFileTxBx.Text

                    My.Computer.FileSystem.RenameFile(selectedFileAndPath, text)
                    oysterHelper.RunProcessTab(Constants.ERMetricsSetup)
                End If

                treeView.Nodes(0).Nodes(j).Expand()
            Else

            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try

    End Sub
    Private Sub Rename9FileToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RenameFileToolStripMenuItem1.Click
        Try
            Dim renameFileName As String = Nothing
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index
            ' Console.WriteLine(j & " " & i)
            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim textToShow As String = treeView.SelectedNode.Text

                Dim rDialog As New RenameDiaglog
                rDialog.Label3.Text = textToShow
                rDialog.ReNameFileTxBx.Text = textToShow
                If rDialog.ShowDialog = DialogResult.OK Then

                    Dim text As String = rDialog.ReNameFileTxBx.Text

                    My.Computer.FileSystem.RenameFile(selectedFileAndPath, text)
                    oysterHelper.RunProcessTab(Constants.Robot)
                End If

                treeView.Nodes(0).Nodes(j).Expand()
            Else

            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try



    End Sub
    Private Sub RenameFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RenameFileToolStripMenuItem.Click

        Try
            Dim renameFileName As String = Nothing
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim textToShow As String = treeView.SelectedNode.Text
                Dim j As Integer = treeView.SelectedNode.Parent.Index
                Dim i As Integer = treeView.SelectedNode.Index


                Dim rDialog As New RenameDiaglog
                rDialog.Label3.Text = textToShow
                rDialog.ReNameFileTxBx.Text = textToShow
                If rDialog.ShowDialog = DialogResult.OK Then

                    Dim text As String = rDialog.ReNameFileTxBx.Text

                    My.Computer.FileSystem.RenameFile(selectedFileAndPath, text)
                    oysterHelper.RunProcessTab(Constants.Welcome)
                End If

                Me.TreeView3.Nodes(0).Nodes(j).Expand()
            Else

            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try




        '   My.Computer.FileSystem.RenameFile("C:\OldFilename.txt", "NewFilename.txt")
    End Sub
    Private Sub ToolStripMakeCopyofFileRobot_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem14.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index


            If treeView.SelectedNode IsNot Nothing Then
                If Not String.IsNullOrEmpty(treeView.SelectedNode.Tag) Then
                    Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                    'MsgBox(selectedFileAndPath)

                    My.Computer.FileSystem.CopyFile(selectedFileAndPath,
                    selectedFileAndPath & "-Copy", Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
                    oysterHelper.RunProcessTab(Constants.RobotTab)

                Else
                    MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)

                End If

                treeView.Nodes(0).Nodes(j).Expand()
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try
    End Sub

    Private Sub ToolStripDeleteFileRobot16_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem16.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)
            Dim j As Integer = treeView.SelectedNode.Parent.Index
            Dim i As Integer = treeView.SelectedNode.Index

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag

                My.Computer.FileSystem.DeleteFile(selectedFileAndPath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
                oysterHelper.RunProcessTab(Constants.RobotTab)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            Me.TreeView3.Nodes(0).Nodes(j).Expand()
        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub

    Private Sub ToolStripShowFileSizeRObot_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem17.Click
        Try
            Dim treeView = DirectCast((sender).Owner.SourceControl, TreeView)

            If treeView.SelectedNode IsNot Nothing And treeView.SelectedNode.Tag IsNot Nothing Then
                Dim selectedFileAndPath As String = treeView.SelectedNode.Tag
                Dim f As New FileInfo(selectedFileAndPath)
                Dim i As Double = (f.Length / 1024) / 1024
                Dim t As String = Format(i, "0.00")

                MessageBox.Show("File Size is: " & t & " MB", "File Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(Constants.SelectFile, Constants.SelectAFile, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Console.WriteLine("Contextmenu error is " & ex.Message)
        End Try


    End Sub
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'End Right click methods

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ''''''''''''''''''''''''''''''''''''''''''''''''''
    'Helpful Stuff Methods to create the CSV and Reference file
    '''''''''''''''''''''''''''''..........................

    'this does its part to create a refence file
    Private Sub CreateRefFilebtn_Click(sender As Object, e As EventArgs) Handles CreateRefFilebtn.Click

        If String.IsNullOrEmpty(ERMetricsTxBx.Text) Or String.IsNullOrWhiteSpace(ERMetricsTxBx.Text) Then
            MessageBox.Show("ERMetrics must be Setup Before Creating a new Reference File", "Setup ERMetrics", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If Not String.IsNullOrEmpty(HSCsvTxBx.Text) Then

            If Not File.Exists(HSCsvTxBx.Text) Then
                MessageBox.Show("CVS File does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                HSCsvTxBx.Text = Nothing
                Exit Sub
            ElseIf Not File.Exists(HSLinkFileTxBx.Text) Then
                MessageBox.Show("LinkFile does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                HSLinkFileTxBx.Text = Nothing
                Exit Sub
            ElseIf File.ReadAllText(HSCsvTxBx.Text).Length = 0 Then
                MessageBox.Show("CVS File is empty, Rerun Oyster", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            End If

            'don't let user click other button that calls backgroundworker until it is done with this run
            CreateCSVBtn.Enabled = False
            CBStrapBtn.Enabled = False
            'false sent to getHelperBean then make reference file
            ' BackgroundWorker1.RunWorkerAsync(oysterHelper.GetHelperBean(False))
            oysterHelper.CreateHelpfulStufFiles(oysterHelper.GetHelperBean(2))

        Else
            MessageBox.Show(Constants.RunOysterFirst, "Empty Parameters", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If

    End Sub

    'Helper Stuff create create csv file from last oyster run
    Private Sub CreateCSVBtn_Click(sender As Object, e As EventArgs) Handles CreateCSVBtn.Click

        Dim helperBean As New HelperBean
        Dim fileCount As Integer = 0
        Dim subString As String = Nothing

        Try


            If Not String.IsNullOrEmpty(HSCsvTxBx.Text) And Not String.IsNullOrEmpty(HSLinkFileTxBx.Text) Then

                If Not File.Exists(HSCsvTxBx.Text) Then
                    MessageBox.Show("CVS File does not Exist, Check if" & vbCrLf & " it has been Deleted!", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSCsvTxBx.Text = Nothing
                    Exit Sub
                ElseIf Not File.Exists(HSLinkFileTxBx.Text) Then
                    MessageBox.Show("LinkFile does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSLinkFileTxBx.Text = Nothing
                    ERMetLinkFileTxbx.Text = Nothing
                    Exit Sub

                ElseIf File.ReadAllText(HSCsvTxBx.Text).Length = 0 Then
                    MessageBox.Show("CVS File is empty, Rerun Oyster", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                Else


                End If

                'don't let user click other button that calls backgroundworker until it is done with this run
                CreateRefFilebtn.Enabled = False
                CBStrapBtn.Enabled = False

                'if sending true to gethelperbean then make csv file
                ' BackgroundWorker1.RunWorkerAsync(oysterHelper.GetHelperBean(True))
                oysterHelper.CreateHelpfulStufFiles(oysterHelper.GetHelperBean(1))
            Else
                MessageBox.Show(Constants.RunOysterFirst, "Empty Parameters", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CBStrapBtn_Click(sender As Object, e As EventArgs) Handles CBStrapBtn.Click



        Dim helperBean As New HelperBean
        Dim fileCount As Integer = 0
        Dim subString As String = Nothing
        Dim Delimiter As String = Nothing
        Try
            'If Not SameRB.Checked Then
            '    If TabRB.Checked Then
            '        Delimiter = vbTab
            '    ElseIf CommaRB.Checked Then
            '        Delimiter = ","
            '    ElseIf PipeRB.Checked Then
            '        Delimiter = "|"
            '    ElseIf SemiColonRB.Checked Then
            '        Delimiter = ";"
            '    End If
            'Else
            '    Delimiter = "SAME"
            'End If


            If Not String.IsNullOrEmpty(HSCsvTxBx.Text) And Not String.IsNullOrEmpty(HSLinkFileTxBx.Text) Then

                If Not File.Exists(HSCsvTxBx.Text) Then
                    MessageBox.Show("CVS File does not Exist, Check if" & vbCrLf & " it has been Deleted!", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSCsvTxBx.Text = Nothing
                    Exit Sub
                ElseIf Not File.Exists(HSLinkFileTxBx.Text) Then
                    MessageBox.Show("LinkFile does not Exist, Rerun Oyster", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    HSLinkFileTxBx.Text = Nothing
                    ERMetLinkFileTxbx.Text = Nothing
                    Exit Sub

                ElseIf File.ReadAllText(HSCsvTxBx.Text).Length = 0 Then
                    MessageBox.Show("CVS File is empty, Rerun Oyster", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub

                End If

                'don't let user click other button that calls backgroundworker until it is done with this run
                CreateRefFilebtn.Enabled = False
                CreateCSVBtn.Enabled = False

                'if sending true to gethelperbean then make csv file
                ' BackgroundWorker1.RunWorkerAsync(oysterHelper.GetHelperBean(True))
                oysterHelper.CreateHelpfulStufFiles(oysterHelper.GetHelperBean(3))

            Else
                MessageBox.Show(Constants.RunOysterFirst, "Empty Parameters", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

        Catch ex As Exception

        End Try





    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'end create CVS and Ref files
    ''''''''''''''''''''''''''''''''''''''''''''''''''''


    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Each click on a tab checks and loads each 
    'tab as needed
    '''''''''''''''''''''''''''''''''''''''''''''''''''' 
    Private Sub MyTabControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OysterTabs.SelectedIndexChanged

        oysterHelper.RunProcessTab(OysterTabs.SelectedTab.Name)


    End Sub


    'Timer used along with the stopwatch to keep time on Oyster runs
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timelbl.Text = String.Format("{0}:{1}:{2}", stopWatch.Elapsed.Hours.ToString("00"), stopWatch.Elapsed.Minutes.ToString("00"), stopWatch.Elapsed.Seconds.ToString("00"))
    End Sub

    Private Sub KBSearchBtn_Click(sender As Object, e As EventArgs) Handles KBSearchBtn.Click

        Dim searchTerm As String = SearchKBTxtBx.Text

        If String.IsNullOrEmpty(searchTerm) Or String.IsNullOrWhiteSpace(searchTerm) Then
            MessageBox.Show("Search Term is Empty!", "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf String.IsNullOrEmpty(KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)) Or String.IsNullOrWhiteSpace(KBLoadFilelbl.Text.Substring(KBLoadFilelbl.Text.IndexOf(":") + 2)) Then
            MessageBox.Show("You must First Load a File to Search", "Search Loaded File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            oysterHelper.SearchKBLoadedFile(searchTerm, helperBean)
        End If


    End Sub



    Private Sub ColumnCheckedLB_MouseClick(sender As Object, e As MouseEventArgs) Handles ColumnCheckedLB.MouseClick
        Dim idx, sidx As Integer
        sidx = ColumnCheckedLB.SelectedIndex
        For idx = 0 To ColumnCheckedLB.Items.Count - 1
            If idx <> sidx Then
                ColumnCheckedLB.SetItemChecked(idx, False)
            Else
                ColumnCheckedLB.SetItemChecked(sidx, True)
            End If
        Next
    End Sub


    Private Sub SearchIdtyBtn_Click(sender As Object, e As EventArgs) Handles SearchIdtyBtn.Click

        If String.IsNullOrEmpty(KBMTextBox.Text) Then
            MessageBox.Show("Search Term Required!", "Search", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            oysterHelper.SearchDataGrid()
        End If



    End Sub

    Private Sub ResetFilterBtn_Click(sender As Object, e As EventArgs) Handles ResetFilterBtn.Click

        Dim bsource As BindingSource = DataGridView1.DataSource
        If bsource Is Nothing Then
            MessageBox.Show("Nothing to Reset!", "Null Reference", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Exit Sub
        End If

        bsource.RemoveFilter()
        KBMFilterCountLbl.Text = "Filtered Count is: "
        KBMTextBox.Text = Nothing

        For Each i As Integer In ColumnCheckedLB.CheckedIndices
            ColumnCheckedLB.SetItemChecked(i, False)
        Next



    End Sub

    'used to speed the load of the datagridview in the IKB Searching Tab
    Private Sub BackgroundWorker2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork

        Try

            Dim FiletoLoad As String = e.Argument

            Dim table As New DataTable()
            Dim bsource As New BindingSource
            Dim DictList As New Dictionary(Of String, String)

            Dim xmlAll As New XDocument


            xmlAll = XDocument.Load(FiletoLoad)
            Dim Attributes As IEnumerable(Of XElement) = xmlAll.Root.Elements("Metadata").Elements("Attributes").Elements("Attribute")
            Dim References As IEnumerable(Of XElement) = xmlAll.Root.Elements("Identities").Elements("Identity")
            Dim oysteridValue As String = Nothing
            xmlAll = Nothing
            Dim column As DataColumn = Nothing
            Dim order As Integer = 0
            For Each xel In Attributes
                column = New DataColumn
                column.DataType = System.Type.GetType("System.String")
                column.ColumnName = xel.Attribute("Name").Value
                column.ReadOnly = True
                column.Unique = False
                table.Columns.Add(column)
                table.Columns(xel.Attribute("Name").Value).SetOrdinal(order)
                DictList.Add(xel.Attribute("Name").Value, xel.Attribute("Tag").Value)
                order = order + 1
            Next
            column = New DataColumn
            column.DataType = System.Type.GetType("System.String")
            column.ColumnName = "OysterId"
            column.ReadOnly = True
            column.Unique = False

            table.Columns.Add(column)
            table.Columns("OysterId").SetOrdinal(order)
            '    Console.WriteLine(order)
            order = Nothing
            ' DictList.Add("OysterId", oysteridValue)
            column = Nothing
            For Each iden In References

                For Each el In iden.Elements("References").Elements("Reference")
                    Dim outputString As String = Nothing
                    Dim str As String = el.Element("Value").Value

                    For Each key As String In DictList.Keys
                        Dim keyPlus As String = DictList.Item(key) & "^"
                        Dim loc As Integer = str.IndexOf(keyPlus)
                        If Not loc = -1 Then
                            Dim i As Integer = str.IndexOf("|")
                            If Not i = -1 Then
                                outputString = outputString + str.Substring(loc + 2, i - 2) & vbTab
                                str = str.Substring(i + 1)
                            Else
                                outputString = outputString + str.Substring(loc + 2) & vbTab
                            End If
                        Else
                            outputString = outputString & vbTab
                        End If

                    Next
                    outputString = outputString & iden.Attribute("Identifier").Value.ToString
                    Dim row1() As String = Split(outputString, vbTab)
                    table.Rows.Add(row1)
                Next
            Next
            ' BackgroundWorker2.ReportProgress(20)u
            Attributes = Nothing
            References = Nothing
            Dim Helpbean As New HelperBean
            Helpbean.DataGridViewTable = table
            Helpbean.IKBMDictionary = DictList
            Helpbean.FileLoaded = FiletoLoad
            e.Result = Helpbean

        Catch ex As Exception
            Console.WriteLine("Error in Backgroundworker2.dowork is " & ex.Message)
        Finally
            GC.WaitForPendingFinalizers()
            GC.Collect()

        End Try
    End Sub



    Private Sub BackgroundWorker2_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted

        Dim hbean As HelperBean = Nothing
        Dim table As DataTable = Nothing
        Dim bsource As New BindingSource
        Dim DictList As New Dictionary(Of String, String)
        ToolStripProgressBar1.Value = 0
        ToolStripProgressBar1.PerformStep()


        Try

            hbean = CType(e.Result, HelperBean)
            table = CType(hbean.DataGridViewTable, DataTable)

            DictList = CType(hbean.IKBMDictionary, Dictionary(Of String, String))
            ColumnCheckedLB.Items.Clear()

            For Each key As String In DictList.Keys
                If Not key.ToUpper.Contains("OysterId".ToUpper) Then
                    ColumnCheckedLB.Items.Add(key)
                End If
            Next
            ColumnCheckedLB.Items.Add("OysterId")
            ToolStripProgressBar1.PerformStep()

            bsource.DataSource = table
            DataGridView1.DataSource = bsource

            Dim aInteger As Integer = 0
            With DataGridView1
                For Each key As String In DictList.Keys
                    .Columns(key).DisplayIndex = aInteger
                    aInteger = aInteger + 1
                Next
                .Columns("OysterId").DisplayIndex = aInteger
            End With

            DictList.Clear()
            Me.DataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells)
            LoadCountLbl.Text = "Reference Line Count is: " & table.Rows.Count & " Rows"
            KBMFileLoadedLbl.Text = "File Loaded is: " & hbean.FileLoaded.Substring(hbean.FileLoaded.LastIndexOf("\") + 1)
            KBMFilterCountLbl.Text = "Filtered Count is:"
        Catch ex As Exception
            MessageBox.Show("Error in Backgroundworker2 Completed is " & ex.Message, "Worker Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            ToolStripStatusLabel1.Text = "Oyster Helper"
            ToolStripProgressBar1.Value = 0
            ToolStripProgressBar1.Visible = False
            hbean = Nothing
            bsource = Nothing
            table = Nothing
            DictList = Nothing
            Cursor = Cursors.Default
            GC.WaitForPendingFinalizers()
            GC.Collect()


        End Try
    End Sub

    Private Sub SelectEntireRowToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SelectEntireRowToolStripMenuItem.Click
        oysterHelper.ToolSelectEntireRow()
    End Sub


    Private Sub StartTest_Click(sender As Object, e As EventArgs) Handles StartTests.Click
        If TestHelper.NewJarTesting = True Then
            TestHelper.NewJarTesting = False
        End If
        If oneRun = True Then
            oneRun = False
        End If
        oysterHelper.RunTests()

    End Sub



    'Used to hide and show the testing tab
    Public Sub EnablePage(page As TabPage, enable As Boolean)

        If (enable) Then
            If page IsNot Nothing Then
                OysterTabs.TabPages.Add(page)
                ListA.Remove(page)
            End If

        Else
            If page IsNot Nothing Then
                OysterTabs.TabPages.Remove(page)
                ListA.Add(page)

                'in case the program is maximized resize the tab when it is visible
                RS.ResizeAllControls(Me, Me.Width, Me.Height)
                '---------------------------------------------------s
            End If

        End If
    End Sub


    '---------------Assertion Tab Work------------------------------------


    '  Private Sub CreateAssertFileBtn_Click(sender As Object, e As EventArgs) Handles CreateAssertFileBtn.Click

    Private Sub CreateAssertFile()


        Dim rButton As RadioButton = GroupBox1.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked = True)

        If rButton Is Nothing Then
            MessageBox.Show("You must choose an Asserton Type!")
            Exit Sub
        End If
        dta = New DataTable


        oysterHelper.CreateTemplate(rButton, dta)


    End Sub

    Private Sub StrToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrToStrAssertion.CheckedChanged

        If StrToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFile()

            ' MessageBox.Show("Use the Create Assertion Button to Create a Template!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub RefToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToStrAssertion.CheckedChanged
        If RefToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            MessageBox.Show("If you have a Reference File" & vbCrLf & " Load it to the DataGridView for use!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            oysterHelper.LoadAssertionTemplate()
            AssertLabel.Text = "Template Loaded is for: " & oysterHelper.GetAssertType.Substring(1)
            'TrimTemplate(dta)
        End If


    End Sub

    Private Sub StrSplitAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrSplitAssertion.CheckedChanged
        If StrSplitAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFile()
            'CreateAssertFileBtn.Enabled = True
            ' MessageBox.Show("Use the Create Assertion Button to Create a Template!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If

    End Sub

    Private Sub RefToRefAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToRefAssertion.CheckedChanged

        If RefToRefAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb

            MessageBox.Show("If you have a Reference File" & vbCrLf & " Load it to the DataGridView for use!", "Assertion File Creation", MessageBoxButtons.OK, MessageBoxIcon.Information)

            oysterHelper.LoadAssertionTemplate()
            AssertLabel.Text = "Template Loaded is for: " & oysterHelper.GetAssertType.Substring(1)
            '  TrimTemplate(dta)

        End If



    End Sub

    Private Sub SaveAssertFileBtn_Click(sender As Object, e As EventArgs) Handles SaveAssertFileBtn.Click

        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.Filter = "CSV File|*.csv|TextFile|*.txt"
        saveFileDialog1.Title = "Save an Assertion File"
        saveFileDialog1.ShowDialog()


        ' If the file name is not an empty string open it for saving.  
        If saveFileDialog1.FileName <> "" Then
            SaveGridDataInFile(saveFileDialog1.FileName)
        End If



    End Sub

    Private Sub SaveGridDataInFile(ByRef fName As String)
        oysterHelper.SaveDataGridAssertionFile(fName)
    End Sub



    Public Sub TrimTemplate(ByRef dta As DataTable)
        oysterHelper.TemplateTrim(dta)



    End Sub

    '--------------------------------------------------------------------------
    '------End Assertion Tab Work
    '-----------------------------------------------------------------------------



    'Start of Test Methods ----------------------------------------------
    '-------------------------------------------------------------------------
    Private Sub ReadTestFileBtn_Click(sender As Object, e As EventArgs) Handles ReadTestFileBtn.Click

        Try
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If

            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                MessageBox.Show("Oyster Helper cannot get your User Name. " & vbCrLf & "Please close Oyster Helper and then Run as Administrator!", "Empty User Name!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If


            If IO.File.Exists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") Then
                oysterHelper.ReadTestFile()
            Else
                MessageBox.Show("Test File not Found!", "Test File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            TestHelper.Testing = False
            TestHelper.isFinished = False
            MessageBox.Show("Error reading test file is " & ex.Message, "Test File Read Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

    End Sub

    'Create the test file if the textboxes are all filled
    Private Sub CreateFileBtn_Click(sender As Object, e As EventArgs) Handles CreateFileBtn.Click

        oysterHelper.CreateTestFile()

    End Sub

    'signin to the reveal the testing tab that is hidden
    Private Sub ToolStripDropDownButton1_DoubleClick(sender As Object, e As EventArgs) Handles ToolStripDropDownButton1.DoubleClick
        oysterHelper.LoginForTesting()

    End Sub

    Private Sub DeleteValuesBtn_Click(sender As Object, e As EventArgs) Handles DeleteValuesBtn.Click

        oysterHelper.DeleteOysterDirectory()
        My.Settings.Testing = True
        My.Settings.Save()

    End Sub

    Private Sub RestartBtn_Click(sender As Object, e As EventArgs) Handles RestartBtn.Click
        Application.Restart()
    End Sub

    Private Sub SearchKBTxtBx_TextChanged(sender As Object, e As EventArgs) Handles SearchKBTxtBx.TextChanged

        If RichTextBox11.Lines.Count > 0 Then
            RichTextBox11.SelectionStart = 0
            RichTextBox11.ScrollToCaret()
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        OH_AboutBox.ShowDialog()
    End Sub


    'Hide the Testing tab if desired fro m the Testing Tab
    Private Sub HideTab_Click_1(sender As Object, e As EventArgs) Handles HideTab.Click

        Dim tab As TabPage = OysterTabs.TabPages.Item("TestTab")
        EnablePage(tab, False)
        My.Settings.Testing = False
        My.Settings.Save()

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        System.Diagnostics.Process.Start(e.Link.LinkData.ToString())
    End Sub

    Private Sub Analyze_Btn_Click(sender As Object, e As EventArgs) Handles Analyze_Btn.Click

        Try
            ToolStripStatusLabel1.Text = "Loading..."

            StatusStrip1.Refresh()

            If LastInputFileRB.Checked Then

                If Not String.IsNullOrEmpty(HSCsvTxBx.Text) Then
                    Cursor = Cursors.WaitCursor

                    Dim analyzeForm As New AnalyzeForm()
                    analyzeForm.thePath = HSCsvTxBx.Text
                    analyzeForm.AnalyzeDataTxBx.Text = HSCsvTxBx.Text
                    analyzeForm.ShowDialog()

                Else
                    MessageBox.Show("Last Input File is Empty!", "File To Load", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

            ElseIf AnotherFileRB.Checked Then

                Dim analyzeForm As New AnalyzeForm()
                analyzeForm.ShowDialog()
            Else

                MessageBox.Show("You must Choose a File to Analyze!", "File To Load", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("Analyze call error is " & ex.Message, "Analyze Button Call", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            Cursor = Cursors.Default

            ToolStripStatusLabel1.Text = Constants.OHelper

            StatusStrip1.Refresh()

        End Try
    End Sub



    Private Sub RunPython_Click(sender As Object, e As EventArgs) Handles RunPythonBtn.Click


        If Not String.IsNullOrEmpty(PythonExeTxBx.Text) AndAlso Not String.IsNullOrEmpty(RobotWkDirTxBx.Text) Then
            ToolStripStatusLabel1.Text = "Robot Running"
            ToolStripProgressBar1.Visible = True

            Dim p As New ProcessClass
            p.RunPython()
        Else
            MessageBox.Show("The Python Executable and Robot Working Directory must Not be empty!", "Requirement", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


    End Sub

    Private Sub RobotSaveFileBtn_Click(sender As Object, e As EventArgs) Handles RobotSaveFileBtn.Click
        If Not String.IsNullOrEmpty(RichTextBox21.Text) Then
            oysterHelper.SaveRichTextBoxFile(Constants.RichTextBox21, Me.RichTextBox21, Me.saveInfoCollection.Item(Constants.RichTextBox21).FileLoaded)
        Else
            MessageBox.Show(Constants.EmptyRichTextBox, Constants.RichTxtBxEmpty, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        ' RichTextBox21.LoadFile("C:\\OysterScoringRobot\\RobotResults.csv", fileType:=RichTextBoxStreamType.PlainText)
    End Sub

    Private Sub PythonEXEBtn_Click(sender As Object, e As EventArgs) Handles PythonEXEBtn.Click
        FolderBrowserDialog1.SelectedPath = Constants.CDrive

        If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
            oysterHelper.FillTextBox(PythonExeTxBx, FolderBrowserDialog1.SelectedPath)

        Else
            Exit Sub
        End If
    End Sub

    Private Sub RobotWkDirBtn_Click(sender As Object, e As EventArgs) Handles RobotWkDirBtn.Click
        If Not String.IsNullOrEmpty(PythonExeTxBx.Text) Then
            FolderBrowserDialog1.SelectedPath = Constants.CDrive
            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                oysterHelper.FillTextBox(RobotWkDirTxBx, FolderBrowserDialog1.SelectedPath)
            Else
                Exit Sub
            End If
        Else
            MessageBox.Show("Python EXE Directory must be Filled!", "Requirement", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If

    End Sub

    Private Sub SaveRobotBtn_Click(sender As Object, e As EventArgs) Handles SaveRobotBtn.Click
        If String.IsNullOrEmpty(PythonExeTxBx.Text) Or String.IsNullOrEmpty(RobotWkDirTxBx.Text) Then
            MessageBox.Show(Constants.RobotTxBxMustBeFilled, "Requirement", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            Me.oysterHelper.Save_All_RobotTab()
        End If

    End Sub

    Private Sub EditRobotFile_Click(sender As Object, e As EventArgs) Handles EditRobotFile.Click
        RichTextBox21.ReadOnly = False
    End Sub

    Private Sub JarTestBtn_Click(sender As Object, e As EventArgs) Handles JarTestBtn.Click
        Try
            'ToolStripStatusLabel1.Text = "Loading..."

            'StatusStrip1.Refresh()
            If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Or String.IsNullOrEmpty(OysterJarTxBx.Text) Then
                MessageBox.Show(Constants.OysterRootWkDirJarAlsoRequired, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            ElseIf String.IsNullOrEmpty(ERMetricsTxBx.Text) Or String.IsNullOrEmpty(ERMetricsJarTxBx.Text) Then
                MessageBox.Show(Constants.ERMetricsDirChoiceError, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else

                Dim testJarForm As New TestJarForm()
                testJarForm.ShowDialog()

            End If



        Catch ex As Exception
            '   MessageBox.Show("Test Oyster Jar call error is " & ex.Message, "Analyze Button Call", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Console.WriteLine(ex.Message)
        Finally
            'Cursor = Cursors.Default

            'ToolStripStatusLabel1.Text = Constants.OHelper

            'StatusStrip1.Refresh()

        End Try



    End Sub




    '--------------------------------------------------------------

    'Very Important ConsoleOutput Methods used to write back
    'to The RichtextBoxes during Oyster and ERmetrics Runs.
    'Console ouput writers for the ProcessClass

    'All output from Oyster,ERmetrics or Python run pass through one of the 
    'three methods ErrorOutput or ConsoleOutput or PythonErrorOutput or 
    'PythonConsoleOutput.  There are delegates for each of them defined
    'at the top of this class.
    '-------------------------------------------------------------
    Private Sub ConsoleOutput(ByVal outputString As String)
        Try

            If Me.InvokeRequired Then
                Dim del As New ConsoleOutputDelegate(AddressOf ConsoleOutput)
                Dim args As Object() = {outputString}
                Me.Invoke(del, args)

            Else
                If helperBean.RichTexBoxNameBool Then
                    RunRichTextBox.ScrollToCaret()
                    RunRichTextBox.AppendText(String.Concat(outputString, Environment.NewLine))
                    ToolStripProgressBar1.PerformStep()
                Else
                    ERMetricsRunRichTxtBx.ScrollToCaret()
                    ERMetricsRunRichTxtBx.AppendText(String.Concat(outputString, Environment.NewLine))
                    ToolStripProgressBar1.PerformStep()
                End If

                If ToolStripProgressBar1.Value = 100 Then
                    ToolStripProgressBar1.Value = 0
                    ToolStripProgressBar1.PerformStep()
                End If
                If outputString.Contains("##ERROR") Or outputString.Contains("###ERROR") Then
                    If TestHelper.NewJarTesting Then
                        myCounter = 1
                        runAgain = True
                        TestHelper.isFinished = False
                        TestHelper.NewJarTesting = False
                    End If

                End If

                If outputString.Contains("Total elapsed time ") Then 'Or outputString.ToUpper.Contains("error".ToUpper) Then

                    StopRun.Enabled = False
                    Me.ToolStripProgressBar1.Visible = False
                    ' Me.ToolStripStatusLabel1.Visible = False
                    ToolStripStatusLabel1.Text = Constants.OHelper
                    StatusStrip1.Refresh()
                    Timer1.Enabled = False
                    'stop the stopwatch and report elapsed time
                    stopWatch.Stop()
                    Dim ts As TimeSpan = stopWatch.Elapsed
                    ' RunTimeValuelbl.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10)
                    ' RunTimeValuelbl.Text = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds)
                    ' oysterHelper.Kill_Oyster()
                    RunERMetricsBtn.Enabled = True

                    If TestHelper.Testing Then
                        ' TestHelper.NewJarTesting = False
                        TestHelper.SecondTest()


                    End If

                    If TestHelper.NewJarTesting And runAgain Then
                        'If myCounter = 1 Then
                        '    'Dim thelper = New TestHelper
                        '    'thelper.StartThreadTests()
                        '    TestHelper.JarTestList.Clear()
                        'End If
                        'Dim setupArr As String() = {"Old Jar " & OysterJarTxBx.Text, "New Test Jar " & My.Settings.NewTestJar}
                        'TestHelper.JarTestList.Add(0, setupArr)
                        Dim myArr As String() = RunRichTextBox.Lines
                        TestHelper.JarTestList.Add(myCounter, myArr)
                        myCounter = myCounter + 1
                        '   Console.WriteLine(Now.ToLongTimeString & " " & myCounter)
                        'Dim thelper = New TestHelper
                        ''thelper.StartThreadTests()
                        'Console.WriteLine(OysterJarTxBx.Text)
                        'Console.WriteLine("All the stuff Oyster run")
                        'Console.WriteLine(outputString)

                        TestHelper.FourthTests()

                        '    runAgain = False

                    End If

                ElseIf outputString.Equals("Complete") Then
                    ToolStripProgressBar1.Visible = False
                    ToolStripStatusLabel1.Text = Constants.OHelper
                    StopRun.Enabled = False

                    If TestHelper.NewJarTesting Then
                        Dim myArr As String() = ERMetricsRunRichTxtBx.Lines
                        TestHelper.JarTestList.Add(myCounter, myArr)
                        myCounter = myCounter + 1
                        If TestHelper.TestJarCounter < RunCount Then 'super number to be a wildcard 3 is 2 oyster runs 5 is 4 oyster runs
                            runAgain = True
                            TestHelper.ThirdTests(TestHelper.TJarBean)
                        Else
                            'TestHelper.isFinished = False
                            'TestHelper.NewJarTesting = False
                            TestHelper.MakeOutput(TestHelper.JarTestList, RunCount)

                        End If

                    End If

                    If TestHelper.Testing Then
                        TestHelper.FinishReport()

                    End If


                End If
            End If



        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

        End Try

    End Sub



    Private Sub ConsoleOutputPython(ByVal outputString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleOutputPythonDelegate(AddressOf ConsoleOutputPython)
            Dim args As Object() = {outputString}
            Me.Invoke(del, args)

        Else

            '  If helperBean.RichTexBoxNameBool Then
            RobotRTBx.ScrollToCaret()
            RobotRTBx.AppendText(String.Concat(outputString, Environment.NewLine))
            ToolStripProgressBar1.PerformStep()
            'Else
            '    ERMetricsRunRichTxtBx.ScrollToCaret()
            '    ERMetricsRunRichTxtBx.AppendText(String.Concat(outputString, Environment.NewLine))
            '    ToolStripProgressBar1.PerformStep()
            'End If

            If ToolStripProgressBar1.Value = 100 Then
                ToolStripProgressBar1.Value = 0
                ToolStripProgressBar1.PerformStep()
            End If


            If outputString.ToUpper.Contains("total running time is".ToUpper) Then

                RichTextBox21.LoadFile(RobotWkDirTxBx.Text & "\RobotResults.csv", fileType:=RichTextBoxStreamType.PlainText)
                PythonFileLbl.Text = "Loaded File is: " & "RobotResults.csv"
                ' Console.WriteLine(RichTextBox21.Lines.Count)
                If RichTextBox21.Lines.Count <= 2 Then
                    RichTextBox21.AppendText(vbCrLf & vbCrLf & "You may need to examine the ERMetrics Log File for Errors!")
                End If

                StopRun.Enabled = False
                Me.ToolStripProgressBar1.Visible = False
                ' Me.ToolStripStatusLabel1.Visible = False
                ToolStripStatusLabel1.Text = Constants.OHelper

            ElseIf outputString.ToUpper.Contains("ERROR") Then
                ToolStripProgressBar1.Visible = False
                ToolStripStatusLabel1.Text = Constants.OHelper
                StopRun.Enabled = False



            End If
        End If
    End Sub

    Sub ConsoleOutputHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleOutput(outLine.Data)
        End If
    End Sub
    Sub ConsoleOutputPythonHandler(ByVal sendingProcess As Object, ByVal outLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(outLine.Data) Then
            ConsoleOutputPython(outLine.Data)
        End If
    End Sub

    Private err As Integer = 0

    Private Sub ConsoleError(ByVal errorString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleErrorDelegate(AddressOf ConsoleError)
            Dim args As Object() = {errorString}
            Me.Invoke(del, args)
        Else
            If helperBean.RichTexBoxNameBool Then
                RunRichTextBox.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
                'StopRun.Enabled = False
                ErrorInRunLbl.Visible = True
                ToolStripStatusLabel1.Text = Constants.OHelper
                ToolStripProgressBar1.Visible = False
                If stopWatch.IsRunning Then
                    stopWatch.Stop()
                End If
                RunERMetricsBtn.Enabled = True

                err = err + 1


                'If err = 2 Then
                '    oysterHelper.Kill_Run()
                '  MessageBox.Show("Error in the Oyster Runner is " & errorString, "Check Your Requirements", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '   Exit Sub
                'End If
            Else
                ERMetricsRunRichTxtBx.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
                StopRun.Enabled = False
                ErrorInRunLbl.Visible = True
                RunERMetricsBtn.Enabled = True
                ToolStripStatusLabel1.Text = Constants.OHelper
                ToolStripStatusLabel1.Visible = True
                ToolStripProgressBar1.Visible = False
                StopRun.Enabled = False
                MessageBox.Show("Error in the Oyster Run", "Check Requirements", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        End If

    End Sub

    Private Sub PythonConsoleError(ByVal errorString As String)
        RobotRTBx.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
        ToolStripProgressBar1.Visible = False
        ToolStripStatusLabel1.Text = Constants.OHelper
        StopRun.Enabled = False
    End Sub

    Private Sub ConsoleErrorPython(ByVal errorString As String)

        If Me.InvokeRequired Then
            Dim del As New ConsoleErrorPythonDelegate(AddressOf PythonConsoleError)
            Dim args As Object() = {errorString}
            Me.Invoke(del, args)
        Else
            RobotRTBx.AppendText(String.Concat("Error Output: ", errorString, Environment.NewLine))
            ToolStripProgressBar1.Visible = False
            ToolStripStatusLabel1.Text = Constants.OHelper
            StopRun.Enabled = False

        End If

    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sendingProcess"></param>
    ''' <param name="errLine"></param>
    Sub ConsoleErrorHandler(ByVal sendingProcess As Object, ByVal errLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(errLine.Data) Then
            ConsoleError(errLine.Data)
        End If
    End Sub

    Sub ConsoleErrorPythonHandler(ByVal sendingProcess As Object, ByVal errLine As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(errLine.Data) Then
            ConsoleErrorPython(errLine.Data)
        End If
    End Sub


    'End ConsoleOutput Methods ----------------------------------------------
    '-----------
    '--------------------------------------------------------------



    'late edition to save filtered IKB file - could be useful
    'Private Sub SaveFilteredIKB_Click(sender As Object, e As EventArgs)

    '    SaveFileDialog1.ShowDialog()


    '    'Dim headers = (From header As DataGridViewColumn In DataGridView1.Columns.Cast(Of DataGridViewColumn)()
    '    '               Select header.HeaderText).ToArray
    '    'Dim rows = From row As DataGridViewRow In DataGridView1.Rows.Cast(Of DataGridViewRow)()
    '    '           Where Not row.IsNewRow
    '    '           Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))
    '    'Using sw As New IO.StreamWriter("C:\\OysterTestSuite\\IdentityUpdate.csv")
    '    '    sw.WriteLine(String.Join(",", headers))
    '    '    For Each r In rows
    '    '        sw.WriteLine(String.Join(",", r))
    '    '    Next
    '    'End Using

    'End Sub
End Class
