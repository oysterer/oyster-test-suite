﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Oysterform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Oysterform))
        Me.Exitbtn = New System.Windows.Forms.Button()
        Me.HomeBtn = New System.Windows.Forms.Button()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OysterHelperToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.OysterTabs = New System.Windows.Forms.TabControl()
        Me.Welcome = New System.Windows.Forms.TabPage()
        Me.WelLoadedFileLbl = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.EditFileBtn = New System.Windows.Forms.Button()
        Me.SaveFileBtn = New System.Windows.Forms.Button()
        Me.RichTextBox9 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CommentSelectedTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnCommentSelectedTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopySelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CutSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeView3 = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowFilePropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OysterRootDirTxBx = New System.Windows.Forms.TextBox()
        Me.OysterWorkDirTxBx = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.InstructionOysterRhTxbx = New System.Windows.Forms.RichTextBox()
        Me.OysterWkDirBtn = New System.Windows.Forms.Button()
        Me.OysterRootDirBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.OysterJarLbl = New System.Windows.Forms.Label()
        Me.OysterJarTxBx = New System.Windows.Forms.TextBox()
        Me.OysterJarBtn = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.SaveAll = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RunOyster = New System.Windows.Forms.Button()
        Me.OysterXmlListBx = New System.Windows.Forms.ListBox()
        Me.ERMetricsSetup = New System.Windows.Forms.TabPage()
        Me.ERM_Wk_DirBtn = New System.Windows.Forms.Button()
        Me.LFDateLbl = New System.Windows.Forms.Label()
        Me.LinkFileDateERLbl = New System.Windows.Forms.Label()
        Me.ERMetricsLoadedFile = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ERMetLinkFileTxbx = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.MoveFileBtn = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ERMetricJarBtn = New System.Windows.Forms.Button()
        Me.ERMetricsJarTxBx = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.EROysterWkDirTxBx = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.EditFileERMetricsBtn = New System.Windows.Forms.Button()
        Me.SaveFileERMetricsBtn = New System.Windows.Forms.Button()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip3 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CommentSelectedTextToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UncommentSelectedTextToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeView4 = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip4 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameFileToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowFileDateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InstructionERMetricsRTxbx = New System.Windows.Forms.RichTextBox()
        Me.SaveERMetricsBtn = New System.Windows.Forms.Button()
        Me.RunERMetricsBtn = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ERMetricsWkDirBtn = New System.Windows.Forms.Button()
        Me.ERMetricsTxBx = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Run = New System.Windows.Forms.TabPage()
        Me.RunTimeValuelbl = New System.Windows.Forms.Label()
        Me.RunTimeEndlbl = New System.Windows.Forms.Label()
        Me.InputFileLbl = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Timelbl = New System.Windows.Forms.Label()
        Me.LinkFilelbl = New System.Windows.Forms.Label()
        Me.RunTimelbl = New System.Windows.Forms.Label()
        Me.OysterRunLinkFilelbl = New System.Windows.Forms.Label()
        Me.RunScriptNamelbl = New System.Windows.Forms.Label()
        Me.OysterRunLbl = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.ERMetricsRunRichTxtBx = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip6 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopyToClipboardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteFromClipboardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.ErrorInRunLbl = New System.Windows.Forms.Label()
        Me.StopRun = New System.Windows.Forms.Button()
        Me.RunRichTextBox = New System.Windows.Forms.RichTextBox()
        Me.WorkWithOysterFiles = New System.Windows.Forms.TabPage()
        Me.WrkOysFilesLbl = New System.Windows.Forms.Label()
        Me.WorkWithOysterFile = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.SaveFile_RichTextBox2 = New System.Windows.Forms.Button()
        Me.EditOystRunBtn = New System.Windows.Forms.Button()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip8 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip5 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpfulStuff = New System.Windows.Forms.TabPage()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.AnalyzeGB = New System.Windows.Forms.GroupBox()
        Me.AnotherFileRB = New System.Windows.Forms.RadioButton()
        Me.LastInputFileRB = New System.Windows.Forms.RadioButton()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Analyze_Btn = New System.Windows.Forms.Button()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.CBStrapBtn = New System.Windows.Forms.Button()
        Me.WgtFileTxBx = New System.Windows.Forms.TextBox()
        Me.InstructionRichTextBox6 = New System.Windows.Forms.RichTextBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.HSLinkDateLbl = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.CreateRefFilebtn = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.HSRefOutputTxBx = New System.Windows.Forms.TextBox()
        Me.InstructionRichTextBoxRef = New System.Windows.Forms.RichTextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.CreateCSVBtn = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.HSCsvTxBx = New System.Windows.Forms.TextBox()
        Me.HSLinkFileTxBx = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.HSOutCsvTxBx = New System.Windows.Forms.TextBox()
        Me.InstructionHSRchtxbx = New System.Windows.Forms.RichTextBox()
        Me.KnowledgeBaseMaint = New System.Windows.Forms.TabPage()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.KBMIdityLbl = New System.Windows.Forms.Label()
        Me.InstructionIKMRichTxBx = New System.Windows.Forms.RichTextBox()
        Me.KBMWholeWordChB = New System.Windows.Forms.CheckBox()
        Me.KBMFileLoadedLbl = New System.Windows.Forms.Label()
        Me.KBMTreeView = New System.Windows.Forms.TreeView()
        Me.KBMFilterCountLbl = New System.Windows.Forms.Label()
        Me.LoadCountLbl = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.ColumnCheckedLB = New System.Windows.Forms.CheckedListBox()
        Me.ResetFilterBtn = New System.Windows.Forms.Button()
        Me.SearchIdtyBtn = New System.Windows.Forms.Button()
        Me.KBMTextBox = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip7 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SelectEntireRowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KBMIdtyLbl = New System.Windows.Forms.Label()
        Me.KnowledgeBaseView = New System.Windows.Forms.TabPage()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.InstructionXMLRichTxBx = New System.Windows.Forms.RichTextBox()
        Me.KBCheckBox = New System.Windows.Forms.CheckBox()
        Me.KBFileLineCountLbl = New System.Windows.Forms.Label()
        Me.SearchKBTxtBx = New System.Windows.Forms.TextBox()
        Me.KBLoadFilelbl = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.KBSearchBtn = New System.Windows.Forms.Button()
        Me.RichTextBox11 = New System.Windows.Forms.RichTextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.IditySearchlbl = New System.Windows.Forms.Label()
        Me.KBSearchTreeView = New System.Windows.Forms.TreeView()
        Me.AssertionHelper = New System.Windows.Forms.TabPage()
        Me.AssertLabel = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.InstructionAssertRTB = New System.Windows.Forms.RichTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RichTextBox5 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.InstructAssertRTB = New System.Windows.Forms.RichTextBox()
        Me.RefToStrAssertion = New System.Windows.Forms.RadioButton()
        Me.RefToRefAssertion = New System.Windows.Forms.RadioButton()
        Me.StrToStrAssertion = New System.Windows.Forms.RadioButton()
        Me.StrSplitAssertion = New System.Windows.Forms.RadioButton()
        Me.SaveAssertFileBtn = New System.Windows.Forms.Button()
        Me.AssertDataGridView = New System.Windows.Forms.DataGridView()
        Me.Robot = New System.Windows.Forms.TabPage()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.PythonFileLbl = New System.Windows.Forms.Label()
        Me.EditRobotFile = New System.Windows.Forms.Button()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.RobotTreeView = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip9 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameFileToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.PythonExeTxBx = New System.Windows.Forms.TextBox()
        Me.RobotWkDirTxBx = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.RobotWkDirBtn = New System.Windows.Forms.Button()
        Me.PythonEXEBtn = New System.Windows.Forms.Button()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.SaveRobotBtn = New System.Windows.Forms.Button()
        Me.RobotSaveFileBtn = New System.Windows.Forms.Button()
        Me.RichTextBox21 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip10 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RichTextBox6 = New System.Windows.Forms.RichTextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.RobotRTBx = New System.Windows.Forms.RichTextBox()
        Me.RunPythonBtn = New System.Windows.Forms.Button()
        Me.TestTab = New System.Windows.Forms.TabPage()
        Me.InstructionsTestingRichxBx = New System.Windows.Forms.RichTextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.JarTestBtn = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.DeleteValuesBtn = New System.Windows.Forms.Button()
        Me.StartTests = New System.Windows.Forms.Button()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.CreateFileBtn = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.ReadTestFileBtn = New System.Windows.Forms.Button()
        Me.HideTab = New System.Windows.Forms.Button()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.RestartBtn = New System.Windows.Forms.Button()
        Me.OysterTestingWelcomeLbl = New System.Windows.Forms.Label()
        Me.InstructionTestRchTxBx = New System.Windows.Forms.RichTextBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Process1 = New System.Diagnostics.Process()
        Me.Process2 = New System.Diagnostics.Process()
        Me.Process3 = New System.Diagnostics.Process()
        Me.Process4 = New System.Diagnostics.Process()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.OysterTabs.SuspendLayout()
        Me.Welcome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.ERMetricsSetup.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip3.SuspendLayout()
        Me.ContextMenuStrip4.SuspendLayout()
        Me.Run.SuspendLayout()
        Me.ContextMenuStrip6.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WorkWithOysterFiles.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip8.SuspendLayout()
        Me.ContextMenuStrip5.SuspendLayout()
        Me.HelpfulStuff.SuspendLayout()
        Me.AnalyzeGB.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.KnowledgeBaseMaint.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip7.SuspendLayout()
        Me.KnowledgeBaseView.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.AssertionHelper.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.AssertDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Robot.SuspendLayout()
        Me.ContextMenuStrip9.SuspendLayout()
        Me.ContextMenuStrip10.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TestTab.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Exitbtn
        '
        Me.Exitbtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Exitbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Exitbtn.ForeColor = System.Drawing.Color.Black
        Me.Exitbtn.Location = New System.Drawing.Point(1224, 646)
        Me.Exitbtn.Margin = New System.Windows.Forms.Padding(2)
        Me.Exitbtn.Name = "Exitbtn"
        Me.Exitbtn.Size = New System.Drawing.Size(70, 30)
        Me.Exitbtn.TabIndex = 11
        Me.Exitbtn.Text = "Exit"
        Me.Exitbtn.UseVisualStyleBackColor = False
        '
        'HomeBtn
        '
        Me.HomeBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.HomeBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HomeBtn.ForeColor = System.Drawing.Color.Black
        Me.HomeBtn.Location = New System.Drawing.Point(1132, 646)
        Me.HomeBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.HomeBtn.Name = "HomeBtn"
        Me.HomeBtn.Size = New System.Drawing.Size(70, 30)
        Me.HomeBtn.TabIndex = 1
        Me.HomeBtn.Text = "Home"
        Me.HomeBtn.UseVisualStyleBackColor = False
        '
        'MenuStrip
        '
        Me.MenuStrip.AutoSize = False
        Me.MenuStrip.BackColor = System.Drawing.Color.Maroon
        Me.MenuStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip.Size = New System.Drawing.Size(1327, 28)
        Me.MenuStrip.Stretch = False
        Me.MenuStrip.TabIndex = 2
        Me.MenuStrip.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.FileToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.FileToolStripMenuItem.Text = "File  "
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.ExitToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ExitToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(104, 24)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OysterHelperToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold)
        Me.HelpToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'OysterHelperToolStripMenuItem
        '
        Me.OysterHelperToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.OysterHelperToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.OysterHelperToolStripMenuItem.Name = "OysterHelperToolStripMenuItem"
        Me.OysterHelperToolStripMenuItem.Size = New System.Drawing.Size(174, 24)
        Me.OysterHelperToolStripMenuItem.Text = "Oyster Helper"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.AboutToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(174, 24)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButton1, Me.ToolStripStatusLabel1, Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 691)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 10, 0)
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.Size = New System.Drawing.Size(1327, 26)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripDropDownButton1.DoubleClickEnabled = True
        Me.ToolStripDropDownButton1.Image = Global.OysterHelper.My.Resources.Resources.ualr2
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.ShowDropDownArrow = False
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(24, 24)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Window
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(79, 21)
        Me.ToolStripStatusLabel1.Text = "Oyster Helper"
        Me.ToolStripStatusLabel1.ToolTipText = "Oyster is Running"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.BackColor = System.Drawing.SystemColors.Window
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(75, 20)
        Me.ToolStripProgressBar1.Step = 20
        '
        'OysterTabs
        '
        Me.OysterTabs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OysterTabs.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.OysterTabs.Controls.Add(Me.Welcome)
        Me.OysterTabs.Controls.Add(Me.ERMetricsSetup)
        Me.OysterTabs.Controls.Add(Me.Run)
        Me.OysterTabs.Controls.Add(Me.WorkWithOysterFiles)
        Me.OysterTabs.Controls.Add(Me.HelpfulStuff)
        Me.OysterTabs.Controls.Add(Me.KnowledgeBaseMaint)
        Me.OysterTabs.Controls.Add(Me.KnowledgeBaseView)
        Me.OysterTabs.Controls.Add(Me.AssertionHelper)
        Me.OysterTabs.Controls.Add(Me.Robot)
        Me.OysterTabs.Controls.Add(Me.TestTab)
        Me.OysterTabs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterTabs.Location = New System.Drawing.Point(0, 30)
        Me.OysterTabs.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterTabs.Name = "OysterTabs"
        Me.OysterTabs.SelectedIndex = 0
        Me.OysterTabs.Size = New System.Drawing.Size(1327, 600)
        Me.OysterTabs.TabIndex = 4
        '
        'Welcome
        '
        Me.Welcome.BackColor = System.Drawing.Color.AliceBlue
        Me.Welcome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Welcome.Controls.Add(Me.WelLoadedFileLbl)
        Me.Welcome.Controls.Add(Me.Label4)
        Me.Welcome.Controls.Add(Me.Label9)
        Me.Welcome.Controls.Add(Me.Label17)
        Me.Welcome.Controls.Add(Me.PictureBox1)
        Me.Welcome.Controls.Add(Me.Label11)
        Me.Welcome.Controls.Add(Me.EditFileBtn)
        Me.Welcome.Controls.Add(Me.SaveFileBtn)
        Me.Welcome.Controls.Add(Me.RichTextBox9)
        Me.Welcome.Controls.Add(Me.TreeView3)
        Me.Welcome.Controls.Add(Me.OysterRootDirTxBx)
        Me.Welcome.Controls.Add(Me.OysterWorkDirTxBx)
        Me.Welcome.Controls.Add(Me.Label8)
        Me.Welcome.Controls.Add(Me.InstructionOysterRhTxbx)
        Me.Welcome.Controls.Add(Me.OysterWkDirBtn)
        Me.Welcome.Controls.Add(Me.OysterRootDirBtn)
        Me.Welcome.Controls.Add(Me.Label3)
        Me.Welcome.Controls.Add(Me.Label5)
        Me.Welcome.Controls.Add(Me.OysterJarLbl)
        Me.Welcome.Controls.Add(Me.OysterJarTxBx)
        Me.Welcome.Controls.Add(Me.OysterJarBtn)
        Me.Welcome.Controls.Add(Me.Label10)
        Me.Welcome.Controls.Add(Me.SaveAll)
        Me.Welcome.Controls.Add(Me.Label1)
        Me.Welcome.Controls.Add(Me.RunOyster)
        Me.Welcome.Controls.Add(Me.OysterXmlListBx)
        Me.Welcome.Location = New System.Drawing.Point(4, 28)
        Me.Welcome.Margin = New System.Windows.Forms.Padding(2)
        Me.Welcome.Name = "Welcome"
        Me.Welcome.Padding = New System.Windows.Forms.Padding(2)
        Me.Welcome.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Welcome.Size = New System.Drawing.Size(1319, 568)
        Me.Welcome.TabIndex = 0
        Me.Welcome.Text = "Oyster Setup & Run"
        '
        'WelLoadedFileLbl
        '
        Me.WelLoadedFileLbl.AutoSize = True
        Me.WelLoadedFileLbl.Location = New System.Drawing.Point(670, 121)
        Me.WelLoadedFileLbl.Name = "WelLoadedFileLbl"
        Me.WelLoadedFileLbl.Size = New System.Drawing.Size(100, 17)
        Me.WelLoadedFileLbl.TabIndex = 61
        Me.WelLoadedFileLbl.Text = "Loaded file is: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(671, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(421, 31)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Welcome to the Oyster ER Helper"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Maroon
        Me.Label9.Location = New System.Drawing.Point(403, 510)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(223, 17)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Double Click a File To View or Edit"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(14, 121)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(149, 17)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "Oyster Root Directory:"
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = CType(resources.GetObject("PictureBox1.ErrorImage"), System.Drawing.Image)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox1.TabIndex = 56
        Me.PictureBox1.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Maroon
        Me.Label11.Location = New System.Drawing.Point(437, 121)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(154, 17)
        Me.Label11.TabIndex = 55
        Me.Label11.Text = "Working Directory Files"
        '
        'EditFileBtn
        '
        Me.EditFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditFileBtn.Location = New System.Drawing.Point(822, 517)
        Me.EditFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.EditFileBtn.Name = "EditFileBtn"
        Me.EditFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.EditFileBtn.TabIndex = 12
        Me.EditFileBtn.Text = "Edit"
        Me.ToolTip1.SetToolTip(Me.EditFileBtn, "Edit the Loaded File")
        Me.EditFileBtn.UseVisualStyleBackColor = False
        '
        'SaveFileBtn
        '
        Me.SaveFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveFileBtn.Location = New System.Drawing.Point(1076, 517)
        Me.SaveFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveFileBtn.Name = "SaveFileBtn"
        Me.SaveFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveFileBtn.TabIndex = 13
        Me.SaveFileBtn.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveFileBtn, "Save the Loaded File")
        Me.SaveFileBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox9
        '
        Me.RichTextBox9.AcceptsTab = True
        Me.RichTextBox9.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox9.ContextMenuStrip = Me.ContextMenuStrip1
        Me.RichTextBox9.Location = New System.Drawing.Point(666, 143)
        Me.RichTextBox9.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox9.Name = "RichTextBox9"
        Me.RichTextBox9.Size = New System.Drawing.Size(638, 361)
        Me.RichTextBox9.TabIndex = 6
        Me.RichTextBox9.Text = ""
        Me.ToolTip1.SetToolTip(Me.RichTextBox9, "Right Click for Menus")
        Me.RichTextBox9.WordWrap = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentSelectedTextToolStripMenuItem, Me.UnCommentSelectedTextToolStripMenuItem, Me.CopySelectionToolStripMenuItem, Me.PasteSelectionToolStripMenuItem, Me.CutSelectionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(213, 114)
        '
        'CommentSelectedTextToolStripMenuItem
        '
        Me.CommentSelectedTextToolStripMenuItem.Name = "CommentSelectedTextToolStripMenuItem"
        Me.CommentSelectedTextToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CommentSelectedTextToolStripMenuItem.Text = "Comment Selected Text"
        '
        'UnCommentSelectedTextToolStripMenuItem
        '
        Me.UnCommentSelectedTextToolStripMenuItem.Name = "UnCommentSelectedTextToolStripMenuItem"
        Me.UnCommentSelectedTextToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.UnCommentSelectedTextToolStripMenuItem.Text = "Uncomment Selected Text"
        '
        'CopySelectionToolStripMenuItem
        '
        Me.CopySelectionToolStripMenuItem.Name = "CopySelectionToolStripMenuItem"
        Me.CopySelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CopySelectionToolStripMenuItem.Text = "Copy Selection"
        '
        'PasteSelectionToolStripMenuItem
        '
        Me.PasteSelectionToolStripMenuItem.Name = "PasteSelectionToolStripMenuItem"
        Me.PasteSelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.PasteSelectionToolStripMenuItem.Text = "Paste Selection"
        '
        'CutSelectionToolStripMenuItem
        '
        Me.CutSelectionToolStripMenuItem.Name = "CutSelectionToolStripMenuItem"
        Me.CutSelectionToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.CutSelectionToolStripMenuItem.Text = "Cut Selection"
        '
        'TreeView3
        '
        Me.TreeView3.ContextMenuStrip = Me.ContextMenuStrip2
        Me.TreeView3.Location = New System.Drawing.Point(382, 143)
        Me.TreeView3.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView3.Name = "TreeView3"
        Me.TreeView3.Size = New System.Drawing.Size(265, 361)
        Me.TreeView3.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.TreeView3, "Right Click for Menus")
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.DeleteSelectedFileToolStripMenuItem, Me.ShowFilePropertiesToolStripMenuItem, Me.RenameFileToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(217, 92)
        Me.ToolTip1.SetToolTip(Me.ContextMenuStrip2, "Right Click")
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem1.Text = "Make Copy of Selected File"
        '
        'DeleteSelectedFileToolStripMenuItem
        '
        Me.DeleteSelectedFileToolStripMenuItem.Name = "DeleteSelectedFileToolStripMenuItem"
        Me.DeleteSelectedFileToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.DeleteSelectedFileToolStripMenuItem.Text = "Delete Selected File"
        '
        'ShowFilePropertiesToolStripMenuItem
        '
        Me.ShowFilePropertiesToolStripMenuItem.Name = "ShowFilePropertiesToolStripMenuItem"
        Me.ShowFilePropertiesToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ShowFilePropertiesToolStripMenuItem.Text = "Show File Size"
        '
        'RenameFileToolStripMenuItem
        '
        Me.RenameFileToolStripMenuItem.Name = "RenameFileToolStripMenuItem"
        Me.RenameFileToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.RenameFileToolStripMenuItem.Text = "Rename File"
        '
        'OysterRootDirTxBx
        '
        Me.OysterRootDirTxBx.Location = New System.Drawing.Point(15, 143)
        Me.OysterRootDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterRootDirTxBx.Name = "OysterRootDirTxBx"
        Me.OysterRootDirTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterRootDirTxBx.Size = New System.Drawing.Size(267, 23)
        Me.OysterRootDirTxBx.TabIndex = 1
        '
        'OysterWorkDirTxBx
        '
        Me.OysterWorkDirTxBx.Location = New System.Drawing.Point(17, 204)
        Me.OysterWorkDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterWorkDirTxBx.Name = "OysterWorkDirTxBx"
        Me.OysterWorkDirTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterWorkDirTxBx.Size = New System.Drawing.Size(267, 23)
        Me.OysterWorkDirTxBx.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(14, 183)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(171, 17)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Oyster Working Directory:"
        '
        'InstructionOysterRhTxbx
        '
        Me.InstructionOysterRhTxbx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionOysterRhTxbx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionOysterRhTxbx.Location = New System.Drawing.Point(31, 22)
        Me.InstructionOysterRhTxbx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionOysterRhTxbx.Name = "InstructionOysterRhTxbx"
        Me.InstructionOysterRhTxbx.ReadOnly = True
        Me.InstructionOysterRhTxbx.Size = New System.Drawing.Size(599, 83)
        Me.InstructionOysterRhTxbx.TabIndex = 49
        Me.InstructionOysterRhTxbx.Text = resources.GetString("InstructionOysterRhTxbx.Text")
        '
        'OysterWkDirBtn
        '
        Me.OysterWkDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.OysterWkDirBtn.Location = New System.Drawing.Point(298, 199)
        Me.OysterWkDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterWkDirBtn.Name = "OysterWkDirBtn"
        Me.OysterWkDirBtn.Size = New System.Drawing.Size(64, 30)
        Me.OysterWkDirBtn.TabIndex = 4
        Me.OysterWkDirBtn.Text = "Browse"
        Me.OysterWkDirBtn.UseVisualStyleBackColor = False
        '
        'OysterRootDirBtn
        '
        Me.OysterRootDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.OysterRootDirBtn.Location = New System.Drawing.Point(298, 138)
        Me.OysterRootDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterRootDirBtn.Name = "OysterRootDirBtn"
        Me.OysterRootDirBtn.Size = New System.Drawing.Size(64, 30)
        Me.OysterRootDirBtn.TabIndex = 2
        Me.OysterRootDirBtn.Text = "Browse"
        Me.OysterRootDirBtn.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(27, 167)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 15)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Example: C:\Oyster"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Maroon
        Me.Label5.Location = New System.Drawing.Point(27, 229)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(191, 15)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Example: C:\Oyster\IdentityUpdate"
        '
        'OysterJarLbl
        '
        Me.OysterJarLbl.AutoSize = True
        Me.OysterJarLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterJarLbl.Location = New System.Drawing.Point(14, 250)
        Me.OysterJarLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.OysterJarLbl.Name = "OysterJarLbl"
        Me.OysterJarLbl.Size = New System.Drawing.Size(124, 17)
        Me.OysterJarLbl.TabIndex = 22
        Me.OysterJarLbl.Text = "Oyster Jar to Run:"
        '
        'OysterJarTxBx
        '
        Me.OysterJarTxBx.Location = New System.Drawing.Point(17, 271)
        Me.OysterJarTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterJarTxBx.Name = "OysterJarTxBx"
        Me.OysterJarTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterJarTxBx.Size = New System.Drawing.Size(267, 23)
        Me.OysterJarTxBx.TabIndex = 5
        '
        'OysterJarBtn
        '
        Me.OysterJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.OysterJarBtn.Location = New System.Drawing.Point(298, 267)
        Me.OysterJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterJarBtn.Name = "OysterJarBtn"
        Me.OysterJarBtn.Size = New System.Drawing.Size(64, 30)
        Me.OysterJarBtn.TabIndex = 6
        Me.OysterJarBtn.Text = "Browse"
        Me.OysterJarBtn.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Maroon
        Me.Label10.Location = New System.Drawing.Point(27, 296)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(195, 15)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Example: C:\Oyster\Oyster-3.6.4.jar"
        '
        'SaveAll
        '
        Me.SaveAll.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveAll.Location = New System.Drawing.Point(298, 333)
        Me.SaveAll.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveAll.Name = "SaveAll"
        Me.SaveAll.Size = New System.Drawing.Size(64, 30)
        Me.SaveAll.TabIndex = 7
        Me.SaveAll.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveAll, "Save Your Above" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Program Choices")
        Me.SaveAll.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 318)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(157, 17)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Oyster Run Script Files:"
        '
        'RunOyster
        '
        Me.RunOyster.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RunOyster.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RunOyster.ForeColor = System.Drawing.Color.Maroon
        Me.RunOyster.Location = New System.Drawing.Point(298, 395)
        Me.RunOyster.Margin = New System.Windows.Forms.Padding(2)
        Me.RunOyster.Name = "RunOyster"
        Me.RunOyster.Size = New System.Drawing.Size(64, 30)
        Me.RunOyster.TabIndex = 9
        Me.RunOyster.Text = "Run"
        Me.ToolTip1.SetToolTip(Me.RunOyster, "Run Oyster")
        Me.RunOyster.UseVisualStyleBackColor = False
        '
        'OysterXmlListBx
        '
        Me.OysterXmlListBx.FormattingEnabled = True
        Me.OysterXmlListBx.ItemHeight = 16
        Me.OysterXmlListBx.Location = New System.Drawing.Point(17, 340)
        Me.OysterXmlListBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterXmlListBx.Name = "OysterXmlListBx"
        Me.OysterXmlListBx.Size = New System.Drawing.Size(267, 148)
        Me.OysterXmlListBx.TabIndex = 4
        '
        'ERMetricsSetup
        '
        Me.ERMetricsSetup.BackColor = System.Drawing.Color.AliceBlue
        Me.ERMetricsSetup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ERMetricsSetup.Controls.Add(Me.ERM_Wk_DirBtn)
        Me.ERMetricsSetup.Controls.Add(Me.LFDateLbl)
        Me.ERMetricsSetup.Controls.Add(Me.LinkFileDateERLbl)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetricsLoadedFile)
        Me.ERMetricsSetup.Controls.Add(Me.Label22)
        Me.ERMetricsSetup.Controls.Add(Me.Label16)
        Me.ERMetricsSetup.Controls.Add(Me.Label7)
        Me.ERMetricsSetup.Controls.Add(Me.Label15)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetLinkFileTxbx)
        Me.ERMetricsSetup.Controls.Add(Me.PictureBox2)
        Me.ERMetricsSetup.Controls.Add(Me.MoveFileBtn)
        Me.ERMetricsSetup.Controls.Add(Me.Label14)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetricJarBtn)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetricsJarTxBx)
        Me.ERMetricsSetup.Controls.Add(Me.Label13)
        Me.ERMetricsSetup.Controls.Add(Me.EROysterWkDirTxBx)
        Me.ERMetricsSetup.Controls.Add(Me.Label12)
        Me.ERMetricsSetup.Controls.Add(Me.EditFileERMetricsBtn)
        Me.ERMetricsSetup.Controls.Add(Me.SaveFileERMetricsBtn)
        Me.ERMetricsSetup.Controls.Add(Me.RichTextBox3)
        Me.ERMetricsSetup.Controls.Add(Me.TreeView4)
        Me.ERMetricsSetup.Controls.Add(Me.InstructionERMetricsRTxbx)
        Me.ERMetricsSetup.Controls.Add(Me.SaveERMetricsBtn)
        Me.ERMetricsSetup.Controls.Add(Me.RunERMetricsBtn)
        Me.ERMetricsSetup.Controls.Add(Me.Label6)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetricsWkDirBtn)
        Me.ERMetricsSetup.Controls.Add(Me.ERMetricsTxBx)
        Me.ERMetricsSetup.Controls.Add(Me.Label2)
        Me.ERMetricsSetup.Location = New System.Drawing.Point(4, 28)
        Me.ERMetricsSetup.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsSetup.Name = "ERMetricsSetup"
        Me.ERMetricsSetup.Size = New System.Drawing.Size(1319, 568)
        Me.ERMetricsSetup.TabIndex = 5
        Me.ERMetricsSetup.Text = "ERMetrics Setup & Run"
        '
        'ERM_Wk_DirBtn
        '
        Me.ERM_Wk_DirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ERM_Wk_DirBtn.Location = New System.Drawing.Point(298, 267)
        Me.ERM_Wk_DirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ERM_Wk_DirBtn.Name = "ERM_Wk_DirBtn"
        Me.ERM_Wk_DirBtn.Size = New System.Drawing.Size(64, 30)
        Me.ERM_Wk_DirBtn.TabIndex = 87
        Me.ERM_Wk_DirBtn.Text = "Browse"
        Me.ERM_Wk_DirBtn.UseVisualStyleBackColor = False
        '
        'LFDateLbl
        '
        Me.LFDateLbl.AutoSize = True
        Me.LFDateLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LFDateLbl.ForeColor = System.Drawing.Color.Maroon
        Me.LFDateLbl.Location = New System.Drawing.Point(116, 364)
        Me.LFDateLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LFDateLbl.Name = "LFDateLbl"
        Me.LFDateLbl.Size = New System.Drawing.Size(33, 15)
        Me.LFDateLbl.TabIndex = 86
        Me.LFDateLbl.Text = "Date"
        '
        'LinkFileDateERLbl
        '
        Me.LinkFileDateERLbl.AutoSize = True
        Me.LinkFileDateERLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkFileDateERLbl.ForeColor = System.Drawing.Color.Maroon
        Me.LinkFileDateERLbl.Location = New System.Drawing.Point(27, 364)
        Me.LinkFileDateERLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LinkFileDateERLbl.Name = "LinkFileDateERLbl"
        Me.LinkFileDateERLbl.Size = New System.Drawing.Size(85, 15)
        Me.LinkFileDateERLbl.TabIndex = 85
        Me.LinkFileDateERLbl.Text = "Link File Date:"
        '
        'ERMetricsLoadedFile
        '
        Me.ERMetricsLoadedFile.AutoSize = True
        Me.ERMetricsLoadedFile.Location = New System.Drawing.Point(663, 121)
        Me.ERMetricsLoadedFile.Name = "ERMetricsLoadedFile"
        Me.ERMetricsLoadedFile.Size = New System.Drawing.Size(100, 17)
        Me.ERMetricsLoadedFile.TabIndex = 84
        Me.ERMetricsLoadedFile.Text = "Loaded file is: "
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(671, 40)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(428, 31)
        Me.Label22.TabIndex = 83
        Me.Label22.Text = "ER-Metrics Statistics in Oyster ER"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Maroon
        Me.Label16.Location = New System.Drawing.Point(403, 510)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(223, 17)
        Me.Label16.TabIndex = 82
        Me.Label16.Text = "Double Click a File To View or Edit"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Maroon
        Me.Label7.Location = New System.Drawing.Point(437, 121)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(154, 17)
        Me.Label7.TabIndex = 81
        Me.Label7.Text = "Working Directory Files"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(14, 318)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(203, 17)
        Me.Label15.TabIndex = 79
        Me.Label15.Text = "Oyster Link File From Last Run"
        '
        'ERMetLinkFileTxbx
        '
        Me.ERMetLinkFileTxbx.Location = New System.Drawing.Point(16, 337)
        Me.ERMetLinkFileTxbx.Name = "ERMetLinkFileTxbx"
        Me.ERMetLinkFileTxbx.ReadOnly = True
        Me.ERMetLinkFileTxbx.Size = New System.Drawing.Size(267, 23)
        Me.ERMetLinkFileTxbx.TabIndex = 78
        '
        'PictureBox2
        '
        Me.PictureBox2.ErrorImage = CType(resources.GetObject("PictureBox2.ErrorImage"), System.Drawing.Image)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox2.TabIndex = 75
        Me.PictureBox2.TabStop = False
        '
        'MoveFileBtn
        '
        Me.MoveFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.MoveFileBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoveFileBtn.Location = New System.Drawing.Point(298, 333)
        Me.MoveFileBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.MoveFileBtn.Name = "MoveFileBtn"
        Me.MoveFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.MoveFileBtn.TabIndex = 73
        Me.MoveFileBtn.Text = "Copy"
        Me.ToolTip1.SetToolTip(Me.MoveFileBtn, "Copy Link File " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "To ERMetrics" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Directory")
        Me.MoveFileBtn.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Maroon
        Me.Label14.Location = New System.Drawing.Point(27, 229)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(260, 15)
        Me.Label14.TabIndex = 71
        Me.Label14.Text = "Example: C:\Oyster\ERCalculator\er-metrics.jar"
        '
        'ERMetricJarBtn
        '
        Me.ERMetricJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ERMetricJarBtn.Location = New System.Drawing.Point(298, 200)
        Me.ERMetricJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricJarBtn.Name = "ERMetricJarBtn"
        Me.ERMetricJarBtn.Size = New System.Drawing.Size(64, 30)
        Me.ERMetricJarBtn.TabIndex = 70
        Me.ERMetricJarBtn.Text = "Browse"
        Me.ERMetricJarBtn.UseVisualStyleBackColor = False
        '
        'ERMetricsJarTxBx
        '
        Me.ERMetricsJarTxBx.Location = New System.Drawing.Point(16, 204)
        Me.ERMetricsJarTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsJarTxBx.Name = "ERMetricsJarTxBx"
        Me.ERMetricsJarTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.ERMetricsJarTxBx.Size = New System.Drawing.Size(267, 23)
        Me.ERMetricsJarTxBx.TabIndex = 68
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(14, 186)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(126, 17)
        Me.Label13.TabIndex = 69
        Me.Label13.Text = "ERMetrics Jar File:"
        '
        'EROysterWkDirTxBx
        '
        Me.EROysterWkDirTxBx.Location = New System.Drawing.Point(16, 271)
        Me.EROysterWkDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.EROysterWkDirTxBx.Name = "EROysterWkDirTxBx"
        Me.EROysterWkDirTxBx.Size = New System.Drawing.Size(267, 23)
        Me.EROysterWkDirTxBx.TabIndex = 66
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(14, 250)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(171, 17)
        Me.Label12.TabIndex = 67
        Me.Label12.Text = "Oyster Working Directory:"
        '
        'EditFileERMetricsBtn
        '
        Me.EditFileERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditFileERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditFileERMetricsBtn.Location = New System.Drawing.Point(822, 517)
        Me.EditFileERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.EditFileERMetricsBtn.Name = "EditFileERMetricsBtn"
        Me.EditFileERMetricsBtn.Size = New System.Drawing.Size(64, 30)
        Me.EditFileERMetricsBtn.TabIndex = 64
        Me.EditFileERMetricsBtn.Text = "Edit"
        Me.ToolTip1.SetToolTip(Me.EditFileERMetricsBtn, "Edit Loaded File")
        Me.EditFileERMetricsBtn.UseVisualStyleBackColor = False
        '
        'SaveFileERMetricsBtn
        '
        Me.SaveFileERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveFileERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveFileERMetricsBtn.Location = New System.Drawing.Point(1076, 517)
        Me.SaveFileERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveFileERMetricsBtn.Name = "SaveFileERMetricsBtn"
        Me.SaveFileERMetricsBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveFileERMetricsBtn.TabIndex = 63
        Me.SaveFileERMetricsBtn.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveFileERMetricsBtn, "Save the Loaded File")
        Me.SaveFileERMetricsBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox3.ContextMenuStrip = Me.ContextMenuStrip3
        Me.RichTextBox3.Location = New System.Drawing.Point(666, 143)
        Me.RichTextBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.Size = New System.Drawing.Size(638, 361)
        Me.RichTextBox3.TabIndex = 62
        Me.RichTextBox3.Text = ""
        Me.ToolTip1.SetToolTip(Me.RichTextBox3, "Right Click for Menus")
        Me.RichTextBox3.WordWrap = False
        '
        'ContextMenuStrip3
        '
        Me.ContextMenuStrip3.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CommentSelectedTextToolStripMenuItem1, Me.UncommentSelectedTextToolStripMenuItem1})
        Me.ContextMenuStrip3.Name = "ContextMenuStrip3"
        Me.ContextMenuStrip3.Size = New System.Drawing.Size(213, 48)
        '
        'CommentSelectedTextToolStripMenuItem1
        '
        Me.CommentSelectedTextToolStripMenuItem1.Name = "CommentSelectedTextToolStripMenuItem1"
        Me.CommentSelectedTextToolStripMenuItem1.Size = New System.Drawing.Size(212, 22)
        Me.CommentSelectedTextToolStripMenuItem1.Text = "Comment Selected Text"
        '
        'UncommentSelectedTextToolStripMenuItem1
        '
        Me.UncommentSelectedTextToolStripMenuItem1.Name = "UncommentSelectedTextToolStripMenuItem1"
        Me.UncommentSelectedTextToolStripMenuItem1.Size = New System.Drawing.Size(212, 22)
        Me.UncommentSelectedTextToolStripMenuItem1.Text = "Uncomment Selected Text"
        '
        'TreeView4
        '
        Me.TreeView4.ContextMenuStrip = Me.ContextMenuStrip4
        Me.TreeView4.Location = New System.Drawing.Point(382, 143)
        Me.TreeView4.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView4.Name = "TreeView4"
        Me.TreeView4.Size = New System.Drawing.Size(265, 361)
        Me.TreeView4.TabIndex = 61
        Me.ToolTip1.SetToolTip(Me.TreeView4, "Right Click for Menus")
        '
        'ContextMenuStrip4
        '
        Me.ContextMenuStrip4.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2, Me.ToolStripMenuItem3, Me.ToolStripMenuItem4, Me.RenameFileToolStripMenuItem2, Me.ShowFileDateToolStripMenuItem})
        Me.ContextMenuStrip4.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip4.Size = New System.Drawing.Size(217, 114)
        Me.ToolTip1.SetToolTip(Me.ContextMenuStrip4, "Right Click")
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem2.Text = "Make Copy of Selected File"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem3.Text = "Delete Selected File"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem4.Text = "Show File Size"
        '
        'RenameFileToolStripMenuItem2
        '
        Me.RenameFileToolStripMenuItem2.Name = "RenameFileToolStripMenuItem2"
        Me.RenameFileToolStripMenuItem2.Size = New System.Drawing.Size(216, 22)
        Me.RenameFileToolStripMenuItem2.Text = "Rename File"
        '
        'ShowFileDateToolStripMenuItem
        '
        Me.ShowFileDateToolStripMenuItem.Name = "ShowFileDateToolStripMenuItem"
        Me.ShowFileDateToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ShowFileDateToolStripMenuItem.Text = "Show File Date"
        '
        'InstructionERMetricsRTxbx
        '
        Me.InstructionERMetricsRTxbx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionERMetricsRTxbx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionERMetricsRTxbx.Location = New System.Drawing.Point(31, 22)
        Me.InstructionERMetricsRTxbx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionERMetricsRTxbx.Name = "InstructionERMetricsRTxbx"
        Me.InstructionERMetricsRTxbx.ReadOnly = True
        Me.InstructionERMetricsRTxbx.Size = New System.Drawing.Size(635, 97)
        Me.InstructionERMetricsRTxbx.TabIndex = 60
        Me.InstructionERMetricsRTxbx.Text = resources.GetString("InstructionERMetricsRTxbx.Text")
        '
        'SaveERMetricsBtn
        '
        Me.SaveERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveERMetricsBtn.Location = New System.Drawing.Point(219, 406)
        Me.SaveERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveERMetricsBtn.Name = "SaveERMetricsBtn"
        Me.SaveERMetricsBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveERMetricsBtn.TabIndex = 57
        Me.SaveERMetricsBtn.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveERMetricsBtn, "Save Your" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Program Choices")
        Me.SaveERMetricsBtn.UseVisualStyleBackColor = False
        '
        'RunERMetricsBtn
        '
        Me.RunERMetricsBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RunERMetricsBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RunERMetricsBtn.ForeColor = System.Drawing.Color.Maroon
        Me.RunERMetricsBtn.Location = New System.Drawing.Point(298, 406)
        Me.RunERMetricsBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RunERMetricsBtn.Name = "RunERMetricsBtn"
        Me.RunERMetricsBtn.Size = New System.Drawing.Size(64, 30)
        Me.RunERMetricsBtn.TabIndex = 56
        Me.RunERMetricsBtn.Text = "Run"
        Me.ToolTip1.SetToolTip(Me.RunERMetricsBtn, "Run ERMetrics")
        Me.RunERMetricsBtn.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Maroon
        Me.Label6.Location = New System.Drawing.Point(27, 167)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(185, 15)
        Me.Label6.TabIndex = 55
        Me.Label6.Text = "Example: C:\Oyster\ERCalculator"
        '
        'ERMetricsWkDirBtn
        '
        Me.ERMetricsWkDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ERMetricsWkDirBtn.Location = New System.Drawing.Point(298, 138)
        Me.ERMetricsWkDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsWkDirBtn.Name = "ERMetricsWkDirBtn"
        Me.ERMetricsWkDirBtn.Size = New System.Drawing.Size(64, 30)
        Me.ERMetricsWkDirBtn.TabIndex = 54
        Me.ERMetricsWkDirBtn.Text = "Browse"
        Me.ERMetricsWkDirBtn.UseVisualStyleBackColor = False
        '
        'ERMetricsTxBx
        '
        Me.ERMetricsTxBx.Location = New System.Drawing.Point(16, 143)
        Me.ERMetricsTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsTxBx.Name = "ERMetricsTxBx"
        Me.ERMetricsTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.ERMetricsTxBx.Size = New System.Drawing.Size(267, 23)
        Me.ERMetricsTxBx.TabIndex = 53
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 124)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(189, 17)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "ERMetrics Working Directory"
        '
        'Run
        '
        Me.Run.BackColor = System.Drawing.Color.AliceBlue
        Me.Run.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Run.Controls.Add(Me.RunTimeValuelbl)
        Me.Run.Controls.Add(Me.RunTimeEndlbl)
        Me.Run.Controls.Add(Me.InputFileLbl)
        Me.Run.Controls.Add(Me.Label30)
        Me.Run.Controls.Add(Me.Timelbl)
        Me.Run.Controls.Add(Me.LinkFilelbl)
        Me.Run.Controls.Add(Me.RunTimelbl)
        Me.Run.Controls.Add(Me.OysterRunLinkFilelbl)
        Me.Run.Controls.Add(Me.RunScriptNamelbl)
        Me.Run.Controls.Add(Me.OysterRunLbl)
        Me.Run.Controls.Add(Me.Label23)
        Me.Run.Controls.Add(Me.Label19)
        Me.Run.Controls.Add(Me.Label21)
        Me.Run.Controls.Add(Me.ERMetricsRunRichTxtBx)
        Me.Run.Controls.Add(Me.PictureBox3)
        Me.Run.Controls.Add(Me.ErrorInRunLbl)
        Me.Run.Controls.Add(Me.StopRun)
        Me.Run.Controls.Add(Me.RunRichTextBox)
        Me.Run.Location = New System.Drawing.Point(4, 28)
        Me.Run.Margin = New System.Windows.Forms.Padding(2)
        Me.Run.Name = "Run"
        Me.Run.Size = New System.Drawing.Size(1319, 568)
        Me.Run.TabIndex = 2
        Me.Run.Text = "Run Output"
        '
        'RunTimeValuelbl
        '
        Me.RunTimeValuelbl.AutoSize = True
        Me.RunTimeValuelbl.Location = New System.Drawing.Point(113, 83)
        Me.RunTimeValuelbl.Name = "RunTimeValuelbl"
        Me.RunTimeValuelbl.Size = New System.Drawing.Size(39, 17)
        Me.RunTimeValuelbl.TabIndex = 94
        Me.RunTimeValuelbl.Text = "Time"
        '
        'RunTimeEndlbl
        '
        Me.RunTimeEndlbl.AutoSize = True
        Me.RunTimeEndlbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RunTimeEndlbl.Location = New System.Drawing.Point(30, 83)
        Me.RunTimeEndlbl.Name = "RunTimeEndlbl"
        Me.RunTimeEndlbl.Size = New System.Drawing.Size(77, 17)
        Me.RunTimeEndlbl.TabIndex = 93
        Me.RunTimeEndlbl.Text = "Start Time:"
        '
        'InputFileLbl
        '
        Me.InputFileLbl.AutoSize = True
        Me.InputFileLbl.Location = New System.Drawing.Point(113, 41)
        Me.InputFileLbl.Name = "InputFileLbl"
        Me.InputFileLbl.Size = New System.Drawing.Size(39, 17)
        Me.InputFileLbl.TabIndex = 92
        Me.InputFileLbl.Text = "Input"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label30.Location = New System.Drawing.Point(42, 41)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(65, 17)
        Me.Label30.TabIndex = 91
        Me.Label30.Text = "InputFile:"
        '
        'Timelbl
        '
        Me.Timelbl.AutoSize = True
        Me.Timelbl.Location = New System.Drawing.Point(585, 83)
        Me.Timelbl.Name = "Timelbl"
        Me.Timelbl.Size = New System.Drawing.Size(64, 17)
        Me.Timelbl.TabIndex = 90
        Me.Timelbl.Text = "00:00:00"
        '
        'LinkFilelbl
        '
        Me.LinkFilelbl.AutoSize = True
        Me.LinkFilelbl.Location = New System.Drawing.Point(113, 63)
        Me.LinkFilelbl.Name = "LinkFilelbl"
        Me.LinkFilelbl.Size = New System.Drawing.Size(29, 17)
        Me.LinkFilelbl.TabIndex = 89
        Me.LinkFilelbl.Text = "link"
        '
        'RunTimelbl
        '
        Me.RunTimelbl.AutoSize = True
        Me.RunTimelbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RunTimelbl.Location = New System.Drawing.Point(440, 83)
        Me.RunTimelbl.Name = "RunTimelbl"
        Me.RunTimelbl.Size = New System.Drawing.Size(150, 17)
        Me.RunTimelbl.TabIndex = 88
        Me.RunTimelbl.Text = "Approx. Time Elapsed:"
        '
        'OysterRunLinkFilelbl
        '
        Me.OysterRunLinkFilelbl.AutoSize = True
        Me.OysterRunLinkFilelbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.OysterRunLinkFilelbl.Location = New System.Drawing.Point(43, 63)
        Me.OysterRunLinkFilelbl.Name = "OysterRunLinkFilelbl"
        Me.OysterRunLinkFilelbl.Size = New System.Drawing.Size(64, 17)
        Me.OysterRunLinkFilelbl.TabIndex = 87
        Me.OysterRunLinkFilelbl.Text = "Link File:"
        '
        'RunScriptNamelbl
        '
        Me.RunScriptNamelbl.AutoSize = True
        Me.RunScriptNamelbl.Location = New System.Drawing.Point(113, 19)
        Me.RunScriptNamelbl.Name = "RunScriptNamelbl"
        Me.RunScriptNamelbl.Size = New System.Drawing.Size(44, 17)
        Me.RunScriptNamelbl.TabIndex = 86
        Me.RunScriptNamelbl.Text = "Script"
        '
        'OysterRunLbl
        '
        Me.OysterRunLbl.AutoSize = True
        Me.OysterRunLbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.OysterRunLbl.Location = New System.Drawing.Point(29, 19)
        Me.OysterRunLbl.Name = "OysterRunLbl"
        Me.OysterRunLbl.Size = New System.Drawing.Size(78, 17)
        Me.OysterRunLbl.TabIndex = 85
        Me.OysterRunLbl.Text = "Run Script:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(671, 40)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(426, 31)
        Me.Label23.TabIndex = 84
        Me.Label23.Text = "Oyster and ERMetrics Run Output"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Maroon
        Me.Label19.Location = New System.Drawing.Point(906, 86)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(170, 20)
        Me.Label19.TabIndex = 83
        Me.Label19.Text = "ERMetrics Run Output"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Maroon
        Me.Label21.Location = New System.Drawing.Point(266, 86)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(142, 20)
        Me.Label21.TabIndex = 82
        Me.Label21.Text = "Oyster Run Output"
        '
        'ERMetricsRunRichTxtBx
        '
        Me.ERMetricsRunRichTxtBx.ContextMenuStrip = Me.ContextMenuStrip6
        Me.ERMetricsRunRichTxtBx.Location = New System.Drawing.Point(677, 112)
        Me.ERMetricsRunRichTxtBx.Margin = New System.Windows.Forms.Padding(2)
        Me.ERMetricsRunRichTxtBx.Name = "ERMetricsRunRichTxtBx"
        Me.ERMetricsRunRichTxtBx.Size = New System.Drawing.Size(628, 393)
        Me.ERMetricsRunRichTxtBx.TabIndex = 78
        Me.ERMetricsRunRichTxtBx.Text = ""
        '
        'ContextMenuStrip6
        '
        Me.ContextMenuStrip6.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopyToClipboardToolStripMenuItem, Me.PasteFromClipboardToolStripMenuItem})
        Me.ContextMenuStrip6.Name = "ContextMenuStrip6"
        Me.ContextMenuStrip6.Size = New System.Drawing.Size(189, 48)
        '
        'CopyToClipboardToolStripMenuItem
        '
        Me.CopyToClipboardToolStripMenuItem.Name = "CopyToClipboardToolStripMenuItem"
        Me.CopyToClipboardToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.CopyToClipboardToolStripMenuItem.Text = "Copy to Clipboard"
        '
        'PasteFromClipboardToolStripMenuItem
        '
        Me.PasteFromClipboardToolStripMenuItem.Name = "PasteFromClipboardToolStripMenuItem"
        Me.PasteFromClipboardToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.PasteFromClipboardToolStripMenuItem.Text = "Paste From Clipboard"
        '
        'PictureBox3
        '
        Me.PictureBox3.ErrorImage = CType(resources.GetObject("PictureBox3.ErrorImage"), System.Drawing.Image)
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox3.TabIndex = 57
        Me.PictureBox3.TabStop = False
        '
        'ErrorInRunLbl
        '
        Me.ErrorInRunLbl.AutoSize = True
        Me.ErrorInRunLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ErrorInRunLbl.ForeColor = System.Drawing.Color.Red
        Me.ErrorInRunLbl.Location = New System.Drawing.Point(439, 522)
        Me.ErrorInRunLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ErrorInRunLbl.Name = "ErrorInRunLbl"
        Me.ErrorInRunLbl.Size = New System.Drawing.Size(181, 20)
        Me.ErrorInRunLbl.TabIndex = 10
        Me.ErrorInRunLbl.Text = "Error in Run Output!!!"
        Me.ErrorInRunLbl.Visible = False
        '
        'StopRun
        '
        Me.StopRun.BackColor = System.Drawing.SystemColors.ControlLight
        Me.StopRun.Enabled = False
        Me.StopRun.ForeColor = System.Drawing.Color.Maroon
        Me.StopRun.Location = New System.Drawing.Point(105, 518)
        Me.StopRun.Margin = New System.Windows.Forms.Padding(2)
        Me.StopRun.Name = "StopRun"
        Me.StopRun.Size = New System.Drawing.Size(70, 30)
        Me.StopRun.TabIndex = 9
        Me.StopRun.Text = "Stop"
        Me.ToolTip1.SetToolTip(Me.StopRun, "Stop Oyster and All Java Processes")
        Me.StopRun.UseVisualStyleBackColor = False
        '
        'RunRichTextBox
        '
        Me.RunRichTextBox.ContextMenuStrip = Me.ContextMenuStrip6
        Me.RunRichTextBox.Location = New System.Drawing.Point(22, 112)
        Me.RunRichTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.RunRichTextBox.Name = "RunRichTextBox"
        Me.RunRichTextBox.Size = New System.Drawing.Size(630, 393)
        Me.RunRichTextBox.TabIndex = 0
        Me.RunRichTextBox.Text = ""
        '
        'WorkWithOysterFiles
        '
        Me.WorkWithOysterFiles.BackColor = System.Drawing.Color.AliceBlue
        Me.WorkWithOysterFiles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.WorkWithOysterFiles.Controls.Add(Me.WrkOysFilesLbl)
        Me.WorkWithOysterFiles.Controls.Add(Me.WorkWithOysterFile)
        Me.WorkWithOysterFiles.Controls.Add(Me.Label24)
        Me.WorkWithOysterFiles.Controls.Add(Me.Label20)
        Me.WorkWithOysterFiles.Controls.Add(Me.PictureBox4)
        Me.WorkWithOysterFiles.Controls.Add(Me.SaveFile_RichTextBox2)
        Me.WorkWithOysterFiles.Controls.Add(Me.EditOystRunBtn)
        Me.WorkWithOysterFiles.Controls.Add(Me.RichTextBox2)
        Me.WorkWithOysterFiles.Controls.Add(Me.TreeView1)
        Me.WorkWithOysterFiles.Location = New System.Drawing.Point(4, 28)
        Me.WorkWithOysterFiles.Margin = New System.Windows.Forms.Padding(2)
        Me.WorkWithOysterFiles.Name = "WorkWithOysterFiles"
        Me.WorkWithOysterFiles.Size = New System.Drawing.Size(1319, 568)
        Me.WorkWithOysterFiles.TabIndex = 3
        Me.WorkWithOysterFiles.Text = "Work with Oyster Files"
        '
        'WrkOysFilesLbl
        '
        Me.WrkOysFilesLbl.AutoSize = True
        Me.WrkOysFilesLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WrkOysFilesLbl.ForeColor = System.Drawing.Color.Maroon
        Me.WrkOysFilesLbl.Location = New System.Drawing.Point(109, 86)
        Me.WrkOysFilesLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.WrkOysFilesLbl.Name = "WrkOysFilesLbl"
        Me.WrkOysFilesLbl.Size = New System.Drawing.Size(145, 17)
        Me.WrkOysFilesLbl.TabIndex = 86
        Me.WrkOysFilesLbl.Text = "Oyster Root Directory"
        '
        'WorkWithOysterFile
        '
        Me.WorkWithOysterFile.AutoSize = True
        Me.WorkWithOysterFile.Location = New System.Drawing.Point(361, 86)
        Me.WorkWithOysterFile.Name = "WorkWithOysterFile"
        Me.WorkWithOysterFile.Size = New System.Drawing.Size(100, 17)
        Me.WorkWithOysterFile.TabIndex = 85
        Me.WorkWithOysterFile.Text = "Loaded file is: "
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(671, 40)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(327, 31)
        Me.Label24.TabIndex = 84
        Me.Label24.Text = "Working With Oyster Files"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Maroon
        Me.Label20.Location = New System.Drawing.Point(70, 508)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(223, 17)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Double Click a File To View or Edit"
        '
        'PictureBox4
        '
        Me.PictureBox4.ErrorImage = CType(resources.GetObject("PictureBox4.ErrorImage"), System.Drawing.Image)
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox4.TabIndex = 79
        Me.PictureBox4.TabStop = False
        '
        'SaveFile_RichTextBox2
        '
        Me.SaveFile_RichTextBox2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveFile_RichTextBox2.Location = New System.Drawing.Point(954, 519)
        Me.SaveFile_RichTextBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveFile_RichTextBox2.Name = "SaveFile_RichTextBox2"
        Me.SaveFile_RichTextBox2.Size = New System.Drawing.Size(64, 30)
        Me.SaveFile_RichTextBox2.TabIndex = 7
        Me.SaveFile_RichTextBox2.Text = "Save"
        Me.ToolTip1.SetToolTip(Me.SaveFile_RichTextBox2, "Save Edited File")
        Me.SaveFile_RichTextBox2.UseVisualStyleBackColor = False
        '
        'EditOystRunBtn
        '
        Me.EditOystRunBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditOystRunBtn.Location = New System.Drawing.Point(650, 519)
        Me.EditOystRunBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.EditOystRunBtn.Name = "EditOystRunBtn"
        Me.EditOystRunBtn.Size = New System.Drawing.Size(64, 30)
        Me.EditOystRunBtn.TabIndex = 6
        Me.EditOystRunBtn.Text = "Edit"
        Me.ToolTip1.SetToolTip(Me.EditOystRunBtn, "Edit Loaded File")
        Me.EditOystRunBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox2.ContextMenuStrip = Me.ContextMenuStrip8
        Me.RichTextBox2.Location = New System.Drawing.Point(364, 109)
        Me.RichTextBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(941, 392)
        Me.RichTextBox2.TabIndex = 4
        Me.RichTextBox2.Text = ""
        Me.ToolTip1.SetToolTip(Me.RichTextBox2, "Right Click for Menus")
        Me.RichTextBox2.WordWrap = False
        '
        'ContextMenuStrip8
        '
        Me.ContextMenuStrip8.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip8.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem9, Me.ToolStripMenuItem10, Me.ToolStripMenuItem11, Me.ToolStripMenuItem12, Me.ToolStripMenuItem13})
        Me.ContextMenuStrip8.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip8.Size = New System.Drawing.Size(213, 114)
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(212, 22)
        Me.ToolStripMenuItem9.Text = "Comment Selected Text"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(212, 22)
        Me.ToolStripMenuItem10.Text = "Uncomment Selected Text"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(212, 22)
        Me.ToolStripMenuItem11.Text = "Copy Selection"
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(212, 22)
        Me.ToolStripMenuItem12.Text = "Paste Selection"
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(212, 22)
        Me.ToolStripMenuItem13.Text = "Cut Selection"
        '
        'TreeView1
        '
        Me.TreeView1.ContextMenuStrip = Me.ContextMenuStrip5
        Me.TreeView1.Location = New System.Drawing.Point(20, 109)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(2)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(323, 392)
        Me.TreeView1.TabIndex = 3
        '
        'ContextMenuStrip5
        '
        Me.ContextMenuStrip5.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip5.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem5, Me.ToolStripMenuItem6, Me.ToolStripMenuItem7, Me.ToolStripMenuItem15})
        Me.ContextMenuStrip5.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip5.Size = New System.Drawing.Size(217, 92)
        Me.ToolTip1.SetToolTip(Me.ContextMenuStrip5, "Right Click")
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem5.Text = "Make Copy of Selected File"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem6.Text = "Delete Selected File"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem7.Text = "Show File Size"
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem15.Text = " "
        '
        'HelpfulStuff
        '
        Me.HelpfulStuff.BackColor = System.Drawing.Color.AliceBlue
        Me.HelpfulStuff.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.HelpfulStuff.Controls.Add(Me.Label50)
        Me.HelpfulStuff.Controls.Add(Me.AnalyzeGB)
        Me.HelpfulStuff.Controls.Add(Me.Label49)
        Me.HelpfulStuff.Controls.Add(Me.Analyze_Btn)
        Me.HelpfulStuff.Controls.Add(Me.Label48)
        Me.HelpfulStuff.Controls.Add(Me.Label47)
        Me.HelpfulStuff.Controls.Add(Me.CBStrapBtn)
        Me.HelpfulStuff.Controls.Add(Me.WgtFileTxBx)
        Me.HelpfulStuff.Controls.Add(Me.InstructionRichTextBox6)
        Me.HelpfulStuff.Controls.Add(Me.LinkLabel1)
        Me.HelpfulStuff.Controls.Add(Me.HSLinkDateLbl)
        Me.HelpfulStuff.Controls.Add(Me.Label32)
        Me.HelpfulStuff.Controls.Add(Me.Label25)
        Me.HelpfulStuff.Controls.Add(Me.Label18)
        Me.HelpfulStuff.Controls.Add(Me.CreateRefFilebtn)
        Me.HelpfulStuff.Controls.Add(Me.Label34)
        Me.HelpfulStuff.Controls.Add(Me.HSRefOutputTxBx)
        Me.HelpfulStuff.Controls.Add(Me.InstructionRichTextBoxRef)
        Me.HelpfulStuff.Controls.Add(Me.Label29)
        Me.HelpfulStuff.Controls.Add(Me.PictureBox6)
        Me.HelpfulStuff.Controls.Add(Me.CreateCSVBtn)
        Me.HelpfulStuff.Controls.Add(Me.Label26)
        Me.HelpfulStuff.Controls.Add(Me.HSCsvTxBx)
        Me.HelpfulStuff.Controls.Add(Me.HSLinkFileTxBx)
        Me.HelpfulStuff.Controls.Add(Me.Label27)
        Me.HelpfulStuff.Controls.Add(Me.Label28)
        Me.HelpfulStuff.Controls.Add(Me.HSOutCsvTxBx)
        Me.HelpfulStuff.Controls.Add(Me.InstructionHSRchtxbx)
        Me.HelpfulStuff.Location = New System.Drawing.Point(4, 28)
        Me.HelpfulStuff.Name = "HelpfulStuff"
        Me.HelpfulStuff.Size = New System.Drawing.Size(1319, 568)
        Me.HelpfulStuff.TabIndex = 7
        Me.HelpfulStuff.Text = "Helpful Stuff"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.ForeColor = System.Drawing.Color.Black
        Me.Label50.Location = New System.Drawing.Point(45, 433)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(329, 68)
        Me.Label50.TabIndex = 111
        Me.Label50.Text = "This can be Helpful for finding Input File Errors and" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "for choosing Attributes fo" &
    "r the Entity Resolution " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Scoring Rule Weights Generator by exploring your " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "las" &
    "t input file or a file of your choice."
        '
        'AnalyzeGB
        '
        Me.AnalyzeGB.Controls.Add(Me.AnotherFileRB)
        Me.AnalyzeGB.Controls.Add(Me.LastInputFileRB)
        Me.AnalyzeGB.ForeColor = System.Drawing.Color.Maroon
        Me.AnalyzeGB.Location = New System.Drawing.Point(396, 408)
        Me.AnalyzeGB.Name = "AnalyzeGB"
        Me.AnalyzeGB.Size = New System.Drawing.Size(263, 82)
        Me.AnalyzeGB.TabIndex = 110
        Me.AnalyzeGB.TabStop = False
        Me.AnalyzeGB.Text = "Choose File To Analyze"
        '
        'AnotherFileRB
        '
        Me.AnotherFileRB.AutoSize = True
        Me.AnotherFileRB.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.AnotherFileRB.Location = New System.Drawing.Point(23, 52)
        Me.AnotherFileRB.Name = "AnotherFileRB"
        Me.AnotherFileRB.Size = New System.Drawing.Size(137, 21)
        Me.AnotherFileRB.TabIndex = 1
        Me.AnotherFileRB.TabStop = True
        Me.AnotherFileRB.Text = "Another Input File"
        Me.AnotherFileRB.UseVisualStyleBackColor = True
        '
        'LastInputFileRB
        '
        Me.LastInputFileRB.AutoSize = True
        Me.LastInputFileRB.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LastInputFileRB.Location = New System.Drawing.Point(23, 25)
        Me.LastInputFileRB.Name = "LastInputFileRB"
        Me.LastInputFileRB.Size = New System.Drawing.Size(190, 21)
        Me.LastInputFileRB.TabIndex = 0
        Me.LastInputFileRB.TabStop = True
        Me.LastInputFileRB.Text = "Last Oyster Run Input File"
        Me.LastInputFileRB.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.ForeColor = System.Drawing.Color.Maroon
        Me.Label49.Location = New System.Drawing.Point(48, 408)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(250, 17)
        Me.Label49.TabIndex = 109
        Me.Label49.Text = "Quick and Basic Analysis of Your Data"
        '
        'Analyze_Btn
        '
        Me.Analyze_Btn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Analyze_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Analyze_Btn.ForeColor = System.Drawing.Color.Maroon
        Me.Analyze_Btn.Location = New System.Drawing.Point(678, 434)
        Me.Analyze_Btn.Name = "Analyze_Btn"
        Me.Analyze_Btn.Size = New System.Drawing.Size(70, 30)
        Me.Analyze_Btn.TabIndex = 108
        Me.Analyze_Btn.Text = "Analyze"
        Me.Analyze_Btn.UseVisualStyleBackColor = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(795, 399)
        Me.Label48.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(355, 17)
        Me.Label48.TabIndex = 107
        Me.Label48.Text = "New Bootstrap Weight Analysis File with same Delimiter"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.ForeColor = System.Drawing.Color.Maroon
        Me.Label47.Location = New System.Drawing.Point(48, 289)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(203, 17)
        Me.Label47.TabIndex = 106
        Me.Label47.Text = "Bootstrap Weight Analysis File."
        '
        'CBStrapBtn
        '
        Me.CBStrapBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CBStrapBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBStrapBtn.ForeColor = System.Drawing.Color.Maroon
        Me.CBStrapBtn.Location = New System.Drawing.Point(1224, 421)
        Me.CBStrapBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.CBStrapBtn.Name = "CBStrapBtn"
        Me.CBStrapBtn.Size = New System.Drawing.Size(64, 30)
        Me.CBStrapBtn.TabIndex = 105
        Me.CBStrapBtn.Text = "Create"
        Me.ToolTip1.SetToolTip(Me.CBStrapBtn, "Create The New File")
        Me.CBStrapBtn.UseVisualStyleBackColor = False
        '
        'WgtFileTxBx
        '
        Me.WgtFileTxBx.Location = New System.Drawing.Point(795, 425)
        Me.WgtFileTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.WgtFileTxBx.Name = "WgtFileTxBx"
        Me.WgtFileTxBx.ReadOnly = True
        Me.WgtFileTxBx.Size = New System.Drawing.Size(401, 23)
        Me.WgtFileTxBx.TabIndex = 104
        Me.ToolTip1.SetToolTip(Me.WgtFileTxBx, "This will be Generated")
        '
        'InstructionRichTextBox6
        '
        Me.InstructionRichTextBox6.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionRichTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionRichTextBox6.Location = New System.Drawing.Point(48, 314)
        Me.InstructionRichTextBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionRichTextBox6.Name = "InstructionRichTextBox6"
        Me.InstructionRichTextBox6.ReadOnly = True
        Me.InstructionRichTextBox6.Size = New System.Drawing.Size(720, 80)
        Me.InstructionRichTextBox6.TabIndex = 103
        Me.InstructionRichTextBox6.Text = resources.GetString("InstructionRichTextBox6.Text")
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline
        Me.LinkLabel1.Location = New System.Drawing.Point(45, 530)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(323, 17)
        Me.LinkLabel1.TabIndex = 102
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Entity Resolution Scoring Rule Weights Generator"
        '
        'HSLinkDateLbl
        '
        Me.HSLinkDateLbl.AutoSize = True
        Me.HSLinkDateLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HSLinkDateLbl.ForeColor = System.Drawing.Color.Maroon
        Me.HSLinkDateLbl.Location = New System.Drawing.Point(899, 235)
        Me.HSLinkDateLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.HSLinkDateLbl.Name = "HSLinkDateLbl"
        Me.HSLinkDateLbl.Size = New System.Drawing.Size(33, 15)
        Me.HSLinkDateLbl.TabIndex = 101
        Me.HSLinkDateLbl.Text = "Date"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Maroon
        Me.Label32.Location = New System.Drawing.Point(810, 235)
        Me.Label32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(85, 15)
        Me.Label32.TabIndex = 100
        Me.Label32.Text = "Link File Date:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.Maroon
        Me.Label25.Location = New System.Drawing.Point(48, 175)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(168, 17)
        Me.Label25.TabIndex = 99
        Me.Label25.Text = "Reference ERMetrics File"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Maroon
        Me.Label18.Location = New System.Drawing.Point(48, 62)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(300, 17)
        Me.Label18.TabIndex = 98
        Me.Label18.Text = "New Input File from Oyster Boolean Rule Runs"
        '
        'CreateRefFilebtn
        '
        Me.CreateRefFilebtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CreateRefFilebtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CreateRefFilebtn.ForeColor = System.Drawing.Color.Maroon
        Me.CreateRefFilebtn.Location = New System.Drawing.Point(1224, 346)
        Me.CreateRefFilebtn.Margin = New System.Windows.Forms.Padding(2)
        Me.CreateRefFilebtn.Name = "CreateRefFilebtn"
        Me.CreateRefFilebtn.Size = New System.Drawing.Size(64, 30)
        Me.CreateRefFilebtn.TabIndex = 97
        Me.CreateRefFilebtn.Text = "Create"
        Me.ToolTip1.SetToolTip(Me.CreateRefFilebtn, "Create The New File")
        Me.CreateRefFilebtn.UseVisualStyleBackColor = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(795, 325)
        Me.Label34.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(199, 17)
        Me.Label34.TabIndex = 95
        Me.Label34.Text = "New ERMetrics Reference File"
        '
        'HSRefOutputTxBx
        '
        Me.HSRefOutputTxBx.Location = New System.Drawing.Point(795, 350)
        Me.HSRefOutputTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.HSRefOutputTxBx.Name = "HSRefOutputTxBx"
        Me.HSRefOutputTxBx.ReadOnly = True
        Me.HSRefOutputTxBx.Size = New System.Drawing.Size(401, 23)
        Me.HSRefOutputTxBx.TabIndex = 94
        Me.ToolTip1.SetToolTip(Me.HSRefOutputTxBx, "This will be Generated")
        '
        'InstructionRichTextBoxRef
        '
        Me.InstructionRichTextBoxRef.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionRichTextBoxRef.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionRichTextBoxRef.Location = New System.Drawing.Point(48, 200)
        Me.InstructionRichTextBoxRef.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionRichTextBoxRef.Name = "InstructionRichTextBoxRef"
        Me.InstructionRichTextBoxRef.ReadOnly = True
        Me.InstructionRichTextBoxRef.Size = New System.Drawing.Size(720, 84)
        Me.InstructionRichTextBoxRef.TabIndex = 90
        Me.InstructionRichTextBoxRef.Text = resources.GetString("InstructionRichTextBoxRef.Text")
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(671, 40)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(278, 31)
        Me.Label29.TabIndex = 89
        Me.Label29.Text = "Oyster Helper Utilities"
        '
        'PictureBox6
        '
        Me.PictureBox6.ErrorImage = CType(resources.GetObject("PictureBox6.ErrorImage"), System.Drawing.Image)
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox6.TabIndex = 88
        Me.PictureBox6.TabStop = False
        '
        'CreateCSVBtn
        '
        Me.CreateCSVBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CreateCSVBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CreateCSVBtn.ForeColor = System.Drawing.Color.Maroon
        Me.CreateCSVBtn.Location = New System.Drawing.Point(1224, 280)
        Me.CreateCSVBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.CreateCSVBtn.Name = "CreateCSVBtn"
        Me.CreateCSVBtn.Size = New System.Drawing.Size(64, 30)
        Me.CreateCSVBtn.TabIndex = 87
        Me.CreateCSVBtn.Text = "Create"
        Me.ToolTip1.SetToolTip(Me.CreateCSVBtn, "Create The New File")
        Me.CreateCSVBtn.UseVisualStyleBackColor = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label26.Location = New System.Drawing.Point(795, 123)
        Me.Label26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(172, 17)
        Me.Label26.TabIndex = 66
        Me.Label26.Text = "Last Oyster Run Input File"
        '
        'HSCsvTxBx
        '
        Me.HSCsvTxBx.AllowDrop = True
        Me.HSCsvTxBx.BackColor = System.Drawing.SystemColors.Control
        Me.HSCsvTxBx.ForeColor = System.Drawing.SystemColors.WindowText
        Me.HSCsvTxBx.Location = New System.Drawing.Point(795, 147)
        Me.HSCsvTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.HSCsvTxBx.Name = "HSCsvTxBx"
        Me.HSCsvTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.HSCsvTxBx.Size = New System.Drawing.Size(493, 23)
        Me.HSCsvTxBx.TabIndex = 58
        '
        'HSLinkFileTxBx
        '
        Me.HSLinkFileTxBx.BackColor = System.Drawing.SystemColors.Control
        Me.HSLinkFileTxBx.Location = New System.Drawing.Point(795, 208)
        Me.HSLinkFileTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.HSLinkFileTxBx.Name = "HSLinkFileTxBx"
        Me.HSLinkFileTxBx.Size = New System.Drawing.Size(493, 23)
        Me.HSLinkFileTxBx.TabIndex = 60
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label27.Location = New System.Drawing.Point(795, 184)
        Me.Label27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(167, 17)
        Me.Label27.TabIndex = 62
        Me.Label27.Text = "Last Oyster Run Link File"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(795, 259)
        Me.Label28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(231, 17)
        Me.Label28.TabIndex = 65
        Me.Label28.Text = "New Input File using same Delimiter"
        '
        'HSOutCsvTxBx
        '
        Me.HSOutCsvTxBx.Location = New System.Drawing.Point(795, 284)
        Me.HSOutCsvTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.HSOutCsvTxBx.Name = "HSOutCsvTxBx"
        Me.HSOutCsvTxBx.ReadOnly = True
        Me.HSOutCsvTxBx.Size = New System.Drawing.Size(401, 23)
        Me.HSOutCsvTxBx.TabIndex = 63
        Me.ToolTip1.SetToolTip(Me.HSOutCsvTxBx, "This will be Generated")
        '
        'InstructionHSRchtxbx
        '
        Me.InstructionHSRchtxbx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionHSRchtxbx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionHSRchtxbx.Location = New System.Drawing.Point(48, 86)
        Me.InstructionHSRchtxbx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionHSRchtxbx.Name = "InstructionHSRchtxbx"
        Me.InstructionHSRchtxbx.ReadOnly = True
        Me.InstructionHSRchtxbx.Size = New System.Drawing.Size(720, 84)
        Me.InstructionHSRchtxbx.TabIndex = 50
        Me.InstructionHSRchtxbx.Text = resources.GetString("InstructionHSRchtxbx.Text")
        '
        'KnowledgeBaseMaint
        '
        Me.KnowledgeBaseMaint.BackColor = System.Drawing.Color.AliceBlue
        Me.KnowledgeBaseMaint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.KnowledgeBaseMaint.Controls.Add(Me.Label45)
        Me.KnowledgeBaseMaint.Controls.Add(Me.PictureBox8)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMIdityLbl)
        Me.KnowledgeBaseMaint.Controls.Add(Me.InstructionIKMRichTxBx)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMWholeWordChB)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMFileLoadedLbl)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMTreeView)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMFilterCountLbl)
        Me.KnowledgeBaseMaint.Controls.Add(Me.LoadCountLbl)
        Me.KnowledgeBaseMaint.Controls.Add(Me.Label36)
        Me.KnowledgeBaseMaint.Controls.Add(Me.Label33)
        Me.KnowledgeBaseMaint.Controls.Add(Me.ColumnCheckedLB)
        Me.KnowledgeBaseMaint.Controls.Add(Me.ResetFilterBtn)
        Me.KnowledgeBaseMaint.Controls.Add(Me.SearchIdtyBtn)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMTextBox)
        Me.KnowledgeBaseMaint.Controls.Add(Me.DataGridView1)
        Me.KnowledgeBaseMaint.Controls.Add(Me.KBMIdtyLbl)
        Me.KnowledgeBaseMaint.Location = New System.Drawing.Point(4, 28)
        Me.KnowledgeBaseMaint.Name = "KnowledgeBaseMaint"
        Me.KnowledgeBaseMaint.Size = New System.Drawing.Size(1319, 568)
        Me.KnowledgeBaseMaint.TabIndex = 8
        Me.KnowledgeBaseMaint.Text = "IKB Searching"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.Maroon
        Me.Label45.Location = New System.Drawing.Point(25, 109)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(346, 31)
        Me.Label45.TabIndex = 100
        Me.Label45.Text = "Knowledge Base Searching"
        '
        'PictureBox8
        '
        Me.PictureBox8.ErrorImage = CType(resources.GetObject("PictureBox8.ErrorImage"), System.Drawing.Image)
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox8.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox8.TabIndex = 90
        Me.PictureBox8.TabStop = False
        '
        'KBMIdityLbl
        '
        Me.KBMIdityLbl.AutoSize = True
        Me.KBMIdityLbl.ForeColor = System.Drawing.Color.Maroon
        Me.KBMIdityLbl.Location = New System.Drawing.Point(556, 23)
        Me.KBMIdityLbl.Name = "KBMIdityLbl"
        Me.KBMIdityLbl.Size = New System.Drawing.Size(123, 17)
        Me.KBMIdityLbl.TabIndex = 62
        Me.KBMIdityLbl.Text = "Available IKB Files"
        '
        'InstructionIKMRichTxBx
        '
        Me.InstructionIKMRichTxBx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionIKMRichTxBx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionIKMRichTxBx.Location = New System.Drawing.Point(31, 22)
        Me.InstructionIKMRichTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionIKMRichTxBx.Name = "InstructionIKMRichTxBx"
        Me.InstructionIKMRichTxBx.ReadOnly = True
        Me.InstructionIKMRichTxBx.Size = New System.Drawing.Size(330, 83)
        Me.InstructionIKMRichTxBx.TabIndex = 61
        Me.InstructionIKMRichTxBx.Text = "1. Begin by loading your IKB File for Searching." & Global.Microsoft.VisualBasic.ChrW(10) & "2. General or specific searching" &
    " is available." & Global.Microsoft.VisualBasic.ChrW(10) & "3. Right click to copy a Search Term or Search by the " & Global.Microsoft.VisualBasic.ChrW(10) & "    Oyster" &
    "Id for an EIS in the IKB XML View."
        '
        'KBMWholeWordChB
        '
        Me.KBMWholeWordChB.AutoSize = True
        Me.KBMWholeWordChB.Location = New System.Drawing.Point(1013, 117)
        Me.KBMWholeWordChB.Name = "KBMWholeWordChB"
        Me.KBMWholeWordChB.Size = New System.Drawing.Size(105, 21)
        Me.KBMWholeWordChB.TabIndex = 15
        Me.KBMWholeWordChB.Text = "Whole Word"
        Me.KBMWholeWordChB.UseVisualStyleBackColor = True
        '
        'KBMFileLoadedLbl
        '
        Me.KBMFileLoadedLbl.AutoSize = True
        Me.KBMFileLoadedLbl.Location = New System.Drawing.Point(18, 167)
        Me.KBMFileLoadedLbl.Name = "KBMFileLoadedLbl"
        Me.KBMFileLoadedLbl.Size = New System.Drawing.Size(100, 17)
        Me.KBMFileLoadedLbl.TabIndex = 14
        Me.KBMFileLoadedLbl.Text = "File Loaded is:"
        '
        'KBMTreeView
        '
        Me.KBMTreeView.Location = New System.Drawing.Point(450, 43)
        Me.KBMTreeView.Name = "KBMTreeView"
        Me.KBMTreeView.Size = New System.Drawing.Size(335, 112)
        Me.KBMTreeView.TabIndex = 13
        '
        'KBMFilterCountLbl
        '
        Me.KBMFilterCountLbl.AutoSize = True
        Me.KBMFilterCountLbl.Location = New System.Drawing.Point(1085, 169)
        Me.KBMFilterCountLbl.Name = "KBMFilterCountLbl"
        Me.KBMFilterCountLbl.Size = New System.Drawing.Size(114, 17)
        Me.KBMFilterCountLbl.TabIndex = 11
        Me.KBMFilterCountLbl.Text = "Filtered Count is:"
        '
        'LoadCountLbl
        '
        Me.LoadCountLbl.AutoSize = True
        Me.LoadCountLbl.Location = New System.Drawing.Point(794, 169)
        Me.LoadCountLbl.Name = "LoadCountLbl"
        Me.LoadCountLbl.Size = New System.Drawing.Size(164, 17)
        Me.LoadCountLbl.TabIndex = 10
        Me.LoadCountLbl.Text = "Reference Line Count is:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(803, 23)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(185, 17)
        Me.Label36.TabIndex = 9
        Me.Label36.Text = "Click on a Column to Search"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(1010, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(128, 17)
        Me.Label33.TabIndex = 8
        Me.Label33.Text = "Enter Search Term"
        '
        'ColumnCheckedLB
        '
        Me.ColumnCheckedLB.FormattingEnabled = True
        Me.ColumnCheckedLB.Location = New System.Drawing.Point(791, 43)
        Me.ColumnCheckedLB.Name = "ColumnCheckedLB"
        Me.ColumnCheckedLB.Size = New System.Drawing.Size(208, 94)
        Me.ColumnCheckedLB.TabIndex = 7
        '
        'ResetFilterBtn
        '
        Me.ResetFilterBtn.ForeColor = System.Drawing.Color.Maroon
        Me.ResetFilterBtn.Location = New System.Drawing.Point(1150, 81)
        Me.ResetFilterBtn.Name = "ResetFilterBtn"
        Me.ResetFilterBtn.Size = New System.Drawing.Size(64, 30)
        Me.ResetFilterBtn.TabIndex = 6
        Me.ResetFilterBtn.Text = "Reset"
        Me.ToolTip1.SetToolTip(Me.ResetFilterBtn, "Clear Search")
        Me.ResetFilterBtn.UseVisualStyleBackColor = True
        '
        'SearchIdtyBtn
        '
        Me.SearchIdtyBtn.ForeColor = System.Drawing.Color.Maroon
        Me.SearchIdtyBtn.Location = New System.Drawing.Point(1010, 81)
        Me.SearchIdtyBtn.Name = "SearchIdtyBtn"
        Me.SearchIdtyBtn.Size = New System.Drawing.Size(64, 30)
        Me.SearchIdtyBtn.TabIndex = 5
        Me.SearchIdtyBtn.Text = "Search"
        Me.ToolTip1.SetToolTip(Me.SearchIdtyBtn, "Search the Loaded IKB")
        Me.SearchIdtyBtn.UseVisualStyleBackColor = True
        '
        'KBMTextBox
        '
        Me.KBMTextBox.Location = New System.Drawing.Point(1010, 43)
        Me.KBMTextBox.Name = "KBMTextBox"
        Me.KBMTextBox.Size = New System.Drawing.Size(204, 23)
        Me.KBMTextBox.TabIndex = 4
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.ContextMenuStrip = Me.ContextMenuStrip7
        Me.DataGridView1.Location = New System.Drawing.Point(21, 191)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridView1.Size = New System.Drawing.Size(1283, 360)
        Me.DataGridView1.TabIndex = 3
        Me.DataGridView1.VirtualMode = True
        '
        'ContextMenuStrip7
        '
        Me.ContextMenuStrip7.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip7.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem8, Me.SelectEntireRowToolStripMenuItem})
        Me.ContextMenuStrip7.Name = "ContextMenuStrip6"
        Me.ContextMenuStrip7.Size = New System.Drawing.Size(242, 48)
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem8.Text = "Copy to Search Textbox"
        '
        'SelectEntireRowToolStripMenuItem
        '
        Me.SelectEntireRowToolStripMenuItem.Name = "SelectEntireRowToolStripMenuItem"
        Me.SelectEntireRowToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.SelectEntireRowToolStripMenuItem.Text = "Search Original File for OysterID"
        '
        'KBMIdtyLbl
        '
        Me.KBMIdtyLbl.AutoSize = True
        Me.KBMIdtyLbl.ForeColor = System.Drawing.Color.Maroon
        Me.KBMIdtyLbl.Location = New System.Drawing.Point(529, 159)
        Me.KBMIdtyLbl.Name = "KBMIdtyLbl"
        Me.KBMIdtyLbl.Size = New System.Drawing.Size(176, 17)
        Me.KBMIdtyLbl.TabIndex = 1
        Me.KBMIdtyLbl.Text = "Double Click a File to Load"
        '
        'KnowledgeBaseView
        '
        Me.KnowledgeBaseView.BackColor = System.Drawing.Color.AliceBlue
        Me.KnowledgeBaseView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.KnowledgeBaseView.Controls.Add(Me.Label43)
        Me.KnowledgeBaseView.Controls.Add(Me.InstructionXMLRichTxBx)
        Me.KnowledgeBaseView.Controls.Add(Me.KBCheckBox)
        Me.KnowledgeBaseView.Controls.Add(Me.KBFileLineCountLbl)
        Me.KnowledgeBaseView.Controls.Add(Me.SearchKBTxtBx)
        Me.KnowledgeBaseView.Controls.Add(Me.KBLoadFilelbl)
        Me.KnowledgeBaseView.Controls.Add(Me.Label35)
        Me.KnowledgeBaseView.Controls.Add(Me.PictureBox5)
        Me.KnowledgeBaseView.Controls.Add(Me.KBSearchBtn)
        Me.KnowledgeBaseView.Controls.Add(Me.RichTextBox11)
        Me.KnowledgeBaseView.Controls.Add(Me.Label31)
        Me.KnowledgeBaseView.Controls.Add(Me.IditySearchlbl)
        Me.KnowledgeBaseView.Controls.Add(Me.KBSearchTreeView)
        Me.KnowledgeBaseView.Location = New System.Drawing.Point(4, 28)
        Me.KnowledgeBaseView.Name = "KnowledgeBaseView"
        Me.KnowledgeBaseView.Size = New System.Drawing.Size(1319, 568)
        Me.KnowledgeBaseView.TabIndex = 9
        Me.KnowledgeBaseView.Text = "IKB XML View"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(693, 499)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(128, 17)
        Me.Label43.TabIndex = 97
        Me.Label43.Text = "Enter Search Term"
        '
        'InstructionXMLRichTxBx
        '
        Me.InstructionXMLRichTxBx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionXMLRichTxBx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionXMLRichTxBx.Location = New System.Drawing.Point(31, 22)
        Me.InstructionXMLRichTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionXMLRichTxBx.Name = "InstructionXMLRichTxBx"
        Me.InstructionXMLRichTxBx.ReadOnly = True
        Me.InstructionXMLRichTxBx.Size = New System.Drawing.Size(515, 57)
        Me.InstructionXMLRichTxBx.TabIndex = 96
        Me.InstructionXMLRichTxBx.Text = "1. Use IKB Searching Tab to find OysterID's unless you know the OysterID." & Global.Microsoft.VisualBasic.ChrW(10) & "2. Larg" &
    "e raw files can take a long time to load. Please have patience." & Global.Microsoft.VisualBasic.ChrW(10) & "3. Click to Sear" &
    "ch after filling the Search Term."
        '
        'KBCheckBox
        '
        Me.KBCheckBox.AutoSize = True
        Me.KBCheckBox.Location = New System.Drawing.Point(1005, 523)
        Me.KBCheckBox.Name = "KBCheckBox"
        Me.KBCheckBox.Size = New System.Drawing.Size(138, 21)
        Me.KBCheckBox.TabIndex = 95
        Me.KBCheckBox.Text = "Whole Word Only"
        Me.KBCheckBox.UseVisualStyleBackColor = True
        '
        'KBFileLineCountLbl
        '
        Me.KBFileLineCountLbl.AutoSize = True
        Me.KBFileLineCountLbl.Location = New System.Drawing.Point(912, 85)
        Me.KBFileLineCountLbl.Name = "KBFileLineCountLbl"
        Me.KBFileLineCountLbl.Size = New System.Drawing.Size(94, 17)
        Me.KBFileLineCountLbl.TabIndex = 94
        Me.KBFileLineCountLbl.Text = "Line Count is:"
        '
        'SearchKBTxtBx
        '
        Me.SearchKBTxtBx.Location = New System.Drawing.Point(696, 522)
        Me.SearchKBTxtBx.Margin = New System.Windows.Forms.Padding(2)
        Me.SearchKBTxtBx.Name = "SearchKBTxtBx"
        Me.SearchKBTxtBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.SearchKBTxtBx.Size = New System.Drawing.Size(275, 23)
        Me.SearchKBTxtBx.TabIndex = 92
        '
        'KBLoadFilelbl
        '
        Me.KBLoadFilelbl.AutoSize = True
        Me.KBLoadFilelbl.Location = New System.Drawing.Point(323, 85)
        Me.KBLoadFilelbl.Name = "KBLoadFilelbl"
        Me.KBLoadFilelbl.Size = New System.Drawing.Size(100, 17)
        Me.KBLoadFilelbl.TabIndex = 91
        Me.KBLoadFilelbl.Text = "Loaded file is: "
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(671, 40)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(441, 31)
        Me.Label35.TabIndex = 90
        Me.Label35.Text = "Identity Knowledge Base XML View"
        '
        'PictureBox5
        '
        Me.PictureBox5.ErrorImage = CType(resources.GetObject("PictureBox5.ErrorImage"), System.Drawing.Image)
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox5.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox5.TabIndex = 89
        Me.PictureBox5.TabStop = False
        '
        'KBSearchBtn
        '
        Me.KBSearchBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.KBSearchBtn.ForeColor = System.Drawing.Color.Maroon
        Me.KBSearchBtn.Location = New System.Drawing.Point(612, 518)
        Me.KBSearchBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.KBSearchBtn.Name = "KBSearchBtn"
        Me.KBSearchBtn.Size = New System.Drawing.Size(64, 30)
        Me.KBSearchBtn.TabIndex = 87
        Me.KBSearchBtn.Text = "Search"
        Me.ToolTip1.SetToolTip(Me.KBSearchBtn, "Search the Loaded File")
        Me.KBSearchBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox11
        '
        Me.RichTextBox11.AcceptsTab = True
        Me.RichTextBox11.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox11.CausesValidation = False
        Me.RichTextBox11.DetectUrls = False
        Me.RichTextBox11.Location = New System.Drawing.Point(326, 109)
        Me.RichTextBox11.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox11.Name = "RichTextBox11"
        Me.RichTextBox11.Size = New System.Drawing.Size(979, 376)
        Me.RichTextBox11.TabIndex = 86
        Me.RichTextBox11.Text = ""
        Me.RichTextBox11.WordWrap = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Maroon
        Me.Label31.Location = New System.Drawing.Point(39, 495)
        Me.Label31.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(244, 17)
        Me.Label31.TabIndex = 85
        Me.Label31.Text = "Double Click a File To View or Search"
        '
        'IditySearchlbl
        '
        Me.IditySearchlbl.AutoSize = True
        Me.IditySearchlbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IditySearchlbl.ForeColor = System.Drawing.Color.Maroon
        Me.IditySearchlbl.Location = New System.Drawing.Point(107, 85)
        Me.IditySearchlbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.IditySearchlbl.Name = "IditySearchlbl"
        Me.IditySearchlbl.Size = New System.Drawing.Size(108, 17)
        Me.IditySearchlbl.TabIndex = 84
        Me.IditySearchlbl.Text = "Oyster IKB Files"
        '
        'KBSearchTreeView
        '
        Me.KBSearchTreeView.ContextMenuStrip = Me.ContextMenuStrip5
        Me.KBSearchTreeView.Location = New System.Drawing.Point(20, 109)
        Me.KBSearchTreeView.Margin = New System.Windows.Forms.Padding(2)
        Me.KBSearchTreeView.Name = "KBSearchTreeView"
        Me.KBSearchTreeView.Size = New System.Drawing.Size(283, 376)
        Me.KBSearchTreeView.TabIndex = 83
        '
        'AssertionHelper
        '
        Me.AssertionHelper.BackColor = System.Drawing.Color.AliceBlue
        Me.AssertionHelper.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.AssertionHelper.Controls.Add(Me.AssertLabel)
        Me.AssertionHelper.Controls.Add(Me.Label46)
        Me.AssertionHelper.Controls.Add(Me.Label44)
        Me.AssertionHelper.Controls.Add(Me.PictureBox9)
        Me.AssertionHelper.Controls.Add(Me.InstructionAssertRTB)
        Me.AssertionHelper.Controls.Add(Me.GroupBox1)
        Me.AssertionHelper.Controls.Add(Me.SaveAssertFileBtn)
        Me.AssertionHelper.Controls.Add(Me.AssertDataGridView)
        Me.AssertionHelper.Location = New System.Drawing.Point(4, 28)
        Me.AssertionHelper.Name = "AssertionHelper"
        Me.AssertionHelper.Size = New System.Drawing.Size(1319, 568)
        Me.AssertionHelper.TabIndex = 12
        Me.AssertionHelper.Text = "Assertion Helper"
        '
        'AssertLabel
        '
        Me.AssertLabel.AutoSize = True
        Me.AssertLabel.ForeColor = System.Drawing.Color.Maroon
        Me.AssertLabel.Location = New System.Drawing.Point(763, 96)
        Me.AssertLabel.Name = "AssertLabel"
        Me.AssertLabel.Size = New System.Drawing.Size(162, 17)
        Me.AssertLabel.TabIndex = 102
        Me.AssertLabel.Text = "Template Loaded is for: "
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.ForeColor = System.Drawing.Color.Maroon
        Me.Label46.Location = New System.Drawing.Point(766, 514)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(156, 17)
        Me.Label46.TabIndex = 101
        Me.Label46.Text = "Assertion File Template"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(671, 40)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(423, 31)
        Me.Label44.TabIndex = 99
        Me.Label44.Text = "Oyster Assertion Template Helper"
        '
        'PictureBox9
        '
        Me.PictureBox9.ErrorImage = CType(resources.GetObject("PictureBox9.ErrorImage"), System.Drawing.Image)
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox9.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox9.TabIndex = 98
        Me.PictureBox9.TabStop = False
        '
        'InstructionAssertRTB
        '
        Me.InstructionAssertRTB.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionAssertRTB.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionAssertRTB.Location = New System.Drawing.Point(31, 22)
        Me.InstructionAssertRTB.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionAssertRTB.Name = "InstructionAssertRTB"
        Me.InstructionAssertRTB.ReadOnly = True
        Me.InstructionAssertRTB.Size = New System.Drawing.Size(622, 80)
        Me.InstructionAssertRTB.TabIndex = 97
        Me.InstructionAssertRTB.Text = resources.GetString("InstructionAssertRTB.Text")
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.AliceBlue
        Me.GroupBox1.Controls.Add(Me.RichTextBox5)
        Me.GroupBox1.Controls.Add(Me.RichTextBox4)
        Me.GroupBox1.Controls.Add(Me.RichTextBox1)
        Me.GroupBox1.Controls.Add(Me.InstructAssertRTB)
        Me.GroupBox1.Controls.Add(Me.RefToStrAssertion)
        Me.GroupBox1.Controls.Add(Me.RefToRefAssertion)
        Me.GroupBox1.Controls.Add(Me.StrToStrAssertion)
        Me.GroupBox1.Controls.Add(Me.StrSplitAssertion)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Maroon
        Me.GroupBox1.Location = New System.Drawing.Point(52, 121)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(308, 387)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Assertion Templates"
        '
        'RichTextBox5
        '
        Me.RichTextBox5.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox5.Location = New System.Drawing.Point(21, 300)
        Me.RichTextBox5.Name = "RichTextBox5"
        Me.RichTextBox5.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RichTextBox5.Size = New System.Drawing.Size(280, 60)
        Me.RichTextBox5.TabIndex = 18
        Me.RichTextBox5.Text = "Forces a single identity structure found in an existing knowledge base to be divi" &
    "ded into two (2) or more identity structures. "
        '
        'RichTextBox4
        '
        Me.RichTextBox4.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox4.Location = New System.Drawing.Point(21, 211)
        Me.RichTextBox4.Name = "RichTextBox4"
        Me.RichTextBox4.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RichTextBox4.Size = New System.Drawing.Size(280, 60)
        Me.RichTextBox4.TabIndex = 17
        Me.RichTextBox4.Text = "Forces multiple identity structures found in an existing EIS to be consolidated i" &
    "nto a single identity structure."
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(21, 126)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RichTextBox1.Size = New System.Drawing.Size(280, 60)
        Me.RichTextBox1.TabIndex = 16
        Me.RichTextBox1.Text = "Forces multiple references to be consolidated with an existing identity structure" &
    "."
        '
        'InstructAssertRTB
        '
        Me.InstructAssertRTB.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructAssertRTB.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructAssertRTB.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstructAssertRTB.Location = New System.Drawing.Point(21, 45)
        Me.InstructAssertRTB.Name = "InstructAssertRTB"
        Me.InstructAssertRTB.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.InstructAssertRTB.Size = New System.Drawing.Size(280, 60)
        Me.InstructAssertRTB.TabIndex = 15
        Me.InstructAssertRTB.Text = "Allows an initial knowledgebase to be created. The process of forcing references " &
    "to match."
        '
        'RefToStrAssertion
        '
        Me.RefToStrAssertion.AutoSize = True
        Me.RefToStrAssertion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefToStrAssertion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.RefToStrAssertion.Location = New System.Drawing.Point(15, 105)
        Me.RefToStrAssertion.Name = "RefToStrAssertion"
        Me.RefToStrAssertion.Size = New System.Drawing.Size(175, 22)
        Me.RefToStrAssertion.TabIndex = 14
        Me.RefToStrAssertion.TabStop = True
        Me.RefToStrAssertion.Text = "Reference to Structure"
        Me.RefToStrAssertion.UseVisualStyleBackColor = True
        '
        'RefToRefAssertion
        '
        Me.RefToRefAssertion.AutoSize = True
        Me.RefToRefAssertion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefToRefAssertion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.RefToRefAssertion.Location = New System.Drawing.Point(15, 21)
        Me.RefToRefAssertion.Name = "RefToRefAssertion"
        Me.RefToRefAssertion.Size = New System.Drawing.Size(188, 22)
        Me.RefToRefAssertion.TabIndex = 13
        Me.RefToRefAssertion.TabStop = True
        Me.RefToRefAssertion.Text = "Reference To Reference"
        Me.RefToRefAssertion.UseVisualStyleBackColor = True
        '
        'StrToStrAssertion
        '
        Me.StrToStrAssertion.AutoSize = True
        Me.StrToStrAssertion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StrToStrAssertion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.StrToStrAssertion.Location = New System.Drawing.Point(15, 189)
        Me.StrToStrAssertion.Name = "StrToStrAssertion"
        Me.StrToStrAssertion.Size = New System.Drawing.Size(172, 22)
        Me.StrToStrAssertion.TabIndex = 8
        Me.StrToStrAssertion.TabStop = True
        Me.StrToStrAssertion.Text = "Structure To Structure"
        Me.StrToStrAssertion.UseVisualStyleBackColor = True
        '
        'StrSplitAssertion
        '
        Me.StrSplitAssertion.AutoSize = True
        Me.StrSplitAssertion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StrSplitAssertion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.StrSplitAssertion.Location = New System.Drawing.Point(15, 274)
        Me.StrSplitAssertion.Name = "StrSplitAssertion"
        Me.StrSplitAssertion.Size = New System.Drawing.Size(118, 22)
        Me.StrSplitAssertion.TabIndex = 9
        Me.StrSplitAssertion.TabStop = True
        Me.StrSplitAssertion.Text = "Structure Split"
        Me.StrSplitAssertion.UseVisualStyleBackColor = True
        '
        'SaveAssertFileBtn
        '
        Me.SaveAssertFileBtn.Location = New System.Drawing.Point(1126, 522)
        Me.SaveAssertFileBtn.Name = "SaveAssertFileBtn"
        Me.SaveAssertFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveAssertFileBtn.TabIndex = 15
        Me.SaveAssertFileBtn.Text = "Save"
        Me.SaveAssertFileBtn.UseVisualStyleBackColor = True
        '
        'AssertDataGridView
        '
        Me.AssertDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.AssertDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.AssertDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AssertDataGridView.Location = New System.Drawing.Point(384, 121)
        Me.AssertDataGridView.Name = "AssertDataGridView"
        Me.AssertDataGridView.Size = New System.Drawing.Size(920, 387)
        Me.AssertDataGridView.TabIndex = 10
        '
        'Robot
        '
        Me.Robot.BackColor = System.Drawing.Color.AliceBlue
        Me.Robot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Robot.Controls.Add(Me.Label56)
        Me.Robot.Controls.Add(Me.PythonFileLbl)
        Me.Robot.Controls.Add(Me.EditRobotFile)
        Me.Robot.Controls.Add(Me.Label58)
        Me.Robot.Controls.Add(Me.Label59)
        Me.Robot.Controls.Add(Me.RobotTreeView)
        Me.Robot.Controls.Add(Me.Label52)
        Me.Robot.Controls.Add(Me.PythonExeTxBx)
        Me.Robot.Controls.Add(Me.RobotWkDirTxBx)
        Me.Robot.Controls.Add(Me.Label53)
        Me.Robot.Controls.Add(Me.RobotWkDirBtn)
        Me.Robot.Controls.Add(Me.PythonEXEBtn)
        Me.Robot.Controls.Add(Me.Label54)
        Me.Robot.Controls.Add(Me.Label55)
        Me.Robot.Controls.Add(Me.SaveRobotBtn)
        Me.Robot.Controls.Add(Me.RobotSaveFileBtn)
        Me.Robot.Controls.Add(Me.RichTextBox21)
        Me.Robot.Controls.Add(Me.RichTextBox6)
        Me.Robot.Controls.Add(Me.Label51)
        Me.Robot.Controls.Add(Me.PictureBox10)
        Me.Robot.Controls.Add(Me.RobotRTBx)
        Me.Robot.Controls.Add(Me.RunPythonBtn)
        Me.Robot.Location = New System.Drawing.Point(4, 28)
        Me.Robot.Name = "Robot"
        Me.Robot.Size = New System.Drawing.Size(1319, 568)
        Me.Robot.TabIndex = 13
        Me.Robot.Text = "Oyster Robot"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.Maroon
        Me.Label56.Location = New System.Drawing.Point(1009, 104)
        Me.Label56.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(140, 20)
        Me.Label56.TabIndex = 121
        Me.Label56.Text = "Robot Run Output"
        '
        'PythonFileLbl
        '
        Me.PythonFileLbl.AutoSize = True
        Me.PythonFileLbl.Location = New System.Drawing.Point(524, 107)
        Me.PythonFileLbl.Name = "PythonFileLbl"
        Me.PythonFileLbl.Size = New System.Drawing.Size(100, 17)
        Me.PythonFileLbl.TabIndex = 120
        Me.PythonFileLbl.Text = "Loaded file is: "
        '
        'EditRobotFile
        '
        Me.EditRobotFile.BackColor = System.Drawing.SystemColors.ControlLight
        Me.EditRobotFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditRobotFile.Location = New System.Drawing.Point(489, 528)
        Me.EditRobotFile.Margin = New System.Windows.Forms.Padding(2)
        Me.EditRobotFile.Name = "EditRobotFile"
        Me.EditRobotFile.Size = New System.Drawing.Size(64, 30)
        Me.EditRobotFile.TabIndex = 119
        Me.EditRobotFile.Text = "Edit"
        Me.ToolTip1.SetToolTip(Me.EditRobotFile, "Edit the Loaded File")
        Me.EditRobotFile.UseVisualStyleBackColor = False
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.Maroon
        Me.Label58.Location = New System.Drawing.Point(37, 524)
        Me.Label58.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(223, 17)
        Me.Label58.TabIndex = 118
        Me.Label58.Text = "Double Click a File To View or Edit"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label59.Location = New System.Drawing.Point(50, 266)
        Me.Label59.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(196, 17)
        Me.Label59.TabIndex = 117
        Me.Label59.Text = "Robot Working Directory Files"
        '
        'RobotTreeView
        '
        Me.RobotTreeView.ContextMenuStrip = Me.ContextMenuStrip9
        Me.RobotTreeView.Location = New System.Drawing.Point(15, 288)
        Me.RobotTreeView.Margin = New System.Windows.Forms.Padding(2)
        Me.RobotTreeView.Name = "RobotTreeView"
        Me.RobotTreeView.Size = New System.Drawing.Size(267, 228)
        Me.RobotTreeView.TabIndex = 116
        Me.ToolTip1.SetToolTip(Me.RobotTreeView, "Right Click for Menus")
        '
        'ContextMenuStrip9
        '
        Me.ContextMenuStrip9.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip9.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem14, Me.ToolStripMenuItem16, Me.ToolStripMenuItem17, Me.RenameFileToolStripMenuItem1})
        Me.ContextMenuStrip9.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip9.Size = New System.Drawing.Size(217, 92)
        Me.ToolTip1.SetToolTip(Me.ContextMenuStrip9, "Right Click")
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem14.Text = "Make Copy of Selected File"
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem16.Text = "Delete Selected File"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(216, 22)
        Me.ToolStripMenuItem17.Text = "Show File Size"
        '
        'RenameFileToolStripMenuItem1
        '
        Me.RenameFileToolStripMenuItem1.Name = "RenameFileToolStripMenuItem1"
        Me.RenameFileToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.RenameFileToolStripMenuItem1.Text = "Rename File"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(14, 130)
        Me.Label52.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(219, 17)
        Me.Label52.TabIndex = 115
        Me.Label52.Text = "Python Executable Directory Path"
        '
        'PythonExeTxBx
        '
        Me.PythonExeTxBx.Location = New System.Drawing.Point(15, 152)
        Me.PythonExeTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.PythonExeTxBx.Name = "PythonExeTxBx"
        Me.PythonExeTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.PythonExeTxBx.Size = New System.Drawing.Size(267, 23)
        Me.PythonExeTxBx.TabIndex = 104
        '
        'RobotWkDirTxBx
        '
        Me.RobotWkDirTxBx.Location = New System.Drawing.Point(15, 213)
        Me.RobotWkDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.RobotWkDirTxBx.Name = "RobotWkDirTxBx"
        Me.RobotWkDirTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.RobotWkDirTxBx.Size = New System.Drawing.Size(267, 23)
        Me.RobotWkDirTxBx.TabIndex = 106
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(14, 192)
        Me.Label53.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(196, 17)
        Me.Label53.TabIndex = 108
        Me.Label53.Text = "Robot Working Directory Path"
        '
        'RobotWkDirBtn
        '
        Me.RobotWkDirBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RobotWkDirBtn.Location = New System.Drawing.Point(298, 209)
        Me.RobotWkDirBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RobotWkDirBtn.Name = "RobotWkDirBtn"
        Me.RobotWkDirBtn.Size = New System.Drawing.Size(64, 30)
        Me.RobotWkDirBtn.TabIndex = 107
        Me.RobotWkDirBtn.Text = "Browse"
        Me.RobotWkDirBtn.UseVisualStyleBackColor = False
        '
        'PythonEXEBtn
        '
        Me.PythonEXEBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.PythonEXEBtn.Location = New System.Drawing.Point(298, 148)
        Me.PythonEXEBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.PythonEXEBtn.Name = "PythonEXEBtn"
        Me.PythonEXEBtn.Size = New System.Drawing.Size(64, 30)
        Me.PythonEXEBtn.TabIndex = 105
        Me.PythonEXEBtn.Text = "Browse"
        Me.PythonEXEBtn.UseVisualStyleBackColor = False
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.Maroon
        Me.Label54.Location = New System.Drawing.Point(27, 176)
        Me.Label54.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(245, 15)
        Me.Label54.TabIndex = 111
        Me.Label54.Text = "Example: C:\Users\USERNAME\Anaconda3"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.Maroon
        Me.Label55.Location = New System.Drawing.Point(27, 238)
        Me.Label55.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(185, 15)
        Me.Label55.TabIndex = 112
        Me.Label55.Text = "Example: C:\OysterScoringRobot"
        '
        'SaveRobotBtn
        '
        Me.SaveRobotBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveRobotBtn.Location = New System.Drawing.Point(298, 270)
        Me.SaveRobotBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveRobotBtn.Name = "SaveRobotBtn"
        Me.SaveRobotBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveRobotBtn.TabIndex = 110
        Me.SaveRobotBtn.Text = "Save"
        Me.SaveRobotBtn.UseVisualStyleBackColor = False
        '
        'RobotSaveFileBtn
        '
        Me.RobotSaveFileBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RobotSaveFileBtn.Location = New System.Drawing.Point(668, 528)
        Me.RobotSaveFileBtn.Name = "RobotSaveFileBtn"
        Me.RobotSaveFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.RobotSaveFileBtn.TabIndex = 103
        Me.RobotSaveFileBtn.Text = "Save"
        Me.RobotSaveFileBtn.UseVisualStyleBackColor = False
        '
        'RichTextBox21
        '
        Me.RichTextBox21.ContextMenuStrip = Me.ContextMenuStrip10
        Me.RichTextBox21.Location = New System.Drawing.Point(387, 130)
        Me.RichTextBox21.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox21.Name = "RichTextBox21"
        Me.RichTextBox21.Size = New System.Drawing.Size(449, 386)
        Me.RichTextBox21.TabIndex = 102
        Me.RichTextBox21.Text = " "
        Me.RichTextBox21.WordWrap = False
        '
        'ContextMenuStrip10
        '
        Me.ContextMenuStrip10.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip10.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem18, Me.ToolStripMenuItem19})
        Me.ContextMenuStrip10.Name = "ContextMenuStrip6"
        Me.ContextMenuStrip10.Size = New System.Drawing.Size(189, 48)
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(188, 22)
        Me.ToolStripMenuItem18.Text = "Copy to Clipboard"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(188, 22)
        Me.ToolStripMenuItem19.Text = "Paste From Clipboard"
        '
        'RichTextBox6
        '
        Me.RichTextBox6.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox6.Location = New System.Drawing.Point(17, 24)
        Me.RichTextBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.RichTextBox6.Name = "RichTextBox6"
        Me.RichTextBox6.ReadOnly = True
        Me.RichTextBox6.Size = New System.Drawing.Size(502, 98)
        Me.RichTextBox6.TabIndex = 101
        Me.RichTextBox6.Text = resources.GetString("RichTextBox6.Text")
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.Maroon
        Me.Label51.Location = New System.Drawing.Point(671, 40)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(323, 31)
        Me.Label51.TabIndex = 100
        Me.Label51.Text = "Oyster Robot with Python"
        '
        'PictureBox10
        '
        Me.PictureBox10.ErrorImage = CType(resources.GetObject("PictureBox10.ErrorImage"), System.Drawing.Image)
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox10.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox10.TabIndex = 99
        Me.PictureBox10.TabStop = False
        '
        'RobotRTBx
        '
        Me.RobotRTBx.ContextMenuStrip = Me.ContextMenuStrip10
        Me.RobotRTBx.Location = New System.Drawing.Point(855, 130)
        Me.RobotRTBx.Margin = New System.Windows.Forms.Padding(2)
        Me.RobotRTBx.Name = "RobotRTBx"
        Me.RobotRTBx.Size = New System.Drawing.Size(449, 386)
        Me.RobotRTBx.TabIndex = 79
        Me.RobotRTBx.Text = "`"
        Me.RobotRTBx.WordWrap = False
        '
        'RunPythonBtn
        '
        Me.RunPythonBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RunPythonBtn.ForeColor = System.Drawing.Color.Maroon
        Me.RunPythonBtn.Location = New System.Drawing.Point(298, 331)
        Me.RunPythonBtn.Name = "RunPythonBtn"
        Me.RunPythonBtn.Size = New System.Drawing.Size(64, 30)
        Me.RunPythonBtn.TabIndex = 0
        Me.RunPythonBtn.Text = "Run"
        Me.RunPythonBtn.UseVisualStyleBackColor = False
        '
        'TestTab
        '
        Me.TestTab.BackColor = System.Drawing.Color.AliceBlue
        Me.TestTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TestTab.Controls.Add(Me.InstructionsTestingRichxBx)
        Me.TestTab.Controls.Add(Me.Panel1)
        Me.TestTab.Controls.Add(Me.OysterTestingWelcomeLbl)
        Me.TestTab.Controls.Add(Me.InstructionTestRchTxBx)
        Me.TestTab.Controls.Add(Me.PictureBox7)
        Me.TestTab.Location = New System.Drawing.Point(4, 28)
        Me.TestTab.Name = "TestTab"
        Me.TestTab.Size = New System.Drawing.Size(1319, 568)
        Me.TestTab.TabIndex = 11
        Me.TestTab.Text = "Test Oyster Helper"
        '
        'InstructionsTestingRichxBx
        '
        Me.InstructionsTestingRichxBx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionsTestingRichxBx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionsTestingRichxBx.Location = New System.Drawing.Point(31, 22)
        Me.InstructionsTestingRichxBx.Name = "InstructionsTestingRichxBx"
        Me.InstructionsTestingRichxBx.ReadOnly = True
        Me.InstructionsTestingRichxBx.Size = New System.Drawing.Size(539, 86)
        Me.InstructionsTestingRichxBx.TabIndex = 118
        Me.InstructionsTestingRichxBx.Text = resources.GetString("InstructionsTestingRichxBx.Text")
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel1.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Label57)
        Me.Panel1.Controls.Add(Me.JarTestBtn)
        Me.Panel1.Controls.Add(Me.Label42)
        Me.Panel1.Controls.Add(Me.DeleteValuesBtn)
        Me.Panel1.Controls.Add(Me.StartTests)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.CreateFileBtn)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.ReadTestFileBtn)
        Me.Panel1.Controls.Add(Me.HideTab)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.RestartBtn)
        Me.Panel1.Location = New System.Drawing.Point(31, 113)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1257, 111)
        Me.Panel1.TabIndex = 117
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.ForeColor = System.Drawing.Color.Maroon
        Me.Label57.Location = New System.Drawing.Point(912, 72)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(95, 17)
        Me.Label57.TabIndex = 121
        Me.Label57.Text = "Test Jar Build"
        '
        'JarTestBtn
        '
        Me.JarTestBtn.BackColor = System.Drawing.Color.Maroon
        Me.JarTestBtn.ForeColor = System.Drawing.Color.White
        Me.JarTestBtn.Location = New System.Drawing.Point(927, 27)
        Me.JarTestBtn.Name = "JarTestBtn"
        Me.JarTestBtn.Size = New System.Drawing.Size(64, 30)
        Me.JarTestBtn.TabIndex = 120
        Me.JarTestBtn.Text = "Test"
        Me.JarTestBtn.UseVisualStyleBackColor = False
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.ForeColor = System.Drawing.Color.Maroon
        Me.Label42.Location = New System.Drawing.Point(1079, 72)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(92, 17)
        Me.Label42.TabIndex = 119
        Me.Label42.Text = "Hide this Tab"
        '
        'DeleteValuesBtn
        '
        Me.DeleteValuesBtn.BackColor = System.Drawing.Color.Maroon
        Me.DeleteValuesBtn.ForeColor = System.Drawing.Color.White
        Me.DeleteValuesBtn.Location = New System.Drawing.Point(431, 27)
        Me.DeleteValuesBtn.Name = "DeleteValuesBtn"
        Me.DeleteValuesBtn.Size = New System.Drawing.Size(64, 30)
        Me.DeleteValuesBtn.TabIndex = 112
        Me.DeleteValuesBtn.Text = "Delete"
        Me.DeleteValuesBtn.UseVisualStyleBackColor = False
        '
        'StartTests
        '
        Me.StartTests.BackColor = System.Drawing.Color.Maroon
        Me.StartTests.ForeColor = System.Drawing.Color.White
        Me.StartTests.Location = New System.Drawing.Point(763, 27)
        Me.StartTests.Name = "StartTests"
        Me.StartTests.Size = New System.Drawing.Size(64, 30)
        Me.StartTests.TabIndex = 1
        Me.StartTests.Text = "Run"
        Me.StartTests.UseVisualStyleBackColor = False
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.ForeColor = System.Drawing.Color.Maroon
        Me.Label41.Location = New System.Drawing.Point(747, 72)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(97, 17)
        Me.Label41.TabIndex = 115
        Me.Label41.Text = "Run the Tests"
        '
        'CreateFileBtn
        '
        Me.CreateFileBtn.BackColor = System.Drawing.Color.Maroon
        Me.CreateFileBtn.ForeColor = System.Drawing.Color.White
        Me.CreateFileBtn.Location = New System.Drawing.Point(99, 27)
        Me.CreateFileBtn.Name = "CreateFileBtn"
        Me.CreateFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.CreateFileBtn.TabIndex = 105
        Me.CreateFileBtn.Text = "Make"
        Me.CreateFileBtn.UseVisualStyleBackColor = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.ForeColor = System.Drawing.Color.Maroon
        Me.Label40.Location = New System.Drawing.Point(566, 72)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(127, 17)
        Me.Label40.TabIndex = 114
        Me.Label40.Text = "Restart Application"
        '
        'ReadTestFileBtn
        '
        Me.ReadTestFileBtn.BackColor = System.Drawing.Color.Maroon
        Me.ReadTestFileBtn.ForeColor = System.Drawing.Color.White
        Me.ReadTestFileBtn.Location = New System.Drawing.Point(265, 27)
        Me.ReadTestFileBtn.Name = "ReadTestFileBtn"
        Me.ReadTestFileBtn.Size = New System.Drawing.Size(64, 30)
        Me.ReadTestFileBtn.TabIndex = 106
        Me.ReadTestFileBtn.Text = "View"
        Me.ReadTestFileBtn.UseVisualStyleBackColor = False
        '
        'HideTab
        '
        Me.HideTab.BackColor = System.Drawing.Color.Maroon
        Me.HideTab.ForeColor = System.Drawing.Color.White
        Me.HideTab.Location = New System.Drawing.Point(1093, 27)
        Me.HideTab.Name = "HideTab"
        Me.HideTab.Size = New System.Drawing.Size(64, 30)
        Me.HideTab.TabIndex = 103
        Me.HideTab.Text = "Hide"
        Me.HideTab.UseVisualStyleBackColor = False
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.ForeColor = System.Drawing.Color.Maroon
        Me.Label39.Location = New System.Drawing.Point(393, 72)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(140, 17)
        Me.Label39.TabIndex = 113
        Me.Label39.Text = "Delete Saved Values"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.Color.Maroon
        Me.Label37.Location = New System.Drawing.Point(68, 72)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(127, 17)
        Me.Label37.TabIndex = 109
        Me.Label37.Text = "Create Testing File"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ForeColor = System.Drawing.Color.Maroon
        Me.Label38.Location = New System.Drawing.Point(238, 72)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(119, 17)
        Me.Label38.TabIndex = 110
        Me.Label38.Text = "View the Test File"
        '
        'RestartBtn
        '
        Me.RestartBtn.BackColor = System.Drawing.Color.Maroon
        Me.RestartBtn.ForeColor = System.Drawing.Color.White
        Me.RestartBtn.Location = New System.Drawing.Point(597, 27)
        Me.RestartBtn.Name = "RestartBtn"
        Me.RestartBtn.Size = New System.Drawing.Size(64, 30)
        Me.RestartBtn.TabIndex = 111
        Me.RestartBtn.Text = "Restart"
        Me.RestartBtn.UseVisualStyleBackColor = False
        '
        'OysterTestingWelcomeLbl
        '
        Me.OysterTestingWelcomeLbl.AutoSize = True
        Me.OysterTestingWelcomeLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterTestingWelcomeLbl.ForeColor = System.Drawing.Color.Maroon
        Me.OysterTestingWelcomeLbl.Location = New System.Drawing.Point(671, 40)
        Me.OysterTestingWelcomeLbl.Name = "OysterTestingWelcomeLbl"
        Me.OysterTestingWelcomeLbl.Size = New System.Drawing.Size(443, 31)
        Me.OysterTestingWelcomeLbl.TabIndex = 116
        Me.OysterTestingWelcomeLbl.Text = "Testing The Oyster Helper Program"
        '
        'InstructionTestRchTxBx
        '
        Me.InstructionTestRchTxBx.BackColor = System.Drawing.Color.AliceBlue
        Me.InstructionTestRchTxBx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionTestRchTxBx.Location = New System.Drawing.Point(112, 236)
        Me.InstructionTestRchTxBx.Name = "InstructionTestRchTxBx"
        Me.InstructionTestRchTxBx.ReadOnly = True
        Me.InstructionTestRchTxBx.Size = New System.Drawing.Size(1106, 322)
        Me.InstructionTestRchTxBx.TabIndex = 108
        Me.InstructionTestRchTxBx.Text = resources.GetString("InstructionTestRchTxBx.Text")
        '
        'PictureBox7
        '
        Me.PictureBox7.ErrorImage = CType(resources.GetObject("PictureBox7.ErrorImage"), System.Drawing.Image)
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox7.TabIndex = 107
        Me.PictureBox7.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'Timer1
        '
        '
        'BackgroundWorker2
        '
        Me.BackgroundWorker2.WorkerReportsProgress = True
        Me.BackgroundWorker2.WorkerSupportsCancellation = True
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Process1
        '
        Me.Process1.StartInfo.Domain = ""
        Me.Process1.StartInfo.LoadUserProfile = False
        Me.Process1.StartInfo.Password = Nothing
        Me.Process1.StartInfo.StandardErrorEncoding = Nothing
        Me.Process1.StartInfo.StandardOutputEncoding = Nothing
        Me.Process1.StartInfo.UserName = ""
        Me.Process1.SynchronizingObject = Me
        '
        'Process2
        '
        Me.Process2.StartInfo.Domain = ""
        Me.Process2.StartInfo.LoadUserProfile = False
        Me.Process2.StartInfo.Password = Nothing
        Me.Process2.StartInfo.StandardErrorEncoding = Nothing
        Me.Process2.StartInfo.StandardOutputEncoding = Nothing
        Me.Process2.StartInfo.UserName = ""
        Me.Process2.SynchronizingObject = Me
        '
        'Process3
        '
        Me.Process3.StartInfo.Domain = ""
        Me.Process3.StartInfo.LoadUserProfile = False
        Me.Process3.StartInfo.Password = Nothing
        Me.Process3.StartInfo.StandardErrorEncoding = Nothing
        Me.Process3.StartInfo.StandardOutputEncoding = Nothing
        Me.Process3.StartInfo.UserName = ""
        Me.Process3.SynchronizingObject = Me
        '
        'Process4
        '
        Me.Process4.StartInfo.Domain = ""
        Me.Process4.StartInfo.LoadUserProfile = False
        Me.Process4.StartInfo.Password = Nothing
        Me.Process4.StartInfo.StandardErrorEncoding = Nothing
        Me.Process4.StartInfo.StandardOutputEncoding = Nothing
        Me.Process4.StartInfo.UserName = ""
        Me.Process4.SynchronizingObject = Me
        '
        'Oysterform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Maroon
        Me.ClientSize = New System.Drawing.Size(1327, 717)
        Me.Controls.Add(Me.OysterTabs)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.HomeBtn)
        Me.Controls.Add(Me.Exitbtn)
        Me.Controls.Add(Me.MenuStrip)
        Me.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Oysterform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Oyster Helper"
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.OysterTabs.ResumeLayout(False)
        Me.Welcome.ResumeLayout(False)
        Me.Welcome.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.ERMetricsSetup.ResumeLayout(False)
        Me.ERMetricsSetup.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip3.ResumeLayout(False)
        Me.ContextMenuStrip4.ResumeLayout(False)
        Me.Run.ResumeLayout(False)
        Me.Run.PerformLayout()
        Me.ContextMenuStrip6.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WorkWithOysterFiles.ResumeLayout(False)
        Me.WorkWithOysterFiles.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip8.ResumeLayout(False)
        Me.ContextMenuStrip5.ResumeLayout(False)
        Me.HelpfulStuff.ResumeLayout(False)
        Me.HelpfulStuff.PerformLayout()
        Me.AnalyzeGB.ResumeLayout(False)
        Me.AnalyzeGB.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.KnowledgeBaseMaint.ResumeLayout(False)
        Me.KnowledgeBaseMaint.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip7.ResumeLayout(False)
        Me.KnowledgeBaseView.ResumeLayout(False)
        Me.KnowledgeBaseView.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.AssertionHelper.ResumeLayout(False)
        Me.AssertionHelper.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.AssertDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Robot.ResumeLayout(False)
        Me.Robot.PerformLayout()
        Me.ContextMenuStrip9.ResumeLayout(False)
        Me.ContextMenuStrip10.ResumeLayout(False)
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TestTab.ResumeLayout(False)
        Me.TestTab.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Exitbtn As Button
    Friend WithEvents HomeBtn As Button
    Friend WithEvents MenuStrip As MenuStrip
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OysterTabs As TabControl
    Friend WithEvents Welcome As TabPage
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents Run As TabPage
    Friend WithEvents WorkWithOysterFiles As TabPage
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents OysterWorkDirTxBx As TextBox
    Friend WithEvents OysterRootDirTxBx As TextBox
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents OysterRootDirBtn As Button
    Friend WithEvents OysterWkDirBtn As Button
    Friend WithEvents OysterXmlListBx As ListBox
    Friend WithEvents Label8 As Label
    Friend WithEvents SaveAll As Button
    Friend WithEvents SaveFile_RichTextBox2 As Button
    Friend WithEvents EditOystRunBtn As Button
    Friend WithEvents StopRun As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OysterJarBtn As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents OysterJarTxBx As TextBox
    Friend WithEvents OysterJarLbl As Label
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents InstructionOysterRhTxbx As RichTextBox
    Friend WithEvents ERMetricsSetup As TabPage
    Friend WithEvents SaveERMetricsBtn As Button
    Friend WithEvents RunERMetricsBtn As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents ERMetricsWkDirBtn As Button
    Friend WithEvents ERMetricsTxBx As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents RichTextBox9 As RichTextBox
    Friend WithEvents TreeView3 As TreeView
    Friend WithEvents InstructionERMetricsRTxbx As RichTextBox
    Friend WithEvents EditFileBtn As Button
    Friend WithEvents SaveFileBtn As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents RunRichTextBox As RichTextBox
    Friend WithEvents ErrorInRunLbl As Label
    Friend WithEvents RunOyster As Button
    Friend WithEvents EditFileERMetricsBtn As Button
    Friend WithEvents SaveFileERMetricsBtn As Button
    Friend WithEvents RichTextBox3 As RichTextBox
    Friend WithEvents TreeView4 As TreeView
    Friend WithEvents EROysterWkDirTxBx As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents ERMetricJarBtn As Button
    Friend WithEvents ERMetricsJarTxBx As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OysterHelperToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label14 As Label
    Friend WithEvents MoveFileBtn As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents ToolStripProgressBar1 As ToolStripProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Label15 As Label
    Friend WithEvents ERMetLinkFileTxbx As TextBox
    Friend WithEvents ERMetricsRunRichTxtBx As RichTextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents CommentSelectedTextToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UnCommentSelectedTextToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WelLoadedFileLbl As Label
    Friend WithEvents HelpfulStuff As TabPage
    Friend WithEvents Label26 As Label
    Friend WithEvents HSCsvTxBx As TextBox
    Friend WithEvents HSLinkFileTxBx As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents HSOutCsvTxBx As TextBox
    Friend WithEvents InstructionHSRchtxbx As RichTextBox
    Friend WithEvents CreateCSVBtn As Button
    Friend WithEvents ERMetricsLoadedFile As Label
    Friend WithEvents WorkWithOysterFile As Label
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents Label29 As Label
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DeleteSelectedFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowFilePropertiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunScriptNamelbl As Label
    Friend WithEvents OysterRunLbl As Label
    Friend WithEvents Timelbl As Label
    Friend WithEvents LinkFilelbl As Label
    Friend WithEvents RunTimelbl As Label
    Friend WithEvents OysterRunLinkFilelbl As Label
    Friend WithEvents InputFileLbl As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents RunTimeEndlbl As Label
    Friend WithEvents CreateRefFilebtn As Button
    Friend WithEvents Label34 As Label
    Friend WithEvents HSRefOutputTxBx As TextBox
    Friend WithEvents InstructionRichTextBoxRef As RichTextBox
    Friend WithEvents RunTimeValuelbl As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label25 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents CopySelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PasteSelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CutSelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip3 As ContextMenuStrip
    Friend WithEvents CommentSelectedTextToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents UncommentSelectedTextToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip4 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip5 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem5 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As ToolStripMenuItem
    Friend WithEvents LFDateLbl As Label
    Friend WithEvents LinkFileDateERLbl As Label
    Friend WithEvents HSLinkDateLbl As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents ContextMenuStrip6 As ContextMenuStrip
    Friend WithEvents CopyToClipboardToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KnowledgeBaseView As TabPage
    Friend WithEvents SearchKBTxtBx As TextBox
    Friend WithEvents KBLoadFilelbl As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents KBSearchBtn As Button
    Friend WithEvents RichTextBox11 As RichTextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents IditySearchlbl As Label
    Friend WithEvents KBSearchTreeView As TreeView
    Friend WithEvents KnowledgeBaseMaint As TabPage
    Friend WithEvents KBMIdtyLbl As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents SearchIdtyBtn As Button
    Friend WithEvents KBMTextBox As TextBox
    Friend WithEvents ResetFilterBtn As Button
    Friend WithEvents ColumnCheckedLB As CheckedListBox
    Friend WithEvents PasteFromClipboardToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label36 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents ContextMenuStrip7 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem8 As ToolStripMenuItem
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents LoadCountLbl As Label
    Friend WithEvents KBMFilterCountLbl As Label
    Friend WithEvents SelectEntireRowToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KBFileLineCountLbl As Label
    Friend WithEvents KBMTreeView As TreeView
    Friend WithEvents KBMFileLoadedLbl As Label
    Friend WithEvents KBCheckBox As CheckBox
    Friend WithEvents KBMWholeWordChB As CheckBox
    Friend WithEvents TestTab As TabPage
    Friend WithEvents StartTests As Button
    Friend WithEvents HideTab As Button
    Friend WithEvents CreateFileBtn As Button
    Friend WithEvents ReadTestFileBtn As Button
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents ToolStripDropDownButton1 As ToolStripDropDownButton
    Friend WithEvents Label38 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents InstructionTestRchTxBx As RichTextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents DeleteValuesBtn As Button
    Friend WithEvents RestartBtn As Button
    Friend WithEvents WrkOysFilesLbl As Label
    Friend WithEvents OysterTestingWelcomeLbl As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents InstructionsTestingRichxBx As RichTextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents InstructionIKMRichTxBx As RichTextBox
    Friend WithEvents KBMIdityLbl As Label
    Friend WithEvents InstructionXMLRichTxBx As RichTextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents AssertionHelper As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RefToStrAssertion As RadioButton
    Friend WithEvents RefToRefAssertion As RadioButton
    Friend WithEvents StrToStrAssertion As RadioButton
    Friend WithEvents StrSplitAssertion As RadioButton
    Friend WithEvents SaveAssertFileBtn As Button
    Friend WithEvents AssertDataGridView As DataGridView
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents Label44 As Label
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents InstructionAssertRTB As RichTextBox
    Friend WithEvents Label46 As Label
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents OpenFileDialog2 As OpenFileDialog
    Friend WithEvents AssertLabel As Label
    Friend WithEvents RichTextBox5 As RichTextBox
    Friend WithEvents RichTextBox4 As RichTextBox
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents InstructAssertRTB As RichTextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents ContextMenuStrip8 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem9 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As ToolStripMenuItem
    Friend WithEvents Label48 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents CBStrapBtn As Button
    Friend WithEvents WgtFileTxBx As TextBox
    Friend WithEvents InstructionRichTextBox6 As RichTextBox
    Friend WithEvents Label49 As Label
    Friend WithEvents Analyze_Btn As Button
    Friend WithEvents AnalyzeGB As GroupBox
    Friend WithEvents AnotherFileRB As RadioButton
    Friend WithEvents LastInputFileRB As RadioButton
    Friend WithEvents Label50 As Label
    Friend WithEvents ToolStripMenuItem15 As ToolStripMenuItem
    Friend WithEvents Robot As TabPage
    Friend WithEvents RunPythonBtn As Button
    Friend WithEvents RobotRTBx As RichTextBox
    Friend WithEvents RobotSaveFileBtn As Button
    Friend WithEvents RichTextBox21 As RichTextBox
    Friend WithEvents RichTextBox6 As RichTextBox
    Friend WithEvents Label51 As Label
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents Label52 As Label
    Friend WithEvents PythonExeTxBx As TextBox
    Friend WithEvents RobotWkDirTxBx As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents RobotWkDirBtn As Button
    Friend WithEvents PythonEXEBtn As Button
    Friend WithEvents Label54 As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents SaveRobotBtn As Button
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents RobotTreeView As TreeView
    Friend WithEvents EditRobotFile As Button
    Friend WithEvents PythonFileLbl As Label
    Friend WithEvents ContextMenuStrip9 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem14 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip10 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem18 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem19 As ToolStripMenuItem
    Friend WithEvents Label56 As Label
    Friend WithEvents RenameFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents RenameFileToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RenameFileToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ShowFileDateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label57 As Label
    Friend WithEvents JarTestBtn As Button
    Friend WithEvents Process1 As Process
    Friend WithEvents ERM_Wk_DirBtn As Button
    Friend WithEvents Process2 As Process
    Friend WithEvents Process3 As Process
    Friend WithEvents Process4 As Process
End Class
