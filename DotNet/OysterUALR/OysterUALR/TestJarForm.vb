﻿Imports System.IO
Imports System.Text.RegularExpressions

Public Class TestJarForm
    Private Sub Exitbtn_Click(sender As Object, e As EventArgs) Handles Exitbtn.Click
        If TestHelper.NewJarTesting Then
            TestHelper.NewJarTesting = False
            TestHelper.isFinished = False
        End If
        Me.Close()

    End Sub

    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            TestHelper.NewJarTesting = True
            Me.RunJarTestBtn.Enabled = True
            Me.LinkFileChgBtn.Visible = False
            ErrorInRunLbl.Hide()
            Me.OysterRootDirTxBx.Text = My.Settings.OysterRootDir
            Me.OysterWorkDirTxBx.Text = My.Settings.OysterWorkingDir
            Me.OysterJarTxBx.Text = My.Settings.OysterJar
            Me.NewJarTxBx.Text = My.Settings.NewTestJar
            Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem

            'TestHelper.ChangeEngine(TJarBean.GetSetFSCluster, TJarBean.GetSetRSwoosh, pathFile)

            Dim AttrFileOldJar As String = TestHelper.GetAttributeFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)


            CurrentEngineLbl.Text = "Loaded Engine Now: " & GetEngine(pathFile)

            Dim linkFile As String = IO.Path.GetFileName(OysterHelper.GetLinkFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text))
            LinkfileLBL.Text = "RunScript  LinkFile: " & linkFile
            Dim fullPathandName As String = My.Settings.ERMetricsWorkDir & "\ermetrics.properties"


            Dim ERLinkFileName As String = Nothing
            ' Console.WriteLine(fullPathandName)
            Dim objReader As System.IO.StreamReader = Nothing
            objReader = New System.IO.StreamReader(fullPathandName)
            ' link.filename=ListABCScoringRuleLinkFile.link
            Do While objReader.Peek() <> -1
                Dim st As String = objReader.ReadLine()
                If st.Contains("link.filename=") And Not st.Contains("#") Then
                    ERLinkFileName = st.Substring(st.IndexOf("=") + 1)

                    Exit Do
                End If
            Loop
            LinkfileERMetricsLbl.Text = "ERMetrics LinkFile: " & ERLinkFileName

            If objReader IsNot Nothing Then
                objReader.Close()
            End If

            If Not ERLinkFileName.Equals(linkFile) Then
                ErrorInRunLbl.Show()
                Me.RunJarTestBtn.Enabled = False
                Me.LinkFileChgBtn.Visible = True
            End If

        Catch ex As Exception
            Console.WriteLine("OnLoad error is " & ex.Message)
        Finally

        End Try



    End Sub
    Private Sub Form1_Shown(sender As Object, e As EventArgs) _
     Handles Me.Shown

        MessageBox.Show("You are in the Form.Shown event.")

    End Sub
    Protected Sub FormActivated(e As EventArgs)
        Try
            If Me.OysterJarTxBx.Text.Equals(Me.NewJarTxBx.Text) Then
                MessageBox.Show("You cannot have Both Jars the Same!", "Old Jar", MessageBoxButtons.OK, MessageBoxIcon.Information)


            End If

        Catch ex As Exception

        Finally

        End Try



    End Sub
    Public Function GetEngine(ByVal pathAndFile As String) As String
        Dim Engine As String = Nothing
        Try

            Dim content = My.Computer.FileSystem.ReadAllText(pathAndFile)
            Dim m As Match = Regex.Match(content, "FSCLuster", RegexOptions.IgnoreCase)
            If m.Success Then
                Engine = "FsCluster"
            Else
                Engine = "RSWooshStandard"
            End If

        Catch ex As Exception
            Console.WriteLine("GetEngine error is " & ex.Message)
        End Try
        Return Engine
    End Function



    Private Sub LoadNewJarBtn_Click(sender As Object, e As EventArgs) Handles LoadNewJarBtn.Click
        Try
            If String.IsNullOrEmpty(OysterRootDirTxBx.Text) Or String.IsNullOrEmpty(OysterWorkDirTxBx.Text) Then
                MessageBox.Show(Constants.OysterRootWkDirRequired, Constants.DirectoryChoice, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else

                OpenFileDialog1.InitialDirectory = OysterRootDirTxBx.Text

                If OpenFileDialog1.ShowDialog = DialogResult.OK Then

                    NewJarTxBx.Text = OpenFileDialog1.FileName
                    ' OHelper.FillTextBox(Me.OysterJarTxBx, OpenFileDialog1.FileName)


                Else
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MsgBox("Loading New Jar Text Box error is " & ex.Message)
        End Try
    End Sub

    Private Sub RunJarTestBtn_Click(sender As Object, e As EventArgs) Handles RunJarTestBtn.Click
        Try
            'Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(Me.OysterJarTxBx.Text) Then
                MessageBox.Show("You must enter and Save an Old Jar to Test!", "Save New Jar to Test", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            ElseIf String.IsNullOrEmpty(My.Settings.NewTestJar) Or String.IsNullOrWhiteSpace(NewJarTxBx.Text) Then
                MessageBox.Show("You must enter and Save a New Jar to Test!", "Save New Jar to Test", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub

            ElseIf Me.OysterJarTxBx.Text.Equals(NewJarTxBx.Text) Then
                MessageBox.Show("Old and New Jar Cannot be the same!", "Jars Match Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            Else

                'reset important variables
                TestHelper.TestJarCounter = 1
                Dim dictionary As New Dictionary(Of Integer, RadioButton)
                TestHelper.isFinished = True
                TestHelper.NewJarTesting = True
                TestHelper.JarTestList.Clear()

                Dim tHelper As New TestHelper
                Dim JarBean As New JarTesterBean
                JarBean.Engine = TestHelper.GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
                If OldAndNewJarRB.Checked Then
                    JarBean.RunOldAndNewJar = True
                    dictionary.Add(1, OldAndNewJarRB)
                ElseIf OldJarRB.Checked Then
                    dictionary.Add(2, OldJarRB)
                    JarBean.RunOldJar = True
                ElseIf NewJarRB.Checked Then
                    dictionary.Add(3, NewJarRB)
                    JarBean.RunNewJar = True
                Else
                    MessageBox.Show("You must choose a Jar to Run", "Jar To Run Required", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If FSClusterRB.Checked Then
                    dictionary.Add(4, FSClusterRB)
                    JarBean.Engine = "FSCluster"
                    JarBean.RunFSCluster = True
                ElseIf RSwooshStandardRB.Checked Then
                    dictionary.Add(4, RSwooshStandardRB)
                    JarBean.Engine = "RSwooshStandard"
                    JarBean.RunRSwoosh = True
                ElseIf BothEngRB.Checked Then
                    dictionary.Add(5, BothEngRB)
                    JarBean.RunBothEngines = True
                Else
                    MessageBox.Show("You must choose a Jar(s) and Engine(s) to Run", "Jar and Engines Required", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
                Me.Hide()
                JarBean.JarDictionary = dictionary

                If JarBean.RunOldAndNewJar And JarBean.RunBothEngines Then
                    JarBean.RunCount = 5
                ElseIf JarBean.RunOldAndNewJar And JarBean.RunRSwoosh Then
                    JarBean.RunCount = 3
                ElseIf JarBean.RunOldAndNewJar And JarBean.RunFSCluster Then
                    JarBean.RunCount = 3
                ElseIf JarBean.RunOldJar And JarBean.RunBothEngines Then
                    JarBean.RunCount = 3
                ElseIf JarBean.RunNewJar And JarBean.RunBothEngines Then
                    JarBean.RunCount = 3
                ElseIf JarBean.RunOldJar And JarBean.RunRSwoosh Then
                    JarBean.RunCount = 2
                ElseIf JarBean.RunOldJar And JarBean.RunFSCluster Then
                    JarBean.RunCount = 2
                ElseIf JarBean.RunNewJar And JarBean.RunRSwoosh Then
                    JarBean.RunCount = 2
                ElseIf JarBean.RunNewJar And JarBean.RunFSCluster Then
                    JarBean.RunCount = 2

                End If

                Dim FSCluster As String = "<EREngine Type=""FSCluster"" />"
                Dim RSWoosh As String = "<EREngine Type=""RSwooshStandard"" />"
                JarBean.GetSetFSCluster = FSCluster
                JarBean.GetSetRSwoosh = RSWoosh
                JarBean.GetSetOldJar = Me.OysterJarTxBx.Text
                JarBean.GetSetNewJar = Me.NewJarTxBx.Text



                tHelper.StartThreadTests()
                TestHelper.ThirdTests(JarBean)
                Me.Close()
            End If



        Catch ex As Exception
            MessageBox.Show("Error in Starting Testing is " & ex.Message, "Testing Start Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TestHelper.Testing = False
            TestHelper.isFinished = False
        Finally

            '   TestHelper.NewJarTesting = False
        End Try
    End Sub


    Private Sub SaveJarBtn_Click(sender As Object, e As EventArgs) Handles SaveJarBtn.Click
        My.Settings.NewTestJar = NewJarTxBx.Text
        My.Settings.Save()
        My.Settings.Reload()

        If Me.OysterJarTxBx.Text.Equals(Me.NewJarTxBx.Text) Then
            MessageBox.Show("Old and New Jar Cannot be the same!", "Jars Match Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            MessageBox.Show("You are now Ready to Test Jars. Click Run to Start the Test.", "Testing Start", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub ChgJarBtn_Click(sender As Object, e As EventArgs) Handles ChgJarBtn.Click
        Me.Hide()
        Oysterform.BringToFront()
        Oysterform.OysterTabs.SelectTab(0)
        Oysterform.OysterJarBtn.PerformClick()

        Oysterform.SaveAll.PerformClick()
        Me.Show()
        Me.BringToFront()
        Me.RefreshBtn.PerformClick()

    End Sub

    Private Sub RefreshBtn_Click(sender As Object, e As EventArgs) Handles RefreshBtn.Click
        Me.OysterJarTxBx.Text = My.Settings.OysterJar

    End Sub

    Private Sub LinkFileChgBtn_Click(sender As Object, e As EventArgs) Handles LinkFileChgBtn.Click
        Try
            Me.Close()
            Oysterform.BringToFront()
            Oysterform.OysterTabs.SelectTab(1)
            Dim fullPathandName As String = My.Settings.ERMetricsWorkDir & "\ermetrics.properties"
            ' Dim myNodeCount As Integer = Oysterform.TreeView4.Nodes(0).GetNodeCount(True)
            Dim nodeCol As TreeNodeCollection = Oysterform.TreeView4.Nodes(0).Nodes

            For Each node As TreeNode In nodeCol
                If node.Text.Equals("ERMetrics.properties") Then
                    Oysterform.TreeView4.SelectedNode = node
                    OysterHelper.LoadFileFromTreeView(Oysterform.TreeView4, Oysterform.RichTextBox3)
                    Exit For
                End If

            Next

        Catch ex As Exception
            MessageBox.Show("Error in Link Change is " & ex.Message, "Link Change Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try


        'Oysterform.OysterJarBtn.PerformClick()

        'Oysterform.SaveAll.PerformClick()
        'Me.Show()
        'Me.BringToFront()
        'Me.RefreshBtn.PerformClick()
    End Sub

    'check for textchanged by space or new entry in  OysterWorkDirTxBx
    'Private Sub OysterJarTxBx_TextChanged(sender As Object, e As EventArgs) Handles OysterJarTxBx.TextChanged
    '    Dim helper As New OysterHelper
    '    Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
    '    If regSpace.IsMatch(OysterJarTxBx.Text) Then
    '        helper.TextChangedSpace(OysterJarTxBx)
    '    Else
    '        helper.TextChanged(OysterJarTxBx)
    '    End If

    'End Sub
    Private Sub NewJarTxBx_TextChanged(sender As Object, e As EventArgs) Handles NewJarTxBx.TextChanged
        Dim helper As New OysterHelper
        Dim regSpace As New System.Text.RegularExpressions.Regex(" ")
        If regSpace.IsMatch(NewJarTxBx.Text) Then
            helper.TextChangedSpace(NewJarTxBx)
        Else
            helper.TextChanged(NewJarTxBx)
        End If

    End Sub

    'Private Sub Button1_Click(sender As Object, e As EventArgs)
    '    Dim attrname As String = TestHelper.GetAttributeFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
    '    Dim newAttributeName As String = Oysterform.OysterRootDirTxBx.Text & "\" & attrname.Substring(1)

    '    Dim xelement As XElement = XElement.Load(newAttributeName)
    '    Dim rules As IEnumerable(Of XElement) = xelement.Descendants("Term")
    '    Dim sArray() As String = Nothing
    '    Dim it As Integer = 0
    '    For Each rule As XElement In rules
    '        '  Console.WriteLine(rule)
    '        Dim attList As IEnumerable(Of XAttribute) = rule.Attributes("SIMILARITY")

    '        For Each att In attList
    '            Dim st As String = att.ToString

    '            '  Console.WriteLine(st.Substring(st.IndexOf("""") + 1))

    '            ReDim Preserve sArray(it)

    '            st = st.Substring(st.IndexOf("""") + 1)
    '            st = st.Substring(0, st.Length - 1)

    '            If st.Contains("(") Then
    '                st = st.Substring(0, st.IndexOf("("))
    '            End If
    '            sArray(it) = st
    '            it += 1

    '        Next

    '    Next
    '    Console.WriteLine(sArray.Count)
    '    Dim I As Integer = 0
    '    Dim NewArray() As String = Nothing
    '    Dim quit As Boolean = False
    '    For Each s As String In sArray
    '        quit = False
    '        If I > 0 Then
    '            For Each str As String In NewArray
    '                If s = str Then
    '                    quit = True
    '                    Exit For
    '                End If
    '            Next
    '        Else

    '            ReDim Preserve NewArray(I)
    '            NewArray(I) = s
    '            I += 1
    '            quit = True
    '        End If

    '        If Not quit Then
    '            ReDim Preserve NewArray(I)
    '            NewArray(I) = s
    '            I += 1
    '        End If
    '        Console.WriteLine(s)
    '    Next
    '    Console.WriteLine(NewArray.Length)

    '    For Each strr As String In NewArray
    '        Console.WriteLine(strr)
    '    Next

    'End Sub

    'Private Sub Button2_Click(sender As Object, e As EventArgs)
    '    Dim attrname As String = TestHelper.GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
    '    '   Dim newAttributeName As String = Oysterform.OysterRootDirTxBx.Text & "\" & attrname.Substring(1)
    '    Console.WriteLine(attrname)
    'End Sub


    'Public Sub AddElementToStringArray(ByVal stringToAdd As String)
    '    ReDim Preserve myStrings(myStringElements)
    '    myStrings(myStringElements) = stringToAdd
    '    myStringElements += 1
    'End Sub
End Class