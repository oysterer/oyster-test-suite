﻿Public Class TestSignInForm

    Public Shared SignIn As Boolean = False


    Private Sub OK_Click(sender As Object, e As EventArgs) Handles OK.Click

        Me.DialogResult = DialogResult.OK
        Me.Hide()


    End Sub

    ''  Detect all numeric characters at the TextBox level And consume  
    ''2, 5, And 8.
    Sub PasswordTextBox_KeyPress(ByVal sender As Object,
          ByVal e As KeyPressEventArgs) Handles PasswordTextBox.KeyPress

        If e.KeyChar >= ChrW(48) And e.KeyChar <= ChrW(122) Then

            Select Case e.KeyChar
                Case ChrW(50), ChrW(53), ChrW(56), ChrW(79), ChrW(121), ChrW(115)
                    PasswordTextBox.Text = PasswordTextBox.Text & e.KeyChar.ToString
                    PasswordTextBox.Select(PasswordTextBox.Text.Length + 1, 1)
                    If PasswordTextBox.Text.Equals("Oys" & 258.ToString) Then
                        SignIn = True
                    Else
                        SignIn = False
                    End If
                    e.Handled = True
            End Select
        End If
    End Sub
    Public Function SignInReturn() As Boolean
        Return SignIn
    End Function


    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        SignIn = False
        Me.DialogResult = DialogResult.Cancel
        Me.Hide()

    End Sub


End Class

