﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OysterHelpForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OysterHelpForm))
        Me.OusterHelpTabCtrl = New System.Windows.Forms.TabControl()
        Me.OysterInstall = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.HelpOysterInstallRhTxBx = New System.Windows.Forms.RichTextBox()
        Me.OysterFiles = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.OHelperHints = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.OysterRunHelp = New System.Windows.Forms.TabPage()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CloseHelperBtn = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.OusterHelpTabCtrl.SuspendLayout()
        Me.OysterInstall.SuspendLayout()
        Me.OysterFiles.SuspendLayout()
        Me.OHelperHints.SuspendLayout()
        Me.OysterRunHelp.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OusterHelpTabCtrl
        '
        Me.OusterHelpTabCtrl.Controls.Add(Me.OysterInstall)
        Me.OusterHelpTabCtrl.Controls.Add(Me.OysterFiles)
        Me.OusterHelpTabCtrl.Controls.Add(Me.OHelperHints)
        Me.OusterHelpTabCtrl.Controls.Add(Me.OysterRunHelp)
        Me.OusterHelpTabCtrl.Dock = System.Windows.Forms.DockStyle.Top
        Me.OusterHelpTabCtrl.Location = New System.Drawing.Point(0, 0)
        Me.OusterHelpTabCtrl.Margin = New System.Windows.Forms.Padding(2)
        Me.OusterHelpTabCtrl.Name = "OusterHelpTabCtrl"
        Me.OusterHelpTabCtrl.SelectedIndex = 0
        Me.OusterHelpTabCtrl.Size = New System.Drawing.Size(761, 455)
        Me.OusterHelpTabCtrl.TabIndex = 0
        '
        'OysterInstall
        '
        Me.OysterInstall.BackColor = System.Drawing.Color.AliceBlue
        Me.OysterInstall.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OysterInstall.Controls.Add(Me.PictureBox1)
        Me.OysterInstall.Controls.Add(Me.Label1)
        Me.OysterInstall.Controls.Add(Me.HelpOysterInstallRhTxBx)
        Me.OysterInstall.ForeColor = System.Drawing.Color.AliceBlue
        Me.OysterInstall.Location = New System.Drawing.Point(4, 22)
        Me.OysterInstall.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterInstall.Name = "OysterInstall"
        Me.OysterInstall.Padding = New System.Windows.Forms.Padding(2)
        Me.OysterInstall.Size = New System.Drawing.Size(753, 429)
        Me.OysterInstall.TabIndex = 0
        Me.OysterInstall.Text = "Installation Help"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(20, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(158, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Installation Help"
        '
        'HelpOysterInstallRhTxBx
        '
        Me.HelpOysterInstallRhTxBx.BackColor = System.Drawing.Color.AliceBlue
        Me.HelpOysterInstallRhTxBx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.HelpOysterInstallRhTxBx.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HelpOysterInstallRhTxBx.Location = New System.Drawing.Point(25, 115)
        Me.HelpOysterInstallRhTxBx.Name = "HelpOysterInstallRhTxBx"
        Me.HelpOysterInstallRhTxBx.ReadOnly = True
        Me.HelpOysterInstallRhTxBx.Size = New System.Drawing.Size(689, 310)
        Me.HelpOysterInstallRhTxBx.TabIndex = 0
        Me.HelpOysterInstallRhTxBx.Text = resources.GetString("HelpOysterInstallRhTxBx.Text")
        '
        'OysterFiles
        '
        Me.OysterFiles.BackColor = System.Drawing.Color.AliceBlue
        Me.OysterFiles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OysterFiles.Controls.Add(Me.PictureBox2)
        Me.OysterFiles.Controls.Add(Me.Label2)
        Me.OysterFiles.Controls.Add(Me.RichTextBox1)
        Me.OysterFiles.Location = New System.Drawing.Point(4, 22)
        Me.OysterFiles.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterFiles.Name = "OysterFiles"
        Me.OysterFiles.Padding = New System.Windows.Forms.Padding(2)
        Me.OysterFiles.Size = New System.Drawing.Size(753, 429)
        Me.OysterFiles.TabIndex = 1
        Me.OysterFiles.Text = "Setup Entity Resolution"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(20, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(533, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Oyster and ERMetrics Files needed for Entity Resolution"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(25, 115)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(726, 279)
        Me.RichTextBox1.TabIndex = 1
        Me.RichTextBox1.Text = resources.GetString("RichTextBox1.Text")
        '
        'OHelperHints
        '
        Me.OHelperHints.BackColor = System.Drawing.Color.AliceBlue
        Me.OHelperHints.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OHelperHints.Controls.Add(Me.PictureBox6)
        Me.OHelperHints.Controls.Add(Me.Label3)
        Me.OHelperHints.Controls.Add(Me.RichTextBox2)
        Me.OHelperHints.ForeColor = System.Drawing.Color.AliceBlue
        Me.OHelperHints.Location = New System.Drawing.Point(4, 22)
        Me.OHelperHints.Name = "OHelperHints"
        Me.OHelperHints.Size = New System.Drawing.Size(753, 429)
        Me.OHelperHints.TabIndex = 2
        Me.OHelperHints.Text = "Oyster Helper Tabs"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(20, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(191, 24)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Oyster Helper Tabs"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.Location = New System.Drawing.Point(25, 115)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.Size = New System.Drawing.Size(726, 279)
        Me.RichTextBox2.TabIndex = 2
        Me.RichTextBox2.Text = resources.GetString("RichTextBox2.Text")
        '
        'OysterRunHelp
        '
        Me.OysterRunHelp.BackColor = System.Drawing.Color.AliceBlue
        Me.OysterRunHelp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.OysterRunHelp.Controls.Add(Me.RichTextBox3)
        Me.OysterRunHelp.Controls.Add(Me.Label4)
        Me.OysterRunHelp.Controls.Add(Me.PictureBox3)
        Me.OysterRunHelp.Location = New System.Drawing.Point(4, 22)
        Me.OysterRunHelp.Name = "OysterRunHelp"
        Me.OysterRunHelp.Size = New System.Drawing.Size(753, 429)
        Me.OysterRunHelp.TabIndex = 3
        Me.OysterRunHelp.Text = "Oyster Run Hints"
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.Color.AliceBlue
        Me.RichTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox3.Location = New System.Drawing.Point(25, 115)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.ReadOnly = True
        Me.RichTextBox3.Size = New System.Drawing.Size(726, 279)
        Me.RichTextBox3.TabIndex = 93
        Me.RichTextBox3.Text = resources.GetString("RichTextBox3.Text")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Maroon
        Me.Label4.Location = New System.Drawing.Point(20, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(167, 24)
        Me.Label4.TabIndex = 91
        Me.Label4.Text = "Oyster Run Hints"
        '
        'CloseHelperBtn
        '
        Me.CloseHelperBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CloseHelperBtn.Location = New System.Drawing.Point(684, 471)
        Me.CloseHelperBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.CloseHelperBtn.Name = "CloseHelperBtn"
        Me.CloseHelperBtn.Size = New System.Drawing.Size(56, 24)
        Me.CloseHelperBtn.TabIndex = 0
        Me.CloseHelperBtn.Text = "Close"
        Me.CloseHelperBtn.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = CType(resources.GetObject("PictureBox1.ErrorImage"), System.Drawing.Image)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(620, 20)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(73, 78)
        Me.PictureBox1.TabIndex = 90
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.ErrorImage = CType(resources.GetObject("PictureBox2.ErrorImage"), System.Drawing.Image)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(620, 20)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(73, 78)
        Me.PictureBox2.TabIndex = 90
        Me.PictureBox2.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.ErrorImage = CType(resources.GetObject("PictureBox6.ErrorImage"), System.Drawing.Image)
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(620, 20)
        Me.PictureBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(73, 78)
        Me.PictureBox6.TabIndex = 89
        Me.PictureBox6.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.ErrorImage = CType(resources.GetObject("PictureBox3.ErrorImage"), System.Drawing.Image)
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(620, 20)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(73, 78)
        Me.PictureBox3.TabIndex = 92
        Me.PictureBox3.TabStop = False
        '
        'OysterHelpForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Maroon
        Me.ClientSize = New System.Drawing.Size(761, 506)
        Me.Controls.Add(Me.CloseHelperBtn)
        Me.Controls.Add(Me.OusterHelpTabCtrl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "OysterHelpForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Oyster Helper Help"
        Me.OusterHelpTabCtrl.ResumeLayout(False)
        Me.OysterInstall.ResumeLayout(False)
        Me.OysterInstall.PerformLayout()
        Me.OysterFiles.ResumeLayout(False)
        Me.OysterFiles.PerformLayout()
        Me.OHelperHints.ResumeLayout(False)
        Me.OHelperHints.PerformLayout()
        Me.OysterRunHelp.ResumeLayout(False)
        Me.OysterRunHelp.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents OusterHelpTabCtrl As TabControl
    Friend WithEvents OysterInstall As TabPage
    Friend WithEvents OysterFiles As TabPage
    Friend WithEvents CloseHelperBtn As Button
    Friend WithEvents HelpOysterInstallRhTxBx As RichTextBox
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents OHelperHints As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents OysterRunHelp As TabPage
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label4 As Label
    Friend WithEvents RichTextBox3 As RichTextBox
End Class
