﻿
'Constants Class

'Static String Class in vb.net.  No Class instance needed to access the String values.
'Constants.CDrive is a good example and all you need to reference any defined member. 
'Great For use in a MsgBox and any String that should be changed only in one place but 
'possibly used in several places. The values should and could change and many more Strings 
'added to help define the program.

'Very OK to change and modify as needed.



Public Class Constants


    Protected Friend Const IdentityUpdate = "IdentityUpdate"
    Protected Friend Const IdentityRelolution = "IdentityResolution"
    Protected Friend Const IdentityCapture = "IdentityCapture"
    Protected Friend Const RefToRefAssertion = "RefToRefAssertion"
    Protected Friend Const StrSplitAssertion = "StrSplitAssertion"
    Protected Friend Const StrToStrAssertion = "StrToStrAssertion"
    Protected Friend Const RefToStrAssertion = "RefToStrAssertion"



    'Used for saving richtextbox loaded file information in a Hashtable
    'for saving the file

    Protected Friend Const RichTextBox21String = "RichTextBox21"
    Protected Friend Const RichTextBox21 = 21
    Protected Friend Const RichTextBox11 = 11
    Protected Friend Const RichTextBox11String = "RichTextBox11"
    Protected Friend Const RichTextBox9 = 9
    Protected Friend Const RichTextBox9String = "RichTextBox9"
    Protected Friend Const RichTextBox2 = 2
    Protected Friend Const RichTextBox2String = "RichTextBox2"
    Protected Friend Const RichTextBox3 = 3
    Protected Friend Const RichTextBox3String = "RichTextBox3"
    Protected Friend Const DataGridView1 = 1
    'Drives
    Protected Friend Const CDrive = "C:\"

    ' Directories to leave out of working with oysterfiles
    Protected Friend Const LOG = "LOG"
    Protected Friend Const LLIB = "LIB"
    Protected Friend Const COMMONSJAR = "COMMONS.IO"
    Protected Friend Const DATA = "DATA"
    Protected Friend Const COPY = "COPY"

    'Directories
    Protected Friend Const INPUT = "INPUT"
    Protected Friend Const OUTPUT = "OUTPUT"
    Protected Friend Const SCRIPTS = "SCRIPTS"
    Protected Friend Const RUNSCRIPTS = "RUNSCRIPT"

    Protected Friend Const OysterRootDirTxtBx = "OysterRootDirTxtBx"

    'Matching
    Protected Friend Const OYSTER As String = "OYSTER"
    Protected Friend Const ATTRIBUTES = "ATTRIBUTES"
    Protected Friend Const Root As String = "Root"
    Protected Friend Const ERMetrics As String = "ERMetrics"
    Protected Friend Const WorkDirectory As String = "WorkDirectory"
    Protected Friend Const EROysterWorkDirectory As String = "EROysterWorkDirectory"

    Protected Friend Const SOURCEDESCRIPTOR = "SOURCEDESCRIPTOR"
    Protected Friend Const METRICS = "METRICS"
    Protected Friend Const Python = "PYTHON"
    Protected Friend Const Robot = "ROBOT"

    'Messages only
    Protected Friend Const RobotTxBxMustBeFilled = "Python Directory and Robot Working Directory must not be Empty!"
    Protected Friend Const SettingsSaved = "Settings are now Saved for" & vbCrLf & " the next time you start the program."
    Protected Friend Const UseBrowseButton = "Please use the Browse Button " & vbCrLf & "to fill the TextBox!"
    Protected Friend Const FileIsEmpty = "This file is Empty at this time!"
    Protected Friend Const ERTextBoxesMustBeFilled = "ERMetrics Working Directory, " & vbCrLf & "ERMetrics Jar and Oyster Working " & vbCrLf & "Directory must not be Empty!"
    Protected Friend Const EmptyRichTextBox = "The RichTextBox is empty!"
    Protected Friend Const EmptyERMetricsDirectory = "ERMetrics Working Directory " & vbCrLf & "Must Not Be Empty!"
    Protected Friend Const LinkFileEmpty = "The Link File is Empty.  Run Oyster again and let it finish!"
    Protected Friend Const CheckERPropertiesFile = "Link File Copied to ERMetrics " & vbCrLf & "working directory.  Check your " & vbCrLf & "properties file if needed."
    Protected Friend Const EmptyLinkFileTextBox = "LinkFile TextBox Cannot be Empty!"
    Protected Friend Const RunScriptNotFound = "OysterHelper Could Not " & vbCrLf & "Find the Chosen RunScript xml file!"
    Protected Friend Const DirectoryDoesNotExist = "Directory Chosen does Not Exist!"
    Protected Friend Const NotTheCorrectDirectory = "Not The Correct Directory"
    Protected Friend Const OysterRootWkDirJarAlsoRequired = "Oyster Root Directory, Oyster Working Directory " & vbCrLf & "and Oyster Jar must not be Empty!"
    Protected Friend Const OysterRootWkDirRequired = "Oyster Root Directory and Oyster " & vbCrLf & "Working Directory must not be Empty!"
    Protected Friend Const OysterRootWkDirJarRequired = "Oyster Root Directory and Oyster " & vbCrLf & "Working Directory and Oyster Jar " & vbCrLf & "must not be Empty and Saved Again to Run!"
    Protected Friend Const AddRichTextBoxName = "RichTextBox Name needs to be " & vbCrLf & "added in the Constants file!"
    Protected Friend Const ErrorCopyingFile = "Exception Copying File is "
    Protected Friend Const MakeFileReadOnlyError = "Could not make your file read only!"
    Protected Friend Const SourceDescriptorFileNotFound = "SourceDescriptor File Not Found"
    Protected Friend Const RichTxtBxEmpty = "RichTextBox Empty"
    Protected Friend Const SelectAFile = "Select a File"
    Protected Friend Const RunOysterFirst = "You must Run Oyster first before using this Utility!"
    Protected Friend Const FileCreationComplete = "File Creation Complete!"


    'Label messages
    Protected Friend Const FileLoadedIs = "File Loaded is: "
    Protected Friend Const OHelper = "Oyster Helper"
    Protected Friend Const DirectoryChoice = "Directory Choice"


    'TabNames
    Protected Friend Const Welcome As String = "Welcome"
    Protected Friend Const WorkWithOysterFiles = "WorkWithOysterFiles"
    Protected Friend Const ERMetricsSetup = "ERMetricsSetup"
    Protected Friend Const RobotTab = "Robot"
    Protected Friend Const HelpfulStuff = "HelpfulStuff"
    Protected Friend Const KnowledgeBaseView = "KnowledgeBaseView"
    Protected Friend Const KnowledgeBaseMaint = "KnowledgeBaseMaint"

    'Files / directoies
    Protected Friend Const JAR = "JAR"
    Protected Friend Const ERMETRICSJAR As String = "ER-METRICS"
    Protected Friend Const OysterJar As String = "OysterJar"
    Protected Friend Const Directory = "Directory"
    Protected Friend Const File = "File"

    'Process class calls Oyster in a command window
    Protected Friend Const CmdWindow As String = "C:\windows\system32\cmd.exe"


    'Errors and Requirement Messages for Msgboxes
    Protected Friend Const SaveMessage As String = "Oyster Root Directory, Working Directory " & vbCrLf & "and Oyster Jar File must all" & vbCrLf & " be entered to Save!"
    Protected Friend Const CorrectWorkingDirectory = "Oyster Working Directory Must Be Correct! "
    Protected Friend Const OysterWorkDirError = "Your Oyster Working Directory is " & vbCrLf & "Wrong for this Run Script!"
    Protected Friend Const RunScriptChoiceError = "Select a Runscript please!"
    Protected Friend Const ERMetricsDirChoiceError = "ERMetrics Directory and ERMetrics " & vbCrLf & " Jar must be entered!"

    Protected Friend Const ERMetricsSaveFile = "ERMetrics Directory and ERMetrics " & vbCrLf & " Jar must be Saved before Running ERMetrics!"
    Protected Friend Const OysterFlavorRequired = "Your choice must be a flavor of " & vbCrLf & "Oyster.jar!  i.e. Oyster-3.6.4.jar"
    Protected Friend Const OysterJarNotInDir = "Oyster.jar Not Found in the " & vbCrLf & "Chosen Directory!"
    Protected Friend Const ERMetricsJarNotFound = "Your chosen ERMetrics path " & vbCrLf & "must contain ER-Metrics.jar to Run!"
    Protected Friend Const RootMessage = "This must be the Oyster Root Directory Only!"
    Protected Friend Const ErrorMsg = "Error working with Root Directory!"
    Protected Friend Const MustHaveScripts = "Your chosen Working Directory " & vbCrLf & " must contain Scripts to Run!"
    Protected Friend Const WorkDirUnderRoot = "You must Choose a Work Directory under the Oyster Root!"
    Protected Friend Const SelectFile = "You must select a File from the listed Files!"
    Protected Friend Const OysterFlavor = "Your chosen Root Directory must" & vbCrLf & "contain a flavor of Oyster.jar!  i.e. Oyster-3.6.4.jar"
    Protected Friend Const CheckPathError = "Error Thrown in CheckPath Function is "
    Protected Friend Const OysterEmpty = "The Oyster Root Directory Must " & vbCrLf & "Not Be Empty!"
    Protected Friend Const ERWorkingDirectory = "ErMetrics Working Directory " & vbCrLf & "must be Entered and Correct!"
    Protected Friend Const ErrorOysterJar = "Error Thrown while working " & vbCrLf & "on Oyster Jar!"
    Protected Friend Const BrowseButton = "Use the Browse Button"
    Protected Friend Const StopJava = "This will stop all Java Processes on this box!"

    Protected Friend Const PythonEXEDir = "Python Working Directory " & vbCrLf & "must be Entered and Correct!"
    Protected Friend Const RobotWkDir = "Robot Working Directory " & vbCrLf & "must be Entered and Correct!"







End Class
