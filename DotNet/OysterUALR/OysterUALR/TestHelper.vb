﻿Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Xml

''' <summary>
'''  Main Testing Class
''' </summary>


Public Class TestHelper

    'these next three shared 
    Public Shared j As Integer = 0
    Public Shared isFinished As Boolean = True

    'don't change this next value used to 
    'make decisions from oyster and ermetrics about
    'are we testing
    Public Shared Testing As Boolean = False
    Public Shared NewJarTesting As Boolean = False
    'Public Shared JarTestRuns As Integer = 2

    'testing dictionaries to hold testing results
    Private Shared WindowTestList As New Dictionary(Of Integer, String)
    Private Shared TestList As New Dictionary(Of Integer, String)
    Private Shared ResultList As New Dictionary(Of Integer, Boolean)

    Public Shared JarTestList As New Dictionary(Of Integer, String())

    'used to hold saved correct values from Oyster Helper for testing purposes`
    Public Shared DList As New Dictionary(Of String, String)

    '-----------------------------------------------------------------------------
    'used to close popup message windows
    ReadOnly hWnd As Integer
    Public Const SC_CLOSE As Long = &HF060 ' Close window
    Public Const WM_SYSCOMMAND = &H112
    Friend Const BM_CLICK As Integer = &HF5

    '--------------------------------------------------------------------
    'The following is used to start test window closing routine in a new thread
    'so that it can use thread.sleep and not affect the the thread
    'running tests or doing the operations in Oyster Helper

    Dim t As New Thread(AddressOf RunWindowClose)
    Public Sub StartThreadTests()
        t.Start()
    End Sub



    'The following function is used to read in the values of saved correct values from Oyster Helper for testing purposes
    'A file is created to hold the textbox names with the correct values so it can be used to fill in the correct values 
    'during testing
    Public Function ReadInSavedValues() As Dictionary(Of String, String)
        Try
            Dim username As String = Environment.UserName
            If String.IsNullOrEmpty(username) Or String.IsNullOrWhiteSpace(username) Then
                username = SystemInformation.UserName
            End If
            '  Dim DList As New Dictionary(Of String, String)
            If FileIO.FileSystem.FileExists("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv") And
                FileIO.FileSystem.GetFileInfo("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv").Length > 0 Then

                Using MyReader As New FileIO.TextFieldParser("C:\Users\" & username & "\AppData\Local\OysterHelperTest\SaveTestSettings.csv")

                    MyReader.TextFieldType = FileIO.FieldType.Delimited
                    MyReader.SetDelimiters(",")
                    Dim currentRow As String()

                    While Not MyReader.EndOfData
                        Try
                            currentRow = MyReader.ReadFields()
                            DList.Add(currentRow.GetValue(0), currentRow.GetValue(1))

                        Catch ex As Exception

                        End Try

                    End While
                End Using
            Else
                Testing = False
                isFinished = False
                DList = Nothing
                MessageBox.Show("Saved File for Testing is Empty or Does not Exist!", "Test File Empty", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If


        Catch ex As Exception
            Testing = False
            isFinished = False
            DList = Nothing
            MsgBox("Read In Testing File Error is " & ex.Message)
        End Try
        Return DList
    End Function

    '---------------------------------------------------------------------------------------------------
    'the testing works like this.  There are three shared dictionaries of integer, string type
    'that are used.  One TestList is used to save what we know is correct test output and then one the other 
    'is to save what output happens when the tests run. We are keeping track by the Caption
    'of each messagebox that comes up when the tests are run.  The runwindowclose method uses the 
    'WindowTestList dictionary to store the captions of the messagebox's as it closes them on a
    'test.  The two are compared in the compareresults method and it loads a third dictionary 
    'ResultsList with the key (all three use the numeric order of the tests for the key) and 
    'a Pass or Fail value in the value to produce the report. We always assume the test will work 
    'but give it a path to not work if the criteria are not the same or met.  Believe me, if it is not 
    'what is expected, it will fail.  So when code is changed, we have a way to test to make sure any 
    'change does not effect the outcome of the tests. The process of creating and running the tests for 
    'several holes in the code that needed to be fixed to meet the criteria for the tests. Works great 
    'with a printable report on completion.

    '-----------------------------------------------------------------------------------------------
    'these are the tests
    Public Sub StartTests()


        Dim oysterHelper As New OysterHelper

        ' runWindowClose()

        Try
            DList = ReadInSavedValues()

            If DList Is Nothing Or DList.Count = 0 Then
                Exit Sub
            End If
            'OysterRootDirTxBx Tests

            Oysterform.OysterTabs.SelectTab(0)

            'try to put a letter in the textbox
            Oysterform.OysterRootDirTxBx.Text = "c"
            'add the position in the test and the Caption of the MessageBox in the test
            TestList.Add(j, "Use the Browse Button")
            j += 1

            'try to put a space in the textbox
            Oysterform.OysterRootDirTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1

            'try to put the wrong directory in the textbox
            oysterHelper.FillTextBox(Oysterform.OysterRootDirTxBx, "c:\")
            TestList.Add(j, "Directory Choice")
            j += 1

            'finally fill the textbox with the correct directory from the testfile created before testing
            oysterHelper.FillTextBox(Oysterform.OysterRootDirTxBx, DList.Item("OysterRootDirTxBx"))

            'end oysterrootdirtxbx------------------------------------------------------------------------------------- 3


            ''OysterWorkDirTxBx

            Oysterform.OysterWorkDirTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1

            Oysterform.OysterWorkDirTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1

            oysterHelper.FillTextBox(Oysterform.OysterWorkDirTxBx, "c:\")
            TestList.Add(j, "Directory Choice")
            j += 1

            oysterHelper.FillTextBox(Oysterform.OysterWorkDirTxBx, DList.Item("OysterWorkDirTxBx"))

            'end oysterworkdirtxbx-------------------------------------------------------------------------------------- 3

            '--try to save without all txboxs loaded------------------------------------------------------------

            'One note.  If you are running tests again which the last run
            'has completed and loaded the text boxes already then this will work
            'so seeing if this works to keep it from failing.  It will not save 
            'with empty boxes but will save with already populated boxes. Both 
            'are correct

            If String.IsNullOrEmpty(Oysterform.OysterJarTxBx.Text) Then
                Oysterform.SaveAll.PerformClick()
                TestList.Add(j, "Requirement")
                j += 1
            Else
                Oysterform.SaveAll.PerformClick()
                TestList.Add(j, "Choices Saved")
                j += 1
            End If


            '--end try to save ---------------------------------------------------------------------------------------- 1

            ''OysterJarTxBx

            Oysterform.OysterJarTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1
            Oysterform.OysterJarTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1
            ''Oysterform.OysterJarTxBx.Text = Space(1)
            oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, "c:\er-metrics.jar")
            TestList.Add(j, "Jar Choice")
            j += 1

            'input correct jar from test script - will tell us to save all choices
            oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, DList.Item("OysterJarTxBx"))
            TestList.Add(j, "Jar Choice")
            j += 1

            '--end oysterjar -------------------------------------------------------------------------------------------- 4

            '--save the three oyster textboxes
            Oysterform.SaveAll.PerformClick()
            TestList.Add(j, "Choices Saved")
            j += 1
            '--end save--------------------------------------------------------------------------------------------------- 1

            '12

            'save for checking Treeview for this run script is loaded
            Dim runscript As String = Nothing

            '--select a run script that does not match the run script for the working directory chosen
            ' --and then run oyster
            'try to run with wrong run file for directory
            If Oysterform.OysterXmlListBx.Items.Count > 0 Then
                ''select run script from oyster working directory
                For Each aStr As String In Oysterform.OysterXmlListBx.Items
                    Try   ' make sure your working directory does not get chosen for this negative test
                        If IO.Path.GetFileName(Oysterform.OysterWorkDirTxBx.Text).Equals(aStr.Substring(0, aStr.ToUpper.IndexOf("RUN"))) Then
                            runscript = aStr
                            Dim index As Integer = Oysterform.OysterXmlListBx.Items.IndexOf(aStr)
                            If index <> 0 Then
                                Oysterform.OysterXmlListBx.SelectedIndex = 0
                                Oysterform.RunOyster.PerformClick()
                            Else
                                Oysterform.OysterXmlListBx.SelectedIndex = index + 1
                                Oysterform.RunOyster.PerformClick()
                            End If
                            Exit For
                        End If
                    Catch ex As Exception

                    End Try
                Next

            End If

            TestList.Add(j, "Oyster")
            j += 1

            '---end try wrong runscript----------------------------------------------------------------------------------------------1


            '--check that he right runscript is show in the treeview
            If SearchTheTreeView(Oysterform.TreeView3, runscript) Is Nothing Then
                MessageBox.Show("TreeView Run Script Loaded is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Oysterform.TreeView3.Focus()
                Oysterform.TreeView3.SelectedNode = SearchTheTreeView(Oysterform.TreeView3, runscript)
                MessageBox.Show("TreeView Run Script Loaded is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            'load the runscript in the richtextbox
            OysterHelper.LoadFileFromTreeView(Oysterform.TreeView3, Oysterform.RichTextBox9)

            'check that the label shows the name of the loaded runscript file
            If Not Oysterform.WelLoadedFileLbl.Text.Contains(runscript) Then
                MessageBox.Show("Label indicating Loaded File is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else

                MessageBox.Show("Label indicating Loaded File is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            '-end treeview checks and load check -------------------------------------------------------------------------------- 2

            '3
            '--through with checks so far from Oyster Setup And Run Tab ------------------------------------



            '--on to loading ERmetrics Tab

            Oysterform.OysterTabs.SelectTab(1)

            '--load ermetricsTxBx working directory ----------------------------------------

            Oysterform.ERMetricsTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1

            Oysterform.ERMetricsTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1
            ' Oysterform.ERMetricsTxBx.Text = Space(1)
            oysterHelper.FillTextBox(Oysterform.ERMetricsTxBx, "c:\")
            TestList.Add(j, "Directory Choice")
            j += 1

            'load the correct answer
            oysterHelper.FillTextBox(Oysterform.ERMetricsTxBx, DList.Item("ERMetricsTxBx"))

            'end load ermetrics working directory-------------------------------------------------------- 3



            'check Oyster Working Directory loaded in ERMetrics Tab in EROysterWkDirTxBx with OysterWorkDirTxBx
            If Not Oysterform.EROysterWkDirTxBx.Text.Equals(Oysterform.OysterWorkDirTxBx.Text) Then
                MessageBox.Show("Loaded Oyster Working Directory in ERMetrics Tab is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else

                MessageBox.Show("Loaded Oyster Working Directory in ERMetrics Tab is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1




            '--------------------------------end check for correct oyster working dirextory in ermetrics tab --- 1

            'One note.  If you are running tests again which the last run
            'has completed and loaded the text boxes already then this will work
            'so seeing if this works to keep it from failing.  It will not save 
            'with empty boxes but will save with already populated boxes. Both 
            'are correct

            If String.IsNullOrEmpty(Oysterform.ERMetricsJarTxBx.Text) Then
                'Try to save...will not save without 3
                Oysterform.SaveERMetricsBtn.PerformClick()
                TestList.Add(j, "ERMetrics Text Boxes")
                j += 1
            Else
                '--save ermetrics
                Oysterform.SaveERMetricsBtn.PerformClick()
                TestList.Add(j, "Save Settings")
                j += 1
            End If



            '----end try to save-------------------------------------------------------------------------1

            'ERMetricsJarTxBx loading

            Oysterform.ERMetricsJarTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1

            Oysterform.ERMetricsJarTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1

            'does not exist
            oysterHelper.FillTextBox(Oysterform.ERMetricsJarTxBx, "c:\ermetrics.jar")
            TestList.Add(j, "Jar Choice")
            j += 1

            oysterHelper.FillTextBox(Oysterform.ERMetricsJarTxBx, DList.Item("ERMetricsJarTxBx"))
            TestList.Add(j, "Jar Choice")
            j += 1

            '----end load ermetrics jar ------------------------------------------------------------------ 4

            '--save ermetrics
            Oysterform.SaveERMetricsBtn.PerformClick()
            TestList.Add(j, "Save Settings")
            j += 1

            '--end save ermetrics-------------------------------------------------------------------------1

            '-make sure the ermetrics.properties file is in the treeview
            If SearchTheTreeView(Oysterform.TreeView4, "ERMetrics.properties") Is Nothing Then
                MessageBox.Show("TTreeView ERMetrics.properties file is Not Present!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                Oysterform.TreeView4.Focus()
                Oysterform.TreeView4.SelectedNode = SearchTheTreeView(Oysterform.TreeView4, "ERMetrics.properties")
                MessageBox.Show("TreeView ERMetrics.properties file is Present!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            'load the ermetrics.properties file
            OysterHelper.LoadFileFromTreeView(Oysterform.TreeView4, Oysterform.RichTextBox3)

            '--check the the label indicates the ermetrics.properties file is loaded
            If Not Oysterform.ERMetricsLoadedFile.Text.Contains("ERMetrics.properties") Then
                MessageBox.Show("Label indicating Loaded File is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Label indicating Loaded File is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1


            '------end ermetric.properties work------------------------------------------------------2

            Oysterform.OysterTabs.SelectTab(0)

            'run oyster after setting correct run script
            For Each aStr As String In Oysterform.OysterXmlListBx.Items
                Try
                    If IO.Path.GetFileName(Oysterform.OysterWorkDirTxBx.Text).Equals(aStr.Substring(0, aStr.ToUpper.IndexOf("RUN"))) Then
                        Dim index As Integer = Oysterform.OysterXmlListBx.Items.IndexOf(aStr)
                        Oysterform.OysterXmlListBx.SelectedIndex = index
                        Oysterform.RunOyster.PerformClick()
                        Exit For
                    End If
                Catch ex As Exception
                End Try
            Next

            Oysterform.OysterTabs.SelectTab(2)
            'stop Oyster run with stop button
            Oysterform.StopRun.PerformClick()
            TestList.Add(j, "Warning")
            j += 1
            '---------------end stop oyster run --------------------------------------------------1

            Oysterform.OysterTabs.SelectTab(1)

            'try to copy link file from stopped run and it will stop you because it is empty
            Oysterform.MoveFileBtn.PerformClick()
            TestList.Add(j, "File Empty")
            j += 1

            'run oyster now to completion so we can check other textboxes that are read only and labels dates etc.
            Oysterform.OysterTabs.SelectTab(0)
            'let it run now
            Oysterform.RunOyster.PerformClick()



            'SecondTest method below will be called to do more testing in the console output delegate in Oysterform when the run ends
            'We must do this because we can't sleep the thread and still get the oyster run completed.  


        Catch ex As Exception
            MsgBox("Error in StartTests is " & ex.Message)
            Testing = False
            isFinished = False
        Finally
            '  Console.WriteLine("Finished first StartTests")
        End Try


    End Sub

    'after running oyster then the these tests are 
    'run next
    Public Shared Sub SecondTest()

        Try
            Dim OysHelper As New OysterHelper
            'First checking all the labels loaded in the run output for a completed Oyster Run

            If Not Oysterform.OysterXmlListBx.SelectedValue.Equals(Oysterform.RunScriptNamelbl.Text) Then
                MessageBox.Show("Run Tab Run Script File Label Wrong", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else

                MessageBox.Show("Run Tab Run Script File Label is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            TestList.Add(j, "Match Found")
            j += 1


            If Not IO.Path.GetFileName(OysterHelper.GetInputCSVFileName(Oysterform.OysterWorkDirTxBx.Text)).Equals(Oysterform.InputFileLbl.Text) Then
                MessageBox.Show("Run Tab Input File Label Wrong", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Run Tab Input File Label is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1


            If Not IO.Path.GetFileName(My.Settings.ERMetricsLinkFile).Equals(Oysterform.LinkFilelbl.Text) Then
                MessageBox.Show("Run Tab Link File Label Wrong", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else

                MessageBox.Show("Run Tab Link File Label is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            '-----------------end checking labels in  Run Output Tab

            'Checking ermetrics tab for infomation loaded in the Oyster Run ---------------------------------------------------

            Oysterform.OysterTabs.SelectTab(1)

            'check link file name against xml file content
            If Not OysterHelper.GetLinkFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text).Equals(Oysterform.ERMetLinkFileTxbx.Text) Then
                MessageBox.Show("Link File in ERMetrics Wrong", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Link file in ERMetrics is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            'should now be able to move the link file from last Oyster Run to the ERMetrics directory for use in running ERMetrics on last run
            'and then check the date label on the run file
            Oysterform.MoveFileBtn.PerformClick()
            TestList.Add(j, "Check Properties File")
            j += 1

            'chcek date link file
            If Not DateTime.Parse(IO.File.GetLastWriteTime(Oysterform.ERMetLinkFileTxbx.Text)) = DateTime.Parse(Oysterform.LFDateLbl.Text.Trim) Then
                MessageBox.Show("Link File Date is Wrong in ERMetrics Tab!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Link File Date is Correct in ERMetrics Tab!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            '------------------------------------------------------------------------


            'helpful stuff tab
            Oysterform.OysterTabs.SelectTab(4)
            If Not OysterHelper.GetInputCSVFileName(Oysterform.OysterWorkDirTxBx.Text).Equals(Oysterform.HSCsvTxBx.Text) Then
                MessageBox.Show("Last Oyster CSV Input File in Helpful Stuff Tab is Wrong!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Last Oyster CSV Input File " & vbCrLf & "in Helpful Stuff Tab is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            If Not OysterHelper.GetLinkFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text).Equals(Oysterform.HSLinkFileTxBx.Text) Then
                MessageBox.Show("Last Produced Oyster Link File in Helpful Stuff Tab is Wrong!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Last Produced Oyster Link File " & vbCrLf & "in Helpful Stuff Tab is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TestList.Add(j, "Match Found")
            j += 1

            'CreateCSV File
            Oysterform.CreateCSVBtn.PerformClick()
            TestList.Add(j, "File Creation")
            j += 1

            'Create New Reference File File
            Oysterform.CreateRefFilebtn.PerformClick()
            TestList.Add(j, "File Creation")
            j += 1

            'Create New Weight File 
            Oysterform.CBStrapBtn.PerformClick()
            TestList.Add(j, "File Creation")
            j += 1



            '40 rows now in testerRows here

            'Oysterform.LastInputFileRB.Checked = True
            'Oysterform.Analyze_Btn.PerformClick()
            ''     MessageBox.Show("Testing", "Testing", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'AnalyzeForm.Exit_Btn2.PerformClick()


            'Python Exe Dir
            Oysterform.OysterTabs.SelectTab(8)

            'try save button with empty textboxes
            Oysterform.SaveRobotBtn.PerformClick()
            TestList.Add(j, "Requirement")
            j += 1
            'Try Run button
            Oysterform.RunPythonBtn.PerformClick()
            TestList.Add(j, "Requirement")
            j += 1
            Oysterform.PythonExeTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1

            Oysterform.PythonExeTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1

            OysHelper.FillTextBox(Oysterform.PythonExeTxBx, "c:\")
            TestList.Add(j, "Directory Choice")
            j += 1

            OysHelper.FillTextBox(Oysterform.PythonExeTxBx, DList.Item("PythonExeTxBx"))

            'try save button with one empty textbox
            Oysterform.SaveRobotBtn.PerformClick()
            TestList.Add(j, "Requirement")
            j += 1
            'end RobotWrkdir-------------------------------------------------------------------------------------- 3

            ''Python Exe Dir
            '  Oysterform.OysterTabs.SelectTab(8)

            Oysterform.RobotWkDirTxBx.Text = "c"
            TestList.Add(j, "Use the Browse Button")
            j += 1

            Oysterform.RobotWkDirTxBx.Text = Chr(32)
            TestList.Add(j, "Use the Browse Button")
            j += 1

            OysHelper.FillTextBox(Oysterform.RobotWkDirTxBx, "c:\")
            TestList.Add(j, "Directory Choice")
            j += 1

            OysHelper.FillTextBox(Oysterform.RobotWkDirTxBx, DList.Item("RobotWkDirTxBx"))
            TestList.Add(j, "Directory Choice")
            j += 1

            'end python exe dir-------------------------------------------------------------------------------------- 3

            '--save the two Robot textboxes
            Oysterform.SaveRobotBtn.PerformClick()
            TestList.Add(j, "Choices Saved")
            j += 1
            '--end save--------------------------------------------------------------------------------------------------- 1

            '------------Go to Working With Oyster files---------------------------------------------------------------------------------

            'Oysterform.OysterTabs.SelectTab(3)
            'Dim tHelper As New TestHelper   '----this is the only way to be able to use the shared variables--- everything only gets one instance of the shared variables

            ''make sure the Link File is in the Treeview and select it if it is-------------
            'If tHelper.SearchTheTreeView(Oysterform.TreeView1, IO.Path.GetFileName(Oysterform.ERMetLinkFileTxbx.Text)) Is Nothing Then
            '    MessageBox.Show("TrOyster Link File is Not in Treeview!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'Else
            '    Oysterform.TreeView1.Focus()
            '    Oysterform.TreeView1.SelectedNode = tHelper.SearchTheTreeView(Oysterform.TreeView1, IO.Path.GetFileName(Oysterform.ERMetLinkFileTxbx.Text))
            '    MessageBox.Show("Oyster Link File is in Treeview!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'End If
            'TestList.Add(j, "Match Found")
            'j += 1

            ''load the ermetrics.properties file - it must be selected above to work
            'OysterHelper.LoadFileFromTreeView(Oysterform.TreeView1, Oysterform.RichTextBox2)

            ''check the the label indicates the ermetrics.properties file is loaded
            'If Not Oysterform.WorkWithOysterFile.Text.Contains(IO.Path.GetFileName(Oysterform.ERMetLinkFileTxbx.Text)) Then
            '    MessageBox.Show("Label indicating Loaded File is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Else
            '    MessageBox.Show("Oyster Link File is Loaded and Label for loaded file agrees!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'End If
            'TestList.Add(j, "Match Found")
            'j += 1


            '--------------------------------------------------------------------------------
            ''Change Oyster Working Directory and make sure it changes in ERMetrics Tab

            'OysHelper.FillTextBox(Oysterform.OysterWorkDirTxBx, DList.Item("OysterWorkDirTxBx").Substring(0, DList.Item("OysterWorkDirTxBx").LastIndexOf("\") + 1) & "BlockingGoodIndex")


            ''check Oyster Working Directory loaded in ERMetrics Tab in EROysterWkDirTxBx with OysterWorkDirTxBx
            'If Not Oysterform.EROysterWkDirTxBx.Text.Equals(Oysterform.OysterWorkDirTxBx.Text) Then
            '    MessageBox.Show("Loaded Oyster Working Directory in ERMetrics Tab is Not Correct!", "No Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Else

            '    MessageBox.Show("Loaded Oyster Working Directory in ERMetrics Tab is Correct!", "Match Found", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'End If
            'TestList.Add(j, "Match Found")
            'j += 1


            ''change it back once tests occurs

            'OysHelper.FillTextBox(Oysterform.OysterWorkDirTxBx, DList.Item("OysterWorkDirTxBx"))

            ''---end changing oyster working directory


            '40 testerrows now

            '  Dim boo As Boolean = ThirdTests()

            '-------Lets work with the IKB Tabs now---------------------------------------------



            '--------------------------------------------------------------
            'Running ERMetrics below will finish the testing so add tests before this.  THe console output of ERMetrics will kickoff the 
            'Reporting once ERMetrics is finished running so add tests above

            Oysterform.OysterTabs.SelectTab(1)
            'let it run now
            Oysterform.RunERMetricsBtn.PerformClick()

            '-----------------End Of Testing---------------------------------------------------

        Catch ex As Exception
            Testing = False
            isFinished = False
            MsgBox("Error in SecondTests is " & ex.Message)

        End Try

    End Sub

    Public Shared Function GetFunctionNAmes(ByVal AttributesXMLFile As String) As String()
        Dim sArray() As String = Nothing
        Dim NewArray() As String = Nothing

        Try
            Dim xelement As XElement = XElement.Load(AttributesXMLFile)
            'Dim xelement As XElement = XElement.Load("C:\Oyster\ListA\Scripts\ListAAttributes3.xml")
            Dim rules As IEnumerable(Of XElement) = xelement.Descendants("Term")
            '   Dim rules As IEnumerable(Of XElement) = xelement.Element("IdentityRules").Element("Rule").Element("Term")
            ' Dim sArray() As String = Nothing
            Dim it As Integer = 0
            For Each rule As XElement In rules
                '  Console.WriteLine(rule)
                Dim attList As IEnumerable(Of XAttribute) = rule.Attributes("SIMILARITY")

                For Each att In attList
                    Dim st As String = att.ToString

                    '  Console.WriteLine(st.Substring(st.IndexOf("""") + 1))

                    ReDim Preserve sArray(it)

                    st = st.Substring(st.IndexOf("""") + 1)
                    st = st.Substring(0, st.Length - 1)

                    If st.Contains("(") Then
                        st = st.Substring(0, st.IndexOf("("))
                    End If
                    sArray(it) = st
                    it += 1

                Next

            Next
            '  Console.WriteLine(sArray.Count)
            Dim I As Integer = 0

            Dim quit As Boolean = False
            For Each s As String In sArray
                quit = False
                If I > 0 Then
                    For Each str As String In NewArray
                        If s = str Then
                            quit = True
                            Exit For
                        End If
                    Next
                Else

                    ReDim Preserve NewArray(I)
                    NewArray(I) = s
                    I += 1
                    quit = True
                End If

                If Not quit Then
                    ReDim Preserve NewArray(I)
                    NewArray(I) = s
                    I += 1
                End If
                '  Console.WriteLine(s)
            Next
            '    Console.WriteLine(NewArray.Length)

            For Each strr As String In NewArray
                '        Console.WriteLine(strr)
            Next
        Catch ex As Exception

        End Try


        Return NewArray

    End Function

    Public Shared Engine1 As String = Nothing
    Public Shared Engine2 As String = Nothing


    Public Shared Sub Header(ByRef file As IO.StreamWriter, ByVal functionCount As Integer)

        file.WriteLine(vbCrLf)
        file.WriteLine("-------------------------------------------------------------------")
        file.WriteLine("UALR Oyster Jar Testing")
        file.WriteLine(Now.ToShortDateString & " " & Now.ToShortTimeString)
        If TJarBean.RunOldAndNewJar Then
            file.WriteLine("Jars Tested: " & TJarBean.GetSetOldJar & " and " & TJarBean.GetSetNewJar)
        ElseIf TJarBean.RunOldJar AndAlso Not TJarBean.RunNewJar Then
            file.WriteLine("Jars Tested: " & TJarBean.GetSetOldJar)
        ElseIf Not TJarBean.RunOldJar AndAlso TJarBean.RunNewJar Then
            file.WriteLine("Jars Tested: " & TJarBean.GetSetNewJar)
        End If

        If TJarBean.RunBothEngines Then
            file.WriteLine("Engines Tested: FSCluster and RSWooshStandard")
        ElseIf TJarBean.RunFSCluster AndAlso Not TJarBean.RunRSwoosh Then

            file.WriteLine("Jars Tested: FSCluster")
        ElseIf Not TJarBean.RunFSCluster AndAlso TJarBean.RunRSwoosh Then
            file.WriteLine("Jars Tested: RSWooshStandard")
        End If

        file.WriteLine("Functions Tested: " & functionCount & " of 37")
        file.WriteLine("-------------------------------------------------------------------")

    End Sub
    Public Shared Sub MakeOutput(ByVal JarTestOutput As Dictionary(Of Integer, String()), ByVal Runcount As Integer)

        NewJarTesting = False
        Dim file As System.IO.StreamWriter = Nothing
        Dim fileName As String = Nothing
        Try
            My.Settings.Reload()
            fileName = Oysterform.OysterWorkDirTxBx.Text & "\Output\" & "JarTesting_" & Now.Month & "_" & Now.Day & "_" & Now.Year & "_" & Now.Minute & Now.Millisecond & ".txt"
            file = My.Computer.FileSystem.OpenTextFileWriter(fileName, True)

            My.Settings.JarTestCount = My.Settings.JarTestCount + 1
            My.Settings.Save()

            Dim attributeFileName As String = GetAttributeFileName(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)

            Dim s() As String = GetFunctionNAmes(Oysterform.OysterRootDirTxBx.Text & "\" & attributeFileName.Substring(1))
            file.WriteLine(vbCrLf)
            file.WriteLine("File Name: " & fileName)


            Header(file, s.Length)

            file.WriteLine("Functions used in Rules")
            file.WriteLine(vbCrLf)

            For Each st As String In s
                file.WriteLine(st)
            Next


            file.WriteLine(vbCrLf)

            For Each pair As KeyValuePair(Of Integer, String()) In JarTestOutput
                Dim WriteIt As Boolean = True
                Dim StringArrayOutput As String()
                StringArrayOutput = JarTestOutput.Item(pair.Key)
                Dim firstCount As Integer = 0
                Dim secondCount As Integer = 0
                Dim writeString As Integer = 0
                ' Dim firstCount As Integer = 0
                For Each str As String In StringArrayOutput

                    'C:\Oyster>java -jar oyster-3.7.4.jar 
                    If str.Contains("java -jar") AndAlso Not str.Contains("er-metrics.jar") Then
                        file.WriteLine(str)
                        file.WriteLine(vbCrLf)
                    End If



                    If str.Contains("Engine Type") Then


                        file.WriteLine(str)
                        file.WriteLine(vbCrLf)
                    End If

                    If str.Contains("Summary Stats") And writeString = 0 Then

                        file.WriteLine("#########################")
                        file.WriteLine(str)
                        writeString = 1

                    ElseIf writeString = 1 Then

                        If str.Contains("Clusters loaded") Then
                            writeString = 2

                        Else
                            file.WriteLine(str)
                        End If
                    End If

                    If str.Contains("First Rule Stats") And firstCount = 0 Then

                        file.WriteLine("#########################")
                        file.WriteLine(str)
                        firstCount = 1

                    ElseIf firstCount = 1 Then

                        If str.Contains("Complete Rule Stats") Then
                            firstCount = 2

                        Else
                            file.WriteLine(str)
                        End If
                    End If

                    If str.Contains("Total elapsed time") Then
                        file.WriteLine(vbCrLf)
                        file.WriteLine(str)
                        file.WriteLine(vbCrLf)


                    End If

                    If str.Contains("Computing True Pairs and Clusters for") And secondCount = 0 Then

                        secondCount = 1
                        file.WriteLine(str)

                    ElseIf secondCount = 1 Then

                        If str.Contains("Complete") Then

                            secondCount = 2


                            file.WriteLine(str)
                            file.WriteLine(vbCrLf)

                        Else
                            file.WriteLine(str)

                        End If
                    End If

                Next
            Next
            If file IsNot Nothing Then
                file.Close()
                file = Nothing
            End If

            Dim rFileForm As New ReadFileForm
            rFileForm.Size() = New System.Drawing.Size(1000, 700)
            rFileForm.ReadFileRichTxBx.LoadFile(fileName, fileType:=RichTextBoxStreamType.PlainText)
            rFileForm.ShowDialog()


        Catch ex As Exception
            Console.WriteLine("MakeOutPut Error is " & ex.Message)
            If file IsNot Nothing Then
                file.Close()
            End If
        Finally
            If file IsNot Nothing Then
                file.Close()
            End If
            NewJarTesting = False
            isFinished = False
        End Try


    End Sub

    Public Shared Sub ChangeEngine(ByVal OldEngine As String, ByVal NewEngine As String, ByVal pathAndFile As String)

        Try

            'Dim content = My.Computer.FileSystem.ReadAllText(pathAndFile)
            'Dim m As Match = Regex.Match(content, "FSCLuster", RegexOptions.IgnoreCase)
            'If m.Success Then
            File.WriteAllText(pathAndFile, File.ReadAllText(pathAndFile).Replace(OldEngine, NewEngine))
            'Else
            'File.WriteAllText(pathAndFile, File.ReadAllText(pathAndFile).Replace(NewEngine, OldEngine))
            'End If

        Catch ex As Exception
            Console.WriteLine("ChangeEngine error is " & ex.Message)
        End Try

    End Sub

    Public Shared Sub ChangeEngine2(ByVal OldEngine As String, ByVal NewEngine As String, ByVal pathAndFile As String, ByVal EngineWanted As String)

        Try

            Dim content = My.Computer.FileSystem.ReadAllText(pathAndFile)
            Dim m As Match = Regex.Match(content, EngineWanted, RegexOptions.IgnoreCase)
            If Not m.Success Then
                File.WriteAllText(pathAndFile, File.ReadAllText(pathAndFile).Replace(OldEngine, NewEngine))
                'Else
                '    File.WriteAllText(pathAndFile, File.ReadAllText(pathAndFile).Replace(NewEngine, OldEngine))
            End If

        Catch ex As Exception
            Console.WriteLine("ChangeEngine2 error is " & ex.Message)
        End Try

    End Sub




    Public Shared TestJarCounter As Integer = 1
    Public Shared TJarBean As New JarTesterBean
    '------------------------------------------------------------
    'may not use it
    Public Shared Sub ThirdTests(ByVal JarBean As JarTesterBean)

        TJarBean = JarBean
        Dim dictionary As New Dictionary(Of Integer, RadioButton)
        Dim oysterHelper As New OysterHelper
        Oysterform.RunCount = JarBean.RunCount

        dictionary = TJarBean.JarDictionary
        Dim i As Integer = 0
        Dim RadioButton1 As RadioButton = Nothing
        Dim RadioButton2 As RadioButton = Nothing

        For Each pair As KeyValuePair(Of Integer, RadioButton) In dictionary
            If i = 0 Then
                RadioButton1 = dictionary.Item(pair.Key)
                i += 1
            Else
                RadioButton2 = dictionary.Item(pair.Key)
            End If
        Next

        Dim RBName1 As String = RadioButton1.Name
        '     Console.WriteLine(RBName1)
        Dim RBName2 As String = RadioButton2.Name
        '      Console.WriteLine(RBName2)

        Try
            If TestJarCounter = 1 Then

                '    Console.WriteLine("TestJarCounter is " & 1)
                Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem
                Dim Engine As String = GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)

                If Not Engine.Equals(TJarBean.Engine) Then
                    If Not TJarBean.GetSetFSCluster.Contains(TJarBean.Engine) Then

                        ChangeEngine2(TJarBean.GetSetFSCluster, TJarBean.GetSetRSwoosh, pathFile, TJarBean.Engine)
                    Else
                        ChangeEngine2(TJarBean.GetSetRSwoosh, TJarBean.GetSetFSCluster, pathFile, TJarBean.Engine)
                    End If

                ElseIf RBName1.Equals("OldAndNewJarRB") AndAlso RBName2.Equals("BothEngRB") Then
                    If Not Engine.Contains("FSCluster") Then
                        ChangeEngine(TJarBean.GetSetRSwoosh, TJarBean.GetSetFSCluster, pathFile)
                    End If
                End If

                If Not RBName1.Equals("OldJarRB") And Not RBName1.Equals("OldAndNewJarRB") Then
                    Oysterform.OysterTabs.SelectTab(0)
                    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
                    Oysterform.SaveAll.PerformClick()
                End If

                'If RBName1.Equals("") Then
                '    Dim content = My.Computer.FileSystem.ReadAllText(pathFile)
                '    Dim m As Match = Regex.Match(content, "FSCLuster", RegexOptions.IgnoreCase)
                '    If Not m.Success Then

                '        File.WriteAllText(pathFile, File.ReadAllText(pathFile).Replace(NewEngine, OldEngine))
                '    End If
                'End If


            ElseIf TestJarCounter = 2 Then
                '      Console.WriteLine("TestJarCounter is " & 2)

                Dim Engine1 As String = GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
                Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem

                If RBName2.Equals("BothEngRB") AndAlso Not RBName1.Equals("OldAndNewJarRB") Then
                    If Engine1.Contains("FSCluster") Then

                        ChangeEngine(TJarBean.GetSetFSCluster, TJarBean.GetSetRSwoosh, pathFile)

                    ElseIf Engine1.Contains("RSwooshStandard") Then

                        ChangeEngine(TJarBean.GetSetRSwoosh, TJarBean.GetSetFSCluster, pathFile)

                    End If
                ElseIf RBName2.Equals("BothEngRB") AndAlso RBName1.Equals("OldAndNewJarRB") Then

                    '    Console.WriteLine("do nothing now")
                End If

                If RBName1.Equals("NewJarRB") Then
                    Oysterform.OysterTabs.SelectTab(0)
                    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
                    Oysterform.SaveAll.PerformClick()

                End If

                If RBName1.Equals("OldAndNewJarRB") Then
                    If Oysterform.OysterJarTxBx.Text.Equals(TJarBean.GetSetOldJar) Then
                        Oysterform.OysterTabs.SelectTab(0)
                        oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
                        Oysterform.SaveAll.PerformClick()
                    Else
                        Oysterform.OysterTabs.SelectTab(0)
                        oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetOldJar)
                        Oysterform.SaveAll.PerformClick()
                    End If
                End If
                ' oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)


            ElseIf TestJarCounter = 3 Then

                ' Console.WriteLine("TestJarCounter is " & 3)

                Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem
                ChangeEngine(TJarBean.GetSetFSCluster, TJarBean.GetSetRSwoosh, pathFile)
                Engine2 = GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
                oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetOldJar)
                Oysterform.OysterTabs.SelectTab(0)
                    Oysterform.SaveAll.PerformClick()

                ElseIf TestJarCounter = 4 Then

                '        Console.WriteLine("TestJarCounter is " & 4)
                'If Not RBName2.Equals("BothEngRB") And Not RBName1.Equals("OldAndNewJarRB") Then
                '    Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem
                '    ChangeEngine(TJarBean.GetSetRSwoosh, TJarBean.GetSetFSCluster, pathFile)
                'End If

                Oysterform.OysterTabs.SelectTab(0)
                oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
                Oysterform.SaveAll.PerformClick()

            ElseIf TestJarCounter = 6 Then

                oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
                Oysterform.OysterTabs.SelectTab(0)
                Oysterform.SaveAll.PerformClick()
            End If




            'If TestJarCounter = 2 Then

            '    Engine1 = GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
            '    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
            '    Oysterform.OysterTabs.SelectTab(0)
            '    Oysterform.SaveAll.PerformClick()

            'ElseIf TestJarCounter = 3 Then

            '    Dim pathFile As String = Oysterform.OysterRootDirTxBx.Text & "\" & Oysterform.OysterXmlListBx.SelectedItem
            '    ChangeEngine(TJarBean.GetSetFSCluster, TJarBean.GetSetRSwoosh, pathFile)
            '    Engine2 = GetOysterEngine(Oysterform.OysterRootDirTxBx.Text, Oysterform.OysterWorkDirTxBx.Text)
            '    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetOldJar)
            '    Oysterform.OysterTabs.SelectTab(0)
            '    Oysterform.SaveAll.PerformClick()

            'ElseIf TestJarCounter = 4 Then

            '    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
            '    Oysterform.OysterTabs.SelectTab(0)
            '    Oysterform.SaveAll.PerformClick()

            'ElseIf TestJarCounter = 6 Then

            '    oysterHelper.FillTextBox(Oysterform.OysterJarTxBx, TJarBean.GetSetNewJar)
            '    Oysterform.OysterTabs.SelectTab(0)
            '    Oysterform.SaveAll.PerformClick()
            'End If

            TestJarCounter += 1

            Oysterform.OysterTabs.SelectTab(0)
            Oysterform.RunOyster.PerformClick()

        Catch ex As Exception
            Console.WriteLine("ThirdTests error is " & ex.Message)
        End Try

    End Sub

    Public Shared Sub FourthTests()

        Oysterform.OysterTabs.SelectTab(1)
        Oysterform.MoveFileBtn.PerformClick()
        Oysterform.RunERMetricsBtn.PerformClick()

    End Sub
    'end third tests--------------------------------------


    'reads the Oyster Run Script used in Oyster Run for the Attribute file name and returns it for use and loading textboxes,etc.
    Protected Friend Shared Function GetAttributeFileName(ByVal oysterRootDirectory As String, ByVal oysterWorkingDirectory As String) As String

        Dim AttributeFileName As String = Nothing
        Dim fullPathAndName As String = Nothing
        Dim objReader As System.IO.StreamReader = Nothing

        Try
            fullPathAndName = oysterRootDirectory & "\" & Path.GetFileName(oysterWorkingDirectory) & "RunScript.xml"

            Dim xelement As XElement = XElement.Load(fullPathAndName)
            'Dim xelement As XElement = XElement.Load("C:\Oyster\ListA\Scripts\ListAAttributes3.xml")
            ' Dim rules As IEnumerable(Of XElement) = xelement.Descendants("AttributePath")

            AttributeFileName = xelement.Descendants("AttributePath").Value
            '     Console.WriteLine(AttributeFileName)

        Catch ex As Exception
            ' MessageBox.Show("Error Thrown Getting Attributel File Name is " & ex.Message, "Oyster Attribute File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Console.WriteLine("Error getting attribute file name is " & ex.Message)
        Finally
            'If objReader IsNot Nothing Then
            '    objReader.Close()
            '    objReader = Nothing

            'End If
        End Try
        Return AttributeFileName
    End Function

    'reads the Oyster Run Attribute file used In Oyster Run For the Attribute file name And returns the OysterEngineName
    Protected Friend Shared Function GetOysterEngine(ByVal oysterRootDirectory As String, ByVal oysterWorkingDirectory As String) As String
        Dim fullPathAndName As String = Nothing
        Dim Engine As String = Nothing

        Try
            fullPathAndName = oysterRootDirectory & "\" & Path.GetFileName(oysterWorkingDirectory) & "RunScript.xml"

            Dim xelement As XElement = XElement.Load(fullPathandName)
            '         <EREngine Type = "RSwooshStandard" />

            Engine = xelement.Element("EREngine").Attribute("Type").Value



            '  Console.WriteLine(Engine)

        Catch ex As Exception
            MessageBox.Show("Error Thrown Getting Engine from run script is " & ex.Message, "Oyster Attribute File", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Console.WriteLine("Error Getting Engine from run script is " & ex.Message)
        Finally

        End Try
        Return Engine
    End Function


    'Creates the final report from the ResultList Dictionary
    Public Shared Sub FinishReport()
        Try
            Testing = False
            isFinished = False
            CompareResults()

            Dim testerRows As New TesterRows

            For Each key In ResultList.Keys
                ' Console.WriteLine(key.ToString)
                Dim PassNoPass As String = Nothing

                If ResultList.Item(key).Equals(True) Then
                    PassNoPass = "PASS"
                Else
                    PassNoPass = "FAIL"
                End If

                testerRows.LoadTestDataGridView(key, PassNoPass)

            Next


            TestOutPutForm.TestingDataGridView.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False

            For i As Integer = 0 To TestOutPutForm.TestingDataGridView.Columns.Count - 1

                If i = TestOutPutForm.TestingDataGridView.Columns.Count - 2 Then
                    TestOutPutForm.TestingDataGridView.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                Else
                    TestOutPutForm.TestingDataGridView.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                End If
            Next i

            TestOutPutForm.ShowDialog()

        Catch ex As Exception
            MsgBox("Finish Report Error is " & ex.Message)
        Finally
            Testing = False
            isFinished = False
        End Try




    End Sub

    'compares the two dictionaries to grade the test run
    Private Shared Sub CompareResults()

        Dim expected As String = Nothing
        Dim actual As String = Nothing

        Try
            ' Console.WriteLine("TestList size " & TestList.Count.ToString)
            'Console.WriteLine("windowsTest List size " & WindowTestList.Count.ToString)

            For Each key In TestList.Keys
                expected = TestList.Item(key)
                '   Console.WriteLine("EXpecting " & expected)
                actual = WindowTestList.Item(key)
                '    Console.WriteLine("Actual " & actual)
                If String.Equals(expected, actual) Then
                    ResultList.Add(key, True)
                Else
                    ResultList.Add(key, False)
                End If
            Next



        Catch ex As Exception
            Testing = False
            isFinished = False
            MsgBox("Compare Results Error is " & ex.Message)

        End Try
    End Sub

    'Public Sub RunTests()
    '    RunWindowClose()
    'End Sub

    '------------------------------------------------------------
    'Closes the windows in testing and 
    'Loads the WindowsTestList Dictionary

    Public Sub RunWindowClose()
        ' Console.WriteLine("called " & Now.ToShortTimeString)
        Dim NMethods As New NativeMethods
        Try
            ' "OysterHelper",

            'get window handle by message window caption
            'col.Add("Oyster Helper")
            Dim col = New List(Of String) From {
                "Use the Browse Button",
                "Oyster",
                "Directory Choice",
                "Jar Choice",
                "Error",
                "Choices Saved",
                "Browse to it Please",
                "Save Settings",
                "Requirement",
                "Warning",
                "Check Properties File",
                "File Empty",
                "No Match Found",
                "Match Found",
                "ERMetrics Text Boxes",
                "File Creation",
                "Testing"
            }


            Dim k As Integer = 0
            Do While isFinished

                ' Console.WriteLine("running")
                Dim hWnd As IntPtr
                For Each str As String In col
                    hWnd = NativeMethods.FindWindow(Nothing, CType(str, String))
                    ' check if handle is valid
                    If hWnd <> IntPtr.Zero Then ' window found
                        ' bring window to front/sets focus on the window 
                        NativeMethods.SetForegroundWindow(hWnd)
                        ''close stop message by clicking yes 
                        ''else close one of the other windows
                        ''by caption - not needed. did not work on
                        ''testers computer so changed
                        'If str.Equals("Warning") Then
                        '    Dim hwndChild As Long
                        '    hwndChild = NativeMethods.FindWindowEx(hWnd, 0, "BUTTON", "&Yes")
                        '    NativeMethods.SendMessage(hwndChild, BM_CLICK, 0, 0)
                        '    WindowTestList.Add(k, str)
                        '    k += 1
                        'Else
                        NativeMethods.SendMessage(hWnd, WM_SYSCOMMAND, SC_CLOSE, 0)
                        If Testing Then
                            WindowTestList.Add(k, str)
                            k += 1
                            '  ElseIf NewJarTesting Then



                        End If

                    End If

                Next
                Threading.Thread.Sleep(2000)

            Loop
            col.Clear()
        Catch ex As Exception
            Testing = False
            isFinished = False
            Console.WriteLine("Error in RunClosewindow " & ex.Message)
            '     MsgBox("Error in RunClosewindow " & ex.Message)
        Finally
            'isFinished = False
            'WindowTestList.Clear()

        End Try
    End Sub

    '--------------------------------------------
    'Search any Treeview recursively

    Dim NodesThatMatch As New List(Of TreeNode)

    Private Function SearchTheTreeView(ByVal TV As TreeView, ByVal TextToFind As String) As TreeNode

        NodesThatMatch.Clear()
        ' Keep calling RecursiveSearch
        For Each TN As TreeNode In TV.Nodes
            '      Console.WriteLine("First For " &TN.Text)
            If TN.Text.Contains(TextToFind) Then
                NodesThatMatch.Add(TN)
            End If

            RecursiveSearch(TN, TextToFind)
        Next
        '    Console.WriteLine("node count" & NodesThatMatch.Count)
        If NodesThatMatch.Count > 0 Then
            Return NodesThatMatch(0)
        Else
            Return Nothing
        End If

    End Function

    Private Sub RecursiveSearch(ByVal treeNode As TreeNode, ByVal TextToFind As String)

        ' Keep calling the test recursively.
        For Each TN As TreeNode In treeNode.Nodes
            '      Console.WriteLine(TN.Text)
            If TN.Text.Contains(TextToFind) Then
                NodesThatMatch.Add(TN)
            End If

            RecursiveSearch(TN, TextToFind)
        Next
    End Sub

    'Used to close windows in testing
    'and must be in a seperate shared class
    Friend Class NativeMethods
        <DllImport("user32.dll", CharSet:=CharSet.Unicode)>
        Protected Friend Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
        End Function


        <DllImport("user32.dll", CharSet:=CharSet.Unicode, EntryPoint:="FindWindow")>
        Public Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
        End Function

        <DllImport("user32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
        Protected Friend Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
        End Function

        <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)>
        Protected Friend Shared Function FindWindowEx(ByVal parentHandle As IntPtr, ByVal childAfter As IntPtr, ByVal lclassName As String, ByVal windowTitle As String) As IntPtr
        End Function

        '---end window closing imports and attributes--------------------

    End Class

End Class

