﻿
'Jar testing bean to store information

Public Class JarTesterBean

    Private OldJar As String
    Private NewJar As String
    Private FSCluster As String
    Private RSwoosh As String
    Private FSClusterRB As Boolean = False
    Private RSwooshRB As Boolean = False
    Private OldJarRB As Boolean = False
    Private NewJarRB As Boolean = False
    Private OldAndNewJarRB As Boolean = False
    Private FSclusterRSwooshRB As Boolean = False
    Private TheRunCount As Integer
    Private TheFSRWCount As Integer
    Private OysterEngine As String

    Private Dictionary As Dictionary(Of Integer, RadioButton)

    Public Property Engine() As String
        Get
            Return OysterEngine
        End Get
        Set(ByVal value As String)
            OysterEngine = value
        End Set
    End Property
    Public Property JarDictionary() As Dictionary(Of Integer, RadioButton)
        Get
            Return Dictionary
        End Get
        Set(ByVal value As Dictionary(Of Integer, RadioButton))
            Dictionary = value
        End Set
    End Property
    Public Property RunOldAndNewJar() As Boolean
        Get
            Return OldAndNewJarRB
        End Get
        Set(ByVal value As Boolean)
            OldAndNewJarRB = value
        End Set
    End Property
    Public Property RunOldJar() As Boolean
        Get
            Return OldJarRB
        End Get
        Set(ByVal value As Boolean)
            OldJarRB = value
        End Set
    End Property
    Public Property RunNewJar() As Boolean
        Get
            Return NewJarRB
        End Get
        Set(ByVal value As Boolean)
            NewJarRB = value
        End Set
    End Property
    Public Property RunBothEngines() As Boolean
        Get
            Return FSclusterRSwooshRB
        End Get
        Set(ByVal value As Boolean)
            FSclusterRSwooshRB = value
        End Set
    End Property
    Public Property FSRWCount() As Integer
        Get
            Return TheFSRWCount
        End Get
        Set(ByVal value As Integer)
            TheFSRWCount = value
        End Set
    End Property

    Public Property RunCount() As Integer
        Get
            Return TheRunCount
        End Get
        Set(ByVal value As Integer)
            TheRunCount = value
        End Set
    End Property
    Public Property RunFSCluster() As Boolean
        Get
            Return FSClusterRB
        End Get
        Set(ByVal value As Boolean)
            FSClusterRB = value
        End Set
    End Property
    Public Property RunRSwoosh() As Boolean
        Get
            Return RSwooshRB
        End Get
        Set(ByVal value As Boolean)
            RSwooshRB = value
        End Set
    End Property
    Public Property GetSetFSCluster() As String
        Get
            Return FSCluster
        End Get
        Set(ByVal value As String)
            FSCluster = value
        End Set
    End Property

    Public Property GetSetOldJar() As String
        Get
            Return OldJar
        End Get
        Set(ByVal value As String)
            OldJar = value
        End Set
    End Property

    Public Property GetSetNewJar() As String
        Get
            Return NewJar
        End Get
        Set(ByVal value As String)
            NewJar = value
        End Set
    End Property

    Public Property GetSetRSwoosh() As String
        Get
            Return RSwoosh
        End Get
        Set(ByVal value As String)
            RSwoosh = value
        End Set
    End Property
End Class
