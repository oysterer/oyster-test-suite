﻿
'Just what is says...the test output form

Public Class TestOutPutForm

    Private strformat As StringFormat
    Private arrColumnsLeft As New ArrayList
    Private arrColumnWidth As New ArrayList
    Private iCellHeight As Integer = 0
    Private iTotalWidth As Integer = 0
    Private iRow As Integer = 0
    Private bFirstPage As Boolean = False
    Private bNewPage As Boolean = False
    Private iHeaderHeight As Integer = 0



    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Dispose()
    End Sub

    Private Sub PrintDocument1_BeginPrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        Try



            strformat = New StringFormat
            strformat.Alignment = StringAlignment.Near
            strformat.LineAlignment = StringAlignment.Center
            'strformat.Trimming = StringTrimming.EllipsisCharacter

            arrColumnsLeft.Clear()
            arrColumnWidth.Clear()
            iCellHeight = 0
            iRow = 0
            bNewPage = True
            bFirstPage = True
            iTotalWidth = 0
            PrintDocument1.DefaultPageSettings.Landscape = True

            For Each dGridCol As DataGridViewColumn In TestingDataGridView.Columns
                iTotalWidth += dGridCol.Width
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Try


            Dim iLeftMargin As Integer = e.MarginBounds.Left
            Dim iTopMargin As Integer = e.MarginBounds.Top
            Dim bMorePagesToPrint As Boolean = False
            Dim iTmpWidth As Integer = 0

            If bFirstPage Then

                For Each Gridcol As DataGridViewColumn In TestingDataGridView.Columns
                    iTmpWidth = CInt(Math.Truncate(Math.Floor(CDbl(CDbl(Gridcol.Width) / CDbl(iTotalWidth) * CDbl(iTotalWidth) * CDbl(e.MarginBounds.Width) / CDbl(iTotalWidth)))))
                    iHeaderHeight = CInt(e.Graphics.MeasureString(Gridcol.HeaderText, Gridcol.InheritedStyle.Font, iTmpWidth).Height)

                    arrColumnsLeft.Add(iLeftMargin)
                    arrColumnWidth.Add(iTmpWidth)
                    iLeftMargin += iTmpWidth

                Next
            End If

            Do While iRow <= TestingDataGridView.Rows.Count - 1
                Dim GridRow As DataGridViewRow = TestingDataGridView.Rows(iRow)

                iCellHeight = GridRow.Height + 30
                Dim icount As Integer = 0

                If iTopMargin + iCellHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then
                    bNewPage = True
                    bFirstPage = False
                    bMorePagesToPrint = True
                    Exit Do
                Else
                    If bNewPage Then
                        Dim drawFont As New Font("Arial", 14)
                        '  e.Graphics.DrawString("Oyster Helper Automated Testing Results", New Font(drawFont, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top)

                        '   e.Graphics.DrawString("Oyster Helper Automated Testing Results", New Font(TestingDataGridView.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top)
                        ' Dim strDate As String = Date.Now.ToLongDateString & " " & Date.Now.ToShortDateString
                        Dim strDate As String = "Oyster Helper Automated Testing Results   " & Date.Now.ToLongDateString
                        e.Graphics.DrawString(strDate, New Font(drawFont, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(strDate, New Font(drawFont, FontStyle.Bold), e.MarginBounds.Width).Width), e.MarginBounds.Top - e.Graphics.MeasureString("Helper", New Font(New Font(drawFont, FontStyle.Bold), FontStyle.Bold), e.MarginBounds.Width).Height)
                        ''  e.Graphics.DrawString(strDate, New Font(TestingDataGridView.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(strDate, New Font(TestingDataGridView.Font, FontStyle.Bold), e.MarginBounds.Width).Width), e.MarginBounds.Top - e.Graphics.MeasureString("Oyster Helper Testing Results", New Font(New Font(TestingDataGridView.Font, FontStyle.Bold), FontStyle.Bold), e.MarginBounds.Width).Height - 13)
                        ' e.Graphics.DrawString(strDate, New Font(TestingDataGridView.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(strDate, New Font(TestingDataGridView.Font, FontStyle.Bold), e.MarginBounds.Width).Width), e.MarginBounds.Top - e.Graphics.MeasureString("TestingDataGridView Details", New Font(New Font(TestingDataGridView.Font, FontStyle.Bold), FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                        'draw column
                        iTopMargin = e.MarginBounds.Top

                        For Each gridCol As DataGridViewColumn In TestingDataGridView.Columns

                            e.Graphics.FillRectangle(New SolidBrush(Color.LightGray), New Rectangle(DirectCast(arrColumnsLeft(icount), Integer), iTopMargin, DirectCast(arrColumnWidth(icount), Integer), iHeaderHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(DirectCast(arrColumnsLeft(icount), Integer), iTopMargin, DirectCast(arrColumnWidth(icount), Integer), iHeaderHeight))
                            e.Graphics.DrawString(gridCol.HeaderText, gridCol.InheritedStyle.Font, New SolidBrush(gridCol.InheritedStyle.ForeColor), New RectangleF(DirectCast(arrColumnsLeft(icount), Integer), iTopMargin, DirectCast(arrColumnWidth(icount), Integer), iHeaderHeight), strformat)
                            icount += 1

                        Next

                        bNewPage = False
                        iTopMargin += iHeaderHeight
                    End If



                    icount = 0

                    For Each cel As DataGridViewCell In GridRow.Cells
                        If cel.Value IsNot Nothing Then
                            e.Graphics.DrawString(cel.Value.ToString, cel.InheritedStyle.Font, New SolidBrush(cel.InheritedStyle.ForeColor), New RectangleF(DirectCast(arrColumnsLeft(icount), Integer), CSng(iTopMargin), DirectCast(arrColumnWidth(icount), Integer), iHeaderHeight), strformat)

                        End If
                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(DirectCast(arrColumnsLeft(icount), Integer), iTopMargin, DirectCast(arrColumnWidth(icount), Integer), iCellHeight))
                        icount += 1
                    Next
                End If
                iRow += 1
                iTopMargin += iCellHeight

            Loop
            If bMorePagesToPrint Then
                e.HasMorePages = True
            Else
                e.HasMorePages = False
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        Dim print_preview As New PrintPreviewDialog

        print_preview.Document = PrintDocument1
        DirectCast(print_preview, Form).WindowState = FormWindowState.Maximized
        print_preview.ShowDialog()
    End Sub


End Class