﻿Imports System.Text



Public Class Form1
    Dim dt As DataTable = Nothing
    Protected Friend assertion As String = Nothing
    Private selectedrb As RadioButton

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles LoadFile.Click
        Try

            dt = New DataTable
            Dim fName As String = ""
            OpenFileDialog1.InitialDirectory = "C:\Oyster\ListA\Input"
            'OpenFileDialog1.Filter = "CSV files(*.csv)|*.csv"
            OpenFileDialog1.Filter = "CSV files(*.csv)|*.csv|TXT|*.txt" ' "BMP|*.bmp|GIF|*.gif|
            OpenFileDialog1.RestoreDirectory = True
            If (OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                fName = OpenFileDialog1.FileName
            End If

            Dim TextLine As String = ""
            Dim SplitLine() As String
            AssertDataGridView.DataSource = Nothing
            dt.Clear()

            If System.IO.File.Exists(fName) = True Then
                Dim objReader As New System.IO.StreamReader(fName, Encoding.ASCII)
                Dim index As Integer = 0
                Do While objReader.Peek() <> -1
                    If index > 0 Then
                        TextLine = objReader.ReadLine()
                        SplitLine = Split(TextLine, ",")
                        ' Console.WriteLine(SplitLine.Count)
                        dt.Rows.Add(SplitLine)
                    Else
                        LoadColumns(objReader.ReadLine(), dt)

                    End If
                    index = index + 1
                Loop
                AssertDataGridView.DataSource = Nothing
                AssertDataGridView.DataSource = dt
            Else
                MsgBox("File Does Not Exist")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub LoadColumns(ByVal headerRow As String, ByRef dt As DataTable)


        ' Dim TextFieldParser1 As New Microsoft.VisualBasic.FileIO.TextFieldParser(headerRow)

        ' TextFieldParser1.Delimiters = New String() {","}
        Dim newline() As String = Split(headerRow, ",")
        ' Dim SplitLine() As String


        'While Not TextFieldParser1.EndOfData
        Dim Row1 As String() = Split(headerRow, ",")

        If AssertDataGridView.Columns.Count = 0 AndAlso Row1.Count > 0 Then
            Dim i As Integer

            For i = 0 To Row1.Count - 1
                dt.Columns.Add(newline(i).ToUpper)

            Next

        End If

    End Sub

    Private Sub SaveGridDataInFile(ByRef fName As String)

        Dim I As Integer = 0
        Dim j As Integer = 0
        Dim cellvalue As String
        Dim rowLine As String = ""

        Try
            If IO.File.Exists(fName) Then
                System.IO.File.Delete(fName)
            End If
            Dim objWriter As New System.IO.StreamWriter(fName, True)

            Dim colString As String = Nothing
            Dim k As Integer = (AssertDataGridView.Columns.Count - 1)
            Dim l As Integer = 0
            For l = 0 To (AssertDataGridView.Columns.Count - 1)
                If k = l Then
                    colString += AssertDataGridView.Columns.Item(l).Name
                Else
                    colString += AssertDataGridView.Columns.Item(l).Name & ","
                End If
            Next

            objWriter.WriteLine(colString)

            For j = 0 To (AssertDataGridView.Rows.Count - 2)

                For I = 0 To (AssertDataGridView.Columns.Count - 1)
                    If Not TypeOf AssertDataGridView.CurrentRow.Cells.Item(I).Value Is DBNull Then
                        cellvalue = AssertDataGridView.Item(I, j).Value
                    Else
                        cellvalue = ""
                    End If
                    If I = (AssertDataGridView.Columns.Count - 1) Then
                        rowLine = rowLine + cellvalue
                    Else
                        rowLine = rowLine + cellvalue + ","
                    End If
                Next

                objWriter.WriteLine(rowLine)
                rowLine = ""

            Next

            objWriter.Close()

            MsgBox("Text written to file")

        Catch e As Exception

            MessageBox.Show("Error occured while writing to the file." + e.ToString())

        Finally

            FileClose(1)

        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles CreateAssertFileBtn.Click




        Dim rButton As RadioButton = GroupBox1.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked = True)

        If rButton Is Nothing Then
            MessageBox.Show("You must choose an Asserton Type!")
            Exit Sub
        End If

        Select Case rButton.Name

            Case StrToStrAssertion.Name
                dt = New DataTable

                dt.Columns.Add("REFID")
                dt.Columns.Add("@OID")
                dt.Columns.Add(GetAssertType)
                Dim TextLine As String
                Dim SplitLine() As String
                TextLine = "1,0RGA18FJ2HE0NWZM,1"
                SplitLine = Split(TextLine, ",")
                dt.Rows.Add(SplitLine)
                TextLine = "2,0RGHPQFVKLL4KD75,1"
                SplitLine = Split(TextLine, ",")
                dt.Rows.Add(SplitLine)

                AssertDataGridView.DataSource = Nothing
                AssertDataGridView.DataSource = dt
                AssertDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

                CreateAssertFileBtn.Enabled = False
                MsgBox("This is an example file only! Change as needed for your Assertion Run!")

            Case StrSplitAssertion.Name
                dt = New DataTable

                dt.Columns.Add("REFID")
                dt.Columns.Add("@RID")
                dt.Columns.Add("@OID")
                dt.Columns.Add(GetAssertType)

                Dim TextLine As String
                Dim SplitLine() As String
                TextLine = "1,School1.B946975,0RGA18FJ2HE0NWZM,1"
                SplitLine = Split(TextLine, ",")
                dt.Rows.Add(SplitLine)

                TextLine = "2,School1.B946974,0RGA18FJ2HE0NWZM,2"
                SplitLine = Split(TextLine, ",")
                ' Console.WriteLine(SplitLine.Count)
                dt.Rows.Add(SplitLine)

                AssertDataGridView.DataSource = Nothing
                AssertDataGridView.DataSource = dt
                CreateAssertFileBtn.Enabled = False

                AssertDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
                MsgBox("This is an example file only! Change as needed for your Assertion Run!")

                'Case ReferenceTransferAsserton.Name
                '    Console.WriteLine(rButton.Name)

                'Case TruePositiveAssertion.Name
                '    Console.WriteLine(rButton.Name)


                'Case TrueNegativelAssertion.Name
                '    Console.WriteLine(rButton.Name)
                'Case RefToRefAssertion.Name
                '    Console.WriteLine(rButton.Name)
                'Case RefToStrAssertion.Name
                '    Console.WriteLine(rButton.Name)
            Case Else
                MsgBox("Only StrToStrAssertion or StrSplitAssertion can be used here!")

                Exit Sub
        End Select



    End Sub

    Private Sub StrToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrToStrAssertion.CheckedChanged

        If StrToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFileBtn.Enabled = True

            MessageBox.Show("Use the Create Assertion Button to Create a Template!")
        End If

    End Sub

    Private Sub RefToStrAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToStrAssertion.CheckedChanged
        If RefToStrAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            MessageBox.Show("If you have a Reference File Load it to the DataGridView for use!")
        End If

    End Sub

    Private Sub StrSplitAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles StrSplitAssertion.CheckedChanged
        If StrSplitAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb
            CreateAssertFileBtn.Enabled = True
            MessageBox.Show("Use the Create Assertion Button to Create a Template!")
        End If

    End Sub

    Private Sub RefToRefAssertion_CheckedChanged(sender As Object, e As EventArgs) Handles RefToRefAssertion.CheckedChanged

        If RefToRefAssertion.Checked Then
            Dim rb As RadioButton = TryCast(sender, RadioButton)
            selectedrb = rb

            MessageBox.Show("If you have a Reference File Load it to the DataGridView for use!")
        End If



    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

        Dim assertType As String = Nothing
        assertType = GetAssertType()

        If Not String.IsNullOrEmpty(assertType) Then


            dt.Columns.Add(assertType)
        Else
            Exit Sub


        End If


        If dt IsNot Nothing Then

            For i As Integer = dt.Rows.Count - 2 To 0 Step -1

                dt.Rows.Remove(dt.Rows(i))

            Next
        Else
            MessageBox.Show("Data Table is empty!")

        End If



    End Sub


    Private Function GetAssertType() As String
        Dim rButton As RadioButton = GroupBox1.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked = True)

        If rButton Is Nothing Then
            MessageBox.Show("You must choose an Asserton Type!")
            Exit Function
        End If

        Dim AssertName As String = Nothing

        Select Case rButton.Name

            Case StrToStrAssertion.Name
                AssertName = “@AssertStrToStr”
                assertion = “@AssertStrToStr”
            Case StrSplitAssertion.Name
                AssertName = “@AssertSplitStr”
                assertion = “@AssertSplitStr”
            Case RefToRefAssertion.Name
                AssertName = "@AssertRefToRef"
                assertion = "@AssertRefToRef"
            Case RefToStrAssertion.Name
                AssertName = “@AssertRefToStr”
                '    assertion = “@AssertRefToStr”
                'Case ReferenceTransferAsserton.Name
                '    Console.WriteLine(rButton.Name)

                'Case TruePositiveAssertion.Name
                '    Console.WriteLine(rButton.Name)


                'Case TrueNegativelAssertion.Name
                '    Console.WriteLine(rButton.Name)
            Case Else
                MsgBox("Assertion Type Not Found")


        End Select

        Return AssertName

    End Function

    Private Sub CloseBtn_Click(sender As Object, e As EventArgs) Handles CloseBtn.Click
        Me.Dispose()

    End Sub

    Private Sub SaveAssertFileBtn_Click(sender As Object, e As EventArgs) Handles SaveAssertFileBtn.Click

        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.Filter = "CSV File|*.csv|TextFile|*.txt"
        saveFileDialog1.Title = "Save an Assertion File"
        saveFileDialog1.ShowDialog()


        ' If the file name is not an empty string open it for saving.  
        If saveFileDialog1.FileName <> "" Then
            SaveGridDataInFile(saveFileDialog1.FileName)
        End If


    End Sub
End Class
