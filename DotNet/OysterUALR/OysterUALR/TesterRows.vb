﻿''' <summary>
''' 
'''  Contains the testing rows from the TestHelper Class
'''  New tests will need to add rows as below to be included 
'''  in the output form for testing with results
'''  
''' </summary>

Public Class TesterRows


    Public Sub LoadTestDataGridView(ByVal key As Integer, ByVal PassNoPass As String)

        Dim row() As String = Nothing

        Select Case key


            Case 0
                row = {"Oyster Root Directory Input", "Try to put a Letter in the Oyster Root Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 1
                row = {"Oyster Root Directory Input", "Try to put a Space in the Oyster Root Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 2
                row = {"Oyster Root Directory Input", "Choose a Directory that is not the Oyster Root Directory.", "This must be the Oyster Root Directory Only!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 3
                row = {"Oyster Working Directory Input", "Try to put a Letter in the Oyster Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 4
                row = {"Oyster Working Directory Input", "Try to put a Space in the Oyster Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 5
                row = {"Oyster Working Directory Input", "Choose a Directory that is not the Oyster Working Directory.", "You must Choose a Work Directory under the Oyster Root!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 6
                row = {"Oyster Settings Save ", "Try to Save without all required Textboxes filled.", "Oyster Root Directory Working Directory and Oyster Jar File must all be entered to Save!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 7
                row = {"Oyster Jar to Run Input", "Try to put a Letter in the Oyster Jar Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 8
                row = {"Oyster Jar to Run Input", "Try to put a Space in the Oyster Jar Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 9
                row = {"Oyster Jar to Run Input", "Try to put a Wrong Jar in the Oyster Jar Directory TextBox.", "Your Choice must use a flavor of Oyster.jar! i.e. Oyster-3.6.4.jar", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 10
                row = {"Oyster Jar to Run Input", "Loading the Correct Oyster Jar will tell us to Save our Choices.", "Please Save your Choices!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 11
                row = {"Oyster Settings Save ", "Save the Choices of the User and it should let you now.", "Settings are now Saved for the next time you start the program.", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 12
                row = {"Oyster Run Script", "Select a wrong Run Script and click the Run Button.", "Your Oyster Working Directory is Wrong for this Run Script!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 13
                row = {"Oyster Run Script in Treeview", "Check for Correct Run Script in the Oyster Treeview.", "Run Script in Treeview is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 14
                row = {"Oyster Run Script In RichTextBox", "Check File that is loaded against Label Showing the Loaded Script.", "Label indicating Loaded File is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 15
                row = {"ERMetrics Working Directory Input", "Try to put a Letter in the ERMetrics Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 16
                row = {"ERMetrics Working Directory Input", "Try to put a Space in the ERMetrics Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 17
                row = {"ERMetrics Working Directory Input", "Choose a directory that is not the ERMetrics Working Directory.", "ErMetrics Working Directory must be Entered and Correct!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 18
                row = {"ERMetrics Tab Oyster Working Directory", "Verify the Oyster Working Directory in ERMetrics Tab.", "Loaded Oyster Working Directory in ERMetrics Tab is Correct", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 19
                row = {"ERMetrics Settings Save", "Try to Save ERMetrics without all Textboxes filled.", "ERMetrics Working Directory ERMetrics Jar and Oyster Working Directory must not be Empty!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 20
                row = {"ERMetrics Jar TextBox Input", "Try to put a Letter in the ERMetrics Jar TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 21
                row = {"ERMetrics Jar TextBox Input", "Try to put a Space in the ERMetrics Jar TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 22
                row = {"ERMetrics Jar TextBox Input", "Choose a jar that is not the ERMetrics Jar.", "Your chosen ERMetrics path must contain ER-Metrics.jar to Run!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 23
                row = {"ERMetrics Jar Textbox Input", "When Correct Jar is Loaded, it will ask you to Save your Choices.", "Please Save Your Choices", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 24
                row = {"ERMetrics Settings Save", "It will now let you Save your Choices.", "Settings are now Saved for the next time you start the program.", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 25
                row = {"ERMetrics.properties File", "Check that ERMetrics.properties file is in ERMetrics Treeview.", "TreeView ERMetrics.properties file is Present!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 26
                row = {"ERMetrics Properties File", "Check File loaded against Label Showing the Loaded Script.", "Label indicating Loaded File is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 27
                row = {"Oyster Run Stopped", "Run Oyster and then Stops It.", "This will Stop All Jave Processes on this Box, Yes", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 28
                row = {"ERMetrics Tab Link File", "Try to Copy the Link File from the Stopped Oyster Run.", "The Link File is Empty! Run Oyster again and let it finish!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 29
                row = {"Run Tab Labels", "Compare Run Script Name to Run Tab Run Script Label Value", "Run Tab Run Script File Label is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 30
                row = {"Run Tab Labels", "Compare Input Script to Run Tab Input File Label Value", "Run Tab Input File Label is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 31
                row = {"Run Tab Labels", "Verify the Oyster Link File in the Run Tab Link File Label is Correct.", "Run Tab Link File Label is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 32
                row = {"ERMetrics Tab Link File", "Verify the Link File from the Oyster Run in ERMetrics Tab is Correct.", "Link file in ERMetrics is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 33
                row = {"ERMetrics Tab Link File", "Copy the Link File from the Oyster Run.", "Link File Copied to ERMetrics working directory. Check your properties file if needed.", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 34
                row = {"ERMetrics Tab Link File Date", "Check the Link File Date in Oyster Run Tab.", "Link File Date is Correct in ERMetrics Tab!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 35
                row = {"Helpful Stuff Last Oyster CSV Input File", "Check the Last Oyster CSV Input File for Correctness.", "Last Oyster CSV Input File in Helpful Stuff Tab Is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 36
                row = {"Helpful Stuff Last Produced Oyster Link File", "Check the Last Produced Oyster Link File name.", "Last Produced Oyster Link File in Helpful Stuff Is Correct!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 37
                row = {"Helpful Stuff Create New CVS File", "Check that the CSV File was Created.", "New CSV File was Created!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 38
                row = {"Helpful Stuff Create New Reference File", "Check that the Reference File was Created.", "New Reference File was Created!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 39
                row = {"Helpful Stuff Create New BootStrap Weight File", "Check that the BootStrap Weight File was Created.", "New BootStrap Weight File was Created!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 40
                row = {"Oyster Robot Settings Save ", "Try to Save without any required Textboxes filled.", "Python Directory and Robot Working Directory must not be Empty!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 41
                row = {"Oyster Robot Run ", "Try to Run the Robot without any required Textboxes filled.", "Python Directory and Robot Working Directory must not be Empty!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 42
                row = {"Python Exe Directory Input", "Try to put a Letter in the Python Exe Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 43
                row = {"Python Exe Directory Input", "Try to put a Space in the Python Exe Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 44
                row = {"Python Exe Directory Input", "Choose a Directory that is not the Python Exe Directory.", "This must be the Python Exe Directory Only!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 45
                row = {"Oyster Robot Settings Save ", "Try to Save without any required Textboxes filled.", "Python Directory and Robot Working Directory must not be Empty!", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 46
                row = {"Robot Working Directory Input", "Try to put a Letter in the Robot Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 47
                row = {"Robot Working Directory Input", "Try to put a Space in the Robot Working Directory TextBox.", "Please use the Browse Button to fill the Textbox", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 48
                row = {"Robot Working Directory Input", "Choose a Directory that is not the Robot Working Directory.", "You must Choose the Robot Work Directory!", PassNoPass}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
            Case 49
                row = {"Robot Settings Save ", "Save the Choices of the User and it should let you now.", "Settings are now Saved for the next time you start the program.", PassNoPass.ToString}
                TestOutPutForm.TestingDataGridView.Rows.Add(row)
                'Case 50
                '    row = {"Working with Oyster Files Tab", "Check that Oyster Link file is in Treeview.", "Oyster Link File is in Treeview!", PassNoPass.ToString}
                '    TestOutPutForm.TestingDataGridView.Rows.Add(row)
                'Case 51
                '    row = {"Working with Oyster Files Tab", "Check that Oyster Link file loaded is in RichTextBox and label agrees.", "Oyster Link File is Loaded and Label for loaded file agrees!", PassNoPass.ToString}
                '    TestOutPutForm.TestingDataGridView.Rows.Add(row)
                'Case 52
                '    row = {"Oyster Working Directory", "Change Oyster Working Directory and Verify Change in ERMetrics Tab.", "Loaded Oyster Working Directory in ERMetrics Tab is Correct", PassNoPass.ToString}
                '    TestOutPutForm.TestingDataGridView.Rows.Add(row)

        End Select


    End Sub









End Class
