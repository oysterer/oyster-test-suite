﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestJarForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestJarForm))
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.OysterRootDirTxBx = New System.Windows.Forms.TextBox()
        Me.OysterWorkDirTxBx = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.OysterJarLbl = New System.Windows.Forms.Label()
        Me.OysterJarTxBx = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.NewJarTxBx = New System.Windows.Forms.TextBox()
        Me.LoadNewJarBtn = New System.Windows.Forms.Button()
        Me.InstructionOysterRhTxbx = New System.Windows.Forms.RichTextBox()
        Me.Exitbtn = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.RunJarTestBtn = New System.Windows.Forms.Button()
        Me.SaveJarBtn = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LinkfileERMetricsLbl = New System.Windows.Forms.Label()
        Me.LinkfileLBL = New System.Windows.Forms.Label()
        Me.RefreshBtn = New System.Windows.Forms.Button()
        Me.ChgJarBtn = New System.Windows.Forms.Button()
        Me.CurrentEngineLbl = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.OldAndNewJarRB = New System.Windows.Forms.RadioButton()
        Me.NewJarRB = New System.Windows.Forms.RadioButton()
        Me.OldJarRB = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BothEngRB = New System.Windows.Forms.RadioButton()
        Me.RSwooshStandardRB = New System.Windows.Forms.RadioButton()
        Me.FSClusterRB = New System.Windows.Forms.RadioButton()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrorInRunLbl = New System.Windows.Forms.Label()
        Me.LinkFileChgBtn = New System.Windows.Forms.Button()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox7
        '
        Me.PictureBox7.ErrorImage = CType(resources.GetObject("PictureBox7.ErrorImage"), System.Drawing.Image)
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(798, 24)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox7.TabIndex = 108
        Me.PictureBox7.TabStop = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(26, 39)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(139, 16)
        Me.Label17.TabIndex = 120
        Me.Label17.Text = "Oyster Root Directory:"
        '
        'OysterRootDirTxBx
        '
        Me.OysterRootDirTxBx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterRootDirTxBx.Location = New System.Drawing.Point(27, 64)
        Me.OysterRootDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterRootDirTxBx.Name = "OysterRootDirTxBx"
        Me.OysterRootDirTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterRootDirTxBx.Size = New System.Drawing.Size(347, 22)
        Me.OysterRootDirTxBx.TabIndex = 109
        '
        'OysterWorkDirTxBx
        '
        Me.OysterWorkDirTxBx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterWorkDirTxBx.Location = New System.Drawing.Point(27, 141)
        Me.OysterWorkDirTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterWorkDirTxBx.Name = "OysterWorkDirTxBx"
        Me.OysterWorkDirTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterWorkDirTxBx.Size = New System.Drawing.Size(347, 22)
        Me.OysterWorkDirTxBx.TabIndex = 111
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(26, 116)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(171, 17)
        Me.Label8.TabIndex = 113
        Me.Label8.Text = "Oyster Working Directory:"
        '
        'OysterJarLbl
        '
        Me.OysterJarLbl.AutoSize = True
        Me.OysterJarLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterJarLbl.ForeColor = System.Drawing.Color.White
        Me.OysterJarLbl.Location = New System.Drawing.Point(26, 194)
        Me.OysterJarLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.OysterJarLbl.Name = "OysterJarLbl"
        Me.OysterJarLbl.Size = New System.Drawing.Size(244, 17)
        Me.OysterJarLbl.TabIndex = 118
        Me.OysterJarLbl.Text = "Old Oyster Jar to Run that is Loaded:"
        '
        'OysterJarTxBx
        '
        Me.OysterJarTxBx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OysterJarTxBx.Location = New System.Drawing.Point(27, 218)
        Me.OysterJarTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.OysterJarTxBx.Name = "OysterJarTxBx"
        Me.OysterJarTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.OysterJarTxBx.Size = New System.Drawing.Size(347, 22)
        Me.OysterJarTxBx.TabIndex = 114
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(26, 271)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 17)
        Me.Label7.TabIndex = 130
        Me.Label7.Text = "New Jar to Run:"
        '
        'NewJarTxBx
        '
        Me.NewJarTxBx.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NewJarTxBx.Location = New System.Drawing.Point(27, 295)
        Me.NewJarTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.NewJarTxBx.Name = "NewJarTxBx"
        Me.NewJarTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.NewJarTxBx.Size = New System.Drawing.Size(347, 22)
        Me.NewJarTxBx.TabIndex = 126
        '
        'LoadNewJarBtn
        '
        Me.LoadNewJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.LoadNewJarBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoadNewJarBtn.Location = New System.Drawing.Point(391, 292)
        Me.LoadNewJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.LoadNewJarBtn.Name = "LoadNewJarBtn"
        Me.LoadNewJarBtn.Size = New System.Drawing.Size(70, 30)
        Me.LoadNewJarBtn.TabIndex = 127
        Me.LoadNewJarBtn.Text = "Browse"
        Me.LoadNewJarBtn.UseVisualStyleBackColor = False
        '
        'InstructionOysterRhTxbx
        '
        Me.InstructionOysterRhTxbx.BackColor = System.Drawing.Color.Maroon
        Me.InstructionOysterRhTxbx.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstructionOysterRhTxbx.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstructionOysterRhTxbx.ForeColor = System.Drawing.Color.White
        Me.InstructionOysterRhTxbx.Location = New System.Drawing.Point(16, 43)
        Me.InstructionOysterRhTxbx.Margin = New System.Windows.Forms.Padding(2)
        Me.InstructionOysterRhTxbx.Name = "InstructionOysterRhTxbx"
        Me.InstructionOysterRhTxbx.ReadOnly = True
        Me.InstructionOysterRhTxbx.Size = New System.Drawing.Size(763, 132)
        Me.InstructionOysterRhTxbx.TabIndex = 133
        Me.InstructionOysterRhTxbx.Text = resources.GetString("InstructionOysterRhTxbx.Text")
        '
        'Exitbtn
        '
        Me.Exitbtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Exitbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Exitbtn.ForeColor = System.Drawing.Color.Black
        Me.Exitbtn.Location = New System.Drawing.Point(798, 654)
        Me.Exitbtn.Margin = New System.Windows.Forms.Padding(2)
        Me.Exitbtn.Name = "Exitbtn"
        Me.Exitbtn.Size = New System.Drawing.Size(70, 30)
        Me.Exitbtn.TabIndex = 134
        Me.Exitbtn.Text = "Close"
        Me.Exitbtn.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'RunJarTestBtn
        '
        Me.RunJarTestBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RunJarTestBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RunJarTestBtn.ForeColor = System.Drawing.Color.Maroon
        Me.RunJarTestBtn.Location = New System.Drawing.Point(693, 654)
        Me.RunJarTestBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RunJarTestBtn.Name = "RunJarTestBtn"
        Me.RunJarTestBtn.Size = New System.Drawing.Size(70, 30)
        Me.RunJarTestBtn.TabIndex = 135
        Me.RunJarTestBtn.Text = "Run"
        Me.RunJarTestBtn.UseVisualStyleBackColor = False
        '
        'SaveJarBtn
        '
        Me.SaveJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.SaveJarBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveJarBtn.Location = New System.Drawing.Point(303, 335)
        Me.SaveJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.SaveJarBtn.Name = "SaveJarBtn"
        Me.SaveJarBtn.Size = New System.Drawing.Size(70, 30)
        Me.SaveJarBtn.TabIndex = 136
        Me.SaveJarBtn.Text = "Save"
        Me.SaveJarBtn.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Maroon
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.LinkfileERMetricsLbl)
        Me.Panel1.Controls.Add(Me.LinkfileLBL)
        Me.Panel1.Controls.Add(Me.RefreshBtn)
        Me.Panel1.Controls.Add(Me.ChgJarBtn)
        Me.Panel1.Controls.Add(Me.CurrentEngineLbl)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.SaveJarBtn)
        Me.Panel1.Controls.Add(Me.OysterRootDirTxBx)
        Me.Panel1.Controls.Add(Me.OysterJarTxBx)
        Me.Panel1.Controls.Add(Me.OysterJarLbl)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.OysterWorkDirTxBx)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.LoadNewJarBtn)
        Me.Panel1.Controls.Add(Me.NewJarTxBx)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(16, 192)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(861, 442)
        Me.Panel1.TabIndex = 140
        '
        'LinkfileERMetricsLbl
        '
        Me.LinkfileERMetricsLbl.AutoSize = True
        Me.LinkfileERMetricsLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkfileERMetricsLbl.ForeColor = System.Drawing.Color.White
        Me.LinkfileERMetricsLbl.Location = New System.Drawing.Point(34, 409)
        Me.LinkfileERMetricsLbl.Name = "LinkfileERMetricsLbl"
        Me.LinkfileERMetricsLbl.Size = New System.Drawing.Size(122, 16)
        Me.LinkfileERMetricsLbl.TabIndex = 161
        Me.LinkfileERMetricsLbl.Text = "ERMetrics LinkFile:"
        '
        'LinkfileLBL
        '
        Me.LinkfileLBL.AutoSize = True
        Me.LinkfileLBL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkfileLBL.ForeColor = System.Drawing.Color.White
        Me.LinkfileLBL.Location = New System.Drawing.Point(34, 381)
        Me.LinkfileLBL.Name = "LinkfileLBL"
        Me.LinkfileLBL.Size = New System.Drawing.Size(121, 16)
        Me.LinkfileLBL.TabIndex = 160
        Me.LinkfileLBL.Text = "RunScript  LinkFile:"
        '
        'RefreshBtn
        '
        Me.RefreshBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RefreshBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshBtn.Location = New System.Drawing.Point(304, 254)
        Me.RefreshBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.RefreshBtn.Name = "RefreshBtn"
        Me.RefreshBtn.Size = New System.Drawing.Size(70, 30)
        Me.RefreshBtn.TabIndex = 159
        Me.RefreshBtn.Text = "Refresh"
        Me.RefreshBtn.UseVisualStyleBackColor = False
        '
        'ChgJarBtn
        '
        Me.ChgJarBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ChgJarBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChgJarBtn.Location = New System.Drawing.Point(391, 214)
        Me.ChgJarBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.ChgJarBtn.Name = "ChgJarBtn"
        Me.ChgJarBtn.Size = New System.Drawing.Size(70, 30)
        Me.ChgJarBtn.TabIndex = 158
        Me.ChgJarBtn.Text = "Change"
        Me.ChgJarBtn.UseVisualStyleBackColor = False
        '
        'CurrentEngineLbl
        '
        Me.CurrentEngineLbl.AutoSize = True
        Me.CurrentEngineLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CurrentEngineLbl.ForeColor = System.Drawing.Color.White
        Me.CurrentEngineLbl.Location = New System.Drawing.Point(475, 25)
        Me.CurrentEngineLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.CurrentEngineLbl.Name = "CurrentEngineLbl"
        Me.CurrentEngineLbl.Size = New System.Drawing.Size(145, 18)
        Me.CurrentEngineLbl.TabIndex = 157
        Me.CurrentEngineLbl.Text = "Loaded Engine Now:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.OldAndNewJarRB)
        Me.GroupBox2.Controls.Add(Me.NewJarRB)
        Me.GroupBox2.Controls.Add(Me.OldJarRB)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(478, 217)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(307, 125)
        Me.GroupBox2.TabIndex = 154
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Jar Choice"
        '
        'OldAndNewJarRB
        '
        Me.OldAndNewJarRB.AutoSize = True
        Me.OldAndNewJarRB.Location = New System.Drawing.Point(22, 89)
        Me.OldAndNewJarRB.Name = "OldAndNewJarRB"
        Me.OldAndNewJarRB.Size = New System.Drawing.Size(136, 22)
        Me.OldAndNewJarRB.TabIndex = 155
        Me.OldAndNewJarRB.TabStop = True
        Me.OldAndNewJarRB.Text = "Old and New Jar"
        Me.OldAndNewJarRB.UseVisualStyleBackColor = True
        '
        'NewJarRB
        '
        Me.NewJarRB.AutoSize = True
        Me.NewJarRB.Location = New System.Drawing.Point(22, 61)
        Me.NewJarRB.Name = "NewJarRB"
        Me.NewJarRB.Size = New System.Drawing.Size(81, 22)
        Me.NewJarRB.TabIndex = 154
        Me.NewJarRB.TabStop = True
        Me.NewJarRB.Text = "New Jar"
        Me.NewJarRB.UseVisualStyleBackColor = True
        '
        'OldJarRB
        '
        Me.OldJarRB.AutoSize = True
        Me.OldJarRB.Location = New System.Drawing.Point(22, 32)
        Me.OldJarRB.Name = "OldJarRB"
        Me.OldJarRB.Size = New System.Drawing.Size(74, 22)
        Me.OldJarRB.TabIndex = 153
        Me.OldJarRB.TabStop = True
        Me.OldJarRB.Text = "Old Jar"
        Me.OldJarRB.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BothEngRB)
        Me.GroupBox1.Controls.Add(Me.RSwooshStandardRB)
        Me.GroupBox1.Controls.Add(Me.FSClusterRB)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(478, 61)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(307, 125)
        Me.GroupBox1.TabIndex = 153
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Engine Choice"
        '
        'BothEngRB
        '
        Me.BothEngRB.AutoSize = True
        Me.BothEngRB.Location = New System.Drawing.Point(22, 86)
        Me.BothEngRB.Name = "BothEngRB"
        Me.BothEngRB.Size = New System.Drawing.Size(249, 22)
        Me.BothEngRB.TabIndex = 153
        Me.BothEngRB.TabStop = True
        Me.BothEngRB.Text = "FSCluster and RSwooshStandard"
        Me.BothEngRB.UseVisualStyleBackColor = True
        '
        'RSwooshStandardRB
        '
        Me.RSwooshStandardRB.AutoSize = True
        Me.RSwooshStandardRB.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RSwooshStandardRB.Location = New System.Drawing.Point(22, 56)
        Me.RSwooshStandardRB.Name = "RSwooshStandardRB"
        Me.RSwooshStandardRB.Size = New System.Drawing.Size(151, 22)
        Me.RSwooshStandardRB.TabIndex = 152
        Me.RSwooshStandardRB.TabStop = True
        Me.RSwooshStandardRB.Text = "RSwooshStandard"
        Me.RSwooshStandardRB.UseVisualStyleBackColor = True
        '
        'FSClusterRB
        '
        Me.FSClusterRB.AutoSize = True
        Me.FSClusterRB.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FSClusterRB.Location = New System.Drawing.Point(22, 26)
        Me.FSClusterRB.Name = "FSClusterRB"
        Me.FSClusterRB.Size = New System.Drawing.Size(92, 22)
        Me.FSClusterRB.TabIndex = 151
        Me.FSClusterRB.TabStop = True
        Me.FSClusterRB.Text = "FSCluster"
        Me.FSClusterRB.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.CloseToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(894, 25)
        Me.MenuStrip1.TabIndex = 141
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(39, 21)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(52, 21)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'ErrorInRunLbl
        '
        Me.ErrorInRunLbl.AutoSize = True
        Me.ErrorInRunLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ErrorInRunLbl.ForeColor = System.Drawing.Color.Red
        Me.ErrorInRunLbl.Location = New System.Drawing.Point(140, 658)
        Me.ErrorInRunLbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ErrorInRunLbl.Name = "ErrorInRunLbl"
        Me.ErrorInRunLbl.Size = New System.Drawing.Size(210, 20)
        Me.ErrorInRunLbl.TabIndex = 142
        Me.ErrorInRunLbl.Text = "LinkFiles Do Not Match!!!"
        Me.ErrorInRunLbl.Visible = False
        '
        'LinkFileChgBtn
        '
        Me.LinkFileChgBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.LinkFileChgBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkFileChgBtn.Location = New System.Drawing.Point(409, 654)
        Me.LinkFileChgBtn.Margin = New System.Windows.Forms.Padding(2)
        Me.LinkFileChgBtn.Name = "LinkFileChgBtn"
        Me.LinkFileChgBtn.Size = New System.Drawing.Size(70, 30)
        Me.LinkFileChgBtn.TabIndex = 159
        Me.LinkFileChgBtn.Text = "Change"
        Me.LinkFileChgBtn.UseVisualStyleBackColor = False
        '
        'TestJarForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Maroon
        Me.ClientSize = New System.Drawing.Size(894, 708)
        Me.Controls.Add(Me.LinkFileChgBtn)
        Me.Controls.Add(Me.ErrorInRunLbl)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.InstructionOysterRhTxbx)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.RunJarTestBtn)
        Me.Controls.Add(Me.Exitbtn)
        Me.Controls.Add(Me.MenuStrip1)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "TestJarForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Oyster Helper Jar Testing"
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents Label17 As Label
    Friend WithEvents OysterRootDirTxBx As TextBox
    Friend WithEvents OysterWorkDirTxBx As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents OysterJarLbl As Label
    Friend WithEvents OysterJarTxBx As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents NewJarTxBx As TextBox
    Friend WithEvents LoadNewJarBtn As Button
    Friend WithEvents InstructionOysterRhTxbx As RichTextBox
    Friend WithEvents Exitbtn As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents RunJarTestBtn As Button
    Friend WithEvents SaveJarBtn As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents OldAndNewJarRB As RadioButton
    Friend WithEvents NewJarRB As RadioButton
    Friend WithEvents OldJarRB As RadioButton
    Friend WithEvents BothEngRB As RadioButton
    Friend WithEvents RSwooshStandardRB As RadioButton
    Friend WithEvents FSClusterRB As RadioButton
    Friend WithEvents CurrentEngineLbl As Label
    Friend WithEvents ChgJarBtn As Button
    Friend WithEvents RefreshBtn As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CloseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LinkfileLBL As Label
    Friend WithEvents LinkfileERMetricsLbl As Label
    Friend WithEvents ErrorInRunLbl As Label
    Friend WithEvents LinkFileChgBtn As Button
End Class
