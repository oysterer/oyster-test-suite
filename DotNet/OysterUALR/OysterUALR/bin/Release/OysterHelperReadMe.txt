

Welcome to Oyster Helper

Oyster Helper is a program to help UALR users with running Oyster and ERMetrics.
It is a .NET program that has been made to run on Windows 10.  It may 
very well run on other Window versions but has not been tested at this time.

You must have the complete Oyster Demos Directory with all the working directories on 
your Machine to use the program.  It does not supply this. Dr. Talburt can help 
if you do not have it already.  It is recommended to make a C:\Oyster directory and 
copy the working directories and root files under it for simplicity. Java 8 must 
be installed on your machine also.

The OysterGUI.zip includes a OysterGui.msi file and a setup.exe file. The program 
should be installed with the MSI file.  It will default to C:\Program Files (x86)\OysterHelper
directory. I have changed the install directory and it installs and the shortcus work fine.
Installation may be for just me or anyone if chosen.  Only the option of just me 
has been tested at this time.  This is being mentioned because one of the things Oyster 
Helper does is save your choices for most textboxes for reloading the next time you 
use the program and it saves these choices for the just me option in the following 
directory: C:\Users\WindowsUserName\AppData\Local\OysterHelper. This information can 
be deleted to start fresh with the program being empty on startup.  This is helpful 
for testing of the program by users.  It is also nice that it keeps your chosen information 
for the next time you use the program. The MSI install will create a shortcut Icon in 
the Windows Start Directory and on the Desktop.  You can remove the program through 
Add and Remove programs in Windows System settings.

With the proper choice of the Oyster Root Directory (c:\Oyster is preferred) and a 
working directory (like c:\Oyster\ListA) and the choice of an Oyster jar to use (like 
Oyster oyster-3.6.4.jar) and saving your choices, you can run Oyster from the program. 
Of course, you have to choose a runscript, edit your Atributes.xml,SourceScript.xml and 
RunScript.xml to run the way you want them to. Some editing is available by loading the 
files from the Treeview to the RichTextBox for editing.  Some useful right click 
commands are available once you choose the edit button to make the file ready to edit.
You will need to save your changes before the run. 

ERMetrics is basically the same.  Load the root ER_Calculator directory and the 
er-metrics jar and save your choices and you can edit the properties file and run 
ERMetrics from the program. You also can copy the last Oyster run output link file to 
the ERMetrics root directory with a click.

When you click either of the run buttons, the Run Output Tab is automatically selected 
so you can watch the runs and see the outputs from the runs. You can stop the runs from 
that screen also.  Any errors should automatically kill the running program. You can 
do other things while it is running but do not change any files being used in the run.
Helpful stuff uses output from the current/last run and should not be used while the 
program is running. Oyster Linkfiles will be empty until the run completes and will 
be empty if you stop the Oyster run with the Stop Button while it is running. The 
stop button will kill ALL java processes on the machine.

The program has another Tab to work on any of the files in the Oyster Directory and 
another Tab called Helpful Stuff.  Helpful Stuff can create three files from the last 
Oyster run.  A new input file that is created from the input file from the last run 
and the link file from the last run. It creates a file that has all records not 
linked by any of the rules in the last run.  This file is helpful when you are to the 
point of linking all records you think you can with your rules and want to work only 
on those records that you have not linked yet while making new rules to run against 
this new input file.

Also, in Helpful Stuff is a way to make a reference file from the last run that can be 
used by ERMetrics as a reference file when you do not have a truth file to check 
your progress with.  It is not perfect and is not a truth file but it may be useful.
There is no real substitue for a truth file.

Oyster Helper provides editing of some files, but nothing like NotePad++.  It is not 
designed to be a total text editor but,it does not put any extra characters that cause 
issues in Oyster runs.  You can comment and uncomment rules and indexes but it is not 
in anyway perfect, only convenient. Major changes of files is much better in NotePad++.
If you edit files outside of Oyster Helper while you are in the program, just reload 
the changed file into the RichTextBox from the Treeview and you will see your file 
with the new changes and saved outside the program.  This makes it easy to 
edit in Notepad++ and quickly see the changes in Oyster Helper for your next run.

Not enough can be said about the directory structure of each working directory and the file 
structure of each file, especially the xml files used in the program. The naming structure 
of these files is very important and should remain constant. Oyster Helper is designed
around these constant structures and names. When creating a new working directory, please 
copy and change one of the existing directories and change its files for your new working 
directory, maintaining the directory structure,the existing xml file structure within the 
files and the name of the files used in Oyster. For example, each Source.xml file is named 
the same "WorkingdirectorySourceDescriptor.xml". For work directory ListA for example, the 
source file is ListASourceDescriptor.xml and so on with other xml files needed in its 
permanent directory and naming structure. This constant structure is one of the great 
designs of the system and needed for Oyster Helper and Oyster to run at this time.

Over time more functionality has been added to Oyster Helper.  In program help has been 
added for most new tabs.