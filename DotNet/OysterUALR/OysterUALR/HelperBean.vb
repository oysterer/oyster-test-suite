﻿''' <summary>
''' 
''' A basic javabean and useful to store and get information
''' modify as needed
''' 
''' </summary>

Public Class HelperBean


    'used to hold richboxname and file loaded for saving
    Private RichTxBoxName As String
    Private RichTxBoxFileLoaded As String
    Private RichTextName As Boolean
    'used to keep track of information
    Private OysterRunScript As String
    Private OysterRunTime As String
    'Helpfull Stuff textboxes
    Private InputCSVFilePath As String
    Private OutPutCSVFilePath As String
    Private LinkFileFilePath As String
    Private OutPutRefFilePath As String
    Private CBStrapFilePath As String

    'Next three attributes for Truthfile Creation
    Private RefIds As Collection
    Private OysterIdTotalCount As Integer
    Private OysterID As String

    'true means create new run file false means
    'create possible truth/Reference file in helpful stuff
    Private CSVorReforCBSFile As Integer



    Private ChartHighestValue As Integer
    Private ChartLowesttValue As Integer

    ' Searching IKB position
    Private iFoundPos As Integer = 0
    Private IKBSearchFileName As String = ""
    'IKB Management
    Private DataGridTable As DataTable
    Private Dictionary As Dictionary(Of String, String)
    Private Bsource As BindingSource

    'these refernces and methods not used at this time
    'Private Analyze As Boolean = False
    'others not use d at this time
    'Private OysterRunCount As Integer

    Private FileToLoad As String

    Private ERMetricsJarTxtBx As String
    'Private OysterLinkFile As String
    'Private OysterInputFile As String
    Private OysterJarFile As String
    Private ERMetricsJarFile As String

    Public Property CBStrapFilePathOutPut() As String
        Get
            Return CBStrapFilePath
        End Get
        Set(ByVal value As String)
            CBStrapFilePath = value
        End Set
    End Property
    Public Property OutPutRefFilePathOutPut() As String
        Get
            Return OutPutRefFilePath
        End Get
        Set(ByVal value As String)
            OutPutRefFilePath = value
        End Set
    End Property

    Public Property KBSearchLoadedFileName() As String
        Get
            Return IKBSearchFileName
        End Get
        Set(ByVal value As String)
            IKBSearchFileName = value
        End Set
    End Property


    Public Property ChartHighValue() As Integer
        Get
            Return ChartHighestValue
        End Get
        Set(ByVal value As Integer)
            ChartHighestValue = value
        End Set
    End Property


    Public Property ChartLowValue() As Integer
        Get
            Return ChartLowesttValue
        End Get
        Set(ByVal value As Integer)
            ChartLowesttValue = value
        End Set
    End Property

    Public Property SearchPositionNow() As Integer
        Get
            Return iFoundPos
        End Get
        Set(ByVal value As Integer)
            iFoundPos = value
        End Set
    End Property

    Public Property BindingSource() As BindingSource
        Get
            Return Bsource
        End Get
        Set(ByVal value As BindingSource)
            Bsource = value
        End Set
    End Property

    Public Property RefId() As Collection
        Get
            Return RefIds
        End Get
        Set(ByVal value As Collection)
            RefIds = value
        End Set
    End Property

    Public Property OysterIdCount() As Integer
        Get
            Return OysterIdTotalCount
        End Get
        Set(ByVal value As Integer)
            OysterIdTotalCount = value
        End Set
    End Property

    Public Property OysterIDValue() As String
        Get
            Return OysterID
        End Get
        Set(ByVal value As String)
            OysterID = value
        End Set
    End Property

    Public Property RichTexBoxNameBool() As Boolean
        Get
            Return RichTextName
        End Get
        Set(ByVal value As Boolean)
            RichTextName = value
        End Set
    End Property

    Public Property CSVorReforCBSFileCreation() As Integer
        Get
            Return CSVorReforCBSFile
        End Get
        Set(ByVal value As Integer)
            CSVorReforCBSFile = value
        End Set
    End Property

    Public Property InputCSVFileAndPath() As String
        Get
            Return InputCSVFilePath
        End Get
        Set(ByVal value As String)
            InputCSVFilePath = value
        End Set
    End Property

    Public Property OutPutCSVFileAndPath() As String
        Get
            Return OutPutCSVFilePath
        End Get
        Set(ByVal value As String)
            OutPutCSVFilePath = value
        End Set
    End Property

    Public Property LinkFileFileAndPath() As String
        Get
            Return LinkFileFilePath
        End Get
        Set(ByVal value As String)
            LinkFileFilePath = value
        End Set
    End Property

    Public Property OysterRunTimeValue() As String
        Get
            Return OysterRunTime
        End Get
        Set(ByVal value As String)
            OysterRunTime = value
        End Set
    End Property


    Public Property OysterRunScriptName() As String
        Get
            Return OysterRunScript
        End Get
        Set(ByVal value As String)
            OysterRunScript = value
        End Set
    End Property

    Public Property FileLoaded() As String
        Get
            Return RichTxBoxFileLoaded
        End Get
        Set(ByVal value As String)
            RichTxBoxFileLoaded = value
        End Set
    End Property

    Public Property RichTextBoxName() As String
        Get
            Return RichTxBoxName
        End Get
        Set(ByVal value As String)
            RichTxBoxName = value
        End Set
    End Property


    'these not used at this time

    Public Property ERMetricsJarTxtBxValue() As String
        Get
            Return ERMetricsJarTxtBx
        End Get
        Set(ByVal value As String)
            ERMetricsJarTxtBx = value
        End Set
    End Property

    Public Property DataGridViewTable() As DataTable
        Get
            Return DataGridTable
        End Get
        Set(ByVal value As DataTable)
            DataGridTable = value
        End Set
    End Property



    'Public Property OysterRunCountValue() As Integer
    '    Get
    '        Return OysterRunCount
    '    End Get
    '    Set(ByVal value As Integer)
    '        OysterRunCount = value
    '    End Set
    'End Property

    Public Property ERMetricsJarName() As String
        Get
            Return ERMetricsJarFile
        End Get
        Set(ByVal value As String)
            ERMetricsJarFile = value
        End Set
    End Property

    Public Property OysterJarFileFileName() As String
        Get
            Return OysterJarFile
        End Get
        Set(ByVal value As String)
            OysterJarFile = value
        End Set
    End Property

    Public Property IKBFileToLoad As String
        Get
            Return FileToLoad
        End Get
        Set(ByVal value As String)
            FileToLoad = value
        End Set
    End Property

    'Public Property OysterInputFileName() As String
    '    Get
    '        Return OysterInputFile
    '    End Get
    '    Set(ByVal value As String)
    '        OysterInputFile = value
    '    End Set
    'End Property

    Public Property IKBMDictionary() As Dictionary(Of String, String)
        Get
            Return Dictionary
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            Dictionary = value
        End Set
    End Property

    'Public Property AnalyzeRun() As Boolean
    '    Get
    '        Return Analyze
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Analyze = value
    '    End Set
    'End Property


    'Public Property OysterLinkFileName() As String
    '    Get
    '        Return OysterLinkFile
    '    End Get
    '    Set(ByVal value As String)
    '        OysterLinkFile = value
    '    End Set
    'End Property


End Class
