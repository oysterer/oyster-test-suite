﻿

Imports System.IO
''' <summary>
''' 
''' ProcessClass.vb
''' 
''' This class starts Oyster in a command window, passes the Runscript and records the output to the run output tab in Oyster Helper.  
''' Oyster writes the same information to an output file also. ERMetrics is also run with this file and writes to the output Tab but no
''' parameters are passed to it.  
''' Class throws to the calling method
''' </summary>
''' 
Public Class ProcessClass

    ''' <summary>
    ''' This method runs a command window and starts the Oyster jar or ERmetrics jar file in a command window. It redirects the output 
    ''' through handlers that write to a richtextbox on the Oysterform in the RUN Output Tab.
    ''' <param name="path">
    ''' The root directory of Oyster or ERMetrics
    ''' </param>
    ''' <param name="theFileToRun">
    ''' The xml run file to run in oyster only otherwise null
    ''' </param>
    ''' ''' <param name="oyster">
    ''' Boolean value for either oyster run or ermetrics run
    ''' </param>
    ''' ''' <param name="jarToUse">
    ''' oyster jar or ermetrics jar to use
    ''' </param>
    ''' </summary>
    ''' 
    '''
    ''' 

    Public Sub Run(ByVal path As String, ByVal theFileToRun As String, ByVal oyster As Boolean, ByVal jarToUse As String, ByVal helperBean As HelperBean)

        Dim myPath As String = path
        Dim fileToRun As String = theFileToRun
        Dim jarForUse As String = jarToUse
        Dim oysterBoolean As Boolean = oyster

        Dim p As Process = Nothing

        Try
            p = New Process
            '       Cursor.Current = Cursors.WaitCursor
            Oysterform.OysterTabs.SelectedTab = Oysterform.Run
            Oysterform.RunRichTextBox.Clear()


            Dim info As New ProcessStartInfo With {
            .UseShellExecute = False,
            .CreateNoWindow = True,
            .WindowStyle = ProcessWindowStyle.Normal,
            .RedirectStandardError = True,
            .RedirectStandardInput = True,
            .RedirectStandardOutput = True,
            .WorkingDirectory = path,
            .FileName = Constants.CmdWindow
          }

            p = Process.Start(info)
            AddHandler p.OutputDataReceived, AddressOf Oysterform.ConsoleOutputHandler
            AddHandler p.ErrorDataReceived, AddressOf Oysterform.ConsoleErrorHandler

            p.BeginErrorReadLine()
            p.BeginOutputReadLine()
            p.StandardInput.WriteLine("cd " & myPath & " " & vbCrLf)
            '  p.StandardInput.WriteLine("set JAVA_OPTS")


            '    If oysterBoolean Then 'run oyster
            Oysterform.Timer1.Interval = 1000
            'reset and start the stopwatch to get runtime to show on Run Output tab
            Oysterform.stopWatch.Reset()
            Oysterform.stopWatch.Start()
            Oysterform.Timer1.Enabled = True
            Oysterform.RunTimeValuelbl.Text = Now.ToLongTimeString
            Oysterform.RunERMetricsBtn.Enabled = False
            p.StandardInput.WriteLine("java -jar " & jarForUse & " " & vbCrLf)
            p.StandardInput.WriteLine(fileToRun & " " & vbCrLf)
            Oysterform.OysterRunning = True

            '   -Xms256m -Xmx2048m
        Catch ex As Exception
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False
            Oysterform.StopRun.Enabled = False
            Oysterform.RunERMetricsBtn.Enabled = True
            MessageBox.Show("Error in running Oyster is " & ex.Message, "Run Oyster Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            'Dim processthreadcol As ProcessThreadCollection = p.Threads
            'Dim i As Integer = 0
            'For Each t As ProcessThread In processthreadcol
            '    i = t.ThreadState
            'Next


            Try
                If p IsNot Nothing Then
                    p.Close()
                End If
                p = Nothing
            Catch ex As Exception
                MessageBox.Show("Error in Closing Process is " & ex.Message, "Run Oyster Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try

        End Try
    End Sub

    Public Sub RunERMetrics(ByVal path As String, ByVal jarToUse As String)

        Dim myPath As String = path
        ' Dim fileToRun As String = theFileToRun
        Dim jarForUse As String = jarToUse
        Dim oysterBoolean As Boolean = False

        Dim p As New Process

        Try
            Oysterform.OysterTabs.SelectedTab = Oysterform.Run

            Oysterform.ERMetricsRunRichTxtBx.Clear()


            Dim info As New ProcessStartInfo With {
                .UseShellExecute = False,
                .CreateNoWindow = True,
                .WindowStyle = ProcessWindowStyle.Normal,
                .RedirectStandardError = True,
                .RedirectStandardInput = True,
                .RedirectStandardOutput = True,
                .WorkingDirectory = path,
                .FileName = Constants.CmdWindow
              }

            p = Process.Start(info)
            AddHandler p.OutputDataReceived, AddressOf Oysterform.ConsoleOutputHandler
            AddHandler p.ErrorDataReceived, AddressOf Oysterform.ConsoleErrorHandler
            p.BeginErrorReadLine()
            p.BeginOutputReadLine()
            p.StandardInput.WriteLine("cd " & myPath & " " & vbCrLf)



            p.StandardInput.WriteLine("java -jar " & jarForUse & " " & vbCrLf)

        Catch ex As Exception
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False
            Oysterform.StopRun.Enabled = False


            MessageBox.Show("Error in running ERMetrics is " & ex.Message, "Run ERMetrics Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            Try
                If p IsNot Nothing Then
                    p.Close()
                    p = Nothing
                End If
            Catch ex As Exception
                MessageBox.Show("Error in Closing Process is " & ex.Message, "Run ERMetrics Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            Oysterform.ToolStripStatusLabel1.Visible = False
            Oysterform.ToolStripProgressBar1.Visible = False
            Oysterform.StopRun.Enabled = False

        End Try
    End Sub


    Public Sub RunPython()

        Dim p As Process = Nothing

        Try
            p = New Process
            Oysterform.RobotRTBx.Clear()
            Dim info As New ProcessStartInfo With {
            .UseShellExecute = False,
            .CreateNoWindow = True,
            .WindowStyle = ProcessWindowStyle.Normal,
            .RedirectStandardError = True,
            .RedirectStandardInput = True,
            .RedirectStandardOutput = True,
            .WorkingDirectory = Oysterform.RobotWkDirTxBx.Text,
            .FileName = Constants.CmdWindow
          }
            Dim list As String = " C:\\OysterTestSuite\\Oyster\\MergePurgeRunScript.xml,C:\OysterTestSuite\\Oyster\\IdentityCaptureRunScript.xml"
            'C:\\OysterTestSuite\\Oyster\\MergePurgeRunScript.xml,C:\\OysterTestSuite\\Oyster\\MergePurgeRunScript.xml,C:\\OysterTestSuite\\Oyster\\MergePurgeRunScript.xml'"
            p = Process.Start(info)
            AddHandler p.OutputDataReceived, AddressOf Oysterform.ConsoleOutputPythonHandler
            AddHandler p.ErrorDataReceived, AddressOf Oysterform.ConsoleErrorPythonHandler
            p.BeginErrorReadLine()
            p.BeginOutputReadLine()
            p.StandardInput.WriteLine("cd " & Oysterform.PythonExeTxBx.Text)
            Dim runFile As String = Oysterform.RobotWkDirTxBx.Text & "\OYSTER Robot.py"
            Dim sp As String = """" + runFile + """"
            'p.StandardInput.WriteLine("Python " & sp)
            p.StandardInput.WriteLine("Python " & sp & list)
        Catch ex As Exception
            Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
            Oysterform.ToolStripStatusLabel1.Visible = True
            Oysterform.ToolStripProgressBar1.Visible = False
            Oysterform.StopRun.Enabled = False

            MessageBox.Show("Error in running Python is " & ex.Message, "Run Python Error", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Finally
            Try
                If p IsNot Nothing Then
                    p.Close()
                End If
                p = Nothing


            Catch ex As Exception
                MessageBox.Show("Error in Closing Process is " & ex.Message, "Run Python Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
                Oysterform.ToolStripStatusLabel1.Visible = True
                Oysterform.ToolStripProgressBar1.Visible = False
                Oysterform.StopRun.Enabled = False
            End Try

        End Try
    End Sub


End Class
