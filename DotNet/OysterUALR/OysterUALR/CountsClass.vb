﻿Imports System.Dynamic
Imports System.Text

'used for the analysis form -AnalyzeForm

Public Class CountsClass
    Inherits DynamicObject

    Public _values As New Dictionary(Of String, Integer)

    Public Sub New()
    End Sub

    Public Function GetPropertyValue(ByVal propertyName As String) As Integer
        If _values.ContainsKey(propertyName) Then
            Return _values(propertyName)
        Else
            Return 0
        End If
    End Function

    Public Function SetPropertyValue(ByVal propertyName As String, ByVal value As Integer) As Boolean

        If _values.ContainsKey(propertyName) Then
            _values(propertyName) = value
        Else
            _values.Add(propertyName, value)
        End If

        Return True
    End Function

    ' Public Shared Header As String = Nothing

    Public Sub LoadColumns(ByVal headerRow As String, ByRef dt As DataTable, ByVal analyzeData As Boolean)

        Dim delimiter As String = Nothing
        Try
            If headerRow.Contains(",") Then
                delimiter = ","
            ElseIf headerRow.Contains(vbTab) Then
                delimiter = vbTab
            ElseIf headerRow.Contains("|") Then
                delimiter = "|"
            ElseIf headerRow.Contains(";") Then
                delimiter = ";"
            End If

            Dim newline() As String = Split(headerRow, delimiter)
            '     Header = headerRow
            Dim Row1 As String() = Split(headerRow, delimiter)
            Dim count As Integer = 0
            If AnalyzeForm.AnalyzeDataGridView.Columns.Count = 0 AndAlso Row1.Count > 0 Then
                Dim i As Integer

                For i = 0 To Row1.Count - 1
                    If analyzeData And count = 0 Then
                        dt.Columns.Add("Statistic")

                        dt.Columns.Add(newline(i).ToUpper)

                        count = 1
                    Else
                        dt.Columns.Add(newline(i).ToUpper)

                    End If
                Next

            End If



        Catch ex As Exception
            MsgBox("Help3 " & ex.Message)

        End Try


    End Sub

    'calculates percentages from simple stats about input file
    Public Function CalculatePercentage(ByRef dt As DataTable) As DataTable

        Dim newRow As String = Nothing
        Dim newRowNull As String = Nothing
        Dim newRowNullPercent As String = Nothing
        Dim Row As DataRow
        Dim percentLine() As String = Nothing
        Dim NullLines() As String = Nothing
        Dim NullPercent() As String = Nothing
        Dim column As DataColumn
        Dim intHolder As Integer = 0
        Dim newValue As Integer = 0
        Dim counter As Integer = 0
        Dim intNullHolder As Integer = 0
        Dim currentRows() As DataRow = dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Try


            For Each Row In currentRows
                For Each column In dt.Columns
                    If Not Row(column).ToString.ToUpper.Contains("Counts".ToUpper) Then
                        If counter = 0 Then
                            intHolder = CType(Row(column), Integer)
                            newRow = "Percent Non-Null".ToUpper & "," & Format(intHolder / intHolder, "Percent")
                            newRowNull = "Null Counts".ToUpper & "," & (intHolder - intHolder).ToString
                            newRowNullPercent = "Percent Null".ToUpper & "," & Format(intHolder - intHolder, "Percent")
                            counter = 1
                        Else
                            newRow = newRow & "," & Format(CType(Row(column), Integer) / intHolder, "Percent")
                            intNullHolder = intHolder - CType(Row(column), Integer)
                            newRowNull = newRowNull & "," & (intHolder - CType(Row(column), Integer)).ToString
                            newRowNullPercent = newRowNullPercent & "," & Format(intNullHolder / intHolder, "Percent")
                        End If

                    End If
                Next

            Next

            percentLine = Split(newRow, ",")
            NullLines = Split(newRowNull, ",")
            NullPercent = Split(newRowNullPercent, ",")
            dt.Rows.Add(NullLines)
            dt.Rows.Add(percentLine)
            dt.Rows.Add(NullPercent)
        Catch ex As Exception
            MessageBox.Show("CalcPercent Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

        Return dt

    End Function

    'calculates distinct counts for simple statistics from input file to Analyze

    Public Function AnalyzeDistinctData(ByRef dt As DataTable, ByVal FileNamePath As String) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim myList As List(Of DistinctColumn)
        Dim myUniqueList As List(Of DistinctColumn)

        Try

            If System.IO.File.Exists(FileNamePath) = True Then
                myList = New List(Of DistinctColumn)
                myUniqueList = New List(Of DistinctColumn)
                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)
                Dim index As Integer = 0
                Dim delimiter As String = Nothing
                Dim countsClass As New CountsClass

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If index > 0 Then
                        SplitLine = Split(TextLine, delimiter)
                        For i = 0 To HeaderRowCount - 1
                            If Not String.IsNullOrEmpty(SplitLine(i)) Then
                                Dim ColumnValue As New DistinctColumn
                                ColumnValue.ItemId = i
                                ColumnValue.Value = SplitLine(i)
                                myList.Add(ColumnValue)
                            End If
                        Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        index = 1
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length

                    End If
                Loop


                Dim Row As String = "Distinct Values".ToUpper & delimiter
                Dim RowUnique As String = "Unique Values".ToUpper & delimiter
                Dim myI As Integer = 1
                '   Dim duplicateProp1 = (From o As DistinctColumn In myList Where o.ItemId = 0 Order By o.ItemId, o.Value Group By o.ItemId, o.Value.ToUpper.Trim Into g = Group Where g.Count >= 1 Select g.First).ToList

                For i = 0 To HeaderRowCount - 1
                    myI = i
                    If i < HeaderRowCount - 1 Then

                        Row = Row & (From item In myList Where item.ItemId = myI Select item.Value.Trim).Distinct().Count() & delimiter
                        RowUnique = RowUnique & (From o As DistinctColumn In myList Where o.ItemId = myI Order By o.ItemId, o.Value Group By o.ItemId, o.Value Into g = Group Where g.Count = 1 Select g.First).Count() & delimiter

                    Else
                        '         
                        Row = Row & (From item In myList Where item.ItemId = myI Select item.Value.Trim).Distinct.Count()
                        RowUnique = RowUnique & (From o As DistinctColumn In myList Where o.ItemId = myI Order By o.ItemId, o.Value Group By o.ItemId, o.Value Into g = Group Where g.Count = 1 Select g.First).Count()

                    End If

                Next

                Dim tLine() As String = Split(Row, delimiter)
                Dim uniqueLine() As String = Split(RowUnique, delimiter)

                dt.Rows.Add(tLine)
                dt.Rows.Add(uniqueLine)
                If Not objReader Is Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If

            Else
                MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("AnalyzeDistinctData Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        Return dt
    End Function

    'used in analyze form
    Public Function AnalyzeData(ByRef dt As DataTable, ByVal FileNamePath As String) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Try

            If System.IO.File.Exists(FileNamePath) = True Then

                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)
                Dim index As Integer = 0

                Dim delimiter As String = Nothing

                Dim countsClass As New CountsClass

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If index > 0 Then
                        SplitLine = Split(TextLine, delimiter)
                        For i = 0 To HeaderRowCount - 1
                            If Not String.IsNullOrEmpty(SplitLine(i)) Then
                                '    Console.WriteLine(HeaderRow(i) & " value " & SplitLine(i))
                                Dim j As Integer = countsClass.GetPropertyValue(HeaderRow(i))
                                j = j + 1
                                countsClass.SetPropertyValue(HeaderRow(i), j)
                            End If
                        Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        LoadColumns(TextLine, dt, True)
                        index = 1
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length


                        For i = 0 To HeaderRowCount - 1
                            countsClass.SetPropertyValue(HeaderRow(i), 0)
                        Next
                    End If
                Loop
                Dim Row As String = "Non-Null Counts".ToUpper & delimiter

                For i = 0 To HeaderRowCount - 1
                    If i < HeaderRowCount - 1 Then
                        Row = Row & countsClass.GetPropertyValue(HeaderRow(i)).ToString & delimiter
                    Else
                        Row = Row & countsClass.GetPropertyValue(HeaderRow(i)).ToString
                    End If
                Next

                Dim tLine() As String = Split(Row, delimiter)

                dt.Rows.Add(tLine)
                If Not objReader Is Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If
            Else
                MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("AnalyzeData Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End Try

        Return dt

    End Function

    'used in the AnalyzeForm class
    Public Function NewAnalyzeData(ByRef dt As DataTable, ByVal FileNamePath As String) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Try

            If System.IO.File.Exists(FileNamePath) = True Then
                Oysterform.ToolStripStatusLabel1.Text = "Loading..."
                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)
                Dim index As Integer = 0

                Dim delimiter As String = Nothing

                Dim countsClass As New CountsClass

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()

                    If index > 0 Then
                        SplitLine = Split(TextLine, delimiter)
                        For i = 0 To HeaderRowCount - 1
                            If Not String.IsNullOrEmpty(SplitLine(i)) Then
                                '    Console.WriteLine(HeaderRow(i) & " value " & SplitLine(i))
                                Dim j As Integer = countsClass.GetPropertyValue(HeaderRow(i).ToUpper)
                                j = j + 1
                                countsClass.SetPropertyValue(HeaderRow(i).ToUpper, j)
                            End If
                        Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        'LoadColumns(TextLine, dt, True)
                        index = 1
                        HeaderRow = Split(TextLine, delimiter)
                        HeaderRowCount = HeaderRow.Length


                        For i = 0 To HeaderRowCount - 1
                            countsClass.SetPropertyValue(HeaderRow(i).ToUpper, 0)
                        Next
                    End If
                Loop
                dt.Columns.Add("Column".ToUpper)
                dt.Columns.Add("Value".ToUpper)
                Dim Row As String = Nothing

                For i = 0 To HeaderRowCount - 1
                    'If i < HeaderRowCount - 1 Then
                    '    Row = Row & HeaderRow(i) & delimiter & countsClass.GetPropertyValue(HeaderRow(i)).ToString & delimiter
                    'Else
                    Row = HeaderRow(i).ToUpper & delimiter & countsClass.GetPropertyValue(HeaderRow(i).ToUpper).ToString

                    Dim tLine() As String = Split(Row, delimiter)

                    dt.Rows.Add(tLine)
                    '   End If
                Next
                If Not objReader Is Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If
                'Dim tLine() As String = Split(Row, delimiter)

                'dt.Rows.Add(tLine)
            Else
                MessageBox.Show("File Does Not Exist", "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MessageBox.Show("AnalyzeData Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            '   Oysterform.ToolStripStatusLabel1.Text = Constants.OHelper
        End Try

        Return dt

    End Function

    'calculates max freq, average, std deviation
    Public Function NewDistribution(ByRef dt As DataTable, ByVal FileNamePath As String, ByVal columnChoice As String, ByVal Top As Integer) As DataTable

        Dim TextLine As String = Nothing
        Dim SplitLine() As String
        Dim HeaderRow() As String = Nothing
        Dim HeaderRowCount As Integer = 0
        Dim choice As String = columnChoice
        Dim i As Integer = 0
        Dim aTop As Integer = Top
        Dim aCount As Integer = 0

        Try

            If System.IO.File.Exists(FileNamePath) = True Then
                dt.Clear()

                ' Dim lineCount = File.ReadAllLines(FileNamePath).Length
                Dim objReader As New System.IO.StreamReader(FileNamePath, Encoding.ASCII)
                Dim index As Integer = 0
                Dim j As Integer = 0
                Dim delimiter As String = Nothing

                Dim countsClass As New CountsClass
                ' Console.WriteLine(choice)

                Do While objReader.Peek() <> -1

                    TextLine = objReader.ReadLine()
                    SplitLine = Split(TextLine, delimiter)

                    If index > 0 Then

                        If Not String.IsNullOrEmpty(SplitLine(i)) Then

                            aCount = aCount + 1
                            j = countsClass.GetPropertyValue(SplitLine(i).ToUpper)
                            j = j + 1
                            countsClass.SetPropertyValue(SplitLine(i).ToUpper, j)
                        End If
                        '  Next
                    Else

                        If TextLine.Contains(",") Then
                            delimiter = ","
                        ElseIf TextLine.Contains(vbTab) Then
                            delimiter = vbTab
                        ElseIf TextLine.Contains("|") Then
                            delimiter = "|"
                        ElseIf TextLine.Contains(";") Then
                            delimiter = ";"
                        End If

                        '  LoadColumns(TextLine, dt, True)

                        index = 1

                        HeaderRow = Split(TextLine, delimiter)

                        HeaderRowCount = HeaderRow.Length


                        For k = 0 To HeaderRowCount - 1

                            If HeaderRow(k).ToUpper.Equals(choice.ToUpper) Then
                                i = k
                                Exit For
                            End If
                        Next

                    End If

                Loop

                dt.Clear()
                Dim tLine() As String = Nothing

                Dim Row As String = ""

                dt.Columns.Add(choice)
                dt.Columns.Add("COUNTS")

                Dim totalCounts As Integer = 0
                Dim aSorted = (From pair In countsClass._values Order By pair.Value Descending)
                Dim counter As Integer = 0
                For Each item In aSorted

                    totalCounts = totalCounts + 1
                Next

                Dim data() As Double = {}
                ReDim data(totalCounts)
                'Console.WriteLine(totalCounts)
                '   Dim average As Double = (aCount / totalCounts)
                Dim theCount As Double = (aCount / totalCounts)
                '  Console.WriteLine("avg Frequency: " & theCount)

                Dim sorted = (From pair In countsClass._values Order By pair.Value Descending).Take(aTop)
                Dim lowestValue As Integer = 0
                Dim highestValue As Integer = 0
                Dim countValue As Integer = 0
                For Each item In sorted
                    If counter = 0 Then
                        highestValue = countsClass.GetPropertyValue(item.Key)
                    End If
                    data.SetValue(countsClass.GetPropertyValue(item.Key), counter)
                    counter = counter + 1

                    Row = item.Key & "," & countsClass.GetPropertyValue(item.Key)
                    '    Console.WriteLine(item.Key & "," & countsClass.GetPropertyValue(item.Key))
                    tLine = Split(Row, delimiter)
                    dt.Rows.Add(tLine)


                Next

                Dim dBle As Double = StandardDeviation(data)
                '     Console.WriteLine(dBle.ToString)

                AnalyzeForm.HighestLBL.Text = "Max Freq: " & highestValue.ToString
                AnalyzeForm.AvgLbl.Text = "Avg Freq: " & Math.Round(theCount, 3, MidpointRounding.AwayFromZero).ToString
                AnalyzeForm.StdDevLbl.Text = "Std Dev: " & Math.Round(dBle, 3, MidpointRounding.AwayFromZero).ToString
                'HighestLBL.TextMax Freq = 10
                'Avg Freq = 6
                'Std Dev = 3.91578
                If Not objReader Is Nothing Then
                    objReader.Close()
                    objReader = Nothing
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("CountsClass Distribution Error is " & ex.Message, "Exception Thrown Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Return dt

    End Function

    'calculates standard deviaiton
    Public Shared Function StandardDeviation(data() As Double) As Double
        Dim mean As Double = 0.0
        Dim sumDeviation As Double = 0.0
        Dim dataSize As Integer = data.Length

        For i As Integer = 0 To dataSize - 1
            mean += data(i)
        Next

        mean = mean / dataSize

        For i As Integer = 0 To dataSize - 1
            sumDeviation += (data(i) - mean) * (data(i) - mean)
        Next
        Dim variance As Double = 0.0
        variance = sumDeviation / (dataSize - 1)
        Return Math.Sqrt(variance)
    End Function

End Class


'used to hold data for analysis methods
Public Class DistinctColumn

    ' Private m_DistinctColumn As List(Of DistinctColumn)

    Private m_Column As Integer
    Public Property ItemId() As Integer
        Get
            Return m_Column
        End Get
        Set(value As Integer)
            m_Column = value
        End Set
    End Property

    Private m_Value As String
    Public Property Value() As String
        Get
            Return m_Value
        End Get
        Set(value As String)
            m_Value = value
        End Set
    End Property

End Class