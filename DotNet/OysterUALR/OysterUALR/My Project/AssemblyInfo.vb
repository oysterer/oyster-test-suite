﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Oyster Helper")>
<Assembly: AssemblyDescription("Program to Help use Oyster and ERMetrics for Entity and Identity Resolution")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("Oyster Helper")>
<Assembly: AssemblyCopyright("Copyright © UALR  2018, 2019")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f0ef21bc-0b35-4ffb-8998-649e2e55e8a4")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.2.6.0")>
<Assembly: AssemblyFileVersion("1.2.6.0")>
