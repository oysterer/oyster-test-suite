﻿namespace OysterHelper
{
    namespace My
    {
        [global::System.Runtime.CompilerServices.CompilerGenerated()]
        [global::System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public sealed partial class MySettings : global::System.Configuration.ApplicationSettingsBase
        {
            private static MySettings defaultInstance = (MySettings)global::System.Configuration.ApplicationSettingsBase.Synchronized(new MySettings());

            /* TODO ERROR: Skipped IfDirectiveTrivia */
            private static bool addedHandler;

            private static object addedHandlerLockObject = new object();

            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
            private static void AutoSaveSettings(global::System.Object sender, global::System.EventArgs e)
            {
                if (My.MyProject.Application.SaveMySettingsOnExit)
                    My.MySettingsProperty.Settings.Save();
            }
            /* TODO ERROR: Skipped EndIfDirectiveTrivia */
            public static MySettings Default
            {
                get
                {

                    /* TODO ERROR: Skipped IfDirectiveTrivia */
                    if (!addedHandler)
                    {
                        lock (addedHandlerLockObject)
                        {
                            if (!addedHandler)
                            {
                                My.MyProject.Application.Shutdown += AutoSaveSettings;
                                addedHandler = true;
                            }
                        }
                    }
                    /* TODO ERROR: Skipped EndIfDirectiveTrivia */
                    return defaultInstance;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string OysterRootDir
            {
                get
                {
                    return System.Convert.ToString(this["OysterRootDir"]);
                }
                set
                {
                    this["OysterRootDir"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string OysterWorkingDir
            {
                get
                {
                    return System.Convert.ToString(this["OysterWorkingDir"]);
                }
                set
                {
                    this["OysterWorkingDir"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string LastRunFile
            {
                get
                {
                    return System.Convert.ToString(this["LastRunFile"]);
                }
                set
                {
                    this["LastRunFile"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string ERMetricsWorkDir
            {
                get
                {
                    return System.Convert.ToString(this["ERMetricsWorkDir"]);
                }
                set
                {
                    this["ERMetricsWorkDir"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string OysterJar
            {
                get
                {
                    return System.Convert.ToString(this["OysterJar"]);
                }
                set
                {
                    this["OysterJar"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string ERMetricsJar
            {
                get
                {
                    return System.Convert.ToString(this["ERMetricsJar"]);
                }
                set
                {
                    this["ERMetricsJar"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string EROysterWorkDir
            {
                get
                {
                    return System.Convert.ToString(this["EROysterWorkDir"]);
                }
                set
                {
                    this["EROysterWorkDir"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("")]
            public string ERMetricsLinkFile
            {
                get
                {
                    return System.Convert.ToString(this["ERMetricsLinkFile"]);
                }
                set
                {
                    this["ERMetricsLinkFile"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("0")]
            public int CSVFileCount
            {
                get
                {
                    return System.Convert.ToInt32(this["CSVFileCount"]);
                }
                set
                {
                    this["CSVFileCount"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("0")]
            public int RefFileCount
            {
                get
                {
                    return System.Convert.ToInt32(this["RefFileCount"]);
                }
                set
                {
                    this["RefFileCount"] = value;
                }
            }

            [global::System.Configuration.UserScopedSetting()]
            [global::System.Diagnostics.DebuggerNonUserCode()]
            [global::System.Configuration.DefaultSettingValue("False")]
            public bool Testing
            {
                get
                {
                    return System.Convert.ToBoolean(this["Testing"]);
                }
                set
                {
                    this["Testing"] = value;
                }
            }
        }
    }

    namespace My
    {
        [global::Microsoft.VisualBasic.HideModuleName()]
        [global::System.Diagnostics.DebuggerNonUserCode()]
        [global::System.Runtime.CompilerServices.CompilerGenerated()]
        internal static class MySettingsProperty
        {
            [global::System.ComponentModel.Design.HelpKeyword("My.Settings")]
            internal static global::OysterHelper.My.MySettings Settings
            {
                get
                {
                    return global::OysterHelper.My.MySettings.Default;
                }
            }
        }
    }
}
