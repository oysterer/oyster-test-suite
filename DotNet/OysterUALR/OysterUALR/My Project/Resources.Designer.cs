﻿namespace OysterHelper
{
    namespace OysterHelper.My.Resources
    {

        // This class was auto-generated by the StronglyTypedResourceBuilder
        // class via a tool like ResGen or Visual Studio.
        // To add or remove a member, edit your .ResX file then rerun ResGen
        // with the /str option, or rebuild your VS project.
        /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCode()]
        [global::System.Runtime.CompilerServices.CompilerGenerated()]
        [global::Microsoft.VisualBasic.HideModuleName()]
        internal static class Resources
        {
            private static global::System.Resources.ResourceManager resourceMan;

            private static global::System.Globalization.CultureInfo resourceCulture;

            /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
            [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
            internal static global::System.Resources.ResourceManager ResourceManager
            {
                get
                {
                    if (object.ReferenceEquals(resourceMan, null))
                    {
                        global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OysterHelper.Resources", typeof(Resources).Assembly);
                        resourceMan = temp;
                    }
                    return resourceMan;
                }
            }

            /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
            [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
            internal static global::System.Globalization.CultureInfo Culture
            {
                get
                {
                    return resourceCulture;
                }
                set
                {
                    resourceCulture = value;
                }
            }

            /// <summary>
        /// Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        /// </summary>
            internal static System.Drawing.Icon UALR
            {
                get
                {
                    object obj = ResourceManager.GetObject("UALR", resourceCulture);
                    return (System.Drawing.Icon)obj;
                }
            }

            /// <summary>
        /// Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
            internal static System.Drawing.Bitmap ualr2
            {
                get
                {
                    object obj = ResourceManager.GetObject("ualr2", resourceCulture);
                    return (System.Drawing.Bitmap)obj;
                }
            }

            /// <summary>
        /// Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
            internal static System.Drawing.Bitmap ualr21
            {
                get
                {
                    object obj = ResourceManager.GetObject("ualr21", resourceCulture);
                    return (System.Drawing.Bitmap)obj;
                }
            }
        }
    }
}
