﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Oyster Helper")]
[assembly: AssemblyDescription("Program to Help Run Oyster and ERMetrics")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("OysterHelper")]
[assembly: AssemblyCopyright("Copyright © UALR  2018")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("f0ef21bc-0b35-4ffb-8998-649e2e55e8a4")]
[assembly: AssemblyVersion("1.2.1.0")]
[assembly: AssemblyFileVersion("1.2.1.0")]


