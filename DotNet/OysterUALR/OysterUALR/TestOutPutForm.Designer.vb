﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TestOutPutForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                strformat.Dispose()
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestOutPutForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.TestingDataGridView = New System.Windows.Forms.DataGridView()
        Me.TestDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TestSteps = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExpectedText = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Results = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.TestingDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem, Me.PrintToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1331, 25)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(40, 21)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(46, 21)
        Me.PrintToolStripMenuItem.Text = "Print"
        '
        'PrintDocument1
        '
        '
        'TestingDataGridView
        '
        Me.TestingDataGridView.AllowUserToAddRows = False
        Me.TestingDataGridView.AllowUserToDeleteRows = False
        Me.TestingDataGridView.BackgroundColor = System.Drawing.Color.AliceBlue
        Me.TestingDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TestDescription, Me.TestSteps, Me.ExpectedText, Me.Results})
        Me.TestingDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TestingDataGridView.Location = New System.Drawing.Point(0, 25)
        Me.TestingDataGridView.Name = "TestingDataGridView"
        Me.TestingDataGridView.ReadOnly = True
        Me.TestingDataGridView.Size = New System.Drawing.Size(1331, 658)
        Me.TestingDataGridView.TabIndex = 2
        '
        'TestDescription
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TestDescription.DefaultCellStyle = DataGridViewCellStyle1
        Me.TestDescription.HeaderText = "Test Description"
        Me.TestDescription.Name = "TestDescription"
        Me.TestDescription.ReadOnly = True
        '
        'TestSteps
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TestSteps.DefaultCellStyle = DataGridViewCellStyle2
        Me.TestSteps.HeaderText = "Test Steps Taken"
        Me.TestSteps.Name = "TestSteps"
        Me.TestSteps.ReadOnly = True
        '
        'ExpectedText
        '
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ExpectedText.DefaultCellStyle = DataGridViewCellStyle3
        Me.ExpectedText.HeaderText = "Expected Message Window Text"
        Me.ExpectedText.Name = "ExpectedText"
        Me.ExpectedText.ReadOnly = True
        '
        'Results
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Results.DefaultCellStyle = DataGridViewCellStyle4
        Me.Results.HeaderText = "    Result    "
        Me.Results.Name = "Results"
        Me.Results.ReadOnly = True
        '
        'TestOutPutForm
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.Maroon
        Me.ClientSize = New System.Drawing.Size(1331, 683)
        Me.Controls.Add(Me.TestingDataGridView)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.Name = "TestOutPutForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Automated Test Results"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.TestingDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents TestingDataGridView As DataGridView
    Friend WithEvents TestDescription As DataGridViewTextBoxColumn
    Friend WithEvents TestSteps As DataGridViewTextBoxColumn
    Friend WithEvents ExpectedText As DataGridViewTextBoxColumn
    Friend WithEvents Results As DataGridViewTextBoxColumn
End Class
