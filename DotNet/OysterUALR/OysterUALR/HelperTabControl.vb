﻿Imports System.IO

''' <summary>
''' 
''' This class helps maintain the tabs as users Tab through them
''' to make sure what is needed is loaded and correct? each time a tab is chosen
''' </summary>

Public Class HelperTabControl

    ''''''''''''''''''''''''''''''''''''''''
    'This method processes the chosen tab when it changes
    'to keep information uptodate and refreshed each time
    'a tab is chosen.  A TreeView is passed byRef for those 
    'tabs with a TreeView.  Right now, only one Treeview


    Public Sub ProcessTab(ByVal TabName As String, Optional ByRef Treeview As TreeView = Nothing)

        Try

            ''''''''''''''''''''''''''''''''''''''''''''''''''
            'clear the incoming Treeview - Clears any treeview 
            'coming in.  Should be filled again
            ''''''''''''''''''''''''''''''''''''''''''''''''
            If Treeview IsNot Nothing Then
                If Treeview.GetNodeCount(False) > 0 Then
                    Treeview.Nodes.Clear()
                End If
            End If

            '''''''''''''''''''''''''''''
            'Process Helpful Stuff tab
            '''''''''''''''''''''''''''''
            If TabName.Equals(Constants.HelpfulStuff) Then
                ' Console.WriteLine(Constants.HelpfulStuff)
                Oysterform.HSCsvTxBx.Text = OysterHelper.GetInputCSVFileName(Oysterform.OysterWorkDirTxBx.Text)
                Oysterform.HSLinkFileTxBx.Text = Oysterform.ERMetLinkFileTxbx.Text

                Oysterform.LFDateLbl.Text = Nothing
                Oysterform.HSLinkDateLbl.Text = Nothing

                If Not String.IsNullOrEmpty(My.Settings.ERMetricsLinkFile) And File.Exists(My.Settings.ERMetricsLinkFile) Then
                    Oysterform.LFDateLbl.Text = IO.File.GetLastWriteTime(My.Settings.ERMetricsLinkFile)
                    Oysterform.HSLinkDateLbl.Text = Oysterform.LFDateLbl.Text
                End If

                'end helpful stuff


            ElseIf TabName.Equals("KnowledgeBaseMaint") Then

                Dim myDir As String = Oysterform.OysterRootDirTxBx.Text

                Treeview.Nodes.Add(myDir)

                For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(
                    myDir, FileIO.SearchOption.SearchTopLevelOnly)
                    '  Console.WriteLine(foundDirectory2 & " 1")
                    If foundDirectory2.ToUpper.Contains(Constants.IdentityUpdate.ToUpper) _
                        Or foundDirectory2.ToUpper.Contains(Constants.IdentityRelolution.ToUpper) _
                        Or foundDirectory2.ToUpper.Contains(Constants.IdentityCapture.ToUpper) _
                        Or foundDirectory2.ToUpper.Contains(Constants.RefToRefAssertion.ToUpper) _
                        Or foundDirectory2.ToUpper.Contains(Constants.StrToStrAssertion.ToUpper) _
                         Or foundDirectory2.ToUpper.Contains(Constants.StrSplitAssertion.ToUpper) _
                          Or foundDirectory2.ToUpper.Contains(Constants.RefToStrAssertion.ToUpper) Then

                        If foundDirectory2.ToUpper.Contains("Copy".ToUpper) Then
                            'we don't want it
                        Else

                            Treeview.TopNode.Nodes.Add(Path.GetFileName(foundDirectory2))
                            'now process the files that are not ERMetrics Directory
                            Dim DNode As TreeNode

                            For Each Fdirectory As String In My.Computer.FileSystem.GetDirectories(foundDirectory2)
                                If Fdirectory.Contains("Input") Or Fdirectory.Contains("Output") Then
                                    DNode = Treeview.TopNode.LastNode.Nodes.Add(Path.GetFileName(Fdirectory))
                                    For Each file As String In My.Computer.FileSystem.GetFiles(Fdirectory)
                                        If file.Contains(".idty") Then
                                            DNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                        End If
                                    Next
                                End If
                            Next

                        End If

                    End If
                Next

                Treeview.Nodes(0).Expand()
                Treeview.Nodes(0).EnsureVisible()
                Treeview.Show()

            ElseIf TabName.Equals("KnowledgeBaseView") Then

                Dim myDir As String = Oysterform.OysterRootDirTxBx.Text

                Treeview.Nodes.Add(myDir)

                For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(
                        myDir, FileIO.SearchOption.SearchTopLevelOnly)
                    '  Console.WriteLine(foundDirectory2 & " 1")
                    If foundDirectory2.ToUpper.Contains(Constants.IdentityUpdate.ToUpper) _
                            Or foundDirectory2.ToUpper.Contains(Constants.IdentityRelolution.ToUpper) _
                            Or foundDirectory2.ToUpper.Contains(Constants.IdentityCapture.ToUpper) _
                            Or foundDirectory2.ToUpper.Contains(Constants.RefToRefAssertion.ToUpper) _
                            Or foundDirectory2.ToUpper.Contains(Constants.StrToStrAssertion.ToUpper) _
                             Or foundDirectory2.ToUpper.Contains(Constants.StrSplitAssertion.ToUpper) _
                              Or foundDirectory2.ToUpper.Contains(Constants.RefToStrAssertion.ToUpper) Then

                        If foundDirectory2.ToUpper.Contains("Copy".ToUpper) Then
                            'we don't want it
                        Else

                            Treeview.TopNode.Nodes.Add(Path.GetFileName(foundDirectory2))
                            'now process the files that are not ERMetrics Directory
                            Dim DNode As TreeNode

                            For Each Fdirectory As String In My.Computer.FileSystem.GetDirectories(foundDirectory2)
                                If Fdirectory.Contains("Input") Or Fdirectory.Contains("Output") Then
                                    DNode = Treeview.TopNode.LastNode.Nodes.Add(Path.GetFileName(Fdirectory))
                                    For Each file As String In My.Computer.FileSystem.GetFiles(Fdirectory)
                                        If file.Contains(".idty") Then
                                            DNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                        End If
                                    Next
                                End If
                            Next

                        End If

                    End If
                Next

                Treeview.Nodes(0).Expand()
                Treeview.Nodes(0).EnsureVisible()
                Treeview.Show()



                ' Process WorkWithOysterFiles tab
            ElseIf TabName.Equals(Constants.WorkWithOysterFiles) Then

                'Console.WriteLine(Constants.WorkWithOysterFiles)
                Dim myDir As String = Oysterform.OysterRootDirTxBx.Text
                Dim ERRootDir As String = Oysterform.ERMetricsTxBx.Text

                Treeview.Nodes.Add(myDir)

                For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(
                    myDir, FileIO.SearchOption.SearchTopLevelOnly)


                    ' Keep certain directories out of the list to work on in TreeView
                    If Not foundDirectory2.ToUpper.Contains(Constants.COMMONSJAR) _
                        And Not foundDirectory2.ToUpper.Contains("OysterHelper".ToUpper) _
                        And Not foundDirectory2.ToUpper.Contains(Constants.LLIB) _
                        And Not foundDirectory2.ToUpper.Contains(Constants.COPY) _
                        And Not foundDirectory2.ToUpper.Contains(Constants.LOG) Then


                        Treeview.TopNode.Nodes.Add(Path.GetFileName(foundDirectory2))
                        '    Console.WriteLine(Path.GetFileName(foundDirectory2))

                        'data directory added
                        If Path.GetFileName(foundDirectory2).ToUpper.Equals(Constants.DATA) Then
                            '     Console.WriteLine("data inside")
                            For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)
                                If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".jar", comparisonType:=1) And
                                    Not file.EndsWith(".ert", comparisonType:=1) Then
                                    Treeview.TopNode.LastNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        End If

                        '''''''''''''''''''''''''''''''''''''''''''''''''''
                        ' specifically for ER_Metrics root directory
                        ' because it has no input,output,scripts directories 
                        ' Like other directories
                        '''''''''''''''''''''''''''''''''''''''''''''''''''
                        If foundDirectory2.ToUpper.Equals(ERRootDir.ToUpper) Then

                            For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)
                                If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".jar", comparisonType:=1) And
                                    Not file.EndsWith(".ert", comparisonType:=1) Then
                                    Treeview.TopNode.LastNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        End If
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'End of special ERMetrics Directory processing
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                        'now process the files that are not ERMetrics Directory
                        Dim DNode As TreeNode
                        For Each Fdirectory As String In My.Computer.FileSystem.GetDirectories(foundDirectory2)
                            DNode = Treeview.TopNode.LastNode.Nodes.Add(Path.GetFileName(Fdirectory))
                            For Each file As String In My.Computer.FileSystem.GetFiles(Fdirectory)
                                If Not file.EndsWith(".zip", comparisonType:=1) Then
                                    DNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        Next
                    End If
                Next

                'Add new topnode so xml files in root can
                'be added to the Treeview on this Tab
                Treeview.Nodes.Add(myDir)

                ''for oyster root directory xml files
                For Each file As String In My.Computer.FileSystem.GetFiles(myDir)
                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".xml", comparisonType:=1) Then
                        Treeview.TopNode.NextNode.Nodes.Add(st).Tag = file
                    End If
                Next


                Treeview.Nodes(0).Expand()
                Treeview.Nodes(1).Expand()
                Treeview.Nodes(0).EnsureVisible()
                Treeview.Show()

                'process tab ERMetrics Setup
            ElseIf TabName.Equals(Constants.ERMetricsSetup) Then
                'Console.WriteLine(Constants.ERMetricsSetup)

                Dim myDir As String = Oysterform.ERMetricsTxBx.Text
                Dim DNode As TreeNode = Treeview.Nodes.Add(myDir)

                For Each file As String In My.Computer.FileSystem.GetFiles(myDir)
                    Dim st As String = Path.GetFileName(file)
                    '  Dim str As String = Path.GetFileName(file) & " " & IO.File.GetLastWriteTime(file)
                    Dim str As String = Path.GetFileName(file)
                    If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".doc", comparisonType:=1) Then

                        '  Dim st As String = Path.GetFileName(file)
                        If st.EndsWith(".properties", comparisonType:=1) Then
                            Oysterform.TreeView4.Nodes(0).Nodes.Add(Path.GetFileName(file)).Tag = file
                        ElseIf st.EndsWith(".log", comparisonType:=1) Then
                            Oysterform.TreeView4.Nodes(0).Nodes.Add(Path.GetFileName(file)).Tag = file
                        ElseIf st.EndsWith(".out", comparisonType:=1) Then
                            Oysterform.TreeView4.Nodes(0).Nodes.Add(Path.GetFileName(file)).Tag = file
                        ElseIf st.EndsWith(".ert", comparisonType:=1) Then
                            Oysterform.TreeView4.Nodes(0).Nodes.Add(Path.GetFileName(file)).Tag = file
                        ElseIf st.EndsWith(".link", comparisonType:=1) Then
                            Oysterform.TreeView4.Nodes(0).Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                        End If
                    End If

                Next

                'add the output directory files
                Dim dir As String = Oysterform.EROysterWkDirTxBx.Text + "\" + "Output"

                Dim BNode As TreeNode = Treeview.Nodes.Add(dir)

                For Each file As String In My.Computer.FileSystem.GetFiles(dir)
                    Dim st As String = Path.GetFileName(file)
                    If st.EndsWith(".link", comparisonType:=1) Then
                        BNode.Nodes.Add(st & " " & IO.File.GetLastWriteTime(file)).Tag = file
                    End If
                Next
                'do some textbox work and settings saves
                Dim rootODir As String = Oysterform.OysterRootDirTxBx.Text
                Dim workODir As String = Oysterform.OysterWorkDirTxBx.Text
                Dim erworkDir As String = Oysterform.EROysterWkDirTxBx.Text
                Oysterform.LFDateLbl.Text = Nothing
                Oysterform.HSLinkDateLbl.Text = Nothing

                '   If My.Settings.OysterWorkingDir.Equals(erworkDir) Then

                If Not String.IsNullOrEmpty(My.Settings.ERMetricsLinkFile) And File.Exists(My.Settings.ERMetricsLinkFile) Then

                        If Not String.IsNullOrEmpty(rootODir) And Not String.IsNullOrEmpty(workODir) Then
                            Oysterform.ERMetLinkFileTxbx.Text = OysterHelper.GetLinkFileName(rootODir, workODir)
                            My.Settings.ERMetricsLinkFile = Oysterform.ERMetLinkFileTxbx.Text
                            Oysterform.LFDateLbl.Text = IO.File.GetLastWriteTime(My.Settings.ERMetricsLinkFile)
                            Oysterform.HSLinkDateLbl.Text = Oysterform.LFDateLbl.Text
                            My.Settings.Save()
                        End If
                    End If
                '     Else


                'Oysterform.ERMetLinkFileTxbx.Text = OysterHelper.GetLinkFileName(rootODir, erworkDir)
                'My.Settings.ERMetricsLinkFile = Oysterform.ERMetLinkFileTxbx.Text
                'Oysterform.LFDateLbl.Text = IO.File.GetLastWriteTime(My.Settings.ERMetricsLinkFile)
                'Oysterform.HSLinkDateLbl.Text = Oysterform.LFDateLbl.Text
                'My.Settings.Save()
                'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                '  End If



                Treeview.Nodes(0).Expand()
                    ' Treeview.Nodes(1).Expand()
                    Treeview.Nodes(0).EnsureVisible()
                    Treeview.Show()



                    ' process OysterSetup Tab named Welcome
                ElseIf (TabName.Equals(Constants.Welcome)) Then

                    ' Console.WriteLine(Constants.Welcome)

                    Dim myDir As String = Oysterform.OysterWorkDirTxBx.Text
                    '   Console.WriteLine(myDir)

                    For Each foundDirectory As String In My.Computer.FileSystem.GetDirectories(
                 myDir, FileIO.SearchOption.SearchAllSubDirectories)
                        If foundDirectory.Contains(myDir) Then
                            Treeview.Nodes.Add(foundDirectory, myDir)
                        End If
                        Exit For
                    Next

                    Dim s As String = Oysterform.OysterRootDirTxBx.Text
                    '      Console.WriteLine(s)
                    Dim str As String = Path.GetFileName(myDir)
                    Dim l As List(Of String) = IO.Directory.GetFiles(s, "*.xml").Select(Function(file) IO.Path.GetFileName(file)).ToList

                    For Each item As String In l
                        If item.ToUpper.Contains(str.ToUpper & Constants.RUNSCRIPTS) And item.StartsWith(str) Then
                            Dim sa As String = s + "\" + item
                            Treeview.Nodes.Add(sa).Tag = sa
                        End If
                    Next


                    Dim DNode As TreeNode
                    For Each foundDirectory2 As String In My.Computer.FileSystem.GetDirectories(
                 myDir, FileIO.SearchOption.SearchTopLevelOnly)
                        If foundDirectory2.ToUpper.Contains(Constants.INPUT) _
                        Or foundDirectory2.ToUpper.Contains(Constants.OUTPUT) _
                         Or foundDirectory2.ToUpper.Contains(Constants.SCRIPTS) Then

                            DNode = Treeview.TopNode.Nodes.Add(Path.GetFileName(foundDirectory2))
                            For Each file As String In My.Computer.FileSystem.GetFiles(foundDirectory2)
                                If Not file.EndsWith(".zip") Then
                                    DNode.Nodes.Add(Path.GetFileName(file)).Tag = file
                                End If
                            Next

                        End If
                    Next


                    Treeview.Nodes(0).Expand()
                    Treeview.Nodes(0).EnsureVisible()
                    Treeview.Show()



                ElseIf (TabName.Equals(Constants.RobotTab)) Then

                    ' Console.WriteLine(Constants.Welcome)

                    Dim myDir As String = Oysterform.RobotWkDirTxBx.Text
                '   Console.WriteLine(myDir)

                '   Dim myDir As String = Oysterform.ERMetricsTxBx.Text
                Dim DNode As TreeNode = Treeview.Nodes.Add(myDir)

                For Each file As String In My.Computer.FileSystem.GetFiles(myDir)
                    Dim st As String = Path.GetFileName(file)
                    '  Dim str As String = Path.GetFileName(file) & " " & IO.File.GetLastWriteTime(file)
                    If Not file.EndsWith(".zip", comparisonType:=1) And Not file.EndsWith(".doc", comparisonType:=1) _
                        And Not file.EndsWith(".jar", comparisonType:=1) And Not file.EndsWith(".xlsx", comparisonType:=1) Then
                        DNode.Nodes.Add(st).Tag = file
                    End If

                Next
                Dim OBean As New HelperBean

                If Oysterform.saveInfoCollection.Contains(21) Then
                    OBean = Oysterform.saveInfoCollection.Item(21)
                    Oysterform.PythonFileLbl.Text = "Loaded File is: " & Path.GetFileName(OBean.FileLoaded)
                End If

                Treeview.Nodes(0).Expand()
                Treeview.Nodes(0).EnsureVisible()
                Treeview.Show()

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub





End Class
