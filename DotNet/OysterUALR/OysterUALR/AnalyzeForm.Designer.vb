﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AnalyzeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AnalyzeForm))
        Me.AnalyzeDataGridView = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.DataLoadBtn = New System.Windows.Forms.Button()
        Me.Exit_Btn2 = New System.Windows.Forms.Button()
        Me.LoadedFileLbl = New System.Windows.Forms.Label()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.AnalyzeDataTxBx = New System.Windows.Forms.TextBox()
        Me.FreqDistTabs = New System.Windows.Forms.TabControl()
        Me.BasicAnalytics = New System.Windows.Forms.TabPage()
        Me.Chart2 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.BasicTab = New System.Windows.Forms.TabPage()
        Me.AppendToCurrentRB = New System.Windows.Forms.RadioButton()
        Me.LoadToErrorRB = New System.Windows.Forms.RadioButton()
        Me.InstrucBasicStatsRTB = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UseTopRB = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.NumbersOnlyRB = New System.Windows.Forms.RadioButton()
        Me.SoundexRB = New System.Windows.Forms.RadioButton()
        Me.LettersOnlyUpperCaseRB = New System.Windows.Forms.RadioButton()
        Me.RawRB = New System.Windows.Forms.RadioButton()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.HighestLBL = New System.Windows.Forms.Label()
        Me.StdDevLbl = New System.Windows.Forms.Label()
        Me.AvgLbl = New System.Windows.Forms.Label()
        Me.TopLbl = New System.Windows.Forms.Label()
        Me.TopTxBx = New System.Windows.Forms.TextBox()
        Me.AttribueDataGridView = New System.Windows.Forms.DataGridView()
        Me.LookAtBtn = New System.Windows.Forms.Button()
        Me.loadedFile2lbl = New System.Windows.Forms.Label()
        Me.LoadCheckedListBox = New System.Windows.Forms.CheckedListBox()
        Me.FreqDistTab = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.structLBL = New System.Windows.Forms.Label()
        Me.HeaderCheckedListBox = New System.Windows.Forms.CheckedListBox()
        Me.CreateDisBtn = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.FileViewTab = New System.Windows.Forms.TabPage()
        Me.instructFGazeRTB = New System.Windows.Forms.RichTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LoadFileGrid = New System.Windows.Forms.Button()
        Me.FileDataGridView = New System.Windows.Forms.DataGridView()
        Me.LFileLbl = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.FindErrors = New System.Windows.Forms.TabPage()
        Me.SaveErrorsBtn = New System.Windows.Forms.Button()
        Me.ECountLbl = New System.Windows.Forms.Label()
        Me.FindsLBL = New System.Windows.Forms.Label()
        Me.instrucErrorsRTB = New System.Windows.Forms.RichTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ErrorsLbl = New System.Windows.Forms.Label()
        Me.FindErrorsBtn = New System.Windows.Forms.Button()
        Me.ErrorsRichTextBox = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip6 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Sellect_AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToClipboardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteFromClipboardToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveFileDialog2 = New System.Windows.Forms.SaveFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.AnalyzeDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FreqDistTabs.SuspendLayout()
        Me.BasicAnalytics.SuspendLayout()
        CType(Me.Chart2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BasicTab.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.AttribueDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FreqDistTab.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FileViewTab.SuspendLayout()
        CType(Me.FileDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FindErrors.SuspendLayout()
        Me.ContextMenuStrip6.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'AnalyzeDataGridView
        '
        Me.AnalyzeDataGridView.AllowUserToAddRows = False
        Me.AnalyzeDataGridView.AllowUserToDeleteRows = False
        Me.AnalyzeDataGridView.AllowUserToResizeColumns = False
        Me.AnalyzeDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.AnalyzeDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.AnalyzeDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.AnalyzeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AnalyzeDataGridView.Location = New System.Drawing.Point(8, 7)
        Me.AnalyzeDataGridView.Name = "AnalyzeDataGridView"
        Me.AnalyzeDataGridView.ReadOnly = True
        Me.AnalyzeDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AnalyzeDataGridView.Size = New System.Drawing.Size(1217, 206)
        Me.AnalyzeDataGridView.TabIndex = 0
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'DataLoadBtn
        '
        Me.DataLoadBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.DataLoadBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataLoadBtn.ForeColor = System.Drawing.Color.Black
        Me.DataLoadBtn.Location = New System.Drawing.Point(1151, 513)
        Me.DataLoadBtn.Name = "DataLoadBtn"
        Me.DataLoadBtn.Size = New System.Drawing.Size(70, 30)
        Me.DataLoadBtn.TabIndex = 1
        Me.DataLoadBtn.Text = "Load"
        Me.DataLoadBtn.UseVisualStyleBackColor = False
        '
        'Exit_Btn2
        '
        Me.Exit_Btn2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Exit_Btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Exit_Btn2.ForeColor = System.Drawing.Color.Black
        Me.Exit_Btn2.Location = New System.Drawing.Point(1231, 624)
        Me.Exit_Btn2.Name = "Exit_Btn2"
        Me.Exit_Btn2.Size = New System.Drawing.Size(70, 28)
        Me.Exit_Btn2.TabIndex = 2
        Me.Exit_Btn2.Text = "Close"
        Me.Exit_Btn2.UseVisualStyleBackColor = False
        '
        'LoadedFileLbl
        '
        Me.LoadedFileLbl.AutoSize = True
        Me.LoadedFileLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoadedFileLbl.ForeColor = System.Drawing.Color.Black
        Me.LoadedFileLbl.Location = New System.Drawing.Point(173, 53)
        Me.LoadedFileLbl.Name = "LoadedFileLbl"
        Me.LoadedFileLbl.Size = New System.Drawing.Size(104, 17)
        Me.LoadedFileLbl.TabIndex = 3
        Me.LoadedFileLbl.Text = "Loaded File is: "
        '
        'Chart1
        '
        ChartArea1.AxisX.IsLabelAutoFit = False
        ChartArea1.AxisX.LabelAutoFitMinFontSize = 10
        ChartArea1.AxisX.LabelAutoFitStyle = CType((((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.IncreaseFont Or System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.DecreaseFont) _
            Or System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.StaggeredLabels) _
            Or System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep30), System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles)
        ChartArea1.AxisX.LabelStyle.Angle = 45
        ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.SystemColors.HotTrack
        ChartArea1.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Enabled = False
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(176, 85)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "ChartArea1"
        Series1.Color = System.Drawing.Color.Maroon
        Series1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Series1.IsValueShownAsLabel = True
        Series1.LabelForeColor = System.Drawing.SystemColors.HotTrack
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Size = New System.Drawing.Size(1045, 445)
        Me.Chart1.TabIndex = 4
        Me.Chart1.Text = "Chart1"
        '
        'AnalyzeDataTxBx
        '
        Me.AnalyzeDataTxBx.Location = New System.Drawing.Point(823, 517)
        Me.AnalyzeDataTxBx.Margin = New System.Windows.Forms.Padding(2)
        Me.AnalyzeDataTxBx.Name = "AnalyzeDataTxBx"
        Me.AnalyzeDataTxBx.ReadOnly = True
        Me.AnalyzeDataTxBx.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.AnalyzeDataTxBx.Size = New System.Drawing.Size(312, 22)
        Me.AnalyzeDataTxBx.TabIndex = 6
        '
        'FreqDistTabs
        '
        Me.FreqDistTabs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FreqDistTabs.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.FreqDistTabs.Controls.Add(Me.BasicAnalytics)
        Me.FreqDistTabs.Controls.Add(Me.BasicTab)
        Me.FreqDistTabs.Controls.Add(Me.FreqDistTab)
        Me.FreqDistTabs.Controls.Add(Me.FileViewTab)
        Me.FreqDistTabs.Controls.Add(Me.FindErrors)
        Me.FreqDistTabs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FreqDistTabs.Location = New System.Drawing.Point(0, 28)
        Me.FreqDistTabs.Name = "FreqDistTabs"
        Me.FreqDistTabs.SelectedIndex = 0
        Me.FreqDistTabs.Size = New System.Drawing.Size(1326, 588)
        Me.FreqDistTabs.TabIndex = 10
        '
        'BasicAnalytics
        '
        Me.BasicAnalytics.BackColor = System.Drawing.Color.AliceBlue
        Me.BasicAnalytics.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BasicAnalytics.Controls.Add(Me.Chart2)
        Me.BasicAnalytics.Controls.Add(Me.PictureBox6)
        Me.BasicAnalytics.Controls.Add(Me.AnalyzeDataGridView)
        Me.BasicAnalytics.Controls.Add(Me.AnalyzeDataTxBx)
        Me.BasicAnalytics.Controls.Add(Me.DataLoadBtn)
        Me.BasicAnalytics.Location = New System.Drawing.Point(4, 28)
        Me.BasicAnalytics.Name = "BasicAnalytics"
        Me.BasicAnalytics.Padding = New System.Windows.Forms.Padding(3)
        Me.BasicAnalytics.Size = New System.Drawing.Size(1318, 556)
        Me.BasicAnalytics.TabIndex = 0
        Me.BasicAnalytics.Text = "Basic Analytics"
        '
        'Chart2
        '
        Me.Chart2.BorderlineWidth = 3
        ChartArea2.AxisX.IsLabelAutoFit = False
        ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        ChartArea2.AxisX2.IsLabelAutoFit = False
        ChartArea2.AxisX2.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        ChartArea2.AxisY2.IsLabelAutoFit = False
        ChartArea2.AxisY2.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.Name = "ChartArea1"
        Me.Chart2.ChartAreas.Add(ChartArea2)
        Legend2.Enabled = False
        Legend2.Name = "Legend1"
        Me.Chart2.Legends.Add(Legend2)
        Me.Chart2.Location = New System.Drawing.Point(8, 219)
        Me.Chart2.Name = "Chart2"
        Series2.ChartArea = "ChartArea1"
        Series2.Color = System.Drawing.Color.Maroon
        Series2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Series2.IsValueShownAsLabel = True
        Series2.IsVisibleInLegend = False
        Series2.LabelAngle = 45
        Series2.LabelForeColor = System.Drawing.Color.Maroon
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.Chart2.Series.Add(Series2)
        Me.Chart2.Size = New System.Drawing.Size(1217, 278)
        Me.Chart2.TabIndex = 90
        Me.Chart2.Text = "Chart2"
        Title1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Title1.Name = "Non-Null Counts"
        Me.Chart2.Titles.Add(Title1)
        '
        'PictureBox6
        '
        Me.PictureBox6.ErrorImage = CType(resources.GetObject("PictureBox6.ErrorImage"), System.Drawing.Image)
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox6.TabIndex = 89
        Me.PictureBox6.TabStop = False
        '
        'BasicTab
        '
        Me.BasicTab.BackColor = System.Drawing.Color.AliceBlue
        Me.BasicTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BasicTab.Controls.Add(Me.AppendToCurrentRB)
        Me.BasicTab.Controls.Add(Me.LoadToErrorRB)
        Me.BasicTab.Controls.Add(Me.InstrucBasicStatsRTB)
        Me.BasicTab.Controls.Add(Me.Label4)
        Me.BasicTab.Controls.Add(Me.UseTopRB)
        Me.BasicTab.Controls.Add(Me.GroupBox2)
        Me.BasicTab.Controls.Add(Me.PictureBox2)
        Me.BasicTab.Controls.Add(Me.Label1)
        Me.BasicTab.Controls.Add(Me.GroupBox1)
        Me.BasicTab.Controls.Add(Me.TopLbl)
        Me.BasicTab.Controls.Add(Me.TopTxBx)
        Me.BasicTab.Controls.Add(Me.AttribueDataGridView)
        Me.BasicTab.Controls.Add(Me.LookAtBtn)
        Me.BasicTab.Controls.Add(Me.loadedFile2lbl)
        Me.BasicTab.Controls.Add(Me.LoadCheckedListBox)
        Me.BasicTab.Location = New System.Drawing.Point(4, 28)
        Me.BasicTab.Name = "BasicTab"
        Me.BasicTab.Size = New System.Drawing.Size(1318, 556)
        Me.BasicTab.TabIndex = 2
        Me.BasicTab.Text = "Basic Statistics"
        '
        'AppendToCurrentRB
        '
        Me.AppendToCurrentRB.AutoSize = True
        Me.AppendToCurrentRB.Location = New System.Drawing.Point(291, 488)
        Me.AppendToCurrentRB.Name = "AppendToCurrentRB"
        Me.AppendToCurrentRB.Size = New System.Drawing.Size(184, 20)
        Me.AppendToCurrentRB.TabIndex = 114
        Me.AppendToCurrentRB.TabStop = True
        Me.AppendToCurrentRB.Text = "Append to Error Discovery"
        Me.AppendToCurrentRB.UseVisualStyleBackColor = True
        '
        'LoadToErrorRB
        '
        Me.LoadToErrorRB.AutoSize = True
        Me.LoadToErrorRB.Location = New System.Drawing.Point(291, 462)
        Me.LoadToErrorRB.Name = "LoadToErrorRB"
        Me.LoadToErrorRB.Size = New System.Drawing.Size(196, 20)
        Me.LoadToErrorRB.TabIndex = 113
        Me.LoadToErrorRB.TabStop = True
        Me.LoadToErrorRB.Text = "Copy to Error Discovery Tab"
        Me.LoadToErrorRB.UseVisualStyleBackColor = True
        '
        'InstrucBasicStatsRTB
        '
        Me.InstrucBasicStatsRTB.BackColor = System.Drawing.Color.AliceBlue
        Me.InstrucBasicStatsRTB.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.InstrucBasicStatsRTB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstrucBasicStatsRTB.Location = New System.Drawing.Point(31, 22)
        Me.InstrucBasicStatsRTB.Name = "InstrucBasicStatsRTB"
        Me.InstrucBasicStatsRTB.ReadOnly = True
        Me.InstrucBasicStatsRTB.Size = New System.Drawing.Size(593, 106)
        Me.InstrucBasicStatsRTB.TabIndex = 112
        Me.InstrucBasicStatsRTB.Text = resources.GetString("InstrucBasicStatsRTB.Text")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Maroon
        Me.Label4.Location = New System.Drawing.Point(671, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(199, 31)
        Me.Label4.TabIndex = 111
        Me.Label4.Text = "Basic Statistics"
        '
        'UseTopRB
        '
        Me.UseTopRB.AutoSize = True
        Me.UseTopRB.Location = New System.Drawing.Point(403, 168)
        Me.UseTopRB.Name = "UseTopRB"
        Me.UseTopRB.Size = New System.Drawing.Size(79, 20)
        Me.UseTopRB.TabIndex = 110
        Me.UseTopRB.TabStop = True
        Me.UseTopRB.Text = "Use Top"
        Me.UseTopRB.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NumbersOnlyRB)
        Me.GroupBox2.Controls.Add(Me.SoundexRB)
        Me.GroupBox2.Controls.Add(Me.LettersOnlyUpperCaseRB)
        Me.GroupBox2.Controls.Add(Me.RawRB)
        Me.GroupBox2.Location = New System.Drawing.Point(291, 201)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 250)
        Me.GroupBox2.TabIndex = 109
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pick a  Method"
        '
        'NumbersOnlyRB
        '
        Me.NumbersOnlyRB.AutoSize = True
        Me.NumbersOnlyRB.Location = New System.Drawing.Point(50, 200)
        Me.NumbersOnlyRB.Name = "NumbersOnlyRB"
        Me.NumbersOnlyRB.Size = New System.Drawing.Size(111, 20)
        Me.NumbersOnlyRB.TabIndex = 3
        Me.NumbersOnlyRB.TabStop = True
        Me.NumbersOnlyRB.Text = "Numbers Only"
        Me.NumbersOnlyRB.UseVisualStyleBackColor = True
        '
        'SoundexRB
        '
        Me.SoundexRB.AutoSize = True
        Me.SoundexRB.Location = New System.Drawing.Point(50, 152)
        Me.SoundexRB.Name = "SoundexRB"
        Me.SoundexRB.Size = New System.Drawing.Size(79, 20)
        Me.SoundexRB.TabIndex = 2
        Me.SoundexRB.TabStop = True
        Me.SoundexRB.Text = "Soundex"
        Me.SoundexRB.UseVisualStyleBackColor = True
        '
        'LettersOnlyUpperCaseRB
        '
        Me.LettersOnlyUpperCaseRB.AutoSize = True
        Me.LettersOnlyUpperCaseRB.Location = New System.Drawing.Point(50, 88)
        Me.LettersOnlyUpperCaseRB.Name = "LettersOnlyUpperCaseRB"
        Me.LettersOnlyUpperCaseRB.Size = New System.Drawing.Size(99, 36)
        Me.LettersOnlyUpperCaseRB.TabIndex = 1
        Me.LettersOnlyUpperCaseRB.TabStop = True
        Me.LettersOnlyUpperCaseRB.Text = "Letters Only " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Upper Case"
        Me.LettersOnlyUpperCaseRB.UseVisualStyleBackColor = True
        '
        'RawRB
        '
        Me.RawRB.AutoSize = True
        Me.RawRB.Location = New System.Drawing.Point(50, 40)
        Me.RawRB.Name = "RawRB"
        Me.RawRB.Size = New System.Drawing.Size(53, 20)
        Me.RawRB.TabIndex = 0
        Me.RawRB.TabStop = True
        Me.RawRB.Text = "Raw"
        Me.RawRB.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.ErrorImage = CType(resources.GetObject("PictureBox2.ErrorImage"), System.Drawing.Image)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox2.TabIndex = 108
        Me.PictureBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(116, 143)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 18)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "Choose an Attribute"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.HighestLBL)
        Me.GroupBox1.Controls.Add(Me.StdDevLbl)
        Me.GroupBox1.Controls.Add(Me.AvgLbl)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(1033, 194)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 238)
        Me.GroupBox1.TabIndex = 106
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Simple Stats"
        '
        'HighestLBL
        '
        Me.HighestLBL.AutoSize = True
        Me.HighestLBL.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HighestLBL.Location = New System.Drawing.Point(15, 59)
        Me.HighestLBL.Name = "HighestLBL"
        Me.HighestLBL.Size = New System.Drawing.Size(70, 17)
        Me.HighestLBL.TabIndex = 0
        Me.HighestLBL.Text = "Max Freq:"
        '
        'StdDevLbl
        '
        Me.StdDevLbl.AutoSize = True
        Me.StdDevLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StdDevLbl.Location = New System.Drawing.Point(23, 168)
        Me.StdDevLbl.Name = "StdDevLbl"
        Me.StdDevLbl.Size = New System.Drawing.Size(62, 16)
        Me.StdDevLbl.TabIndex = 2
        Me.StdDevLbl.Text = "Std. Dev:"
        '
        'AvgLbl
        '
        Me.AvgLbl.AutoSize = True
        Me.AvgLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AvgLbl.Location = New System.Drawing.Point(16, 114)
        Me.AvgLbl.Name = "AvgLbl"
        Me.AvgLbl.Size = New System.Drawing.Size(69, 16)
        Me.AvgLbl.TabIndex = 1
        Me.AvgLbl.Text = "Avg. Freq:"
        '
        'TopLbl
        '
        Me.TopLbl.AutoSize = True
        Me.TopLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TopLbl.Location = New System.Drawing.Point(300, 170)
        Me.TopLbl.Name = "TopLbl"
        Me.TopLbl.Size = New System.Drawing.Size(33, 16)
        Me.TopLbl.TabIndex = 105
        Me.TopLbl.Text = "Top"
        '
        'TopTxBx
        '
        Me.TopTxBx.Location = New System.Drawing.Point(339, 168)
        Me.TopTxBx.Name = "TopTxBx"
        Me.TopTxBx.Size = New System.Drawing.Size(55, 22)
        Me.TopTxBx.TabIndex = 104
        Me.TopTxBx.Text = "100"
        '
        'AttribueDataGridView
        '
        Me.AttribueDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.AttribueDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.AttribueDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AttribueDataGridView.Location = New System.Drawing.Point(523, 175)
        Me.AttribueDataGridView.Name = "AttribueDataGridView"
        Me.AttribueDataGridView.Size = New System.Drawing.Size(478, 276)
        Me.AttribueDataGridView.TabIndex = 103
        '
        'LookAtBtn
        '
        Me.LookAtBtn.ForeColor = System.Drawing.Color.Maroon
        Me.LookAtBtn.Location = New System.Drawing.Point(150, 474)
        Me.LookAtBtn.Name = "LookAtBtn"
        Me.LookAtBtn.Size = New System.Drawing.Size(70, 30)
        Me.LookAtBtn.TabIndex = 102
        Me.LookAtBtn.Text = "Analyze"
        Me.LookAtBtn.UseVisualStyleBackColor = True
        '
        'loadedFile2lbl
        '
        Me.loadedFile2lbl.AutoSize = True
        Me.loadedFile2lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loadedFile2lbl.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.loadedFile2lbl.Location = New System.Drawing.Point(520, 146)
        Me.loadedFile2lbl.Name = "loadedFile2lbl"
        Me.loadedFile2lbl.Size = New System.Drawing.Size(104, 17)
        Me.loadedFile2lbl.TabIndex = 101
        Me.loadedFile2lbl.Text = "Loaded File is: "
        '
        'LoadCheckedListBox
        '
        Me.LoadCheckedListBox.FormattingEnabled = True
        Me.LoadCheckedListBox.Location = New System.Drawing.Point(115, 175)
        Me.LoadCheckedListBox.Name = "LoadCheckedListBox"
        Me.LoadCheckedListBox.Size = New System.Drawing.Size(140, 276)
        Me.LoadCheckedListBox.TabIndex = 100
        '
        'FreqDistTab
        '
        Me.FreqDistTab.BackColor = System.Drawing.Color.AliceBlue
        Me.FreqDistTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FreqDistTab.Controls.Add(Me.Label3)
        Me.FreqDistTab.Controls.Add(Me.structLBL)
        Me.FreqDistTab.Controls.Add(Me.HeaderCheckedListBox)
        Me.FreqDistTab.Controls.Add(Me.CreateDisBtn)
        Me.FreqDistTab.Controls.Add(Me.PictureBox1)
        Me.FreqDistTab.Controls.Add(Me.Chart1)
        Me.FreqDistTab.Controls.Add(Me.LoadedFileLbl)
        Me.FreqDistTab.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.FreqDistTab.Location = New System.Drawing.Point(4, 28)
        Me.FreqDistTab.Name = "FreqDistTab"
        Me.FreqDistTab.Padding = New System.Windows.Forms.Padding(3)
        Me.FreqDistTab.Size = New System.Drawing.Size(1318, 556)
        Me.FreqDistTab.TabIndex = 1
        Me.FreqDistTab.Text = "Attribute Histograms"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(671, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(312, 31)
        Me.Label3.TabIndex = 112
        Me.Label3.Text = "Data Attribute Histogram"
        '
        'structLBL
        '
        Me.structLBL.AutoSize = True
        Me.structLBL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.structLBL.ForeColor = System.Drawing.Color.Maroon
        Me.structLBL.Location = New System.Drawing.Point(27, 349)
        Me.structLBL.Name = "structLBL"
        Me.structLBL.Size = New System.Drawing.Size(124, 16)
        Me.structLBL.TabIndex = 93
        Me.structLBL.Text = "Choose an Attribute"
        '
        'HeaderCheckedListBox
        '
        Me.HeaderCheckedListBox.FormattingEnabled = True
        Me.HeaderCheckedListBox.Location = New System.Drawing.Point(29, 114)
        Me.HeaderCheckedListBox.Name = "HeaderCheckedListBox"
        Me.HeaderCheckedListBox.Size = New System.Drawing.Size(120, 225)
        Me.HeaderCheckedListBox.TabIndex = 91
        '
        'CreateDisBtn
        '
        Me.CreateDisBtn.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CreateDisBtn.Location = New System.Drawing.Point(54, 397)
        Me.CreateDisBtn.Name = "CreateDisBtn"
        Me.CreateDisBtn.Size = New System.Drawing.Size(70, 30)
        Me.CreateDisBtn.TabIndex = 92
        Me.CreateDisBtn.Text = "Graph"
        Me.CreateDisBtn.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = CType(resources.GetObject("PictureBox1.ErrorImage"), System.Drawing.Image)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox1.TabIndex = 90
        Me.PictureBox1.TabStop = False
        '
        'FileViewTab
        '
        Me.FileViewTab.BackColor = System.Drawing.Color.AliceBlue
        Me.FileViewTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FileViewTab.Controls.Add(Me.instructFGazeRTB)
        Me.FileViewTab.Controls.Add(Me.Label5)
        Me.FileViewTab.Controls.Add(Me.LoadFileGrid)
        Me.FileViewTab.Controls.Add(Me.FileDataGridView)
        Me.FileViewTab.Controls.Add(Me.LFileLbl)
        Me.FileViewTab.Controls.Add(Me.PictureBox3)
        Me.FileViewTab.Location = New System.Drawing.Point(4, 28)
        Me.FileViewTab.Name = "FileViewTab"
        Me.FileViewTab.Size = New System.Drawing.Size(1318, 556)
        Me.FileViewTab.TabIndex = 3
        Me.FileViewTab.Text = "File Gazing"
        '
        'instructFGazeRTB
        '
        Me.instructFGazeRTB.BackColor = System.Drawing.Color.AliceBlue
        Me.instructFGazeRTB.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.instructFGazeRTB.Location = New System.Drawing.Point(28, 24)
        Me.instructFGazeRTB.Name = "instructFGazeRTB"
        Me.instructFGazeRTB.ReadOnly = True
        Me.instructFGazeRTB.Size = New System.Drawing.Size(587, 58)
        Me.instructFGazeRTB.TabIndex = 115
        Me.instructFGazeRTB.Text = resources.GetString("instructFGazeRTB.Text")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Maroon
        Me.Label5.Location = New System.Drawing.Point(671, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(343, 31)
        Me.Label5.TabIndex = 114
        Me.Label5.Text = "Gaze and Peruse Input File"
        '
        'LoadFileGrid
        '
        Me.LoadFileGrid.Location = New System.Drawing.Point(1227, 508)
        Me.LoadFileGrid.Name = "LoadFileGrid"
        Me.LoadFileGrid.Size = New System.Drawing.Size(70, 30)
        Me.LoadFileGrid.TabIndex = 113
        Me.LoadFileGrid.Text = "Load It"
        Me.LoadFileGrid.UseVisualStyleBackColor = True
        '
        'FileDataGridView
        '
        Me.FileDataGridView.AllowUserToAddRows = False
        Me.FileDataGridView.AllowUserToDeleteRows = False
        Me.FileDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.FileDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.FileDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.FileDataGridView.Location = New System.Drawing.Point(25, 115)
        Me.FileDataGridView.Name = "FileDataGridView"
        Me.FileDataGridView.ReadOnly = True
        Me.FileDataGridView.Size = New System.Drawing.Size(1279, 380)
        Me.FileDataGridView.TabIndex = 112
        '
        'LFileLbl
        '
        Me.LFileLbl.AutoSize = True
        Me.LFileLbl.Location = New System.Drawing.Point(25, 91)
        Me.LFileLbl.Name = "LFileLbl"
        Me.LFileLbl.Size = New System.Drawing.Size(99, 16)
        Me.LFileLbl.TabIndex = 111
        Me.LFileLbl.Text = "Loaded File is: "
        '
        'PictureBox3
        '
        Me.PictureBox3.ErrorImage = CType(resources.GetObject("PictureBox3.ErrorImage"), System.Drawing.Image)
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox3.TabIndex = 109
        Me.PictureBox3.TabStop = False
        '
        'FindErrors
        '
        Me.FindErrors.BackColor = System.Drawing.Color.AliceBlue
        Me.FindErrors.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FindErrors.Controls.Add(Me.SaveErrorsBtn)
        Me.FindErrors.Controls.Add(Me.ECountLbl)
        Me.FindErrors.Controls.Add(Me.FindsLBL)
        Me.FindErrors.Controls.Add(Me.instrucErrorsRTB)
        Me.FindErrors.Controls.Add(Me.Label6)
        Me.FindErrors.Controls.Add(Me.ErrorsLbl)
        Me.FindErrors.Controls.Add(Me.FindErrorsBtn)
        Me.FindErrors.Controls.Add(Me.ErrorsRichTextBox)
        Me.FindErrors.Controls.Add(Me.PictureBox4)
        Me.FindErrors.Location = New System.Drawing.Point(4, 28)
        Me.FindErrors.Name = "FindErrors"
        Me.FindErrors.Size = New System.Drawing.Size(1318, 556)
        Me.FindErrors.TabIndex = 4
        Me.FindErrors.Text = "Error Discovery"
        '
        'SaveErrorsBtn
        '
        Me.SaveErrorsBtn.Location = New System.Drawing.Point(1227, 510)
        Me.SaveErrorsBtn.Name = "SaveErrorsBtn"
        Me.SaveErrorsBtn.Size = New System.Drawing.Size(64, 30)
        Me.SaveErrorsBtn.TabIndex = 118
        Me.SaveErrorsBtn.Text = "Save"
        Me.SaveErrorsBtn.UseVisualStyleBackColor = True
        '
        'ECountLbl
        '
        Me.ECountLbl.AutoSize = True
        Me.ECountLbl.Location = New System.Drawing.Point(674, 106)
        Me.ECountLbl.Name = "ECountLbl"
        Me.ECountLbl.Size = New System.Drawing.Size(174, 16)
        Me.ECountLbl.TabIndex = 117
        Me.ECountLbl.Text = "Possible Error Line Count is:"
        '
        'FindsLBL
        '
        Me.FindsLBL.AutoSize = True
        Me.FindsLBL.Location = New System.Drawing.Point(25, 494)
        Me.FindsLBL.Name = "FindsLBL"
        Me.FindsLBL.Size = New System.Drawing.Size(34, 16)
        Me.FindsLBL.TabIndex = 116
        Me.FindsLBL.Text = "Hits:"
        '
        'instrucErrorsRTB
        '
        Me.instrucErrorsRTB.BackColor = System.Drawing.Color.AliceBlue
        Me.instrucErrorsRTB.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.instrucErrorsRTB.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.instrucErrorsRTB.Location = New System.Drawing.Point(25, 26)
        Me.instrucErrorsRTB.Name = "instrucErrorsRTB"
        Me.instrucErrorsRTB.ReadOnly = True
        Me.instrucErrorsRTB.Size = New System.Drawing.Size(601, 59)
        Me.instrucErrorsRTB.TabIndex = 115
        Me.instrucErrorsRTB.Text = "Possible Errors:      NA, '' ,  NANA ,  NANANA , UNKNOWN , #NAME? , NULL , " & Global.Microsoft.VisualBasic.ChrW(10) & "<  , " &
    "> , ! ,  $ , = ,  ^ , % ,  & , ? , VALUE , + , : ,  _  , AAA to ZZZ, U, # U"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Maroon
        Me.Label6.Location = New System.Drawing.Point(671, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(258, 31)
        Me.Label6.TabIndex = 114
        Me.Label6.Text = "Find Possible Errors"
        '
        'ErrorsLbl
        '
        Me.ErrorsLbl.AutoSize = True
        Me.ErrorsLbl.Location = New System.Drawing.Point(25, 106)
        Me.ErrorsLbl.Name = "ErrorsLbl"
        Me.ErrorsLbl.Size = New System.Drawing.Size(99, 16)
        Me.ErrorsLbl.TabIndex = 113
        Me.ErrorsLbl.Text = "Loaded File is: "
        '
        'FindErrorsBtn
        '
        Me.FindErrorsBtn.Location = New System.Drawing.Point(1104, 510)
        Me.FindErrorsBtn.Name = "FindErrorsBtn"
        Me.FindErrorsBtn.Size = New System.Drawing.Size(70, 30)
        Me.FindErrorsBtn.TabIndex = 112
        Me.FindErrorsBtn.Text = "Find"
        Me.FindErrorsBtn.UseVisualStyleBackColor = True
        '
        'ErrorsRichTextBox
        '
        Me.ErrorsRichTextBox.ContextMenuStrip = Me.ContextMenuStrip6
        Me.ErrorsRichTextBox.Location = New System.Drawing.Point(25, 130)
        Me.ErrorsRichTextBox.Name = "ErrorsRichTextBox"
        Me.ErrorsRichTextBox.Size = New System.Drawing.Size(1279, 354)
        Me.ErrorsRichTextBox.TabIndex = 111
        Me.ErrorsRichTextBox.Text = ""
        Me.ErrorsRichTextBox.WordWrap = False
        '
        'ContextMenuStrip6
        '
        Me.ContextMenuStrip6.AllowMerge = False
        Me.ContextMenuStrip6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Sellect_AllToolStripMenuItem, Me.CopyToClipboardToolStripMenuItem, Me.PasteFromClipboardToolStripMenuItem1})
        Me.ContextMenuStrip6.Name = "ContextMenuStrip6"
        Me.ContextMenuStrip6.Size = New System.Drawing.Size(204, 70)
        '
        'Sellect_AllToolStripMenuItem
        '
        Me.Sellect_AllToolStripMenuItem.Name = "Sellect_AllToolStripMenuItem"
        Me.Sellect_AllToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.Sellect_AllToolStripMenuItem.Text = "Select All"
        '
        'CopyToClipboardToolStripMenuItem
        '
        Me.CopyToClipboardToolStripMenuItem.Name = "CopyToClipboardToolStripMenuItem"
        Me.CopyToClipboardToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.CopyToClipboardToolStripMenuItem.Text = "Copy to Clipboard"
        '
        'PasteFromClipboardToolStripMenuItem1
        '
        Me.PasteFromClipboardToolStripMenuItem1.Name = "PasteFromClipboardToolStripMenuItem1"
        Me.PasteFromClipboardToolStripMenuItem1.Size = New System.Drawing.Size(203, 22)
        Me.PasteFromClipboardToolStripMenuItem1.Text = "Paste From Clipboard"
        '
        'PictureBox4
        '
        Me.PictureBox4.ErrorImage = CType(resources.GetObject("PictureBox4.ErrorImage"), System.Drawing.Image)
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(1230, 14)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(74, 83)
        Me.PictureBox4.TabIndex = 110
        Me.PictureBox4.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AutoSize = False
        Me.MenuStrip1.BackColor = System.Drawing.Color.Maroon
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip1.Size = New System.Drawing.Size(1327, 25)
        Me.MenuStrip1.TabIndex = 11
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(45, 21)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.BackColor = System.Drawing.Color.Maroon
        Me.CloseToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(115, 24)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'AnalyzeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Maroon
        Me.ClientSize = New System.Drawing.Size(1327, 657)
        Me.Controls.Add(Me.FreqDistTabs)
        Me.Controls.Add(Me.Exit_Btn2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.Name = "AnalyzeForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Quick Data Analysis"
        CType(Me.AnalyzeDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FreqDistTabs.ResumeLayout(False)
        Me.BasicAnalytics.ResumeLayout(False)
        Me.BasicAnalytics.PerformLayout()
        CType(Me.Chart2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BasicTab.ResumeLayout(False)
        Me.BasicTab.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.AttribueDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FreqDistTab.ResumeLayout(False)
        Me.FreqDistTab.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FileViewTab.ResumeLayout(False)
        Me.FileViewTab.PerformLayout()
        CType(Me.FileDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FindErrors.ResumeLayout(False)
        Me.FindErrors.PerformLayout()
        Me.ContextMenuStrip6.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents AnalyzeDataGridView As DataGridView
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents DataLoadBtn As Button
    Friend WithEvents Exit_Btn2 As Button
    Friend WithEvents LoadedFileLbl As Label
    Friend WithEvents Chart1 As DataVisualization.Charting.Chart
    Friend WithEvents AnalyzeDataTxBx As TextBox
    Friend WithEvents FreqDistTabs As TabControl
    Friend WithEvents BasicAnalytics As TabPage
    Friend WithEvents FreqDistTab As TabPage
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents CloseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HeaderCheckedListBox As CheckedListBox
    Friend WithEvents CreateDisBtn As Button
    Friend WithEvents structLBL As Label
    Friend WithEvents Chart2 As DataVisualization.Charting.Chart
    Friend WithEvents BasicTab As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents HighestLBL As Label
    Friend WithEvents StdDevLbl As Label
    Friend WithEvents AvgLbl As Label
    Friend WithEvents TopLbl As Label
    Friend WithEvents TopTxBx As TextBox
    Friend WithEvents AttribueDataGridView As DataGridView
    Friend WithEvents LookAtBtn As Button
    Friend WithEvents loadedFile2lbl As Label
    Friend WithEvents LoadCheckedListBox As CheckedListBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents FileViewTab As TabPage
    Friend WithEvents LFileLbl As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents SoundexRB As RadioButton
    Friend WithEvents LettersOnlyUpperCaseRB As RadioButton
    Friend WithEvents RawRB As RadioButton
    Friend WithEvents FileDataGridView As DataGridView
    Friend WithEvents NumbersOnlyRB As RadioButton
    Friend WithEvents FindErrors As TabPage
    Friend WithEvents FindErrorsBtn As Button
    Friend WithEvents ErrorsRichTextBox As RichTextBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents UseTopRB As RadioButton
    Friend WithEvents LoadFileGrid As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents ErrorsLbl As Label
    Friend WithEvents InstrucBasicStatsRTB As RichTextBox
    Friend WithEvents instrucErrorsRTB As RichTextBox
    Friend WithEvents FindsLBL As Label
    Friend WithEvents ECountLbl As Label
    Friend WithEvents ContextMenuStrip6 As ContextMenuStrip
    Friend WithEvents Sellect_AllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CopyToClipboardToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PasteFromClipboardToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents SaveErrorsBtn As Button
    Friend WithEvents SaveFileDialog2 As SaveFileDialog
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents LoadToErrorRB As RadioButton
    Friend WithEvents AppendToCurrentRB As RadioButton
    Friend WithEvents instructFGazeRTB As RichTextBox
End Class
